Electron source code lives here: https://github.com/Unity-Technologies/electron/

It gets built on Yamato: https://yamato.prd.cds.internal.unity3d.com/jobs/1058-Electron

We currently only build Electron for macOS. On Windows and Linux, we use the one from node packages.