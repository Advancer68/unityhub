const axios = require('axios');
const globalAgent = require('global-agent');
const proxyHelper = require('./proxyHelper');
const logger = require('./logger')('NetworkSetup');
class NetworkSetup {
    start() {
        try {
            this.originalEnvVars = Object.assign({}, process.env);
            proxyHelper.parseEnvironmentVariables();
            this.setupAxiosDefaults();
            this.initializeGlobalAgent();
        }
        catch (e) {
            logger.warn('failed to setup the network proxy', e);
        }
    }
    setupAxiosDefaults() {
        axios.defaults.proxy = false;
    }
    initializeGlobalAgent() {
        globalAgent.bootstrap();
        if (proxyHelper.http) {
            global.GLOBAL_AGENT.HTTP_PROXY = proxyHelper.http.string;
        }
        if (proxyHelper.https) {
            global.GLOBAL_AGENT.HTTPS_PROXY = proxyHelper.https.string;
        }
    }
}
module.exports = new NetworkSetup();
//# sourceMappingURL=networkSetup.js.map