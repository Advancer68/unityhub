const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;
const sinonChai = require("sinon-chai");
const logger = require('./logger')('loggerSpec');
chai.use(sinonChai);
describe('logger.js', function () {
    describe('_concatLogs', () => {
        it('should concatenate its input delimited by |', () => {
            expect(logger._concatLogs('test1', 'test2')).to.equal("'test1'|'test2'");
        });
        it('should flatten the objects', () => {
            expect(logger._concatLogs({ test1: 'test1' })).to.equal('{ test1: \'test1\' }');
        });
        it('should accepts all the types', () => {
            expect(logger._concatLogs([], [1, 2, 3], { t1: 't1', t2: { t3: 1 } }, 1, null, undefined))
                .to.equal("[]|[ 1, 2, 3 ]|{ t1: 't1', t2: { t3: 1 } }|1|null|undefined");
        });
        it('should not fail with no input', () => {
            expect(logger._concatLogs()).to.equal('');
        });
    });
});
//# sourceMappingURL=logger.spec.js.map