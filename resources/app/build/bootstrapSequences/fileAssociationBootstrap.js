var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const BaseBootstrap = require('./baseBootstrap');
const unityHubProtocolHandler = require('../unityhubProtocolHandler');
const hubIPCService = require('../services/localIPC/hubIPCService');
const logger = require('../logger')('fileAssociationBootstrap');
class FileAssociationBootstrap extends BaseBootstrap {
    static start() {
        return __awaiter(this, void 0, void 0, function* () {
            let url;
            process.argv.forEach((arg) => {
                if (arg.includes(unityHubProtocolHandler.fullProtocolName)) {
                    url = arg;
                }
            });
            yield this._sendURLToMainInstance(url);
        });
    }
    static _sendURLToMainInstance(url) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!url) {
                logger.error('Could not find argument with Hub protocol.');
                return;
            }
            const ipcClient = yield hubIPCService.connect();
            ipcClient.on('hub-protocol:url-handled', () => {
                logger.info(`${url} was handled, closing the file association process`);
                process.exit(0);
            });
            logger.info(`Sending handle url request to main thread for ${url}`);
            ipcClient.emit('hub-protocol:handle-url', { url });
        });
    }
}
module.exports = FileAssociationBootstrap;
//# sourceMappingURL=fileAssociationBootstrap.js.map