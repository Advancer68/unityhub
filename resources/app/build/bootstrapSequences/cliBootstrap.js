var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const BaseBootstrap = require('./baseBootstrap');
const app = require('../app.js');
const localCli = require('../services/localCLI/localCLI');
const outputService = require('../services/outputService');
const argv = require('yargs')
    .option('version', {
    alias: 'v',
    describe: 'install the corresponding version',
    type: 'string'
})
    .option('changeset', {
    alias: 'c',
    describe: 'install the corresponding changeset',
    type: 'string'
})
    .option('module', {
    alias: 'm',
    describe: 'install the corresponding modules'
})
    .option('set', {
    alias: 's',
    describe: 'set the install path'
})
    .option('get', {
    alias: 'g',
    describe: 'get the install path'
})
    .option('releases', {
    alias: 'r',
    describe: 'return current Editors releases'
})
    .option('installed', {
    alias: 'i',
    describe: 'return installed Editors'
})
    .option('childModules', {
    alias: 'cm',
    type: 'boolean',
    default: false,
    describe: 'install all child modules of selected modules'
})
    .argv;
const numeral = require('numeral');
class CliBootstrap extends BaseBootstrap {
    static start() {
        app.setFeatureActive('IPC', false);
        app.setFeatureActive('REST', false);
        app.setFeatureActive('UI', false);
        app.setFeatureActive('HUB_PROTOCOL', false);
        app.setFeatureActive('AUTO_UPDATE', false);
        app.setFeatureActive('HEADLESS_MODE', true);
        app.start();
        app.on('ready', () => __awaiter(this, void 0, void 0, function* () {
            localCli.init();
            outputService.init('cli');
            if (argv.headless === 'install') {
                outputService.on('download.progress', (id, stats) => process.stdout.write(`${numeral(stats.total.completed * 0.01).format('0.00%')} of Unity editor ${JSON.stringify(id)} was downloaded \n`));
            }
            outputService.on('installer.start', (id) => process.stdout.write(`Unity Editor ${JSON.stringify(id)} successfully downloaded.\nInstalling Unity Editor ${JSON.stringify(id)}`));
            outputService.on('installer.end', () => process.stdout.write('Installation done'));
            outputService.on('download.nospace', () => process.stdout.write('Failed to download Unity Editor due to lack of available disk space'));
            outputService.on('installer.modules.end', () => process.stdout.write('Modules installed successfully.'));
            ['app.end', 'download.cancel', 'download.error.cluster', 'download.cancelModule', 'installer.modules.end', 'installer.end', 'download.nospace']
                .forEach(event => {
                outputService.on(event, () => {
                    setTimeout((() => app.quitApp()), 1000);
                });
            });
            try {
                yield localCli.handleArgs(argv);
            }
            catch (e) {
                outputService.logForCli(`Failed to execute the command due the following, please see '-- --headless help' for assistance.\n${e}`);
            }
        }));
    }
}
module.exports = CliBootstrap;
//# sourceMappingURL=cliBootstrap.js.map