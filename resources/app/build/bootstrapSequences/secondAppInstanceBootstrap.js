const BaseBootstrap = require('./baseBootstrap');
const logger = require('../logger')('secondAppInstanceBootstrap');
class SecondAppInstanceBootstrap extends BaseBootstrap {
    static start() {
        logger.info('Second instance of the Hub was started. Closing..');
        process.exit(0);
    }
}
module.exports = SecondAppInstanceBootstrap;
//# sourceMappingURL=secondAppInstanceBootstrap.js.map