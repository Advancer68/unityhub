"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const yargs_1 = require("yargs");
const os_1 = __importDefault(require("os"));
const windowManager_1 = __importDefault(require("./windowManager/windowManager"));
const unityhubProtocolHandler_1 = __importDefault(require("./unityhubProtocolHandler"));
const cloudAnalytics_1 = __importDefault(require("./services/cloudAnalytics/cloudAnalytics"));
const baseBootstrap_1 = __importDefault(require("./bootstrapSequences/baseBootstrap"));
const fileAssociationBootstrap_1 = __importDefault(require("./bootstrapSequences/fileAssociationBootstrap"));
const installServiceBootstrap_1 = __importDefault(require("./bootstrapSequences/installServiceBootstrap"));
const secondAppInstanceBootstrap_1 = __importDefault(require("./bootstrapSequences/secondAppInstanceBootstrap"));
const bugReporterBootstrap_1 = __importDefault(require("./bootstrapSequences/bugReporterBootstrap"));
const cliBootstrap_1 = __importDefault(require("./bootstrapSequences/cliBootstrap"));
const onboardingService_1 = __importDefault(require("./services/onboarding/onboardingService"));
class Bootstrap {
    constructor(electronApp) {
        this.isSecondAppInstance = null;
        if (yargs_1.argv.headless)
            return;
        this.isSecondAppInstance = !(electronApp.requestSingleInstanceLock());
        electronApp.on('second-instance', () => __awaiter(this, void 0, void 0, function* () {
            if (onboardingService_1.default.hasOnboarding) {
                const onboardingState = yield onboardingService_1.default.getOnboardingState();
                if (onboardingState.isFinished === false) {
                    windowManager_1.default.onboardingWindow.show();
                    return;
                }
            }
            windowManager_1.default.mainWindow.show();
        }));
    }
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            let bootstrapSequence = baseBootstrap_1.default;
            if (this.isInstallerProcess()) {
                bootstrapSequence = installServiceBootstrap_1.default;
            }
            else if (yargs_1.argv.bugReporter) {
                bootstrapSequence = bugReporterBootstrap_1.default;
            }
            else if (yargs_1.argv.headless) {
                bootstrapSequence = cliBootstrap_1.default;
            }
            else if (this.isSecondAppInstance) {
                yield cloudAnalytics_1.default.quitEvent('Duplicate');
                if (this.isFileAssociationProcess()) {
                    bootstrapSequence = fileAssociationBootstrap_1.default;
                }
                else {
                    bootstrapSequence = secondAppInstanceBootstrap_1.default;
                }
            }
            bootstrapSequence.start();
        });
    }
    isInstallerProcess() {
        return yargs_1.argv.winInstaller && os_1.default.platform() === 'win32';
    }
    isFileAssociationProcess() {
        let hasCustomProtocolURL = false;
        process.argv.forEach((arg) => {
            if (arg.includes(unityhubProtocolHandler_1.default.fullProtocolName)) {
                hasCustomProtocolURL = true;
            }
        });
        return hasCustomProtocolURL;
    }
}
exports.default = Bootstrap;
//# sourceMappingURL=bootstrap.js.map