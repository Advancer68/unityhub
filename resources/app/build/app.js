var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const logger = require('./logger')('App');
const { app, dialog } = require('electron');
require('electron-debug')();
const EventEmitter = require('events');
const machina = require('machina');
const postal = require('postal');
require('machina.postal')(machina, postal);
const auth = require('./services/localAuth/auth');
const licenseCore = require('./services/licenseService/licenseCore');
const licenseClient = require('./services/licenseService/licenseClientProxy');
const localSettings = require('./services/localSettings/localSettings');
const editorManager = require('./services/editorManager/editorManager');
const EditorApp = require('./services/editorApp/editorapp');
const localProject = require('./services/localProject/localProject');
const cloudCollab = require('./services/cloudCollab/cloudCollab');
const localDownload = require('./services/localDownload/unityDownload');
const autoUpdater = require('./services/hubAutoUpdate/auto-updater');
const cloudConfig = require('./services/cloudConfig/cloudConfig');
const localConfig = require('./services/localConfig/localConfig');
const cloudAnalytics = require('./services/cloudAnalytics/cloudAnalytics');
const analyticsQueue = require('./services/cloudAnalytics/analyticsQueue');
const HubRestService = require('./services/localREST/hubRestService');
const HubIPCService = require('./services/localIPC/hubIPCService');
const onboardingService = require('./services/onboarding/onboardingService');
const livePushService = require('./services/livePush/livePushService');
const splashScreenService = require('./services/splashScreen/splashScreenService');
const plasticSCMService = require('./services/plasticSCMService/plasticSCMService');
const windowManager = require('./windowManager/windowManager');
const tokenManager = require('./tokenManager/tokenManager');
const trayMenu = require('./appComponents/trayMenu/trayMenu');
const systemInfo = require('./services/localSystemInfo/systemInfo');
const unityHubProtocolHandler = require('./unityhubProtocolHandler');
const networkInterceptors = require('./services/localAuth/networkInterceptors');
const learnContent = require('./services/learnContent/learnContentService');
const unityPackageManagerService = require('./services/unityPackageManager/unityPackageManagerService');
const openOptionsService = require('./services/openOptions/openOptionsService');
const i18nHelper = require('./i18nHelper');
const argumentParser = require('./appComponents/argumentParser');
const errorBox = require('./appComponents/errorBox');
const applicationMenu = require('./appComponents/applicationMenu');
const applicationRouteHelper = require('./appComponents/applicationRouteHelper');
const networkSetup = require('./networkSetup');
const proxyHelper = require('./proxyHelper');
const pluginManagerLight = require('./services/pluginManager/pluginManagerLight');
let features;
class App extends EventEmitter {
    get UI_RELATED_EVENTS() {
        return ['window-all-closed', 'activate'];
    }
    get events() {
        return {
            'ready': this.initSequence,
            'window-all-closed': this.restoreLicenseFile,
            'activate': this.showProperWindow,
            'before-quit': (event) => {
                if (!this.isQuitting) {
                    this.quitGracefully(event);
                }
            },
            'will-finish-launching': this.setupHubProtocolHandler,
            'login': this.handleLogin,
            'web-contents-created': this.handleRemoteWebContent
        };
    }
    get features() {
        return features;
    }
    setFeatureActive(feature, status) {
        if (features[feature] === undefined) {
            throw new Error(`Cannot set feature status for non-existing feature ${feature}`);
        }
        if (typeof status !== 'boolean') {
            throw new Error(`Cannot set feature status for feature ${feature}. ${status} is not a boolean value.`);
        }
        features[feature] = status;
    }
    constructor() {
        super();
        this.unityHubLabel = 'Unity Hub';
        this.appStartTime = new Date().getTime();
        this.editors = app.editors = {};
        features = {
            IPC: true,
            REST: true,
            UI: true,
            HUB_PROTOCOL: true,
            AUTO_UPDATE: true,
            HEADLESS_MODE: false
        };
        app.setAppUserModelId('com.unity3d.unityhub');
        app.setLoginItemSettings({
            openAtLogin: openOptionsService.openAtLoginEnabled(),
        });
        argumentParser.setupYargs();
    }
    start() {
        networkSetup.start();
        argumentParser.parseArguments();
        this._initCommunicationServices();
        errorBox.replaceElectronAPI();
        this._bindApplicationEventHandlers(this.events);
        this._addLegacyAPI();
    }
    _initCommunicationServices() {
        if (this.features.REST) {
            this.restService = new HubRestService();
        }
        if (this.features.IPC) {
            this.ipcService = new HubIPCService();
        }
    }
    _createApplicationMenu() {
        if (process.platform === 'darwin') {
            applicationMenu.createApplicationMenu(this.unityHubLabel);
        }
        else {
            applicationMenu.createEmptyApplicationMenu();
        }
    }
    _bindApplicationEventHandlers(mapping) {
        if (!this.features.UI) {
            this.UI_RELATED_EVENTS.forEach(event => { delete mapping[event]; });
        }
        Object.keys(mapping).forEach((event) => {
            app.on(event, mapping[event].bind(this));
        });
    }
    _addLegacyAPI() {
        app.workOffline = () => {
            windowManager.signInWindow.close();
        };
    }
    initSequence() {
        return __awaiter(this, void 0, void 0, function* () {
            logger.debug('Electron main process is ready. Beginning init sequence..');
            i18nHelper.configure();
            systemInfo.logInfo();
            logger.debug(`Feature flags: ${JSON.stringify(this.features)}`);
            try {
                yield this.initializeServices();
                yield this.initializeApplication();
            }
            catch (error) {
                logger.error(`Failed to start Unity Hub, reason: ${error}`);
                if (error.stack !== undefined) {
                    logger.error(error.stack);
                }
                dialog.showErrorBox(error.message, error.stack);
            }
        });
    }
    initializeServices() {
        return __awaiter(this, void 0, void 0, function* () {
            logger.debug('Initializing services..');
            yield localSettings.init();
            yield cloudConfig.init();
            yield localConfig.init();
            yield tokenManager.init();
            yield localDownload.init();
            yield editorManager.init();
            yield EditorApp.init();
            analyticsQueue.init();
            yield windowManager.init();
            yield auth.init();
            yield licenseCore.reset();
            yield licenseClient.init();
            yield learnContent.init();
            yield unityPackageManagerService.init();
            if (this.features.AUTO_UPDATE) {
                yield autoUpdater.init();
            }
            if (this.features.REST) {
                yield this.restService.start();
            }
            if (this.features.IPC) {
                yield this.ipcService.start();
            }
            yield localProject.init();
            yield cloudCollab.init();
            yield cloudAnalytics.init(this.appStartTime, this.features.HEADLESS_MODE);
            yield pluginManagerLight.init();
            yield livePushService.init(windowManager, licenseClient);
            yield splashScreenService.init(auth);
            yield plasticSCMService.init(localProject);
            logger.debug('Done initializing services');
        });
    }
    initializeApplication() {
        return __awaiter(this, void 0, void 0, function* () {
            logger.debug('Initializing application..');
            networkInterceptors.init();
            if (this.features.UI) {
                yield this.initializeUI();
            }
            logger.debug('Application ready');
            this.emit('ready');
            if (this.features.HUB_PROTOCOL) {
                unityHubProtocolHandler.init();
            }
        });
    }
    initializeUI() {
        return __awaiter(this, void 0, void 0, function* () {
            logger.debug('Initializing UI..');
            trayMenu.init();
            this._createApplicationMenu();
            const { window, page } = yield applicationRouteHelper.getInitialRoute();
            logger.debug('Done initializing UI');
            if (!app.argv.silent) {
                const user = yield auth.getUserInfo();
                windowManager.mainWindow.setTitle();
                windowManager.showProperWindow(JSON.parse(user));
                logger.debug(`Showing ${window} with page: ${page}`);
            }
            else if (app.argv._ && app.argv._.length > 0) {
                logger.info('editor Args', app.argv._);
                EditorApp.editorArgs = app.argv._;
            }
        });
    }
    restoreLicenseFile() {
        return __awaiter(this, void 0, void 0, function* () {
            const result = yield licenseCore.verifyLicense();
            if (result !== true) {
                licenseCore.restoreBackupLicenseFile();
            }
        });
    }
    showProperWindow() {
        return __awaiter(this, void 0, void 0, function* () {
            const onboardingRoute = yield applicationRouteHelper.getOnboardingRoute();
            if (onboardingRoute) {
                windowManager.onboardingWindow.show();
            }
            else {
                const user = yield auth.getUserInfo();
                windowManager.showProperWindow(JSON.parse(user));
            }
        });
    }
    quitGracefully(event) {
        return __awaiter(this, void 0, void 0, function* () {
            event.preventDefault();
            if (editorManager && editorManager.isJobInProgress()) {
                this.promptQuitDialog();
                return;
            }
            if (onboardingService.hasOnboarding === false) {
                this.quitApp();
                return;
            }
            try {
                const state = yield onboardingService.getOnboardingState();
                const onboardingInProgress = state && !state.isFinished;
                if (onboardingInProgress) {
                    this.promptQuitDialog();
                }
                else {
                    this.quitApp();
                }
            }
            catch (error) {
                this.quitApp();
            }
        });
    }
    promptQuitDialog() {
        if (!this.features.UI) {
            return;
        }
        dialog.showMessageBox({
            type: 'error',
            title: i18nHelper.i18n.translate('QUIT_WINDOW.WARNING'),
            message: i18nHelper.i18n.translate('QUIT_WINDOW.QUIT_INSTALL'),
            buttons: [i18nHelper.i18n.translate('QUIT_WINDOW.CANCEL'), i18nHelper.i18n.translate('QUIT_WINDOW.QUIT_ANYWAY')]
        }, (buttonIndex) => {
            if (buttonIndex === 1) {
                this.quitApp();
            }
        });
    }
    quitApp() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.restService) {
                this.restService.stop();
            }
            if (this.ipcService) {
                this.ipcService.stop();
            }
            editorManager.closeInstallService();
            try {
                yield cloudAnalytics.quitEvent('Shortcut');
            }
            catch (error) {
            }
            this._endProcess();
        });
    }
    _endProcess() {
        this.isQuitting = true;
        process.kill(process.pid, 'SIGINT');
    }
    setupHubProtocolHandler() {
        if (!this.features.HUB_PROTOCOL) {
            return;
        }
        app.on('open-url', (e, url) => {
            logger.info(`Trying to open url ${url}`);
            if (url.includes(unityHubProtocolHandler.fullProtocolName)) {
                unityHubProtocolHandler.handleUrl(url);
            }
        });
        logger.info('Checking arguments for Windows custom URI scheme');
        let foundURIArgument = false;
        process.argv.forEach((arg) => {
            if (arg.includes(unityHubProtocolHandler.fullProtocolName)) {
                logger.info(`Found custom URI argument: ${arg}`);
                unityHubProtocolHandler.handleUrl(arg);
                foundURIArgument = true;
            }
        });
        if (!foundURIArgument) {
            logger.info('No custom URI argument found');
        }
    }
    handleLogin(event, webContents, details, authInfo, callback) {
        if (!proxyHelper.auth) {
            return;
        }
        event.preventDefault();
        const { username, password } = proxyHelper.auth;
        callback(username, password);
    }
    handleRemoteWebContent(_event, contents) {
        contents.on('will-attach-webview', (__event, webPreferences) => {
            logger.debug('Disabling node intergration for newly attached webview');
            webPreferences.nodeIntegration = false;
        });
        contents.on('new-window', (event) => __awaiter(this, void 0, void 0, function* () {
            logger.debug('Preventing new window from opening');
            event.preventDefault();
        }));
    }
}
module.exports = new App();
//# sourceMappingURL=app.js.map