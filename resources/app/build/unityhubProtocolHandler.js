const releaseService = require('./services/editorManager/unityReleaseService');
const windowManager = require('./windowManager/windowManager');
const logger = require('./logger')('UnityHubProtocolHandler');
class UnityHubProtocolHandler {
    get protocolName() { return 'unityhub'; }
    get fullProtocolName() { return `${this.protocolName}://`; }
    constructor() {
        this.initialized = false;
    }
    init() {
        this.initialized = true;
        if (this.url) {
            this.handleUrl(this.url);
        }
    }
    handleUrl(url) {
        if (!this.initialized) {
            this._handleUrlOnInit(url);
            return Promise.resolve();
        }
        const { version, revisionHash, moduleId } = this._getVersionInfoFromUrl(url);
        if (moduleId) {
            try {
                logger.info(`Doing a custom install of the module ${moduleId} on the editor version ${version} at revision ${revisionHash}`);
                windowManager.mainWindow.show(windowManager.mainWindow.pages.INSTALLS, `custom=true&module=${moduleId}&version=${version}`);
            }
            catch (e) {
                logger.warn(`Error while trying to install a custom module: ${e} ${e.stack}`);
                logger.info('Reverting to installs page');
                windowManager.mainWindow.show(windowManager.mainWindow.pages.INSTALLS);
            }
            return Promise.resolve();
        }
        else {
            logger.info(`Doing a custom install of ${version} at revision ${revisionHash}`);
            return releaseService.setCustomEditorInfo(version, revisionHash)
                .then(() => {
                logger.info('Loading custom install page');
                windowManager.mainWindow.show(windowManager.mainWindow.pages.INSTALLS, 'custom=true');
            })
                .catch((e) => {
                logger.warn(`Error while trying to install a custom editor: ${e} ${e.stack}`);
                logger.info('Reverting to installs page');
                windowManager.mainWindow.show(windowManager.mainWindow.pages.INSTALLS);
            });
        }
    }
    _handleUrlOnInit(url) {
        this.url = url;
    }
    _getVersionInfoFromUrl(url) {
        const protocolSegment = this.fullProtocolName;
        const [version, revisionHash, request] = url.substring(protocolSegment.length, url.length).split('/');
        if (request) {
            const moduleId = request.split('=')[1];
            return { version, revisionHash, moduleId };
        }
        return { version, revisionHash };
    }
}
module.exports = new UnityHubProtocolHandler();
//# sourceMappingURL=unityhubProtocolHandler.js.map