"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const chai_1 = __importDefault(require("chai"));
const sinon_chai_1 = __importDefault(require("sinon-chai"));
const proxyquire_1 = __importDefault(require("proxyquire"));
const windowManager_1 = __importDefault(require("./windowManager/windowManager"));
const unityhubProtocolHandler_1 = __importDefault(require("./unityhubProtocolHandler"));
const cloudAnalytics_1 = __importDefault(require("./services/cloudAnalytics/cloudAnalytics"));
const baseBootstrap_1 = __importDefault(require("./bootstrapSequences/baseBootstrap"));
const fileAssociationBootstrap_1 = __importDefault(require("./bootstrapSequences/fileAssociationBootstrap"));
const installServiceBootstrap_1 = __importDefault(require("./bootstrapSequences/installServiceBootstrap"));
const secondAppInstanceBootstrap_1 = __importDefault(require("./bootstrapSequences/secondAppInstanceBootstrap"));
const bootstrap_1 = __importDefault(require("./bootstrap"));
const bugReporterBootstrap_1 = __importDefault(require("./bootstrapSequences/bugReporterBootstrap"));
const cliBootstrap_1 = __importDefault(require("./bootstrapSequences/cliBootstrap"));
chai_1.default.use(sinon_chai_1.default);
const { expect } = chai_1.default;
describe('bootstrap.js', () => {
    let sandbox;
    let bootstrap;
    let yargs;
    let os;
    let electronApp;
    let isSecondAppInstance;
    let firstAppInstanceCallback;
    beforeEach(() => {
        sandbox = sinon_1.default.sandbox.create();
        isSecondAppInstance = false;
        electronApp = {
            requestSingleInstanceLock: sandbox.stub().callsFake(() => !isSecondAppInstance),
            on: sandbox.stub().callsFake((eventName, callback) => {
                if (eventName === 'second-instance') {
                    firstAppInstanceCallback = callback;
                }
            }),
        };
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('by default', () => {
        it('should set isSecondAppInstance properly', () => {
            bootstrap = new bootstrap_1.default(electronApp);
            expect(bootstrap.isSecondAppInstance).to.equal(isSecondAppInstance);
        });
    });
    describe('when second app instance starts', () => {
        beforeEach(() => {
            bootstrap = new bootstrap_1.default(electronApp);
            sandbox.stub(windowManager_1.default.mainWindow, 'show');
            firstAppInstanceCallback();
        });
        it('should display the onboardingWindow', () => {
            expect(windowManager_1.default.mainWindow.show).to.have.been.called;
        });
    });
    describe('start', () => {
        beforeEach(() => {
            sandbox.stub(baseBootstrap_1.default, 'start');
            sandbox.stub(installServiceBootstrap_1.default, 'start');
            sandbox.stub(fileAssociationBootstrap_1.default, 'start');
            sandbox.stub(bugReporterBootstrap_1.default, 'start');
            sandbox.stub(cliBootstrap_1.default, 'start');
            sandbox.stub(secondAppInstanceBootstrap_1.default, 'start');
        });
        describe('when argument --winInstaller is passed', () => {
            beforeEach(() => {
                yargs = { argv: { winInstaller: true } };
            });
            describe('on windows', () => {
                beforeEach(() => {
                    os = {
                        platform: () => 'win32',
                    };
                });
                it('should call InstallServiceBootstrap sequence', () => {
                    bootstrap = new (proxyquire_1.default('./bootstrap', {
                        yargs,
                        os,
                    }).default)(electronApp);
                    bootstrap.start();
                    expect(installServiceBootstrap_1.default.start).to.have.been.called;
                });
            });
            describe('on mac', () => {
                beforeEach(() => {
                    os = {
                        platform: () => 'darwin',
                    };
                });
                it('should call BaseBootstrap sequence', () => {
                    bootstrap = new (proxyquire_1.default('./bootstrap', {
                        yargs,
                        os,
                    }).default)(electronApp);
                    bootstrap.start();
                    expect(baseBootstrap_1.default.start).to.have.been.called;
                });
            });
        });
        describe('when application is a second instance', () => {
            beforeEach(() => {
                bootstrap = new bootstrap_1.default(electronApp);
                bootstrap.isSecondAppInstance = true;
                sandbox.stub(cloudAnalytics_1.default, 'quitEvent').resolves();
            });
            describe('by default', () => {
                beforeEach(() => bootstrap.start());
                it('should send an analytics event', () => {
                    expect(cloudAnalytics_1.default.quitEvent).to.have.been.called;
                });
                it('should call SecondAppInstanceBootstrap sequence', () => {
                    expect(secondAppInstanceBootstrap_1.default.start).to.have.been.called;
                });
            });
            describe('and was launched with a custom uri scheme passed as an argument', () => {
                let customUriArgument;
                beforeEach(() => {
                    customUriArgument = `${unityhubProtocolHandler_1.default.fullProtocolName}foo/bar`;
                    process.argv = [customUriArgument];
                    return bootstrap.start();
                });
                it('should call FileAssociationBootstrap sequence', () => {
                    expect(fileAssociationBootstrap_1.default.start).to.have.been.called;
                });
            });
        });
        describe('bootstrapping with --bugReporter', () => {
            beforeEach(() => {
                bootstrap = new (proxyquire_1.default('./bootstrap', {
                    yargs: { argv: { bugReporter: true } },
                }).default)(electronApp);
                bootstrap.start();
            });
            it('should call BugReporterBootStrap sequence', () => {
                expect(bugReporterBootstrap_1.default.start).to.have.been.called;
            });
        });
        describe('bootstrapping with --headless', () => {
            beforeEach(() => {
                bootstrap = new (proxyquire_1.default('./bootstrap', {
                    yargs: { argv: { headless: true } },
                }).default)(electronApp);
                bootstrap.start();
            });
            it('should call cliBootStrap sequence', () => {
                expect(cliBootstrap_1.default.start).to.have.been.called;
            });
        });
        describe('bootstrapping with nothing', () => {
            beforeEach(() => {
                bootstrap = new (proxyquire_1.default('./bootstrap', {
                    yargs: { argv: {} },
                }).default)(electronApp);
                process.argv = [];
                bootstrap.start();
            });
            it('should call BaseBootstrap sequence', () => {
                expect(baseBootstrap_1.default.start).to.have.been.called;
            });
        });
    });
});
//# sourceMappingURL=bootstrap.spec.js.map