const logger = require('./logger')('ProxyHelper');
const PROXY_ENV_VARS = {
    http: 'HTTP_PROXY',
    https: 'HTTPS_PROXY'
};
let environment;
class ProxyHelper {
    get http() { return environment.http; }
    get https() { return environment.https; }
    get auth() { return environment.auth; }
    constructor() {
        environment = {};
    }
    parseEnvironmentVariables() {
        environment = {};
        this._setupProtocol('http');
        this._setupProtocol('https');
    }
    _setupProtocol(protocol) {
        const string = process.env[PROXY_ENV_VARS[protocol]];
        if (!string) {
            return;
        }
        const url = this._parseUrl(string);
        if (!url) {
            logger.info(`URL for ${protocol} proxy is invalid.`);
            return;
        }
        this._setupAuth(protocol, url);
        const stringWithoutAuth = `${url.protocol}//${url.host}`;
        environment[protocol] = { string, stringWithoutAuth, url };
        logger.info(`Proxy configurations found for protocol ${protocol}: ${string}`);
    }
    _parseUrl(url) {
        try {
            return new URL(url);
        }
        catch (error) {
            logger.info('Could not parse url.');
            return undefined;
        }
    }
    _setupAuth(protocol, urlObject) {
        if (this.auth) {
            return;
        }
        const { username, password } = urlObject;
        if (username && password) {
            environment.auth = { username, password };
            logger.info(`Using authentication information from ${protocol} proxy environment variable`);
        }
    }
}
module.exports = new ProxyHelper();
//# sourceMappingURL=proxyHelper.js.map