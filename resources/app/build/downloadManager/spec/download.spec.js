const downloadManager = require('../downloadManager');
const Download = require('../download');
const { MT_FILE_DL_STATUS } = require('../../services/localDownload/lib/constants');
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
    .use(require('sinon-chai'))
    .use(require('chai-as-promised'));
describe('Download', () => {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('new download', () => {
        it('should return a download object', () => {
            const dl = new Download('url', 'filePath');
            expect(typeof dl).to.equal('object');
            expect(dl.url).to.equal('url');
            expect(dl.filePath).to.equal('filePath');
            expect(dl.status).to.equal(MT_FILE_DL_STATUS.NOT_STARTED);
        });
    });
    describe('start', () => {
        it('should tell the manager about it', () => {
            sandbox.stub(downloadManager, 'startDownload');
            const dl = new Download('url', 'filePath', downloadManager);
            sandbox.stub(dl, 'emit');
            dl.start();
            expect(dl.status).to.equal(MT_FILE_DL_STATUS.PENDING);
            expect(downloadManager.startDownload).to.have.been.calledOnce;
            expect(dl.emit).to.have.been.calledWith('start');
        });
    });
    describe('stop', () => {
        it('should tell the manager about it', () => {
            sandbox.stub(downloadManager, 'stopDownload');
            const dl = new Download('url', 'filePath', downloadManager);
            sandbox.stub(dl, 'emit');
            dl.stop();
            expect(dl.status).to.equal(MT_FILE_DL_STATUS.STOPPED);
            expect(downloadManager.stopDownload).to.have.been.calledOnce;
            expect(dl.emit).to.have.been.calledWith('stop');
        });
    });
    describe('destroy', () => {
        it('should tell the manager about it', () => {
            sandbox.stub(downloadManager, 'stopDownload');
            sandbox.stub(downloadManager, 'removeDownload');
            const dl = new Download('url', 'filePath', downloadManager);
            sandbox.stub(dl, 'emit');
            dl.destroy();
            expect(dl.status).to.equal(MT_FILE_DL_STATUS.DESTROYED);
            expect(downloadManager.stopDownload).to.have.been.calledOnce;
            expect(downloadManager.removeDownload).to.have.been.calledOnce;
            expect(dl.emit).to.have.been.calledWith('destroy');
        });
    });
    describe('ended', () => {
        it('should tell the world about it', () => {
            const dl = new Download('url', 'filePath', downloadManager);
            sandbox.stub(dl, 'emit');
            dl.ended();
            expect(dl.status).to.equal(MT_FILE_DL_STATUS.FINISHED);
            expect(dl.emit).to.have.been.calledWith('end');
        });
    });
    describe('isCancelled', () => {
        it('should be true when the status is stopped', () => {
            const dl = new Download('url', 'filePath', downloadManager);
            dl.status = MT_FILE_DL_STATUS.STOPPED;
            expect(dl.isCancelled()).to.equal(true);
        });
        it('should be true when the status is destroyed', () => {
            const dl = new Download('url', 'filePath', downloadManager);
            dl.status = MT_FILE_DL_STATUS.DESTROYED;
            expect(dl.isCancelled()).to.equal(true);
        });
        it('should be false when the status is finished', () => {
            const dl = new Download('url', 'filePath', downloadManager);
            dl.status = MT_FILE_DL_STATUS.FINISHED;
            expect(dl.isCancelled()).to.equal(false);
        });
        it('should be false when the status is pending', () => {
            const dl = new Download('url', 'filePath', downloadManager);
            dl.status = MT_FILE_DL_STATUS.PENDING;
            expect(dl.isCancelled()).to.equal(false);
        });
        it('should be false when the status is not started', () => {
            const dl = new Download('url', 'filePath', downloadManager);
            dl.status = MT_FILE_DL_STATUS.NOT_STARTED;
            expect(dl.isCancelled()).to.equal(false);
        });
        it('should be false when the status is downloading', () => {
            const dl = new Download('url', 'filePath', downloadManager);
            dl.status = MT_FILE_DL_STATUS.DOWNLOADING;
            expect(dl.isCancelled()).to.equal(false);
        });
        it('should be false when the status is error', () => {
            const dl = new Download('url', 'filePath', downloadManager);
            dl.status = MT_FILE_DL_STATUS.ERROR;
            expect(dl.isCancelled()).to.equal(false);
        });
    });
    describe('errored', () => {
        it('should tell the world about it', () => {
            const dl = new Download('url', 'filePath', downloadManager);
            sandbox.stub(dl, 'emit');
            const e = 'error message';
            dl.errored(e);
            expect(dl.status).to.equal(MT_FILE_DL_STATUS.ERROR);
            expect(dl.error).to.equal(e);
            expect(dl.emit).to.have.been.calledWith('error', dl, e);
        });
    });
    describe('resume', () => {
        it('should tell the manager about it', () => {
            sandbox.stub(downloadManager, 'resumeDownload');
            const dl = new Download('url', 'filePath', downloadManager);
            sandbox.stub(dl, 'emit');
            dl.resume();
            expect(dl.status).to.equal(MT_FILE_DL_STATUS.PENDING);
            expect(downloadManager.resumeDownload).to.have.been.calledOnce;
            expect(dl.emit).to.have.been.calledWith('resume');
        });
    });
    describe('getStats', () => {
        it('should return the stats', () => {
            const dl = new Download('url', 'filePath', downloadManager);
            expect(dl.getStats()).to.eql({
                total: { size: 0, downloaded: 0, completed: 0 }
            });
        });
    });
    describe('setStats', () => {
        it('should set the stats', () => {
            const dl = new Download('url', 'filePath', downloadManager);
            dl.setStats({
                total: { size: 1, downloaded: 1, completed: 1 }
            });
            expect(dl.stats).to.eql({
                total: { size: 1, downloaded: 1, completed: 1 }
            });
        });
    });
});
//# sourceMappingURL=download.spec.js.map