const Download = require('./download');
const logger = require('../logger')('DownloadManager');
const DownloadRequest = require('./downloadRequest');
const { fs } = require('../fileSystem');
class DownloadManager {
    constructor() {
        this.downloadsPending = [];
        this.downloadsProcessing = [];
        this.maxConcurrentDownloads = 10;
    }
    download(url, filePath) {
        const download = new Download(url, filePath, this);
        return download;
    }
    startDownload(download) {
        this.downloadsPending.push(download);
        this.processPendingDownloads();
    }
    resumeDownload() {
        logger.info('Resume not implemented yet');
    }
    stopDownload(download) {
        if (download && download.request) {
            download.request.abort();
        }
    }
    removeDownload(download) {
        if (fs.existsSync(download.filePath)) {
            fs.unlink(download.filePath);
        }
    }
    processPendingDownloads() {
        if (!thereAreValidDownloadsPending.bind(this)()) {
            return;
        }
        if (this.downloadsProcessing.length >= this.maxConcurrentDownloads) {
            return;
        }
        while (this.downloadsProcessing.length < this.maxConcurrentDownloads && this.downloadsPending.length > 0) {
            const download = this.downloadsPending.shift();
            if (!download.isCancelled()) {
                this.downloadsProcessing.push(download);
                DownloadRequest.downloadFile(download)
                    .then(() => {
                    download.ended();
                    logger.info(`download ended with status: ${download.status} ${download.filePath}`);
                })
                    .catch((e) => {
                    download.errored(e);
                    logger.warn(`Could not download from ${download.url} to ${download.filePath} ${e} ${e.stack}`);
                })
                    .then(() => {
                    removeDownloadFromProcessing.bind(this)(download);
                    this.processPendingDownloads();
                });
            }
        }
    }
}
function thereAreValidDownloadsPending() {
    if (this.downloadsPending.length === 0) {
        return false;
    }
    for (let i = 0; i < this.downloadsPending.length; i++) {
        if (!this.downloadsPending[i].isCancelled()) {
            return true;
        }
    }
    return false;
}
function removeDownloadFromProcessing(download) {
    let i = this.downloadsProcessing.length;
    while (i > 0) {
        if (this.downloadsProcessing[i] === download) {
            this.downloadsProcessing.splice(i, 1);
        }
        i -= 1;
    }
}
module.exports = new DownloadManager();
//# sourceMappingURL=downloadManager.js.map