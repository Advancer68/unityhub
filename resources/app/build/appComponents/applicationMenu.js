const { app, Menu, systemPreferences, shell } = require('electron');
const hubBugReporter = require('../services/hubBugReporter/bug-reporter');
const i18nHelper = require('../i18nHelper');
class ApplicationMenu {
    createApplicationMenu(unityHubLabel) {
        const template = [
            {
                label: app.getName(),
                submenu: [
                    {
                        label: `${i18nHelper.i18n.translate('APP.ABOUT')} ${unityHubLabel}`,
                        role: 'about'
                    }, {
                        type: 'separator'
                    }, {
                        label: `${i18nHelper.i18n.translate('APP.HIDE')} ${unityHubLabel}`,
                        role: 'hide'
                    }, {
                        label: i18nHelper.i18n.translate('APP.HIDEOTHERS'),
                        role: 'hideothers'
                    }, {
                        type: 'separator'
                    }, {
                        label: `${i18nHelper.i18n.translate('APP.QUIT')} ${unityHubLabel}`,
                        role: 'quit'
                    }
                ],
            },
            {
                label: i18nHelper.i18n.translate('EDIT.EDIT'),
                submenu: [
                    {
                        label: i18nHelper.i18n.translate('EDIT.UNDO'),
                        role: 'undo'
                    }, {
                        label: i18nHelper.i18n.translate('EDIT.REDO'),
                        role: 'redo'
                    }, {
                        type: 'separator'
                    }, {
                        label: i18nHelper.i18n.translate('EDIT.CUT'),
                        role: 'cut'
                    }, { label: i18nHelper.i18n.translate('EDIT.COPY'),
                        role: 'copy'
                    }, { label: i18nHelper.i18n.translate('EDIT.PASTE'),
                        role: 'paste'
                    }, {
                        label: i18nHelper.i18n.translate('EDIT.PASTEANDMATCHSTYLE'),
                        role: 'pasteandmatchstyle'
                    }, {
                        label: i18nHelper.i18n.translate('EDIT.DELETE'),
                        role: 'delete'
                    }, {
                        label: i18nHelper.i18n.translate('EDIT.SELECTALL'),
                        role: 'selectall'
                    }
                ],
            },
            {
                label: i18nHelper.i18n.translate('MENUWINDOW.MENUWINDOW'),
                role: 'window',
                submenu: [
                    {
                        label: i18nHelper.i18n.translate('MENUWINDOW.MINIMIZE'),
                        role: 'minimize'
                    }, {
                        label: i18nHelper.i18n.translate('MENUWINDOW.CLOSE'),
                        role: 'close'
                    }, {
                        label: i18nHelper.i18n.translate('MENUWINDOW.ZOOM'),
                        role: 'zoom'
                    }, {
                        type: 'separator'
                    }, {
                        label: i18nHelper.i18n.translate('MENUWINDOW.FRONT'),
                        role: 'front'
                    }
                ],
            },
            {
                label: i18nHelper.i18n.translate('HELP.HELP'),
                role: 'help',
                submenu: [
                    {
                        label: i18nHelper.i18n.translate('HELP.SEEMANUAL'),
                        click() { shell.openExternal('https://docs.unity.cn/cn/2019.4/Manual/GettingStartedUnityHub.html'); }
                    },
                    {
                        label: i18nHelper.i18n.translate('HELP.REPORTABUG'),
                        click() { hubBugReporter.showBugReporter(); }
                    }
                ],
            },
        ];
        const menu = Menu.buildFromTemplate(template);
        systemPreferences.setUserDefault('NSDisabledDictationMenuItem', 'boolean', true);
        systemPreferences.setUserDefault('NSDisabledCharacterPaletteMenuItem', 'boolean', true);
        Menu.setApplicationMenu(menu);
    }
    createEmptyApplicationMenu() {
        Menu.setApplicationMenu(null);
    }
}
module.exports = new ApplicationMenu();
//# sourceMappingURL=applicationMenu.js.map