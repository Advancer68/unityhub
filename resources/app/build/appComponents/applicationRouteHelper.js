var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const logger = require('../logger')('ApplicationRouteHelper');
const onboardingService = require('../services/onboarding/onboardingService');
const localSettings = require('../services/localSettings/localSettings');
const licenseClient = require('../services/licenseService/licenseClientProxy');
const localProject = require('../services/localProject/localProject');
const windowManager = require('../windowManager/windowManager');
const localConfig = require('../services/localConfig/localConfig');
const editorManager = require('../services/editorManager/editorManager');
const communityService = require('../services/community/communityService');
class ApplicationRouteHelper {
    getInitialRoute() {
        return __awaiter(this, void 0, void 0, function* () {
            const onboardingInitialRoute = yield this.getOnboardingRoute();
            if (onboardingInitialRoute !== null) {
                return { window: 'onboardingWindow', page: onboardingInitialRoute };
            }
            const route = yield this.getMainRoute();
            return { window: 'mainWindow', page: route };
        });
    }
    getOnboardingRoute() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!onboardingService.hasOnboarding) {
                logger.info('hadOnboarding is false');
                return null;
            }
            const onboardingState = yield onboardingService.getOnboardingState();
            if (onboardingState.isFinished) {
                logger.info('onboarding flow is finished');
                return null;
            }
            return onboardingState.url;
        });
    }
    getMainRoute() {
        logger.info('Getting initial route for Main Window');
        return new Promise((resolve) => {
            if (licenseClient.isLicenseValid() === false) {
                if (localSettings.get(localSettings.keys.ENABLE_ENTITLEMENT_LICENSING) === true) {
                    logger.info(`${localSettings.keys.ENABLE_ENTITLEMENT_LICENSING} setting is set to true but the license is invalid. ` +
                        'The client might be unreachable or the user does not have the entitlement to start the editor');
                    resolve(windowManager.mainWindow.pages.RECENT_PROJECT);
                }
                else {
                    resolve(windowManager.mainWindow.pages.LICENSE_MANAGEMENT);
                }
            }
            else {
                localProject.getRecentProjects(true).then(projects => {
                    if (Object.keys(editorManager.availableEditors).length === 0) {
                        logger.info('Loading installs page');
                        resolve(windowManager.mainWindow.pages.INSTALLS);
                    }
                    else if (projects.length === 0 && !localConfig.isLearnDisabled()) {
                        logger.info('Loading learn page');
                        resolve(windowManager.mainWindow.pages.LEARN);
                    }
                    else {
                        logger.info('Loading default page');
                        if (communityService.getShowCommunity()) {
                            resolve(windowManager.mainWindow.pages.COMMUNITY);
                        }
                        else {
                            resolve(windowManager.mainWindow.pages.RECENT_PROJECTS);
                        }
                    }
                });
            }
        }).catch((e) => {
            logger.error('Failed to load initial route');
            throw e;
        });
    }
}
module.exports = new ApplicationRouteHelper();
//# sourceMappingURL=applicationRouteHelper.js.map