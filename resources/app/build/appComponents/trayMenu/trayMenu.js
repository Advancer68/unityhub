var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const path = require('path');
const os = require('os');
const { app, Tray, Menu } = require('electron');
const localProject = require('../../services/localProject/localProject');
const onboardingService = require('../../services/onboarding/onboardingService');
const windowManager = require('../../windowManager/windowManager');
const TrayMenuBuilder = require('./trayMenuBuilder');
const trayMenuData = require('./trayMenuData');
const logger = require('../../logger')('TrayMenu');
const postal = require('postal');
const auth = require('../../services/localAuth/auth');
const RECENT_PROJECTS_LOOKUP_INTERVAL = 60000;
class TrayMenu {
    init() {
        logger.info('Init');
        this.createTray();
        const debugMode = app.argv.debug === true;
        this.trayMenuBuilder = new TrayMenuBuilder(debugMode, trayMenuData);
        if (os.platform() === 'linux') {
            this.buildTray();
            setTimeout(() => this.buildTray(), RECENT_PROJECTS_LOOKUP_INTERVAL);
            postal.subscribe({
                channel: 'app',
                topic: 'connectInfo.changed',
                callback: () => {
                    this.buildTray();
                }
            });
            postal.subscribe({
                channel: 'app',
                topic: 'userInfo.changed',
                callback: () => {
                    this.buildTray();
                }
            });
            postal.subscribe({
                channel: 'app',
                topic: 'license.change',
                callback: () => {
                    this.buildTray();
                }
            });
        }
    }
    createTray() {
        if (app.tray) {
            return;
        }
        const trayIconImage = (os.platform() !== 'darwin') ? 'images/tray/trayIconWindows.png' : 'images/tray/trayIconTemplate.png';
        const iconPath = process.defaultApp ? path.resolve(trayIconImage) : path.join(app.getAppPath(), trayIconImage);
        app.tray = new Tray(iconPath);
        app.tray.on('click', () => {
            if (os.platform() === 'win32') {
                this.onWindowsTrayClicked();
            }
            else {
                this.openTray();
            }
        });
        app.tray.on('right-click', () => this.openTray());
    }
    onWindowsTrayClicked() {
        return __awaiter(this, void 0, void 0, function* () {
            if (windowManager.mainWindow.isVisible()) {
                windowManager.mainWindow.show();
                return;
            }
            if (windowManager.splashWindow.isVisible()) {
                windowManager.splashWindow.show();
                return;
            }
            const user = yield auth.getUserInfo();
            windowManager.showProperWindow(JSON.parse(user));
        });
    }
    buildTray(popUpContext = false) {
        return __awaiter(this, void 0, void 0, function* () {
            const [recentProjects, onboardingState] = yield Promise.all([this._getRecentProjects(), this._getOnboardingState()]);
            app.trayMenu = Menu.buildFromTemplate(this.trayMenuBuilder.buildTrayMenuTemplate(recentProjects, onboardingState));
            if (os.platform() === 'linux') {
                app.tray.setContextMenu(app.trayMenu);
            }
            if (popUpContext) {
                app.tray.popUpContextMenu(app.trayMenu);
            }
        });
    }
    openTray() {
        return this.buildTray(true);
    }
    _getRecentProjects() {
        return __awaiter(this, void 0, void 0, function* () {
            const recentProjects = yield localProject.getProjectsFromMemory();
            return recentProjects.sort(this._reverseSortProjectsByLastModifiedDate);
        });
    }
    _reverseSortProjectsByLastModifiedDate(firstProject, secondProject) {
        return secondProject.lastModified - firstProject.lastModified;
    }
    _getOnboardingState() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!onboardingService.hasOnboarding)
                return undefined;
            return yield onboardingService.getOnboardingState();
        });
    }
}
module.exports = new TrayMenu();
//# sourceMappingURL=trayMenu.js.map