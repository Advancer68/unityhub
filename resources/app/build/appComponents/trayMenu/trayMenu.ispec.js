'use strict';
const localProject = require('../../services/localProject/localProject');
const auth = require('../../services/localAuth/auth');
const licenseClient = require('../../services/licenseService/licenseClientProxy');
const autoUpdater = require('../../services/hubAutoUpdate/auto-updater');
const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const { app } = require('electron/electron');
const trayMenu = require('./trayMenu');
describe('Tray Menu', function () {
    let sandbox;
    const recentProjects = [];
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        app.argv = { debug: false };
        trayMenu.init();
        sandbox.stub(app.tray, 'popUpContextMenu').resolves();
    });
    afterEach(() => {
        sandbox.restore();
    });
    it('should create a new icon for the Tray', () => {
        expect(app.tray).to.be.a('Tray');
    });
    describe('openTray', () => {
        beforeEach(() => {
            sandbox.stub(localProject, 'getRecentProjects').resolves(recentProjects);
            sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
            sandbox.stub(trayMenu.trayMenuBuilder, 'buildTrayMenuTemplate').returns([]);
            auth.connectInfo.loggedIn = false;
            auth.userInfo.displayName = 'anonymous';
            autoUpdater.updateAvailable = false;
            return trayMenu.openTray();
        });
        it('should create a new menu for the Tray', () => {
            expect(app.trayMenu).to.be.a('Menu');
        });
        it('should call popUpContextMenu', () => {
            expect(app.tray.popUpContextMenu).to.have.been.called;
        });
    });
});
//# sourceMappingURL=trayMenu.ispec.js.map