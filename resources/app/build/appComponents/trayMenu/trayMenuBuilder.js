const _ = require('lodash');
const auth = require('../../services/localAuth/auth');
const licenseClient = require('../../services/licenseService/licenseClientProxy');
const windowManager = require('../../windowManager/windowManager');
const localConfig = require('../../services/localConfig/localConfig');
const autoUpdater = require('../../services/hubAutoUpdate/auto-updater');
const communityService = require('../../services/community/communityService');
const { i18n } = require('../../i18nHelper');
class TrayMenuBuilder {
    constructor(debugMode, trayMenuData) {
        this.debugMode = debugMode;
        this.menuItems = trayMenuData.menuItems;
        this.menuData = trayMenuData.menuData;
    }
    buildTrayMenuTemplate(recentProjects, onboardingState) {
        const template = this._generateMenuArray(recentProjects, onboardingState);
        return this._createTemplateFromArrays(template);
    }
    _generateMenuArray(recentProjects, onboardingState) {
        const template = [];
        if (onboardingState && !onboardingState.isFinished) {
            return [this._buildOnboardingMenu()];
        }
        if (recentProjects.length > 0 && licenseClient.isLicenseValid()) {
            template.push(this._buildRecentProjectsMenu(recentProjects));
        }
        if (!localConfig.isSignInDisabled()) {
            template.push(this._buildUserMenu());
        }
        template.push(this._buildAppMenu());
        if (this.debugMode) {
            template.push(this._buildDevMenu());
        }
        return template;
    }
    _buildOnboardingMenu() {
        return [
            this._getMenuItem(this.menuItems.OPEN_HUB_ONBOARDING),
            this._getMenuItem(this.menuItems.QUIT),
        ];
    }
    _buildUserMenu() {
        if (auth.connectInfo.loggedIn === true) {
            return [
                this._getMenuItem(this.menuItems.GO_TO_DASHBOARD),
                this._getMenuItem(this.menuItems.SIGN_OUT)
            ];
        }
        return [
            this._getMenuItem(this.menuItems.SIGN_IN)
        ];
    }
    _buildAppMenu() {
        const menu = [];
        menu.push(this._getMenuItem(this.menuItems.TROUBLESHOOTING));
        if (licenseClient.isLicenseValid() === false && !licenseClient.isLicenseEntitlementEnabled()) {
            menu.push(this._getMenuItem(this.menuItems.ACTIVATE_LICENSE));
        }
        else {
            menu.push(this._getMenuItem(this.menuItems.OPEN_HUB));
        }
        if (communityService.getShowCommunity()) {
            menu.push(this._getMenuItem(this.menuItems.OPEN_COMMUNITY));
        }
        if (autoUpdater.updateAvailable === true) {
            menu.push(this._getMenuItem(this.menuItems.RESTART_AND_UPDATE));
            menu.push(this._getMenuItem(this.menuItems.QUIT_AND_UPDATE));
        }
        else {
            menu.push(this._getMenuItem(this.menuItems.QUIT));
        }
        return menu;
    }
    _buildDevMenu() {
        const menu = [
            this._getMenuItem(this.menuItems.RELOAD),
            this._getMenuItem(this.menuItems.DEVTOOLS)
        ];
        if (windowManager.signInWindow.isVisible()) {
            menu.push(this._getMenuItem(this.menuItems.DEVTOOLS_SIGN_IN));
        }
        if (windowManager.newProjectWindow.isVisible()) {
            menu.push(this._getMenuItem(this.menuItems.DEVTOOLS_NEW_PROJECT));
        }
        return menu;
    }
    _buildRecentProjectsMenu(recentProjects) {
        const menu = [];
        recentProjects.slice(0, 5).forEach((project) => {
            menu.push(this.menuData[this.menuItems.RECENT_PROJECT](project));
        });
        return menu;
    }
    _createTemplateFromArrays(arrays) {
        let template = [];
        const separator = [{ type: 'separator' }];
        if (arrays.length === 1) {
            template = arrays[0];
        }
        else {
            arrays.forEach((currArray, index) => {
                if (index > 0) {
                    template = template.concat(separator, currArray);
                }
                else {
                    template = currArray;
                }
            });
        }
        return template;
    }
    _getMenuItem(name) {
        let menuItem = this.menuData[name];
        if (_.isFunction(menuItem)) {
            menuItem = menuItem();
        }
        if (menuItem.label && menuItem.label.code) {
            menuItem.label = i18n.translate(menuItem.label.code, menuItem.label.params);
        }
        if (menuItem.submenu) {
            for (const option of menuItem.submenu) {
                if (option.label && option.label.code) {
                    option.label = i18n.translate(option.label.code, option.label.params);
                }
            }
        }
        return menuItem;
    }
}
module.exports = TrayMenuBuilder;
//# sourceMappingURL=trayMenuBuilder.js.map