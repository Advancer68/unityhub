const { app } = require('electron');
const yargs = require('yargs');
const logger = require('../logger')('ArgumentParser');
const onboardingService = require('../services/onboarding/onboardingService');
const communityService = require('../services/community/communityService');
const uprService = require('../services/upr/uprService');
const ucgService = require('../services/ucg/ucgService');
class ArgumentParser {
    setupYargs() {
        yargs.help('h')
            .alias('h', 'help');
        app.argv = yargs.argv;
    }
    parseArguments() {
        if (app.argv.debugMode) {
            app.argv.debug = true;
            logger.activateConsoleOutput();
        }
        if (app.argv.clearOnboardingFlags) {
            onboardingService.clearFlags();
        }
        if (app.argv.hasOnboarding) {
            onboardingService.setHasOnboarding(true);
        }
        if (app.argv.community) {
            communityService.setShowCommunity(true);
        }
        if (app.argv.upr) {
            uprService.setShowUPR(true);
        }
        if (app.argv.ucg) {
            ucgService.setShowUCG(true);
        }
    }
}
module.exports = new ArgumentParser();
//# sourceMappingURL=argumentParser.js.map