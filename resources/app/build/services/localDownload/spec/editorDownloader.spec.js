const setting = require('../../localSettings/localSettings');
const proxyquire = require('proxyquire');
const EditorDownloader = proxyquire('../lib/editorDownloader', {
    'remote-file-size': (url) => {
        if (url === 'bad-url')
            throw new Error();
        return Promise.resolve();
    }
});
const Downloader = require('../../../downloadManager/downloadManager');
const { HUB_DL_STATUS } = require('../lib/constants');
const { getFakeDownload } = require('./helper');
const { fs } = require('../../../fileSystem');
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(chaiAsPromised);
chai.use(sinonChai);
describe("EditorDownloader", () => {
    const version = '2077.3.0f1';
    let dl, sandbox, clock;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        sandbox.stub(Downloader, 'download').callsFake(getFakeDownload);
        sandbox.stub(fs, 'mkdir').callsFake((path, callback) => callback(undefined, path));
        EditorDownloader.createEditorDownloader({ id: version, downloadUrl: 'path/to/file' })
            .then((download) => {
            dl = download;
            sandbox.spy(dl, 'emit');
            clock = sandbox.useFakeTimers();
        });
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('constructor', () => {
        it('should throw an exception when editor param is invalid', () => {
            expect(() => new EditorDownloader()).to.throw(TypeError);
            expect(() => new EditorDownloader({ id: version })).to.throw(TypeError);
        });
    });
    describe('createEditorDownloader', () => {
        it('should reject with TypeError when no editor param is given', () => {
            return expect(EditorDownloader.createEditorDownloader()).to.be.rejectedWith(TypeError);
        });
        it('should reject with TypeError when editor params are missing', () => {
            return expect(EditorDownloader.createEditorDownloader({ id: version })).to.be.rejectedWith(TypeError);
        });
        it('should reject with TypeError when invalid editor param are given', () => {
            return expect(EditorDownloader.createEditorDownloader({ id: version, downloadUrl: 'invalid url' })).to.be.rejectedWith(TypeError);
        });
    });
    describe('destroy', () => {
        beforeEach(() => {
            sandbox.stub(Downloader, 'destroy');
        });
        it('should emit a \'destroy\' event with its id', () => {
            dl.destroy();
            expect(dl.emit).to.have.been.calledWith('destroy', version);
        });
        it('should change the status of the editorDownloader instance CANCELED', () => {
            dl.destroy();
            expect(dl.status).to.equal(HUB_DL_STATUS.CANCELED);
        });
    });
    describe('start', () => {
        beforeEach(() => {
            sandbox.stub(Downloader, 'start');
            sandbox.stub(setting, 'get').callsFake(() => {
                return 1000;
            });
        });
        it('should emit a \'start\' event with its id', () => {
            return dl.start().then(() => {
                expect(dl.emit).to.have.been.calledWith('start', version);
            });
        });
        it('should change the status of the EditorDownloader instance to STARTED', () => {
            return dl.start().then(() => {
                expect(dl.status).to.equal(HUB_DL_STATUS.STARTED);
            });
        });
        it('should start to track download progress', () => {
            dl.editorDownload.getStats.callsFake(() => { return { total: { downloaded: 100 } }; });
            sandbox.stub(dl.downloadsCluster, 'getStats').callsFake(() => { return { total: { downloaded: 50 } }; });
            return dl.start().then(() => {
                expect(dl.downloaded).equal(0);
                clock.tick(1000);
                expect(dl.downloaded).equal(150);
            });
        });
        it('should should destroy the cluster if a bug happened', () => {
            sandbox.stub(dl.downloadsCluster, 'destroy').callsFake(() => { return {}; });
            dl.editorDownload.url = 'bad-url';
            return dl.start().then(() => {
                expext('it should not resolve the promise').equal('but it did');
            }).catch(() => {
                expect(dl.downloadsCluster.destroy).to.have.been.called;
            });
        });
    });
    describe('getEditorInstallPath', () => {
        it('should returns the path of the editor installer', () => {
            expect(dl.getEditorInstallPath()).equal(dl.editorDownload.filePath);
        });
    });
    describe('getDownloadedModules', () => {
        it('should return modules downloads managed by the downloads cluster', () => {
            expect(dl.getDownloadedModules()).deep.eql(dl.downloadsCluster.getDownloadedModules());
        });
    });
    describe('getStats', () => {
        beforeEach(() => {
            dl.downloaded = 1000;
            dl.totalProgress = 0.20;
        });
        it('should returns the total of downloaded bytes', () => {
            expect(dl.getStats().total.downloaded).equal(1000);
        });
        it('should returns percentage of download total progress', () => {
            expect(dl.getStats().total.completed).equal(20);
        });
    });
});
//# sourceMappingURL=editorDownloader.spec.js.map