const sinon = require('sinon');
module.exports = {
    getFakeDownload() {
        return {
            start: sinon.stub(),
            on: sinon.stub(),
            getStats: sinon.stub(),
            destroy: sinon.stub(),
            filePath: 'downloadPath'
        };
    }
};
//# sourceMappingURL=helper.js.map