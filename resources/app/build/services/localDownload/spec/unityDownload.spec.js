var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const unityDownload = require('../unityDownload');
const editorDownloader = require('../lib/editorDownloader');
const downloadsCluster = require('../lib/downloadsCluster');
const { getFakeDownload } = require('./helper');
const { fs, hubFS } = require('../../../fileSystem');
const _ = require('lodash');
const proxyquire = require('proxyquire');
const path = require('path');
const storage = require('electron-json-storage');
const Downloader = require('../../../downloadManager/downloadManager');
const { editorHelper } = require('../../../common/common');
const windowManager = require('../../../windowManager/windowManager');
const cloudAnalytics = require('../../cloudAnalytics/cloudAnalytics');
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const specData = require('./specs.data.json');
chai.use(chaiAsPromised);
chai.use(sinonChai);
describe('UnityDownload', () => {
    let sandbox, downloadsData = {}, userDataPath = path.join('/', 'user', 'data', 'path'), unityUserDataPath = path.join(userDataPath, 'Unity'), resumeDownloadStub, downloadStub, wrongPath = path.join('a', 'wrong', 'path');
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        windowManager.broadcastContent = sandbox.stub();
        sandbox.stub(fs, 'mkdir').callsFake((path, callback) => callback(undefined, path));
        sandbox.stub(editorHelper, 'getDefaultAssetStoreFolderName').returns('assetStore');
        sandbox.stub(editorHelper, 'getUserDataPath').returns(unityUserDataPath);
        sandbox.stub(editorHelper, 'getBaseUserDataPath').returns(userDataPath);
        sandbox.stub(editorHelper, 'getUnityFolderInUserData').returns('Unity');
        sandbox.stub(cloudAnalytics, 'addEvent');
        sandbox.stub(storage, 'set');
        downloadStub = sandbox.stub(Downloader, 'download').callsFake((url, dlPath) => buildFakeDownload(dlPath));
        resumeDownloadStub = sandbox.stub(Downloader, 'resumeDownload').callsFake((dlPath) => {
            if (dlPath !== wrongPath) {
                return buildFakeDownload(dlPath);
            }
            else {
                let err = new Error();
                err.context = { error: { message: 'Invalid file path' } };
                throw err;
            }
        });
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('Init', () => {
        let unfinishedDownloads;
        beforeEach(() => {
            unfinishedDownloads = {
                '1': path.join('my', 'path', 'to', 'download'),
                '2': path.join('daolnwod', 'ot', 'htap', 'ym'),
                '3': path.join('ho', 'ho', 'hoo'),
                '4': wrongPath
            };
            sandbox.stub(path, 'dirname').returns('/some/path');
            sandbox.stub(storage, 'get').callsFake((key, callback) => {
                callback(undefined, Object.assign({}, unfinishedDownloads));
            });
            return unityDownload.init();
        });
        it('should resume the package downloads', () => {
            _.each(unfinishedDownloads, (path) => {
                expect(resumeDownloadStub).to.have.been.calledWith(path);
            });
        });
        it('should only store valid unfinished downloads', () => {
            let validUnfinishedDownloads = Object.assign({}, unfinishedDownloads);
            delete validUnfinishedDownloads['4'];
            expect(storage.set).to.have.been.calledWith('downloads', validUnfinishedDownloads);
        });
    });
    describe('downloadPackage', () => {
        beforeEach(() => {
            sandbox.stub(hubFS, 'getDiskSpaceAvailable').returns({ available: 100 });
        });
        it('should check the location of assets', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(fs, 'statSync');
            yield unityDownload.downloadPackage({ id: 'fakeId', url: 'fakeUrl', filename: 'fakeFileName' });
            expect(fs.statSync).to.have.been.calledWith(path.join(unityUserDataPath, 'assetStore'));
        }));
        it('should create the location of assets if it does not exists', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(fs, 'statSync').throws();
            sandbox.stub(fs, 'mkdirSync');
            yield unityDownload.downloadPackage({ id: 'fakeId', url: 'fakeUrl', filename: 'fakeFileName' });
            expect(fs.mkdirSync).to.have.been.calledWith(path.join(unityUserDataPath, 'assetStore'));
        }));
        it('should send an error event if it fails to get package size', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(unityDownload, '_getPackageSize').throws();
            sandbox.stub(fs, 'mkdirSync');
            yield unityDownload.downloadPackage({ id: 'fakeId', url: 'fakeUrl', filename: 'fakeFileName', title: 'fake package' });
            expect(windowManager.broadcastContent).to.have.been.calledWith('download.error', 'ERROR.CANNOT_GET_SIZE', {
                id: 'fakeId',
                i18n: { name: 'fake package' }
            });
        }));
        it('should send an error event if the package size is bigger than downloadPath or destinationPath', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(unityDownload, '_getPackageSize').returns(200);
            sandbox.stub(fs, 'mkdirSync');
            yield unityDownload.downloadPackage({ id: 'fakeId', url: 'fakeUrl', filename: 'fakeFileName', title: 'fake package' });
            expect(windowManager.broadcastContent).to.have.been.calledWith('download.error', 'ERROR.NOT_ENOUGH_SPACE_SPECIFIC', {
                i18n: { itemName: 'fake package' },
                id: 'fakeId'
            });
        }));
        it('should add to the in progress downloads and store it', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(unityDownload, '_getPackageSize').returns(11);
            sandbox.stub(fs, 'statSync');
            yield unityDownload.downloadPackage({ id: 'fakeId', url: 'fakeUrl', filename: 'fakeFileName' });
            expect(storage.set).to.have.been.calledWith(sinon.match('downloads'), sinon.match.has('fakeId'));
        }));
    });
    describe('downloadUnityEditor', () => {
        let startStub;
        beforeEach(() => {
            startStub = sandbox.stub(editorDownloader.prototype, 'start');
        });
        it('start downloading an editor', () => {
            return unityDownload.downloadUnityEditor({ id: 'v1', downloadUrl: specData.downloadUrl }, [])
                .then(() => expect(startStub).to.have.been.called);
        });
        it('should throw an error when the editor downloadUrl is missing', () => {
            return expect(unityDownload.downloadUnityEditor({ id: 'v1' }, [])).to.be.rejectedWith(TypeError);
        });
        it('should throw an error when the editor downloadUrl is invalid', () => {
            return expect(unityDownload.downloadUnityEditor({
                id: 'v1',
                downloadUrl: 'invalidUrl'
            }, [])).to.be.rejectedWith(TypeError);
        });
    });
    describe('downloadEditorModules', () => {
        it('start downloading an editor', () => {
            const stub = sandbox.stub(downloadsCluster.prototype, 'start');
            return unityDownload.downloadEditorModules({ id: 'version' }, [])
                .then(() => expect(stub).to.have.been.called);
        });
    });
    describe('copyUnityEditor', () => {
        let copyModulesStub;
        beforeEach(() => {
            copyModulesStub = sandbox.stub(unityDownload, 'copyModules').resolves();
        });
        it('start copying an editor', () => {
            let copyStub = sandbox.stub(fs, 'copy').resolves();
            return unityDownload.copyUnityEditor({ id: 'v1', downloadUrl: specData.copyPath }, [])
                .then(() => expect(copyStub).to.have.been.called)
                .then(() => expect(copyModulesStub).to.have.been.called);
        });
        it('should throw an error when the editor downloadUrl is missing', () => {
            expect(() => unityDownload.copyUnityEditor({ id: 'v1' }, [])).to.throw(TypeError);
        });
        it('should be rejected when the editor downloadUrl is invalid', () => {
            let copyStub = sandbox.stub(fs, 'copy').rejects();
            return expect(unityDownload.copyUnityEditor({ id: 'v1', downloadUrl: 'invalidUrl' }, [])).to.be.rejectedWith(Error);
        });
    });
    describe('copyEditorModules', () => {
        it('Copy valid modules to an editor', () => {
            sandbox.stub(fs, 'copy').resolves();
            return unityDownload.copyEditorModules({ id: 'v1' }, specData.validCopyModules, 'install/path')
                .then((result) => {
                expect(result.length).to.eq(2);
                expect(result[0]).to.have.all.keys('name', 'downloadUrl', 'installerPath');
                expect(result[1]).to.have.all.keys('name', 'downloadUrl', 'installerPath');
            });
        });
        it('Copy partially valid modules to an editor', () => {
            sandbox.stub(fs, 'copy')
                .onFirstCall().rejects()
                .onSecondCall().resolves();
            return unityDownload.copyEditorModules({ id: 'v1' }, specData.partialValidCopyModules, 'install/path')
                .then((result) => {
                expect(result.length).to.eq(1);
                expect(result[0]).to.have.all.keys('name', 'downloadUrl', 'installerPath');
            });
        });
        it('Try to copy all invalid modules to an editor', () => {
            sandbox.stub(fs, 'copy').rejects();
            return unityDownload.copyEditorModules({ id: 'v1' }, specData.invalidCopyModules, 'install/path')
                .then((result) => {
                expect(result.length).to.eq(0);
            });
        });
    });
    describe('cancelDownload', () => {
        const v1 = "v1", v2 = "v2";
        let dlClusterStub, dlEditorStub, rimrafStub;
        beforeEach(() => {
            dlClusterStub = sandbox.stub(downloadsCluster.prototype);
            dlEditorStub = sandbox.stub(editorDownloader.prototype);
            rimrafStub = sandbox.stub();
            const unityDownload = proxyquire('../unityDownload', { rimraf: rimrafStub });
            return Promise.all([
                unityDownload.downloadEditorModules({ id: v1, downloadUrl: specData.downloadUrl }, [])
                    .then(() => unityDownload.cancelDownload(v1)),
                unityDownload.downloadUnityEditor({ id: v2, downloadUrl: specData.downloadUrl }, [])
                    .then(() => unityDownload.cancelDownload(v2))
            ]);
        });
        it('should destroy downloads', () => {
            expect(dlClusterStub.destroy).to.have.been.calledOnce;
            expect(dlEditorStub.destroy).to.have.been.calledOnce;
        });
    });
    describe.skip("isInProgress", () => {
        it('should be true when there is a download in progress', () => {
            const stubs = {
                '../lib/editorDownloader': {
                    start: () => {
                        console.log('nya');
                    },
                    '@global': true
                }
            };
            const unityDownload = proxyquire('../unityDownload', stubs);
            return unityDownload.downloadUnityEditor({ id: "i", downloadUrl: specData.downloadUrl }, [])
                .then(() => expect(unityDownload.isInProgress('i')).to.be.true);
        });
    });
    describe.skip('getDownloadProgress', () => {
        it('should return the total progress of the download ', () => {
            sandbox.stub(downloadsCluster.prototype, 'on');
            sandbox.stub(editorDownloader.prototype, 'getStats').callsFake(() => { return { total: { completed: 10 } }; });
            sandbox.stub(downloadsCluster.prototype, 'getStats').callsFake(() => { return { total: { completed: 50 } }; });
            unityDownload.downloadUnityEditor({ id: 'id', downloadUrl: specData.downloadUrl }, [])
                .then(() => expect(unityDownload.getDownloadProgress('id')).to.equal(10));
            unityDownload.downloadEditorModules({ id: 'id2', downloadUrl: specData.downloadUrl }, [])
                .then(() => expect(unityDownload.getDownloadProgress('id2')).to.equal(50));
        });
    });
    function buildFakeDownload(dlPath) {
        const downloadObject = getFakeDownload();
        downloadsData[dlPath] = {
            downloadObject,
            events: {}
        };
        downloadObject.start.returnsThis();
        downloadObject.on.callsFake((event, callback) => {
            downloadsData[dlPath].events[event] = callback;
        });
        return downloadObject;
    }
});
//# sourceMappingURL=unityDownload.spec.js.map