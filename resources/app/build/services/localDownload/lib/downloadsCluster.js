const downloader = require('../../../downloadManager/downloadManager');
const EventEmitter = require('events');
const path = require('path');
const { hubFS } = require('../../../fileSystem');
const logger = require('../../../logger')('DownloadsCluster');
const promisify = require('es6-promisify');
const getRemoteSize = promisify(require('remote-file-size'));
const _ = require('lodash');
const settings = require('../../localSettings/localSettings');
const { HUB_DL_STATUS, MT_FILE_DL_STATUS } = require('./constants');
const numeral = require('numeral');
class DownloadsCluster extends EventEmitter {
    constructor(id, downloadDest = hubFS.getTmpDirPath()) {
        super();
        if (id === undefined) {
            throw new TypeError('Bad parameter passed to DownloadsCluster constructor');
        }
        this.id = id;
        this.downloadDest = downloadDest;
        this.downloads = [];
        this.invalidDownloads = [];
        this.progressInterval = null;
        this.downloadsTotalSize = 0;
        this.downloaded = 0;
        this.totalProgress = 0;
        this.status = HUB_DL_STATUS.NOT_STARTED;
        this.ready = false;
    }
    static createDownloadsCluster(id, modules, downloadDest = hubFS.getTmpDirPath()) {
        if (id === undefined || modules.length === undefined || !Array.isArray(modules)) {
            return Promise.reject(new TypeError('Bad parameter passed to DownloadsCluster constructor'));
        }
        const dlCluster = new DownloadsCluster(id, downloadDest);
        const modulePromises = [];
        modules.forEach((module) => {
            if (module.id === undefined) {
                modulePromises.push(Promise.reject(new TypeError('DownloadsCluster constructor: one component as an undefined id')));
            }
            else {
                modulePromises.push(dlCluster.getFilenameFromUrl(module.downloadUrl)
                    .then((dlFileName) => {
                    if (dlFileName === undefined) {
                        return Promise.reject(new TypeError(`DownloadsCluster constructor: Invalid url: ${module.downloadUrl}, url must point to a file`));
                    }
                    const download = downloader.download(module.downloadUrl, path.join(dlCluster.downloadDest, dlFileName));
                    download.finished = false;
                    download.moduleId = module.id;
                    download.moduleName = module.name;
                    download.checksum = module.checksum;
                    dlCluster.downloads.push(download);
                    registerDownloadEvents(dlCluster, download);
                    return dlCluster;
                }));
            }
        });
        return Promise.all(modulePromises)
            .then(() => {
            dlCluster.ready = true;
            return dlCluster;
        });
    }
    getFilenameFromUrl(url) {
        if (!hubFS.hasExtension(url)) {
            return hubFS.getFinalRedirectUrl(url)
                .then((newUrl) => hubFS.filenameFromUrl(newUrl));
        }
        return Promise.resolve(hubFS.filenameFromUrl(url));
    }
    start() {
        if (!this.ready) {
            logger.info(`Downloads Cluster ${this.id}: not ready!`);
            return Promise.reject(`Downloads Cluster ${this.id}: not ready!`);
        }
        logger.debug(`Downloads Cluster ${this.id}: starting`);
        if (this.downloads.length === 0) {
            logger.info('Cluster is empty, nothing to start');
            this.status = HUB_DL_STATUS.FINISHED;
            this.emit('end', this.id);
            return Promise.resolve();
        }
        const sizeFetchPromises = [];
        this.downloads.forEach((download) => {
            sizeFetchPromises.push(getRemoteSize(download.url)
                .catch((e) => {
                if (e && e.message === 'Unable to determine file size') {
                    logger.warn(`Could not determine file size of ${download.moduleId}. Trying to download it still`);
                    return Promise.resolve(0);
                }
                logger.warn(`Cluster ${this.id}: Invalid URL for module ${download.moduleId}, won't download it. Error: ${JSON.stringify(e)}`);
                this.invalidDownloads.push({
                    id: download.moduleId,
                    name: download.moduleName,
                    error: 'Invalid url'
                });
                download.toRemove = true;
                return Promise.resolve(0);
            }));
        });
        const startPromise = Promise.all(sizeFetchPromises)
            .then((sizes) => {
            this.downloadsTotalSize = _.sum(sizes);
            logger.info(`Cluster total download size: ${numeral(this.downloadsTotalSize).format('0.00b')}`);
        })
            .then(() => hubFS.mkdirRecursive(this.downloadDest))
            .then(() => {
            this.downloads = this.downloads.filter(dl => !dl.toRemove);
            if (this.downloads.length === 0) {
                logger.info('Cluster contained only invalid urls, nothing to start');
                this.status = HUB_DL_STATUS.FINISHED;
                this.emit('end', this.id);
            }
            else {
                logger.info(`Cluster ${this.id}: downloads starting`);
                this.downloads.forEach((download) => download.start());
                this.status = HUB_DL_STATUS.STARTED;
                startProgressMonitoring(this);
                this.emit('start', this.id);
            }
        })
            .catch(e => {
            logger.warn(`Download Cluster ${this.id}: Start Error: ${e.message}`);
            throw e;
        });
        return startPromise;
    }
    destroy() {
        this.downloads.forEach((download) => {
            download.destroy();
        });
        this.status = HUB_DL_STATUS.CANCELED;
        clearInterval(this.progressInterval);
        this.emit('destroy', this.id);
        logger.info(`Download Cluster ${this.id}: Canceled`);
    }
    getStats() {
        return { total: {
                completed: this.totalProgress * 100,
                downloaded: this.downloaded
            } };
    }
    getDownloadedModules() {
        const modules = [];
        this.downloads.forEach(download => {
            modules.push({
                id: download.moduleId,
                installerPath: download.filePath
            });
        });
        return modules;
    }
}
function onError(cluster, dl) {
    if (cluster.status !== HUB_DL_STATUS.CANCELED) {
        cluster.downloadsTotalSize -= dl.getStats().total.size;
        cluster.emit('error', dl);
        logger.warn(`Download cluster ${cluster.id}: Failed to download module ${dl.moduleId}`);
        logger.warn(`Download error: ${dl.error}`);
    }
}
function onEnd(cluster, dl) {
    logger.debug(`Download cluster ${cluster.id}: end event for ${dl.moduleId}`);
    dl.finished = true;
    const allDownloadsFinished = cluster.downloads.reduce((prevStatus, download) => prevStatus && download.finished, true);
    if (allDownloadsFinished) {
        logger.info(`Cluster ${cluster.id}: Finished`);
        cluster.status = HUB_DL_STATUS.FINISHED;
        clearInterval(cluster.progressInterval);
        cluster.emit('end', cluster.id);
    }
}
function registerDownloadEvents(cluster, download) {
    download.on('end', onEnd.bind(null, cluster));
    download.on('error', onError.bind(null, cluster));
}
function startProgressMonitoring(cluster) {
    if (cluster.status === HUB_DL_STATUS.NOT_STARTED) {
        throw Error('Can\'t monitor progress on not started cluster');
    }
    const period = settings.get(settings.keys.DL_CLUSTER_INTERVAL);
    cluster.progressInterval = setInterval(() => {
        cluster.downloaded = 0;
        cluster.downloads.forEach((download) => {
            if (download.status !== MT_FILE_DL_STATUS.ERROR && download.status !== MT_FILE_DL_STATUS.STOPPED) {
                cluster.downloaded += download.getStats().total.downloaded;
            }
        });
        cluster.totalProgress = cluster.downloaded / cluster.downloadsTotalSize;
        logger.debug(`Cluster downloads ${cluster.id} progress: ${numeral(cluster.totalProgress).format('0.00%')}`);
    }, period);
}
module.exports = DownloadsCluster;
//# sourceMappingURL=downloadsCluster.js.map