module.exports = {
    HUB_DL_STATUS: {
        NOT_STARTED: 'not started',
        STARTED: 'started',
        FINISHED: 'finished',
        FAILED: 'failed',
        CANCELED: 'canceled',
        PENDING: 'pending',
    },
    MT_FILE_DL_STATUS: {
        DESTROYED: -3,
        STOPPED: -2,
        ERROR: -1,
        NOT_STARTED: 0,
        DOWNLOADING: 1,
        ERR_AND_RETRYING: 2,
        FINISHED: 3,
        PENDING: 4
    },
};
//# sourceMappingURL=constants.js.map