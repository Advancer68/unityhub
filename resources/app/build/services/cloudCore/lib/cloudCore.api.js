const cloudConfig = require('../../cloudConfig/cloudConfig');
const systemInfo = require('../../localSystemInfo/systemInfo');
const logger = require('../../../logger')('CloudCoreAPI');
const axios = require('axios');
const cloudCoreApiClient = {
    getUser(id, accessToken) {
        const url = `${cloudConfig.urls.core}/api/users/${id}`;
        logger.debug(`GET ${url}`);
        const config = this._getCommonConfig(accessToken);
        return axios.get(url, config);
    },
    getOrganizations(accessToken) {
        const url = `${cloudConfig.urls.core}/api/users/me?include=orgs`;
        logger.debug(`GET ${url}`);
        return axios.get(url, this._getCommonConfig(accessToken));
    },
    getTeamsSeatOrganizations(accessToken, userId) {
        if (cloudConfig.urls.seat_required) {
            const url = `${cloudConfig.urls.genesis_api_url}/v1/entitlements`;
            logger.debug(`GET ${url}`);
            const config = this._getCommonConfig(accessToken);
            config.params = {
                userId,
                isActive: true,
                namespace: 'unity_teams',
                type: 'TEAMS',
                tag: ['UnityTeamsFree', 'UnityTeamsStandard', 'UnityTeamsPro']
            };
            return axios.get(url, config);
        }
        return Promise.reject();
    },
    postRefreshAccessToken(refreshToken) {
        const url = `${cloudConfig.urls.core}/api/login/refresh`;
        const data = {
            grant_type: 'refresh_token',
            refresh_token: refreshToken
        };
        logger.debug(`POST ${url}`);
        return axios.post(url, data, this._getCommonConfig());
    },
    postLoginWithAuthCodeAndRedirectUri(code, redirectUri) {
        const url = `${cloudConfig.urls.identity}/v1/oauth2/token`;
        const data = {
            client_id: 'launcher',
            client_secret: '48CDB6BC4E7630C886CECBA2730A0CC9',
            grant_type: 'authorization_code',
            code,
            redirect_uri: redirectUri
        };
        logger.debug(`POST ${url}`);
        return axios.post(url, data, this._getCommonConfig());
    },
    postAuthorizedRedirect(payload) {
        const url = `${cloudConfig.urls.identity}/v1/oauth2/session_token`;
        logger.debug(`POST ${url}`);
        return axios.post(url, payload, this._getCommonConfig())
            .then(response => `${cloudConfig.urls.identity}/v1/oauth2/session_token/authorize?token=${response.data.session_token}`);
    },
    getProjects(accessToken) {
        const url = `${cloudConfig.urls.core}/api/projects`;
        logger.debug(`GET ${url}`);
        return axios.get(url, this._getCommonConfig(accessToken));
    },
    _getCommonConfig(accessToken) {
        const headerObj = {
            headers: {
                'User-Agent': `hub ${systemInfo.hubVersion()}.cn`,
                date: (new Date()).toUTCString(),
                'Content-Type': 'application/json'
            },
            responseType: 'json'
        };
        if (accessToken) {
            headerObj.headers.AUTHORIZATION = `Bearer ${accessToken}`;
        }
        return headerObj;
    }
};
module.exports = cloudCoreApiClient;
//# sourceMappingURL=cloudCore.api.js.map