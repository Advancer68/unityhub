const cloudApiClient = require('./cloudCore.api');
const cloudConfig = require('../../cloudConfig/cloudConfig');
const settings = require('../../localSettings/localSettings');
const logger = require('../../../logger')('CloudCoreAPI');
const axios = require('axios');
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(chaiAsPromised);
chai.use(sinonChai);
describe('cloudCoreApiClient', () => {
    let sandbox, coreApiMock, settingsMock;
    const successResponse = 'success', failResponse = 'fail', user = 'me', token = 'abc', wrongToken = 'wrongToken';
    let path, wrongCode, code, redirectUri, payload, sessionId, expectedUrl;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        settingsMock = {
            [settings.keys.CLOUD_ENVIRONMENT]: settings.cloudEnvironments.PRODUCTION,
            [settings.keys.SERVICES_URL_INTERVAL]: 100
        };
        sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);
        sandbox.stub(logger, 'error');
        sandbox.stub(cloudConfig, 'refreshData').resolves();
        sandbox.stub(cloudConfig, 'urls').get(() => {
            return {
                core: 'http://fakeCoreUrl.com',
                identity: 'http://fakeIdentityUrl.com',
            };
        });
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('getUser', () => {
        beforeEach(() => {
            sandbox.stub(axios, 'get').callsFake((url, options) => {
                if (url === cloudConfig.urls.core + path && options.headers.AUTHORIZATION === `Bearer ${token}`)
                    return Promise.resolve(successResponse);
                return Promise.reject(failResponse);
            });
        });
        const path = `/api/users/${user}`;
        it('should return a resolving promise with the response when the request succeeded', () => {
            return expect(cloudApiClient.getUser(user, token)).to.eventually.equal(successResponse);
        });
        it('should return a rejecting promise with the response when the request fails', () => {
            return expect(cloudApiClient.getUser(user, wrongToken)).to.eventually.be.rejectedWith(failResponse);
        });
    });
    describe('getOrganizations', () => {
        beforeEach(() => {
            path = '/api/users/me?include=orgs';
            sandbox.stub(axios, 'get').callsFake((url, options) => {
                if (url === cloudConfig.urls.core + path && options.headers.AUTHORIZATION === `Bearer ${token}`)
                    return Promise.resolve(successResponse);
                return Promise.reject(failResponse);
            });
        });
        it('should return a resolving promise with the response when the request succeeded', () => {
            return expect(cloudApiClient.getOrganizations(token)).to.eventually.equal(successResponse);
        });
        it('should return a rejecting promise with the response when the request fails', () => {
            return expect(cloudApiClient.getOrganizations(wrongToken)).to.eventually.be.rejectedWith(failResponse);
        });
    });
    describe('postRefreshAccessToken', () => {
        beforeEach(() => {
            path = '/api/login/refresh';
            sandbox.stub(axios, 'post').callsFake((url, data) => {
                if (url === cloudConfig.urls.core + path && data.refresh_token === token)
                    return Promise.resolve(successResponse);
                return Promise.reject(failResponse);
            });
        });
        it('should return a resolving promise with the response when the request succeeded', () => {
            return expect(cloudApiClient.postRefreshAccessToken(token)).to.eventually.equal(successResponse);
        });
        it('should return a rejecting promise with the response when the request fails', () => {
            return expect(cloudApiClient.postRefreshAccessToken(wrongToken)).to.eventually.be.rejectedWith(failResponse);
        });
    });
    describe('postLoginWithAuthCodeAndRedirectUri', () => {
        beforeEach(() => {
            path = '/v1/oauth2/token';
            wrongCode = 'wrongeCode';
            code = 'weif29fyh23';
            redirectUri = 'http://redirect.uri';
            sandbox.stub(axios, 'post').callsFake((url, data) => {
                if (url === cloudConfig.urls.identity + path && data.code === code && data.redirect_uri === redirectUri)
                    return Promise.resolve(successResponse);
                return Promise.reject(failResponse);
            });
        });
        it('should return a resolving promise with the response when the request succeeded', () => {
            return expect(cloudApiClient.postLoginWithAuthCodeAndRedirectUri(code, redirectUri)).to.eventually.equal(successResponse);
        });
        it('should return a rejecting promise with the response when the request fails', () => {
            return expect(cloudApiClient.postLoginWithAuthCodeAndRedirectUri(wrongCode, redirectUri)).to.eventually.be.rejectedWith(failResponse);
        });
    });
    describe('postAuthorizedRedirect', () => {
        beforeEach(() => {
            path = '/v1/oauth2/session_token';
            payload = {};
            sessionId = '1234';
            expectedUrl = `${cloudConfig.urls.identity}/v1/oauth2/session_token/authorize?token=${sessionId}`;
            sandbox.stub(axios, 'post').callsFake((url, data) => {
                if (url === cloudConfig.urls.identity + path && data === payload)
                    return Promise.resolve({ data: { session_token: sessionId } });
                return Promise.reject(failResponse);
            });
        });
        it('should returns the redirection url with the session id as query param', () => {
            return expect(cloudApiClient.postAuthorizedRedirect(payload)).to.eventually.equal(expectedUrl);
        });
        it('should returns a rejecting promise with the response when the request fails', () => {
            return expect(cloudApiClient.postAuthorizedRedirect()).to.eventually.be.rejectedWith(failResponse);
        });
    });
    describe('getProjects', () => {
        const path = '/api/projects';
        beforeEach(() => {
            const path = '/api/projects';
            sandbox.stub(axios, 'get').callsFake((url, options) => {
                if (url === cloudConfig.urls.core + path && options.headers.AUTHORIZATION === `Bearer ${token}`)
                    return Promise.resolve(successResponse);
                return Promise.reject(failResponse);
            });
        });
        it('should return a resolving promise with the response when the request succeeded', () => {
            return expect(cloudApiClient.getProjects(token)).to.eventually.equal(successResponse);
        });
        it('should return a rejecting promise with the response when the request fails', () => {
            return expect(cloudApiClient.getProjects(wrongToken)).to.eventually.be.rejectedWith(failResponse);
        });
    });
});
//# sourceMappingURL=cloudCore.api.spec.js.map