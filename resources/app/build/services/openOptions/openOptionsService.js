var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { app } = require('electron');
const promisify = require('es6-promisify');
const localStorageAsync = require('electron-json-storage');
const i18nHelper = require('../../i18nHelper');
const localStorage = {
    get: promisify(localStorageAsync.get, localStorageAsync),
    set: promisify(localStorageAsync.set, localStorageAsync),
};
const OPEN_OPTIONS_STORAGE_KEY = 'open-options-config';
const keys = {
    enable: 'enable',
    disable: 'disable',
};
class OpenOptions {
    getAvailableSettings() {
        return [
            { name: i18nHelper.i18n.translate('OPEN_OPTIONS.ENABLE'), key: keys.enable },
            { name: i18nHelper.i18n.translate('OPEN_OPTIONS.DISABLE'), key: keys.disable },
        ];
    }
    getCurrentSetting() {
        return __awaiter(this, void 0, void 0, function* () {
            const setting = yield localStorage.get(OPEN_OPTIONS_STORAGE_KEY);
            return (setting && setting.value) || keys.enable;
        });
    }
    setSetting(value) {
        return __awaiter(this, void 0, void 0, function* () {
            app.setLoginItemSettings({
                openAtLogin: value === keys.enable,
            });
            return yield localStorage.set(OPEN_OPTIONS_STORAGE_KEY, { value });
        });
    }
    openAtLoginEnabled() {
        return __awaiter(this, void 0, void 0, function* () {
            const setting = yield this.getCurrentSetting();
            return setting === keys.enable;
        });
    }
}
module.exports = new OpenOptions();
//# sourceMappingURL=openOptionsService.js.map