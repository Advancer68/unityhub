var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { fs } = require('../../fileSystem');
const path = require('path');
const sax = require('sax');
const EditorPrefLinux = require('./editorPref_linux');
const editorPreferencesJson = require('./spec/resources/editorPreferencesLinux');
const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
function loadMockData() {
    return __awaiter(this, void 0, void 0, function* () {
        return {
            editorPreferences: yield readFile('editorPreferencesLinux.xml'),
            updatedEditorPreferences: yield readFile('updatedEditorPreferencesLinux.xml')
        };
    });
}
function readFile(fileName) {
    return new Promise(resolve => {
        const filePath = path.join(__dirname, 'spec', 'resources', fileName);
        fs.readFile(filePath, 'utf8', (error, contents) => {
            if (error) {
                throw error;
            }
            else {
                resolve(contents);
            }
        });
    });
}
describe('EditorPref Linux', () => {
    let sandbox, editorPref, mockData;
    beforeEach(() => __awaiter(this, void 0, void 0, function* () {
        sandbox = sinon.sandbox.create();
        editorPref = new EditorPrefLinux();
        mockData = yield loadMockData();
    }));
    afterEach(() => {
        sandbox.restore();
    });
    describe('getPrefs', () => {
        let keyPrefix, prefPath, errorMessage, fileSystem, result;
        beforeEach(() => {
            fileSystem = {};
            errorMessage = "UURGH";
            sandbox.stub(fs, 'readFile').callsFake((filePath, encoding, callback) => {
                const preferences = fileSystem[filePath];
                if (!preferences) {
                    callback(errorMessage);
                }
                else {
                    callback(null, preferences);
                }
            });
        });
        describe('when no argument is passed', () => {
            beforeEach(() => {
                fileSystem[EditorPrefLinux.defaultPrefPath] = mockData.editorPreferences;
            });
            it('should fetch all preferences in default path', () => __awaiter(this, void 0, void 0, function* () {
                result = yield editorPref.getPrefs(keyPrefix, prefPath);
                expect(result).to.eql(editorPreferencesJson);
            }));
        });
        describe('when only key prefix is passed', () => {
            let expectedResult;
            beforeEach(() => {
                keyPrefix = 'RecentlyUsedProjectPaths';
                fileSystem[EditorPrefLinux.defaultPrefPath] = mockData.editorPreferences;
                expectedResult = Object.keys(editorPreferencesJson)
                    .filter(key => key.startsWith(keyPrefix))
                    .reduce((obj, key) => {
                    obj[key] = editorPreferencesJson[key];
                    return obj;
                }, {});
            });
            it('should fetch all preferences with keys matching the passed prefix', () => __awaiter(this, void 0, void 0, function* () {
                result = yield editorPref.getPrefs(keyPrefix, prefPath);
                expect(result).to.eql(expectedResult);
            }));
        });
        describe('when key prefix and preferences path is passed', () => {
            let expectedResult;
            beforeEach(() => {
                keyPrefix = 'RecentlyUsedProjectPaths';
                prefPath = 'foo/ho/ho/bar';
                fileSystem[prefPath] = mockData.editorPreferences;
                expectedResult = Object.keys(editorPreferencesJson)
                    .filter(key => key.startsWith(keyPrefix))
                    .reduce((obj, key) => {
                    obj[key] = editorPreferencesJson[key];
                    return obj;
                }, {});
            });
            it('should fetch all prefs with keys matching the passed prefix in passed path', () => __awaiter(this, void 0, void 0, function* () {
                result = yield editorPref.getPrefs(keyPrefix, prefPath);
                expect(result).to.eql(expectedResult);
            }));
        });
        describe('when the preferences are not found', () => {
            it('should return empty object', () => __awaiter(this, void 0, void 0, function* () {
                result = yield editorPref.getPrefs(keyPrefix, prefPath);
                expect(result).to.eql({});
            }));
        });
        describe('when the preferences could not be parsed', () => {
            beforeEach(() => {
                sandbox.stub(sax, 'parser').throws('BLAAARGH');
            });
            it('should return empty object', () => __awaiter(this, void 0, void 0, function* () {
                result = yield editorPref.getPrefs(keyPrefix, prefPath);
                expect(result).to.eql({});
            }));
        });
    });
    describe('_updateRecentProjectsInEditorPreferences', () => {
        let recentProjects, expectedResult;
        beforeEach(() => {
            recentProjects = [
                '/home/unity/Apple Catch 6000',
                '/home/unity/New Unity Project 1/New Unity Project 1 (1)',
                '/home/unity/New Unity Project (2)',
                '/home/unity/New Unity Project (2) (copy)',
                '/home/unity/New Unity Project (8)',
                '/home/unity/New Unity Project (7)',
                '/home/unity/New Unity Project (3)',
                '/home/unity/New Unity Project 1/New Unity Project 1',
                '/home/unity/New Unity Project (6)',
                '/home/unity/New Unity Project (5)',
                '/home/unity/New Unity Project 1',
                '/home/unity/New Unity Project (4)',
                '/home/unity/Documents/3D tower template on cloud'
            ];
            expectedResult = mockData.updatedEditorPreferences.replace(/\r/g, '');
            plistWriteFileError = null;
            sandbox.stub(fs, 'readFile').callsFake((filePath, encoding, callback) => {
                callback(null, mockData.editorPreferences);
            });
            sandbox.stub(fs, 'writeFile').resolves();
        });
        it('should set editor preferences properly', () => __awaiter(this, void 0, void 0, function* () {
            yield editorPref._updateRecentProjectsInEditorPreferences(recentProjects);
            expect(fs.writeFile).to.have.been.calledWith(EditorPrefLinux.defaultPrefPath, expectedResult);
        }));
        it('should not fail if writing the preferences threw an exception', () => {
            fs.writeFile.throws('Blaaah');
            return expect(() => editorPref._updateRecentProjectsInEditorPreferences(recentProjects)).not.to.throw;
        });
    });
    describe('_b64EncodeUnicode', () => {
        it('should properly encode the ascii string', () => {
            expect(editorPref._b64EncodeUnicode('foo bar ascii string')).to.equal('Zm9vIGJhciBhc2NpaSBzdHJpbmc=');
        });
        it('should properly encode the utf-8 string', () => {
            expect(editorPref._b64EncodeUnicode('одной')).to.equal('0L7QtNC90L7QuQ==');
        });
    });
    describe('_b64DecodeUnicode', () => {
        it('should properly decode the encoded ascii string', () => {
            expect(editorPref._b64DecodeUnicode('Zm9vIGJhciBhc2NpaSBzdHJpbmc=')).to.equal('foo bar ascii string');
        });
        it('should properly decode the encoded utf-8 string', () => {
            expect(editorPref._b64DecodeUnicode('0L7QtNC90L7QuQ==')).to.equal('одной');
        });
    });
});
//# sourceMappingURL=editorPref_linux.spec.js.map