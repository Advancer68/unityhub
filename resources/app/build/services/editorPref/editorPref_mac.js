var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const os = require('os');
const plist = require('simple-plist');
const { promisify } = require('util');
const BaseEditorPref = require('./baseEditorPref');
const logger = require('../../logger')('EditorPrefMac');
const { hubFS } = require('../../fileSystem');
const defaultPrefsPaths = `${os.homedir()}/Library/Preferences/com.unity3d.UnityEditor5.x.plist`;
class EditorPrefMac extends BaseEditorPref {
    static get defaultPrefsPaths() { return defaultPrefsPaths; }
    getPrefs(keyPrefix = '', prefPath = EditorPrefMac.defaultPrefsPaths) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield hubFS.isFilePresentPromise(prefPath);
            }
            catch (err) {
                logger.warn(`com.unity3d.UnityEditor5.x.plist does not exist: ${err}`);
                return {};
            }
            try {
                const preferences = yield promisify(plist.readFile)(prefPath);
                return Object.keys(preferences)
                    .filter((key) => key.startsWith(keyPrefix))
                    .reduce((obj, key) => {
                    obj[key] = preferences[key];
                    return obj;
                }, {});
            }
            catch (err) {
                logger.warn('Exception happened while reading the editor Preferences', err);
                return {};
            }
        });
    }
    _updateRecentProjectsInEditorPreferences(recentProjects) {
        return __awaiter(this, void 0, void 0, function* () {
            const preferences = yield this.getPrefs();
            for (let i = 0; i < BaseEditorPref.MaxRecentProjectEntries; i++) {
                const key = this._formatRecentProjectKey(i);
                if (i < recentProjects.length) {
                    preferences[key] = recentProjects[i];
                }
                else {
                    delete preferences[key];
                }
            }
            yield this._setPrefs(preferences);
        });
    }
    _formatRecentProjectKey(index) {
        return `${BaseEditorPref.RecentProjectPrefix}-${index}`;
    }
    _setPrefs(preferences) {
        return promisify(plist.writeFile)(EditorPrefMac.defaultPrefsPaths, preferences)
            .catch(error => {
            logger.warn('Could not update editor preferences. Error occurred.', error);
        });
    }
}
module.exports = EditorPrefMac;
//# sourceMappingURL=editorPref_mac.js.map