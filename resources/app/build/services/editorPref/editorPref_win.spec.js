var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const proxyquire = require('proxyquire');
const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
const Registry = {
    getKeyValues() { },
    getRegistryKeyName() { },
    setBinaryValue() { },
    deleteKey() { },
    '@noCallThru': true
};
const registry = require('../editorApp/lib/registry_win');
const EditorPrefWin = proxyquire('./editorPref_win', {
    '@unityhub/unity-editor-registry': Registry
});
describe('EditorPref windows', () => {
    let sandbox, editorPref;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        editorPref = new EditorPrefWin();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('getPrefs', () => {
        let keyPrefix, registryKey, keys, result;
        beforeEach(() => {
            keys = {
                [registry.keys.prefs5x]: {
                    foo: 'bar',
                    bar: 'baz'
                }
            };
            sandbox.stub(Registry, 'getKeyValues').callsFake((key) => {
                const regKeyValue = keys[key];
                if (!regKeyValue)
                    return [];
                return [Object.keys(regKeyValue), Object.values(regKeyValue)];
            });
        });
        describe('when no argument is passed', () => {
            it('should fetch all the values from the default registry key', () => __awaiter(this, void 0, void 0, function* () {
                result = yield editorPref.getPrefs(keyPrefix, registryKey);
                expect(result).to.eql(keys[registry.keys.prefs5x]);
            }));
        });
        describe('when only key prefix is passed', () => {
            let expectedResults;
            beforeEach(() => {
                keyPrefix = 'myPrefix';
                expectedResults = {
                    [`${keyPrefix}-foo`]: 'bar',
                    [`${keyPrefix}-bar`]: 'boo',
                };
                Object.assign(keys[registry.keys.prefs5x], expectedResults);
            });
            it('should fetch all the values with keys matching the passed prefix', () => __awaiter(this, void 0, void 0, function* () {
                result = yield editorPref.getPrefs(keyPrefix, registryKey);
                expect(result).to.eql(expectedResults);
            }));
        });
        describe('when key prefix and registry key is passed', () => {
            let expectedResults;
            beforeEach(() => {
                keyPrefix = 'myPrefix';
                registryKey = 'fun\\software\\KEY';
                expectedResults = {
                    [`${keyPrefix}-foo`]: 'bar',
                    [`${keyPrefix}-bar`]: 'boo',
                };
                keys[registryKey] = Object.assign({
                    hoho: 'haha',
                    foo: 'fun'
                }, expectedResults);
            });
            it('should fetch all values with keys matching the passed prefix from the passed registry key', () => __awaiter(this, void 0, void 0, function* () {
                result = yield editorPref.getPrefs(keyPrefix, registryKey);
                expect(result).to.eql(expectedResults);
            }));
        });
        describe('when registry key is not found', () => {
            it('should return empty object', () => __awaiter(this, void 0, void 0, function* () {
                keyPrefix = '';
                registryKey = 'Oops, not a registry key';
                result = yield editorPref.getPrefs(keyPrefix, registryKey);
                expect(result).to.eql({});
            }));
        });
        describe('when the Registry module throws an exception', () => {
            beforeEach(() => {
                Registry.getKeyValues.throws();
            });
            it('should return empty object', () => __awaiter(this, void 0, void 0, function* () {
                result = yield editorPref.getPrefs(keyPrefix, registryKey);
                expect(result).to.eql({});
            }));
        });
    });
    describe('_updateRecentProjectsInEditorPreferences', () => {
        let recentProjects;
        beforeEach(() => {
            recentProjects = [
                'foo\\bar',
                'bar\\baz\\boo'
            ];
            sandbox.stub(Registry, 'getRegistryKeyName').callsFake(name => `${name}_foo`);
            sandbox.stub(Registry, 'setBinaryValue');
            sandbox.stub(Registry, 'deleteKey');
            editorPref._updateRecentProjectsInEditorPreferences(recentProjects);
        });
        it('should format the keys properly for passed recent projects', () => {
            recentProjects.forEach((projectPath, index) => {
                expect(Registry.setBinaryValue).to.have.been.calledWith(sinon.match.string, `${EditorPrefWin.RecentProjectPrefix}-${index}_foo`, sinon.match.string);
            });
        });
        it('should set the keys properly for passed recent projects', () => {
            recentProjects.forEach((projectPath, index) => {
                expect(Registry.setBinaryValue).to.have.been.calledWith(sinon.match.string, sinon.match.string, projectPath);
            });
        });
        it('should delete the keys between the last recent project and the maximum stored projects', () => {
            for (let i = recentProjects.length; i < EditorPrefWin.MaxRecentProjectEntries; i++) {
                expect(Registry.deleteKey).to.have.been.calledWith(sinon.match.string, `${EditorPrefWin.RecentProjectPrefix}-${i}_foo`);
            }
        });
    });
});
//# sourceMappingURL=editorPref_win.spec.js.map