var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { fs } = require('../../fileSystem');
const xml2js = require('xml2js');
const sax = require('sax');
const os = require('os');
const BaseEditorPref = require('./baseEditorPref');
const logger = require('../../logger')('EditorPrefLinux');
const defaultPrefPath = `${os.homedir()}/.local/share/unity3d/prefs`;
let prefsData;
class EditorPrefLinux extends BaseEditorPref {
    static get defaultPrefPath() { return defaultPrefPath; }
    get prefsData() { return prefsData; }
    getPrefs(keyPrefix = '', prefPath = EditorPrefLinux.defaultPrefPath) {
        prefsData = { prefs: {} };
        return new Promise((resolve) => {
            fs.readFile(prefPath, 'utf8', (error, fileContent) => {
                try {
                    if (error) {
                        logger.warn(error);
                        resolve({});
                    }
                    else {
                        const parser = sax.parser(true, { trim: true });
                        const prefList = {};
                        let currentTag = {};
                        parser.onopentag = (node) => {
                            currentTag = node;
                            this._populatePrefsData(currentTag, null);
                        };
                        parser.ontext = (text) => {
                            this._populatePrefsData(currentTag, text);
                            if (currentTag.name === 'pref' && currentTag.attributes.name.startsWith(keyPrefix)) {
                                if (currentTag.attributes.type === 'string') {
                                    prefList[currentTag.attributes.name] = this._b64DecodeUnicode(text);
                                }
                                else {
                                    prefList[currentTag.attributes.name] = text;
                                }
                            }
                        };
                        parser.onend = () => {
                            resolve(prefList);
                        };
                        parser.write(fileContent).close();
                    }
                }
                catch (err) {
                    logger.warn('Exception happened while reading the editor Preferences', err);
                    resolve({});
                }
            });
        });
    }
    _populatePrefsData(tag, value) {
        if (tag.name === 'unity_prefs') {
            prefsData.version = {
                major: tag.attributes.version_major,
                minor: tag.attributes.version_minor
            };
        }
        else if (tag.name === 'pref') {
            if (value === null) {
                value = '';
            }
            prefsData.prefs[tag.attributes.name] = {
                type: tag.attributes.type,
                value
            };
        }
    }
    _updateRecentProjectsInEditorPreferences(recentProjects) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.getPrefs();
            for (let i = 0; i < BaseEditorPref.MaxRecentProjectEntries; i++) {
                const key = this._formatRecentProjectKey(i);
                if (i < recentProjects.length) {
                    this._setPrefStringValue(key, recentProjects[i]);
                }
                else {
                    delete prefsData.prefs[key];
                }
            }
            yield this._updatePrefs();
        });
    }
    _formatRecentProjectKey(index) {
        return `${BaseEditorPref.RecentProjectPrefix}-${index}`;
    }
    _setPrefStringValue(key, value) {
        prefsData.prefs[key] = {
            type: 'string',
            value: this._b64EncodeUnicode(value)
        };
    }
    _updatePrefs() {
        const preferencesString = this._buildPrefsData();
        return fs.writeFile(EditorPrefLinux.defaultPrefPath, preferencesString)
            .catch(error => {
            logger.warn('Could not update editor preferences. Error occurred.', error);
        });
    }
    _buildPrefsData() {
        const builder = new xml2js.Builder({
            rootName: 'unity_prefs',
            headless: true,
            renderOpts: {
                pretty: true,
                indent: ' ',
                newline: '\n',
                allowEmpty: true
            }
        });
        const formattedPrefsData = {
            $: {
                version_major: prefsData.version.major,
                version_minor: prefsData.version.minor
            },
            pref: []
        };
        Object.keys(prefsData.prefs).sort().forEach((prefName) => {
            const prefData = prefsData.prefs[prefName];
            formattedPrefsData.pref.push({
                _: prefData.value,
                $: {
                    name: prefName,
                    type: prefData.type
                }
            });
        });
        return builder.buildObject(formattedPrefsData);
    }
    _b64EncodeUnicode(str) {
        const btoa = (b64Decoded) => Buffer.from(b64Decoded, 'binary').toString('base64');
        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g, (match, p1) => String.fromCharCode('0x' + p1)));
    }
    _b64DecodeUnicode(str) {
        const atob = (b64Encoded) => Buffer.from(b64Encoded, 'base64').toString('binary');
        return decodeURIComponent(atob(str).split('').map((c) => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)).join(''));
    }
}
module.exports = EditorPrefLinux;
//# sourceMappingURL=editorPref_linux.js.map