var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const _ = require('lodash');
const BaseEditorPref = require('./baseEditorPref');
const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
describe('BaseEditorPref', () => {
    let editorPref, sandbox, updateRecentProjectsStub;
    class EditorPref extends BaseEditorPref {
        getPrefs() { }
        _updateRecentProjectsInEditorPreferences(recentProjects) {
            return __awaiter(this, void 0, void 0, function* () {
                yield updateRecentProjectsStub(recentProjects);
            });
        }
    }
    beforeEach(() => {
        editorPref = new EditorPref();
        sandbox = sinon.sandbox.create();
        updateRecentProjectsStub = sandbox.stub().resolves();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('getRecentProjects', () => {
        let prefs, projectPaths, result;
        beforeEach(() => {
            projectPaths = {
                trailingSlashes: '/fun/path/',
                badPathSeparators: '\\path\\path\\path',
                normalRecentProject: '/path/to/project',
                duplicatePath: '/path/to/project',
            };
            prefs = {};
            sandbox.stub(editorPref, 'getPrefs').callsFake(prefix => _.omitBy(prefs, (value, key) => !key.startsWith(prefix)));
        });
        it('should return recentProjects', () => __awaiter(this, void 0, void 0, function* () {
            prefs = {
                [`${BaseEditorPref.RecentProjectPrefix}-0`]: 'foo/bar',
                [`${BaseEditorPref.RecentProjectPrefix}-1`]: 'foo/baz',
                potato: 5,
                [`${BaseEditorPref.RecentProjectPrefix}-2`]: 'bar/baz',
            };
            result = yield editorPref.getRecentProjects();
            const expectedResults = [
                'foo/bar',
                'foo/baz',
                'bar/baz'
            ];
            expect(result).to.eql(expectedResults);
        }));
        it('should have removed trailing slashes from paths', () => __awaiter(this, void 0, void 0, function* () {
            prefs = {
                [`${BaseEditorPref.RecentProjectPrefix}-0`]: projectPaths.trailingSlashes,
            };
            result = yield editorPref.getRecentProjects();
            expect(_.endsWith(result[0], '/')).to.be.false;
        }));
        it('should have converted path separators', () => __awaiter(this, void 0, void 0, function* () {
            prefs = {
                [`${BaseEditorPref.RecentProjectPrefix}-0`]: projectPaths.badPathSeparators,
            };
            result = yield editorPref.getRecentProjects();
            expect(result[0]).not.to.include('\\');
        }));
        it('should have sorted the paths by key name', () => __awaiter(this, void 0, void 0, function* () {
            prefs = {
                [`${BaseEditorPref.RecentProjectPrefix}-1`]: 'Second',
                [`${BaseEditorPref.RecentProjectPrefix}-2`]: 'Last',
                [`${BaseEditorPref.RecentProjectPrefix}-0`]: 'First'
            };
            result = yield editorPref.getRecentProjects();
            expect(result).to.eql(['First', 'Second', 'Last']);
        }));
        it('should have removed duplicate paths', () => __awaiter(this, void 0, void 0, function* () {
            prefs = {
                [`${BaseEditorPref.RecentProjectPrefix}-0`]: 'foo/bar',
                [`${BaseEditorPref.RecentProjectPrefix}-1`]: 'foo/bar',
                [`${BaseEditorPref.RecentProjectPrefix}-2`]: 'foo/bar',
                [`${BaseEditorPref.RecentProjectPrefix}-3`]: 'foo/foo'
            };
            result = yield editorPref.getRecentProjects();
            expect(result).to.eql(['foo/bar', 'foo/foo']);
        }));
    });
    describe('setMostRecentProject', () => {
        let recentProjects, mostRecentProject;
        beforeEach(() => {
            mostRecentProject = 'most/recent/project';
            recentProjects = ['foo', 'bar', 'foo/bar'];
            sandbox.stub(editorPref, 'getRecentProjects').callsFake(() => recentProjects);
        });
        it('should set passed project path as most recent project', () => __awaiter(this, void 0, void 0, function* () {
            yield editorPref.setMostRecentProject(mostRecentProject);
            const newRecentProjects = updateRecentProjectsStub.getCall(0).args[0];
            expect(newRecentProjects[0]).to.equal(mostRecentProject);
        }));
        it('should have converted path separators in passed path', () => __awaiter(this, void 0, void 0, function* () {
            mostRecentProject = 'fe\\fi\\fo';
            yield editorPref.setMostRecentProject(mostRecentProject);
            const newRecentProjects = updateRecentProjectsStub.getCall(0).args[0];
            expect(newRecentProjects[0]).to.equal('fe/fi/fo');
        }));
        it('should have removed duplicate project from recent project list', () => __awaiter(this, void 0, void 0, function* () {
            recentProjects.push(mostRecentProject);
            yield editorPref.setMostRecentProject(mostRecentProject);
            const newRecentProjects = updateRecentProjectsStub.getCall(0).args[0];
            newRecentProjects.splice(0, 1);
            newRecentProjects.forEach(project => expect(project).not.to.equal(mostRecentProject));
        }));
        it('should have updated recent projects in editor preferences', () => __awaiter(this, void 0, void 0, function* () {
            yield editorPref.setMostRecentProject(mostRecentProject);
            expect(updateRecentProjectsStub).to.have.been.calledWith([mostRecentProject].concat(recentProjects));
        }));
    });
    describe('deleteProject', () => {
        let recentProjects, projectPath;
        beforeEach(() => {
            projectPath = 'deleted/project';
            recentProjects = ['foo', 'bar', 'foo/bar', 'deleted/project'];
            sandbox.stub(editorPref, 'getRecentProjects').callsFake(() => recentProjects);
        });
        it('should delete the selected project', () => __awaiter(this, void 0, void 0, function* () {
            yield editorPref.deleteProject(projectPath);
            const newRecentProjects = updateRecentProjectsStub.getCall(0).args[0];
            expect(newRecentProjects.indexOf('deleted/project')).to.equal(-1);
        }));
    });
});
//# sourceMappingURL=baseEditorPrefs.spec.js.map