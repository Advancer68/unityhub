var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const _ = require('lodash');
class BaseEditorPref {
    static get RecentProjectPrefix() { return 'RecentlyUsedProjectPaths'; }
    static get MaxRecentProjectEntries() { return 40; }
    getPrefs() {
        throw Error('method getPrefs not overridden.');
    }
    getRecentProjects() {
        return __awaiter(this, void 0, void 0, function* () {
            let recentProjects = yield this.getPrefs(BaseEditorPref.RecentProjectPrefix);
            _.each(recentProjects, (value, key) => {
                value = this._convertPathSeparators(value);
                value = this._trimTrailingSlashes(value);
                recentProjects[key] = value;
            });
            recentProjects = this._getSortedRecentProjectPaths(recentProjects);
            return _.uniq(recentProjects);
        });
    }
    setMostRecentProject(mostRecentProject) {
        return __awaiter(this, void 0, void 0, function* () {
            mostRecentProject = this._convertPathSeparators(mostRecentProject);
            let recentProjects = yield this.getRecentProjects();
            recentProjects = recentProjects.filter((recentProject) => recentProject !== mostRecentProject);
            recentProjects.unshift(mostRecentProject);
            yield this._updateRecentProjectsInEditorPreferences(recentProjects);
        });
    }
    deleteProject(projectPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const recentProjects = yield this.getRecentProjects();
            const index = recentProjects.indexOf(projectPath);
            if (index >= 0) {
                recentProjects.splice(index, 1);
            }
            yield this._updateRecentProjectsInEditorPreferences(recentProjects);
        });
    }
    _getSortedRecentProjectPaths(recentProjects) {
        return Object.keys(recentProjects).sort().map(key => recentProjects[key]);
    }
    _trimTrailingSlashes(value) {
        return value.replace(/\/$/g, '');
    }
    _convertPathSeparators(value) {
        return value.replace(/\\/g, '/');
    }
    _updateRecentProjectsInEditorPreferences() {
        return __awaiter(this, void 0, void 0, function* () {
            throw Error('method _updateRecentProjectsInEditorPreferences not overridden.');
        });
    }
}
module.exports = BaseEditorPref;
//# sourceMappingURL=baseEditorPref.js.map