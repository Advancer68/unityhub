const postal = require('postal');
const requireAll = require('require-all');
const _ = require('lodash');
const logger = require('../../logger')('IpcControllerHelper');
class IpcControllerHelper {
    constructor(server, state) {
        this.server = server;
        this.state = state;
    }
    getAllControllers(directoryPath) {
        const controllers = {};
        requireAll({
            dirname: directoryPath,
            filter: /((?!spec)|.*)\.js$/,
            resolve: (ControllerClass) => {
                controllers[this._formatName(ControllerClass.name)] = new ControllerClass(this.server, this.state);
            }
        });
        logger.debug(`Found the following controllers in path: ${JSON.stringify(Object.keys(controllers))}`);
        return controllers;
    }
    bindControllers(controllers) {
        _.each(controllers, controller => {
            this._bindControllerIpcEvents(controller);
            this._bindControllerPostalEvents(controller);
        });
    }
    _formatName(className) {
        const trimmed = className.substr(0, className.lastIndexOf('Controller'));
        return _.lowerFirst(trimmed);
    }
    _bindControllerIpcEvents(controller) {
        _.each(controller.routes, (callbackMethod, eventName) => {
            this.server.on(eventName, (data, socket) => {
                callbackMethod.apply(controller, [data, socket, eventName]);
            });
        });
    }
    _bindControllerPostalEvents(controller) {
        _.each(controller.events, (callbackMethod, eventName) => {
            postal.subscribe({
                channel: 'app',
                topic: eventName,
                callback: (data) => {
                    callbackMethod.apply(controller, [data, eventName]);
                }
            });
        });
    }
}
module.exports = IpcControllerHelper;
//# sourceMappingURL=ipcControllerHelper.js.map