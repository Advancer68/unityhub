var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const os = require('os');
const processInfo = require('process');
const { hubFS } = require('../../../fileSystem');
const BaseController = require('../baseController');
const windowManager = require('../../../windowManager/windowManager');
const editorManager = require('../../editorManager/editorManager');
const EditorApp = require('../../editorApp/editorapp');
const logger = require('../../../logger')('IPCServer:window');
class WindowController extends BaseController {
    get routes() {
        return {
            'window:show': this.show,
            'window:show:modal': this.showModal,
            'window:hide': this.hide
        };
    }
    get events() {
        return {
            'main-window.change': this.onWindowChange,
        };
    }
    show(data, socket, eventName) {
        return __awaiter(this, void 0, void 0, function* () {
            this._setEditorArgs(data);
            const fragment = data.url;
            if (!fragment) {
                yield windowManager.mainWindow.show();
                this._sendMainWindowHandleData(eventName);
            }
            else if (fragment.indexOf('login') !== -1) {
                windowManager.showSignInWindow();
            }
            else if (fragment.indexOf('new') !== -1) {
                const editor = { version: data.editorVersion };
                windowManager.showNewProjectWindow(editor);
            }
            else {
                try {
                    yield editorManager.checkAutoLocate(data.location);
                }
                catch (err) {
                    windowManager.broadcastContent('auto.locate.error', err);
                }
                yield windowManager.mainWindow.show();
                windowManager.mainWindow.loadFragmentURL(fragment);
                this._sendMainWindowHandleData(eventName);
            }
        });
    }
    showModal(data, socket, eventName) {
        if (this.state.modalEditor) {
            this.server.emit(this.state.modalEditor, 'window.change', { state: 'all-closed' });
        }
        this.state.modalEditor = socket;
        this.state.sessionId = data.sessionId;
        this.state.editorVersion = data.editorVersion;
        this.state.editorLocation = data.location;
        if (this.state.editorLocation && this.state.editorVersion) {
            windowManager.mainWindow.setTitle(`${this.state.editorVersion} from ${hubFS.getEditorFolderFromExecutable(this.state.editorLocation)}`);
        }
        this.show(data, socket, eventName);
    }
    hide() {
        windowManager.mainWindow.hide();
    }
    onWindowChange(data) {
        if (!this.state.modalEditor)
            return;
        this.server.emit(this.state.modalEditor, 'window.change', data);
        if (data.state && data.state === 'all-closed') {
            this.state.clearModalEditor();
        }
    }
    _sendMainWindowHandleData(eventName) {
        let handleData;
        if (os.platform() === 'win32') {
            const handle = windowManager.mainWindow.getNativeWindowHandle();
            const lHandle = this._getHandle(handle);
            const aSessionId = processInfo.pid;
            handleData = { handle: lHandle, sessionId: aSessionId };
        }
        else {
            handleData = { sessionId: processInfo.pid };
        }
        this.server.emitAll(eventName, handleData);
    }
    _getHandle(handle) {
        let result;
        try {
            if (os.endianness() === 'LE') {
                result = handle.readInt32LE();
            }
            else {
                result = handle.readInt32BE();
            }
        }
        catch (error) {
            logger.error(`Couldn't parse handle for Hub window: ${error.message}`);
            result = 0;
        }
        return result;
    }
    _setEditorArgs(data) {
        if (data.editorArgs) {
            EditorApp.editorArgs = data.editorArgs.split(' ');
        }
    }
}
module.exports = WindowController;
//# sourceMappingURL=window.js.map