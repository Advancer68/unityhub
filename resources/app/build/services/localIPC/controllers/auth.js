const BaseController = require('../baseController');
const UnityAuth = require('../../localAuth/auth');
class AuthController extends BaseController {
    get routes() {
        return {
            'connectInfo:get': this.getConnectInfo,
            'userInfo:get': this.getUserInfo,
            'orgInfo:get': this.getOrganizationsInfo,
            'auth:logout': this.logout
        };
    }
    get events() {
        return {
            'userInfo.changed': this.onUserInfoChanged,
            'connectInfo.changed': this.onConnectInfoChanged
        };
    }
    getConnectInfo(data, socket) {
        UnityAuth.getConnectInfo().then((connectInfo) => {
            this.server.emit(socket, 'connectInfo:changed', JSON.parse(connectInfo));
        });
    }
    getUserInfo(data, socket) {
        UnityAuth.getUserInfo().then((userInfo) => {
            this.server.emit(socket, 'userInfo:changed', JSON.parse(userInfo));
        });
    }
    getOrganizationsInfo(data, socket) {
        UnityAuth.getOrganizations().then((orgInfo) => {
            this.server.emit(socket, 'orgInfo:changed', orgInfo);
        });
    }
    logout() {
        UnityAuth.logout();
    }
    onUserInfoChanged(data) {
        this.server.emitAll('userInfo:changed', data.userInfo);
    }
    onConnectInfoChanged(data) {
        this.server.emitAll('connectInfo:changed', data.connectInfo);
    }
}
module.exports = AuthController;
//# sourceMappingURL=auth.js.map