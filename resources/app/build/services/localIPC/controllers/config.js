const BaseController = require('../baseController');
const CloudConfig = require('../../cloudConfig/cloudConfig');
class ConfigController extends BaseController {
    get routes() {
        return {
            'config:get': this.getConfig,
            'config:get-default': this.getDefaultUrls,
            'config:get-urls': this.getUrls
        };
    }
    getConfig(data, socket, eventName) {
        this.server.emit(socket, eventName, CloudConfig.urls);
    }
    getDefaultUrls(data, socket, eventName) {
        const config = CloudConfig.getDefaultUrls(data.env);
        this.server.emit(socket, eventName, config);
    }
    getUrls(data, socket, eventName) {
        this.server.emit(socket, eventName, CloudConfig.urls);
    }
}
module.exports = ConfigController;
//# sourceMappingURL=config.js.map