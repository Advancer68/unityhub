const BaseController = require('../baseController');
const logger = require('../../../logger')('IPCServer:Health');
class HealthController extends BaseController {
    get routes() {
        return {
            'health:check': this.healthCheck
        };
    }
    healthCheck(data, socket, eventName) {
        logger.info('Health check requested');
        this.server.emit(socket, eventName, { health: true });
    }
}
module.exports = HealthController;
//# sourceMappingURL=health.js.map