var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const BaseController = require('../baseController');
const licenseClient = require('../../licenseService/licenseClientProxy');
class LicenseController extends BaseController {
    get routes() {
        return {
            'license:info': this.getInfo,
            'license:activate': this.activate,
            'license:return': this.returnLicense,
            'license:refresh': this.refresh,
            'license:load': this.load,
            'license:save': this.save,
            'license:clear-errors': this.clearErrors
        };
    }
    get events() {
        return {
            'license.change': this.onLicenseChange
        };
    }
    getInfo(data, socket, eventName) {
        licenseClient.getLicenseInfo((err, licenseData) => {
            this.server.emit(socket, eventName, licenseData);
        });
    }
    activate(data, socket, eventName) {
        licenseClient.activateNewLicense((err, succeeded) => {
            this.server.emit(socket, eventName, succeeded);
        });
    }
    returnLicense(data, socket, eventName) {
        licenseClient.returnLicense(() => {
            this.server.emit(socket, eventName, '');
        });
    }
    refresh(data, socket, eventName) {
        licenseClient.refreshLicense((err, succeeded) => {
            this.server.emit(socket, eventName, succeeded);
        });
    }
    load(data, socket, eventName) {
        return __awaiter(this, void 0, void 0, function* () {
            const { succeeded } = yield licenseClient.loadLicenseLegacy();
            this.server.emit(socket, eventName, succeeded);
        });
    }
    save(data, socket, eventName) {
        return __awaiter(this, void 0, void 0, function* () {
            const { succeeded } = yield licenseClient.saveLicense();
            this.server.emit(socket, eventName, succeeded);
        });
    }
    clearErrors(data, socket, eventName) {
        licenseClient.clearErrors(() => {
            this.server.emit(socket, eventName, '');
        });
    }
    onLicenseChange(data, eventName, socket) {
        if (socket) {
            this.server.emit(socket, eventName, data);
        }
        else {
            this.server.emitAll(eventName, data);
        }
    }
}
module.exports = LicenseController;
//# sourceMappingURL=license.js.map