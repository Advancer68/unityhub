var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const BaseController = require('../baseController');
const localProject = require('../../localProject/localProject');
class LocalProjectController extends BaseController {
    get routes() {
        return {
            'project:get-recent': this.getRecent,
            'project:get-data': this.getProjectData,
            'project:open': this.openProject,
            'project:set-editor-args': this.setEditorArgs
        };
    }
    get events() {
        return {
            'project.opened': this._forwardPostalEventToModalEditor,
            'project.create': this._forwardPostalEventToModalEditor,
            'project.createTemp': this._forwardPostalEventToModalEditor,
            'project.openedFromCloud': this._forwardPostalEventToModalEditor
        };
    }
    getRecent(data, socket, eventName) {
        localProject.getRecentProjects().then(recentProjects => {
            this.server.emit(socket, eventName, recentProjects);
        });
    }
    getProjectData(data, socket, eventName) {
        return __awaiter(this, void 0, void 0, function* () {
            const projectData = yield localProject.getProjectData(data.path);
            this.server.emit(socket, eventName, projectData);
        });
    }
    openProject(data, socket, eventName) {
        return __awaiter(this, void 0, void 0, function* () {
            let response;
            if (data.path) {
                response = yield localProject.openProject(data.path, undefined, { source: 'localIPC' });
            }
            else {
                response = yield localProject.showOpenOtherDialog('localIPC');
            }
            this.server.emit(socket, eventName, response);
        });
    }
    _forwardPostalEventToModalEditor(data, eventName) {
        if (!this.state.modalEditor)
            return;
        this.server.emit(this.state.modalEditor, eventName, data);
    }
}
module.exports = LocalProjectController;
//# sourceMappingURL=localProject.js.map