var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const path = require('path');
const { UnityIPCServer, UnityIPCClient } = require('../../common/common');
const logger = require('../../logger')('HubIPCService');
const IpcControllerHelper = require('./ipcControllerHelper');
const hubIPCState = require('./hubIPCState');
const windowManager = require('../../windowManager/windowManager');
const ipcServerName = 'hubIPCService';
const controllersPath = path.join(__dirname, 'controllers');
class HubIPCService {
    constructor() {
        this.state = hubIPCState;
        this.ipcServer = new UnityIPCServer(ipcServerName);
        this.ipcControllerHelper = new IpcControllerHelper(this.ipcServer, this.state);
        this.controllers = this.ipcControllerHelper.getAllControllers(controllersPath);
    }
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            logger.info('Init');
            yield this.ipcServer.startIPCServer();
            this.ipcServer.on('socket.disconnected', this.onSocketDisconnect.bind(this));
            this.ipcControllerHelper.bindControllers(this.controllers);
        });
    }
    onSocketDisconnect(socket, destroyedSocketID) {
        logger.info(`socket has disconnected: ${socket} , ${destroyedSocketID}!`);
        if (this.state.modalEditor === socket) {
            this.state.clearModalEditor();
            windowManager.mainWindow.hide();
        }
    }
    stop() {
        this.ipcServer.closeSockets();
    }
    static connect() {
        return __awaiter(this, void 0, void 0, function* () {
            const ipcClient = new UnityIPCClient(ipcServerName);
            yield ipcClient.connect();
            yield this._waitForConnection(ipcClient);
            return ipcClient;
        });
    }
    static _waitForConnection(ipcClient) {
        return new Promise(resolve => {
            ipcClient.on('connect', () => resolve());
        });
    }
}
module.exports = HubIPCService;
//# sourceMappingURL=hubIPCService.js.map