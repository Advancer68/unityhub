let modalEditor;
let sessionId;
let editorVersion;
let editorLocation;
class HubIPCState {
    get modalEditor() { return modalEditor; }
    set modalEditor(socket) { modalEditor = socket; }
    get sessionId() { return sessionId; }
    set sessionId(id) { sessionId = id; }
    get editorVersion() { return editorVersion; }
    set editorVersion(version) { editorVersion = version; }
    get editorLocation() { return editorLocation; }
    set editorLocation(location) { editorLocation = location; }
    clearModalEditor() {
        this.modalEditor = undefined;
        this.sessionId = undefined;
        this.editorVersion = undefined;
        this.editorLocation = undefined;
    }
}
module.exports = new HubIPCState();
//# sourceMappingURL=hubIPCState.js.map