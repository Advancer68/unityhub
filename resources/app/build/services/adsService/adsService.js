var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const axios = require('axios');
const promisify = require('es6-promisify');
const localStorageAsync = require('electron-json-storage');
const localStorage = {
    get: promisify(localStorageAsync.get, localStorageAsync),
    set: promisify(localStorageAsync.set, localStorageAsync),
};
const adsConfigKey = 'ads-config';
class AdsService {
    constructor() {
        this.endpoint = 'https://public-cdn.cloud.unitychina.cn/hub/prod/top-bar';
    }
    init() {
        this.syncConfig();
    }
    getConfig() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield localStorage.get(adsConfigKey);
        });
    }
    syncConfig() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield axios.get(`${this.endpoint}/ads.json`, null, { responseType: 'json' });
            localStorage.set(adsConfigKey, this.parseConfig(response.data));
        });
    }
    parseConfig(data) {
        if (!data) {
            return {};
        }
        const { ads, url } = data;
        return {
            ads: `${this.endpoint}/${ads}`,
            url
        };
    }
}
module.exports = new AdsService();
//# sourceMappingURL=adsService.js.map