var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const cloudConfig = require('./cloudConfig');
const settings = require('../localSettings/localSettings');
const localStorage = require('electron-json-storage');
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const nock = require('nock');
chai.use(chaiAsPromised);
chai.use(sinonChai);
describe('CloudConfig', () => {
    let sandbox, settingsMock;
    let mockStorage = {};
    beforeEach(() => __awaiter(this, void 0, void 0, function* () {
        sandbox = sinon.sandbox.create();
        settingsMock = {
            [settings.keys.CLOUD_ENVIRONMENT]: settings.cloudEnvironments.DEV,
            [settings.keys.SERVICES_URL_INTERVAL]: 100
        };
        sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);
        sandbox.stub(localStorage, 'get').callsFake((key, callback) => { callback(null, mockStorage); });
        sandbox.stub(localStorage, 'set').callsFake((key, value, callback) => { callback(); });
        let startRefreshIntervalStub = sandbox.stub(cloudConfig, 'startRefreshInterval').resolves();
        yield cloudConfig.init();
        startRefreshIntervalStub.restore();
    }));
    afterEach(() => {
        sandbox.restore();
    });
    describe('init', () => {
        beforeEach(() => {
            sandbox.stub(cloudConfig, 'startRefreshInterval').resolves();
        });
        it('should call the startRefreshInterval', () => {
            return cloudConfig.init().then(() => {
                expect(cloudConfig.startRefreshInterval).to.have.been.called;
            });
        });
        it('should set the default cloudConfig as cloudConfig when there is no cache', () => {
            mockStorage = null;
            cloudConfig.data = undefined;
            return cloudConfig.init().then(() => {
                expect(cloudConfig.urls).to.deep.equal(cloudConfig.getDefaultUrls());
            });
        });
        it('should set the cached cloudConfig as cloudConfig when there is cache', () => {
            cloudConfig.data = undefined;
            mockStorage = { blah: "blah" };
            return cloudConfig.init().then(() => {
                expect(cloudConfig.urls).to.deep.equal(mockStorage);
            });
        });
    });
    describe('refreshConfigUrls', () => {
        let httpMock;
        beforeEach(() => __awaiter(this, void 0, void 0, function* () {
            httpMock = nock(/public-cdn\.cloud\.unitychina\.cn/);
        }));
        afterEach(() => nock.cleanAll());
        it('should return a resolving promise when request succeeded', () => {
            httpMock.get(/.*/).reply(200);
            let promise = cloudConfig.refreshData();
            return expect(promise).to.eventually.be.fulfilled;
        });
        it('should return a resolving promise when request fails', () => {
            httpMock.get(/.*/).reply(400);
            let promise = cloudConfig.refreshData();
            return expect(promise).to.eventually.be.fulfilled;
        });
        it.skip('should set the config urls with the response when the request succeeded', (done) => {
            let response = { blah: 'blah' };
            httpMock.get(/.*/).reply(200, response);
            cloudConfig.refreshData()
                .then(() => {
                expect(cloudConfig.urls).to.deep.equal(response);
                done();
            })
                .catch(done);
        });
        it('should set the config urls to the default list when the request failed', () => {
            httpMock.get(/.*/).reply(400);
            return cloudConfig.refreshData()
                .then(() => {
            });
        });
    });
    describe('startUrlsRefreshInterval', () => {
        xit('should call setInterval', () => {
            let clock = sandbox.useFakeTimers();
            let stub = sandbox.stub(cloudConfig, 'refreshConfigUrls');
            cloudConfig.startUrlsRefreshInterval();
            clock.tick(settingsMock[settings.keys.SERVICES_URL_INTERVAL] + 1);
            expect(stub).to.have.been.called;
        });
    });
    describe('stopUrlRefreshInterval', () => {
        it('should clear interval', () => {
            const timers = require('timers');
            let clock = sandbox.useFakeTimers(), stub = sandbox.stub(clock, 'clearInterval');
            sandbox.stub(cloudConfig, 'refreshConfigUrls');
            cloudConfig.startRefreshInterval();
            clock.tick(settingsMock[settings.keys.SERVICES_URL_INTERVAL] + 1);
            cloudConfig.stopRefreshInterval();
            expect(stub).to.have.been.called;
        });
    });
    describe('getConfigUrl', function () {
        it('should resolve with the expected url when the given url id is valid', () => {
            const promises = cloudConfig.urlIDs.map((id, index) => cloudConfig.getConfigUrl(index));
            const results = cloudConfig.urlIDs.map((id) => cloudConfig.urls[id]);
            return expect(Promise.all(promises)).to.eventually.deep.equal(results);
        });
        it('should reject when the given url id is not a number', () => {
            return expect(cloudConfig.getConfigUrl('NaaaaN')).to.eventually.be.rejected;
        });
        it('should reject when the given url is not within the valid range', () => {
            return expect(cloudConfig.getConfigUrl(cloudConfig.urlIDs.length)).to.eventually.be.rejected;
        });
    });
});
//# sourceMappingURL=cloudConfig.spec.js.map