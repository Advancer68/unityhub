const logger = require('../../logger')('CloudConfig');
const settings = require('../localSettings/localSettings');
const PollingClient = require('../../common/pollingClient');
const defaultCloudUrls = {
    dev: {
        core: 'https://andy-core.cloud.unity.cn',
        collab: 'https://andy-collab.cloud.unity3d.com',
        webauth: 'https://accounts-dev.unity3d.com',
        login: 'https://andy-core.cloud.unity3d.com/api/login',
        identity: 'https://api-int.unity.cn',
        license: 'https://license-dev.hq.unity3d.com',
        activation: 'https://activation-dev.hq.unity3d.com',
        perf: 'https://staging-perf.cloud.unity3d.com',
        portal: 'https://id-int.unity.cn',
        hub_installer_location: 'https://public-cdn.cloud.unitychina.cn/hub/dev/',
        'activity-feed': false,
        analytics: 'http://localhost:5000/v1/events',
        'cdp-analytics': 'http://localhost:5000/v1/events',
        communityUrl: 'https://connect-dev.unity.com/hub',
        uprUrl: 'https://connect.unity.cn/hub/upr',
        ucgUrl: 'https://connect.unity.cn/hub/ucg',
        analyticsOptOut: 'https://remote-config-proxy-stg.uca.cloud.unity3d.com/',
        plasticWebServer: 'https://plasticweb-int.unity.cn',
        templatesConfig: 'https://template-cdn.cloud.unity3d.com/staging/dynamicTemplatesConfig.json',
    },
    staging: {
        core: 'https://staging-core.cloud.unity.cn',
        collab: 'https://staging-collab.cloud.unity3d.com',
        webauth: 'https://accounts-staging.unity3d.com',
        login: 'https://staging-core.cloud.unity3d.com/api/login',
        identity: 'https://api-staging.unity.cn',
        license: 'https://license-staging.hq.unity3d.com',
        activation: 'https://activation-staging.hq.unity3d.com',
        perf: 'https://staging-perf.cloud.unity3d.com',
        portal: 'https://id-staging.unity.cn',
        hub_installer_location: 'https://public-cdn.cloud.unitychina.cn/hub/stage/',
        'activity-feed': false,
        analytics: 'https://cloud-staging.uca.cloud.unity3d.com',
        'cdp-analytics': 'https://stg-lender.cdp.internal.unity3d.com',
        communityUrl: 'https://connect-internal.unity.com/hub',
        uprUrl: 'https://connect.unity.cn/hub/upr',
        ucgUrl: 'https://connect.unity.cn/hub/ucg',
        analyticsOptOut: 'https://remote-config-proxy-stg.uca.cloud.unity3d.com/',
        plasticWebServer: 'https://plasticweb-staging.unity.cn',
        templatesConfig: 'https://template-cdn.cloud.unity3d.com/staging/dynamicTemplatesConfig.json',
    },
    production: {
        core: 'https://core.cloud.unity.cn',
        collab: 'https://collab.cloud.unity3d.com',
        webauth: 'https://accounts.unity3d.com',
        login: 'https://core.cloud.unity3d.com/api/login',
        identity: 'https://api.unity.cn',
        license: 'https://license.unity.cn',
        activation: 'https://activation.unity.cn',
        perf: 'https://perf.cloud.unity3d.com',
        portal: 'https://id.unity.cn',
        hub_installer_location: 'https://public-cdn.cloud.unitychina.cn/hub/prod/',
        'activity-feed': false,
        analytics: 'https://analytics.cloud.unity3d.com',
        'cdp-analytics': 'https://prd-lender.cdp.internal.unity3d.com',
        communityUrl: 'https://connect.unity.cn/hub',
        analyticsOptOut: 'https://config.uca.cloud.unity3d.com/',
        uprUrl: 'https://connect.unity.cn/hub/upr',
        ucgUrl: 'https://connect.unity.cn/hub/ucg',
        plasticWebServer: 'https://plasticweb.unity.cn',
        templatesConfig: 'https://template-cdn.cloud.unity3d.com/prod/dynamicTemplatesConfig.json',
    }
};
const urlIDs = [
    'core',
    'collab',
    'webauth',
    'login',
    'license',
    'activation',
    'identity',
    'portal',
    'perf',
    'releases',
    'activity-feed',
    'cdp-analytics',
    'communityUrl',
    'analyticsOptOut'
];
const defaultConfigApi = 'http://public-cdn.cloud.unitychina.cn/config/';
const STORAGE_KEY = 'cloudConfig.json';
class CloudConfig extends PollingClient {
    get logger() {
        return logger;
    }
    get jsonStorageKey() {
        return STORAGE_KEY;
    }
    get refreshIntervalKey() {
        return settings.keys.SERVICES_URL_INTERVAL;
    }
    get isInitBlocking() {
        return true;
    }
    get defaultData() {
        return defaultCloudUrls[this.cloudEnvironment];
    }
    get overwriteDefault() {
        return true;
    }
    get urlIDs() {
        return urlIDs;
    }
    get urls() {
        return this.data;
    }
    setEndpoint() {
        let baseUrl = settings.get(settings.keys.SERVICE_CONFIG_BASE_URL) || defaultConfigApi;
        if (baseUrl.slice(-1) !== '/') {
            baseUrl += '/';
        }
        this.endpoint = baseUrl + this.cloudEnvironment;
        logger.info(`Cloud Config endpoint: ${this.endpoint}`);
    }
    getDefaultUrls() {
        return this.defaultData;
    }
    getConfigUrl(urlIndex) {
        if (!isNaN(urlIndex) && urlIndex >= 0 && urlIndex < urlIDs.length) {
            return Promise.resolve(this.data[urlIDs[urlIndex]]);
        }
        return Promise.reject(`cloudConfig service: Invalid url index ${urlIndex}, valid range is [0;${urlIDs.length}]`);
    }
    getHubBaseEndpoint() {
        return this.data.hub_installer_location;
    }
    getCommunityUrl() {
        if (this.data.communityUrl === null || this.data.communityUrl === undefined || this.data.communityUrl.length === 0) {
            return Promise.resolve(this.defaultData.communityUrl);
        }
        return Promise.resolve(this.data.communityUrl);
    }
    getUPRUrl() {
        if (this.data.uprUrl === null || this.data.uprUrl === undefined || this.data.uprUrl.length === 0) {
            return Promise.resolve(this.defaultData.uprUrl);
        }
        return Promise.resolve(this.data.uprUrl);
    }
    getUCGUrl() {
        if (this.data.ucgUrl === null || this.data.ucgUrl === undefined || this.data.ucgUrl.length === 0) {
            return Promise.resolve(this.defaultData.ucgUrl);
        }
        return Promise.resolve(this.data.ucgUrl);
    }
    getPlasticWebServer() {
        if (this.data.plasticWebServer === null || this.data.plasticWebServer === undefined || this.data.plasticWebServer.length === 0) {
            return Promise.resolve(this.defaultData.plasticWebServer);
        }
        return Promise.resolve(this.data.plasticWebServer);
    }
}
module.exports = new CloudConfig();
//# sourceMappingURL=cloudConfig.js.map