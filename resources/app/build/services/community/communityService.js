var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const communityConfig = require('./defaultCommunity');
const localStorage = require('electron-json-storage');
const axios = require('axios');
const promisify = require('es6-promisify');
const auth = require('../localAuth/auth');
class CommunityService {
    constructor() {
        this.showCommunity = communityConfig.showCommunity;
        this.endpoint = 'https://connect.unity.com/api/hub/lastUpdateTime';
    }
    setShowCommunity(show) {
        this.showCommunity = show;
    }
    getShowCommunity() {
        return this.showCommunity;
    }
    setLastViewTime(time = new Date()) {
        localStorage.set('lastViewTime', { lastViewTime: time });
    }
    getLastViewTime() {
        return __awaiter(this, void 0, void 0, function* () {
            const hasKey = yield promisify(localStorage.has)('lastViewTime');
            if (hasKey) {
                const lastViewTime = yield promisify(localStorage.get)('lastViewTime');
                return Promise.resolve(lastViewTime.lastViewTime);
            }
            const date = new Date();
            this.setLastViewTime(date);
            return Promise.resolve(date.toJSON());
        });
    }
    getLastUpdateTime() {
        return auth.getUserInfo().then(user => {
            var userId = JSON.parse(user).userId;
            const buff = Buffer.from(userId);
            const encodedUserId = encodeURIComponent(buff.toString('base64'));
            return axios.get(this.endpoint, {
                params: {
                    userID: encodedUserId
                },
                responseType: 'json'
            });
        })
            .then(response => {
            const data = JSON.parse(JSON.stringify(response.data));
            return Promise.resolve(data.lastUpdateTime);
        });
    }
    updateTimeGreaterThanViewTime() {
        var lastUpdateTime;
        var lastViewTime;
        return this.getLastUpdateTime().then((val) => {
            lastUpdateTime = val;
            return this.getLastViewTime();
        }).then((val) => {
            lastViewTime = val;
            if (lastUpdateTime > lastViewTime) {
                return true;
            }
            return false;
        });
    }
}
module.exports = new CommunityService();
//# sourceMappingURL=communityService.js.map