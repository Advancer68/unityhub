const cloudConfig = require('../cloudConfig/cloudConfig');
const licenseCore = require('./licenseCore');
const expect = require('chai').expect;
const os = require('os');
const DOMParser = require('xmldom').DOMParser;
const { fs } = require('../../fileSystem');
const axios = require('axios'), sinon = require('sinon');
describe('LicenseCore', function () {
    let sandbox;
    const testLicenseData = 'hello';
    let stub;
    beforeEach(function () {
        sandbox = sinon.sandbox.create();
        sandbox.stub(fs, 'readFile').withArgs(licenseCore.licenseFile)
            .yields(undefined, testLicenseData);
        sandbox.stub(fs, 'writeFileSync').returns();
        sandbox.stub(fs, 'unlinkSync').returns();
        stub = sandbox.stub(axios, 'post');
        stub.returns(Promise.resolve({ statusCode: 200, body: `<Transaction> 
          <Survey> 
            <Answered>true</Answered> 
          </Survey> 
          <Rx>9854f2c55e2ebf250e42d5bd0a0c538d0d4f81e6</Rx> 
        </Transaction>`
        }));
        sandbox.stub(cloudConfig, 'urls').get(() => ({}));
    });
    afterEach(function () {
        sandbox.restore();
    });
    it('verify updateLicense parameter', function (done) {
        licenseCore.updateLicense().then(() => {
            expect(licenseCore.lastAttemptUpdateDate).not.null;
            expect(stub).to.have.been.calledOnce;
            [url, data] = stub.args[0];
            expect(url.includes('cmd=2')).to.equal(true);
            expect(data).to.equal(testLicenseData);
            done();
        }).catch(done);
    });
    it('verify returnLicense parameter', function (done) {
        licenseCore.returnLicense().then(() => {
            expect(stub).to.have.been.calledOnce;
            [url, data] = stub.args[0];
            expect(url.includes('cmd=3')).to.equal(true);
            expect(data).to.equal(testLicenseData);
            done();
        }).catch(done);
    });
    it('Windows10Test', function () {
        sandbox.stub(os, 'platform').returns('win32');
        const releaseStub = sandbox.stub(os, 'release');
        releaseStub.onCall(0).returns('10.0.14393');
        releaseStub.onCall(1).returns('9.0.14393');
        licenseCore.machineBindings = { 1: '1', 2: '2', 3: '3', 4: '4', 5: '5' };
        expect(licenseCore.rawLicenseData().includes('Key="5"')).to.equal(true);
        expect(licenseCore.rawLicenseData().includes('Key="5"')).to.equal(false);
    });
    it('Should include machine binding 5 when machine binding 2 is null', function () {
        sandbox.stub(os, 'platform').returns('darwin');
        licenseCore.machineBindings = { 1: '1', 2: null, 3: '3', 4: '4', 5: '5' };
        expect(licenseCore.rawLicenseData().includes('Key="5"')).to.equal(true);
    });
    it('Should include machine binding 5 when machine binding 2 is empty', function () {
        sandbox.stub(os, 'platform').returns('darwin');
        licenseCore.machineBindings = { 1: '1', 2: '', 3: '3', 4: '4', 5: '5' };
        expect(licenseCore.rawLicenseData().includes('Key="5"')).to.equal(true);
    });
    describe('ReadLicenseKind', function () {
        var xmlWithoutFeatures = '<root><something/></root>';
        var xmlProFeatures = `<root>
    <Features>
            <Feature Value="22"/>
            <Feature Value="33"/>
            <Feature Value="23"/>
            <Feature Value="12"/>
            <Feature Value="34"/>
            <Feature Value="13"/>
            <Feature Value="24"/>
            <Feature Value="36"/>
            <Feature Value="25"/>
            <Feature Value="17"/>
            <Feature Value="18"/>
            <Feature Value="19"/>
            <Feature Value="0"/>
            <Feature Value="1"/>
            <Feature Value="2"/>
            <Feature Value="3"/>
            <Feature Value="4"/>
            <Feature Value="5"/>
            <Feature Value="6"/>
            <Feature Value="8"/>
            <Feature Value="9"/>
            <Feature Value="30"/>
            <Feature Value="20"/>
            <Feature Value="21"/>
        </Features>
      </root>
    `;
        var xmlPeFeatures = `<root>
    <Features>
            <Feature Value="33"/>
            <Feature Value="1"/>
            <Feature Value="12"/>
            <Feature Value="2"/>
            <Feature Value="24"/>
            <Feature Value="3"/>
            <Feature Value="36"/>
            <Feature Value="17"/>
            <Feature Value="19"/>
            <Feature Value="62"/>
        </Features>
      </root>
    `;
        it('ReturnFalseWithoutFeatures', function () {
            var doc = new DOMParser().parseFromString(xmlWithoutFeatures);
            expect(licenseCore.readToken(doc)).to.equal(false);
        });
        it('CanReadProFeatures', function () {
            var doc = new DOMParser().parseFromString(xmlProFeatures);
            expect(licenseCore.readToken(doc)).to.equal(true);
            expect(licenseCore.getLicenseKind()).to.equal("Unity Pro");
            expect(licenseCore.mToken.length).to.equal(24);
        });
        it('CanReadPersonalFeatures', function () {
            var doc = new DOMParser().parseFromString(xmlPeFeatures);
            expect(licenseCore.readToken(doc)).to.equal(true);
            expect(licenseCore.getLicenseKind()).to.equal("Unity Personal");
            expect(licenseCore.mToken.length).to.equal(10);
        });
    });
});
//# sourceMappingURL=licenseCore.spec.js.map