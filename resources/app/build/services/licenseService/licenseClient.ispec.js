const licenseCore = require('./licenseCore');
const licenseHelper = require('./licenseHelper');
const tokenManager = require('../../tokenManager/tokenManager');
const { fs } = require('../../fileSystem');
const os = require('os');
const request = require('request-promise-native'), sinon = require('sinon');
sinon.stub(licenseCore, 'verifyLicense')
    .returns(Promise.resolve(true));
const licenseClient = require('./licenseClient');
const licenseServiceFsm = require('./licenseServiceFsm');
const expect = require('chai').expect;
const path = require('path');
var dialog = require('electron').dialog;
var machineService = require('../machineService/machineService');
const UnityAuth = require('../localAuth/auth');
function WaitFSMToState(state, callback) {
    licenseServiceFsm.on('transition', (transition) => {
        if (transition.toState == state) {
            licenseClient.getLicenseInfo((error, result) => {
                callback(error, result);
            });
        }
    });
}
function WaitFSMToMultlipleState(state, nextState, callback) {
    var firstStateHit = false;
    licenseServiceFsm.on('transition', function (transition) {
        if (transition.toState === state) {
            firstStateHit = true;
            return;
        }
        if (firstStateHit && transition.toState == nextState) {
            licenseClient.getLicenseInfo((error, result) => {
                callback(error, result);
            });
        }
    });
}
describe('LicenseClient', function () {
    var sandbox;
    var testLicenseData = fs.readFileSync(path.join(__dirname, 'testLicenses/ValidLicense.ulf'));
    var testLicenseDataWithStopDate = fs.readFileSync(path.join(__dirname, 'testLicenses/ValidLicense.withstopdate.ulf'));
    var binding1 = 'F87CA0D8-038B-5215-94A8-912E1C376AE2';
    var binding2 = 'C02PW2FJG8WQ';
    var timeBinding1 = 'F87CA0D8-038B-5215-94A8-912E1C376AE2';
    var timeBinding2 = 'C02PW2FJG8WQ';
    var invalidBinding1 = 'F87CA0D8-038B-5215-94A8-912E1C376AE3';
    beforeEach(function () {
        sandbox = sinon.sandbox.create();
        licenseClient.setFakeLicense(false);
        if (licenseCore.verifyLicense.restore !== undefined) {
            licenseCore.verifyLicense.restore();
        }
        sandbox.stub(fs, 'writeFileSync').returns();
        sandbox.stub(fs, 'unlinkSync').returns();
        sandbox.stub(fs, 'renameSync').returns();
        sandbox.stub(Date, 'now').returns();
        sandbox.stub(fs, 'writeFile').callsFake((p1, p2, callback) => { callback(); });
    });
    afterEach(function () {
        licenseServiceFsm.off('transition');
        sandbox.restore();
    });
    function stubMachingBindings(b1, b2) {
        sandbox
            .stub(machineService, 'getAllBindings')
            .returns(Promise.resolve({ 1: b1, 2: b2 }));
        if (os.platform() === 'win32') {
            licenseCore.machineBindings = { 1: b1, 2: b2, 4: '' };
        }
        else {
            licenseCore.machineBindings = { 1: b1, 2: b2 };
        }
    }
    function reset() {
        licenseServiceFsm.reset();
        licenseClient.reset();
    }
    describe('Unity Hub License Service', function () {
        beforeEach(() => {
            stubMachingBindings(timeBinding1, timeBinding2);
            reset();
        });
        it('basic test with no license', function (done) {
            UnityAuth.userInfo.valid = true;
            tokenManager.accessToken.value = '1';
            sandbox
                .stub(licenseCore, 'initLicense')
                .returns(Promise.resolve('<?xml version="1.0" encoding="UTF-8"?>\n<Transaction><Survey><Answered>true</Answered></Survey></Transaction>'));
            sandbox
                .stub(fs, 'readFile').withArgs(licenseCore.licenseFile)
                .yields(undefined, '');
            WaitFSMToState('licenseNew', (error, result) => {
                var licenseInfo = JSON.parse(result);
                expect(licenseInfo.initialized).to.equal(true);
                done();
            });
        });
        it.skip('basic test with valid license ', function (done) {
            sandbox
                .stub(fs, 'readFile').withArgs(licenseCore.licenseFile)
                .yields(undefined, testLicenseDataWithStopDate);
            WaitFSMToState('licenseValid', (error, result) => {
                var licenseInfo = JSON.parse(result);
                expect(licenseInfo.initialized).to.equal(true);
                expect(licenseInfo.activated).to.equal(true);
                done();
            });
            reset();
        });
        it('basic test with valid license and invalid xml time ', function (done) {
            sandbox
                .stub(fs, 'readFile').withArgs(licenseCore.licenseFile)
                .yields(undefined, testLicenseDataWithStopDate);
            sandbox
                .stub(licenseHelper, "decrypt")
                .returns("0");
            WaitFSMToState('licenseInvalid', (error, result) => {
                var licenseInfo = JSON.parse(result);
                expect(licenseInfo.initialized).to.equal(true);
                done();
            });
        });
    });
    describe('loadLicense', function () {
        beforeEach(function () {
            sandbox
                .stub(dialog, 'showOpenDialog')
                .returns(['test.ulf']);
            stubMachingBindings(timeBinding1, timeBinding2);
            reset();
        });
        it('should failed to load empty license file', function (done) {
            sandbox
                .stub(fs, 'readFile').withArgs(licenseCore.licenseFile)
                .yields(undefined, '');
            licenseClient.loadLicense(function (err, res) {
                licenseClient.getLicenseInfo(function (error, result) {
                    var licenseInfo = JSON.parse(result);
                    expect(licenseInfo.activated).to.equal(false);
                    done();
                });
            });
        });
        it.skip('should failed to load a license file with invalid machine binding', function (done) {
            sandbox
                .stub(fs, 'readFile').withArgs(licenseCore.licenseFile)
                .yields(undefined, testLicenseData);
            WaitFSMToState('licenseInvalid', (error, result) => {
                var licenseInfo = JSON.parse(result);
                expect(licenseInfo.initialized).to.equal(true);
                done();
            });
        });
        it.skip('should able to load a valid license file', function (done) {
            sandbox
                .stub(fs, 'readFile').withArgs(licenseCore.licenseFile)
                .yields(undefined, '')
                .withArgs('test.ulf')
                .yields(undefined, testLicenseData);
            WaitFSMToState('initializing', (error, result) => {
                var licenseInfo = JSON.parse(result);
                done();
            });
            reset();
            licenseServiceFsm.state = 'licenseInvalid';
            licenseClient.loadLicense(function (err, res) {
                expect(res).to.equal(true);
            });
        });
    });
    describe('saveLicense', function () {
        beforeEach(function () {
            sandbox
                .stub(dialog, 'showSaveDialog')
                .returns('test.ulf');
        });
        it('license activation file should be saved', function (done) {
            stubMachingBindings(binding1, binding1);
            licenseClient.saveLicense(function (err, result) {
                expect(result).to.equal(true);
                done();
            });
        });
        it('license activation file should return false if there is IO error', function (done) {
            var err = new Error('ENOENT');
            err.code = 'ENOENT';
            if (fs.writeFile.restore !== undefined) {
                fs.writeFile.restore();
            }
            sandbox
                .stub(fs, 'writeFile').callsFake((p1, p2, callback) => {
                callback(err);
            })
                .withArgs(licenseCore.licenseFile);
            stubMachingBindings(binding1, binding1);
            licenseClient.saveLicense(function (err, result) {
                expect(result).to.equal(false);
                done();
            });
        });
        it('license activation file should not be saved when user cannceled dialog', function (done) {
            dialog.showSaveDialog.restore();
            sandbox
                .stub(dialog, 'showSaveDialog')
                .returns(undefined);
            licenseClient.saveLicense(function (err, result) {
                expect(result).to.equal(false);
                done();
            });
        });
    });
    describe('activateNewLicense', function () {
        beforeEach(function () {
            sandbox
                .stub(fs, 'statSync')
                .returns(undefined);
        });
        it('should in initialized status after receive 200 response', function (done) {
            sandbox
                .stub(request, 'post', function () {
                done();
                return Promise.resolve('<?xml version="1.0" encoding="UTF-8"?>\n<Transaction><Survey><Answered>true</Answered></Survey></Transaction>');
            });
            licenseServiceFsm.state = 'licenseInvalid';
            licenseClient.activateNewLicense(function (error, result) { });
        });
        it('should in initialized status after receive 200 response (activateNewLicense)', function (done) {
            sandbox
                .stub(request, 'post', function () {
                done();
                return Promise.resolve('<?xml version="1.0" encoding="UTF-8"?>\n<Transaction><Survey><Answered>true</Answered></Survey></Transaction>');
            });
            licenseServiceFsm.state = 'licenseInvalid';
            licenseClient.activateNewLicense(function (error, result) { });
        });
        it('should in error status after receive 400 error', function (done) {
            sandbox
                .stub(licenseCore, 'initLicense')
                .returns(Promise.reject({ statusCode: 400 }));
            WaitFSMToMultlipleState('licenseNew', 'licenseInvalid', (error, result) => {
                done();
            });
            licenseServiceFsm.state = 'licenseInvalid';
            licenseClient.activateNewLicense();
        });
        it('should in maintenance status after receive 503 response', function (done) {
            sandbox
                .stub(licenseCore, 'initLicense')
                .returns(Promise.reject({ statusCode: 503 }));
            licenseServiceFsm.on('transition', function (transition) {
                if (transition.toState == 'licenseMaintenance') {
                    done();
                }
            });
            licenseServiceFsm.state = 'licenseInvalid';
            licenseClient.activateNewLicense();
        });
    });
    describe('refreshLicense', function () {
        it('should not update license for empty license file', function (done) {
            var postStub = sandbox
                .stub(request, 'post');
            sandbox
                .stub(fs, 'readFile').withArgs(licenseCore.licenseFile)
                .yields(undefined, '');
            WaitFSMToMultlipleState('licenseUpdate', 'licenseValid', (error, result) => {
                expect(request.post).to.have.been.notCalled;
                done();
            });
            licenseServiceFsm.state = 'licenseValid';
            licenseClient.refreshLicense(function (error, result) { });
        });
        it('should not update license for invalid license file', function (done) {
            var postStub = sandbox
                .stub(request, 'post');
            licenseServiceFsm.state = 'licenseInvalid';
            licenseClient.refreshLicense(function (error, result) { });
            licenseClient.getLicenseInfo(function (error, result) {
                var licenseInfo = JSON.parse(result);
                expect(licenseInfo.returned).to.equal(false);
                expect(licenseServiceFsm.state).to.equal('licenseInvalid');
                done();
            });
        });
        it('should not update license for invalid response data and error should be cleared', function (done) {
            sandbox
                .stub(licenseCore, 'updateLicense')
                .returns(Promise.resolve({ statusCode: 200, body: 'wrong response' }));
            licenseServiceFsm.on('transition', function (transition) {
                if (transition.toState == 'licenseInvalid') {
                    licenseClient.clearErrors(function () {
                        licenseClient.getLicenseInfo(function (err, r) {
                            var licenseInfo1 = JSON.parse(r);
                            expect(licenseInfo1.error).to.equal(false);
                            done();
                        });
                    });
                }
            });
            licenseServiceFsm.state = 'licenseValid';
            licenseClient.refreshLicense();
        });
        it('should update license for valid license file', function (done) {
            stubMachingBindings(binding1, binding1);
            sandbox
                .stub(licenseCore, 'updateLicense')
                .returns(Promise.resolve(`<Transaction> 
    <Survey> 
      <Answered>true</Answered> 
    </Survey> 
    <Rx>9854f2c55e2ebf250e42d5bd0a0c538d0d4f81e6</Rx> 
  </Transaction>`));
            sandbox
                .stub(licenseCore, 'downloadLicense')
                .returns(Promise.resolve(testLicenseData.toString()));
            reset();
            licenseClient.getLicenseInfo(function (error, result) { });
            WaitFSMToMultlipleState('licenseUpdated', 'initializing', (error, result) => {
                expect(fs.unlinkSync).to.have.been.calledOnce;
                done();
            });
            licenseServiceFsm.state = 'licenseValid';
            licenseClient.refreshLicense();
        });
        it.skip('should keep license valid for update license failure', function (done) {
            var postStub = sandbox
                .stub(request, 'post');
            sandbox
                .stub(licenseCore, 'updateLicense')
                .returns(Promise.resolve(`<Transaction> 
    <Survey> 
      <Answered>true</Answered> 
    </Survey> 
    <Rx>9854f2c55e2ebf250e42d5bd0a0c538d0d4f81e6</Rx> 
  </Transaction>`));
            sandbox
                .stub(licenseCore, 'downloadLicense')
                .returns(Promise.reject({ statusCode: 400, response: { body: "" } }));
            WaitFSMToMultlipleState('licenseUpdate', 'licenseValid', (error, result) => {
                expect(fs.unlinkSync).not.to.have.been.calledOnce;
                done();
            });
            reset();
            licenseServiceFsm.state = 'licenseValid';
            licenseClient.refreshLicense();
        });
        it('should delete license for 448 response for update license request', function (done) {
            sandbox
                .stub(licenseCore, 'updateLicense')
                .returns(Promise.reject({ statusCode: 448, response: { body: `` } }));
            var hitInvalid = false;
            WaitFSMToMultlipleState('licenseUpdate', 'licenseInvalid', (error, result) => {
                if (!hitInvalid) {
                    expect(fs.unlinkSync).to.have.been.calledOnce;
                    done();
                    hitInvalid = true;
                }
            });
            reset();
            licenseServiceFsm.state = 'licenseValid';
            licenseClient.refreshLicense();
        });
    });
    describe('returnLicense', function () {
        beforeEach(() => {
            stubMachingBindings(timeBinding1, timeBinding2);
        });
        it('shoudn\'t return if license file is empty license ', function (done) {
            sandbox
                .stub(fs, 'readFileSync')
                .returns('');
            licenseClient.getLicenseInfo(function (error, result) { });
            reset();
            licenseClient.returnLicense(function (error, result) { });
            licenseClient.getLicenseInfo(function (error, result) {
                var licenseInfo = JSON.parse(result);
                expect(licenseInfo.returned).to.equal(false);
                done();
            });
        });
        it('should return license for valid license file', function (done) {
            var postStub = sandbox
                .stub(licenseCore, 'returnLicense')
                .returns(Promise.resolve(`<Transaction> 
    <Survey> 
      <Answered>true</Answered> 
    </Survey> 
    <Rx>9854f2c55e2ebf250e42d5bd0a0c538d0d4f81e6</Rx> 
  </Transaction>`));
            sandbox
                .stub(licenseCore, 'downloadLicense')
                .returns(Promise.resolve({ statusCode: 200, body: testLicenseData.toString() }));
            var hitInvalid = false;
            WaitFSMToState('licenseInvalid', (error, result) => {
                if (!hitInvalid) {
                    expect(fs.unlinkSync).to.have.been.calledOnce;
                    done();
                    hitInvalid = true;
                }
            });
            reset();
            licenseServiceFsm.state = 'licenseValid';
            licenseClient.returnLicense();
        });
        it('shouldn\'t return license when return response is 400', function (done) {
            var postStub = sandbox
                .stub(licenseCore, 'returnLicense')
                .returns(Promise.reject({ statusCode: 400, response: { body: '' } }));
            var hitLicenseReturn = false;
            licenseServiceFsm.on('transition', function (transition) {
                if (transition.toState === 'licenseReturn') {
                    hitLicenseReturn = true;
                }
                if (hitLicenseReturn && transition.toState == 'licenseValid') {
                    expect(fs.unlinkSync).to.have.been.notCalled;
                    done();
                }
            });
            reset();
            licenseServiceFsm.state = 'licenseValid';
            licenseClient.returnLicense();
        });
        it('shouldn\'t return license when download response in return operation is 400', function (done) {
            var postStub = sandbox
                .stub(licenseCore, 'returnLicense')
                .returns(Promise.resolve(`<Transaction> 
    <Survey> 
      <Answered>true</Answered> 
    </Survey> 
    <Rx>9854f2c55e2ebf250e42d5bd0a0c538d0d4f81e6</Rx> 
  </Transaction>`));
            sandbox
                .stub(licenseCore, 'downloadLicense')
                .returns(Promise.reject({ statusCode: 400, body: "" }));
            var hitLicenseReturn = false;
            licenseServiceFsm.on('transition', function (transition) {
                if (transition.toState === 'licenseReturn') {
                    hitLicenseReturn = true;
                }
                if (hitLicenseReturn && transition.toState == 'licenseValid') {
                    expect(fs.unlinkSync).to.have.been.notCalled;
                    done();
                }
            });
            reset();
            licenseServiceFsm.state = 'licenseValid';
            licenseClient.returnLicense();
        });
    });
    describe('setActivationId', function () {
        it.skip('should activate license with valid serial number', function (done) {
            UnityAuth.userInfo.valid = true;
            UnityAuth.userInfo.accessToken = '1';
            sandbox
                .stub(fs, 'readFileSync')
                .returns('');
            sandbox
                .stub(licenseCore, 'initLicense')
                .returns(Promise.resolve('<?xml version="1.0" encoding="UTF-8"?>\n<Transaction><Survey><Answered>true</Answered></Survey></Transaction>'));
            sandbox
                .stub(licenseCore, 'downloadLicense')
                .returns(Promise.resolve(testLicenseData.toString()));
            var activateCalled = false;
            licenseServiceFsm.on('transition', function (transition) {
                if (transition.toState == 'licenseNew') {
                    licenseClient.setActivationId("testRxId", function (error, result) { });
                    activateCalled = true;
                }
                if (activateCalled && transition.toState == 'uninitialized') {
                    expect(fs.writeFileSync).to.have.been.calledOnce;
                    licenseServiceFsm.state = 'licenseInvalid';
                    done();
                    return;
                }
            });
            reset();
            licenseServiceFsm.state = 'licenseInvalid';
            licenseClient.activateNewLicense();
        });
        it('should change to invalid license when initLicense failed', function (done) {
            sandbox
                .stub(licenseCore, 'initLicense')
                .returns(Promise.reject('something wrong'));
            sandbox
                .stub(licenseCore, 'restoreBackupLicenseFile')
                .returns(Promise.reject('something wrong'));
            reset();
            licenseServiceFsm.state = 'licenseInvalid';
            var hit = false;
            WaitFSMToMultlipleState('licenseNew', 'licenseInvalid', (error, result) => {
                if (!hit) {
                    done();
                    hit = true;
                }
            });
            licenseClient.activateNewLicense();
            licenseClient.setActivationId("testRxId", function (error, result) { });
        });
        it('should restore old license with failed activation', function (done) {
            sandbox
                .stub(licenseCore, 'initLicense')
                .returns(Promise.resolve({ statusCode: 200, body: '<?xml version="1.0" encoding="UTF-8"?>\n<Transaction><Survey><Answered>true</Answered></Survey></Transaction>' }));
            sandbox
                .stub(licenseCore, 'downloadLicense')
                .returns(Promise.resolve({ statusCode: 400, body: 'error' }));
            sandbox
                .stub(fs, 'statSync').returns(true);
            sandbox
                .stub(fs, 'existsSync').returns(true);
            sandbox
                .stub(fs, 'rename', (p1, p2, callback) => { callback(null); });
            sandbox
                .stub(licenseCore, 'verifyLicense').returns(Promise.resolve(true));
            licenseServiceFsm.on('transition', function (transition) {
                if (transition.toState == 'licenseValid') {
                    done();
                }
            });
            reset();
            licenseServiceFsm.state = 'licenseValid';
            licenseClient.activateNewLicense();
            licenseClient.setActivationId("testRxId", function (error, result) { });
        });
    });
});
//# sourceMappingURL=licenseClient.ispec.js.map