const EntitlementCache = require('./entitlementCache');
const settings = require('../localSettings/localSettings');
const _ = require('lodash');
const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
let entitlementCache;
describe('EntitlementCache', () => {
    let sandbox;
    let settingsMock;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        settingsMock = {};
        sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);
        entitlementCache = new EntitlementCache();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('configuration', () => {
        it('should obtain cache timeout from settings', () => {
            settingsMock[settings.keys.ENTITLEMENT_CACHE_TIMEOUT_IN_SEC] = 3712;
            expect(entitlementCache._getCacheTimeoutInMs()).to.be.equal(3712 * 1000);
        });
        it('should obtain cache timeout from settings up to max', () => {
            settingsMock[settings.keys.ENTITLEMENT_CACHE_TIMEOUT_IN_SEC] = EntitlementCache.maxCacheTimeoutInSec;
            expect(entitlementCache._getCacheTimeoutInMs()).to.be.equal(EntitlementCache.maxCacheTimeoutInSec * 1000);
        });
        it('should use default timeout when setting not found', () => {
            expect(settingsMock[settings.keys.ENTITLEMENT_CACHE_TIMEOUT_IN_SEC]).to.be.undefined;
            expect(entitlementCache._getCacheTimeoutInMs()).to.be.equal(EntitlementCache.defaultCacheTimeoutInSec * 1000);
        });
        it('should use default timeout when setting invalid', () => {
            settingsMock[settings.keys.ENTITLEMENT_CACHE_TIMEOUT_IN_SEC] = 'invalid';
            expect(entitlementCache._getCacheTimeoutInMs()).to.be.equal(EntitlementCache.defaultCacheTimeoutInSec * 1000);
        });
        it('should use max timeout when setting over max', () => {
            settingsMock[settings.keys.ENTITLEMENT_CACHE_TIMEOUT_IN_SEC] = EntitlementCache.maxCacheTimeoutInSec + 1;
            expect(entitlementCache._getCacheTimeoutInMs()).to.be.equal(EntitlementCache.maxCacheTimeoutInSec * 1000);
        });
        it('should use min timeout when setting below min', () => {
            settingsMock[settings.keys.ENTITLEMENT_CACHE_TIMEOUT_IN_SEC] = -10;
            expect(entitlementCache._getCacheTimeoutInMs()).to.be.equal(EntitlementCache.minCacheTimeoutInSec * 1000);
        });
    });
    describe('cacheResponse', () => {
        it('should update cache when empty', () => {
            const now = _.now();
            sandbox.stub(_, 'now').returns(now);
            entitlementCache.cacheResponse(['com.entitlement1', 'com.entitlement2', 'com.entitlement3'], {
                entitlements: {
                    'com.entitlement1': '...',
                    'com.entitlement2': '...'
                }
            });
            expect(entitlementCache._cachedEntitlements).to.deep.equal({
                'com.entitlement1': { entitlement: 'com.entitlement1', hasEntitlement: true, entitlementGroupId: '...', responseTimestamp: now },
                'com.entitlement2': { entitlement: 'com.entitlement2', hasEntitlement: true, entitlementGroupId: '...', responseTimestamp: now },
                'com.entitlement3': { entitlement: 'com.entitlement3', hasEntitlement: false, entitlementGroupId: '', responseTimestamp: now }
            });
        });
        it('should update cache with new data when not empty', () => {
            const now = _.now();
            const pastNow = now - 3712;
            sandbox.stub(_, 'now').returns(now);
            entitlementCache._cachedEntitlements = {
                'com.entitlement1': { entitlement: 'com.entitlement1', hasEntitlement: true, entitlementGroupId: '...', responseTimestamp: pastNow },
                'com.entitlement2': { entitlement: 'com.entitlement2', hasEntitlement: true, entitlementGroupId: '...', responseTimestamp: pastNow }
            };
            hasEntitlement: true,
                entitlementCache.cacheResponse(['com.entitlement2', 'com.entitlement3'], {
                    entitlements: {
                        'com.entitlement2': '...',
                        'com.entitlement3': '...'
                    }
                });
            expect(entitlementCache._cachedEntitlements).to.deep.equal({
                'com.entitlement1': { entitlement: 'com.entitlement1', hasEntitlement: true, entitlementGroupId: '...', responseTimestamp: pastNow },
                'com.entitlement2': { entitlement: 'com.entitlement2', hasEntitlement: true, entitlementGroupId: '...', responseTimestamp: now },
                'com.entitlement3': { entitlement: 'com.entitlement3', hasEntitlement: true, entitlementGroupId: '...', responseTimestamp: now },
            });
        });
    });
    describe('getCachedResponse', () => {
        it('should return null when cache is empty', () => {
            const cachedResponse = entitlementCache.getCachedResponseForEntitlements(['com.entitlement1', 'com.entitlement2']);
            expect(cachedResponse).to.be.null;
        });
        it('should return cached response when all required entitlement are in', () => {
            const now = _.now();
            entitlementCache._cachedEntitlements = {
                'com.entitlement1': { entitlement: 'com.entitlement1', hasEntitlement: true, entitlementGroupId: '...', responseTimestamp: now },
                'com.entitlement2': { entitlement: 'com.entitlement2', hasEntitlement: true, entitlementGroupId: '...', responseTimestamp: now }
            };
            const cachedResponse = entitlementCache.getCachedResponseForEntitlements(['com.entitlement1', 'com.entitlement2']);
            expect(cachedResponse).to.be.deep.equal({
                entitlements: {
                    'com.entitlement1': '...',
                    'com.entitlement2': '...'
                }
            });
        });
        it('should return null when any required entitlement is missing', () => {
            const now = _.now();
            entitlementCache._cachedEntitlements = {
                'com.entitlement1': { entitlement: 'com.entitlement1', entitlementGroupId: '...', responseTimestamp: now }
            };
            const cachedResponse = entitlementCache.getCachedResponseForEntitlements(['com.entitlement1', 'com.entitlement2']);
            expect(cachedResponse).to.be.null;
        });
        it('should return null when any required entitlement is outdated', () => {
            const now = _.now();
            entitlementCache._cachedEntitlements = {
                'com.entitlement1': { entitlement: 'com.entitlement1', entitlementGroupId: '...', responseTimestamp: now },
                'com.entitlement2': { entitlement: 'com.entitlement2', entitlementGroupId: '...', responseTimestamp: now - entitlementCache._maxCachedDurationInMs - 1 }
            };
            const cachedResponse = entitlementCache.getCachedResponseForEntitlements(['com.entitlement1', 'com.entitlement2']);
            expect(cachedResponse).to.be.null;
        });
    });
});
//# sourceMappingURL=entitlementCache.spec.js.map