'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const electron = require('electron');
const licenseClient = require('./licenseClient');
const IpcLicenseClient = require('./ipcLicenseClient');
const settings = require('../localSettings/localSettings');
class LicenseClientProxy {
    get fileBaseClient() { return licenseClient; }
    get ipcClient() {
        if (typeof this._ipcLicenseClient === 'undefined') {
            const discardHandshakeAndEntitlementRequests = true;
            this._ipcLicenseClient = new IpcLicenseClient(discardHandshakeAndEntitlementRequests);
        }
        return this._ipcLicenseClient;
    }
    get licenseKinds() { return this._getLicenseClient().licenseKinds; }
    init(options) {
        return this._getLicenseClient().init(options);
    }
    setActivationId(rx, callback) {
        this._getLicenseClient().setActivationId(rx, callback);
    }
    reset() {
        this._getLicenseClient().reset();
    }
    isLicenseValid() {
        return this._getLicenseClient().isLicenseValid();
    }
    getLicenseInfo(callback) {
        return this._getLicenseClient().getLicenseInfo(callback);
    }
    createPersonalLicense() {
        return __awaiter(this, void 0, void 0, function* () {
            return this._getLicenseClient().createPersonalLicense();
        });
    }
    activateNewLicense(callback) {
        this._getLicenseClient().activateNewLicense(callback);
    }
    resetLicenseState() {
        this._getLicenseClient().resetLicenseState();
    }
    returnLicense(callback) {
        this._getLicenseClient().returnLicense(callback);
    }
    loadLicenseLegacy() {
        return this._getLicenseClient().loadLicenseLegacy();
    }
    chooseLicenseFileToLoad() {
        return this._getLicenseClient().chooseLicenseFileToLoad();
    }
    loadLicense(fileNames) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._getLicenseClient().loadLicense(fileNames);
        });
    }
    saveLicense() {
        return this._getLicenseClient().saveLicense();
    }
    refreshLicense(callback) {
        return this._getLicenseClient().refreshLicense(callback);
    }
    validateSerialNumber(serialNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._getLicenseClient().validateSerialNumber(serialNumber);
        });
    }
    submitLicense(serialNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._getLicenseClient().submitLicense(serialNumber);
        });
    }
    clearErrors(callback) {
        this._getLicenseClient().clearErrors(callback);
    }
    isLicenseEntitlementEnabled() {
        return (settings.get(settings.keys.ENABLE_ENTITLEMENT_LICENSING));
    }
    _getLicenseClient() {
        if (!this.isLicenseEntitlementEnabled()) {
            return this.fileBaseClient;
        }
        return this.ipcClient;
    }
}
module.exports = new LicenseClientProxy();
//# sourceMappingURL=licenseClientProxy.js.map