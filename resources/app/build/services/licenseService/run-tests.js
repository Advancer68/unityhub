var Mocha = require('mocha');
var mocha = new Mocha();
var testDir = './';
Mocha.utils.lookupFiles(testDir, ['spec.js'], true).forEach(mocha.addFile.bind(mocha));
mocha.run(process.exit);
//# sourceMappingURL=run-tests.js.map