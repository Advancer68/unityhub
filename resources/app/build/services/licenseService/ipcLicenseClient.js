'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const licenseCore = require('./licenseCore');
const LicenseClientBase = require('./licenseClientBase');
const IpcMessenger = require('./ipcLicenseClient/ipcMessenger');
const EntitlementCache = require('./entitlementCache');
const logger = require('../../logger')('IpcLicenseClient');
const os = require('os');
const _ = require('lodash');
const LaunchProcess = require('../editorApp/launchprocess');
const settings = require('../localSettings/localSettings');
const postal = require('postal');
const tokenManager = require('../../tokenManager/tokenManager');
const { fs } = require('../../fileSystem');
const util = require('util');
const fsAccess = util.promisify(fs.access);
const Entitlements = {
    UseEditor: 'com.unity.editor',
    UseLegacyProFlag: 'com.unity.editor.legacy.pro',
    UseEduWatermark: 'com.unity.editor.watermarks.edu',
    UseTrialWatermark: 'com.unity.editor.watermarks.trial'
};
class IpcLicenseClient extends LicenseClientBase {
    constructor(discardHandshakeAndEntitlementRequests) {
        super();
        this._eventsMap = new Map([['userInfo.changed', this.sendAccessToken.bind(this)]]);
        this._discardHandshakeAndEntitlementRequests = discardHandshakeAndEntitlementRequests;
        this._messenger = new IpcMessenger(discardHandshakeAndEntitlementRequests);
        this._entitlementCache = new EntitlementCache();
        this._licenseInfo = LicenseClientBase.getDefaultLicenseInfo();
        this._messenger.on('connectionLost', () => {
            if (this._initializing === true) {
                return;
            }
            this._onMessengerConnectionLost(IpcMessenger.errorKinds.endpointDisconnected);
        });
    }
    init({ useExtendedTimeout = false, pipeName = this._getLicenseClientPipeName() } = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            this._initializing = true;
            this._registerEvents();
            try {
                yield this._messenger.init(pipeName, useExtendedTimeout);
                yield this.sendAccessToken();
                this._initializing = false;
                return this.getLicenseInfo();
            }
            catch (e) {
                return this._onMessengerConnectionLost(e);
            }
        });
    }
    _registerEvents() {
        for (const [eventName, handler] of this._eventsMap) {
            postal.subscribe({
                channel: 'app',
                topic: eventName,
                callback: handler.bind(this)
            });
        }
    }
    _onMessengerConnectionLost(error) {
        return __awaiter(this, void 0, void 0, function* () {
            let pipeName = null;
            if (error === IpcMessenger.errorKinds.endpointDisconnected) {
                pipeName = this._getLicenseClientPipeName();
            }
            else if (error === IpcMessenger.errorKinds.handshakeFailed || error === IpcMessenger.errorKinds.endpointPermissionDenied) {
                pipeName = this._getLicenseClientPipeName('Hub');
            }
            if (pipeName == null) {
                return;
            }
            const clientPath = this._getLicenseClientApplicationPath();
            if (!clientPath) {
                logger.error(`Missing config value for ${settings.keys.LICENSE_CLIENT_APPLICATION_PATH}`);
                return;
            }
            try {
                yield fsAccess(clientPath, fs.constants.X_OK);
            }
            catch (err) {
                logger.error(`Unable to access licensing client (${clientPath}): ${err}. Please validate ${settings.keys.LICENSE_CLIENT_APPLICATION_PATH} config`);
                return;
            }
            try {
                yield LaunchProcess.start(clientPath, ['--namedPipe', pipeName]);
                yield this.init({ useExtendedTimeout: true, pipeName });
            }
            catch (launchErr) {
                logger.error(`An error occurred while starting the licensing client (${clientPath}): ${launchErr}`);
            }
        });
    }
    setActivationId() {
        logger.info('setActivationId called but not supported by the current implementation of ipc license client');
    }
    reset() {
        this._licenseInfo = LicenseClientBase.getDefaultLicenseInfo();
    }
    isLicenseValid() {
        if (settings.get(settings.keys.globalMachineSettings.LICENSING_SERVICE_BASE_URL))
            return true;
        return this._licenseInfo.activated;
    }
    getLicenseInfo(callback) {
        if (this._discardHandshakeAndEntitlementRequests) {
            this._licenseInfo = this._messenger.isConnected ? IpcLicenseClient.getValidLicenseInfo() : LicenseClientBase.getDefaultLicenseInfo();
            if (callback !== undefined) {
                callback(undefined, JSON.stringify(this._licenseInfo));
            }
            return Promise.resolve(JSON.stringify(this._licenseInfo));
        }
        const entitlementsRequest = {
            messageType: 'EntitlementsRequest',
            entitlements: [
                Entitlements.UseEditor,
                Entitlements.UseLegacyProFlag,
                Entitlements.UseEduWatermark,
                Entitlements.UseTrialWatermark
            ]
        };
        const cachedResponse = this._entitlementCache.getCachedResponseForEntitlements(entitlementsRequest.entitlements);
        if (cachedResponse !== null) {
            this._updateLicenseInfo(cachedResponse);
            if (callback !== undefined) {
                callback(undefined, JSON.stringify(this._licenseInfo));
            }
            return Promise.resolve(JSON.stringify(this._licenseInfo));
        }
        return this._messenger.sendMessage(entitlementsRequest).then((response) => {
            if (response instanceof Error) {
                this._licenseInfo = LicenseClientBase.getDefaultLicenseInfo();
            }
            else {
                this._entitlementCache.cacheResponse(entitlementsRequest.entitlements, response);
                this._updateLicenseInfo(response);
            }
            if (callback !== undefined) {
                callback(undefined, JSON.stringify(this._licenseInfo));
            }
            return Promise.resolve(JSON.stringify(this._licenseInfo));
        });
    }
    createPersonalLicense() {
        return __awaiter(this, void 0, void 0, function* () {
            logger.info('createPersonalLicense called but not supported by the current implementation of ipc license client');
        });
    }
    sendAccessToken() {
        return this._messenger.sendMessage({
            messageType: 'AccessTokenRequest',
            value: tokenManager.accessToken.value,
            expiration: tokenManager.accessToken.expiration
        });
    }
    activateNewLicense() {
        logger.info('activateNewLicense called but not supported by the current implementation of ipc license client');
    }
    resetLicenseState() {
        logger.info('resetLicenseState called but not supported by the current implementation of ipc license client');
    }
    returnLicense() {
        logger.info('returnLicense called but not supported by the current implementation of ipc license client');
    }
    loadLicenseLegacy() {
        logger.info('loadLicenseLegacy called but not supported by the current implementation of ipc license client');
    }
    chooseLicenseFileToLoad() {
        logger.info('chooseLicenseFileToLoad called but not supported by the current implementation of ipc license client');
    }
    loadLicense(fileNames) {
        return __awaiter(this, void 0, void 0, function* () {
            logger.info('loadLicense called but not supported by the current implementation of ipc license client', fileNames);
        });
    }
    saveLicense() {
        logger.info('saveLicense called but not supported by the current implementation of ipc license client');
    }
    refreshLicense() {
        logger.info('refreshLicense called but not supported by the current implementation of ipc license client');
    }
    clearErrors() {
        logger.info('clearErrors called but not supported by the current implementation of ipc license client');
    }
    _updateLicenseInfo(response) {
        this._licenseInfo.activated = this._hasEntitlement(response, Entitlements.UseEditor);
        this._licenseInfo.valid = true;
        this._licenseInfo.initialized = true;
        this._licenseInfo.flow = this._findHighestKindAvailable(response);
        this._licenseInfo.offlineDisabled = false;
        this._licenseInfo.transactionId = '';
    }
    _findHighestKindAvailable(entitlementResponse) {
        if (this._hasEntitlement(entitlementResponse, Entitlements.UseLegacyProFlag)) {
            return licenseCore.licenseKinds.PRO;
        }
        if (this._hasEntitlement(entitlementResponse, Entitlements.UseEduWatermark)) {
            return licenseCore.licenseKinds.EDU;
        }
        if (this._hasEntitlement(entitlementResponse, Entitlements.UseEditor)) {
            return licenseCore.licenseKinds.PERSONAL;
        }
        if (this._hasEntitlement(entitlementResponse, Entitlements.UseTrialWatermark)) {
            return licenseCore.licenseKinds.TRIAL;
        }
        return licenseCore.licenseKinds.UNKNOWN;
    }
    _hasEntitlement(entitlementResponse, entitlement) {
        return _.has(entitlementResponse.entitlements, entitlement);
    }
    _getLicenseClientPipeName(suffix) {
        const useLegacyPipeName = settings.get(settings.keys.USE_LEGACY_LICENSING_CLIENT_PIPE_NAME);
        const pipeName = useLegacyPipeName === true ? 'Unity-LicenseClient' : `Unity-LicenseClient-${os.userInfo().username}`;
        return _.isEmpty(suffix) ? pipeName : `${pipeName}-${suffix}`;
    }
    _getLicenseClientApplicationPath() {
        return settings.get(settings.keys.LICENSE_CLIENT_APPLICATION_PATH);
    }
    static getValidLicenseInfo() {
        const licenseInfo = LicenseClientBase.getDefaultLicenseInfo();
        licenseInfo.activated = true;
        licenseInfo.valid = true;
        licenseInfo.initialized = true;
        licenseInfo.flow = licenseCore.licenseKinds.PRO;
        licenseInfo.offlineDisabled = false;
        licenseInfo.transactionId = '';
        return licenseInfo;
    }
}
module.exports = IpcLicenseClient;
//# sourceMappingURL=ipcLicenseClient.js.map