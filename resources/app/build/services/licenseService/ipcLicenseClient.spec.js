var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const os = require('os');
const licenseCore = require('./licenseCore');
const settings = require('../localSettings/localSettings');
const LicenseClientBase = require('./licenseClientBase');
const logger = { error() { } };
const loggerStub = () => {
    return logger;
};
const tokenManagerStub = {
    accessToken: {
        value: '1234',
        expiration: 1234
    }
};
const proxyquire = require('proxyquire').noCallThru();
const IpcLicenseClient = proxyquire('./ipcLicenseClient', {
    '../../logger': loggerStub,
    '../../tokenManager/tokenManager': tokenManagerStub
});
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
chai.use(chaiAsPromised);
chai.use(require('chai-string'));
let ipcLicenseClient;
describe('IpcLicenseClient', () => {
    let sandbox;
    let settingsMock;
    let entitlementCacheResponseStub;
    let osUserName;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        settingsMock = {};
        sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);
        osUserName = 'JohnDoe';
        sandbox.stub(os, 'userInfo').returns({ username: osUserName });
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('when discarding handshake and entitlement requests', () => {
        beforeEach(() => {
            ipcLicenseClient = new IpcLicenseClient(true);
            entitlementCacheResponseStub = sandbox.stub(ipcLicenseClient._entitlementCache, 'getCachedResponseForEntitlements').returns(null);
        });
        describe('constructor', () => {
            it('should instantiate messenger with right parameter', () => {
                expect(ipcLicenseClient._messenger._discardHandshake).to.be.true;
            });
        });
        describe('getValidLicenseInfo', () => {
            it('should be a valid license info', function () {
                const validLicenseInfo = IpcLicenseClient.getValidLicenseInfo();
                const expectedLicenseInfo = LicenseClientBase.getDefaultLicenseInfo();
                expectedLicenseInfo.activated = true;
                expectedLicenseInfo.valid = true;
                expectedLicenseInfo.initialized = true;
                expectedLicenseInfo.flow = licenseCore.licenseKinds.PRO;
                expectedLicenseInfo.offlineDisabled = false;
                expectedLicenseInfo.transactionId = '';
                expect(validLicenseInfo).to.be.deep.equal(expectedLicenseInfo);
            });
        });
        describe('getLicenseInfo', () => {
            it('should not send entitlement request to messenger', () => __awaiter(this, void 0, void 0, function* () {
                const sendMessageStub = sandbox.stub(ipcLicenseClient._messenger, 'sendMessage').returns(Promise.resolve({}));
                yield ipcLicenseClient.getLicenseInfo();
                expect(sendMessageStub.called).to.be.false;
            }));
            it('should invoke callback and be resolved', () => __awaiter(this, void 0, void 0, function* () {
                const callback = sandbox.spy();
                const getLicenseInfoPromise = ipcLicenseClient.getLicenseInfo(callback);
                yield getLicenseInfoPromise;
                expect(callback.called).to.be.true;
                expect(getLicenseInfoPromise).to.be.resolved;
            }));
            describe('when messenger is not connected', () => {
                beforeEach(() => {
                    ipcLicenseClient._messenger._connected = false;
                });
                it('should return default license info', () => __awaiter(this, void 0, void 0, function* () {
                    const callback = sandbox.spy();
                    yield ipcLicenseClient.getLicenseInfo(callback);
                    expect(callback.called).to.be.true;
                    const licenseInfo = JSON.parse(callback.getCall(0).args[1]);
                    expect(licenseInfo).to.deep.equal(LicenseClientBase.getDefaultLicenseInfo());
                }));
            });
            describe('when messenger is connected', () => {
                beforeEach(() => {
                    ipcLicenseClient._messenger._connected = true;
                });
                it('should return valid license info', () => __awaiter(this, void 0, void 0, function* () {
                    const callback = sandbox.spy();
                    yield ipcLicenseClient.getLicenseInfo(callback);
                    expect(callback.called).to.be.true;
                    const licenseInfo = JSON.parse(callback.getCall(0).args[1]);
                    expect(licenseInfo).to.deep.equal(IpcLicenseClient.getValidLicenseInfo());
                }));
            });
        });
    });
    describe('when not discarding handshake and entitlement requests', () => {
        beforeEach(() => {
            ipcLicenseClient = new IpcLicenseClient();
            entitlementCacheResponseStub = sandbox.stub(ipcLicenseClient._entitlementCache, 'getCachedResponseForEntitlements').returns(null);
            sandbox.stub(ipcLicenseClient, 'sendAccessToken');
        });
        describe('init', () => {
            beforeEach(() => {
                sandbox.stub(ipcLicenseClient, 'getLicenseInfo').returns(Promise.resolve());
                sandbox.stub(ipcLicenseClient._messenger, 'init').returns(Promise.resolve());
            });
            it('should call init on messenger', () => __awaiter(this, void 0, void 0, function* () {
                yield ipcLicenseClient.init();
                expect(ipcLicenseClient._messenger.init).to.have.been.calledWith(`Unity-LicenseClient-${osUserName}`);
            }));
            it('should call getLicenseInfo when init is complete', () => __awaiter(this, void 0, void 0, function* () {
                yield ipcLicenseClient.init();
                expect(ipcLicenseClient.getLicenseInfo).to.have.been.called;
            }));
            it('should send access token', () => __awaiter(this, void 0, void 0, function* () {
                yield ipcLicenseClient.init();
                expect(ipcLicenseClient.sendAccessToken).to.have.been.called;
            }));
        });
        describe('licenseKinds', () => {
            it('should contain all kinds of licenses', () => {
                expect(ipcLicenseClient.licenseKinds).to.deep.equal({
                    PRO: 'Unity Pro',
                    PLUS: 'Unity Plus',
                    EDU: 'Unity Edu',
                    PERSONAL: 'Unity Personal',
                    TRIAL: 'Unity Trial',
                    UNKNOWN: 'Unity'
                });
            });
        });
        describe('getLicenseInfo', () => {
            it('should send entitlement request to messenger', () => __awaiter(this, void 0, void 0, function* () {
                const sendMessageStub = sandbox.stub(ipcLicenseClient._messenger, 'sendMessage').returns(Promise.resolve({}));
                yield ipcLicenseClient.getLicenseInfo();
                expect(sendMessageStub.called).to.be.true;
                expect(sendMessageStub.getCall(0).args[0]).to.deep.equal({
                    messageType: 'EntitlementsRequest',
                    entitlements: ['com.unity.editor', 'com.unity.editor.legacy.pro', 'com.unity.editor.watermarks.edu', 'com.unity.editor.watermarks.trial']
                });
            }));
            it('should call callback with license info as json string', () => __awaiter(this, void 0, void 0, function* () {
                const callback = sandbox.spy();
                sandbox.stub(ipcLicenseClient._messenger, 'sendMessage').returns(Promise.resolve({}));
                yield ipcLicenseClient.getLicenseInfo(callback);
                expect(callback.called).to.be.true;
                expect(callback.getCall(0).args[0]).to.be.undefined;
                let licenseInfo = JSON.parse(callback.getCall(0).args[1]);
                let expectedLicenseInfo = LicenseClientBase.getDefaultLicenseInfo();
                expectedLicenseInfo.flow = ipcLicenseClient.licenseKinds.UNKNOWN;
                expectedLicenseInfo.initialized = true;
                expectedLicenseInfo.valid = true;
                expect(licenseInfo).to.deep.equal(expectedLicenseInfo);
            }));
            describe('when entitlements found in cache', () => {
                it('should call callback with license info as json string', () => __awaiter(this, void 0, void 0, function* () {
                    entitlementCacheResponseStub.returns({});
                    const callback = sandbox.spy();
                    sandbox.stub(ipcLicenseClient._messenger, 'sendMessage').returns(Promise.resolve({}));
                    yield ipcLicenseClient.getLicenseInfo(callback);
                    expect(callback.called).to.be.true;
                    expect(callback.getCall(0).args[0]).to.be.undefined;
                    let licenseInfo = JSON.parse(callback.getCall(0).args[1]);
                    let expectedLicenseInfo = LicenseClientBase.getDefaultLicenseInfo();
                    expectedLicenseInfo.flow = ipcLicenseClient.licenseKinds.UNKNOWN;
                    expectedLicenseInfo.initialized = true;
                    expectedLicenseInfo.valid = true;
                    expect(licenseInfo).to.deep.equal(expectedLicenseInfo);
                }));
            });
            it('should call callback even if request failed but with default license info as json string', () => __awaiter(this, void 0, void 0, function* () {
                const callback = sandbox.spy();
                sandbox.stub(ipcLicenseClient._messenger, 'sendMessage').returns(Promise.resolve(new Error('huho')));
                yield ipcLicenseClient.getLicenseInfo(callback);
                expect(callback.called).to.be.true;
                expect(callback.getCall(0).args[0]).to.be.undefined;
                let licenseInfo = JSON.parse(callback.getCall(0).args[1]);
                let expectedLicenseInfo = LicenseClientBase.getDefaultLicenseInfo();
                expect(licenseInfo).to.deep.equal(expectedLicenseInfo);
            }));
            it('should call callback with full featured license info as json string', () => __awaiter(this, void 0, void 0, function* () {
                const callback = sandbox.spy();
                sandbox.stub(ipcLicenseClient._messenger, 'sendMessage').returns(Promise.resolve({
                    entitlements: {
                        'com.unity.editor': '...',
                        'com.unity.editor.legacy.pro': '...',
                        'com.unity.editor.watermarks.edu': '...',
                        'com.unity.editor.watermarks.trial': '...'
                    }
                }));
                yield ipcLicenseClient.getLicenseInfo(callback);
                expect(callback.called).to.be.true;
                expect(callback.getCall(0).args[0]).to.be.undefined;
                let licenseInfo = JSON.parse(callback.getCall(0).args[1]);
                let expectedLicenseInfo = LicenseClientBase.getDefaultLicenseInfo();
                expectedLicenseInfo.activated = true;
                expectedLicenseInfo.initialized = true;
                expectedLicenseInfo.valid = true;
                expectedLicenseInfo.flow = ipcLicenseClient.licenseKinds.PRO;
                expectedLicenseInfo.offlineDisabled = false;
                expectedLicenseInfo.transactionId = '';
                expect(licenseInfo).to.deep.equal(expectedLicenseInfo);
            }));
            describe('flow', () => {
                it('should be PRO when having at least entitlement com.unity.editor.legacy.pro', () => __awaiter(this, void 0, void 0, function* () {
                    const callback = sandbox.spy();
                    sandbox.stub(ipcLicenseClient._messenger, 'sendMessage').returns(Promise.resolve({
                        entitlements: {
                            'com.unity.editor.legacy.pro': '...',
                            'com.unity.editor.watermarks.edu': '...',
                            'com.unity.editor': '...',
                            'com.unity.editor.watermarks.trial': '...'
                        }
                    }));
                    yield ipcLicenseClient.getLicenseInfo(callback);
                    expect(callback.called).to.be.true;
                    expect(callback.getCall(0).args[0]).to.be.undefined;
                    let licenseInfo = JSON.parse(callback.getCall(0).args[1]);
                    expect(licenseInfo.flow).to.equal(ipcLicenseClient.licenseKinds.PRO);
                }));
                it('should be EDU when having at least entitlement com.unity.editor.watermarks.edu', () => __awaiter(this, void 0, void 0, function* () {
                    const callback = sandbox.spy();
                    sandbox.stub(ipcLicenseClient._messenger, 'sendMessage').returns(Promise.resolve({
                        entitlements: {
                            'com.unity.editor.watermarks.edu': '...',
                            'com.unity.editor': '...',
                            'com.unity.editor.watermarks.trial': '...'
                        }
                    }));
                    yield ipcLicenseClient.getLicenseInfo(callback);
                    expect(callback.called).to.be.true;
                    expect(callback.getCall(0).args[0]).to.be.undefined;
                    let licenseInfo = JSON.parse(callback.getCall(0).args[1]);
                    expect(licenseInfo.flow).to.equal(ipcLicenseClient.licenseKinds.EDU);
                }));
                it('should be PERSONAL when having at least entitlement com.unity.editor', () => __awaiter(this, void 0, void 0, function* () {
                    const callback = sandbox.spy();
                    sandbox.stub(ipcLicenseClient._messenger, 'sendMessage').returns(Promise.resolve({
                        entitlements: {
                            'com.unity.editor': '...',
                            'com.unity.editor.watermarks.trial': '...'
                        }
                    }));
                    yield ipcLicenseClient.getLicenseInfo(callback);
                    expect(callback.called).to.be.true;
                    expect(callback.getCall(0).args[0]).to.be.undefined;
                    let licenseInfo = JSON.parse(callback.getCall(0).args[1]);
                    expect(licenseInfo.flow).to.equal(ipcLicenseClient.licenseKinds.PERSONAL);
                }));
                it('should be TRIAL when having at least entitlement com.unity.editor.watermarks.trial', () => __awaiter(this, void 0, void 0, function* () {
                    const callback = sandbox.spy();
                    sandbox.stub(ipcLicenseClient._messenger, 'sendMessage').returns(Promise.resolve({
                        entitlements: { 'com.unity.editor.watermarks.trial': '...' }
                    }));
                    yield ipcLicenseClient.getLicenseInfo(callback);
                    expect(callback.called).to.be.true;
                    expect(callback.getCall(0).args[0]).to.be.undefined;
                    let licenseInfo = JSON.parse(callback.getCall(0).args[1]);
                    expect(licenseInfo.flow).to.equal(ipcLicenseClient.licenseKinds.TRIAL);
                }));
                it('should be UNKNOWN when having no entitlement', () => __awaiter(this, void 0, void 0, function* () {
                    const callback = sandbox.spy();
                    sandbox.stub(ipcLicenseClient._messenger, 'sendMessage').returns(Promise.resolve({
                        entitlements: {}
                    }));
                    yield ipcLicenseClient.getLicenseInfo(callback);
                    expect(callback.called).to.be.true;
                    expect(callback.getCall(0).args[0]).to.be.undefined;
                    let licenseInfo = JSON.parse(callback.getCall(0).args[1]);
                    expect(licenseInfo.flow).to.equal(ipcLicenseClient.licenseKinds.UNKNOWN);
                }));
            });
        });
        describe('license activation state', () => {
            it('should be false by default', () => {
                expect(ipcLicenseClient.isLicenseValid()).to.be.false;
            });
            it('should be false when getLicenseInfo called and we don\'t have com.unity.editor entitlement', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(ipcLicenseClient._messenger, 'sendMessage').returns(Promise.resolve({
                    entitlements: { 'com.weird.not.unity': '...' }
                }));
                yield ipcLicenseClient.getLicenseInfo();
                expect(ipcLicenseClient.isLicenseValid()).to.be.false;
            }));
            it('should be true when getLicenseInfo called and we have com.unity.editor entitlement', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(ipcLicenseClient._messenger, 'sendMessage').returns(Promise.resolve({
                    entitlements: { 'com.unity.editor': '...' }
                }));
                yield ipcLicenseClient.getLicenseInfo();
                expect(ipcLicenseClient.isLicenseValid()).to.be.true;
            }));
        });
        describe('get license pipe name', () => {
            describe('when no configuration provided', () => {
                it('should contain os user name', () => {
                    expect(ipcLicenseClient._getLicenseClientPipeName())
                        .to
                        .equal(`Unity-LicenseClient-${osUserName}`);
                });
                it('should be suffixed when requested', () => {
                    expect(ipcLicenseClient._getLicenseClientPipeName('suffix'))
                        .to
                        .equal(`Unity-LicenseClient-${osUserName}-suffix`);
                });
            });
            describe('when configuration says to use legacy pipe name', () => {
                beforeEach(() => {
                    settingsMock[settings.keys.USE_LEGACY_LICENSING_CLIENT_PIPE_NAME] = true;
                });
                it('should use legacy pipe name', () => {
                    expect(ipcLicenseClient._getLicenseClientPipeName())
                        .to
                        .equal('Unity-LicenseClient');
                });
                it('should be suffixed when requested', () => {
                    expect(ipcLicenseClient._getLicenseClientPipeName('suffix'))
                        .to
                        .equal('Unity-LicenseClient-suffix');
                });
            });
        });
    });
    describe('when named pipe not opened', () => {
        beforeEach(() => {
            ipcLicenseClient = new IpcLicenseClient();
            sandbox.stub(ipcLicenseClient._messenger._ipc.config, 'retry').get(() => 10);
            sandbox.stub(ipcLicenseClient._messenger._ipc.config, 'retry').set(() => { });
            sandbox.stub(ipcLicenseClient._messenger._ipc.config, 'maxRetries').get(() => 1).set();
            sandbox.stub(ipcLicenseClient._messenger._ipc.config, 'maxRetries').set(() => { });
            sandbox.stub(logger, 'error').callsFake(() => { });
        });
        describe('init', () => {
            it('should handle undefined licenseClientApplicationPath', () => __awaiter(this, void 0, void 0, function* () {
                yield ipcLicenseClient.init();
                expect(logger.error).calledWith('Missing config value for licenseClientApplicationPath');
            }));
            it('should handle null licenseClientApplicationPath', () => __awaiter(this, void 0, void 0, function* () {
                settingsMock[settings.keys.LICENSE_CLIENT_APPLICATION_PATH] = null;
                yield ipcLicenseClient.init();
                expect(logger.error).calledWith('Missing config value for licenseClientApplicationPath');
            }));
            it('should handle empty licenseClientApplicationPath', () => __awaiter(this, void 0, void 0, function* () {
                settingsMock[settings.keys.LICENSE_CLIENT_APPLICATION_PATH] = '';
                yield ipcLicenseClient.init();
                expect(logger.error).calledWith('Missing config value for licenseClientApplicationPath');
            }));
            it('should handle license client not found', () => __awaiter(this, void 0, void 0, function* () {
                settingsMock[settings.keys.LICENSE_CLIENT_APPLICATION_PATH] = '/some/invalid/path';
                yield ipcLicenseClient.init();
                expect(logger.error.called);
                expect(logger.error.getCall(0).args[0]).to.startsWith('Unable to access licensing client');
            }));
        });
    });
    describe('sendAccessToken', () => {
        beforeEach(() => {
            sandbox.stub(ipcLicenseClient._messenger, 'sendMessage').returns(0);
        });
        it('should return the result of sending the message', () => {
            expect(ipcLicenseClient.sendAccessToken()).to.equal(0);
        });
        it('should send the access token info through the IpcMessenger', () => {
            ipcLicenseClient.sendAccessToken();
            expect(ipcLicenseClient._messenger.sendMessage).to.have.been.calledWith({
                messageType: 'AccessTokenRequest',
                value: tokenManagerStub.accessToken.value,
                expiration: tokenManagerStub.accessToken.expiration
            });
        });
    });
});
//# sourceMappingURL=ipcLicenseClient.spec.js.map