var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const machina = require('machina');
const logger = require('../../logger')('LicenseFSM');
const parseString = require('xml2js').parseString;
const postal = require('postal');
const LICENSE_STATUS = require('./licenseStatus');
const tokenManager = require('../../tokenManager/tokenManager');
const _ = require('lodash');
const UnityAuth = require('../localAuth/auth');
function debugOnEnter(fsm) {
    logger.info(`${fsm.priorState} --> ${fsm.state}`);
}
const licenseCore = require('./licenseCore');
const CrossEnvLicense = 448;
const UpgradeFailure = 449;
const stateMachineEvents = {
    namespace: 'fsm.license',
    states: {
        uninitialized: {
            '*': function () {
                this.transition('initializing');
            }
        },
        initializing: {
            _onEnter() {
                debugOnEnter(this);
                licenseCore.verifyLicense()
                    .then((result) => {
                    if (result) {
                        this.transition('licenseValid');
                    }
                    else if (licenseCore.licenseStatus === LICENSE_STATUS.kLicenseErrorFlag_NoLicense) {
                        if (UnityAuth.userInfo.valid && tokenManager.accessToken.value !== '') {
                            this.handle('licenseInvalid');
                        }
                        else {
                            this.transition('notLogin');
                        }
                    }
                    else if (licenseCore.mMustUpdateSetByVersion) {
                        this.transition('licenseUpdate');
                    }
                    else {
                        this.transition('licenseInvalid');
                    }
                })
                    .catch(() => {
                    this.transition('licenseInvalid');
                });
            },
            new: 'licenseNew',
            reset: 'initializing',
            _onExit() {
                if (!licenseCore.mMustUpdateSetByVersion || licenseCore.getAccessToken() === '') {
                    this.emit('initialized');
                }
            }
        },
        notLogin: {
            _onEnter() {
                debugOnEnter(this);
            },
            new: 'licenseNew',
            reset: 'initializing'
        },
        licenseValid: {
            _onEnter() {
                debugOnEnter(this);
                this.emit('licenseValid');
                var scheduledUpdateTime = 2000;
                var regularCheckTime = 3600000;
                if (licenseCore.lastAttemptUpdateDate != null) {
                    scheduledUpdateTime = parseInt((licenseCore.lastAttemptUpdateDate.getTime() + 7200000) - new Date().getTime(), 10);
                }
                if (scheduledUpdateTime < 2000) {
                    scheduledUpdateTime = 2000;
                }
                this.timer = setTimeout(() => {
                    if (licenseCore.getMustUpdate()) {
                        this.transition('licenseUpdate');
                    }
                }, scheduledUpdateTime);
                this.licenseRegularCheck = setTimeout(() => {
                    this.reset();
                }, regularCheckTime);
            },
            return: 'licenseReturn',
            refresh: 'licenseUpdate',
            reset: 'initializing',
            new() {
                this.transition('licenseNew');
            },
            _onExit() {
                clearTimeout(this.timer);
                clearTimeout(this.licenseRegularCheck);
            }
        },
        licenseNew: {
            _onEnter() {
                debugOnEnter(this);
                licenseCore.initLicense()
                    .then((response) => {
                    parseString(response, (err, result) => {
                        if (err) {
                            this.generalLicenseErrorHandler(err);
                            return;
                        }
                        this.emit('survey', result.Transaction.survey);
                    });
                    this.emit('licenseNew');
                })
                    .catch((err) => {
                    this.generalLicenseErrorHandler(err);
                    this.tryRestoreBackupLicense();
                });
            },
            reset: 'initializing',
            activate(rxID) {
                licenseCore.setRx(rxID);
                licenseCore.moveReplaceLicenseFile();
                licenseCore.downloadLicense()
                    .then(response => licenseCore.writeLicenseFile(response))
                    .then(() => {
                    this.emit('issued', { activationMethod: 'Online' });
                    this.transition('initializing');
                })
                    .catch((error) => {
                    this.generalLicenseErrorHandler(error);
                    this.tryRestoreBackupLicense();
                });
            }
        },
        licenseUpdate: {
            _onEnter() {
                debugOnEnter(this);
                licenseCore.updateLicense()
                    .then((response) => {
                    parseString(response, (error, result) => {
                        if (error) {
                            this.emit('error', {
                                error,
                                message: response
                            });
                            this.transition('licenseInvalid');
                            return;
                        }
                        licenseCore.setRx(result.Transaction.Rx);
                        this.transition('licenseUpdated');
                    });
                    this.emit('licenseUpdate');
                })
                    .catch((error) => {
                    const reason = error.response;
                    if (reason.status === CrossEnvLicense || reason.status === UpgradeFailure) {
                        if (licenseCore.mMustUpdateSetByVersion) {
                            this.emit('initialized');
                            licenseCore.clearLicense();
                        }
                        else if (reason.status === CrossEnvLicense) {
                            licenseCore.deleteLicense();
                            licenseCore.clearActivation();
                        }
                        licenseCore.resetUpdateFlag();
                        this.emit('error', {
                            error: reason,
                            message: reason.data
                        });
                        this.transition('licenseInvalid');
                    }
                    else {
                        this.generalLicenseErrorHandler(reason);
                        this.transition('licenseValid');
                    }
                });
            },
            reset: 'initializing'
        },
        licenseUpdated: {
            _onEnter() {
                debugOnEnter(this);
                licenseCore.downloadLicense()
                    .then(response => licenseCore.writeLicenseFile(response))
                    .then(() => {
                    postal.publish({
                        channel: 'app',
                        topic: 'license.change',
                        data: {
                            kind: licenseCore.getLicenseKind()
                        }
                    });
                    this.emit('updated');
                    this.transition('initializing');
                })
                    .catch((reason) => {
                    this.emit('error', {
                        error: reason,
                        message: reason.error || reason
                    });
                    this.transition('licenseValid');
                });
            },
            reset: 'initializing'
        },
        licenseReturn: {
            _onEnter() {
                debugOnEnter(this);
                licenseCore.returnLicense()
                    .then((response) => {
                    parseString(response, (err, result) => {
                        if (err) {
                            this.handle('error', err);
                            return;
                        }
                        licenseCore.setRx(result.Transaction.Rx);
                    });
                    postal.publish({
                        channel: 'app',
                        topic: 'license.change',
                        data: {
                            kind: 'Returned'
                        }
                    });
                    this.emit('return');
                    this.transition('licenseReturned');
                })
                    .catch((reason) => {
                    this.handle('error', reason.error);
                });
            },
            error(error) {
                this.emit('error', {
                    error,
                    errorCode: 'ERROR.LICENSE.FAILED_TO_RETURN',
                });
                this.transition('licenseValid');
            },
            reset: 'initializing'
        },
        licenseReturned: {
            _onEnter() {
                debugOnEnter(this);
                licenseCore.downloadLicense()
                    .then(() => {
                    this.emit('returned');
                    licenseCore.deleteLicense();
                    this.transition('licenseInvalid');
                })
                    .catch((reason) => {
                    this.handle('error', reason.error);
                });
            },
            error(error) {
                this.emit('error', {
                    error,
                    errorCode: 'ERROR.LICENSE.FAILED_TO_RETURN',
                });
                this.transition('licenseValid');
            },
            reset: 'initializing'
        },
        licenseMaintenance: {
            _onEnter() {
                debugOnEnter(this);
                this.emit('maintenance');
            },
            reset: 'initializing'
        },
        licenseInvalid: {
            _onEnter() {
                debugOnEnter(this);
                this.emit('licenseInvalid');
            },
            new: 'licenseNew',
            reset: 'initializing'
        },
    },
    start() {
        this.handle('init');
    },
    activateNewLicense() {
        this.handle('new');
    },
    returnLicense() {
        this.handle('return');
    },
    updateLicense() {
        this.handle('refresh');
    },
    loadLicense(licenseData) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield licenseCore.verifyLicenseData(licenseData, true);
            }
            catch (error) {
                logger.debug(error);
                return { errorCode: 'ERROR.LICENSE.INVALID_LICENSE_FILE', succeeded: false };
            }
            try {
                yield licenseCore.writeLicenseFile(licenseData);
                this.emit('issued', { activationMethod: 'Manual' });
                this.transition('initializing');
                return { succeeded: true };
            }
            catch (error) {
                logger.debug(error);
                return { errorCode: 'ERROR.LICENSE.FAILED_SAVING_LICENSE_FILE', succeeded: false };
            }
        });
    },
    setActivationId(rx) {
        this.handle('activate', rx);
    },
    reset() {
        this.handle('reset');
    },
    softReset() {
        if (this.state !== 'licenseValid') {
            this.handle('reset');
        }
    },
    generalLicenseErrorHandler(error) {
        logger.warn(error);
        const errorObject = {
            error,
            errorCode: 'ERROR.LICENSE.SERVER.GENERIC',
            params: {
                i18n: {}
            }
        };
        if (error != null) {
            if (error.status === 503) {
                this.transition('licenseMaintenance');
                errorObject.errorCode = 'ERROR.LICENSE.SERVER.MAINTENANCE';
            }
            else if (error.status >= 500 && error.status <= 599) {
                errorObject.errorCode = 'ERROR.LICENSE.SERVER.STATUS_CODE';
                errorObject.params.i18n = { statusCode: error.status.toString() };
            }
        }
        this.emit('error', errorObject);
    },
    tryRestoreBackupLicense() {
        licenseCore.restoreBackupLicenseFile()
            .then((result) => {
            if (result) {
                this.transition('licenseValid');
            }
            else {
                this.transition('licenseInvalid');
            }
        })
            .catch(() => {
            this.transition('licenseInvalid');
        });
    }
};
class LicenseStateMachine {
    constructor() {
        _.assignIn(this, new machina.Fsm(stateMachineEvents));
    }
    init() {
        const self = this;
        this.start();
        postal.subscribe({
            channel: 'fsm.user',
            topic: 'loggedIn',
            callback: () => { self.softReset(); }
        });
    }
}
module.exports = new LicenseStateMachine();
//# sourceMappingURL=licenseServiceFsm.js.map