var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const path = require('path');
const { fs } = require('../../../fileSystem');
const gui_windows_install = 'plastic.exe';
const child_process = require('child_process');
const windowsInstallPaths = [
    'C:\\Program Files\\PlasticSCM5\\client',
    'C:\\Program Files (x86)\\PlasticSCM5\\client',
];
class PlasticSCMUtil {
    getInstallFilename() {
        return 'plasticscm-cloud-windows.exe';
    }
    getConfigPath() {
        return path.join(process.env.LOCALAPPDATA, 'plastic4');
    }
    getInstallPath() {
        windowsInstallPaths.push(...process.env.path.split(';'));
        const index = windowsInstallPaths.findIndex(p => {
            const exists = fs.existsSync(path.join(p, gui_windows_install));
            return exists;
        });
        if (index >= 0) {
            return windowsInstallPaths[index];
        }
        return '';
    }
    getCM() {
        return path.join(this.getInstallPath(), 'cm.exe');
    }
    getCloudConfigClient() {
        return path.join(this.getInstallPath(), 'clconfigureclient.exe');
    }
    isCloudEdition() {
        const installPath = this.getInstallPath();
        if (!installPath) {
            return false;
        }
        const tokenPath = path.join(installPath, 'cloudedition.token');
        return fs.existsSync(tokenPath);
    }
    install(execPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const self = this;
            return new Promise((resolve, reject) => {
                child_process.exec(`${execPath} --mode unattended --unattendedmodeui minimal --disable-components server,ideintegrations,eclipse,mylyn,intellij12`, { maxBuffer: 1024 * 1024 * 218 }, (error) => {
                    self.setupEnv();
                    if (error) {
                        return reject(error);
                    }
                    return resolve();
                });
            });
        });
    }
    setupEnv() {
        const clientPath = this.getInstallPath();
        if (clientPath && process.env.Path.indexOf(clientPath) < 0) {
            process.env.Path = `${process.env.Path};${clientPath}`;
        }
    }
    platform() {
        return 'win32';
    }
}
module.exports = PlasticSCMUtil;
//# sourceMappingURL=plastic_scm_utils_win.js.map