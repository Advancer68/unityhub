var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const path = require('path');
const { fs } = require('../../../fileSystem');
const gui_mac_base = '/Applications/PlasticSCM.app';
const gui_mac_install = gui_mac_base + '/Contents/MacOS/PlasticSCM';
const installerMac = require('../../localInstaller/platform_dependent/unityInstaller_mac');
const logger = require('../../../logger')('PlasticSCMUtil');
class PlasticSCMUtil {
    getInstallFilename() {
        return 'plasticscm-cloud-mac.pkg';
    }
    getConfigPath() {
        return path.join(process.env.HOME, '.plastic4');
    }
    getInstallPath() {
        if (fs.existsSync(gui_mac_install)) {
            return gui_mac_install;
        }
        return '';
    }
    getCM() {
        return gui_mac_base + '/Contents/Applications/cm.app/Contents/MacOS/cm';
    }
    getCloudConfigClient() {
        return gui_mac_base + '/Contents/Applications/clconfigureclient.app/Contents/MacOS/clconfigureclient';
    }
    isCloudEdition() {
        const installPath = this.getInstallPath();
        if (!installPath) {
            return false;
        }
        const basePath = installPath.split('MacOS')[0];
        const tokenPath = path.join(basePath, 'MonoBundle', 'cloudedition.token');
        return fs.existsSync(tokenPath);
    }
    install(execPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const self = this;
            this.checkAndSetUpEmptyConfig();
            return new Promise((resolve, reject) => {
                installerMac.installPlastic(execPath).then(() => {
                    self.deleteEmptyConfig();
                    return resolve();
                }).catch(error => {
                    return reject(error);
                });
            });
        });
    }
    checkAndSetUpEmptyConfig() {
        const configFolder = this.getConfigPath();
        const clientConfig = path.join(configFolder, 'client.conf');
        try {
            if (!fs.existsSync(configFolder)) {
                fs.mkdirSync(configFolder);
            }
            if (!fs.existsSync(clientConfig)) {
                fs.closeSync(fs.openSync(clientConfig, 'w'));
            }
        }
        catch (error) {
            logger.error('failed to setup plastic config:', error);
        }
    }
    deleteEmptyConfig() {
        const configFolder = this.getConfigPath();
        const clientConfig = path.join(configFolder, 'client.conf');
        try {
            if (fs.existsSync(clientConfig)) {
                const content = fs.readFileSync(clientConfig, 'utf8');
                if (!content || !content.trim()) {
                    fs.unlink(clientConfig);
                }
            }
        }
        catch (error) {
            logger.error('failed to delete plastic config:', error);
        }
    }
    platform() {
        return 'darwin';
    }
}
module.exports = PlasticSCMUtil;
//# sourceMappingURL=plastic_scm_utils_mac.js.map