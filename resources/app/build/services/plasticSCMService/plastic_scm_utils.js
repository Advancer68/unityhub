var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { app, shell } = require('electron');
const path = require('path');
const axios = require('axios');
const tools = require('../editorApp/platformtools');
const PlasticSCMUtilsPlatform = tools.require(`${__dirname}/lib/plastic_scm_utils`);
const DOMParser = require('xmldom').DOMParser;
const { fs } = require('../../fileSystem');
const logger = require('../../logger')('PlasticSCMUtil');
const child_process = require('child_process');
const windowManager = require('../../windowManager/windowManager');
const os = require('os');
const profileTemplate = `<?xml version="1.0"?>
<ServerProfileData xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
  <Profiles>
  </Profiles>
</ServerProfileData>`;
const plasticLogFile = path.join(app.getPath('userData'), 'logs', 'plasticscm.log');
const ignoreList = ['.idea', 'plastic.ini'];
const PLASTIC_SCM_PLUGIN_VERSION = '1.1.6-preview';
const setLanguageMinimalVersion = '9.0.16.5075';
class PlasticSCMUtil extends PlasticSCMUtilsPlatform {
    constructor() {
        super();
        this.endpoint = 'https://public-cdn.cloud.unitychina.cn/hub/prod/plastic/plastic-scm-conf.json';
        this.cloneInfo = {};
        this.uploadInfo = {};
        this.versionStatus = null;
    }
    getProfilesPath() {
        return path.join(this.getConfigPath(), 'profiles.conf');
    }
    getIgnoreConfPath() {
        return path.join(this.getConfigPath(), 'ignore.conf');
    }
    getClientConfigPath() {
        return path.join(this.getConfigPath(), 'client.conf');
    }
    getVersionFileName() {
        return 'plastic-version';
    }
    setIgnoreList() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (this.getInstallPath() && this.getConfigPath()) {
                    const ignoreFile = this.getIgnoreConfPath();
                    let content = '';
                    if (fs.existsSync(ignoreFile)) {
                        content = fs.readFileSync(ignoreFile, 'utf8');
                    }
                    var appendFile = fs.createWriteStream(ignoreFile, {
                        flags: 'a',
                    });
                    ignoreList.forEach(line => {
                        if (content.indexOf(line) < 0) {
                            appendFile.write(os.EOL);
                            appendFile.write(line);
                        }
                    });
                    appendFile.end();
                }
            }
            catch (error) {
                logger.error(error);
            }
        });
    }
    isValidEdition() {
        return __awaiter(this, void 0, void 0, function* () {
            return Promise.resolve(this.isCloudEdition());
        });
    }
    isValidVersion(forceSync) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.versionStatus || this.versionStatus.current === '0.0.0.0' || forceSync) {
                try {
                    yield this.syncVersionData();
                }
                catch (error) {
                    logger.info(error);
                    Promise.resolve(true);
                }
            }
            const { current, versionData } = this.versionStatus;
            const isValid = this.plasticVersionGTE(current, versionData.requirements.minimalVersion);
            return Promise.resolve(isValid);
        });
    }
    syncVersionData() {
        return __awaiter(this, void 0, void 0, function* () {
            const version = yield this.getCurrentVersion();
            const remoteVersionData = yield this.getVersionRequirement();
            this.versionStatus = {
                current: version,
                versionData: remoteVersionData,
            };
            return Promise.resolve();
        });
    }
    plasticVersionGTE(current, required) {
        const currentParts = current.split('.');
        const requiredParts = required.split('.');
        if (currentParts.length > requiredParts.length) {
            return true;
        }
        else if (currentParts.length < requiredParts.length) {
            return false;
        }
        else {
            for (let i = 0; i < currentParts.length; i++) {
                const currentPartNum = parseInt(currentParts[i]);
                const requiredPartNum = parseInt(requiredParts[i]);
                if (currentPartNum > requiredPartNum) {
                    return true;
                }
                else if (currentPartNum < requiredPartNum) {
                    return false;
                }
            }
        }
        return true;
    }
    getCurrentVersion() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.getInstallPath()) {
                return Promise.resolve('0.0.0.0');
            }
            const cmd = this.getCM();
            return new Promise((resolve, reject) => {
                child_process.exec(`"${cmd}" version`, (error, stdout, stderr) => {
                    if (error) {
                        plasticLog('failed to get current version: ', error);
                        reject();
                    }
                    if (stdout) {
                        const versions = stdout.match(/(\d+\.\d+\.\d+\.\d+)/);
                        if (versions.length > 0) {
                            return resolve(versions[0]);
                        }
                        return resolve('');
                    }
                    if (stderr) {
                        logger.info(error);
                        return reject();
                    }
                });
            });
        });
    }
    getVersionRequirement() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield axios.get(this.endpoint, null, { responseType: 'json' });
            return Promise.resolve(response.data);
        });
    }
    getRemoteVerion() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.versionStatus) {
                try {
                    yield this.syncVersionData();
                }
                catch (error) {
                    logger.info(error);
                    Promise.resolve();
                }
            }
            return Promise.resolve(this.versionStatus.versionData.version);
        });
    }
    getRemotePluginVerion() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this.versionStatus) {
                try {
                    yield this.syncVersionData();
                }
                catch (error) {
                    logger.info(error);
                    Promise.resolve(PLASTIC_SCM_PLUGIN_VERSION);
                }
            }
            return Promise.resolve((this.versionStatus && this.versionStatus.versionData && this.versionStatus.versionData.plugin) || PLASTIC_SCM_PLUGIN_VERSION);
        });
    }
    mergeProfile(workingMode, email, token, serverUrl) {
        return __awaiter(this, void 0, void 0, function* () {
            const profilePath = this.getProfilesPath();
            let doc;
            if (!fs.existsSync(profilePath)) {
                doc = new DOMParser().parseFromString(profileTemplate);
            }
            else {
                const content = fs.readFileSync(profilePath, 'utf-8');
                doc = new DOMParser().parseFromString(content);
            }
            const serverProfileData = doc.getElementsByTagName('ServerProfileData')[0];
            const rootProfile = serverProfileData.getElementsByTagName('Profiles')[0];
            const profiles = rootProfile.getElementsByTagName('ServerProfile');
            const targetName = this.ensureSSL(`${serverUrl}_${workingMode}`);
            let find = false;
            for (var i = 0; i < profiles.length; i++) {
                const name = profiles[i].getElementsByTagName('Name')[0];
                if (name.childNodes[0].data === targetName) {
                    find = true;
                    break;
                }
            }
            if (!find) {
                const serverProfileElementWithSSL = yield this.getProfileNode({ doc, serverUrl, workingMode, email, token });
                rootProfile.appendChild(serverProfileElementWithSSL);
                fs.writeFileSync(this.getProfilesPath(), doc.toString());
            }
        });
    }
    getProfileNode({ doc, serverUrl, workingMode, email, token }) {
        return __awaiter(this, void 0, void 0, function* () {
            token = yield this.crypt(token);
            const targetName = `${serverUrl}_${workingMode}`;
            const serverProfileElement = doc.createElement('ServerProfile');
            const nameElement = doc.createElement('Name');
            const serverElement = doc.createElement('Server');
            const workingModeElement = doc.createElement('WorkingMode');
            const securityConfigElement = doc.createElement('SecurityConfig');
            nameElement.appendChild(doc.createTextNode(this.ensureSSL(targetName)));
            serverElement.appendChild(doc.createTextNode(this.ensureSSL(serverUrl)));
            workingModeElement.appendChild(doc.createTextNode(workingMode));
            securityConfigElement.appendChild(doc.createTextNode(`::0:${email}:${token}:`));
            serverProfileElement.appendChild(nameElement);
            serverProfileElement.appendChild(serverElement);
            serverProfileElement.appendChild(workingModeElement);
            serverProfileElement.appendChild(securityConfigElement);
            return serverProfileElement;
        });
    }
    crypt(token) {
        const cmd = this.getCM();
        return new Promise((resolve, reject) => {
            child_process.exec(`"${cmd}" crypt ${token}`, (error, stdout, stderr) => {
                if (error) {
                    plasticLog('failed to crypt: ', error);
                    reject();
                }
                if (stdout) {
                    let token = stdout.split('|SoC|')[1];
                    token = token.replace('\r\n', '');
                    return resolve(token);
                }
                if (stderr) {
                    plasticLog(stderr);
                    return reject();
                }
            });
        });
    }
    configClient(workingMode, email, token, serverUrl) {
        return __awaiter(this, void 0, void 0, function* () {
            const idx = serverUrl.lastIndexOf(':');
            const server = serverUrl.substring(0, idx);
            const prot = serverUrl.substring(idx + 1);
            const cmd = this.getCloudConfigClient();
            return new Promise((resolve, reject) => {
                const params = ['--language=en', `--workingmode=${workingMode}`, `--user=${email}`, `--password=${token}`, `--server=${server}`, `--port=${prot}`];
                const configProc = child_process.spawn(`"${cmd}"`, params, { shell: true });
                configProc.stderr.on('data', error => {
                    plasticLog('failed to config plastic client', error.toString('utf8'));
                });
                configProc.on('close', function (data) {
                    if (data === 0) {
                        return resolve();
                    }
                    else {
                        return reject();
                    }
                });
            });
        });
    }
    updateClientConfig(workingMode, email, token, serverUrl) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.configClient(workingMode, email, token, serverUrl);
            yield this.mergeProfile(workingMode, email, token, serverUrl);
            this.setIgnoreList();
        });
    }
    createWorkspace({ name, workspacePath, repo }) {
        return __awaiter(this, void 0, void 0, function* () {
            const cmd = this.getCM();
            logger.info('create plastic workspace: ', `cm workspace create "${name}" "${workspacePath}" --repository="${repo}"`);
            return new Promise((resolve, reject) => {
                child_process.exec(`"${cmd}" workspace create "${name}" "${workspacePath}" --repository="${repo}"`, (error, stdout, stderr) => {
                    if (error) {
                        plasticLog('failed to create plastic workspace', error);
                        return reject('failed_create_workspace');
                    }
                    if (stdout) {
                        logger.info('create plastic workspace', stdout);
                        return resolve();
                    }
                    if (stderr) {
                        plasticLog(stderr);
                        return reject('failed_create_workspace');
                    }
                });
            });
        });
    }
    cloneRepo({ id, projectName, repoPath, serverUrl, workingMode = 'LDAPWorkingMode', email, token }) {
        return __awaiter(this, void 0, void 0, function* () {
            this.cloneInfo[id] = {};
            const self = this;
            const cmd = this.getCM();
            try {
                yield this.updateClientConfig(workingMode, email, token, this.ensureSSL(serverUrl));
                const workspaceName = yield this.ensureWorkspaceName(projectName);
                const workspacePath = path.join(repoPath, workspaceName);
                const branchName = this.getBranchName(email);
                const parseProcessDataAndNotify = this.parseProcessDataAndNotify;
                yield this.createWorkspace({ name: workspaceName, workspacePath, repo: `${projectName}@${this.ensureSSL(serverUrl)}` });
                yield this.createBranch(branchName, workspacePath);
                return new Promise((resolve, reject) => {
                    const params = ['switch', `/main/${branchName}`, '--forcedetailedprogress'];
                    const cmShellProc = child_process.spawn(`"${cmd}"`, params, { shell: true, cwd: workspacePath });
                    cmShellProc.on('close', function (data) {
                        if (data === 0) {
                            self.cloneInfo[id] = Object.assign(Object.assign({}, self.cloneInfo[id]), { value: 100, status: 'success', path: workspacePath });
                            return resolve(workspacePath);
                        }
                        else {
                            self.cloneInfo[id] = Object.assign(Object.assign({}, self.cloneInfo[id]), { value: 100, status: 'fail' });
                            return reject();
                        }
                    });
                    cmShellProc.stdout.on('data', function (data) {
                        parseProcessDataAndNotify(self, id, data.toString());
                    });
                    cmShellProc.stderr.on('data', function (data) {
                        plasticLog('failed to clone project', data.toString());
                    });
                });
            }
            catch (error) {
                plasticLog(error);
                self.cloneInfo[id] = Object.assign(Object.assign({}, self.cloneInfo[id]), { value: 100, status: 'fail' });
                return Promise.reject(error);
            }
        });
    }
    setClonedProjectType(id, isUnityProject) {
        this.cloneInfo[id].isUnityProject = isUnityProject;
    }
    hideClonedProjectProgressBar(id) {
        this.cloneInfo[id].hideProgressBar = true;
    }
    hideCheckinProjectProgressBar(id) {
        this.uploadInfo[id].hideProgressBar = true;
    }
    ensureSSL(serverUrl) {
        if (!serverUrl.startsWith('ssl://')) {
            return `ssl://${serverUrl}`;
        }
        return serverUrl;
    }
    parseProcessDataAndNotify(self, id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            if (data) {
                const currentTime = new Date().getTime();
                const results = data.match(/.+?(\d*)%\s*(\d*.?\d*)\/(\d*.?\d*)\s*(\w+).*?(\d*\/\d*\s+files)/) || [];
                if ((results).length >= 6) {
                    const percent = results[1];
                    const sizeDownloaded = parseFloat(results[2]);
                    const unit = results[4];
                    const downloadCoefficient = unit === 'GB' ? 1024 : 1;
                    const preCloneInfo = self.cloneInfo[id] || {};
                    const preSizeDownloaded = preCloneInfo.downloaded || 0;
                    const result = { id, value: percent };
                    if (preCloneInfo.time) {
                        const timePassed = currentTime - preCloneInfo.time;
                        const speed = ((sizeDownloaded - preSizeDownloaded) * downloadCoefficient * 1000.0 / timePassed).toFixed(1);
                        result.speed = `${speed} MB/s`;
                    }
                    self.cloneInfo[id] = Object.assign(Object.assign({}, result), { downloaded: sizeDownloaded, time: currentTime });
                    windowManager.broadcastContent('clone.status', result);
                }
            }
        });
    }
    ensureWorkspaceName(workspaceName) {
        const getNoneRepeatWorkspaceName = this.getNoneRepeatWorkspaceName;
        const cmd = this.getCM();
        return new Promise((resolve, reject) => {
            child_process.exec(`"${cmd}" wk list`, (error, stdout, stderr) => {
                if (error) {
                    plasticLog('failed to list plastic workspaces', error);
                    reject();
                }
                if (stdout) {
                    const workspaces = stdout.split('\n');
                    const wkNames = workspaces.map(wk => wk.trim().split('@')[0]);
                    return resolve(getNoneRepeatWorkspaceName(wkNames, workspaceName));
                }
                if (stderr) {
                    plasticLog(stderr);
                    return resolve(workspaceName);
                }
            });
        });
    }
    getNoneRepeatWorkspaceName(nameList, name) {
        let finalName = name;
        let seq = 1;
        while (nameList.findIndex(n => n === finalName) >= 0) {
            finalName = name + ` ${seq}`;
            seq++;
        }
        return finalName;
    }
    openLog() {
        shell.openItem(plasticLogFile);
    }
    openProjectInFolder(id) {
        shell.openItem(this.cloneInfo[id].path);
    }
    addProjectToPlasticSCM(projectName, projectPath, serverUrl, email, token, addToExistingRepo) {
        return __awaiter(this, void 0, void 0, function* () {
            let repo = serverUrl;
            const branchName = this.getBranchName(email);
            try {
                if (!addToExistingRepo) {
                    serverUrl = this.ensureSSL(serverUrl);
                    repo = yield this.createPlasticSCMRepository(projectName, serverUrl, email, token);
                }
                const workspaceName = yield this.ensureWorkspaceName(projectName);
                yield this.createWorkspace({ name: workspaceName, workspacePath: projectPath, repo });
                yield this.createBranch(branchName, projectPath);
                yield this.switchBranch(branchName, projectPath);
                return Promise.resolve();
            }
            catch (error) {
                return Promise.reject();
            }
        });
    }
    checkinProject(workspacePath) {
        return __awaiter(this, void 0, void 0, function* () {
            const cmd = this.getCM();
            const parseProcessDataAndNotify = this.parseCommitProcessDataAndNotify;
            const self = this;
            const id = workspacePath;
            this.uploadInfo[id] = { id, percent: 1 };
            windowManager.broadcastContent('checkin.status', this.uploadInfo[id]);
            yield this.checkAndSetupProjectIgnore(workspacePath);
            return new Promise((resolve, reject) => {
                const params = ['ci', '--private', '--all', '--machinereadable', '-c "Initial commit"'];
                const cmShellProc = child_process.spawn(`"${cmd}"`, params, { shell: true, cwd: workspacePath });
                cmShellProc.on('close', function (data) {
                    if (data === 0) {
                        self.uploadInfo[id] = { id, percent: 100, status: 'success' };
                        windowManager.broadcastContent('checkin.status', self.uploadInfo[id]);
                        return resolve('');
                    }
                    else {
                        self.uploadInfo[id] = { id, percent: 100, status: 'failed' };
                        windowManager.broadcastContent('checkin.status', self.uploadInfo[id]);
                        return reject();
                    }
                });
                cmShellProc.stdout.on('data', function (data) {
                    parseProcessDataAndNotify(self, id, data.toString());
                });
                cmShellProc.stderr.on('data', function (data) {
                    plasticLog('failed to commit project', data.toString());
                });
            });
        });
    }
    checkAndSetupProjectIgnore(workspacePath) {
        return __awaiter(this, void 0, void 0, function* () {
            const ignoreFile = path.join(workspacePath, 'ignore.conf');
            if (!fs.existsSync(ignoreFile)) {
                fs.writeFileSync(ignoreFile, projectIgnoreList);
            }
        });
    }
    mergeToMain(email, workspacePath) {
        return __awaiter(this, void 0, void 0, function* () {
            const branchName = this.getBranchName(email);
            const cmd = this.getCM();
            const mergeComment = 'Merge to main';
            return new Promise((resolve, reject) => {
                child_process.exec(`"${cmd}" merge br:/main/${branchName} --to=br:/main --merge -c="${mergeComment}"`, { cwd: workspacePath, maxBuffer: 1024 * 1024 * 1024 }, (error, stdout, stderr) => {
                    if (error) {
                        reject(error);
                    }
                    if (stdout) {
                        return resolve();
                    }
                    if (stderr) {
                        return reject(stderr);
                    }
                });
            });
        });
    }
    parseCommitProcessDataAndNotify(self, id, data) {
        return __awaiter(this, void 0, void 0, function* () {
            if (data) {
                const results = data.match(/.+?(\d+.?\d*)\s+(\w+)\/(\d+.?\d*)\s+(\w+)/) || [];
                if (results.length >= 4) {
                    const sizeUploaded = parseFloat(results[1]);
                    const sizeUploadedUnit = results[2];
                    const sizeTotal = parseFloat(results[3]);
                    const sizeTotalUnit = results[4];
                    const uploadedInBytes = getBytes(sizeUploaded, sizeUploadedUnit);
                    const sizeTotalInBytes = getBytes(sizeTotal, sizeTotalUnit);
                    if (sizeTotalInBytes === 0) {
                        return;
                    }
                    const percent = Math.round(uploadedInBytes * 100.0 / sizeTotalInBytes);
                    const result = { id, percent: percent >= 1 ? percent : 1 };
                    self.uploadInfo[id] = result;
                    windowManager.broadcastContent('checkin.status', result);
                }
            }
        });
    }
    createPlasticSCMRepository(projectName, serverUrl, email, token) {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.updateClientConfig('LDAPWorkingMode', email, token, serverUrl);
            const cmd = this.getCM();
            return new Promise((resolve, reject) => {
                child_process.exec(`"${cmd}" repository create ${serverUrl} "${projectName}"`, (error, stdout, stderr) => {
                    if (error) {
                        plasticLog('failed to create plasticscm repository', error);
                        return reject('failed_create_repo');
                    }
                    if (stderr) {
                        plasticLog(stderr);
                        return reject('failed_create_repo');
                    }
                    return resolve(`${projectName}@${serverUrl}`);
                });
            });
        });
    }
    createBranch(branchName, workspacePath) {
        return __awaiter(this, void 0, void 0, function* () {
            const cmd = this.getCM();
            return new Promise((resolve) => {
                child_process.exec(`"${cmd}" branch mk /main/${branchName}`, { cwd: workspacePath }, (error, stdout, stderr) => {
                    const errorAll = error || stderr;
                    if (errorAll) {
                        if (errorAll.toString().indexOf('already exists') < 0) {
                            plasticLog('failed to create branch:', errorAll);
                        }
                    }
                    return resolve();
                });
            });
        });
    }
    switchBranch(branchName, workspacePath) {
        return __awaiter(this, void 0, void 0, function* () {
            const cmd = this.getCM();
            return new Promise((resolve) => {
                const params = ['switch', `/main/${branchName}`, '--forcedetailedprogress'];
                const proc = child_process.spawn(`"${cmd}"`, params, { shell: true, cwd: workspacePath });
                proc.stderr.on('data', error => {
                    plasticLog('failed to switch branch:', error.toString('utf8'));
                });
                proc.on('close', function (data) {
                    return resolve();
                });
            });
        });
    }
    getBranchName(email) {
        const branchName = email.split('@')[0];
        return branchName.replace(/\s+/g, '');
    }
    setPlasticClientLanguage() {
        return __awaiter(this, void 0, void 0, function* () {
            const currentVersion = yield this.getCurrentVersion();
            if (!this.plasticVersionGTE(currentVersion, setLanguageMinimalVersion)) {
                return;
            }
            const clientConfigPath = this.getClientConfigPath();
            if (!fs.existsSync(clientConfigPath)) {
                return;
            }
            const content = fs.readFileSync(clientConfigPath, 'utf-8');
            if (!content) {
                return;
            }
            const regex = /<Language>.*<\/Language>/;
            const newContent = content.replace(regex, '<Language>zh-Hans</Language>');
            fs.writeFile(clientConfigPath, newContent);
        });
    }
    log(...errors) {
        plasticLog(errors);
    }
}
function plasticLog(...errors) {
    logger.error(errors);
    fs.writeFile(plasticLogFile, errors);
}
function getBytes(size, unit) {
    if (unit === 'bytes') {
        return size;
    }
    if (unit === 'KB') {
        return size * 1024;
    }
    if (unit === 'MB') {
        return size * 1024 * 1024;
    }
    return size * 1024 * 1024 * 1024;
}
module.exports = new PlasticSCMUtil();
const projectIgnoreList = `
Library
library
Temp
temp
Obj
obj
Build
build
Builds
builds
UserSettings
usersettings
MemoryCaptures
memorycaptures
Logs
logs
/ignore.conf
*.private
*.private.meta
^*.private.[0-9]+$
^*.private.[0-9]+.meta$
**/Assets/AssetStoreTools
**/assets/assetstoretools
/Assets/Plugins/PlasticSCM*
/assets/plugins/PlasticSCM*
.vs
.gradle
ExportedObj
.consulo
*.csproj
*.unityproj
*.sln
*.suo
*.tmp
*.user
*.userprefs
*.pidb
*.booproj
*.svd
*.pdb
*.mdb
*.opendb
*.VC.db
*.pidb.meta
*.pdb.meta
*.mdb.meta
sysinfo.txt
*.apk
*.unitypackage
.collabignore
crashlytics-build.properties
**/Assets/AddressableAssetsData/*/*.bin*
**/assets/addressableassetsdata/*/*.bin*
**/Assets/StreamingAssets/aa.meta
**/assets/streamingassets/*/aa/*
.DS_Store*
Thumbs.db
Desktop.ini

`;
//# sourceMappingURL=plastic_scm_utils.js.map