var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const axios = require('axios');
const promisify = require('es6-promisify');
const localStorageAsync = require('electron-json-storage');
const cloudConfig = require('../cloudConfig/cloudConfig');
const systemInfo = require('../localSystemInfo/systemInfo');
const postal = require('postal');
const localStorage = {
    get: promisify(localStorageAsync.get, localStorageAsync),
    set: promisify(localStorageAsync.set, localStorageAsync),
};
const splashScreenKey = 'splash-screen';
const durationKey = 'duration';
const lastShowTimeKey = 'lastShowTime';
const entitlementKey = 'hasEntitlement';
const nextShowTimeKey = 'nextShowTime';
const dayInMilliSeconds = 24 * 60 * 60 * 1000;
class SplashScreenService {
    constructor() {
        this.endpoint = 'https://connect.unity.cn/api/hub/splashScreenDuration';
    }
    init(auth) {
        postal.subscribe({
            channel: 'app',
            topic: 'userInfo.changed',
            callback: data => this.onUserInfoChanged(data),
        });
        this.auth = auth;
    }
    onUserInfoChanged(data) {
        if (data.userInfo && data.userInfo.userId) {
            this.syncDurationAndEntitlement();
        }
    }
    getConfig() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield localStorage.get(splashScreenKey);
        });
    }
    updateLastShowTime() {
        return __awaiter(this, void 0, void 0, function* () {
            const conf = yield this.getConfig();
            conf[lastShowTimeKey] = new Date().getTime();
            conf[nextShowTimeKey] = 0;
            return yield localStorage.set(splashScreenKey, conf);
        });
    }
    updateNextShowTime() {
        return __awaiter(this, void 0, void 0, function* () {
            const conf = yield this.getConfig();
            conf[nextShowTimeKey] = new Date().getTime() + dayInMilliSeconds;
            return yield localStorage.set(splashScreenKey, conf);
        });
    }
    setDurationAndEntitlement(duration, hasEntitlement) {
        return __awaiter(this, void 0, void 0, function* () {
            const conf = yield this.getConfig();
            conf[durationKey] = duration;
            conf[entitlementKey] = hasEntitlement;
            return yield localStorage.set(splashScreenKey, conf);
        });
    }
    syncDurationAndEntitlement() {
        return __awaiter(this, void 0, void 0, function* () {
            const user = yield this.auth.getUserInfo();
            const { userId, accessToken } = JSON.parse(user);
            if (userId && accessToken) {
                const duration = yield this.getDuration();
                const hasEntitlement = yield this.checkEntitlement(userId, accessToken);
                this.setDurationAndEntitlement(duration, hasEntitlement);
            }
        });
    }
    getDuration() {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield axios.get(this.endpoint, null, { responseType: 'json' });
            const result = JSON.parse(JSON.stringify(response.data));
            return result.durationInMinutes;
        });
    }
    checkEntitlement(userId, accessToken) {
        return __awaiter(this, void 0, void 0, function* () {
            const url = `${cloudConfig.urls.identity}/v1/entitlements?isActive=true&userId=${userId}&namespace=unity_reward`;
            const conf = {
                headers: {
                    'User-Agent': `hub ${systemInfo.hubVersion()}.cn`,
                    date: (new Date()).toUTCString(),
                    'Content-Type': 'application/json',
                    AUTHORIZATION: `Bearer ${accessToken}`,
                },
                responseType: 'json'
            };
            const response = yield axios.get(url, conf);
            const data = JSON.parse(JSON.stringify(response.data));
            return data.results.length > 0;
        });
    }
    shouldShow() {
        return __awaiter(this, void 0, void 0, function* () {
            const now = new Date().getTime();
            const conf = yield localStorage.get(splashScreenKey);
            const hasEntitlement = conf && conf[entitlementKey];
            let show = false;
            if (hasEntitlement) {
                const nextShowTime = (conf && conf[nextShowTimeKey]) || 0;
                if (nextShowTime !== 0) {
                    if (nextShowTime <= now) {
                        show = true;
                    }
                }
                else {
                    const lastShowTime = (conf && conf[lastShowTimeKey]) || 0;
                    const durationInMinutes = conf && conf[durationKey];
                    const duration = parseInt(durationInMinutes, 10);
                    if (duration <= 0 || (now - lastShowTime >= duration * 60 * 1000)) {
                        show = true;
                    }
                }
            }
            if (show) {
                this.updateLastShowTime();
            }
            return show;
        });
    }
}
module.exports = new SplashScreenService();
//# sourceMappingURL=splashScreenService.js.map