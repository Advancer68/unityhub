const EventEmitter = require('events');
const windowManager = require('../windowManager/windowManager');
class outputService extends EventEmitter {
    init(mode) {
        this.mode = mode;
    }
    notifyContent(...content) {
        if (this.mode === 'cli') {
            this.emit(...content);
        }
        else {
            windowManager.broadcastContent(...content);
        }
    }
    logForCli(text) {
        process.stdout.write(`${text}\n`);
        this.emit('app.end');
    }
}
module.exports = new outputService();
//# sourceMappingURL=outputService.js.map