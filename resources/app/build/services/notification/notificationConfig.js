var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const promisify = require('es6-promisify');
const localStorageAsync = require('electron-json-storage');
const i18nHelper = require('../../i18nHelper');
const localStorage = {
    get: promisify(localStorageAsync.get, localStorageAsync),
    set: promisify(localStorageAsync.set, localStorageAsync),
};
const NOTIFICATION_STORAGE_KEY = 'notificationConfig';
class notificationConfig {
    getAvailableSettings() {
        return [
            { name: i18nHelper.i18n.translate('NOTIFICATION.RECEIVE_ALL'), key: 'all' },
            { name: i18nHelper.i18n.translate('NOTIFICATION.RECEIVE_NOTHING'), key: 'nothing' },
        ];
    }
    getCurrentSetting() {
        return __awaiter(this, void 0, void 0, function* () {
            const setting = yield localStorage.get(NOTIFICATION_STORAGE_KEY);
            return (setting && setting.key) || 'all';
        });
    }
    setSetting(key) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield localStorage.set(NOTIFICATION_STORAGE_KEY, { key });
        });
    }
    notificationEnabled() {
        return __awaiter(this, void 0, void 0, function* () {
            const setting = yield this.getCurrentSetting();
            return setting === 'all';
        });
    }
}
module.exports = new notificationConfig();
//# sourceMappingURL=notificationConfig.js.map