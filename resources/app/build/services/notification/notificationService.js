var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { Notification } = require('electron');
const notificationConfig = require('./notificationConfig');
const windowManager = require('../../windowManager/windowManager');
let lastShowTime = 0;
class NotificationService {
    showsNotification({ title, body, icon, click }) {
        return __awaiter(this, void 0, void 0, function* () {
            const enabled = yield notificationConfig.notificationEnabled();
            if (enabled && Notification.isSupported()) {
                const notification = new Notification({
                    title,
                    body,
                    silent: false,
                    icon,
                });
                notification.on('click', () => {
                    if (click) {
                        click();
                    }
                });
                notification.show();
            }
        });
    }
    showSimpleNotification(title, body, duration = 0) {
        const now = new Date().getTime();
        if (!windowManager.mainWindow.isVisible() && now - lastShowTime >= duration) {
            lastShowTime = now;
            this.showsNotification({
                title,
                body,
                click: () => {
                    windowManager.mainWindow.show();
                },
            });
        }
    }
}
module.exports = new NotificationService();
//# sourceMappingURL=notificationService.js.map