var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const InstallCli = require('../controllers/installCli');
const editorManager = require('../../editorManager/editorManager');
const unityReleaseService = require('../../editorManager/unityReleaseService');
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(chaiAsPromised);
chai.use(sinonChai);
describe('InstallCli', () => {
    let sandbox;
    let installCli;
    const testReleases = [
        { version: 'version1' },
        { version: 'version2' }
    ];
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        installCli = new InstallCli();
        sandbox.spy(editorManager, 'downloadUnityEditor');
        sandbox.spy(installCli, 'installVersion');
        sandbox.spy(installCli, 'installVersionChangeset');
        sandbox.stub(editorManager, 'getReleases').returns(testReleases);
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('handleArgs', () => {
        const versionChangesetModule = { version: "someVersion", changeset: "someChangeset", modules: "someModule" };
        const successfulResult = Object.assign({}, testReleases[0], { id: testReleases[0].version, progress: 0 });
        it('should have been called installVersion with the correct args', () => {
            installCli.handleArgs({ version: 'version1' });
            expect(installCli.installVersion).to.have.been.calledWith('version1');
        });
        it('should have been called installVersionChangeset with the correct arg', () => {
            installCli.handleArgs({ version: "someVersion", changeset: "someChangeset", module: "someModule" });
            expect(installCli.installVersionChangeset).to.have.been.calledWith(versionChangesetModule.version, versionChangesetModule.changeset, versionChangesetModule.modules);
        });
        it('should install versions correctly', () => {
            installCli.handleArgs({ version: "version1" });
            expect(editorManager.downloadUnityEditor).to.have.been.calledWith(successfulResult);
        });
        it('should throw an error to cli if version specified by user is not available', () => {
            installCli.handleArgs({ version: "fakeVer" });
            expect(installCli.installVersion).to.throw('No editor version matched');
        });
        it('should throw an error if changeset is invalid', () => {
            sandbox.stub(unityReleaseService, 'setCustomEditorInfo').rejects('invalid');
            expect(installCli.handleArgs(versionChangesetModule)).to.be.rejectedWith('invalid');
        });
        it('should install version changeset correctly', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(unityReleaseService, 'setCustomEditorInfo').resolves();
            sandbox.stub(unityReleaseService, 'getCustomEditorInfo').returns('editor');
            yield installCli.handleArgs(versionChangesetModule);
            expect(editorManager.downloadUnityEditor).to.have.been.calledWith('editor', []);
        }));
        it('should throw an error if no version arg is passed', () => {
            expect(installCli.handleArgs({})).to.be.rejectedWith('Missing [-v|--version] argument.');
        });
    });
});
//# sourceMappingURL=installCli.spec.js.map