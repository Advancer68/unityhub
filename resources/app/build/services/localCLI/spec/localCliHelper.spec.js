const LocalCliHelper = require('../lib/localCliHelper');
const expect = require('chai').expect;
const sinon = require('sinon');
describe('LocalCliHelper', () => {
    let sandbox;
    const cliModules = ['id1', 'id2'];
    const editorModules = [{ id: 'id1' }, { id: 'id2' }];
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('findModules', () => {
        beforeEach(() => {
            sandbox.spy(LocalCliHelper, '_removeInstalledModules');
            sandbox.spy(LocalCliHelper, '_getEditorModules');
            sandbox.spy(LocalCliHelper, '_addModuleDependencies');
            sandbox.spy(LocalCliHelper, '_removeDuplicates');
        });
        it('should call removeInstalledModules if flag checkForInstalledModules is true', () => {
            LocalCliHelper.findModules(editorModules, cliModules, true);
            expect(LocalCliHelper._removeInstalledModules).to.have.been.calledOnce;
        });
        it('should correctly call private functions', () => {
            const result = LocalCliHelper.findModules(editorModules, 'id1', false);
            expect(LocalCliHelper._getEditorModules).to.have.been.called;
            expect(LocalCliHelper._addModuleDependencies).to.have.been.called;
            expect(LocalCliHelper._removeDuplicates).to.have.been.called;
            expect(LocalCliHelper._removeInstalledModules).to.have.not.been.called;
            expect(result).to.eql([editorModules[0]]);
        });
    });
    describe('_getEditorModules', () => {
        const editorModulesParent = [{ id: 'id1', parent: 'id2' }, { id: 'id3' }];
        beforeEach(() => {
            sandbox.stub(process.stdout, "write").returns(0);
        });
        it('should find all cliModules in editorModules', () => {
            expect(LocalCliHelper._getEditorModules(editorModules, cliModules)).to.eql(editorModules);
        });
        it('should not find a module in list and print out message', () => {
            expect(LocalCliHelper._getEditorModules(editorModulesParent, cliModules)).to.eql([editorModulesParent[0]]);
            expect(process.stdout.write.getCall(0).args[0]).to.equal(`Missing module id2. Skipping..\n\r`);
        });
        it('should be empty if no cliModules', () => {
            expect(LocalCliHelper._getEditorModules(editorModulesParent, [])).to.be.empty;
        });
    });
    describe('_addModuleDependencies', () => {
        const componentList = [{ id: 'id1', parent: 'id3' }, { id: 'id2' }];
        const componentListWithMatchingParent = [{ id: 'id1', parent: 'id2' }, { id: 'id2' }];
        const editorModulesWithSync = [{ id: 'id1', sync: 'id2' }, { id: 'id2' }];
        beforeEach(() => {
            sandbox.stub(process.stdout, "write").returns(0);
        });
        it('should write out that there is a missing module parent', () => {
            LocalCliHelper._addModuleDependencies(editorModules, componentList);
            expect(process.stdout.write.getCall(0).args[0]).to.equal('Missing module id3 that is a dependency of id1.\n\r');
        });
        it('should write out that it is adding a module parent', () => {
            LocalCliHelper._addModuleDependencies(editorModules, componentListWithMatchingParent);
            expect(process.stdout.write.getCall(0).args[0]).to.equal('Adding module id2 as dependency of id1.\n\r');
        });
        it('should write out that it is adding a sync module', () => {
            LocalCliHelper._addModuleDependencies(editorModulesWithSync, componentList);
            expect(process.stdout.write.getCall(0).args[0]).to.equal('Missing module id3 that is a dependency of id1.\n\r');
            expect(process.stdout.write.getCall(1).args[0]).to.equal('Adding module id1 as dependency of id2.\n\r');
        });
    });
    describe('_getModuleInList', () => {
        const moduleList = [
            { id: 'id1' },
            { id: 'id2' },
        ];
        it('should return the module after finding in list', () => {
            expect(LocalCliHelper._getModuleInList('id1', moduleList)).to.eql(moduleList[0]);
        });
        it('should return undefined when module does not exist in list', () => {
            expect(LocalCliHelper._getModuleInList('id3', moduleList)).to.be.undefined;
        });
    });
    describe('_removeInstalledModules', () => {
        const componentList = [{ id: 'id2' }];
        const componentListSelected = [
            { id: 'id1', selected: true },
            { id: 'id2' },
        ];
        beforeEach(() => {
            sandbox.stub(process.stdout, "write").returns(0);
        });
        it('should not remove modules', () => {
            expect(LocalCliHelper._removeInstalledModules(componentList)).to.eql(componentList);
        });
        it('should remove selected modules', () => {
            expect(LocalCliHelper._removeInstalledModules(componentListSelected)).to.eql(componentList);
            expect(process.stdout.write.getCall(0).args[0]).to.equal("Omitting module id1 because it's already installed \n\r");
        });
    });
    describe('_removeDuplicates', () => {
        const listWithDuplicate = [
            { id: 'id1' },
            { id: 'id2' },
            { id: 'id1' },
            { id: 'id3' }
        ];
        const listWithoutDuplicate = [
            { id: 'id1' },
            { id: 'id2' },
            { id: 'id3' }
        ];
        it('should remove duplicates correctly', () => {
            expect(LocalCliHelper._removeDuplicates(listWithDuplicate)).to.eql(listWithoutDuplicate);
        });
        it('should not remove elements when there are no duplicates', () => {
            expect(LocalCliHelper._removeDuplicates(listWithoutDuplicate)).to.eql(listWithoutDuplicate);
        });
    });
});
//# sourceMappingURL=localCliHelper.spec.js.map