var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
const InstallPathCli = require('../controllers/installPathCli');
const editorManager = require('../../editorManager/editorManager');
const { hubFS } = require('../../../fileSystem');
const outputService = require('../../outputService');
describe('InstallPathCli', () => {
    let sandbox;
    let installPathCli;
    let installPath;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        installPathCli = new InstallPathCli();
        sandbox.stub(outputService, 'logForCli');
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('handleArgs', () => {
        let setInstruction = { set: "somePath" };
        let getInstruction = { get: true };
        beforeEach(() => {
            sandbox.stub(editorManager, 'getSecondaryInstallLocation').callsFake(() => { return "installPath"; });
            sandbox.spy(editorManager, 'setSecondaryInstallLocation');
        });
        describe('path valid, path exists', () => {
            beforeEach(() => {
                sandbox.stub(hubFS, 'isValidPath').callsFake(() => { return true; });
                sandbox.stub(hubFS, 'pathExists').resolves(true);
                sandbox.stub(editorManager, 'validateInstallPath').callsFake(() => { return true; });
            });
            it('should execute correct set instruction', () => __awaiter(this, void 0, void 0, function* () {
                yield installPathCli.handleArgs(setInstruction);
                expect(editorManager.setSecondaryInstallLocation).to.have.been.called;
                expect(outputService.logForCli)
                    .to.have.been.calledWith(`All Unity Editors will be installed to ${setInstruction.set}`);
            }));
            it('should execute correct get instruction', () => __awaiter(this, void 0, void 0, function* () {
                yield installPathCli.handleArgs(getInstruction);
                expect(editorManager.getSecondaryInstallLocation).to.have.been.called;
                expect(outputService.logForCli).to.have.been.calledWith('installPath');
            }));
            it('should return path because no set arguments are passed in', () => __awaiter(this, void 0, void 0, function* () {
                yield expect(installPathCli.handleArgs({}));
                expect(editorManager.getSecondaryInstallLocation).to.have.been.called;
                expect(outputService.logForCli).to.have.been.calledWith('installPath');
            }));
            it('should throw an error due to too many arguments', () => __awaiter(this, void 0, void 0, function* () {
                yield expect(installPathCli.handleArgs({ set: "somePath", get: true }))
                    .to.be.rejectedWith('Please provide either \'set\' or \'get\' arguments for the \'install-path\' command');
            }));
        });
        describe('path doesn\'t exists', () => {
            beforeEach(() => {
                sandbox.stub(hubFS, 'isValidPath').callsFake(() => { return false; });
                sandbox.stub(hubFS, 'pathExists').resolves(true);
                sandbox.stub(editorManager, 'validateInstallPath').callsFake(() => { return true; });
            });
            it('should throw invalid path error', () => __awaiter(this, void 0, void 0, function* () {
                yield expect(installPathCli.handleArgs(setInstruction)).to.be.rejectedWith('This path is not valid.');
            }));
        });
        describe('path not validated', () => {
            beforeEach(() => {
                sandbox.stub(hubFS, 'isValidPath').callsFake(() => { return true; });
                sandbox.stub(hubFS, 'pathExists').resolves(false);
                sandbox.stub(editorManager, 'validateInstallPath').callsFake(() => { return true; });
            });
            it('should throw DNE error', () => __awaiter(this, void 0, void 0, function* () {
                yield expect(installPathCli.handleArgs(setInstruction)).to.be.rejectedWith('This directory does not exist.');
            }));
        });
        describe('path not permitted', () => {
            beforeEach(() => {
                sandbox.stub(hubFS, 'isValidPath').callsFake(() => { return true; });
                sandbox.stub(hubFS, 'pathExists').resolves(true);
                sandbox.stub(editorManager, 'validateInstallPath').callsFake(() => { return false; });
            });
            it('should throw not permitted error', () => __awaiter(this, void 0, void 0, function* () {
                yield expect(installPathCli.handleArgs(setInstruction)).to.be.rejectedWith('You cannot install Unity Editors in the hub directory.');
            }));
        });
    });
});
//# sourceMappingURL=installPathCli.spec.js.map