const EditorsCli = require('../controllers/editorsCli');
const editorManager = require('../../editorManager/editorManager');
const unityReleaseService = require('../../editorManager/unityReleaseService');
const outputService = require('../../outputService');
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(chaiAsPromised);
chai.use(sinonChai);
describe('EditorsCli', () => {
    let sandbox;
    let editorsCli;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        editorsCli = new EditorsCli();
        sandbox.stub(outputService, 'logForCli');
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('handleArgs', () => {
        it('when releases passed, it pass the result of _prettifyReleases to logForCli', () => {
            sandbox.stub(editorsCli, '_prettifyReleases').callsFake(() => { return 'list of releases'; });
            editorsCli.handleArgs({ releases: true });
            expect(outputService.logForCli).to.have.been.calledWith('list of releases');
        });
        it('when installed passed, it pass the result of _prettifyEditors to logForCli', () => {
            sandbox.stub(editorsCli, '_prettifyEditors').callsFake(() => { return 'list of editors'; });
            editorsCli.handleArgs({ installed: true });
            expect(outputService.logForCli).to.have.been.calledWith('list of editors');
        });
        it('when no args is passed, it pass the result of _prettifyEditors and _prettifyReleases to logForCli', () => {
            sandbox.stub(editorsCli, '_prettifyEditors').callsFake(() => { return 'list of editors'; });
            sandbox.stub(editorsCli, '_prettifyReleases').callsFake(() => { return 'list of releases'; });
            editorsCli.handleArgs({});
            expect(outputService.logForCli).to.have.been.calledWith('list of releases\nlist of editors');
        });
    });
    describe('_prettifyReleases', () => {
        it('it should only list the versions if the releases are not installed', () => {
            sandbox.stub(editorManager, 'availableEditors').get(() => ({ '2017.1.0f2': {} }));
            sandbox.stub(unityReleaseService, 'releases').get(() => ([
                { version: '2017.1.0f1' },
                { version: '2017.2.0f1' },
                { version: '2017.3.0f1' },
            ]));
            expect(editorsCli._prettifyReleases()).to.equal('2017.1.0f1\n2017.2.0f1\n2017.3.0f1');
        });
        it('it should add the path for the installed releases', () => {
            sandbox.stub(editorManager, 'availableEditors').get(() => ({
                '2017.1.0f2': {
                    location: ['path1'],
                    version: '2017.1.0f2'
                }
            }));
            sandbox.stub(unityReleaseService, 'releases').get(() => ([
                { version: '2017.1.0f2' },
                { version: '2017.2.0f1' },
                { version: '2017.3.0f1' },
            ]));
            expect(editorsCli._prettifyReleases()).to.equal('2017.1.0f2 , installed at path1\n' +
                '2017.2.0f1\n' +
                '2017.3.0f1');
        });
        it('it should filer out the installed releases', () => {
            sandbox.stub(editorManager, 'availableEditors').get(() => ({
                '2017.1.0f2': {
                    location: ['path1'],
                    version: '2017.1.0f2'
                },
                '2017.1.0f1': {
                    location: ['path2'],
                    version: '2017.1.0f2'
                }
            }));
            sandbox.stub(unityReleaseService, 'releases').get(() => ([
                { version: '2017.1.0f2' },
                { version: '2017.2.0f1' },
                { version: '2017.3.0f1' },
            ]));
            expect(editorsCli._prettifyReleases(true)).to.equal('2017.2.0f1\n' +
                '2017.3.0f1');
        });
    });
    describe('_prettifyEditors', () => {
        it('it should list installed editors with their location', () => {
            sandbox.stub(editorManager, 'availableEditors').get(() => ({
                '2017.1.0f2': {
                    location: ['path1'],
                    version: '2017.1.0f2'
                },
                '2017.2.0f2': {
                    location: ['path2'],
                    version: '2017.2.0f2'
                },
                '2017.3.0f2': {
                    location: ['path3'],
                    version: '2017.3.0f2'
                },
            }));
            expect(editorsCli._prettifyEditors()).to.equal('2017.1.0f2 , installed at path1\n' +
                '2017.2.0f2 , installed at path2\n' +
                '2017.3.0f2 , installed at path3');
        });
    });
    describe('_getInstalledPostFix', () => {
        it('it should show the version and location', () => {
            expect(editorsCli._getInstalledPostFix({ version: '1234', location: ['path1'] }))
                .to.equal('1234 , installed at path1');
        });
    });
});
//# sourceMappingURL=editorsCli.spec.js.map