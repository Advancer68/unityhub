var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const localCLI = require('./localCLI');
chai.use(sinonChai);
chai.use(chaiAsPromised);
class FakeController {
    constructor() {
        this.commands = ['test'];
    }
    handleArgs(arg) {
        return arg;
    }
}
describe('LocalCLI', () => {
    let sandbox;
    const fakeController = new FakeController();
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        sandbox.stub(fakeController, 'handleArgs');
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('bindControllers', () => {
        it('should find the right commands list', () => {
            localCLI.bindControllers([fakeController]);
            localCLI.commands['test']();
            expect(fakeController.handleArgs).to.have.been.called;
        });
    });
    describe('handleArgs', () => {
        beforeEach(() => {
            localCLI.commands['test'] = fakeController.handleArgs.bind(fakeController);
        });
        it('should execute the right command', () => {
            localCLI.handleArgs({ 'headless': 'test' });
            expect(fakeController.handleArgs).to.have.been.called;
        });
        it('should throw the correct error', () => __awaiter(this, void 0, void 0, function* () {
            expect(localCLI.handleArgs({ 'headless': 'nothing' })).to.be.rejectedWith('\'nothing\' is not a Hub command.');
        }));
    });
});
//# sourceMappingURL=localCLI.spec.js.map