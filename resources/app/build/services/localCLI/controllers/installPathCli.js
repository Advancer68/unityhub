var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const editorManager = require('../../editorManager/editorManager');
const outputService = require('../../outputService');
const { hubFS } = require('../../../fileSystem');
class InstallPathController {
    constructor() {
        this.commands = ['install-path', 'ip'];
    }
    handleArgs(args) {
        return __awaiter(this, void 0, void 0, function* () {
            if (args.set && args.get) {
                throw new Error('Please provide either \'set\' or \'get\' arguments for the \'install-path\' command');
            }
            if (!args.set) {
                const installPath = editorManager.getSecondaryInstallLocation();
                outputService.logForCli(installPath);
                return;
            }
            if (!hubFS.isValidPath(args.set)) {
                throw new Error('This path is not valid.');
            }
            const isPathExists = yield hubFS.pathExists(args.set);
            if (!isPathExists) {
                throw new Error('This directory does not exist.');
            }
            if (!editorManager.validateInstallPath(args.set)) {
                throw new Error('You cannot install Unity Editors in the hub directory.');
            }
            yield editorManager.setSecondaryInstallLocation(args.set);
            outputService.logForCli(`All Unity Editors will be installed to ${args.set}`);
        });
    }
}
module.exports = InstallPathController;
//# sourceMappingURL=installPathCli.js.map