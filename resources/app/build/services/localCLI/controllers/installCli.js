var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const editorManager = require('../../editorManager/editorManager');
const unityReleaseService = require('../../editorManager/unityReleaseService');
const LocalCliHelper = require('../lib/localCliHelper');
class InstallController {
    constructor() {
        this.commands = ['install', 'i'];
    }
    handleArgs(args) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!args.version) {
                throw new Error('Missing [-v|--version] argument.');
            }
            if (args.changeset) {
                yield this.installVersionChangeset(args.version, args.changeset, args.module);
            }
            else {
                this.installVersion(args.version.toString(), args.module);
            }
        });
    }
    installVersionChangeset(version, changeset, modules = []) {
        return __awaiter(this, void 0, void 0, function* () {
            yield unityReleaseService.setCustomEditorInfo(version, changeset);
            const editor = unityReleaseService.getCustomEditorInfo();
            const components = LocalCliHelper.findModules(editor.modules, modules);
            editorManager.downloadUnityEditor(editor, components);
        });
    }
    installVersion(version, modules = []) {
        const releases = editorManager.getReleases();
        let editor = releases.find(release => release.version.startsWith(version));
        if (!editor) {
            throw new Error('No editor version matched');
        }
        editor = Object.assign({}, editor, { id: editor.version, progress: 0 });
        const components = LocalCliHelper.findModules(editor.modules, modules);
        editorManager.downloadUnityEditor(editor, components);
    }
}
module.exports = InstallController;
//# sourceMappingURL=installCli.js.map