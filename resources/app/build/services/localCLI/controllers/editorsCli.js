const editorManager = require('../../editorManager/editorManager');
const unityReleaseService = require('../../editorManager/unityReleaseService');
const outputService = require('../../outputService');
class EditorsController {
    constructor() {
        this.commands = ['editors', 'e'];
    }
    handleArgs(args) {
        if (args.releases) {
            outputService.logForCli(this._prettifyReleases());
        }
        else if (args.installed) {
            outputService.logForCli(this._prettifyEditors());
        }
        else {
            const result = `${this._prettifyReleases(true)}\n${this._prettifyEditors()}`;
            outputService.logForCli(result);
        }
    }
    _prettifyReleases(excludeInstalled = false) {
        return unityReleaseService.releases.map((release) => {
            if (editorManager.availableEditors[release.version]) {
                if (!excludeInstalled) {
                    return `${this._getInstalledPostFix(editorManager.availableEditors[release.version])}`;
                }
                return null;
            }
            return release.version;
        }).filter((e) => e != null).join('\n');
    }
    _prettifyEditors() {
        return Object.keys(editorManager.availableEditors)
            .map((editor) => this._getInstalledPostFix(editorManager.availableEditors[editor]))
            .join('\n');
    }
    _getInstalledPostFix(installedEditor) {
        return `${installedEditor.version} , installed at ${installedEditor.location[0]}`;
    }
}
module.exports = EditorsController;
//# sourceMappingURL=editorsCli.js.map