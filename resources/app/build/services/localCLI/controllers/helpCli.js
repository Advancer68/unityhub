const outputService = require('../../outputService');
const { fs } = require('../../../fileSystem');
const path = require('path');
class HelpController {
    constructor() {
        this.commands = ['help', 'h'];
    }
    handleArgs() {
        const content = fs.readFileSync(path.join(__dirname, '..', 'lib', 'help.txt'));
        outputService.logForCli(content);
    }
}
module.exports = HelpController;
//# sourceMappingURL=helpCli.js.map