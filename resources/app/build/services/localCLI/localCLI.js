var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const path = require('path');
const requireAll = require('require-all');
const _ = require('lodash');
const controllersPath = path.join(__dirname, 'controllers');
class LocalCLI {
    constructor() {
        this.controllers = {};
        this.commands = {};
    }
    init() {
        requireAll({
            dirname: controllersPath,
            filter: /((?!spec)|.*)\.js$/,
            resolve: (ControllerClass) => {
                this.controllers[this._formatName(ControllerClass.name)] = new ControllerClass();
            }
        });
        this.bindControllers(this.controllers);
    }
    _formatName(className) {
        const trimmed = className.substr(0, className.lastIndexOf('Controller'));
        return _.lowerFirst(trimmed);
    }
    bindControllers(controllers) {
        _.each(controllers, controller => {
            _.each(controller.commands, command => {
                this.commands[command] = controller.handleArgs.bind(controller);
            });
        });
    }
    handleArgs(cliArgs) {
        return __awaiter(this, void 0, void 0, function* () {
            let goodInstruction = false;
            yield Promise.all(_.map(Object.keys(this.commands), (command) => __awaiter(this, void 0, void 0, function* () {
                if (cliArgs.headless === command) {
                    goodInstruction = true;
                    yield this.commands[command](cliArgs);
                }
            })));
            if (!goodInstruction) {
                throw new Error(`'${cliArgs.headless}' is not a Hub command.`);
            }
        });
    }
}
module.exports = new LocalCLI();
//# sourceMappingURL=localCLI.js.map