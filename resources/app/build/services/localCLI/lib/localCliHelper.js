class LocalCliHelper {
    static findModules(editorModules, cliModules, checkForInstalledModules = false) {
        let components;
        if (!Array.isArray(cliModules)) {
            cliModules = [cliModules];
        }
        components = this._getEditorModules(editorModules, cliModules);
        components = this._addModuleDependencies(editorModules, components);
        components = this._removeDuplicates(components);
        if (checkForInstalledModules) {
            components = this._removeInstalledModules(components);
        }
        return components;
    }
    static _getEditorModules(editorModules, cliModules) {
        const componentList = [];
        cliModules.forEach(moduleId => {
            const component = editorModules.find(module => module.id === moduleId);
            if (!component) {
                process.stdout.write(`Missing module ${moduleId}. Skipping..\n\r`);
            }
            else {
                componentList.push(component);
                const childComponents = editorModules.filter((module) => module.parent === component.id);
                if (childComponents.length > 0) {
                    componentList.push(...childComponents);
                }
            }
        });
        return componentList;
    }
    static _addModuleDependencies(editorModules, componentList) {
        const updatedList = [];
        componentList.forEach((component) => {
            if (component.parent) {
                const parentComponent = this._getModuleInList(component.parent, editorModules);
                if (!parentComponent) {
                    process.stdout.write(`Missing module ${component.parent} that is a dependency of ${component.id}.\n\r`);
                }
                else if (componentList.indexOf(parentComponent) < 0) {
                    process.stdout.write(`Adding module ${component.parent} as dependency of ${component.id}.\n\r`);
                    updatedList.push(parentComponent);
                }
            }
            const syncModules = editorModules.filter(module => module.sync === component.id);
            syncModules.forEach(module => {
                process.stdout.write(`Adding module ${module.id} as dependency of ${component.id}.\n\r`);
                updatedList.push(module);
            });
            updatedList.push(component);
        });
        return updatedList;
    }
    static _getModuleInList(moduleId, list) {
        return list.find(module => module.id === moduleId);
    }
    static _removeInstalledModules(componentList) {
        return componentList.filter(component => {
            if (component.selected) {
                process.stdout.write(`Omitting module ${component.id} because it's already installed \n\r`);
            }
            return !component.selected;
        });
    }
    static _removeDuplicates(componentList) {
        const uniqueIds = [];
        return componentList.filter(component => !uniqueIds.find(id => id === component.id) && uniqueIds.push(component.id));
    }
}
module.exports = LocalCliHelper;
//# sourceMappingURL=localCliHelper.js.map