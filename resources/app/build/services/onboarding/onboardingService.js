var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const localStorageAsync = require('electron-json-storage');
const promisify = require('es6-promisify');
const { isBoolean, isString } = require('lodash');
const onboardingConfig = require('./defaultOnboarding');
const localStorage = {
    get: promisify(localStorageAsync.get, localStorageAsync),
    set: promisify(localStorageAsync.set, localStorageAsync),
    remove: promisify(localStorageAsync.remove, localStorageAsync),
};
const ONBOARDING_KEY = 'onboarding';
class OnboardingService {
    constructor() {
        this.setHasOnboarding();
    }
    setHasOnboarding(hasOnboading = onboardingConfig.hasOnboarding) {
        this.hasOnboarding = hasOnboading;
        if (this.hasOnboarding === true) {
            this.defaultOnboarding = {
                isFinished: false,
                url: '/install'
            };
        }
        else {
            this.defaultOnboarding = {
                isFinished: true,
                url: undefined
            };
        }
    }
    setOnboardingURL(url) {
        return localStorage.set(ONBOARDING_KEY, { isFinished: false, onboardingURL: url });
    }
    setOnboardingFinished() {
        return localStorage.set(ONBOARDING_KEY, { isFinished: true });
    }
    getOnboardingState() {
        return __awaiter(this, void 0, void 0, function* () {
            let storageData = {};
            try {
                storageData = yield localStorage.get(ONBOARDING_KEY);
            }
            catch (err) {
                return this.defaultOnboarding;
            }
            const data = {};
            if (storageData) {
                data.isFinished = isBoolean(storageData.isFinished) ? storageData.isFinished : this.defaultOnboarding.isFinished;
                data.url = isString(storageData.url) ? storageData.url : this.defaultOnboarding.url;
                return data;
            }
            return this.defaultOnboarding;
        });
    }
    clearFlags() {
        return __awaiter(this, void 0, void 0, function* () {
            yield localStorage.remove(ONBOARDING_KEY);
        });
    }
}
module.exports = new OnboardingService();
//# sourceMappingURL=onboardingService.js.map