"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sinon_1 = __importDefault(require("sinon"));
const chai_1 = __importDefault(require("chai"));
const sinon_chai_1 = __importDefault(require("sinon-chai"));
const unityInstallState_1 = __importDefault(require("./unityInstallState"));
chai_1.default.use(sinon_chai_1.default);
const { expect } = chai_1.default;
describe('UnityInstallState', () => {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon_1.default.sandbox.create();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('addInstall', () => {
        let installId;
        beforeEach(() => {
            installId = '123';
            unityInstallState_1.default.addInstall(installId);
        });
        afterEach(() => {
            unityInstallState_1.default.removeInstall(installId);
        });
        it('should add install to list', () => {
            unityInstallState_1.default.installError(installId);
            expect(unityInstallState_1.default.getFailedInstalls()[0].version).to.eq(installId);
        });
    });
    describe('removeInstall', () => {
        let installId;
        beforeEach(() => {
            installId = '123';
            unityInstallState_1.default.addInstall(installId);
            unityInstallState_1.default.installError(installId);
        });
        it('should remove install from list', () => {
            expect(unityInstallState_1.default.getFailedInstalls()[0].version).to.equal(installId);
            unityInstallState_1.default.removeInstall(installId);
            expect(unityInstallState_1.default.getFailedInstalls().length).to.equal(0);
        });
    });
    describe('installError', () => {
        let installId;
        beforeEach(() => {
            installId = '123';
            unityInstallState_1.default.addInstall(installId);
        });
        afterEach(() => {
            unityInstallState_1.default.removeInstall(installId);
        });
        it('should mark install as failed', () => {
            expect(unityInstallState_1.default.getFailedInstalls().length).to.equal(0);
            unityInstallState_1.default.installError(installId);
            const firstFailedInstall = unityInstallState_1.default.getFailedInstalls()[0];
            expect(firstFailedInstall.version).to.equal(installId);
            expect(firstFailedInstall.error).to.be.true;
            expect(firstFailedInstall.errorIsForModules).to.be.false;
        });
    });
    describe('installModulesError', () => {
        let installId;
        beforeEach(() => {
            installId = '123';
            unityInstallState_1.default.addInstall(installId);
        });
        afterEach(() => {
            unityInstallState_1.default.removeInstall(installId);
        });
        it('should mark install as failed and error is in modules', () => {
            expect(unityInstallState_1.default.getFailedInstalls().length).to.equal(0);
            unityInstallState_1.default.installModulesError(installId);
            const firstFailedInstall = unityInstallState_1.default.getFailedInstalls()[0];
            expect(firstFailedInstall.version).to.equal(installId);
            expect(firstFailedInstall.error).to.be.true;
            expect(firstFailedInstall.errorIsForModules).to.be.true;
        });
    });
    describe('getFailedInstalls', () => {
        it('when no installs are available should return no installs', () => {
            expect(unityInstallState_1.default.getFailedInstalls().length).to.equal(0);
        });
        it('when one install is available but have not failed should return no installs', () => {
            unityInstallState_1.default.addInstall('123');
            expect(unityInstallState_1.default.getFailedInstalls().length).to.equal(0);
            unityInstallState_1.default.removeInstall('123');
        });
        it('when one install is available and failed should return failed install', () => {
            const installId = '123';
            unityInstallState_1.default.addInstall(installId);
            expect(unityInstallState_1.default.getFailedInstalls().length).to.equal(0);
            unityInstallState_1.default.installError(installId);
            expect(unityInstallState_1.default.getFailedInstalls()[0].version).to.equal(installId);
            unityInstallState_1.default.removeInstall(installId);
        });
        it('when two installs are available and none have errors should return no installs', () => {
            const installIds = ['123', '456'];
            installIds.forEach((id) => unityInstallState_1.default.addInstall(id));
            expect(unityInstallState_1.default.getFailedInstalls().length).to.equal(0);
            installIds.forEach((id) => unityInstallState_1.default.removeInstall(id));
        });
        it('when two installs are available and all have errors should return all installs', () => {
            const installIds = ['123', '456'];
            installIds.forEach((id) => unityInstallState_1.default.addInstall(id));
            expect(unityInstallState_1.default.getFailedInstalls().length).to.equal(0);
            installIds.forEach((id) => unityInstallState_1.default.installError(id));
            expect(unityInstallState_1.default.getFailedInstalls().length).to.equal(2);
            installIds.forEach((id) => unityInstallState_1.default.removeInstall(id));
        });
        it('when two installs are available and one has errors should return right install', () => {
            const installIds = ['123', '456'];
            installIds.forEach((id) => unityInstallState_1.default.addInstall(id));
            expect(unityInstallState_1.default.getFailedInstalls().length).to.equal(0);
            unityInstallState_1.default.installError(installIds[0]);
            expect(unityInstallState_1.default.getFailedInstalls().length).to.equal(1);
            expect(unityInstallState_1.default.getFailedInstalls()[0].version).to.equal(installIds[0]);
            installIds.forEach((id) => unityInstallState_1.default.removeInstall(id));
        });
    });
});
//# sourceMappingURL=unityInstallState.spec.js.map