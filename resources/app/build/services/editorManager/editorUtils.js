'use strict';
const tools = require('../editorApp/platformtools');
const _ = require('lodash');
const path = require('path');
const { fs } = require('../../fileSystem');
const logger = require('../../logger')('EditorManager::Utils');
const buildTargetData = require('./buildTarget.json');
const editorUtilsPlatforms = tools.require(`${__dirname}/editorUtils`);
function findBuildPlatformInfo(editorPath) {
    const buildPlatforms = [];
    const platformsPath = editorUtilsPlatforms.getPlaybackEnginesPathsFromFolder(editorPath);
    const promiseList = [];
    for (let i = 0; i < platformsPath.length; i++) {
        promiseList.push(findBuildPlatformsPerPath(platformsPath[i], buildPlatforms));
    }
    return Promise.all(promiseList)
        .then(() => buildPlatforms)
        .catch((e) => {
        logger.warn(`An error occurred while trying to find the platforms of ${editorPath}: ${e}`);
        return buildPlatforms;
    });
}
function findBuildPlatformsPerPath(platformsPath, buildPlatforms) {
    return getBuildPlatformFromFolder(platformsPath)
        .then((buildPlatformList) => {
        if (buildPlatformList) {
            buildPlatformList.forEach((buildPlatform) => {
                if (typeof buildPlatform === 'object') {
                    buildPlatforms.push(buildPlatform);
                }
            });
        }
    });
}
function getBuildPlatformFromFolder(platformsPath) {
    return fs.readdir(platformsPath)
        .then((folders) => {
        if (folders.length > 0) {
            const promiseList = folders.map(folder => {
                const buildPlatform = getBuildPlatform(folder);
                if (buildPlatform && typeof buildPlatform === 'object') {
                    return buildPlatform;
                }
                return Promise.resolve();
            });
            return Promise.all(promiseList);
        }
        return Promise.resolve([]);
    })
        .catch((e) => {
        if (e.code === 'ENOENT') {
            logger.debug(`No platform found in ${platformsPath}`);
        }
        else {
            throw e;
        }
    });
}
function getBuildPlatform(platformsPath) {
    const platformFolder = path.basename(platformsPath);
    const result = _.find(buildTargetData, { dirName: platformFolder });
    if (!result) {
        return null;
    }
    if (result.buildTargetGroup) {
        result.buildTarget = editorUtilsPlatforms.getOSBuildTarget();
    }
    return result;
}
class EditorUtils {
    constructor() {
        const editorUtils = tools.require(`${__dirname}/editorUtils`);
        _.merge(this, editorUtils);
    }
    findBuildPlatforms(editorPath) {
        return findBuildPlatformInfo(path.dirname(editorPath));
    }
}
module.exports = new EditorUtils();
//# sourceMappingURL=editorUtils.js.map