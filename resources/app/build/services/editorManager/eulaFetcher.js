var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const DOMParser = require('xmldom').DOMParser;
const axios = require('axios');
const logger = require('../../logger');
class EulaFetcher {
    static fetch(module) {
        return __awaiter(this, void 0, void 0, function* () {
            const methodName = `_${module.id}`;
            if (typeof EulaFetcher[methodName] === 'function') {
                return EulaFetcher[methodName](module.eulaUrl1);
            }
            return '';
        });
    }
    static ['_android-sdk-ndk-tools'](url) {
        return __awaiter(this, void 0, void 0, function* () {
            return axios(url)
                .then((response) => {
                const doc = new DOMParser().parseFromString(response.data, 'text/xml');
                let text = '';
                const elements = doc.documentElement.getElementsByTagName('license');
                for (let i = 0; i < elements.length; i++) {
                    if (elements[i].getAttribute('id') === 'android-sdk-license') {
                        text = elements[i].firstChild.data;
                        break;
                    }
                }
                return text;
            })
                .catch((e) => {
                logger.warn(`Failed to fetch eula for android-sdk-ndk-tools, at ${url}`);
                logger.debug(e);
                return '';
            });
        });
    }
}
module.exports = EulaFetcher;
//# sourceMappingURL=eulaFetcher.js.map