"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
const UnityInstallInfo_1 = __importDefault(require("./UnityInstallInfo"));
class UnityInstallState {
    constructor() {
        this.installs = [];
    }
    addInstall(id) {
        this.installs.push(new UnityInstallInfo_1.default(id));
    }
    removeInstall(id) {
        const index = this.getInstallIndex(id);
        if (index === -1) {
            return;
        }
        this.installs.splice(index, 1);
    }
    installError(id) {
        const index = this.getInstallIndex(id);
        if (index === -1) {
            return;
        }
        this.installs[index].error = true;
    }
    installModulesError(id) {
        const index = this.getInstallIndex(id);
        if (index === -1) {
            return;
        }
        this.installs[index].error = true;
        this.installs[index].errorIsForModules = true;
    }
    getFailedInstalls() {
        return this.installs.filter((installInfo) => installInfo.error);
    }
    getInstallIndex(id) {
        return this.installs.findIndex((installInfo) => installInfo.version === id);
    }
}
module.exports = new UnityInstallState();
//# sourceMappingURL=unityInstallState.js.map