const path = require('path');
const { fs, hubFS } = require('../../fileSystem');
const editorUtils = {
    makeEditorInstallsDir(installsPath) {
        return hubFS.elevateAndMakeDir(installsPath);
    },
    getEditorPathFromFolder(editorFolder) {
        return path.join(editorFolder, 'Editor', 'Unity');
    },
    findUnityVersion(editorPath) {
        let fullVersion = path.basename(path.dirname(path.dirname(editorPath)));
        fullVersion = fullVersion.replace(/^Unity-/, '');
        return {
            version: fullVersion,
        };
    },
    validateEditorFile(editorPath) {
        return new Promise((resolve, reject) => {
            if (!editorPath) {
                reject({ errorCode: 'ERROR.INVALID_PATH' });
            }
            else if (!fs.existsSync(editorPath)) {
                reject({ errorCode: 'ERROR.FILE_NOT_FOUND' });
            }
            else {
                resolve(true);
            }
        });
    },
    validateInstallPath() {
        return true;
    },
    getEditorLocationPatterns() {
        return '';
    },
    getSaihaiModuleNames() {
        return [];
    },
    getSaihaiSettings() {
        return {};
    },
    getOSBuildTarget() {
        return 'WebGL';
    },
    getRegistryPackagesPath(editorPath) {
        return path.join(editorPath, '..', 'Data', 'Resources', 'PackageManager', 'Editor');
    },
    getBuiltInPackagesPath(editorPath) {
        return path.join(editorPath, '..', 'Data', 'Resources', 'PackageManager', 'BuiltInPackages');
    },
    getPlaybackEnginesPathsFromFolder(editorFolder) {
        return [
            path.join(editorFolder, 'Data', 'PlaybackEngines')
        ];
    },
};
module.exports = editorUtils;
//# sourceMappingURL=editorUtils_linux.js.map