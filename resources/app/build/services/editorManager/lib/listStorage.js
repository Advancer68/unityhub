var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const promisify = require('es6-promisify');
const localStorage = require('electron-json-storage');
const logger = require('../../../logger')('ListStorage');
const storageKey = Symbol();
const items = Symbol();
const updateItemStorage = Symbol();
const isLocationAnArray = Symbol();
const primaryKey = Symbol();
class ListStorage {
    get items() { return this[items]; }
    constructor(_storageKey, _primaryKey = 'id') {
        this[storageKey] = _storageKey;
        this[primaryKey] = _primaryKey;
        this[items] = {};
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.fetchItems();
        });
    }
    fetchItems() {
        return promisify(localStorage.get)(this[storageKey])
            .then((_items) => {
            logger.info(`${Object.keys(_items).length} located items found in storage`);
            this[items] = _items;
        })
            .catch((err) => {
            logger.debug(err);
            logger.warn('The list of manually located items is corrupted, flushing list...');
            this[items] = {};
            return this[updateItemStorage]();
        });
    }
    [updateItemStorage]() {
        return promisify(localStorage.set)(this[storageKey], this[items]);
    }
    [isLocationAnArray](location) {
        return location instanceof Array;
    }
    addItem(item) {
        this[items][item[this[primaryKey]]] = item;
        return this[updateItemStorage]();
    }
    removeItem(item) {
        delete this[items][item[this[primaryKey]]];
        return this[updateItemStorage]();
    }
    clear() {
        this[items] = {};
        return this[updateItemStorage]();
    }
    save() {
        return this[updateItemStorage]();
    }
}
module.exports = ListStorage;
//# sourceMappingURL=listStorage.js.map