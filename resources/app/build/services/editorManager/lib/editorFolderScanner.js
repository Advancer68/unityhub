const path = require('path');
const logger = require('../../../logger')('EditorFolderScanner');
const editorUtils = require('../editorUtils');
const { fs } = require('../../../fileSystem');
const VERSION_DIRNAME_REGEX = new RegExp('^\\d+\\.\\d+\\.\\d+[a|b|f|p|c](\\d+)(c)*(\\d+)*$');
const basePath = Symbol();
const editorFolders = Symbol();
const scan = Symbol();
const isFolderNotFound = Symbol();
const filterInvalidEditorFolderNames = Symbol();
const filterFoldersWithNoExecutable = Symbol();
class EditorFolderScanner {
    get editorFolders() { return this[editorFolders]; }
    constructor(_basePath) {
        this[basePath] = _basePath;
        this[editorFolders] = [];
    }
    fetchEditorFolders() {
        return this[scan]()
            .then((paths) => {
            this[editorFolders] = paths;
            this[filterInvalidEditorFolderNames]();
            this[filterFoldersWithNoExecutable]();
            logger.info(`${this.editorFolders.length} out of ${paths.length} valid paths found in path: ${this[basePath]}`);
        });
    }
    [scan]() {
        return fs.readdir(this[basePath])
            .catch((error) => {
            if (this[isFolderNotFound](error)) {
                return [];
            }
            return Promise.reject(error);
        });
    }
    [isFolderNotFound](error) {
        return error.code === 'ENOENT' || error.code === 'ENOTDIR';
    }
    [filterInvalidEditorFolderNames]() {
        this[editorFolders] = this.editorFolders.filter((folderName) => VERSION_DIRNAME_REGEX.test(folderName));
    }
    [filterFoldersWithNoExecutable]() {
        this[editorFolders] = this.editorFolders.filter((folderName) => fs.existsSync(editorUtils.getEditorPathFromFolder(path.join(this[basePath], folderName))));
    }
}
module.exports = EditorFolderScanner;
//# sourceMappingURL=editorFolderScanner.js.map