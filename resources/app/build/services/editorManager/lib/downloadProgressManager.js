var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const localStorageCallback = require('electron-json-storage');
const promisify = require('es6-promisify');
const logger = require('../../../logger')('EditorDownloadProgress');
const localStorage = {
    get: promisify(localStorageCallback.get),
    set: promisify(localStorageCallback.set),
    remove: promisify(localStorageCallback.remove)
};
const IN_PROGRESS = 'IN_PROGRESS';
const DOWNLOAD_PROGRESS = 'downloadProgress';
class DownloadProgressManager {
    isEditorDownloadComplete(editorId) {
        return __awaiter(this, void 0, void 0, function* () {
            const storageKey = this._getDownloadProgressKey(editorId);
            try {
                const downloadProgress = yield localStorage.get(storageKey);
                return downloadProgress !== IN_PROGRESS;
            }
            catch (err) {
                logger.error(`Unable to check for the download progress of editor: ${err}`);
                return true;
            }
        });
    }
    startTrackingEditorDownloadProgress(editorId) {
        return __awaiter(this, void 0, void 0, function* () {
            const storageKey = this._getDownloadProgressKey(editorId);
            try {
                yield localStorage.set(storageKey, IN_PROGRESS);
            }
            catch (err) {
                logger.error(`Unable to store download progress of editor in storage: ${err}`);
            }
        });
    }
    setEditorDownloadProgressToComplete(editorId) {
        return __awaiter(this, void 0, void 0, function* () {
            const storageKey = this._getDownloadProgressKey(editorId);
            try {
                yield localStorage.remove(storageKey);
            }
            catch (err) {
                logger.error(`Unable to remove download progress of editor in storage: ${err}`);
            }
        });
    }
    _getDownloadProgressKey(editorId) {
        return `${DOWNLOAD_PROGRESS}:${editorId}`;
    }
}
module.exports = new DownloadProgressManager();
//# sourceMappingURL=downloadProgressManager.js.map