var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const _ = require('lodash');
const windowManager = require('../../../windowManager/windowManager');
const InstalledEditorList = require('./installedEditorList');
const LocatedEditorStorage = require('./locatedEditorStorage');
const editorUtils = require('../editorUtils');
const logger = require('../../../logger')('EditorList');
const { fs } = require('../../../fileSystem');
const downloadProgressManager = require('./downloadProgressManager');
const UnityVersion = require('@unityhub/unity-version');
const EDITORS_KEY = 'editors';
const installedEditorList = Symbol();
const locatedEditorStorage = Symbol();
const updateAvailableEditors = Symbol();
class EditorList {
    get latest() {
        const editorKeys = Object.keys(this.availableEditors);
        let latest = null;
        if (editorKeys.length > 0) {
            editorKeys.sort((version1, version2) => {
                if (UnityVersion.isValid(version1) === false) {
                    return -1;
                }
                else if (UnityVersion.isValid(version2) === false) {
                    return 1;
                }
                return new UnityVersion(version1).compare(version2);
            });
            latest = this.availableEditors[editorKeys[editorKeys.length - 1]];
        }
        return latest;
    }
    get secondaryInstallPath() { return this[installedEditorList].secondaryInstallPath; }
    constructor(installPath) {
        this[installedEditorList] = new InstalledEditorList(installPath);
        this[locatedEditorStorage] = new LocatedEditorStorage(EDITORS_KEY);
        this.availableEditors = {};
    }
    init() {
        logger.info('init');
        return Promise.resolve()
            .then(() => this[installedEditorList].findInstalledEditors())
            .then(() => this[locatedEditorStorage].fetchEditors())
            .then(() => this[updateAvailableEditors]())
            .then(() => this.initEditorsBuildPlatform());
    }
    initEditorsBuildPlatform() {
        return __awaiter(this, void 0, void 0, function* () {
            yield Promise.all(Object.values(this.availableEditors)
                .map((editor) => this.addBuildPlatformsToEditor(editor)));
        });
    }
    addBuildPlatformsToEditor(editor) {
        return __awaiter(this, void 0, void 0, function* () {
            editor.buildPlatforms = yield editorUtils.findBuildPlatforms(editor.location[0]);
        });
    }
    [updateAvailableEditors]() {
        return Promise.resolve()
            .then(() => {
            this.availableEditors =
                Object.assign({}, this[installedEditorList].editors, this[locatedEditorStorage].editors);
        })
            .then(() => this.checkandAssignEditorDownloadStatus())
            .then(() => this.initEditorsBuildPlatform())
            .then(() => windowManager.broadcastContent('available-editors.changed', this.availableEditors));
    }
    checkandAssignEditorDownloadStatus() {
        return __awaiter(this, void 0, void 0, function* () {
            yield Promise.all(Object.values(this.availableEditors)
                .map((editor) => __awaiter(this, void 0, void 0, function* () {
                editor.isDownloadCorrupted = !(yield downloadProgressManager.isEditorDownloadComplete(editor.version));
                if (editor.isDownloadCorrupted && editor.manual) {
                    editor.isDownloadCorrupted = false;
                    yield downloadProgressManager.setEditorDownloadProgressToComplete(editor.version);
                }
            })));
        });
    }
    validateEditorsAreAvailable() {
        const getUnavailableEditors = [];
        const unavailableEditors = [];
        _.each(this.availableEditors, (editor) => {
            const promise = fs.pathExists(editor.location[0])
                .then((editorAvailable) => {
                if (!editorAvailable) {
                    unavailableEditors.push(editor);
                }
            });
            getUnavailableEditors.push(promise);
        });
        return Promise.all(getUnavailableEditors).then(() => {
            logger.info(`${unavailableEditors.length} Editor(s) are not currently available.`);
            return unavailableEditors;
        });
    }
    removeCorruptEditor(editor) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (editor.folderPath) {
                    windowManager.broadcastContent('delete.corrupt.editor.start', editor);
                    yield this.removeInstalledEditor(editor);
                    yield fs.remove(editor.folderPath);
                    windowManager.broadcastContent('delete.corrupt.editor.end', editor);
                }
                else {
                    windowManager.broadcastContent('delete.corrupt.editor.error', editor.version);
                    logger.info(`no folder path defined for ${editor.version}`);
                }
            }
            catch (err) {
                windowManager.broadcastContent('delete.error', editor.version);
                logger.info(`Removing editor folder unsuccessful due to ${err}`);
            }
        });
    }
    addLocatedEditor(editor) {
        return Promise.resolve()
            .then(() => {
            if (this.availableEditors[editor.version]) {
                throw new Error(`Another Unity ${editor.version} exists on your machine.
         You need to uninstall/remove it before adding a new one.`);
            }
            return this[locatedEditorStorage].addEditor(editor);
        })
            .then(() => this[updateAvailableEditors]())
            .then(() => this.availableEditors[editor.version]);
    }
    removeLocatedEditor(editor) {
        return this[locatedEditorStorage].removeEditor(editor)
            .then(() => this[updateAvailableEditors]());
    }
    addInstalledEditor(version) {
        return this[installedEditorList].addEditorVersion(version)
            .then(() => this[updateAvailableEditors]())
            .then(() => this.availableEditors[version]);
    }
    updateInstalledEditor(version) {
        return this[installedEditorList].updateEditorVersion(version)
            .then(() => this[updateAvailableEditors]())
            .then(() => this.availableEditors[version]);
    }
    removeInstalledEditor(editor) {
        return this[installedEditorList].removeEditor(editor)
            .then(() => this[updateAvailableEditors]());
    }
    setSecondaryInstallLocation(installPath) {
        return this[installedEditorList].setSecondaryInstallLocation(installPath)
            .then(() => this[updateAvailableEditors]());
    }
}
module.exports = EditorList;
//# sourceMappingURL=editorList.js.map