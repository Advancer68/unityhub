var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const pathutil = require('path');
const { fs, hubFS } = require('../../../fileSystem');
const logger = require('../../../logger')('EditorSearch');
const postal = require('postal');
const editorUtils = require('../editorUtils');
const PATHS = {
    EDITOR_OLD: editorUtils.getEditorLocationPatterns('editor_old'),
    EDITOR: editorUtils.getEditorLocationPatterns('editor')
};
class EditorSearch {
    constructor(startPath, levels = 3) {
        this.startPath = startPath || [];
        this.levels = levels;
        this.editorPaths = [];
    }
    search(callback = false) {
        return __awaiter(this, void 0, void 0, function* () {
            let editorPaths = [];
            try {
                this.startPath = typeof this.startPath === 'string' ? [this.startPath] : this.startPath;
                for (const path of this.startPath) {
                    editorPaths = editorPaths.concat(yield recursiveEditorSearch(path, this.levels));
                }
                editorPaths = Array.from(new Set(editorPaths));
                this.editorPaths = editorPaths;
                if (callback)
                    callback(null, editorPaths);
                emit('done', editorPaths);
                return editorPaths;
            }
            catch (e) {
                if (callback)
                    callback(e, []);
                emit('error', e);
                logger.warn(e);
                throw e;
            }
        });
    }
    getEditorsInstalledWithHub(paths, callback) {
        return __awaiter(this, void 0, void 0, function* () {
            if (paths instanceof Function) {
                callback = paths;
                paths = this.editorPaths;
            }
            else {
                paths = paths || this.editorPaths;
            }
            const editorsInstalledWithHub = [];
            for (const path of paths) {
                try {
                    const exists = yield editorUtils.isEditorInstalledWithHub(path);
                    if (exists) {
                        editorsInstalledWithHub.push(path);
                    }
                }
                catch (e) {
                    logger.verbose(`Reading ${path} failed. Continue to next path. \nError: ${e.message}`);
                }
            }
            if (callback)
                callback(null, editorsInstalledWithHub);
            emit('done', editorsInstalledWithHub);
            return editorsInstalledWithHub;
        });
    }
    getEditorPaths() {
        return this.editorPaths;
    }
}
function recursiveEditorSearch(startPath, level, paths = [], done = true) {
    return __awaiter(this, void 0, void 0, function* () {
        let currDir;
        if (--level <= 0) {
            return paths;
        }
        let fileNames;
        try {
            fileNames = yield fs.readdir(startPath);
        }
        catch (e) {
            logger.verbose(`Can't read ${startPath}. Stopping function execution and returning paths, is it first call: ${done}.\nError:${e.message}`);
            return paths;
        }
        const asyncCount = fileNames.length;
        if (!asyncCount)
            return paths;
        for (const fileName of fileNames) {
            try {
                currDir = yield fs.lstat(pathutil.join(startPath, fileName));
            }
            catch (e) {
                logger.verbose(`Reading ${pathutil.join(startPath, fileName)} failed. Continue to next path. \nError: ${e.message}`);
            }
            try {
                if (currDir && currDir.isDirectory()) {
                    const pathNewVersion = pathutil.join(startPath, fileName, PATHS.EDITOR);
                    const pathOldVersion = PATHS.EDITOR_OLD ? pathutil.join(startPath, fileName, PATHS.EDITOR_OLD) : false;
                    const newPathExists = yield fs.pathExists(pathNewVersion);
                    if (newPathExists) {
                        paths.push(pathNewVersion);
                    }
                    if (pathOldVersion) {
                        const oldPathExists = yield fs.pathExists(pathOldVersion);
                        if (oldPathExists) {
                            paths.push(pathOldVersion);
                        }
                    }
                    paths = yield recursiveEditorSearch(pathutil.join(startPath, fileName), level, paths, false);
                }
            }
            catch (e) {
                logger.warn(e);
                throw e;
            }
        }
        if (done) {
            const tmpPaths = [];
            for (const path of paths) {
                if (!(yield hubFS.isInRecycleBin(path)))
                    tmpPaths.push(path);
            }
            paths = tmpPaths;
            for (const fileName of fileNames) {
                if (fileName === 'Unity.app') {
                    const path = pathutil.join(startPath, PATHS.EDITOR);
                    if (yield fs.pathExists(path)) {
                        paths.push(path);
                    }
                }
                else if (fileName === 'Unity.exe') {
                    paths.push(pathutil.join(startPath, fileName));
                }
            }
        }
        return paths;
    });
}
function emit(eventName, eventData) {
    postal.publish({
        channel: 'EditorSearch',
        topic: eventName,
        data: eventData
    });
}
module.exports = EditorSearch;
//# sourceMappingURL=editorSearch.js.map