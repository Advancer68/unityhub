const InstalledEditorLocation = require('./installedEditorLocation');
const CustomInstallLocation = require('./customInstallLocation');
const logger = require('../../../logger')('installedEditorList');
const SECONDARY_INSTALL_PATH_KEY = 'secondaryInstallPath';
const defaultPath = Symbol();
const baseInstallLocation = Symbol();
const secondaryInstallLocation = Symbol();
const editors = Symbol();
const scanEditorPaths = Symbol();
const updateEditors = Symbol();
const areBothEditorLocationsNotEmpty = Symbol();
const resolveEditorLocationConflicts = Symbol();
const cloneDeep = Symbol();
const mergeEditorLocations = Symbol();
const isEqualToBasePathOrEmpty = Symbol();
const clearSecondaryInstallLocation = Symbol();
class InstalledEditorList {
    get editors() { return this[editors]; }
    get secondaryInstallPath() { return this[secondaryInstallLocation].installPath; }
    constructor(defaultInstallPath) {
        this[defaultPath] = defaultInstallPath;
        this[baseInstallLocation] = new InstalledEditorLocation(defaultInstallPath);
        this[secondaryInstallLocation] = new CustomInstallLocation(SECONDARY_INSTALL_PATH_KEY);
        this[editors] = {};
    }
    findInstalledEditors() {
        return this[secondaryInstallLocation].initializeCustomLocation()
            .then(() => this[scanEditorPaths]())
            .then(() => this[updateEditors]());
    }
    [scanEditorPaths]() {
        return this[baseInstallLocation].scanForInstallsAndGenerateEditors()
            .then(() => this[secondaryInstallLocation].scanForInstallsAndGenerateEditors());
    }
    [updateEditors]() {
        if (this[areBothEditorLocationsNotEmpty]()) {
            this[editors] = this[resolveEditorLocationConflicts]();
        }
        else {
            this[editors] = this[mergeEditorLocations]();
        }
    }
    [areBothEditorLocationsNotEmpty]() {
        return Object.keys(this[baseInstallLocation].editors).length > 0
            && Object.keys(this[secondaryInstallLocation].editors).length > 0;
    }
    [resolveEditorLocationConflicts]() {
        const baseEditorList = this[baseInstallLocation].editors;
        const secondaryEditorList = this[secondaryInstallLocation].editors;
        const mergedList = this[cloneDeep](baseEditorList);
        Object.keys(secondaryEditorList).forEach((editorVersion) => {
            if (mergedList[editorVersion]) {
                logger.info(`More than one location detected for Editor ${editorVersion}.`);
                mergedList[editorVersion].location.push(secondaryEditorList[editorVersion].location[0]);
            }
            else {
                mergedList[editorVersion] = this[cloneDeep](secondaryEditorList[editorVersion]);
            }
        });
        return mergedList;
    }
    [cloneDeep](obj) {
        return JSON.parse(JSON.stringify(obj));
    }
    [mergeEditorLocations]() {
        const mergedList = Object.assign({}, this[baseInstallLocation].editors, this[secondaryInstallLocation].editors);
        return this[cloneDeep](mergedList);
    }
    addEditorVersion(version) {
        return Promise.resolve()
            .then(() => {
            if (!this[secondaryInstallLocation].installPath) {
                return this[baseInstallLocation].updateEditorVersion(version);
            }
            return this[secondaryInstallLocation].updateEditorVersion(version);
        })
            .then(() => this[updateEditors]());
    }
    updateEditorVersion(version) {
        return Promise.resolve()
            .then(() => {
            if (this[baseInstallLocation].editors[version]) {
                return this[baseInstallLocation].updateEditorVersion(version);
            }
            return this[secondaryInstallLocation].updateEditorVersion(version);
        })
            .then(() => this[updateEditors]());
    }
    removeEditor(editor) {
        return this[baseInstallLocation].removeEditor(editor)
            .then(() => this[secondaryInstallLocation].removeEditor(editor))
            .then(() => this[updateEditors]());
    }
    setSecondaryInstallLocation(installPath) {
        if (this[isEqualToBasePathOrEmpty](installPath)) {
            return this[clearSecondaryInstallLocation]();
        }
        return this[secondaryInstallLocation].setInstallLocation(installPath)
            .then(() => this[secondaryInstallLocation].scanForInstallsAndGenerateEditors())
            .then(() => this[updateEditors]());
    }
    [isEqualToBasePathOrEmpty](installPath) {
        return !installPath || installPath === this[defaultPath];
    }
    [clearSecondaryInstallLocation]() {
        return this[secondaryInstallLocation].setInstallLocation('')
            .then(() => this[updateEditors]());
    }
}
module.exports = InstalledEditorList;
//# sourceMappingURL=installedEditorList.js.map