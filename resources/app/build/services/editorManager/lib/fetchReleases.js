var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const requestp = require('request-promise-native');
const logger = require('../../../logger')('FetchReleases');
const { fs } = require('../../../fileSystem');
const iniUtility = require('ini');
const os = require('os');
const saihaiDataFormatter = require('./saihaiDataFormatter');
const platform = os.platform();
const fetchAllVersionsFromSaihai = Symbol();
const getConfigurations = Symbol();
const handleConfigurationsData = Symbol();
const saihaiData = Symbol();
class FetchReleases {
    fetch() {
        return __awaiter(this, void 0, void 0, function* () {
            const isSaihaiAvailable = yield this.isSaihaiAvailable();
            if (!isSaihaiAvailable) {
                logger.warn('FetchReleases.get() was called when http://saihai.hq.unity3d.com is not available');
                throw new Error('http://saihai.hq.unity3d.com is not available');
            }
            const saihaiFreshData = yield this[fetchAllVersionsFromSaihai]();
            this[saihaiData] = saihaiDataFormatter.format(saihaiFreshData);
            this[saihaiData] = yield this[getConfigurations](this[saihaiData]);
            return this[saihaiData];
        });
    }
    get() {
        return this[saihaiData] ? this[saihaiData] : false;
    }
    deleteCache() {
        this[saihaiData] = undefined;
    }
    save(dir, formatted = false) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this[saihaiData]) {
                try {
                    yield fs.outputFile(dir, JSON.stringify(this[saihaiData], null, formatted ? 4 : 0));
                    return true;
                }
                catch (e) {
                    logger.warn(`Error when trying to save saihaiData to file\n${e.message} is saihaiData defined: ${!!this[saihaiData]}`);
                    throw e;
                }
            }
            return false;
        });
    }
    [fetchAllVersionsFromSaihai](state = 'promoted', releases = []) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { count } = yield requestp({
                    uri: `http://saihai.hq.unity3d.com/api/json/v2/builds/${state}/?format=json&limit=0&offset=0`,
                    json: true
                });
                const { results } = yield requestp({
                    uri: `http://saihai.hq.unity3d.com/api/json/v2/builds/${state}/?format=json&limit=${count}&offset=0`,
                    json: true
                });
                releases = releases.concat(results);
                if (state === 'promoted') {
                    releases = yield this[fetchAllVersionsFromSaihai]('published', releases);
                    return releases;
                }
                return releases;
            }
            catch (e) {
                logger.warn(`Trying to get ${state} releases\n${e.message}`);
                throw e;
            }
        });
    }
    [getConfigurations](releases) {
        return __awaiter(this, void 0, void 0, function* () {
            var promises = [];
            for (let i = 0; i < releases.length; i++) {
                promises.push(this[handleConfigurationsData](releases[i], i, releases));
            }
            try {
                yield Promise.all(promises);
            }
            catch (e) {
                logger.warn(`Error occured when trying to get configuration data\n${e}`);
            }
            return this[saihaiData];
        });
    }
    [handleConfigurationsData](release, index, releases) {
        return __awaiter(this, void 0, void 0, function* () {
            let modulesData;
            try {
                modulesData = yield requestp(release.iniUrl);
            }
            catch (e) {
                logger.warn(`Configuration file is not available for ${release.version} (${release.revision_medium}), url: ${release.iniUrl}`);
                return;
            }
            try {
                modulesData = iniUtility.parse(modulesData);
            }
            catch (e) {
                logger.warn(`Configuration file is not valid. Parsing failed. ${release.version} (${release.revision_medium}), url: ${release.iniUrl}`);
                return;
            }
            Object.keys(modulesData).forEach((key) => {
                let newKey = key;
                if (key === 'Unity')
                    newKey = 'Editor';
                if (releases[index].modules[newKey]) {
                    releases[index].modules[newKey].description = modulesData[key].description;
                    releases[index].modules[newKey].installedSize = platform === 'win32' ? modulesData[key].installedsize * 1024 : modulesData[key].installedsize;
                    releases[index].modules[newKey].downloadSize = platform === 'win32' ? modulesData[key].size * 1024 : modulesData[key].size;
                    releases[index].modules[newKey].checksum = modulesData[key].md5;
                    releases[index].modules[newKey].internal = false;
                }
            });
            this[saihaiData] = releases;
        });
    }
    getReleasesMappedByBaseVersion(replace = false) {
        if (!this[saihaiData])
            return false;
        if (this[saihaiData].constructor === Object)
            return this[saihaiData];
        const res = this[saihaiData].reduce((obj, release) => {
            if (!obj[release.version_base])
                obj[release.version_base] = [];
            obj[release.version_base].push(release);
            return obj;
        }, {});
        Object.keys(res).map((baseVersion) => {
            res[baseVersion].sort((a, b) => b.timestamp_sort - a.timestamp_sort);
            return baseVersion;
        });
        if (replace)
            this[saihaiData] = res;
        return res;
    }
    getPublicReleases(replace = false) {
        let publicReleases;
        if (this[saihaiData].constructor === Object) {
            publicReleases = {};
            Object.keys(this[saihaiData]).map((key) => {
                if (!publicReleases[key])
                    publicReleases[key] = [];
                this[saihaiData][key].map((version, index) => {
                    if (this[saihaiData][key][index].modules.Editor.internal)
                        return version;
                    const id = publicReleases[key].push(this[saihaiData][key][index]) - 1;
                    Object.keys(this[saihaiData][key][index].modules).map((modName) => {
                        if (this[saihaiData][key][index].modules[modName].internal) {
                            delete publicReleases[key][id].modules[modName];
                        }
                        return modName;
                    });
                    return version;
                });
                return key;
            });
            if (replace)
                this[saihaiData] = publicReleases;
            return publicReleases;
        }
        publicReleases = [];
        this[saihaiData].map((version, index) => {
            if (!version.modules.Editor.internal) {
                const id = publicReleases.push(version) - 1;
                Object.keys(this[saihaiData][index].modules).map((modName) => {
                    if (this[saihaiData][index].modules[modName].internal) {
                        delete publicReleases[id].modules[modName];
                    }
                    return modName;
                });
            }
            return version;
        });
        if (replace)
            this[saihaiData] = publicReleases;
        return publicReleases;
    }
    isSaihaiAvailable() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield requestp('http://saihai.hq.unity3d.com/api/json/v2/?format=json');
                return true;
            }
            catch (e) {
                return false;
            }
        });
    }
}
module.exports = new FetchReleases();
//# sourceMappingURL=fetchReleases.js.map