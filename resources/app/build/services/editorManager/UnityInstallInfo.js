"use strict";
class UnityInstallInfo {
    constructor(version) {
        this.version = version;
        this.error = false;
        this.errorIsForModules = false;
    }
}
module.exports = UnityInstallInfo;
//# sourceMappingURL=UnityInstallInfo.js.map