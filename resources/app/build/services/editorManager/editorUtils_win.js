const { fs } = require('../../fileSystem');
const logger = require('../../logger')('EditorManager::Utils');
const path = require('path');
const editorVersion = require('@unityhub/unity-editor-version');
const { app } = require('electron');
function findManufacturer(editorPath) {
    return new Promise((resolve) => {
        const manufacturer = editorVersion.getUnityManufacturer(editorPath).trim();
        logger.debug(`Manufacturer:${manufacturer}`);
        resolve(manufacturer);
    });
}
const editorUtils = {
    getEditorPathFromFolder(editorFolder) {
        return path.join(editorFolder, 'Editor', 'Unity.exe');
    },
    isEditorInstalledWithHub(editorLocation) {
        return new Promise((resolve, reject) => {
            editorLocation = editorLocation.split(/\\|\//g);
            const index = editorLocation.indexOf('Unity.exe');
            if (index >= 0) {
                editorLocation = path.join.apply([], editorLocation.splice(0, index - 1));
                fs.pathExists(path.join(editorLocation, 'modules.json'))
                    .then((exists) => resolve(exists))
                    .catch((error) => reject(error));
            }
            else {
                reject('Not Editor path');
            }
        });
    },
    getEditorLocationPatterns(key) {
        const editorLocationPatters = {
            EDITOR: path.join('Editor', 'Unity.exe'),
            EDITOR_OLD: 'Unity.exe'
        };
        return key ? editorLocationPatters[key.toUpperCase()] : editorLocationPatters;
    },
    findUnityVersion(editorPath) {
        const versionWithChangeset = editorVersion.getUnityVersion(editorPath).trim().split('_');
        return {
            version: versionWithChangeset[0],
        };
    },
    getPlaybackEnginesPathsFromFolder(editorFolder) {
        return [
            path.join(editorFolder, 'Data', 'PlaybackEngines')
        ];
    },
    getOSBuildTarget() {
        return 'Win';
    },
    getRegistryPackagesPath(editorPath) {
        return path.join(editorPath, '..', 'Data', 'Resources', 'PackageManager', 'Editor');
    },
    getBuiltInPackagesPath(editorPath) {
        return path.join(editorPath, '..', 'Data', 'Resources', 'PackageManager', 'BuiltInPackages');
    },
    validateEditorFile(editorPath, skipSignatureCheck) {
        return new Promise((resolve, reject) => {
            try {
                if (!editorPath) {
                    reject({ errorCode: 'ERROR.INVALID_PATH' });
                }
                else if (!editorPath.endsWith('.exe')) {
                    reject({ errorCode: 'ERROR.NOT_AN_APP' });
                }
                else if (!fs.existsSync(editorPath)) {
                    reject({ errorCode: 'ERROR.FILE_NOT_FOUND' });
                }
                else if (skipSignatureCheck) {
                    if (!editorPath.endsWith('Unity.exe')) {
                        reject({ errorCode: 'ERROR.NOT_AN_APP' });
                    }
                    else {
                        resolve(true);
                    }
                }
                else {
                    findManufacturer(editorPath).then((manufacturer) => {
                        if (manufacturer !== 'Unity Technologies ApS') {
                            reject({ errorCode: 'ERROR.NOT_SIGNED' });
                        }
                        else if (!editorPath.endsWith('Unity.exe')) {
                            reject({ errorCode: 'ERROR.NOT_AN_UNITY_APP' });
                        }
                        else {
                            resolve(true);
                        }
                    });
                }
            }
            catch (error) {
                reject(error);
            }
        });
    },
    validateInstallPath(location) {
        const hubPath = path.dirname(app.getPath('exe'));
        return (location.indexOf(hubPath) === -1);
    },
    getSaihaiSettings() {
        return {
            unityPrefix: 'Unity',
            setupPostfix: 'Setup',
            platformName: 'Windows',
            platformNamePostfix: '64',
            installerFormat: 'exe',
            targetSupportString: 'TargetSupportInstaller',
            configurationPlatformName: 'win'
        };
    },
    getSaihaiModuleNames() {
        return [
            {
                name: 'Editor',
                nameFull: 'Unity Editor'
            },
            {
                name: 'Linux',
                nameFull: 'Linux Support'
            },
            {
                name: 'WebGL',
                nameFull: 'WebGL Support'
            },
            {
                name: 'Android',
                nameFull: 'Android Support'
            },
            {
                name: 'AppleTV',
                nameFull: 'AppleTV Support'
            },
            {
                name: 'iOS',
                nameFull: 'iOS Support'
            },
            {
                name: 'Mac',
                nameFull: 'Mac Support'
            },
            {
                name: 'Mac-Mono',
                nameFull: 'Mac-Mono Support'
            },
            {
                name: 'Metro',
                nameFull: 'Metro Support'
            },
            {
                name: 'Facebook-Games',
                nameFull: 'Facebook-Games Support'
            },
            {
                name: 'UWP-IL2CPP',
                nameFull: 'UWP-IL2CPP Support'
            },
            {
                name: 'Windows-IL2CPP',
                nameFull: 'Windows-IL2CPP Support'
            },
            {
                name: 'Samsung-TV',
                nameFull: 'Samsung-TV Support'
            },
            {
                name: 'Tizen',
                nameFull: 'Tizen Support'
            },
            {
                name: 'Vuforia-AR',
                nameFull: 'Vuforia-AR Support'
            },
            {
                name: 'Wii-U',
                nameFull: 'Wii-U Support'
            },
            {
                name: 'Playstation-3',
                nameFull: 'Playstation-3 Support'
            },
            {
                name: 'Playstation-4',
                nameFull: 'Playstation-4 Support'
            },
            {
                name: 'Playstation-Vita',
                nameFull: 'Playstation-Vita Support'
            },
            {
                name: 'Xbox-One',
                nameFull: 'Xbox-One Support'
            },
            {
                name: 'StandardAssets',
                nameFull: 'Standard Assets'
            },
            {
                name: 'Documentation',
                nameFull: 'Documentation'
            }
        ];
    }
};
module.exports = editorUtils;
//# sourceMappingURL=editorUtils_win.js.map