const logger = require('../../logger')('Releases');
const settings = require('../localSettings/localSettings');
const _ = require('lodash');
const cloudConfig = require('../cloudConfig/cloudConfig');
const os = require('os');
const PollingClient = require('../../common/pollingClient');
const platform = os.platform();
const defaultUnityReleases = require(`./defaultReleases-${platform}.json`);
const GenReleases = require('@unityhub/hub-generate-releases').Releases;
const STORAGE_KEY = 'releases';
class Releases extends PollingClient {
    constructor() {
        super();
        this.customEditorInfo = {};
        this.directInstallEditorInfo = {};
        this.customEditorsInProgress = [];
    }
    get logger() {
        return logger;
    }
    get jsonStorageKey() {
        return STORAGE_KEY;
    }
    get refreshIntervalKey() {
        return settings.keys.RELEASES_REFRESH_INTERVAL;
    }
    get isInitBlocking() {
        return false;
    }
    get defaultData() {
        return defaultUnityReleases;
    }
    get overwriteDefault() {
        return true;
    }
    setEndpoint() {
        const releasesFile = `releases-${platform}.json`;
        this.endpoint = cloudConfig.getHubBaseEndpoint() + releasesFile;
        logger.info(`Releases endpoint: ${this.endpoint}`);
    }
    filterReleasesBasedOnOS(releasesObj) {
        return _.flatMap(releasesObj, (releasesList, releaseType) => releasesList
            .filter((release) => release)
            .map((release) => ({
            releaseType,
            downloadUrl: release.downloadUrl,
            lts: release.lts ? release.lts : false,
            downloadSize: release.downloadSize,
            installedSize: release.installedSize,
            checksum: release.checksum,
            modules: release.modules,
            version: release.version,
            sync: release.sync ? release.sync : '',
            parent: release.parent ? release.parent : ''
        })));
    }
    get releases() {
        const ret = this.filterReleasesBasedOnOS(this.data);
        const customEditors = this.getCustomEditorsInProgress();
        return [...ret, ...customEditors];
    }
    getDefaultReleases() {
        return this.filterReleasesBasedOnOS(defaultUnityReleases);
    }
    setCustomEditorInfo(version, revisionHash) {
        return this.fetchReleaseInfo(version, revisionHash)
            .then((releaseInfo) => {
            this.customEditorInfo = releaseInfo;
            this.customEditorInfo.id = this.customEditorInfo.version;
            this.customEditorInfo.name = `Unity ${this.customEditorInfo.version}`;
        });
    }
    setCustomEditorUrl(downloadBaseUrl) {
        this.customEditorInfo.downloadBaseUrl = downloadBaseUrl;
    }
    fetchReleaseInfo(version, revisionHash) {
        let downloadBaseUrl = `https://download.unitychina.cn/download_unity/${revisionHash}/`;
        if (version.indexOf('b') > 0 || version.indexOf('a') > 0) {
            downloadBaseUrl = `http://beta.unitychina.cn/download/${revisionHash}/`;
        }
        this.setCustomEditorUrl(downloadBaseUrl);
        return this._getReleaseInfoPerPlatform(version, downloadBaseUrl, os.platform());
    }
    _getReleaseInfoPerPlatform(version, downloadBaseUrl, releasePlatform) {
        const r = new GenReleases();
        return r.getReleaseInfoPerPlatform({ version, downloadBaseUrl }, releasePlatform);
    }
    getCustomEditorInfo() {
        return this.customEditorInfo;
    }
    addCustomEditorInProgress(editor) {
        editor.version = editor.version || editor.id;
        this.customEditorsInProgress.push(editor);
    }
    removeCustomEditorInProgress(editor) {
        if (this.customEditorsInProgress.length === 0) {
            return;
        }
        if (!editor || editor.id <= 0) {
            logger.warn('Trying to remove an invalid custom editor');
            return;
        }
        if (!editor.id && editor.version) {
            editor.id = editor.version;
        }
        _.remove(this.customEditorsInProgress, { id: editor.id });
    }
    getCustomEditorsInProgress() {
        return this.customEditorsInProgress;
    }
}
module.exports = new Releases();
//# sourceMappingURL=unityReleaseService.js.map