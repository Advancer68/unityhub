const LocatedEditorStorage = require('../lib/locatedEditorStorage');
const localStorage = require('electron-json-storage');
const LocalStorageMock = require('./localStorageMock');
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
    .use(require('sinon-chai'))
    .use(require('chai-as-promised'));
describe('LocatedEditorStorage', () => {
    let sandbox, localStorageMock, storageKey, locatedEditorStorage;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        localStorageMock = new LocalStorageMock(localStorage);
        localStorageMock.stubWithSandbox(sandbox);
        storageKey = 'editors';
        locatedEditorStorage = new LocatedEditorStorage(storageKey);
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('new instance', () => {
        it('should initialize editors to empty object', () => {
            expect(locatedEditorStorage.editors).to.eql({});
        });
    });
    describe('fetchEditors', () => {
        describe('when storage is empty', () => {
            beforeEach(() => {
                return locatedEditorStorage.fetchEditors();
            });
            it('editors should stay empty', () => {
                expect(locatedEditorStorage.editors).to.eql({});
            });
        });
        describe('when storage throws error', () => {
            beforeEach(() => {
                localStorageMock.setKeyThatThrowsError(storageKey);
                return locatedEditorStorage.fetchEditors();
            });
            it('editors should stay empty', () => {
                expect(locatedEditorStorage.editors).to.eql({});
            });
            it('should initialize the storage key in storage with empty object', () => {
                expect(localStorageMock.data[storageKey]).to.eql({});
            });
        });
        describe('when storage is not empty and invalid', () => {
            let storageEditors, validStorage;
            beforeEach(() => {
                storageEditors = { foo: { version: 'foo', location: 'foo' } };
                validStorage = { foo: { version: 'foo', location: ['foo'] } };
                localStorageMock.data[storageKey] = storageEditors;
                return locatedEditorStorage.fetchEditors();
            });
            it('should set corrected editors to storage content', () => {
                expect(locatedEditorStorage.editors).to.eql(validStorage);
            });
        });
        describe('when storage is not empty and valid', () => {
            let storageEditors;
            beforeEach(() => {
                storageEditors = { foo: { version: 'foo', location: ['foo'] } };
                localStorageMock.data[storageKey] = storageEditors;
                return locatedEditorStorage.fetchEditors();
            });
            it('should set editors to storage content', () => {
                expect(locatedEditorStorage.editors).to.eql(storageEditors);
            });
        });
    });
    describe('addEditor', () => {
        let editor;
        beforeEach(() => {
            editor = { version: 'foo' };
            return locatedEditorStorage.addEditor(editor);
        });
        it('should add editor to list', () => {
            expect(locatedEditorStorage.editors[editor.version]).to.eql(editor);
        });
        it('should add editor to storage', () => {
            expect(localStorageMock.data[storageKey][editor.version]).to.eql(editor);
        });
    });
    describe('removeEditor', () => {
        let editor;
        beforeEach(() => {
            editor = { version: 'bar' };
            return locatedEditorStorage.addEditor(editor)
                .then(() => expect(locatedEditorStorage.editors[editor.version]).to.eql(editor))
                .then(() => expect(localStorageMock.data[storageKey][editor.version]).to.eql(editor))
                .then(() => locatedEditorStorage.removeEditor(editor));
        });
        it('should remove editor from list', () => {
            expect(locatedEditorStorage.editors[editor.version]).not.to.be.defined;
        });
        it('should remove editor from storage', () => {
            expect(localStorageMock.data[storageKey][editor.version]).not.to.be.defined;
        });
    });
});
//# sourceMappingURL=locatedEditorStorage.spec.js.map