var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const EditorSearch = require('./../lib/editorSearch');
const editorUtils = require('./../editorUtils');
const { fs } = require('../../../fileSystem');
const platform = require("os").platform();
const pathutil = require('path');
const postal = require('postal');
const os = require('os');
const FsMock = require('./fsMock');
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
    .use(require('sinon-chai'))
    .use(require('chai-as-promised'));
if (os.platform() !== 'linux') {
    describe('EditorSearch', () => {
        let sandbox, fsMock, basePaths, levels, basePath, isEditorInstalledWithHubStub, postalSpy;
        beforeEach(() => {
            sandbox = sinon.sandbox.create();
            fsMock = new FsMock(fs);
            fsMock.stubWithSandbox(sandbox);
            sandbox.stub(postal, 'publish');
            let gelp = sandbox.stub(editorUtils, 'getEditorLocationPatterns');
            if (platform === 'win32') {
                gelp.withArgs('editor').returns('Unity.exe');
                gelp.withArgs('editor_old').returns(pathutil.join('Editor', 'Unity.exe'));
            }
            else {
                gelp.withArgs('editor').returns(pathutil.join('Unity.app', 'Contents', 'MacOS', 'Unity'));
                gelp.withArgs('editor_old').returns(undefined);
            }
            basePath = 'fakePath';
            basePaths = ['fakePath', 'fakePath2'];
            levels = 4;
        });
        afterEach(() => {
            sandbox.restore();
        });
        describe('new instance', () => {
            let editorSearch, editorSearch2;
            beforeEach(() => {
                editorSearch = new EditorSearch(basePath, levels);
                editorSearch2 = new EditorSearch(basePaths, levels);
            });
            it('editorPaths should be set to empty Array', () => {
                expect(editorSearch.editorPaths).to.eql([]);
            });
            it('levels property should be set to ' + levels, () => {
                expect(editorSearch.levels).to.eql(levels);
            });
            it('startPath property should be set to ' + basePath, () => {
                expect(editorSearch.startPath).to.eql(basePath);
            });
            it('editorPaths should be set to empty Array', () => {
                expect(editorSearch2.editorPaths).to.eql([]);
            });
            it('levels property should be set to ' + levels, () => {
                expect(editorSearch2.levels).to.eql(levels);
            });
            it('startPath property should be set to ' + basePaths, () => {
                expect(editorSearch2.startPath).to.eql(basePaths);
            });
        });
        describe('setting values after creating new instance', () => {
            let editorSearch;
            beforeEach(() => {
                editorSearch = new EditorSearch(basePath, levels);
            });
            it('it should set new values of instance properties', () => {
                editorSearch.startPath = "newPath";
                editorSearch.levels = 6;
                expect(editorSearch.startPath).to.eql("newPath");
                expect(editorSearch.levels).to.eql(6);
            });
        });
        describe('editor search', () => {
            let editorSearch;
            beforeEach(() => {
                editorSearch = new EditorSearch(basePath, levels);
            });
            describe('search for editors when basePath doesn\'t exist', () => {
                it('should return empty Array in promise', (done) => {
                    editorSearch.search().then((res) => {
                        try {
                            expect(res).to.eql([]);
                            expect(editorSearch.getEditorPaths()).to.eql([]);
                            done();
                        }
                        catch (e) {
                            done(e);
                        }
                    });
                });
                it('should return empty Array in callback', (done) => {
                    editorSearch.search((err, res) => {
                        try {
                            expect(res).to.eql([]);
                            expect(editorSearch.getEditorPaths()).to.eql([]);
                            done();
                        }
                        catch (e) {
                            done(e);
                        }
                    });
                });
                it('should return empty Array in via postal event "done"', (done) => {
                    try {
                        editorSearch.search().then(() => {
                            expect(postal.publish).to.have.been.calledWith({
                                channel: 'EditorSearch',
                                topic: 'done',
                                data: []
                            });
                            done();
                        });
                    }
                    catch (e) {
                        done(e);
                    }
                });
            });
            describe('search for editors when given basePath throws error', () => {
                const error = {
                    code: 'foo',
                    message: 'Bar'
                };
                beforeEach(() => {
                    fsMock.setPathThatThrowsError(basePath, error);
                });
                it('should return empty Array', (done) => {
                    editorSearch.search().then(res => {
                        try {
                            expect(res).to.eql([]);
                            done();
                        }
                        catch (e) {
                            done(e);
                        }
                    });
                });
            });
            describe('search for editors when fs is simulated in the way that editors exist', () => {
                const trashFolderName = platform === 'win32' ? "$RECYCLE.BIN" : ".Trash";
                beforeEach(() => {
                    fsMock.setDir({
                        "fakePath": {
                            "random1": null,
                            "random2": null,
                            "UnityInstallations": {
                                "UnityOld": {
                                    "Unity.exe": null
                                },
                                "UnityNew": {
                                    "Editor": {
                                        "Unity.exe": null
                                    }
                                },
                                "UnityInstaledWithHub": {
                                    "Editor": {
                                        "Unity.exe": null,
                                    },
                                    "modules.json": null
                                },
                                "UnityMac": {
                                    "Unity.app": {
                                        "Contents": {
                                            "MacOS": {
                                                "Unity": null
                                            }
                                        }
                                    }
                                },
                                "UnityMacInstalledWithHub": {
                                    "modules.json": null,
                                    "Unity.app": {
                                        "Contents": {
                                            "MacOS": {
                                                "Unity": null
                                            }
                                        }
                                    }
                                }
                            },
                            "random3": {
                                "random_sub1": {
                                    "random_sub2": {
                                        "Editor": {
                                            "no_unity_here.exe": null
                                        }
                                    }
                                }
                            },
                            "random4": null,
                            [trashFolderName]: {
                                "win": {
                                    "Editor": {
                                        "Unity.exe": null
                                    }
                                },
                                "mac": {
                                    "Unity.app": {
                                        "Contents": {
                                            "MacOS": {
                                                "Unity": null
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    });
                });
                it('Find all Editors', (done) => {
                    editorSearch.search().then(res => {
                        if (platform === 'win32') {
                            try {
                                expect(res.length).to.eql(3);
                                done();
                            }
                            catch (e) {
                                done(e);
                            }
                        }
                        else if (platform === 'darwin') {
                            try {
                                expect(res.length).to.eql(2);
                                done();
                            }
                            catch (e) {
                                done(e);
                            }
                        }
                        else {
                            done();
                        }
                    });
                });
                it('Check if editor paths were cached', (done) => {
                    if (platform === 'win32') {
                        editorSearch.search().then((res) => {
                            try {
                                expect(editorSearch.getEditorPaths().length).to.eql(3);
                                done();
                            }
                            catch (e) {
                                done(e);
                            }
                        });
                    }
                    else if (platform === 'darwin') {
                        editorSearch.search().then((res) => {
                            try {
                                expect(editorSearch.getEditorPaths().length).to.eql(2);
                                done();
                            }
                            catch (e) {
                                done(e);
                            }
                        });
                    }
                    else {
                        done();
                    }
                });
                describe("Get editors installed only with hub", () => {
                    beforeEach(() => __awaiter(this, void 0, void 0, function* () {
                        isEditorInstalledWithHubStub = sinon.stub(editorUtils, 'isEditorInstalledWithHub').returns(true);
                        return yield editorSearch.search();
                    }));
                    afterEach(() => {
                        isEditorInstalledWithHubStub.restore();
                    });
                    it('promise without passing paths', () => __awaiter(this, void 0, void 0, function* () {
                        let res = yield editorSearch.getEditorsInstalledWithHub();
                        expect(res.length).to.eql(platform === 'win32' ? 3 : 2);
                    }));
                    it('promise with paths as argument', () => __awaiter(this, void 0, void 0, function* () {
                        let res = yield editorSearch.getEditorsInstalledWithHub(editorSearch.getEditorPaths());
                        expect(res.length).to.eql(platform === 'win32' ? 3 : 2);
                    }));
                    it('callback without passing paths', (done) => {
                        editorSearch.getEditorsInstalledWithHub((err, res) => {
                            try {
                                expect(res.length).to.eql(platform === 'win32' ? 3 : 2);
                                done();
                            }
                            catch (e) {
                                done(e);
                            }
                        });
                    });
                    it('callback with paths as argument', (done) => {
                        editorSearch.getEditorsInstalledWithHub(editorSearch.getEditorPaths(), (err, res) => {
                            try {
                                expect(res.length).to.eql(platform === 'win32' ? 3 : 2);
                                done();
                            }
                            catch (e) {
                                done(e);
                            }
                        });
                    });
                });
                it('Should find Editor when given install folder too', (done) => {
                    if (platform === 'win32') {
                        new EditorSearch(pathutil.join('fakePath', 'UnityInstallations', 'UnityOld'), 2).search().then((res) => {
                            try {
                                expect(res.length).to.eql(1);
                                done();
                            }
                            catch (e) {
                                done(e);
                            }
                        });
                    }
                    else if (platform === 'darwin') {
                        new EditorSearch(pathutil.join('fakePath', 'UnityInstallations', 'UnityMac'), 2).search().then((res) => {
                            try {
                                expect(res.length).to.eql(1);
                                done();
                            }
                            catch (e) {
                                done(e);
                            }
                        });
                    }
                    else {
                        done();
                    }
                });
            });
        });
    });
}
//# sourceMappingURL=editorSearch.spec.js.map