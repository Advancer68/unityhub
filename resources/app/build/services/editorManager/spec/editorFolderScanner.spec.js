const EditorFolderScanner = require('../lib/editorFolderScanner');
const { fs } = require('../../../fileSystem');
const path = require('path');
const FsMock = require('./fsMock');
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
    .use(require('sinon-chai'))
    .use(require('chai-as-promised'));
describe('EditorFolderScanner', () => {
    let sandbox, fsMock, basePath;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        fsMock = new FsMock(fs);
        fsMock.stubWithSandbox(sandbox);
        basePath = 'my/fun/path';
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('new instance', () => {
        it('should set editorFolders to empty array', () => {
            const editorFolderScanner = new EditorFolderScanner(basePath);
            expect(editorFolderScanner.editorFolders).to.eql([]);
        });
    });
    describe('fetchEditorFolders', () => {
        let editorFolderScanner;
        beforeEach(() => {
            editorFolderScanner = new EditorFolderScanner(basePath);
        });
        describe('when folder is not found', () => {
            beforeEach(() => {
                return editorFolderScanner.fetchEditorFolders();
            });
            it('should return empty array', () => {
                expect(editorFolderScanner.editorFolders).to.eql([]);
            });
        });
        describe('when there was an error looking up the folder content', () => {
            it('should throw the error', () => {
                const error = { code: 'foo', message: 'Bar' };
                fsMock.setPathThatThrowsError(basePath, error);
                return expect(editorFolderScanner.fetchEditorFolders()).to.be.eventually.rejectedWith(error);
            });
        });
        describe('when the folder is empty', () => {
            beforeEach(() => {
                fsMock.setFilesForPath([], basePath);
                return editorFolderScanner.fetchEditorFolders();
            });
            it('should return an empty array', () => {
                expect(editorFolderScanner.editorFolders).to.eql([]);
            });
        });
        describe('when the folder contains invalid version formats', () => {
            let validFolderNames, invalidFolderNames;
            beforeEach(() => {
                validFolderNames = [
                    '5.6.1a1',
                    '5.6.1b2',
                    '5.6.0f3',
                    '5.6.3p4',
                    '2017.1.1a4',
                    '2017.2.2b3',
                    '2017.3.0f2',
                    '2017.1.3p1',
                    '2018.2.0a1'
                ];
                invalidFolderNames = [
                    'foo',
                    '2017',
                    'Unity 5.6',
                    '2017.3',
                    '2017.2.1',
                    '2017.3.1b',
                    '2018.2.1z4'
                ];
                const folderNames = validFolderNames.concat(invalidFolderNames);
                fsMock.setFilesForPath(folderNames, basePath);
                sandbox.stub(fs, 'existsSync').returns(true);
                return editorFolderScanner.fetchEditorFolders();
            });
            it('should filter out invalid names', () => {
                invalidFolderNames.forEach((name) => {
                    expect(editorFolderScanner.editorFolders).not.to.include(name);
                });
            });
            it('should keep valid names', () => {
                validFolderNames.forEach((name) => {
                    expect(editorFolderScanner.editorFolders).to.include(name);
                });
            });
        });
        describe('when the folder contains folders with no executable', () => {
            let foldersWithExecutable, foldersWithoutExecutable;
            beforeEach(() => {
                foldersWithExecutable = [
                    '2017.1.3p1',
                    '2018.2.0a1'
                ];
                foldersWithoutExecutable = [
                    '2018.1.3p1',
                    '2019.2.0a1'
                ];
                const folderNames = foldersWithExecutable.concat(foldersWithoutExecutable);
                fsMock.setFilesForPath(folderNames, basePath);
                sandbox.stub(fs, 'existsSync').callsFake((folder) => {
                    for (let i in foldersWithExecutable) {
                        if (folder.includes(foldersWithExecutable[i])) {
                            return true;
                        }
                    }
                    return false;
                });
                return editorFolderScanner.fetchEditorFolders();
            });
            it('should filter out folders without executable', () => {
                foldersWithoutExecutable.forEach((name) => {
                    expect(editorFolderScanner.editorFolders).not.to.include(name);
                });
            });
            it('should keep folders with executable', () => {
                foldersWithExecutable.forEach((name) => {
                    expect(editorFolderScanner.editorFolders).to.include(name);
                });
            });
        });
        describe('when the folder contains only valid version formats', () => {
            let folderNames;
            beforeEach(() => {
                folderNames = [
                    '5.6.1a1',
                    '5.6.1b2',
                    '5.6.0f3',
                    '5.6.3p4',
                    '2017.1.1a4',
                    '2017.2.2b3',
                    '2017.3.0f2',
                    '2017.1.3p1',
                    '2018.2.0a1'
                ];
                fsMock.setFilesForPath(folderNames, basePath);
                sandbox.stub(fs, 'existsSync').returns(true);
                return editorFolderScanner.fetchEditorFolders();
            });
            it('should return all folder names', () => {
                expect(editorFolderScanner.editorFolders).to.eql(folderNames);
            });
        });
    });
});
//# sourceMappingURL=editorFolderScanner.spec.js.map