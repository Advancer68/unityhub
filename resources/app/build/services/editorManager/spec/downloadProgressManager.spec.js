var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const localStorage = require('electron-json-storage');
const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const proxyquire = require('proxyquire');
chai.use(sinonChai);
chai.use(chaiAsPromised);
describe('downloadProgressManager', () => {
    let sandbox;
    let downloadProgressManager;
    let localStorageGetValue;
    let localStorageSetValue;
    let localStorageRemoveValue;
    let localStorageSetStub;
    let localStorageGetStub;
    let localStorageRemoveStub;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        localStorageGetStub = sandbox.stub(localStorage, 'get').callsFake(() => Promise.resolve(localStorageGetValue));
        localStorageSetStub = sandbox.stub(localStorage, 'set').resolves();
        sandbox.stub(localStorage, 'remove').resolves();
        downloadProgressManager = proxyquire('../lib/downloadProgressManager', {
            'localStorage': {
                get: localStorageGetStub,
                set: localStorageSetStub,
                remove: localStorageRemoveStub
            },
        });
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('isEditorDownloadComplete', () => {
        describe('editor download last status was in progress', () => {
            it('should return false', () => __awaiter(this, void 0, void 0, function* () {
                localStorageGetValue = 'IN_PROGRESS';
                const isDownloadComplete = yield downloadProgressManager.isEditorDownloadComplete('2019.1.1');
                expect(isDownloadComplete).to.equal(false);
            }));
        });
        describe('editor download progress is not found', () => {
            it('should return true', () => __awaiter(this, void 0, void 0, function* () {
                localStorageGetValue = undefined;
                const isDownloadComplete = yield downloadProgressManager.isEditorDownloadComplete('2019.1.1');
                expect(isDownloadComplete).to.equal(true);
            }));
        });
        describe('editor download progress is an empty object', () => {
            it('should return true', () => __awaiter(this, void 0, void 0, function* () {
                localStorageGetValue = {};
                const isDownloadComplete = yield downloadProgressManager.isEditorDownloadComplete('2019.1.1');
                expect(isDownloadComplete).to.equal(true);
            }));
        });
    });
    describe('startTrackingEditorDownloadProgress', () => {
        it('should set in progress value to local storage', () => __awaiter(this, void 0, void 0, function* () {
            localStorageSetValue = {};
            yield downloadProgressManager.startTrackingEditorDownloadProgress('2019.1.1');
            expect(localStorage.set).to.have.been.called;
        }));
    });
    describe('setEditorDownloadProgressToComplete', () => {
        it('should remove download progress from local storage', () => __awaiter(this, void 0, void 0, function* () {
            localStorageRemoveValue = {};
            yield downloadProgressManager.setEditorDownloadProgressToComplete('2019.1.1');
            expect(localStorage.remove).to.have.been.calledWith('downloadProgress:2019.1.1');
        }));
    });
});
//# sourceMappingURL=downloadProgressManager.spec.js.map