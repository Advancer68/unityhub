const settings = require('../../localSettings/localSettings');
const cloudConfig = require('../../cloudConfig/cloudConfig');
const localStorage = require('electron-json-storage');
const os = require('os');
const chai = require('chai');
const sinon = require('sinon');
const nock = require('nock');
const expect = chai.expect;
chai.use(require('sinon-chai')).use(require('chai-as-promised'));
const releaseService = require('../unityReleaseService');
describe('Release', () => {
    let sandbox, settingsMock;
    let mockStorage = {};
    let endpoint = '/releases.json';
    let response = {
        official: [
            {
                version: 'foo',
                downloadUrl: 'fancyOSBar',
                modules: [],
            }
        ],
    };
    let osResponse = [
        {
            releaseType: 'official',
            version: 'foo',
            lts: false,
            downloadUrl: 'fancyOSBar',
            checksum: undefined,
            downloadSize: undefined,
            installedSize: undefined,
            modules: [],
            parent: '',
            sync: ''
        },
    ];
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        settingsMock = {
            [settings.keys.CLOUD_ENVIRONMENT]: settings.cloudEnvironments.DEV,
            [settings.keys.RELEASES_REFRESH_INTERVAL]: 100,
        };
        sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);
        sandbox.stub(releaseService, 'startRefreshInterval').returns(Promise.resolve());
        sandbox.stub(localStorage, 'get').callsFake((key, callback) => { callback(null, mockStorage); });
        sandbox.stub(localStorage, 'set').callsFake((key, value, callback) => { callback(); });
        sandbox.stub(cloudConfig, 'data').get(() => { return { hub_installer_location: 'http://fake.com/' }; });
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('init', () => {
        beforeEach(() => {
            sandbox.stub(os, 'platform').returns('darwin');
        });
        it('should call the startRefreshInterval', () => {
            return releaseService.init().then(() => {
                expect(releaseService.startRefreshInterval).to.have.been.called;
            });
        });
        it('should set the default releases as releases when there is no cache', () => {
            mockStorage = null;
            releaseService.data = undefined;
            return releaseService.init().then(() => {
                expect(releaseService.releases).to.deep.equal(releaseService.getDefaultReleases());
            });
        });
        it('should set the cached releases as releases when there is cache', () => {
            mockStorage = response;
            releaseService.data = undefined;
            return releaseService.init().then(() => {
                expect(releaseService.releases).to.deep.equal(osResponse);
            });
        });
    });
    describe('refreshReleases', () => {
        let httpMock;
        beforeEach(() => {
            mockStorage = null;
            releaseService.init();
            httpMock = nock(cloudConfig.data.hub_installer_location);
        });
        afterEach(() => {
            nock.cleanAll();
        });
        it('should return a resolving promise when request succeeded', () => {
            httpMock.get(endpoint).reply(200);
            let promise = releaseService.refreshData();
            return expect(promise).to.eventually.be.fulfilled;
        });
        it('should return a resolving promise when request fails', () => {
            httpMock.get(endpoint).reply(400);
            let promise = releaseService.refreshData();
            return expect(promise).to.eventually.be.fulfilled;
        });
    });
    describe('getReleases', () => {
        let httpMock;
        beforeEach(() => {
            httpMock = nock(/.*/);
            mockStorage = null;
            sandbox.stub(os, 'platform').returns('darwin');
        });
        afterEach(() => {
            nock.cleanAll();
        });
        describe('when the request succeeds', () => {
            it('should set the releases with the response', () => {
                httpMock.get(/.*/).reply(200, response);
                releaseService.data = undefined;
                releaseService.init();
                releaseService.online = true;
                return releaseService.refreshData().then(() => {
                    expect(releaseService.releases.length).to.equal(1);
                    expect(releaseService.releases).to.deep.equal(osResponse);
                });
            });
            it('if the response is empty should return the default list of releases', () => {
                httpMock.get(/.*/).reply(200, {});
                releaseService.data = undefined;
                releaseService.init();
                releaseService.online = true;
                return releaseService.refreshData().then(() => {
                    expect(releaseService.releases.length).to.be.at.least(1);
                    expect(releaseService.releases).to.not.eql(osResponse);
                });
            });
            it('should also add the custom editors in progress to the releases', () => {
                sandbox.stub(releaseService, 'getCustomEditorsInProgress').returns([{ id: 'bar', version: 'bar' }]);
                httpMock.get(/.*/).reply(200, response);
                releaseService.data = undefined;
                releaseService.init();
                releaseService.online = true;
                return releaseService.refreshData().then(() => {
                    expect(releaseService.releases).to.eql([...osResponse, { id: 'bar', version: 'bar' }]);
                });
            });
        });
        describe('when the request fails', () => {
            it('should set the releases to the default list', () => {
                httpMock.get(/.*/).reply(400);
                releaseService.data = undefined;
                releaseService.init();
                releaseService.online = true;
                return releaseService.refreshData().then(() => {
                    expect(releaseService.releases).to.deep.equal(releaseService.getDefaultReleases());
                });
            });
        });
    });
    describe('startReleasesRefreshInterval', () => {
        beforeEach(() => {
            releaseService.startRefreshInterval.restore();
            mockStorage = null;
            releaseService.init();
        });
        xit('should call setInterval', () => {
            let clock = sandbox.useFakeTimers();
            let stub = sandbox.stub(releaseService, 'refreshData').returns(Promise.resolve());
            releaseService.startRefreshInterval().then(() => {
                clock.tick(settingsMock[settings.keys.RELEASES_REFRESH_INTERVAL] + 1);
                expect(stub).to.have.been.called.twice;
            });
        });
    });
    describe('get/setCustomEditorInfo', () => {
        it('should set the custom editor with all the given info and add some missing elements', () => {
            let version = '1234.5.6f1';
            sandbox.stub(releaseService, '_getReleaseInfoPerPlatform').returns(Promise.resolve({
                'version': version
            }));
            return releaseService.setCustomEditorInfo('1234.5.6f1', 'aaaaaa')
                .then(() => expect(releaseService.getCustomEditorInfo()).to.eql({
                'version': version,
                'id': version,
                'name': `Unity ${version}`
            }));
        });
    });
    describe('Custom Editors In Progress', () => {
        describe('Add to the list', () => {
            describe('when the list is empty', () => {
                it('should add the given custom editor to the list', () => {
                    let editor = {
                        version: '1234.5.6f1',
                        id: '1234.5.6f1'
                    };
                    releaseService.addCustomEditorInProgress(editor);
                    expect(releaseService.getCustomEditorsInProgress()).to.eql([editor]);
                });
            });
            describe('when the list is not empty', () => {
                it('should add the given custom editor to the list', () => {
                    let editor = {
                        version: '1234.5.6f1',
                        id: '1234.5.6f1'
                    };
                    releaseService.customEditorsInProgress = [editor];
                    let newEditor = {
                        version: '9876.5.4f1',
                        id: '9876.5.4f1'
                    };
                    releaseService.addCustomEditorInProgress(newEditor);
                    expect(releaseService.getCustomEditorsInProgress()).to.eql([editor, newEditor]);
                });
                it('should add the given custom editor to the list even if it is the same', () => {
                    let editor = {
                        version: '1234.5.6f1',
                        id: '1234.5.6f1'
                    };
                    releaseService.customEditorsInProgress = [editor];
                    let newEditor = {
                        version: '1234.5.6f1',
                        id: '1234.5.6f1'
                    };
                    releaseService.addCustomEditorInProgress(newEditor);
                    expect(releaseService.getCustomEditorsInProgress()).to.eql([editor, newEditor]);
                });
            });
        });
        describe('Remove from the list', () => {
            describe('when the list is empty', () => {
                it('should do nothing', () => {
                    let editor = {
                        version: '1234.5.6f1',
                        id: '1234.5.6f1'
                    };
                    releaseService.removeCustomEditorInProgress(editor);
                    expect(releaseService.getCustomEditorsInProgress()).to.eql([]);
                });
            });
            describe('when the list is not empty', () => {
                it('should remove an editor when it is present', () => {
                    let editor = {
                        version: '1234.5.6f1',
                        id: '1234.5.6f1'
                    };
                    releaseService.customEditorsInProgress = [editor];
                    releaseService.removeCustomEditorInProgress(editor);
                    expect(releaseService.getCustomEditorsInProgress()).to.eql([]);
                });
                it('should remove an editor when it is present among numerous', () => {
                    let editor = {
                        version: '9876.5.4f1',
                        id: '9876.5.4f1'
                    };
                    let editor2 = {
                        version: '1234.5.6f1',
                        id: '1234.5.6f1'
                    };
                    releaseService.customEditorsInProgress = [editor, editor2];
                    releaseService.removeCustomEditorInProgress(editor2);
                    expect(releaseService.getCustomEditorsInProgress()).to.eql([editor]);
                });
                it('should do nothing when trying to remove an editor not in the list', () => {
                    let editor = {
                        version: '9876.5.4f1',
                        id: '9876.5.4f1'
                    };
                    let editor2 = {
                        version: '1234.5.6f1',
                        id: '1234.5.6f1'
                    };
                    releaseService.customEditorsInProgress = [editor];
                    releaseService.removeCustomEditorInProgress(editor2);
                    expect(releaseService.getCustomEditorsInProgress()).to.eql([editor]);
                });
                it('should log an error when the editor is undefined', () => {
                    let editor2 = {
                        version: '1234.5.6f1',
                        id: '1234.5.6f1'
                    };
                    releaseService.customEditorsInProgress = [editor2];
                    releaseService.removeCustomEditorInProgress();
                    expect(releaseService.getCustomEditorsInProgress()).to.eql([editor2]);
                });
                it('should remove when the editor has version but not id', () => {
                    let editor2 = {
                        version: '1234.5.6f1',
                        id: '1234.5.6f1'
                    };
                    releaseService.customEditorsInProgress = [editor2];
                    releaseService.removeCustomEditorInProgress({ version: '1234.5.6f1' });
                    expect(releaseService.getCustomEditorsInProgress()).to.eql([]);
                });
                it('should not remove the editor if when the editor is invalid', () => {
                    let editor2 = {
                        version: '1234.5.6f1',
                        id: '1234.5.6f1'
                    };
                    releaseService.customEditorsInProgress = [editor2];
                    releaseService.removeCustomEditorInProgress({});
                    expect(releaseService.getCustomEditorsInProgress()).to.eql([editor2]);
                });
            });
        });
    });
});
//# sourceMappingURL=unityReleaseService.spec.js.map