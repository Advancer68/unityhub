const errorKey = Symbol();
class LocalStorageMock {
    constructor(localStorage) {
        this.localStorage = localStorage;
        this.data = {};
    }
    stubWithSandbox(sandbox) {
        sandbox.stub(this.localStorage, 'get').callsFake((key, callback) => {
            let result = this.data[key];
            if (!result) {
                result = {};
            }
            if (result[errorKey]) {
                return callback(result[errorKey]);
            }
            return callback(null, result);
        });
        sandbox.stub(this.localStorage, 'set').callsFake((key, value, callback) => {
            this.data[key] = value;
            callback(null);
        });
    }
    setKeyThatThrowsError(key, error) {
        this.data[key] = { [errorKey]: error };
    }
}
module.exports = LocalStorageMock;
//# sourceMappingURL=localStorageMock.js.map