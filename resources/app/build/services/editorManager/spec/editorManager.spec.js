var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const EditorList = require('../lib/editorList');
const unityInstaller = require('../../localInstaller/unityInstaller');
const unityDownload = require('../../localDownload/unityDownload');
const releaseService = require('../unityReleaseService');
const { hubFS } = require('../../../fileSystem');
const cloudAnalytics = require('../../cloudAnalytics/cloudAnalytics');
const editorUtils = require('../editorUtils');
const windowManager = require('../../../windowManager/windowManager');
const localSettings = require('../../../services/localSettings/localSettings');
const downloadProgressManager = require('../lib/downloadProgressManager');
const unityInstallState = require('../unityInstallState');
const path = require('path');
const proxyquire = require('proxyquire');
const storageStub = { get() { }, set() { } };
const eulaFetcherStub = { getEulaText() { } };
const editorManager = proxyquire('../editorManager', {
    'electron-json-storage': storageStub,
    './eulaFetcher': eulaFetcherStub
});
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
    .use(require('sinon-chai'))
    .use(require('chai-as-promised'));
describe('EditorManager', () => {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        sandbox.stub(editorUtils, 'findUnityVersion').callsFake((path) => {
            if (path === '/usr/test234') {
                return Promise.resolve({ version: '234' });
            }
            else {
                return Promise.resolve({ version: '123' });
            }
        });
        sandbox.stub(editorUtils, 'validateEditorFile').callsFake(() => Promise.resolve(true));
        sandbox.stub(cloudAnalytics, 'addEvent');
        sandbox.stub(releaseService, 'init').returns(Promise.resolve());
        windowManager.broadcastContent = sandbox.stub();
        windowManager.mainWindow.showOpenFileDialog = sandbox.stub();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('init', () => {
        beforeEach(() => {
            sandbox.stub(EditorList.prototype, 'init');
            sandbox.stub(editorManager, 'registerDownloadEvents');
            sandbox.stub(editorManager, '_initSecondaryInstallLocation');
            return editorManager.init();
        });
        it('should initialize releaseService', () => {
            expect(releaseService.init).to.have.been.called;
        });
        it('should initialize editorList', () => {
            expect(EditorList.prototype.init).to.have.been.called;
        });
        it('should register download events', () => {
            expect(editorManager.registerDownloadEvents).to.have.been.called;
        });
        it('should set secondary install location if machine wide setting found', () => {
            expect(editorManager._initSecondaryInstallLocation).to.have.been.called;
        });
    });
    describe('addEditor', () => {
        let location, skipSignatureCheck, versionInfo, newEditor, result;
        beforeEach(() => {
            location = 'my/located/editor/path';
            skipSignatureCheck = false;
            versionInfo = {
                version: 'foo',
            };
            newEditor = Object.assign({}, versionInfo, {
                location: [location],
                manual: true
            });
            sandbox.stub(editorManager, 'validateEditorFile').resolves();
            sandbox.stub(editorManager, 'findVersion').resolves(versionInfo);
            sandbox.stub(EditorList.prototype, 'addLocatedEditor').resolves();
            return editorManager.addEditor(location, skipSignatureCheck)
                .then((_result) => result = _result);
        });
        it('should validate editor location', () => {
            expect(editorManager.validateEditorFile).to.have.been.called;
        });
        it('should generate editor information based on passed location', () => {
            const generatedEditor = EditorList.prototype.addLocatedEditor.args[0][0];
            expect(generatedEditor.version).to.equal(versionInfo.version);
        });
        it('should add editor to list of editors', () => {
            expect(EditorList.prototype.addLocatedEditor).to.have.been.calledWith(newEditor);
        });
        it('should return added editor', () => {
            expect(result).to.eql(newEditor);
        });
    });
    describe('removeEditor', () => {
        let editor;
        beforeEach(() => {
            editor = {
                version: 'foo',
                bar: 'baz'
            };
            sandbox.stub(editorManager, 'availableEditors').get(() => ({ foo: editor, bar: { version: 'bar' } }));
            sandbox.stub(EditorList.prototype, 'removeLocatedEditor').resolves();
            return editorManager.removeEditor(editor.version);
        });
        it('should remove editor from list', () => {
            expect(EditorList.prototype.removeLocatedEditor).to.have.been.calledWith(editor);
        });
    });
    describe('uninstallEditor', () => {
        let editor;
        beforeEach(() => {
            editor = {
                version: '2017.4.0a1',
                location: ['/my/fun/path']
            };
            sandbox.stub(unityInstaller, 'uninstall').resolves();
            sandbox.stub(unityInstaller.platformDependent, 'prepareInstallService').resolves();
        });
        it('when version is not available should reject', () => {
            return expect(editorManager.uninstallEditor(editor.version)).to.eventually.be.rejected;
        });
        it('when editor was manually added should resolve', () => {
            editor.manual = true;
            editorManager.availableEditors[editor.version] = editor;
            return expect(editorManager.uninstallEditor(editor.version)).to.eventually.be.fulfilled;
        });
        describe('when editor is valid', () => {
            beforeEach(() => {
                sandbox.stub(editorManager, 'availableEditors').get(() => ({ [editor.version]: editor }));
                sandbox.stub(EditorList.prototype, 'removeInstalledEditor');
                return editorManager.uninstallEditor(editor.version);
            });
            it('should uninstall editor', () => {
                expect(unityInstaller.uninstall).to.have.been.called;
            });
            it('should remove editor from list', () => {
                expect(EditorList.prototype.removeInstalledEditor).to.have.been.calledWith(editor);
            });
        });
    });
    describe('checkAutoLocate', () => {
        let versionInfo, availableEditors;
        beforeEach(() => {
            versionInfo = {
                version: 'foo',
            };
            editorManager.autoLocateInfo = undefined;
            availableEditors = {};
            sandbox.stub(editorManager, 'availableEditors').get(() => availableEditors);
            sandbox.stub(editorManager, 'findVersion').resolves(versionInfo);
        });
        it('should call addEditor if the version does not exist', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(editorManager, 'addEditor').resolves();
            yield editorManager.checkAutoLocate('bar');
            expect(editorManager.addEditor).to.have.been.called;
        }));
        it('should call addEditor if the version exists but location with another locations', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(editorManager, 'addEditor').resolves();
            availableEditors.foo = { location: ['bar'] };
            yield editorManager.checkAutoLocate('test');
            expect(editorManager.addEditor).to.have.been.called;
        }));
        it('should not set the autoLocateInfo if the version exists with the same location', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(editorManager, 'addEditor');
            availableEditors.foo = { location: ['test'] };
            yield editorManager.checkAutoLocate('test');
            expect(editorManager.addEditor).not.to.have.been.called;
        }));
    });
    describe('getEditor', () => {
        let editor, availableEditors;
        beforeEach(() => {
            editor = {
                version: 'foo'
            };
            availableEditors = {};
            sandbox.stub(editorManager, 'availableEditors').get(() => availableEditors);
        });
        describe('when editor is unavailable', () => {
            it('when editor list is empty should reject with proper error', () => {
                return editorManager.getEditor(editor.version).catch((error) => {
                    expect(error.code).to.equal(editorManager.errorCodes.NO_EDITORS_FOUND);
                });
            });
            it('when editor list is not empty should reject with proper error', () => {
                availableEditors = { bar: { version: 'bar' } };
                return editorManager.getEditor(editor.version).catch((error) => {
                    expect(error.code).to.equal(editorManager.errorCodes.VERSION_NOT_FOUND);
                });
            });
        });
        describe('when editor is available', () => {
            beforeEach(() => {
                availableEditors = { foo: editor };
            });
            it('should return editor', () => {
                return expect(editorManager.getEditor(editor.version)).to.eventually.eql(editor);
            });
        });
    });
    describe('getDefaultEditorVersion', () => {
        it('should resolve default editor version', () => {
            const defaultEditor = { version: 'foo' };
            sandbox.stub(EditorList.prototype, 'latest').get(() => defaultEditor);
            return expect(editorManager.getDefaultEditorVersion()).to.eventually.eql(defaultEditor.version);
        });
    });
    describe('getDefaultEditor', () => {
        it('should resolve default editor ', () => {
            const defaultEditor = { version: 'foo' };
            sandbox.stub(EditorList.prototype, 'latest').get(() => defaultEditor);
            return expect(editorManager.getDefaultEditor()).to.eventually.eql(defaultEditor);
        });
    });
    describe('downloadUnityEditor', () => {
        const editor = { id: 'editorId', downloadSize: 1, installedSize: 1 };
        const modules = [{ downloadSize: 1, installedSize: 1 }, { downloadSize: 1, installedSize: 1 }];
        const downloadDest = 'C:\\somewhere\\around\\here';
        describe('should succesfully start install process', () => {
            beforeEach(() => {
                sandbox.stub(downloadProgressManager, 'startTrackingEditorDownloadProgress').resolves();
                sandbox.stub(unityInstallState, 'addInstall');
                sandbox.stub(hubFS, 'getTmpDirPath').returns(downloadDest);
                sandbox.stub(editorManager, 'hasEnoughSpaceOnDisk');
                sandbox.stub(unityInstaller.platformDependent, 'prepareInstallService').resolves();
                sandbox.stub(unityDownload, 'copyUnityEditor').resolves([downloadDest, [{ module1: 'module1' }, { module: 'other' }]]);
                sandbox.stub(editorManager, 'getDefaultReleasesModules').returns([{ id: 'something' }, { module1: 'something' }]);
                sandbox.stub(editorManager, 'installEditor').resolves();
                sandbox.stub(unityDownload, 'downloadUnityEditor');
            });
            it('should correctly call functions with arguments', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(hubFS, 'startsWithHttp').returns(false);
                yield editorManager.downloadUnityEditor(editor, modules);
                expect(downloadProgressManager.startTrackingEditorDownloadProgress).to.have.been.called;
                expect(unityInstallState.addInstall).to.have.been.calledWith(editor.id);
                expect(editorManager.hasEnoughSpaceOnDisk).to.have.been.calledWith(editor, modules, downloadDest, false);
                expect(unityDownload.copyUnityEditor).to.have.been.calledWith(editor, modules, downloadDest);
            }));
            it('should correctly call the install editor function with the correct modules copied over', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(hubFS, 'startsWithHttp').returns(false);
                yield editorManager.downloadUnityEditor(editor, modules);
                expect(editorManager.installEditor).to.have.been.calledWith(editor.id, downloadDest, [{ module1: "module1" }, { module: "other" }]);
            }));
            it('should start download unity editor if the url starts with http', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(hubFS, 'startsWithHttp').returns(true);
                yield editorManager.downloadUnityEditor(editor, modules);
                expect(unityDownload.downloadUnityEditor).to.have.been.calledWith(editor, modules, downloadDest);
            }));
        });
        describe('should not succesfully start install process', () => {
            beforeEach(() => {
                sandbox.stub(releaseService, 'removeCustomEditorInProgress');
                sandbox.stub(hubFS, 'getTmpDirPath').returns(downloadDest);
                sandbox.stub(unityInstallState, 'installError');
            });
            it('should start catch error if download is canceled', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(downloadProgressManager, 'startTrackingEditorDownloadProgress').rejects();
                yield editorManager.downloadUnityEditor(editor, modules);
                expect(unityInstallState.installError).to.have.been.calledWith(editor.id);
            }));
            it('should start catch unhandled error when downloading/installing editors', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(downloadProgressManager, 'startTrackingEditorDownloadProgress').resolves();
                sandbox.stub(hubFS, 'startsWithHttp').returns(false);
                sandbox.stub(unityDownload, 'copyUnityEditor').resolves([downloadDest, []]);
                sandbox.stub(editorManager, 'installEditor').rejects();
                yield editorManager.downloadUnityEditor(editor, modules);
                expect(unityInstallState.installError).to.have.been.calledWith(editor.id);
                expect(releaseService.removeCustomEditorInProgress).to.have.been.calledWith(editor);
            }));
        });
    });
    describe('installEditor', () => {
        let newEditor, installerPath, modules, releases, modulesFromInstall, result;
        beforeEach(() => {
            newEditor = {
                version: 'foo',
                location: ['my/location'],
                isDefault: false,
                modules: [{ foo: 'bar' }]
            };
            installerPath = 'my/fun/path';
            modules = [{ foo: 'fun' }, { bar: 'baz' }];
            releases = [{ version: 'foo', bar: 'baz' }];
            modulesFromInstall = { successful: [{ foo: 'fun' }], failed: [] };
            sandbox.stub(releaseService, 'releases').get(() => releases);
            sandbox.stub(unityInstaller, 'installEditor').resolves({ modules: modulesFromInstall, restartRequired: false });
            sandbox.stub(EditorList.prototype, 'addInstalledEditor').resolves(newEditor);
            sandbox.stub(downloadProgressManager, 'setEditorDownloadProgressToComplete').resolves();
            return editorManager.installEditor(newEditor.version, installerPath, modules)
                .then((_result) => result = _result);
        });
        it('should install editor', () => {
            const destinationPath = sinon.match(newEditor.version);
            const args = [releases[0], installerPath, destinationPath, modules];
            expect(unityInstaller.installEditor).to.have.been.calledWith(...args);
        });
        it('should add editor to list', () => {
            expect(EditorList.prototype.addInstalledEditor).to.have.been.calledWith(newEditor.version);
        });
        it('should send ipc event', () => {
            const endEventName = unityInstaller.installerEvents.END;
            const args = [endEventName, newEditor.version, sinon.match.object];
            expect(windowManager.broadcastContent).to.have.been.calledWith(...args);
        });
        it('should return new editor', () => {
            const formattedEditorResponse = {
                editorPath: newEditor.location[0],
                isDefault: newEditor.isDefault,
                modules: modulesFromInstall,
                updatedModules: newEditor.modules,
                buildPlatforms: newEditor.buildPlatforms,
                restartRequired: false
            };
            expect(result).to.eql(formattedEditorResponse);
        });
        it('should set download progress of an editor to complete', () => {
            expect(downloadProgressManager.setEditorDownloadProgressToComplete).to.have.been.calledWith(newEditor.version);
        });
    });
    describe('installModules', () => {
        let editor, modules, modulesFromInstall, restartRequired, result;
        beforeEach(() => {
            editor = {
                version: 'foo',
                modules: [{ foo: 'bar' }],
                buildPlatforms: [{ foo1: 'bar1' }],
                location: ['my/path/to/foo/editor/unity']
            };
            modules = [{ foo: 'fun' }, { bar: 'baz' }];
            modulesFromInstall = { successful: [{ foo: 'fun' }], failed: [] };
            restartRequired = false;
            sandbox.stub(editorManager, 'availableEditors').get(() => ({ foo: editor }));
            sandbox.stub(unityInstaller, 'installModules').resolves({ modules: modulesFromInstall, restartRequired });
            sandbox.stub(EditorList.prototype, 'updateInstalledEditor').resolves(editor);
            return editorManager.installModules(editor.version, modules)
                .then((_result) => result = _result);
        });
        it('should install modules', () => {
            const baseInstallPath = sinon.match(editor.version);
            const args = [editor, baseInstallPath, modules];
            expect(unityInstaller.installModules).to.have.been.calledWith(...args);
        });
        it('should update editor in list', () => {
            expect(EditorList.prototype.updateInstalledEditor).to.have.been.calledWith(editor.version);
        });
        it('should send ipc event', () => {
            const endEventName = unityInstaller.installerEvents.MODULES_END;
            const args = [endEventName, editor.version, sinon.match.object];
            expect(windowManager.broadcastContent).to.have.been.calledWith(...args);
        });
        it('should return updated editor info', () => {
            const formattedEditorResponse = {
                modules: modulesFromInstall,
                updatedModules: editor.modules,
                buildPlatforms: editor.buildPlatforms,
                restartRequired
            };
            expect(result).to.eql(formattedEditorResponse);
        });
    });
    describe('getReleases', () => {
        let releaseServiceStub;
        beforeEach(() => {
            releaseServiceStub = sinon.stub(releaseService, 'releases').get(() => { return 'foo'; });
        });
        afterEach(() => {
            releaseServiceStub.restore();
        });
        it('should return the list of editors', () => {
            expect(editorManager.getReleases()).to.equal('foo');
        });
    });
    describe('getJobsInProgress', () => {
        let jobsBackup;
        before(() => {
            jobsBackup = unityInstaller.jobs;
        });
        afterEach(() => {
            unityInstaller.jobs = jobsBackup;
        });
        describe('when there are installing, uninstalling and queued versions in the jobs queue', () => {
            beforeEach(() => {
                unityInstaller.jobs = {
                    a: { editor: { version: 'a' }, status: 'QUEUED', jobType: 'editorInstall' },
                    b: { editor: { version: 'b' }, status: 'QUEUED', jobType: 'editorUninstall' },
                    c: { editor: { version: 'c' }, status: 'QUEUED', jobType: 'moduleInstall' },
                    d: { editor: { version: 'd' }, status: 'INSTALLING', jobType: 'editorInstall' },
                    e: { editor: { version: 'e' }, status: 'UNINSTALLING', jobType: 'editorInstall' },
                    f: { editor: { version: 'f' }, status: 'INVALID_STATUS', jobType: 'editorInstall' },
                    g: { editor: { version: 'g' }, status: 'QUEUED', jobType: 'INVALID_JOB_TYPE' },
                };
            });
            it('should return an array of in progress jobs with valid status', () => {
                let result = editorManager.getJobsInProgress();
                expect(result.length).to.eql(5);
                expect(result[0].status).to.eq('queued_installing');
                expect(result[1].status).to.eq('queued_uninstalling');
                expect(result[2].status).to.eq('modules_queued_installing');
                expect(result[3].status).to.eq('installing');
                expect(result[4].status).to.eq('uninstalling');
            });
            it('should return an array where the manual flag is false in all of them', () => {
                editorManager.getJobsInProgress().forEach((editor) => {
                    expect(editor.manual).to.eql(false);
                });
            });
        });
        describe('when the jobs are invalid', () => {
            beforeEach(() => {
                unityInstaller.jobs = {
                    a: { editor: { version: 'a' }, status: 'QUEUED', jobType: 'editorInstall' },
                    b: { editor: { version: 'b' }, status: 'QUEUED', jobType: 'editorUninstall' },
                    c: { editor: { version: 'c' }, status: 'QUEUED', jobType: 'moduleInstall' },
                    d: { editor: {}, status: 'INVALID_JOB_WITH_NO_EDITOR_VERSION', jobType: 'INVALID_JOB' },
                    e: { status: 'INVALID_JOB_WITH_NO_EDITOR', jobType: 'INVALID_JOB' },
                };
            });
            it('should ignore the invalid jobs and return the rest', () => {
                let result = editorManager.getJobsInProgress();
                expect(result.length).to.eql(3);
                expect(result[0].status).to.eq('queued_installing');
                expect(result[1].status).to.eq('queued_uninstalling');
                expect(result[2].status).to.eq('modules_queued_installing');
            });
        });
    });
    describe('getDefaultReleasesModules', () => {
        const foundRelease = { version: '2049.1.0f3', modules: [] };
        const releases = [{ version: '2077.2.3p5', modules: [] }, foundRelease];
        beforeEach(() => {
            sandbox.stub(releaseService, 'releases').get(() => releases);
        });
        it('should return the modules of the given version when it is found', () => {
            expect(editorManager.getDefaultReleasesModules(foundRelease.version)).to.equal(foundRelease.modules);
        });
        it('should return an empty array when the given version is not found', () => {
            expect(editorManager.getDefaultReleasesModules()).to.deep.equal([]);
            expect(editorManager.getDefaultReleasesModules('meow')).to.deep.equal([]);
        });
    });
    describe('hasEnoughSpaceOnDisk', () => {
        const editor = { downloadSize: 1, installedSize: 1 };
        const modules = [{ downloadSize: 1, installedSize: 1 }, { downloadSize: 1, installedSize: 1 }];
        const downloadDest = 'C:\\somewhere\\around\\here';
        const onlyModules = false;
        it('should reject if there is not enough space and all are on the same disk', () => {
            sandbox.stub(editorManager, 'getRequiredSizesPerDisk').returns(Promise.resolve({
                C: {
                    totalSizeRequired: 4,
                    available: 3
                }
            }));
            return expect(editorManager.hasEnoughSpaceOnDisk(editor, modules, downloadDest, onlyModules)).to.be.rejectedWith('Not enough space on disk to download and install');
        });
        it('should reject if there is not enough space and on one of the two disks', () => {
            sandbox.stub(editorManager, 'getRequiredSizesPerDisk').returns(Promise.resolve({
                "C": {
                    "available": 1,
                    "totalSizeRequired": 2
                },
                "D": {
                    "available": 5,
                    "totalSizeRequired": 2
                }
            }));
            return expect(editorManager.hasEnoughSpaceOnDisk(editor, modules, downloadDest, onlyModules)).to.be.rejectedWith('Not enough space on disk to download and install');
        });
        it('should resolve if there is enough space on all disks', () => {
            sandbox.stub(editorManager, 'getRequiredSizesPerDisk').returns(Promise.resolve({
                "C": {
                    "available": 3,
                    "totalSizeRequired": 2
                },
                "D": {
                    "available": 5,
                    "totalSizeRequired": 2
                }
            }));
            return expect(editorManager.hasEnoughSpaceOnDisk(editor, modules, downloadDest, true)).to.be.fulfilled;
        });
    });
    describe('getRequiredSizesPerDisk', () => {
        it('should return the required size on the same disk', () => {
            const editor = { downloadSize: 1, installedSize: 1 };
            const modules = [{ downloadSize: 1, installedSize: 1 }, { downloadSize: 1, installedSize: 1 }];
            const downloadDest = 'C:\\somewhere\\around\\here';
            const onlyModules = false;
            sandbox.stub(hubFS, 'getDiskUsage').returns(Promise.resolve({ available: 3, free: 2, total: 10 }));
            sandbox.stub(hubFS, 'getDiskRootPath').returns('C');
            return expect(editorManager.getRequiredSizesPerDisk(editor, modules, downloadDest, onlyModules)).to.eventually.eql({
                C: {
                    totalSizeRequired: 6,
                    available: 3
                }
            });
        });
        it('should return the required size on the same disk with only modules', () => {
            const editor = { downloadSize: 1, installedSize: 1 };
            const modules = [{ downloadSize: 1, installedSize: 1 }, { downloadSize: 1, installedSize: 1 }];
            const downloadDest = 'C:\\somewhere\\around\\here';
            const onlyModules = true;
            sandbox.stub(hubFS, 'getDiskUsage').returns(Promise.resolve({ available: 3, free: 2, total: 10 }));
            sandbox.stub(hubFS, 'getDiskRootPath').returns('C');
            return expect(editorManager.getRequiredSizesPerDisk(editor, modules, downloadDest, onlyModules)).to.eventually.eql({
                C: {
                    totalSizeRequired: 4,
                    available: 3
                }
            });
        });
        it('should return the required size on two disks when download and install paths are different', () => {
            const editor = { downloadSize: 1, installedSize: 1 };
            const modules = [{ downloadSize: 1, installedSize: 1 }, { downloadSize: 1, installedSize: 1 }];
            const downloadDest = 'C:\\somewhere\\around\\here';
            sandbox.stub(hubFS, 'getDiskUsage')
                .withArgs('C:\\').returns(Promise.resolve({ available: 3, free: 2, total: 10 }))
                .withArgs('D:\\').returns(Promise.resolve({ available: 5, free: 2, total: 10 }));
            sandbox.stub(hubFS, 'getDiskRootPath')
                .onFirstCall().returns('D:\\')
                .onSecondCall().returns('C:\\');
            return expect(editorManager.getRequiredSizesPerDisk(editor, modules, downloadDest, true)).to.eventually.eql({
                "C:\\": {
                    "available": 3,
                    "totalSizeRequired": 2
                },
                "D:\\": {
                    "available": 5,
                    "totalSizeRequired": 2
                }
            });
        });
    });
    describe('getSecondaryInstallLocation', () => {
        it('should get secondary install path', () => {
            const installPath = 'my/install/path';
            sandbox.stub(EditorList.prototype, 'secondaryInstallPath').get(() => installPath);
            expect(editorManager.getSecondaryInstallLocation()).to.equal(installPath);
        });
    });
    describe('setSecondaryInstallLocation', () => {
        it('should set secondary install path', () => {
            let installPath = 'my/install/path', newInstallPath = 'my/new/path';
            sandbox.stub(EditorList.prototype, 'setSecondaryInstallLocation').callsFake((location) => {
                installPath = location;
                return Promise.resolve();
            });
            return editorManager.setSecondaryInstallLocation(newInstallPath)
                .then(() => {
                expect(installPath).to.equal(newInstallPath);
            });
        });
    });
    describe('chooseSecondaryInstallLocation', () => {
        it('should open dialog to let user choose install location', () => {
            windowManager.mainWindow.showOpenFileDialog.resolves();
            return editorManager.chooseSecondaryInstallLocation()
                .then(() => {
                expect(windowManager.mainWindow.showOpenFileDialog).to.have.been.called;
            });
        });
    });
    describe('rememberSelectedModules', () => {
        it('should store the selected modules\' id in storage', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(storageStub, 'set').callsFake((key, val, cbk) => { cbk(undefined); });
            const selection = [{ id: 'a' }, { id: 'b' }, { id: 'c' }];
            yield editorManager.rememberSelectedModules(selection);
            expect(storageStub.set).to.have.been.calledWith('moduleSelection', ['a', 'b', 'c']);
        }));
    });
    describe('getPersistedModuleSelection', () => {
        describe('when storage returns other than an array', () => {
            it('should resolve with an empty array', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(storageStub, 'get').callsFake((key, cbk) => { cbk(undefined, {}); });
                expect(yield editorManager.getPersistedModuleSelection()).to.equal(null);
            }));
        });
        describe('when storage returns an array', () => {
            it('should resolve with storage content', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(storageStub, 'get').callsFake((key, cbk) => { cbk(undefined, ['a', 'b', 'c']); });
                expect(yield editorManager.getPersistedModuleSelection()).to.eql(['a', 'b', 'c']);
            }));
        });
    });
    describe('getDiskSpaceAvailable', () => {
        it('should call the hubFS.getDiskSpaceAvailable with the given folder', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(hubFS, 'getDiskSpaceAvailable');
            yield editorManager.getDiskSpaceAvailable('myFolder');
            expect(hubFS.getDiskSpaceAvailable).to.have.been.calledWith('myFolder');
        }));
        it('should call the hubFS.getDiskSpaceAvailable with currentInstallPath if no folder is given', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(hubFS, 'getDiskSpaceAvailable');
            yield editorManager.getDiskSpaceAvailable();
            expect(hubFS.getDiskSpaceAvailable).to.have.been.calledWith(editorManager.currentInstallPath);
        }));
    });
    describe('_initSecondaryInstallLocation', () => {
        let settingsMock;
        beforeEach(() => {
            settingsMock = {};
            sandbox.stub(localSettings, 'get').callsFake((key) => settingsMock[key]);
            sandbox.stub(editorManager, 'setSecondaryInstallLocation');
        });
        describe('When current secondary install location is empty', () => {
            beforeEach(() => {
                sandbox.stub(editorManager, 'getSecondaryInstallLocation').returns('');
            });
            describe('when configuration value is found', () => {
                it('should set secondary install location', () => {
                    const testPath = path.join('C:', 'Some', 'Path');
                    settingsMock[localSettings.keys.MACHINE_WIDE_SECONDARY_INSTALL_LOCATION] = testPath;
                    editorManager._initSecondaryInstallLocation();
                    expect(editorManager.setSecondaryInstallLocation.calledWith(testPath)).to.be.true;
                });
            });
            describe('when configuration value is empty and current secondary install location is empty', () => {
                it('should NOT set secondary install location', () => {
                    settingsMock[localSettings.keys.MACHINE_WIDE_SECONDARY_INSTALL_LOCATION] = '';
                    editorManager._initSecondaryInstallLocation();
                    expect(editorManager.setSecondaryInstallLocation).to.not.be.called;
                });
            });
            describe('when configuration value is undefined', () => {
                it('should NOT set secondary install location', () => {
                    expect(settingsMock[localSettings.keys.MACHINE_WIDE_SECONDARY_INSTALL_LOCATION]).to.be.undefined;
                    editorManager._initSecondaryInstallLocation();
                    expect(editorManager.setSecondaryInstallLocation).to.not.be.called;
                });
            });
        });
        describe('When current secondary install location is NOT empty', () => {
            beforeEach(() => {
                sandbox.stub(editorManager, 'getSecondaryInstallLocation').returns(path.join('C:', 'Current', 'Secondary', 'Path'));
            });
            describe('when configuration value is found', () => {
                it('should NOT set secondary install location', () => {
                    const testPath = path.join('C:', 'Some', 'Path');
                    settingsMock[localSettings.keys.MACHINE_WIDE_SECONDARY_INSTALL_LOCATION] = testPath;
                    editorManager._initSecondaryInstallLocation();
                    expect(editorManager.setSecondaryInstallLocation).to.not.be.called;
                });
            });
        });
    });
    describe('getEulaText', () => {
        it('should resolve with remote eula content', () => __awaiter(this, void 0, void 0, function* () {
            const eula = 'eula text';
            sandbox.stub(eulaFetcherStub, 'fetch').resolves(eula);
            const result = yield editorManager.getEulaText({});
            expect(result).to.equal(eula);
        }));
        it('should resolve with empty string when fetching from remote throw exception', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(eulaFetcherStub, 'fetch').throws();
            expect(yield editorManager.getEulaText({})).to.be.empty;
        }));
    });
    describe('getBuiltInPackagesPath', () => {
        it('should call getBuiltInPackagesPath of editorUtils', () => {
            const getBuiltInPackagesStub = sandbox.stub(editorUtils, 'getBuiltInPackagesPath');
            editorManager.getBuiltInPackagesPath();
            expect(getBuiltInPackagesStub).to.have.been.called;
        });
    });
    describe('getFailedInstalls', () => {
        let failedInstalls;
        beforeEach(() => {
            failedInstalls = [{ version: '123' }, { version: '456' }];
            sandbox.stub(unityInstallState, 'getFailedInstalls').returns(failedInstalls);
            sandbox.stub(unityInstallState, 'removeInstall');
        });
        it('should return failed installs', () => {
            expect(editorManager.getFailedInstalls()).to.eq(failedInstalls);
        });
        it('should clear failed installs list after fetching it to avoid displaying errors twice', () => {
            editorManager.getFailedInstalls();
            failedInstalls.forEach((installInfo) => {
                expect(unityInstallState.removeInstall).to.have.been.calledWith(installInfo.version);
            });
        });
    });
});
//# sourceMappingURL=editorManager.spec.js.map