const editorUtils = require('../editorUtils.js');
const { fs } = require('../../../fileSystem');
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const os = require('os');
const path = require('path');
if (os.platform() === 'darwin') {
    chai.use(chaiAsPromised);
    chai.use(sinonChai);
    describe('EditorUtils', () => {
        let sandbox;
        beforeEach(() => {
            sandbox = sinon.sandbox.create();
        });
        afterEach(() => {
            sandbox.restore();
        });
        describe.skip('findUnityVersion', () => {
            it('should return editor version', (done) => {
                editorUtils.findUnityVersion('/Applications/Unity/Unity.app')
                    .then(version => {
                    expect(version.version).to.equal('4.7.2f1');
                }).then(done, done);
            });
        });
        describe('findBuildPlatforms', () => {
            it('should find the build platform buildTarget and name of a given editor when it has only the default platform', () => {
                sandbox.stub(fs, 'readdir')
                    .onFirstCall().returns(Promise.resolve(['MacStandaloneSupport']))
                    .onSecondCall().returns(Promise.resolve([]));
                return editorUtils.findBuildPlatforms('/Applications/Unity/Hub/Editor/2018.1.0b5')
                    .then(buildPlatforms => {
                    expect(buildPlatforms).to.eql([
                        {
                            "buildTarget": "StandaloneOSX",
                            "dirName": "MacStandaloneSupport",
                            "name": "macOS"
                        }
                    ]);
                });
            });
            it('should find the build platform buildTarget, buildTargetGroup and name of a given editor when it has the facebook platform', () => {
                sandbox.stub(fs, 'readdir')
                    .onFirstCall().returns(Promise.resolve(['Facebook']))
                    .onSecondCall().returns(Promise.resolve([]));
                return editorUtils.findBuildPlatforms('/Applications/Unity/Hub/Editor/2018.1.0b5')
                    .then(buildPlatforms => {
                    expect(buildPlatforms).to.eql([
                        {
                            "buildTarget": "OSXUniversal",
                            "buildTargetGroup": "facebook",
                            "dirName": "Facebook",
                            "name": "Facebook"
                        }
                    ]);
                });
            });
            it('should find the build platform buildTargets and names of a given editor when it has multiple platforms', () => {
                sandbox.stub(fs, 'readdir')
                    .onFirstCall().returns(Promise.resolve(['AndroidPlayer']))
                    .onSecondCall().returns(Promise.resolve(['MacStandaloneSupport']));
                return editorUtils.findBuildPlatforms('/Applications/Unity/somewhere/2017.3.1f1/Editor')
                    .then(buildPlatforms => {
                    expect(buildPlatforms).to.eql([
                        {
                            "buildTarget": "Android",
                            "dirName": "AndroidPlayer",
                            "name": "Android",
                        },
                        {
                            "buildTarget": "StandaloneOSX",
                            "dirName": "MacStandaloneSupport",
                            "name": "macOS"
                        }
                    ]);
                });
            });
        });
        describe('validateEditorFile', () => {
            it.skip('should check the file', (done) => {
                editorUtils.validateEditorFile('/Applications/Unity/Unity.app')
                    .then(version => {
                    expect(version).to.equal(true);
                }).then(done, done);
            });
        });
        it('should reject file that does not exist', (done) => {
            sandbox.stub(fs, 'existsSync').returns(false);
            editorUtils.validateEditorFile('/Applications/Unity.app')
                .catch(error => {
                expect(error.errorCode).to.equal('ERROR.FILE_NOT_FOUND');
            }).then(done, done);
        });
        it('should reject file that is not an app', (done) => {
            sandbox.stub(fs, 'existsSync').returns(true);
            editorUtils.validateEditorFile('/Applications/Unity/Documentation/license.txt')
                .catch(error => {
                expect(error.errorCode).to.equal('ERROR.NOT_AN_APP');
            }).then(done, done);
        });
        it('should reject file not signed by Unity', (done) => {
            sandbox.stub(fs, 'existsSync').returns(true);
            editorUtils.validateEditorFile('/Applications/FaceTime.app')
                .catch(error => {
                expect(error.errorCode).to.equal('ERROR.NOT_SIGNED');
            }).then(done, done);
        });
        it.skip('should reject file that is not Unity editor when skipping signature check', (done) => {
            sandbox.stub(fs, 'existsSync').returns(true);
            sandbox.stub(fs, 'readFileSync').returns({ editorInformation: { CFBundleIdentifier: 'com.unity3d.UnityHub' } });
            editorUtils.validateEditorFile('/Applications/UnityHub.app', true)
                .catch(error => {
                expect(error.errorCode).to.equal('ERROR.NOT_AN_UNITY_APP');
            }).then(done, done);
        });
        it('should reject file that is not Unity editor and no Info.plist when skipping signature check', (done) => {
            sandbox.stub(fs, 'existsSync').returns(true);
            sandbox.stub(fs, 'readFileSync').throws('Error: Info.plist does not exist');
            editorUtils.validateEditorFile('/Applications/Unity/Unity.app', true)
                .catch(error => {
                expect(error.errorCode).to.equal('ERROR.FAILED_GET_INFO');
            }).then(done, done);
        });
        describe('getRegistryPackagesPath', () => {
            it('should get the correct path for the registry packages', () => {
                const editorPath = path.join('testing', 'path');
                const builtInPackagesPath = editorUtils.getRegistryPackagesPath(editorPath);
                expect(builtInPackagesPath).to.equal('testing/path/Contents/Resources/PackageManager/Editor');
            });
        });
        describe('getBuiltInPackagesPath', () => {
            it('should get the correct path for the built in packages', () => {
                const editorPath = path.join('testing', 'path');
                const builtInPackagesPath = editorUtils.getBuiltInPackagesPath(editorPath);
                expect(builtInPackagesPath).to.equal('testing/path/Contents/Resources/PackageManager/BuiltInPackages');
            });
        });
    });
}
//# sourceMappingURL=editorUtils_mac.spec.js.map