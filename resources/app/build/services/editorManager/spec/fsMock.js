const errorKey = Symbol();
const pathutil = require('path');
class FsMock {
    constructor(fs) {
        this.fs = fs;
        this.fileSystem = {};
    }
    stubWithSandbox(sandbox) {
        sandbox.stub(this.fs, 'readdir').callsFake((dirName) => {
            const dir = this.fileSystem[dirName];
            if (!dir) {
                return Promise.reject({
                    code: 'ENOENT'
                });
            }
            if (dir[errorKey]) {
                return Promise.reject(dir[errorKey]);
            }
            return Promise.resolve(dir);
        });
        sandbox.stub(this.fs, 'lstat').callsFake((path) => {
            const commonExtensions = [
                '.exe',
                '.app',
                '.dll',
                '.png',
            ];
            class Stats {
                constructor(dirPath) {
                    this.path = dirPath;
                    this.fs = this.fileSystem;
                }
                isDirectory() {
                    const directoryNames = this.path.split(/\\|\//g);
                    for (const name of directoryNames) {
                        if (commonExtensions.includes(pathutil.extname(name))) {
                            return false;
                        }
                    }
                    return true;
                }
            }
            return Promise.resolve(new Stats(path));
        });
        sandbox.stub(this.fs, 'pathExists').callsFake((path) => Promise.resolve(!!this.fileSystem[path]));
    }
    setFilesForPath(files, path) {
        this.fileSystem[path] = files;
    }
    setDir(fs, base = '') {
        if (Object.keys(fs).length === 0)
            return;
        Object.keys(fs).forEach((index) => {
            if (fs[index]) {
                if (typeof fs[index] === 'string') {
                    this.fileSystem[pathutil.join(base, index)] = [fs[index]];
                }
                else {
                    this.fileSystem[pathutil.join(base, index)] = Object.keys(fs[index]);
                    this.setDir(fs[index], pathutil.join(base, index));
                }
            }
            else {
                this.fileSystem[pathutil.join(base, index)] = [];
            }
        });
    }
    setPathThatThrowsError(path, error) {
        this.fileSystem[path] = {
            [errorKey]: error
        };
    }
}
module.exports = FsMock;
//# sourceMappingURL=fsMock.js.map