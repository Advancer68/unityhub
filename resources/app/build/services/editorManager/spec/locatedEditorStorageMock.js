const LocatedEditorStorage = require('../lib/locatedEditorStorage');
const addEditor = Symbol();
const removeEditor = Symbol();
class LocatedEditorStorageMock {
    constructor() {
        this.editors = {};
    }
    stubWithSandbox(sandbox) {
        sandbox.stub(LocatedEditorStorage.prototype, 'editors').get(() => this.editors);
        this.fetchEditors =
            sandbox.stub(LocatedEditorStorage.prototype, 'fetchEditors').resolves();
        sandbox.stub(LocatedEditorStorage.prototype, 'addEditor')
            .callsFake((editor) => this[addEditor](editor));
        sandbox.stub(LocatedEditorStorage.prototype, 'removeEditor')
            .callsFake((editor) => this[removeEditor](editor));
    }
    [addEditor](editor) {
        this.editors[editor.version] = editor;
        return Promise.resolve();
    }
    [removeEditor](editor) {
        delete this.editors[editor.version];
        return Promise.resolve();
    }
}
module.exports = LocatedEditorStorageMock;
//# sourceMappingURL=locatedEditorStorageMock.js.map