const logger = require('../../logger')('BugReporter');
const path = require('path');
const { exec, spawn } = require('child_process');
const { app } = require('electron');
class BugReporter {
    showBugReporter() {
        logger.info('spawn child process for bug reporter');
        let command = process.execPath;
        const param = [];
        if (command.indexOf('electron') !== -1) {
            param.push(`${path.resolve(__dirname, '..', '..', 'index.js')}`, '--bugReporter', '--inspect', `${app.argv.debug ? '--debugMode' : ''}`);
            const proc = spawn(command, param);
            proc.on('error', (err) => {
                if (err) {
                    logger.warn(err);
                }
            });
        }
        else {
            command = `"${command}" -- --bugReporter ${app.argv.debug ? '--debugMode' : ''}`;
            exec(command, (err) => {
                if (err) {
                    logger.warn(err);
                }
            });
        }
    }
}
module.exports = new BugReporter();
//# sourceMappingURL=bug-reporter.js.map