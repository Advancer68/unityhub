const deepRead = require('recursive-readdir');
const logger = require('../../logger')('PackageFinder');
const { editorHelper } = require('../../common/common');
class UnityPackageFinder {
    constructor() {
        this.defaultPaths = [editorHelper.getDefaultAssetStorePath()];
    }
    find(dir) {
        return deepRead(dir)
            .then(files => files.filter(file => file.endsWith('.unitypackage')))
            .catch((error) => {
            logger.warn(error);
            return [];
        });
    }
    findMany(dirs = []) {
        return Promise.all(dirs.map(this.find))
            .then(lists => lists.reduce((a, b) => a.concat(b), []).sort());
    }
}
module.exports = new UnityPackageFinder();
//# sourceMappingURL=packageFinder.js.map