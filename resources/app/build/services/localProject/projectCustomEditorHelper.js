var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { hubFS } = require('../../fileSystem');
const path = require('path');
const EditorApp = require('../editorApp/editorapp');
class projectCustomEditorHelper {
    constructor() {
        this.customEditors = {};
    }
    _isHubInfoExist(projectPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const isFileExist = yield hubFS.pathExists(projectPath, 'hubInfo.json');
            return isFileExist;
        });
    }
    getCustomEditorPath(projectPath) {
        return __awaiter(this, void 0, void 0, function* () {
            if (yield this._isHubInfoExist(projectPath)) {
                const hubInfo = JSON.parse(yield hubFS.readFile(path.join(projectPath, 'hubInfo.json')));
                if (hubInfo.customEditor) {
                    if (hubInfo.customEditor.absolutePath) {
                        return hubInfo.customEditor.absolutePath;
                    }
                    if (hubInfo.customEditor.relativePath) {
                        return path.join(projectPath, hubInfo.customEditor.relativePath);
                    }
                }
            }
            return '';
        });
    }
    _addCustomEditor(projectPath, editorPath) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!editorPath)
                editorPath = yield this.getCustomEditorPath(projectPath);
            this.customEditors[projectPath] = editorPath;
        });
    }
    checkForAndAddCustomEditorPath(projectPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const editorPath = yield this.getCustomEditorPath(projectPath);
            const isCustomEditor = editorPath !== '';
            if (isCustomEditor)
                this._addCustomEditor(projectPath, editorPath);
            return isCustomEditor;
        });
    }
    hasCustomEditor(projectPath) {
        return this.customEditors[projectPath];
    }
    getCustomEditorForProject(projectPath) {
        const editorPath = this.customEditors[projectPath];
        if (editorPath) {
            return Promise.resolve(new EditorApp({ editorPath }));
        }
        throw Error('No matching project path in list of custom editor');
    }
}
module.exports = new projectCustomEditorHelper();
//# sourceMappingURL=projectCustomEditorHelper.js.map