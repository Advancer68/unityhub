const _ = require('lodash');
const path = require('path');
const packages = require('./../packages');
const { fs } = require('../../../fileSystem');
const windowManager = require('../../../windowManager/windowManager');
const editorManager = require('../../editorManager/editorManager');
const fixture = require('./localProject.fixture');
const electron = require('electron');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
chai.use(chaiAsPromised);
describe('Packages', () => {
    let sandbox, settingsMock;
    const case0Path = 'case0Path';
    const case1Path = 'case1Path';
    const case2Path = 'case2Path';
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        windowManager.broadcastContent = sandbox.stub();
        sandbox.stub(editorManager, 'getDefaultEditorApp').returns(fixture.editorManager.getDefaultEditorApp());
        sandbox.stub(editorManager, 'getEditorApp').returns(fixture.editorManager.getEditorApp());
        sandbox.stub(fs, 'pathExists').callsFake((projPath) => {
            if (!projPath) {
                return Promise.resolve(true);
            }
            if (projPath === case0Path
                || projPath === path.join('projectWithLock', 'Temp', 'UnityLockfile')) {
                return Promise.resolve(true);
            }
            if (projPath === case1Path
                || projPath === path.join(case1Path, 'Packages', 'manifest.json')
                || projPath === path.join(case1Path, 'Packages')) {
                return Promise.resolve(false);
            }
            if (projPath === case2Path
                || projPath === path.join(case2Path, 'Packages', 'manifest.json')) {
                return Promise.resolve(false);
            }
            return Promise.resolve(true);
        });
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('needsManifestRelocation', () => {
        beforeEach(() => {
            windowManager.mainWindow.hide = sandbox.stub().resolves();
        });
        it('should return 0 if manifest does not need relocation', () => {
            return packages.needsManifestRelocation(case0Path).then(result => {
                expect(result).to.deep.eq({ code: 0 });
            });
        });
        it('should return 1 if manifest needs relocation', () => {
            return packages.needsManifestRelocation(case1Path).then(result => {
                expect(result).to.deep.eq({ code: 1 });
            });
        });
        it('should return 2 if package folder exists', () => {
            return packages.needsManifestRelocation(case2Path).then(result => {
                expect(result).to.deep.eq({ code: 2 });
            });
        });
    });
});
//# sourceMappingURL=packages.spec.js.map