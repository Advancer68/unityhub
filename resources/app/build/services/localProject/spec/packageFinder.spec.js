const os = require('os');
const path = require('path');
const { fs } = require('../../../fileSystem');
const sinon = require('sinon');
const expect = require('chai').expect;
const proxyquire = require('proxyquire').noPreserveCache();
describe('.UnityPackage Finder', function () {
    let sandbox;
    const pkgFolders = ['assets', 'standard', 'empty'];
    const standardPkgs = ['2D', 'Utility', 'Effects', 'Vehicles', 'Cameras'];
    const fakePaths = {
        darwin: '/Applications/Unity/Standard Assets',
        win32: path.join(os.homedir(), 'Program Files', 'Unity', 'Editor', 'Standard Packages'),
        linux: `/opt/Unity/Standard Assets`
    }[os.platform()];
    function prepareFinder(deepReadResult, fakeFs) {
        const deepReadStub = function () {
            return Promise.resolve(deepReadResult);
        };
        const fakeEditorManager = {
            getDefaultEditorApp: function () {
                return Promise.resolve({ standardAssets: fakePaths });
            }
        };
        return proxyquire('../packageFinder', { 'recursive-readdir': deepReadStub,
            '../editorManager/editorManager': fakeEditorManager,
            'fs': fakeFs || {}
        });
    }
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('find()', function () {
        function checkFind(finder, searchPath, sourcePkgs) {
            return finder.find(searchPath).then((packagePaths) => {
                expect(packagePaths.length).to.equal(sourcePkgs.length);
                packagePaths.forEach((file) => {
                    expect(file.startsWith(searchPath)).to.be.true;
                    expect(file.endsWith('.unitypackage')).to.be.true;
                });
            });
        }
        it(`should find all .unitypackages in the specified directory`, () => {
            const result = standardPkgs.map(pkg => path.join('standard', `${pkg}.unitypackage`));
            return checkFind(prepareFinder(result), 'standard', standardPkgs);
        });
        it(`should ignore all non-unitypackage files`, () => {
            let packages = standardPkgs.map(pkg => path.join('standard', `${pkg}.unitypackage`));
            let result = packages.concat(['some.txt', 'README.md']);
            return checkFind(prepareFinder(result), 'standard', standardPkgs);
        });
        it(`should resolve to an empty array if no packages found`, () => {
            return checkFind(prepareFinder(), 'empty', []);
        });
    });
    describe.skip('findMany()', function () {
        let finder = prepareFinder(['test1.unitypackage', 'test2.unitypackage']);
        before(() => {
            sandbox.spy(finder, 'find');
        });
        it(`should find from every directory specified`, () => {
            return finder.findMany(pkgFolders)
                .then(() => {
                expect(finder.find.firstCall.args[0]).to.equal(pkgFolders[0]);
                expect(finder.find.secondCall.args[0]).to.equal(pkgFolders[1]);
                expect(finder.find.thirdCall.args[0]).to.equal(pkgFolders[2]);
            });
        });
        it(`should return an alphanumerically ordered list with all packages' paths`, () => {
            return finder.findMany(pkgFolders)
                .then((list) => {
                expect(list[0]).to.equal('test1.unitypackage');
                expect(list[1]).to.equal('test1.unitypackage');
                expect(list[2]).to.equal('test1.unitypackage');
                expect(list[3]).to.equal('test2.unitypackage');
                expect(list[4]).to.equal('test2.unitypackage');
                expect(list[5]).to.equal('test2.unitypackage');
                expect(list.length).to.equal(6);
            });
        });
    });
});
//# sourceMappingURL=packageFinder.spec.js.map