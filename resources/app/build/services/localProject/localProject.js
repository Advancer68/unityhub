'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const localStorageAsync = require('electron-json-storage');
const path = require('path');
const _ = require('lodash');
const os = require('os');
const { fs, hubFS } = require('../../fileSystem');
const editorPref = require('../editorPref/editorPref');
const yaml = require('js-yaml');
const postal = require('postal');
const tarlib = require('tar');
const logger = require('../../logger')('LocalProject');
const editorManager = require('../editorManager/editorManager');
const promisify = require('es6-promisify');
const packageFinder = require('./packageFinder');
const packageHelper = require('./packageHelper');
const settings = require('../localSettings/localSettings');
const templateService = require('../unityTemplate/unityTemplateService');
const windowManager = require('../../windowManager/windowManager');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const packageService = require('./packages');
const hubIPCState = require('../localIPC/hubIPCState');
const licenseClient = require('../licenseService/licenseClientProxy');
const projectCustomEditorHelper = require('./projectCustomEditorHelper');
const outputService = require('../outputService');
const unityTemplateService = require('../unityTemplate/unityTemplateService');
const plasticSCMService = require('../plasticSCMService/plasticSCMService');
const PlasticSCMUtils = require('../plasticSCMService/plastic_scm_utils');
const kProjectSettingsFolderPath = 'ProjectSettings';
const kProjectAssetsFolderPath = 'Assets';
const kProjectVersionPath = path.join(kProjectSettingsFolderPath, 'ProjectVersion.txt');
const kProjectSettingsPath = path.join(kProjectSettingsFolderPath, 'ProjectSettings.asset');
const kProjectPlasticSCMPath = '.plastic';
const PROJECT_DIR_KEY = 'projectDir';
const PROJECTS_INFO = 'projectsInfo';
const PLASTIC_SCM_PLUGIN = 'com.unity.plasticscm-cn';
const packmanRegistry = 'https://packages-v2.unity.cn/';
const baseUserDefault = {
    projectType: '3D',
    defaultProjectName: 'New Unity Project',
    ShowNewProjectButton: true,
    platformPathNameSeparator: path.sep,
    isLaunching: 'true',
    showBackButtons: true
};
const createProjectResultCodes = {
    success: 0,
    missingProjectName: 1,
    invalidProjectName: 2,
    missingPath: 3,
    invalidPath: 4,
    folderNotEmpty: 5
};
const localStorage = {
    get: promisify(localStorageAsync.get, localStorageAsync),
    set: promisify(localStorageAsync.set, localStorageAsync),
};
const safeAction = function (func) {
    try {
        let result = func;
        if (!(func instanceof Promise)) {
            result = func();
        }
        return result;
    }
    catch (error) {
        logger.warn(`Unknown error running action: ${error}`);
        return Promise.reject(error);
    }
};
function onLaunchError() {
    windowManager.mainWindow.show();
}
class LockFileError extends Error {
    constructor(errorCode, projectName) {
        super();
        this.name = 'LockFileError';
        this.errorCode = errorCode;
        this.projectName = projectName;
    }
}
class LocalProject {
    constructor() {
        this.recentProjects = [];
        this.refreshUserDefault();
        this.createProjectResultCodes = createProjectResultCodes;
    }
    init() {
        logger.info('Init');
        this.getRecentProjects(true)
            .then(() => this.refreshUserDefault())
            .then(() => this.registerEvents());
    }
    plasticSCMEnabled(projectPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const plasticSCMPath = path.join(projectPath, kProjectPlasticSCMPath);
            return yield fs.existsSync(plasticSCMPath);
        });
    }
    getProjectData(projectPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const fsStatFunc = promisify(fs.stat);
            const fsReadFileFunc = promisify(fs.readFile);
            const versionPath = path.join(projectPath, kProjectVersionPath);
            const settingsPath = path.join(projectPath, kProjectSettingsPath);
            let projectVersion;
            let projectChangeset;
            let lastModified;
            try {
                const projectStat = yield fsStatFunc(projectPath);
                lastModified = projectStat.mtime;
            }
            catch (e) {
                lastModified = undefined;
            }
            try {
                const projectVersionFileContents = yield fsReadFileFunc(versionPath, 'utf8');
                const versionDoc = yaml.safeLoad(projectVersionFileContents);
                projectVersion = versionDoc.m_EditorVersion;
                const CHANGESET_REGEX = /\(([^)]+)\)/;
                const matches = CHANGESET_REGEX.exec(versionDoc.m_EditorVersionWithRevision);
                projectChangeset = matches ? matches[1] : null;
            }
            catch (e) {
                projectVersion = null;
            }
            const result = {
                path: projectPath,
                containingFolderPath: path.dirname(projectPath),
                version: projectVersion,
                changeset: projectChangeset,
                lastModified,
                isCustomEditor: yield projectCustomEditorHelper.checkForAndAddCustomEditorPath(projectPath),
                plasticSCMEnabled: yield this.plasticSCMEnabled(projectPath),
            };
            try {
                let projectSettingsData = yield fsReadFileFunc(settingsPath, 'utf8');
                projectSettingsData = projectSettingsData.split('\n').slice(3).join('\n');
                const playerSettings = yaml.safeLoad(projectSettingsData).PlayerSettings;
                result.title = path.basename(projectPath);
                result.cloudProjectId = playerSettings.cloudProjectId;
                result.resultorganizationId = playerSettings.organizationId;
                result.cloudEnabled = playerSettings.cloudEnabled === 0;
                result.localProjectId = playerSettings.productGUID;
            }
            catch (e) {
                result.title = path.basename(projectPath);
                result.cloudEnabled = false;
            }
            return _.omitBy(result, _.isNil);
        });
    }
    getRefreshInterval() {
        return settings.get(settings.keys.RECENT_PROJECT_INTERVAL);
    }
    getProjectsFromMemory() {
        return Promise.resolve(this.recentProjects);
    }
    getRecentProjects(forceRefresh = false, newProjectDirectory) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!forceRefresh && !windowManager.mainWindow.isFocused()) {
                return this.getProjectsFromMemory();
            }
            const recentProjectsPath = yield editorPref.getRecentProjects();
            const recentProjectsAll = yield Promise.all(recentProjectsPath.map(projectPath => this.getProjectData(projectPath)));
            const recentProjects = recentProjectsAll.filter((project) => project && project.lastModified !== undefined);
            if (!_.isEqual(recentProjects, this.recentProjects)) {
                this.recentProjects = recentProjects;
                windowManager.broadcastContent('recent-project.updated', recentProjects, newProjectDirectory);
            }
            return recentProjects;
        });
    }
    getProjectVersion(projectPath) {
        const fsReadFile = promisify(fs.readFile);
        const versionPath = path.join(projectPath, kProjectVersionPath);
        return fsReadFile(versionPath, 'utf8').then(fileContent => {
            try {
                const versionDoc = yaml.safeLoad(fileContent);
                return Promise.resolve(versionDoc.m_EditorVersion);
            }
            catch (err) {
                logger.info(err);
                return Promise.resolve();
            }
        }).catch((error) => {
            logger.info(error);
            return Promise.resolve();
        });
    }
    chooseProjectDialog() {
        let projectDirectory;
        return this.getUserDefault().then(userDefault => {
            const args = {
                title: 'Select a project to open...',
                defaultPath: userDefault.projectDirectory || os.homedir(),
                properties: ['openDirectory'],
            };
            return windowManager.mainWindow.showOpenFileDialog(args)
                .then((folderName) => { projectDirectory = folderName; })
                .then(() => localStorage.set(PROJECT_DIR_KEY, path.dirname(projectDirectory)))
                .then(() => this.refreshUserDefault())
                .then(() => this.getProjectVersion(projectDirectory))
                .then((editorVersion) => ({ projectDirectory, editorVersion, projectName: path.basename(projectDirectory) }));
        });
    }
    getProjectsInfo() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield localStorage.get(PROJECTS_INFO);
        });
    }
    setProjectsInfo(value) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield localStorage.set(PROJECTS_INFO, value);
        });
    }
    clearProjectInfo(projectPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const infos = yield localStorage.get(PROJECTS_INFO);
            delete infos[projectPath];
            yield this.setProjectsInfo(infos);
        });
    }
    showOpenOtherDialog(source) {
        return this.chooseProjectDialog()
            .then(({ projectDirectory, editorVersion }) => {
            const editor = editorManager.availableEditors[editorVersion];
            if (!editor) {
                return editorManager.getDefaultEditorVersion()
                    .then((preferredEditorVersion) => ({ projectDirectory, editorVersion, preferredEditorVersion }));
            }
            this.openProject(projectDirectory, editorVersion, { source });
            return false;
        });
    }
    chooseDirectory() {
        return this.getUserDefault().then(userDefault => {
            const args = {
                title: 'Select a directory where to save your project...',
                buttonLabel: 'Select Folder',
                defaultPath: userDefault.projectDirectory || os.homedir(),
                properties: ['openDirectory', 'createDirectory']
            };
            return windowManager.newProjectWindow.showOpenFileDialog(args);
        });
    }
    openProject(projectPath, editorVersion, options) {
        return __awaiter(this, void 0, void 0, function* () {
            options = options || {};
            const customArgs = yield this.getProjectsInfo();
            const cloudAnalyticsInfo = {};
            let projectData = {};
            try {
                yield this.validateOpenProject(projectPath);
                logger.info(`openProject projectPath: ${projectPath}, current editor:`, hubIPCState.modalEditor);
                const isModalEditorUndefined = !hubIPCState.modalEditor;
                if (!licenseClient.isLicenseValid()) {
                    throw { errorCode: 'ERROR.LAUNCH_EDITOR.LICENSE' };
                }
                postal.publish({
                    channel: 'app',
                    topic: 'project.opened',
                    data: { projectPath }
                });
                if (isModalEditorUndefined) {
                    let editorAppForProject;
                    if (projectCustomEditorHelper.hasCustomEditor(projectPath)) {
                        editorAppForProject = yield projectCustomEditorHelper.getCustomEditorForProject(projectPath);
                    }
                    else {
                        editorAppForProject = yield editorManager.getEditorApp(editorVersion);
                    }
                    yield editorAppForProject.openProject(projectPath, options, customArgs, onLaunchError);
                }
                windowManager.mainWindow.hide();
                projectData = yield this.addToRecentProjects(projectPath, editorVersion);
                let targetPlatform;
                if (options.buildPlatform) {
                    targetPlatform = options.buildPlatform.buildTargetGroup ? options.buildPlatform.buildTargetGroup : options.buildPlatform.buildTarget;
                }
                cloudAnalyticsInfo.status = 'Success';
                cloudAnalyticsInfo.target_platform = targetPlatform;
            }
            catch (err) {
                projectData = this.recentProjects.find((project) => project.path === projectPath);
                if (!projectData) {
                    projectData = yield this.getProjectData(projectPath);
                }
                cloudAnalyticsInfo.status = 'Error';
                if (err instanceof LockFileError) {
                    outputService.notifyContent('project.lock_file_error', err.projectName);
                }
                else {
                    throw err;
                }
            }
            finally {
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.OPEN_PROJECT,
                    msg: Object.assign({ editor_version: editorVersion, cloudprojectid: projectData ? projectData.cloudProjectId : '', organizationid: projectData ? projectData.organizationId : '', localprojectid: projectData ? projectData.localProjectId : '', source: options.source }, cloudAnalyticsInfo)
                });
            }
        });
    }
    addProject() {
        return __awaiter(this, void 0, void 0, function* () {
            const projectData = yield this.chooseProjectDialog();
            yield this.validateOpenProject(projectData.projectDirectory);
            yield editorPref.setMostRecentProject(projectData.projectDirectory);
            this.getRecentProjects(true, projectData.projectDirectory);
        });
    }
    addProjectWithDir(projectDirectory) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield this.validateOpenProject(projectDirectory);
                yield editorPref.setMostRecentProject(projectDirectory);
                this.getRecentProjects(true, projectDirectory);
                return Promise.resolve();
            }
            catch (error) {
                return Promise.reject(error);
            }
        });
    }
    removeProject(projectPath) {
        return __awaiter(this, void 0, void 0, function* () {
            yield editorPref.deleteProject(projectPath);
            this.getRecentProjects(true);
        });
    }
    closeHomeWindow() {
        windowManager.mainWindow.hide();
    }
    setOrganizationId(organizationId) {
        this.organizationId = organizationId;
    }
    getTemplatePath() {
        return __awaiter(this, void 0, void 0, function* () {
            return unityTemplateService._getTemplateDownloadDestination();
        });
    }
    getTemplateWithPlastic(template) {
        return __awaiter(this, void 0, void 0, function* () {
            const templateName = path.basename(template);
            const distPath = yield unityTemplateService._getTemplateDownloadDestination();
            const nameParts = templateName.split('-');
            if (nameParts.length != 2) {
                return template;
            }
            const templateNameWithPlastic = `${nameParts[0]}-plasticscm-${nameParts[1]}`;
            return path.join(distPath, templateNameWithPlastic);
        });
    }
    createPlasticTemplate(src, dist) {
        return __awaiter(this, void 0, void 0, function* () {
            const unpackPath = path.dirname(dist);
            const tempDistUnpackPath = path.join(unpackPath, `${new Date().getTime()}`);
            yield fs.ensureDir(tempDistUnpackPath);
            yield tarlib.extract({ file: src, C: tempDistUnpackPath, sync: true });
            const manifestPath = path.join(tempDistUnpackPath, 'package', 'ProjectData~', 'Packages', 'manifest.json');
            const manifestRawdata = fs.readFileSync(manifestPath);
            const manifest = JSON.parse(manifestRawdata);
            manifest.registry = packmanRegistry;
            manifest.dependencies[PLASTIC_SCM_PLUGIN] = yield PlasticSCMUtils.getRemotePluginVerion();
            fs.writeFileSync(manifestPath, JSON.stringify(manifest, null, 4));
            tarlib.create({ file: dist, C: tempDistUnpackPath, sync: true }, ['package']);
            fs.remove(tempDistUnpackPath);
        });
    }
    addPlasticSCMPlugin(projectPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const manifestPath = path.join(projectPath, 'Packages', 'manifest.json');
            const manifestRawdata = fs.readFileSync(manifestPath);
            const manifest = JSON.parse(manifestRawdata);
            manifest.registry = packmanRegistry;
            manifest.dependencies[PLASTIC_SCM_PLUGIN] = yield PlasticSCMUtils.getRemotePluginVerion();
            fs.writeFileSync(manifestPath, JSON.stringify(manifest, null, 4));
        });
    }
    toggleEnablePlasticSCM(enablePlasticSCM, template) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!enablePlasticSCM || !fs.existsSync(template)) {
                return Promise.resolve(template);
            }
            const templateWithPlastic = yield this.getTemplateWithPlastic(template);
            if (!fs.existsSync(templateWithPlastic)) {
                yield this.createPlasticTemplate(template, templateWithPlastic);
            }
            return Promise.resolve(templateWithPlastic);
        });
    }
    createProject(projectName, projectDirectory, projectTemplate, selectedPackages = [], defaultScene, editorVersion, enableAnalytics, enablePlasticSCM, regionUrl) {
        return __awaiter(this, void 0, void 0, function* () {
            let template;
            if (projectTemplate) {
                if (projectTemplate.code !== undefined) {
                    template = projectTemplate.code;
                }
                else {
                    template = projectTemplate.name;
                    if (projectTemplate.tar) {
                        template = projectTemplate.tar;
                    }
                }
            }
            else {
                template = templateService.defaultTemplateCode;
            }
            if (enablePlasticSCM) {
                try {
                    yield plasticSCMService.createWorkspaceWithBranch(projectName, projectDirectory, regionUrl);
                }
                catch (error) {
                    return Promise.reject(error);
                }
            }
            const selectedPackagesName = selectedPackages.map((selectedPackage) => path.basename(selectedPackage));
            return safeAction(() => {
                const fullProjectPath = path.join(projectDirectory, projectName);
                const organizationId = this.organizationId;
                logger.info(`createProject projectPath: ${fullProjectPath}, current editor: `, hubIPCState.modalEditor);
                const isModalEditorUndefined = !hubIPCState.modalEditor;
                if (!licenseClient.isLicenseValid()) {
                    return Promise.reject({ errorCode: 'ERROR.LAUNCH_EDITOR.LICENSE' });
                }
                postal.publish({
                    channel: 'app',
                    topic: 'project.create',
                    data: { projectName, projectDirectory, projectType: template, selectedPackages, defaultScene, organizationId }
                });
                let returnPromise = Promise.resolve();
                if (isModalEditorUndefined) {
                    this.toggleEnablePlasticSCM(enablePlasticSCM, template).then(newTemplate => {
                        logger.info('createProject with template', newTemplate);
                        returnPromise = editorManager.getEditorApp(editorVersion)
                            .then(editorApp => editorApp.createProject(fullProjectPath, selectedPackages, newTemplate, organizationId, onLaunchError));
                    });
                }
                return returnPromise
                    .then(() => {
                    windowManager.mainWindow.hide();
                    windowManager.onboardingWindow.hide();
                })
                    .then(() => localStorage.set(PROJECT_DIR_KEY, projectDirectory))
                    .then(() => this.refreshUserDefault())
                    .then(() => this.addToRecentProjects(fullProjectPath, editorVersion))
                    .then(() => {
                    cloudAnalytics.addEvent({
                        type: cloudAnalytics.eventTypes.NEW_PROJECT,
                        msg: {
                            editor_version: editorVersion,
                            template_id: `${projectTemplate.name || projectTemplate.code}`,
                            organizationid: organizationId,
                            localprojectid: undefined,
                            project_type: 'normal',
                            packages: selectedPackagesName,
                            enable_analytics: enableAnalytics,
                            status: 'Success',
                        }
                    });
                    if (enablePlasticSCM && !projectTemplate.tar) {
                        this.addProjectToPlasticAfterCreated(fullProjectPath);
                    }
                });
            }).catch((err) => {
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.NEW_PROJECT,
                    msg: {
                        editor_version: editorVersion,
                        template_id: `${template}`,
                        organizationid: this.organizationId,
                        localprojectid: undefined,
                        project_type: 'normal',
                        packages: selectedPackagesName,
                        enable_analytics: enableAnalytics,
                        status: 'Error',
                    }
                });
                return Promise.reject(err);
            });
        });
    }
    addProjectToPlasticAfterCreated(fullProjectPath) {
        const maxIteratorTime = 1000 * 20;
        const startTime = new Date().getTime();
        const checkAndSetPlastic = setInterval(() => {
            const manifestPath = path.join(fullProjectPath, 'Packages', 'manifest.json');
            const notSupportedManifestPath = path.join(fullProjectPath, 'UnityPackageManager', 'manifest.json');
            const thisTime = new Date().getTime();
            if (fs.existsSync(manifestPath)) {
                this.addPlasticSCMPlugin(fullProjectPath);
                clearInterval(checkAndSetPlastic);
            }
            if (fs.existsSync(notSupportedManifestPath)) {
                clearInterval(checkAndSetPlastic);
            }
            if (thisTime - startTime >= maxIteratorTime) {
                clearInterval(checkAndSetPlastic);
            }
        }, 500);
    }
    createTempProject(packages = [], editorVersion) {
        const isModalEditorDefined = !!hubIPCState.modalEditor;
        if (!licenseClient.isLicenseValid()) {
            return Promise.reject({ errorCode: 'ERROR.LAUNCH_EDITOR.LICENSE' });
        }
        postal.publish({
            channel: 'app',
            topic: 'project.createTemp',
            data: { packages }
        });
        if (isModalEditorDefined)
            return Promise.resolve();
        const projectTemplate = templateService.defaultTemplateCode;
        const selectedPackagesName = packages.map((selectedPackage) => path.basename(selectedPackage));
        return editorManager.getEditorApp(editorVersion)
            .then(editorApp => editorApp.createTempProject(packages, projectTemplate, onLaunchError))
            .then(() => windowManager.mainWindow.hide())
            .then(() => {
            cloudAnalytics.addEvent({
                type: cloudAnalytics.eventTypes.NEW_PROJECT,
                msg: {
                    editor_version: editorVersion,
                    template_id: `${projectTemplate}`,
                    organizationid: this.organizationId,
                    localprojectid: undefined,
                    project_type: 'temp',
                    packages: selectedPackagesName,
                    enable_analytics: false,
                    status: 'Success',
                }
            });
        })
            .catch((err) => {
            cloudAnalytics.addEvent({
                type: cloudAnalytics.eventTypes.NEW_PROJECT,
                msg: {
                    editor_version: editorVersion,
                    template_id: `${projectTemplate}`,
                    organizationid: this.organizationId,
                    localprojectid: undefined,
                    project_type: 'temp',
                    packages: selectedPackagesName,
                    enable_analytics: false,
                    status: 'Error',
                }
            });
            return Promise.reject(err);
        });
    }
    refreshUserDefault() {
        this.userDefault = baseUserDefault;
        this.userDefaultPromise = localStorage.get(PROJECT_DIR_KEY).then((storedProjectDir) => {
            this.userDefault.projectDirectory = _.isEmpty(storedProjectDir) ? os.homedir() : storedProjectDir;
            return this.userDefault;
        }).catch((err) => {
            logger.warn(err);
            this.userDefault.projectDirectory = os.homedir();
            return this.userDefault;
        });
        return this.userDefaultPromise;
    }
    getUserDefault() {
        return this.userDefaultPromise;
    }
    getUniqueProjectName(projectDir, projectName) {
        return path.basename(hubFS.getUniquePath(path.join(projectDir, projectName)));
    }
    getPackageList(customPaths = []) {
        const paths = packageFinder.defaultPaths.concat(customPaths);
        return packageFinder.findMany(paths)
            .then(files => Promise.all(files.map(filename => packageHelper.getPackageMetadata(filename))))
            .then(filesData => {
            this.cachedPackageList = filesData.map(file => ({ path: file.filename, packageInfo: file.json }));
            return this.cachedPackageList;
        });
    }
    registerEvents() {
        postal.subscribe({
            channel: 'app',
            topic: 'project.open',
            callback: (project) => this.openProject(project.projectPath, project.editorVersion, { source: 'tray' })
        });
        return Promise.resolve();
    }
    addToRecentProjects(projectPath, editorVersion, cloudProjectId, organizationId) {
        return __awaiter(this, void 0, void 0, function* () {
            let projectData = this.recentProjects.find((project) => project.path === projectPath);
            if (!projectData) {
                projectData = yield this.getProjectData(projectPath);
                projectData.lastModified = new Date();
                projectData.resultorganizationId = organizationId;
                this.recentProjects.unshift(projectData);
            }
            else {
                projectData.lastModified = new Date();
            }
            if (cloudProjectId) {
                projectData.cloudProjectId = cloudProjectId;
            }
            if (editorVersion) {
                projectData.version = editorVersion;
            }
            windowManager.broadcastContent('recent-project.updated', this.recentProjects, projectPath);
            return Promise.resolve(projectData);
        });
    }
    validateOpenProject(fullPath) {
        return this.validatePath(fullPath)
            .then(() => this.validateAssetPath(fullPath))
            .then(() => this.validateLockFile(fullPath));
    }
    validateAssetPath(fullPath) {
        return hubFS.isFilePresentPromise(path.join(fullPath, kProjectAssetsFolderPath))
            .catch(() => Promise.reject({ errorCode: 'ERROR.PROJECT.INVALID_PATH', params: { i18n: { path: fullPath } } }));
    }
    validatePath(fullPath) {
        return new Promise((resolve, reject) => {
            if (os.platform() === 'darwin') {
                if (fullPath.match(/^\/Users\/[^/]+\/?$/) ||
                    fullPath.match(/^\/Users\/[^/]+\/(Downloads|Library|Desktop|Documents)\/?$/)) {
                    reject({ errorCode: 'ERROR.PROJECT.INVALID_PATH', params: { i18n: { path: fullPath } } });
                }
            }
            resolve();
        }).then(() => hubFS.isFilePresentPromise(fullPath)
            .catch(() => Promise.reject({ errorCode: 'ERROR.PROJECT.PATH_NOT_FOUND', params: { i18n: { path: fullPath } } })));
    }
    validateLockFile(fullPath) {
        const lockFilePath = path.join(fullPath, 'Temp', 'UnityLockfile');
        let lockFileExists = true;
        return new Promise((resolve, reject) => hubFS.isFilePresentPromise(lockFilePath).catch(() => {
            lockFileExists = false;
            return resolve();
        }).then(() => {
            if (lockFileExists) {
                return hubFS.isFileUnlocked(lockFilePath)
                    .then(resolve)
                    .catch(() => {
                    const projectName = path.basename(fullPath);
                    reject(new LockFileError('ERROR.PROJECT.ALREADY_OPEN', projectName));
                });
            }
            return Promise.resolve();
        }));
    }
    needsManifestRelocation(projectPath) {
        return packageService.needsManifestRelocation(projectPath);
    }
    relocateManifest(projectPath) {
        return packageService.relocateManifest(projectPath);
    }
    validateCreateProject(name = '', directory = '') {
        name = name.replace(/(\/)\1+$/, '');
        directory = directory.replace(/(\/)\1+$/, '');
        const fullPath = path.join(directory, name);
        if (!name) {
            return createProjectResultCodes.missingProjectName;
        }
        else if (!hubFS.isValidFileName(name)) {
            return createProjectResultCodes.invalidProjectName;
        }
        else if (!directory) {
            return createProjectResultCodes.missingPath;
        }
        else if (!hubFS.isValidPath(directory)) {
            return createProjectResultCodes.invalidPath;
        }
        else if (fs.existsSync(fullPath)) {
            if (fs.readdirSync(fullPath) !== [])
                return createProjectResultCodes.folderNotEmpty;
        }
        return createProjectResultCodes.success;
    }
}
module.exports = new LocalProject();
//# sourceMappingURL=localProject.js.map