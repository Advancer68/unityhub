var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;
const path = require('path');
const projectCustomEditorHelper = require('./projectCustomEditorHelper');
const EditorApp = require('../editorApp/editorapp');
describe('ProjectCustomEditorHelper', () => {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });
    describe('getCustomEditorPath', () => {
        it('should return absolute path from hubInfo.json', () => __awaiter(this, void 0, void 0, function* () {
            const customEditorPath = yield projectCustomEditorHelper.getCustomEditorPath(path.join(__dirname, 'spec/resources/testCustomEditor_absolutePath'));
            expect(customEditorPath).to.equal('some/absolute/path');
        }));
        it('should return absolute path when both are defined', () => __awaiter(this, void 0, void 0, function* () {
            const customEditorPath = yield projectCustomEditorHelper.getCustomEditorPath(path.join(__dirname, 'spec/resources/testCustomEditor_bothPath'));
            expect(customEditorPath).to.equal('some/absolute/path');
        }));
        it('should still return an absolute path when relative path is given in hubInfo.json', () => __awaiter(this, void 0, void 0, function* () {
            const customEditorPath = yield projectCustomEditorHelper.getCustomEditorPath(path.join(__dirname, 'spec/resources/testCustomEditor_relativePath'));
            expect(customEditorPath).to.equal(path.join(__dirname, 'spec/resources/testCustomEditor_relativePath', 'some/relative/path'));
        }));
        it('should return nothing when relative nor absolute is defined', () => __awaiter(this, void 0, void 0, function* () {
            const customEditorPath = yield projectCustomEditorHelper.getCustomEditorPath(path.join(__dirname, 'spec/resources/testCustomEditor_noPath'));
            expect(customEditorPath).to.equal('');
        }));
        it('should return nothing when customEditor is not defined', () => __awaiter(this, void 0, void 0, function* () {
            const customEditorPath = yield projectCustomEditorHelper.getCustomEditorPath(path.join(__dirname, 'spec/resources/testCustomEditor_noCustomEditorKey'));
            expect(customEditorPath).to.equal('');
        }));
    });
    describe('getCustomEditorForProject', () => {
        beforeEach(() => {
            projectCustomEditorHelper.customEditors = { 'custom/editor': 'an editor app' };
        });
        it('should get custom editor', () => __awaiter(this, void 0, void 0, function* () {
            const editorApp = yield projectCustomEditorHelper.getCustomEditorForProject('custom/editor');
            expect(editorApp).to.deep.equal(new EditorApp({ editorPath: 'an editor app' }));
        }));
        it('should throw error if project path doesnt exist', () => {
            expect(() => projectCustomEditorHelper.getCustomEditorForProject('no/project')).to.throw('No matching project path in list of custom editor');
        });
    });
});
//# sourceMappingURL=projectCustomEditorHelper.spec.js.map