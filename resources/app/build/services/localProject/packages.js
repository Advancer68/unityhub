var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const path = require('path');
const { fs } = require('../../fileSystem');
const projectPackages = {
    oldFolder: 'UnityPackageManager',
    newFolder: 'Packages',
    manifestFile: 'manifest.json'
};
const needsManifestCodes = {
    success: 0,
    migrate: 1,
    packagesFolderExists: 2
};
const relocateManifestCodes = {
    success: 0,
    couldNotCreatePackagesFolder: 1,
    couldNotMoveManifest: 2
};
class Packages {
    needsManifestRelocation(projectPath) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!projectPath) {
                return false;
            }
            const newPackagesFolderPath = path.join(projectPath, projectPackages.newFolder);
            const oldManifestFilePath = path.join(projectPath, projectPackages.oldFolder, projectPackages.manifestFile);
            const newManifestFilePath = path.join(newPackagesFolderPath, projectPackages.manifestFile);
            const newManifestExists = yield fs.pathExists(newManifestFilePath);
            const oldManifestExists = yield fs.pathExists(oldManifestFilePath);
            const migrationNeeded = !newManifestExists && oldManifestExists;
            let code = needsManifestCodes.success;
            if (migrationNeeded) {
                code = needsManifestCodes.migrate;
                const exists = yield fs.pathExists(newPackagesFolderPath);
                if (exists) {
                    code = needsManifestCodes.packagesFolderExists;
                }
            }
            return { code };
        });
    }
    relocateManifest(projectPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const newPackagesFolderPath = path.join(projectPath, projectPackages.newFolder);
            const oldManifestFilePath = path.join(projectPath, projectPackages.oldFolder, projectPackages.manifestFile);
            const newManifestFilePath = path.join(newPackagesFolderPath, projectPackages.manifestFile);
            try {
                yield fs.mkdir(newPackagesFolderPath);
            }
            catch (error) {
                return { code: relocateManifestCodes.couldNotCreatePackagesFolder, error: error.message };
            }
            try {
                yield fs.rename(oldManifestFilePath, newManifestFilePath);
            }
            catch (error) {
                return { code: relocateManifestCodes.couldNotMoveManifest, error: error.message };
            }
            return { code: relocateManifestCodes.success };
        });
    }
}
module.exports = new Packages();
//# sourceMappingURL=packages.js.map