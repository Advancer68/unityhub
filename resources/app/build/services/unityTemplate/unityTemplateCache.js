const hubCache = require('../../common/hubCache');
const LOCAL_TEMPLATES_KEY = 'localTemplates';
const EDITOR_VERSION_INFO_KEY = 'editorVersionInfo';
class unityTemplateCache extends hubCache {
    setLocalTemplatesByEditor(editorVersion, templates) {
        this.set(this._getLocalTemplatesKey(editorVersion), templates);
    }
    getLocalTemplatesByEditor(editorVersion) {
        return this.get(this._getLocalTemplatesKey(editorVersion));
    }
    _getLocalTemplatesKey(editorVersion) {
        return `${LOCAL_TEMPLATES_KEY}:${editorVersion}`;
    }
    setLocalEditorVersionInfo(editorVersion, editorVersionInfo) {
        this.set(this._getEditorVersionInfoKey(editorVersion), editorVersionInfo);
    }
    getLocalEditorVersionInfo(editorVersion) {
        return this.get(this._getEditorVersionInfoKey(editorVersion));
    }
    _getEditorVersionInfoKey(editorVersion) {
        return `${EDITOR_VERSION_INFO_KEY}:${editorVersion}`;
    }
}
module.exports = new unityTemplateCache();
//# sourceMappingURL=unityTemplateCache.js.map