var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { app } = require('electron');
const path = require('path');
const os = require('os');
const tarlib = require('tar');
const { fs } = require('../../fileSystem');
const unityTemplateCacheService = require('./unityTemplateCache');
const unityPackageManagerService = require('../unityPackageManager/unityPackageManagerService');
const outputService = require('../outputService');
const editorManager = require('../editorManager/editorManager');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const hubIPCState = require('../localIPC/hubIPCState');
const logger = require('../../logger')('Templates');
const defaultUnityTemplates = require('./resources/defaultTemplates.json');
const TEMPLATE_STATES = {
    READY: 'ready',
    UPGRADE_AVAILABLE: 'upgradeAvailable',
    NOT_INSTALLED: 'notInstalled',
    INSTALLING: 'installing',
    UPGRADING: 'upgrading'
};
const TEMPLATES_PATH = {
    darwin: path.join('Contents', 'Resources', 'PackageManager', 'ProjectTemplates'),
    win32: path.join('..', 'Data', 'Resources', 'PackageManager', 'ProjectTemplates'),
    linux: path.join('..', 'Data', 'Resources', 'PackageManager', 'ProjectTemplates'),
}[os.platform()];
function extractTemplateFromTar(tarPath) {
    return new Promise((resolve) => {
        if (!/^.*\.tgz$/.test(tarPath)) {
            resolve();
            return;
        }
        fs.createReadStream(tarPath).pipe(new tarlib.Parse({
            filter: (p) => p === 'package/package.json'
        }).on('entry', (entry) => {
            let buff = '';
            entry.on('data', (chunk) => {
                buff += chunk;
            });
            entry.on('end', () => {
                const pkg = JSON.parse(buff);
                pkg.tar = tarPath;
                resolve(pkg);
            });
        }));
    });
}
function scanTemplates(projectTemplates) {
    return fs.readdir(projectTemplates).then((templateFolders) => {
        const promises = templateFolders.map((fileName) => {
            const fullPath = path.join(projectTemplates, fileName);
            return fs.stat(fullPath).then((stat) => {
                if (!stat.isDirectory()) {
                    return extractTemplateFromTar(fullPath);
                }
                return fs.readFile(path.join(fullPath, 'package.json'), 'utf8')
                    .then((data) => Promise.resolve(JSON.parse(data)))
                    .catch((err) => {
                    logger.warn('No package.json found for the template', err.message);
                    return Promise.resolve();
                });
            });
        });
        return Promise.all(promises).then((templates) => {
            templates = templates.filter(Boolean);
            return Promise.resolve(templates);
        });
    });
}
class Templates {
    get previewSuffix() { return '(Preview)'; }
    get defaultTemplateCode() {
        return defaultUnityTemplates.defaultTemplateCode;
    }
    getTemplateStates() { return TEMPLATE_STATES; }
    getLocalTemplatesForEditor(version) {
        return __awaiter(this, void 0, void 0, function* () {
            const localEditorTemplates = yield this._getTemplatesForEditorVersion(version);
            const localTemplates = yield this._getDownloadedTemplates(version);
            const allLocalTemplates = this._mergeLocalTemplates(localEditorTemplates, localTemplates);
            return allLocalTemplates;
        });
    }
    _mergeLocalTemplates(localEditorTemplates, localTemplates) {
        const localTemplateNames = localTemplates.map(localTemplate => localTemplate.name);
        const filteredLocalEditorTemplates = localEditorTemplates.filter((localEditorTemplate) => !localTemplateNames.includes(localEditorTemplate.name));
        return filteredLocalEditorTemplates.concat(localTemplates);
    }
    loadRemoteTemplatesAsynchronously(version) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const remoteTemplates = yield this.getRemoteTemplatesForEditor(version);
                const upgradableTemplates = yield this.getUpgradableTemplates(version);
                outputService.notifyContent('remote.templates.loaded', { remoteTemplates, upgradableTemplates, version });
            }
            catch (error) {
                logger.warn(`Could not get available templates: ${error.message}`);
                outputService.notifyContent('remote.templates.load_error', { version });
            }
        });
    }
    getRemoteTemplatesForEditor(version) {
        return __awaiter(this, void 0, void 0, function* () {
            const localTemplates = yield this.getLocalTemplatesForEditor(version);
            const unityInfo = yield this._getUnityInfo(version);
            const downloadableTemplates = yield this._getRemoteTemplates(unityInfo);
            return this._filterOutLocalTemplates(downloadableTemplates, localTemplates);
        });
    }
    getUpgradableTemplates(version) {
        return __awaiter(this, void 0, void 0, function* () {
            const localTemplates = yield this.getLocalTemplatesForEditor(version);
            const unityInfo = yield this._getUnityInfo(version);
            return this._processUpgradableTemplates(yield unityPackageManagerService.getUpgradableTemplates(localTemplates, unityInfo));
        });
    }
    downloadRemoteTemplate(templateName, version) {
        return __awaiter(this, void 0, void 0, function* () {
            const templateInfo = { name: templateName };
            try {
                outputService.notifyContent('template.download.start', { templateInfo, version });
                const unityInfo = yield this._getUnityInfo(version);
                const destination = yield this._getTemplateDownloadDestination();
                const downloadedTemplate = yield unityPackageManagerService.downloadRemoteTemplate(templateName, destination, unityInfo);
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.TEMPLATE_DOWNLOAD,
                    msg: {
                        editor_version: version,
                        template_id: `${templateName}@${downloadedTemplate.version}`,
                        status: 'Success'
                    }
                });
                outputService.notifyContent('template.download.end', { templateInfo: downloadedTemplate, version });
                return downloadedTemplate;
            }
            catch (error) {
                if (error.message === 'insufficient space') {
                    outputService.notifyContent('remote.templates.space_error', 'downloading', { templateInfo, version });
                }
                else {
                    outputService.notifyContent('remote.templates.install_error', { templateInfo, version });
                }
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.TEMPLATE_DOWNLOAD,
                    msg: {
                        editor_version: version,
                        template_id: templateName,
                        status: 'Error'
                    }
                });
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.ERROR,
                    msg: {
                        error_message: `Error downloading dynamic template: ${error.message}`,
                        error_type: 'exception'
                    }
                });
                logger.error(error.message);
            }
            return {};
        });
    }
    upgradeTemplate(templateName, version) {
        return __awaiter(this, void 0, void 0, function* () {
            const templateInfo = { name: templateName };
            try {
                outputService.notifyContent('template.upgrade.start', { templateInfo, version });
                const unityInfo = yield this._getUnityInfo(version);
                const destination = yield this._getTemplateDownloadDestination();
                const updatedTemplate = yield unityPackageManagerService.downloadRemoteTemplate(templateName, destination, unityInfo);
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.TEMPLATE_UPDATE,
                    msg: {
                        editor_version: version,
                        template_id: `${templateName}@${updatedTemplate.version}`,
                        status: 'Success'
                    }
                });
                outputService.notifyContent('template.upgrade.end', { templateInfo: updatedTemplate, version });
            }
            catch (error) {
                if (error.message === 'insufficient space') {
                    outputService.notifyContent('remote.templates.space_error', 'upgrading', { templateInfo, version });
                }
                else {
                    outputService.notifyContent('templates.upgrade_error', { templateInfo, version });
                }
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.TEMPLATE_UPDATE,
                    msg: {
                        editor_version: version,
                        template_id: templateName,
                        status: 'Error'
                    }
                });
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.ERROR,
                    msg: {
                        error_message: `Error upgrading dynamic template: ${error.message}`,
                        error_type: 'exception'
                    }
                });
                logger.error(error.message);
            }
            return {};
        });
    }
    _getTemplateDownloadDestination() {
        return __awaiter(this, void 0, void 0, function* () {
            const templatePath = path.join(app.getPath('userData'), 'Templates');
            const templateManifest = path.join(templatePath, 'manifest.json');
            const templateManifestExists = yield fs.pathExists(templateManifest);
            yield fs.ensureDir(templatePath);
            if (!templateManifestExists) {
                yield fs.writeJson(templateManifest, {});
            }
            return templatePath;
        });
    }
    _getEditorPath(version) {
        return __awaiter(this, void 0, void 0, function* () {
            if (hubIPCState.modalEditor && hubIPCState.editorLocation) {
                logger.info('using hub ipc editor location');
                return hubIPCState.editorLocation;
            }
            try {
                const editor = yield this._getEditor(version);
                const editorPath = editor.location[0];
                return editorPath;
            }
            catch (error) {
                logger.warn('failed to find the editor location, are you sure you have this editor installed on your machine ?');
                return '';
            }
        });
    }
    _getUnityInfo(version) {
        return __awaiter(this, void 0, void 0, function* () {
            const unityEditorInfoCache = unityTemplateCacheService.getLocalEditorVersionInfo(version);
            if (unityEditorInfoCache) {
                return unityEditorInfoCache;
            }
            const editorPath = yield this._getEditorPath(version);
            const registryPackagesPath = yield this._getRegistryPackagesPath(editorPath);
            const builtInPackagesPath = this._getBuiltInPackagesPath(editorPath);
            const unityEditorInfo = {
                unityVersion: version,
                editorPath: registryPackagesPath,
                builtInPackagesPath
            };
            unityTemplateCacheService.setLocalEditorVersionInfo(version, unityEditorInfo);
            return unityEditorInfo;
        });
    }
    _filterOutLocalTemplates(remoteTemplates, localTemplates) {
        return remoteTemplates
            .filter(remoteTemplate => !localTemplates.map(localTemplate => localTemplate.name).includes(remoteTemplate.name));
    }
    _getEditor(version) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield editorManager.getEditor(version);
        });
    }
    _getRegistryPackagesPath(editorPath) {
        return editorManager.getRegistryPackagesPath(editorPath);
    }
    _getBuiltInPackagesPath(editorPath) {
        return editorManager.getBuiltInPackagesPath(editorPath);
    }
    _getTemplatesForEditorVersion(version) {
        return __awaiter(this, void 0, void 0, function* () {
            const localTemplatesCache = unityTemplateCacheService.getLocalTemplatesByEditor(version);
            if (localTemplatesCache) {
                return localTemplatesCache;
            }
            const editorPath = yield this._getEditorPath(version);
            const localTemplates = yield this._getTemplatesForEditorPath(editorPath);
            unityTemplateCacheService.setLocalTemplatesByEditor(version, localTemplates);
            return localTemplates;
        });
    }
    _getTemplatesForEditorPath(editorPath) {
        if (!editorPath) {
            logger.warn('Editor not found, the hub will fall back to default templates');
            return Promise.resolve(defaultUnityTemplates.templates);
        }
        return scanTemplates(path.join(editorPath, TEMPLATES_PATH))
            .then(templates => this._processLocalEditorTemplates(templates))
            .then(templates => {
            if (templates.length > 0) {
                return Promise.resolve(this._setPreviewFlag(templates));
            }
            logger.info('No templates bundled with the editor');
            return Promise.resolve(defaultUnityTemplates.templates);
        }).catch((error) => {
            logger.warn(error);
            return Promise.resolve(defaultUnityTemplates.templates);
        });
    }
    _setPreviewFlag(templates) {
        return templates.map(template => {
            if (template.displayName && template.displayName.includes(this.previewSuffix)) {
                template.displayName = template.displayName.replace(this.previewSuffix, '').trim();
                template.isPreview = true;
            }
            return template;
        });
    }
    _getDownloadedTemplates(version) {
        return __awaiter(this, void 0, void 0, function* () {
            const unityInfo = yield this._getUnityInfo(version);
            const destination = yield this._getTemplateDownloadDestination();
            const localTemplates = yield unityPackageManagerService.getDownloadedTemplatesForEditorVersion(destination, unityInfo);
            return this._processLocalTemplates(localTemplates);
        });
    }
    _getRemoteTemplates(unityInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            return this._processDownloadableTemplates(yield unityPackageManagerService.getDownloadableTemplates(unityInfo));
        });
    }
    _processUpgradableTemplates(upgradableTemplates) {
        return this._processTemplates(upgradableTemplates, TEMPLATE_STATES.UPGRADE_AVAILABLE);
    }
    _processLocalEditorTemplates(localEditorTemplates) {
        return this._processTemplates(localEditorTemplates, TEMPLATE_STATES.READY);
    }
    _processLocalTemplates(localTemplates) {
        return this._processTemplates(localTemplates, TEMPLATE_STATES.READY);
    }
    _processDownloadableTemplates(downloadableTemplates) {
        return this._processTemplates(downloadableTemplates, TEMPLATE_STATES.NOT_INSTALLED);
    }
    _processTemplates(templates, status) {
        try {
            return templates.map(({ name, displayName, version, description, type, tar, tarball: { url } = { url: '' }, dynamicIcon }) => ({
                name,
                displayName,
                version,
                description,
                type,
                status,
                tar,
                url,
                dynamicIcon
            }));
        }
        catch (error) {
            logger.warn(`Could not process the templates : ${error.message}`);
            throw error;
        }
    }
}
module.exports = new Templates();
//# sourceMappingURL=unityTemplateService.js.map