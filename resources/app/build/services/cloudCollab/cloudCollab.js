var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const _ = require('lodash');
const os = require('os');
const path = require('path');
const postal = require('postal');
const promisify = require('es6-promisify');
const { fs, hubFS } = require('../../fileSystem');
const logger = require('../../logger')('CloudCollab');
const cloudCore = require('../cloudCore/cloudCore');
const cloudConfig = require('../cloudConfig/cloudConfig');
const collabApiClient = require('./lib/cloudCollab.api');
const editorManager = require('../editorManager/editorManager');
const localProject = require('../localProject/localProject');
const UnityAuth = require('../localAuth/auth');
const windowManager = require('../../windowManager/windowManager');
const tokenManager = require('../../tokenManager/tokenManager');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const hubIPCState = require('../localIPC/hubIPCState');
const licenseClient = require('../licenseService/licenseClientProxy');
function cleanupProjectTitle(projectTitle) {
    projectTitle = _.trimStart(projectTitle);
    return projectTitle.replace(/([?<>:*|"\\])|(\s|\.)$/g, '_');
}
function openCloudProjectFromEditor(organizationId, projectId, projectTitle, cloudProjectId, selectedDir, editorVersion, options) {
    projectTitle = cleanupProjectTitle(projectTitle);
    const projectPath = hubFS.getUniquePath(path.join(selectedDir, projectTitle));
    return promisify(fs.mkdir)(projectPath)
        .then(() => editorManager.getEditorApp(editorVersion))
        .then(editorApp => editorApp.openCloudProject(projectPath, projectId, organizationId, options))
        .then(() => windowManager.mainWindow.hide())
        .then(() => localProject.addToRecentProjects(projectPath, editorVersion, cloudProjectId, organizationId))
        .then(() => {
        cloudAnalytics.addEvent({
            type: cloudAnalytics.eventTypes.OPEN_PROJECT,
            msg: {
                editor_version: editorVersion,
                cloudprojectid: cloudProjectId,
                organizationid: organizationId,
                localprojectid: undefined,
                source: 'cloud',
                status: 'Success',
            }
        });
    });
}
function associateVersionToProject(project, versions) {
    const versionInfo = versions.find((info) => info.guid === project.guid);
    if (versionInfo !== undefined) {
        project.unity_version = versionInfo.branch_versions.master;
    }
}
class CloudCollab {
    constructor() {
        this.cloudCache = [];
    }
    init() {
        postal.subscribe({
            channel: 'app',
            topic: 'connectInfo.changed',
            callback: () => {
                this.clearCache();
            }
        });
    }
    getProjects() {
        return Promise
            .all([
            cloudCore.getProjects(),
            cloudCore.getTeamsSeatOrganizations(UnityAuth.userInfo.userId),
            collabApiClient.getProjectVersions(tokenManager.accessToken.value)
        ])
            .then(([projects, orgFKs, projectVersions]) => {
            _.each(projects, (project) => {
                if (projectVersions) {
                    associateVersionToProject(project, projectVersions);
                }
                if (orgFKs) {
                    _.extend(project, { hasSeat: project.org_foreign_key in orgFKs });
                }
            });
            return projects;
        })
            .catch(error => {
            logger.warn(`Could not fetch cloud projects. Error: ${error}`);
            throw error;
        });
    }
    hasCache() {
        return this.cloudCache !== [];
    }
    clearCache() {
        this.cloudCache = [];
    }
    cacheProjectList(currentCache) {
        this.cloudCache = currentCache;
    }
    getCache() {
        return this.cloudCache;
    }
    updateCloudCache() {
        return __awaiter(this, void 0, void 0, function* () {
            let updated = false;
            const currentCache = this.getCache();
            const projects = yield this.getProjects();
            const validProjects = projects.filter((project) => !project.archived && project.active && project.service_flags.collab);
            const diff = _.xorBy(currentCache, validProjects, 'guid');
            if (diff.length > 0) {
                this.cacheProjectList(validProjects);
                updated = true;
            }
            return updated;
        });
    }
    chooseDirectory() {
        return localProject.getUserDefault().then(userDefault => {
            const args = {
                title: 'Choose location for new project',
                buttonLabel: 'Select Folder',
                defaultPath: userDefault.projectDirectory || os.homedir(),
                properties: ['openDirectory', 'createDirectory']
            };
            return windowManager.mainWindow.showOpenFileDialog(args);
        });
    }
    openProject(organizationId, projectId, projectTitle, cloudProjectId, editorVersion, options) {
        return this.chooseDirectory().then(selectedDir => {
            if (!licenseClient.isLicenseValid()) {
                return Promise.reject({ errorCode: 'ERROR.LAUNCH_EDITOR.LICENSE' });
            }
            if (!hubIPCState.modalEditor) {
                return openCloudProjectFromEditor(organizationId, projectId, projectTitle, cloudProjectId, selectedDir, editorVersion, options);
            }
            const projectPath = hubFS.getUniquePath(selectedDir + path.sep + projectTitle);
            postal.publish({
                channel: 'app',
                topic: 'project.openedFromCloud',
                data: {
                    organizationId,
                    projectId,
                    projectPath
                }
            });
            return Promise.resolve();
        });
    }
    getUnityTeamsUrlForProject(project) {
        const cloudCoreBase = cloudConfig.urls.core;
        return `${cloudCoreBase}/orgs/${project.org_id}/projects/${project.id}/unity-teams/`;
    }
}
module.exports = new CloudCollab();
//# sourceMappingURL=cloudCollab.js.map