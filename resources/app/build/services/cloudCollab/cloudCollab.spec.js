const cloudCore = require('../cloudCore/cloudCore');
const cloudConfig = require('../cloudConfig/cloudConfig');
const cloudCollab = require('./cloudCollab');
const collabApiClient = require('./lib/cloudCollab.api');
const unityAuth = require('../localAuth/auth');
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
chai.use(chaiAsPromised);
describe('CloudCollab', () => {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('getProjects', () => {
        beforeEach(() => {
            sandbox.stub(cloudCore, 'getProjects');
            sandbox.stub(cloudCore, 'getTeamsSeatOrganizations');
            sandbox.stub(collabApiClient, 'getProjectVersions');
            unityAuth.userInfo = {
                userId: 'foo123'
            };
        });
        it('should call cloudCore.getProjects', () => {
            cloudCollab.getProjects();
            expect(cloudCore.getProjects).to.have.been.calledOnce;
        });
        it('should be fulfilled with project list when cloudCore.getProjects succeeds', () => {
            let projectList = [];
            cloudCore.getProjects.resolves(projectList);
            return expect(cloudCollab.getProjects()).to.eventually.equal(projectList);
        });
        it('should set the version of each project', () => {
            cloudCore.getProjects.resolves([
                { guid: '123' },
                { guid: '323' },
                { guid: 'abc' },
            ]);
            collabApiClient.getProjectVersions.resolves([
                { guid: '123', branch_versions: { master: '1' } },
                { guid: 'abc', branch_versions: { master: '2' } }
            ]);
            return expect(cloudCollab.getProjects()).to.eventually.eql([
                { guid: '123', unity_version: '1' },
                { guid: '323' },
                { guid: 'abc', unity_version: '2' },
            ]);
        });
        it('should skip the versions assignment if it is null', () => {
            let projects = [
                { guid: '123' },
                { guid: '323' },
                { guid: 'abc' },
            ];
            cloudCore.getProjects.resolves(projects);
            collabApiClient.getProjectVersions.resolves(null);
            return expect(cloudCollab.getProjects()).to.eventually.eql(projects);
        });
    });
    describe('when managing the cache', () => {
        let cloudProjects;
        beforeEach(() => {
            cloudProjects = [
                { name: 'Theme Park', archived: false, active: true, service_flags: { collab: true }, guid: 1 },
                { name: 'Theme Hospital', archived: false, active: true, service_flags: { collab: true, guid: 2 } },
                { name: 'Dungeon Keeper', archived: false, active: true, service_flags: { collab: true }, guid: 3 },
                { name: 'Populous', archived: false, active: true, service_flags: { collab: true }, guid: 4 }
            ];
            cache = cloudProjects.slice(0, 2);
            cloudCollab.cloudCache = cache;
        });
        it('hasCache should return true', () => {
            return expect(cloudCollab.hasCache()).to.eql(true);
        });
        it('should get the corect cache', () => {
            return expect(cloudCollab.getCache()).to.eql(cache);
        });
        it('should set the right content', () => {
            cloudCollab.cacheProjectList(cloudProjects);
            return expect(cloudCollab.cloudCache).to.eql(cloudProjects);
        });
        it('should clear the cache', () => {
            cloudCollab.clearCache();
            return expect(cloudCollab.cloudCache).to.eql([]);
        });
    });
    describe('getUnityTeamsUrlForProject', () => {
        it('should return properly formatted cloud teams url', () => {
            const urls = { core: 'http://core.com' };
            const project = {
                org_id: 'foo',
                id: 'bar'
            };
            sandbox.stub(cloudConfig, 'urls').get(() => urls);
            const result = cloudCollab.getUnityTeamsUrlForProject(project);
            expect(result).to.eq(`${urls.core}/orgs/foo/projects/bar/unity-teams/`);
        });
    });
});
//# sourceMappingURL=cloudCollab.spec.js.map