var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const _ = require('lodash');
const util = require('util');
const electron = require('electron');
const tools = require('./platformtools');
const launchProcess = require('./launchprocess');
const logger = require('../../logger')('EditorApp');
const settings = require('../localSettings/localSettings');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const licenseClient = require('../licenseService/licenseClientProxy');
const tokenManager = require('../../tokenManager/tokenManager');
const EditorAppPlatform = tools.require(`${__dirname}/lib/editorapp`);
let cloudEnvironment;
class EditorApp extends EditorAppPlatform {
    constructor(appOptions) {
        super(appOptions);
        this.version = appOptions.version;
        this.main = appOptions.main;
    }
    static init() {
        logger.info('Init');
        cloudEnvironment = settings.get(settings.keys.CLOUD_ENVIRONMENT);
        EditorApp.editorArgs = null;
    }
    processError(error, args) {
        let errorCode = 'ERROR.LAUNCH_EDITOR.GENERIC';
        if (error && error.code) {
            switch (error.code) {
                case 'ENOTDIR':
                case 'EPERM':
                case 'ENOENT':
                case 'EACCES':
                case 'LICENSE':
                    errorCode = `ERROR.LAUNCH_EDITOR.${error.code}`;
                    break;
                default:
                    errorCode = 'ERROR.LAUNCH_EDITOR.GENERIC';
            }
        }
        logger.warn(`Launch error app.editor: ${this.exec}, args: ${args}`, errorCode);
        return { errorCode, params: { i18n: { path: this.exec } }, error };
    }
    getOptionalArguments(options, customArgs) {
        options = options || {};
        customArgs = customArgs || [];
        const args = ['-useHub', '-hubIPC'];
        if (cloudEnvironment && customArgs.indexOf('-cloudEnvironment') <= -1) {
            args.push('-cloudEnvironment', cloudEnvironment);
        }
        if (options.buildPlatform !== undefined && options.buildPlatform.buildTarget !== undefined && customArgs.indexOf('-buildTarget') <= -1) {
            args.push('-buildTarget', options.buildPlatform.buildTarget);
            if (options.buildPlatform.buildTargetGroup !== undefined && customArgs.indexOf('-buildTargetGroup') <= -1) {
                args.push('-buildTargetGroup', options.buildPlatform.buildTargetGroup);
            }
        }
        if (Array.isArray(options.args)) {
            args.push(...options.args);
        }
        return args;
    }
    start(args, onError) {
        onError = onError || _.noop;
        args.push('-hubSessionId', cloudAnalytics.common.session_guid);
        if (tokenManager.accessToken && tokenManager.accessToken.value) {
            args.push('-accessToken', tokenManager.accessToken.value);
        }
        if (EditorApp.editorArgs) {
            args = args.concat(EditorApp.editorArgs);
            EditorApp.editorArgs = null;
        }
        if (!licenseClient.isLicenseValid()) {
            return Promise.reject(this.processError({ code: 'LICENSE' }));
        }
        return launchProcess.start(this.exec, args)
            .then(process => {
            process.on('error', error => {
                logger.error(`Error launching the Editor: ${error}`);
                onError(error);
                electron.dialog.showMessageBox({
                    type: 'error',
                    title: 'Unity Launch Error',
                    message: this.processError(error, args).message,
                    buttons: []
                });
            });
            return process;
        })
            .catch(error => Promise.reject(this.processError(error, args)));
    }
    openProject(projectPath, options, customArgs, onError) {
        return __awaiter(this, void 0, void 0, function* () {
            if (customArgs && customArgs[projectPath] && customArgs[projectPath].cliArgs) {
                customArgs = customArgs[projectPath].cliArgs.split(/\n| /);
            }
            else {
                customArgs = [];
            }
            const optionalArgs = this.getOptionalArguments(options, customArgs);
            let args = ['-projectpath', projectPath];
            args = [...args, ...customArgs, ...optionalArgs];
            logger.debug(`opening the project with args ${util.inspect(args)}`);
            return this.start(args, onError);
        });
    }
    createProject(projectPath, selectedPackages, projectTemplate, organizationId, onError) {
        return __awaiter(this, void 0, void 0, function* () {
            const templateKey = projectTemplate === 0 || projectTemplate === 1 ? 'projectTemplate' : 'cloneFromTemplate';
            const args = ['-createproject', projectPath, ...selectedPackages, `-${templateKey}`, projectTemplate, '-cloudOrganization', organizationId,
                '-cloudEnvironment', cloudEnvironment, '-useHub', '-hubIPC'];
            logger.info('createProject with args: ', args);
            return this.start(args, onError);
        });
    }
    createTempProject(selectedPackages, projectTemplate, onError) {
        const templateKey = projectTemplate === 0 || projectTemplate === 1 ? 'projectTemplate' : 'cloneFromTemplate';
        const args = ['-createproject', ...selectedPackages, `-${templateKey}`, projectTemplate, '-temporary',
            '-cloudEnvironment', cloudEnvironment, '-useHub', '-hubIPC', '-skipUpgradeDialogs'];
        return this.start(args, onError);
    }
    openCloudProject(projectPath, projectId, organizationId, options, onError) {
        let args = ['-createProject', projectPath, '-cloudProject', projectId, '-cloudOrganization', organizationId];
        args = [...args, ...this.getOptionalArguments(options)];
        return this.start(args, onError);
    }
}
module.exports = EditorApp;
//# sourceMappingURL=editorapp.js.map