var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const EditorApp = require('./editorapp');
const localSettings = require('../localSettings/localSettings');
const licenseClient = require('../licenseService/licenseClientProxy');
const launchProcess = require('./launchprocess');
const tokenManager = require('../../tokenManager/tokenManager');
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
chai.use(chaiAsPromised);
describe('EditorApp', () => {
    let sandbox;
    let editorApp;
    let editorPath = 'some/fake/path';
    let settingsMock;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        settingsMock = {
            [localSettings.keys.CLOUD_ENVIRONMENT]: localSettings.cloudEnvironments.DEV,
        };
        sandbox.stub(localSettings, 'get').callsFake((key) => settingsMock[key]);
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('processError', () => {
        beforeEach(() => {
            editorApp = new EditorApp({ editorPath: editorPath });
        });
        it('should return generic error code if the given error does not have a code', () => {
            let result = editorApp.processError({});
            expect(result.errorCode).to.equal('ERROR.LAUNCH_EDITOR.GENERIC');
        });
        it('should return generic error code if the given error code is not listed in the functions', () => {
            let result = editorApp.processError({ code: 'someRandomCode' });
            expect(result.errorCode).to.equal('ERROR.LAUNCH_EDITOR.GENERIC');
        });
        it('should return specific error code if the given error code is listed in the functuons', () => {
            let result = editorApp.processError({ code: 'EACCES' });
            expect(result.errorCode).to.equal('ERROR.LAUNCH_EDITOR.EACCES');
        });
    });
    describe('getOptionalArguments', () => {
        beforeEach(() => {
            editorApp = new EditorApp({ editorPath: editorPath });
        });
        it('should add hub related arguments', () => {
            let result = editorApp.getOptionalArguments();
            expect(result).to.include.members(['-useHub', '-hubIPC']);
        });
        it('should add buildTarget if it is passed as part of buildPlatform', () => {
            let result = editorApp.getOptionalArguments({ buildPlatform: {
                    buildTarget: 'testTarget'
                } }, []);
            expect(result).to.include.members(['-buildTarget', 'testTarget']);
        });
        it('should add buildTarget and buildTargetGroupif it is passed as part of buildPlatform', () => {
            let result = editorApp.getOptionalArguments({ buildPlatform: {
                    buildTarget: 'testTarget',
                    buildTargetGroup: 'testTargetGroup'
                } }, []);
            expect(result).to.include.members(['-buildTarget', 'testTarget', '-buildTargetGroup', 'testTargetGroup']);
        });
        it('should add cloudEnvironment if it exists', () => {
            EditorApp.init();
            let result = editorApp.getOptionalArguments([], []);
            expect(result).to.include.members(['-cloudEnvironment', 'dev']);
        });
        it('should add every other args', () => {
            let result = editorApp.getOptionalArguments({ args: ['foo', 'bar'] }, []);
            expect(result).to.include.members(['foo', 'bar']);
        });
        it('custom arguments should override other options', () => {
            let result = editorApp.getOptionalArguments({ args: ['foo', 'bar'] }, ['-cloudEnvironment']);
            expect(result).to.include.members(['foo', 'bar']);
        });
    });
    describe('start', () => {
        let fakeProcess;
        beforeEach(() => {
            fakeProcess = {
                on: () => { }
            };
        });
        describe('when the license is valid', () => {
            beforeEach(() => {
                sandbox.stub(licenseClient, 'isLicenseValid').callsFake(() => true);
            });
            it('should start the launch process with given args and returns it process', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(launchProcess, 'start').callsFake(() => Promise.resolve(fakeProcess));
                let process = yield editorApp.start(['some args']);
                expect(process).to.equal(fakeProcess);
                expect(launchProcess.start.getCall(0).args[1]).to.include.members(['some args']);
            }));
            it('should add the hub session id to args', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(launchProcess, 'start').callsFake(() => Promise.resolve(fakeProcess));
                let process = yield editorApp.start(['some args']);
                expect(process).to.equal(fakeProcess);
                expect(launchProcess.start.getCall(0).args[1]).to.include.members(['-hubSessionId']);
            }));
            it('should add the access token to args', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(launchProcess, 'start').callsFake(() => Promise.resolve(fakeProcess));
                tokenManager.accessToken.value = '1';
                let process = yield editorApp.start(['some args']);
                expect(process).to.equal(fakeProcess);
                expect(launchProcess.start.getCall(0).args[1]).to.include.members(['-accessToken']);
            }));
            it('should add the editor args if exist', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(launchProcess, 'start').callsFake(() => Promise.resolve(fakeProcess));
                EditorApp.editorArgs = ['-a', 'b', '-c', 'd'];
                let process = yield editorApp.start(['some args']);
                expect(process).to.equal(fakeProcess);
                expect(launchProcess.start.getCall(0).args[1]).to.include.members(['-a', 'b', '-c', 'd']);
            }));
            it('should call processError if there was an error', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(launchProcess, 'start').callsFake(() => Promise.reject('error'));
                sandbox.stub(editorApp, 'processError');
                try {
                    yield editorApp.start(['some args']);
                    expect(true).to.equal(false);
                }
                catch (err) {
                    expect(editorApp.processError.getCall(0).args[1]).to.include.members(['some args', '-hubSessionId']);
                    expect(editorApp.processError.getCall(0).args[0]).to.equal('error');
                }
            }));
        });
        describe('when the license is invalid', () => {
            it('should call processError if there was a license error', () => __awaiter(this, void 0, void 0, function* () {
                sandbox.stub(licenseClient, 'isLicenseValid').callsFake(() => false);
                sandbox.stub(editorApp, 'processError');
                try {
                    yield editorApp.start(['some args']);
                    expect(true).to.equal(false);
                }
                catch (err) {
                    expect(editorApp.processError.getCall(0).args[0].code).to.eq('LICENSE');
                }
            }));
        });
    });
    describe('createTempProject', () => {
        beforeEach(() => {
            sandbox.stub(licenseClient, 'isLicenseValid').callsFake(() => true);
            sandbox.stub(launchProcess, 'start').callsFake(() => Promise.resolve({ on: () => { } }));
        });
        it('should pass the "-skipUpgradeDialogs" argument', () => __awaiter(this, void 0, void 0, function* () {
            yield editorApp.createTempProject(['some package'], 'a template', () => null);
            expect(launchProcess.start.getCall(0).args[1]).to.include.members(['-skipUpgradeDialogs']);
        }));
    });
});
//# sourceMappingURL=editorApp.spec.js.map