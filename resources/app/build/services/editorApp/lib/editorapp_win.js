const path = require('path');
class EditorAppWin {
    constructor(appOptions) {
        if (appOptions.editorPath === undefined) {
            throw new TypeError('missing mandatory editorPath options');
        }
        else {
            this.editorPath = appOptions.editorPath;
            this.exec = appOptions.editorPath;
            this.path = path.join(path.dirname(this.exec), '..');
            this.resources = path.resolve(this.path, 'Data', 'Resources');
        }
    }
}
module.exports = EditorAppWin;
//# sourceMappingURL=editorapp_win.js.map