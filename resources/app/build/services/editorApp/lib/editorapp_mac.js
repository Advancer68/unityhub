const path = require('path');
class EditorAppMac {
    constructor(appOptions) {
        if (appOptions.editorPath === undefined) {
            throw new TypeError('missing mandatory editorPath options');
        }
        else {
            this.editorPath = appOptions.editorPath;
            this.exec = `${appOptions.editorPath}/Contents/MacOS/Unity`;
            this.path = path.dirname(appOptions.editorPath);
            this.resources = path.resolve(this.path, 'Contents', 'Resources');
        }
    }
}
module.exports = EditorAppMac;
//# sourceMappingURL=editorapp_mac.js.map