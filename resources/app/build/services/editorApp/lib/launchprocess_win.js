const registryKeys = require('./registry_win');
const Registry = require('@unityhub/unity-editor-registry');
const networkSetup = require('../../../networkSetup');
const SYSTEM_ENVARS_REGISTRY_PATH = `${registryKeys.keys.hklm}\\${registryKeys.keys.sysEnVar}`;
const USER_ENVARS_REGISTRY_PATH = `${registryKeys.keys.hkcu}\\${registryKeys.keys.envar}`;
class LaunchProcessWin {
    getEnVars() {
        return Object.assign({}, networkSetup.originalEnvVars);
    }
    getMergedEnVars() {
        const childEnVarsOld = Object.assign({}, networkSetup.originalEnvVars);
        const childEnVars = this._uppercasifyKeys(childEnVarsOld);
        const [sysEnVarKeys, sysEnVarValues] = Registry.getKeyValues(SYSTEM_ENVARS_REGISTRY_PATH);
        const [usrEnVarKeys, usrEnVarValues] = Registry.getKeyValues(USER_ENVARS_REGISTRY_PATH);
        this._setEnVars(sysEnVarKeys, sysEnVarValues, childEnVars);
        this._resolveAllEnVarToPath(childEnVars);
        const sysEnVars = Object.assign({}, childEnVars);
        this._setEnVars(usrEnVarKeys, usrEnVarValues, childEnVars);
        this._resolveAllEnVarToPath(childEnVars, sysEnVars);
        let pathEnVar = '';
        pathEnVar = this._appendPathEnVar(sysEnVarKeys, sysEnVarValues, pathEnVar);
        pathEnVar = this._appendPathEnVar(usrEnVarKeys, usrEnVarValues, pathEnVar);
        this._setPathEnVar(childEnVars, pathEnVar);
        ['path', 'Path', 'PATH'].forEach((defn) => {
            if (childEnVars[defn])
                childEnVars[defn] = this._resolveEnVarToPath(childEnVars[defn], childEnVars, sysEnVarValues);
        });
        return childEnVars;
    }
    _setEnVars(enVarKeys, enVarValues, currentEnVars) {
        if (!enVarKeys)
            return;
        enVarKeys.forEach((key, index) => {
            const upperCaseKey = key.toUpperCase();
            if (upperCaseKey !== 'PATH') {
                currentEnVars[upperCaseKey] = enVarValues[index];
            }
        });
    }
    _appendPathEnVar(enVarKeys, enVarValues, pathEnVar) {
        if (!enVarKeys)
            return pathEnVar;
        enVarKeys.forEach((key, index) => {
            if (key.toUpperCase() === 'PATH') {
                pathEnVar = pathEnVar.concat(enVarValues[index], ';');
            }
        });
        return pathEnVar;
    }
    _setPathEnVar(childEnVars, pathEnVarValue) {
        if (pathEnVarValue.length !== 0) {
            ['path', 'Path', 'PATH'].forEach((defn) => {
                if (childEnVars[defn])
                    childEnVars[defn] = pathEnVarValue;
            });
        }
    }
    _uppercasifyKeys(childEnVarsOld) {
        const childEnVars = {};
        Object.keys(childEnVarsOld).forEach((key) => {
            if (key.toUpperCase() !== 'PATH') {
                childEnVars[key.toUpperCase()] = childEnVarsOld[key];
            }
            else {
                childEnVars[key] = childEnVarsOld[key];
            }
        });
        return childEnVars;
    }
    _resolveAllEnVarToPath(currentEnVars, sysEnVars = {}) {
        for (const [enVarKey, enVarVal] of Object.entries(currentEnVars)) {
            if (enVarVal.includes('%')) {
                currentEnVars[enVarKey] = this._resolveEnVarToPath(enVarVal, currentEnVars, sysEnVars);
            }
        }
    }
    _resolveEnVarToPath(enVarValue, currentEnVars, sysEnVars) {
        return this._resolveEnVarToPathRecursive(enVarValue, currentEnVars, [], sysEnVars, []);
    }
    _resolveEnVarToPathRecursive(enVarValue, usrEnVars, usrResolvedEnVars, sysEnVars, sysResolvedEnvars) {
        if (enVarValue.match(/%([^%]+)%/g)) {
            enVarValue = enVarValue.replace(/%([^%]+)%/g, (org, match) => {
                const usrResolvedEnVarsCopy = usrResolvedEnVars.slice();
                const sysResolvedEnvarsCopy = sysResolvedEnvars.slice();
                const upperCaseKey = match.toUpperCase();
                let result = org;
                if (!usrResolvedEnVars.includes(upperCaseKey)
                    && !sysResolvedEnvars.includes(upperCaseKey)
                    && usrEnVars[upperCaseKey]) {
                    usrResolvedEnVarsCopy.push(upperCaseKey);
                    result = this._resolveEnVarToPathRecursive(usrEnVars[upperCaseKey], usrEnVars, usrResolvedEnVarsCopy.slice(), sysEnVars, sysResolvedEnvarsCopy.slice());
                }
                if (result === org
                    && !sysResolvedEnvars.includes(upperCaseKey)
                    && sysEnVars[upperCaseKey]) {
                    sysResolvedEnvarsCopy.push(upperCaseKey);
                    result = this._resolveEnVarToPathRecursive(sysEnVars[upperCaseKey], usrEnVars, usrResolvedEnVarsCopy.slice(), sysEnVars, sysResolvedEnvarsCopy.slice());
                }
                return result;
            });
        }
        return enVarValue;
    }
}
module.exports = LaunchProcessWin;
//# sourceMappingURL=launchprocess_win.js.map