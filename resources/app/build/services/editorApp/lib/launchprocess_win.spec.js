const expect = require('chai').expect;
const sinon = require('sinon');
const os = require('os');
const registryKeys = require('./registry_win');
const networkSetup = require('../../../networkSetup');
const SYSTEM_ENVARS_REGISTRY_PATH = `${registryKeys.keys.hklm}\\${registryKeys.keys.sysEnVar}`;
const USER_ENVARS_REGISTRY_PATH = `${registryKeys.keys.hkcu}\\${registryKeys.keys.envar}`;
if (os.platform() === 'win32') {
    const Registry = require('@unityhub/unity-editor-registry');
    const LaunchProcessWin = require('./launchprocess_win');
    describe('LaunchProcessWin', () => {
        let sandbox;
        let launchProcessWin;
        beforeEach(() => {
            launchProcessWin = new LaunchProcessWin();
            sandbox = sinon.sandbox.create();
        });
        describe('getMergedEnVars', () => {
            describe('when nothing new is defined', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'test': 'oldval'
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH)
                            return [];
                        if (registryKey === USER_ENVARS_REGISTRY_PATH)
                            return [[], []];
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should replace old values', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'unityhub',
                        'TEST': 'oldval'
                    });
                });
            });
            describe('when new value for a user environment variable is defined', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'test': 'oldval'
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH)
                            return [[], []];
                        if (registryKey === USER_ENVARS_REGISTRY_PATH)
                            return [['test'], ['newval']];
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should replace old value of the user environment variable', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'unityhub',
                        'TEST': 'newval'
                    });
                });
            });
            describe('when new value for a user and a system environment variable is defined', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'test': 'oldval'
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH)
                            return [['username'], ['chenlei']];
                        if (registryKey === USER_ENVARS_REGISTRY_PATH)
                            return [['test'], ['newval']];
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should replace old values of those environment variables', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'chenlei',
                        'TEST': 'newval'
                    });
                });
            });
            describe('when a new user and a system environment variable is added', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'test': 'oldval'
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH) {
                            return [['username', 'newkey'], ['chenlei', 'newval']];
                        }
                        if (registryKey === USER_ENVARS_REGISTRY_PATH) {
                            return [['test'], ['newval']];
                        }
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should add the new environment varibles with corresponding value', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'chenlei',
                        'TEST': 'newval',
                        'NEWKEY': 'newval'
                    });
                });
            });
            describe('when a new user and a system environment variable is added', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'test': 'oldval'
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH) {
                            return [['username', 'newsyskey'], ['chenlei', 'newsysval']];
                        }
                        if (registryKey === USER_ENVARS_REGISTRY_PATH) {
                            return [['test', 'newusrkey'], ['newval', 'newusrval']];
                        }
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should replace old values', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'chenlei',
                        'TEST': 'newval',
                        'NEWSYSKEY': 'newsysval',
                        'NEWUSRKEY': 'newusrval'
                    });
                });
            });
            describe('when user and a system environment variable overlaps', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'test': 'oldval'
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH) {
                            return [['username'], ['chenlei']];
                        }
                        if (registryKey === USER_ENVARS_REGISTRY_PATH) {
                            return [['username'], ['shelley']];
                        }
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should take the user variable instead of the system one', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'shelley',
                        'TEST': 'oldval'
                    });
                });
            });
            describe('when environment variable keys are defined in different upper or lower cases', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'Username': 'unityhub',
                        'TEST': 'oldval'
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH) {
                            return [['uSerName'], ['chenlei']];
                        }
                        if (registryKey === USER_ENVARS_REGISTRY_PATH) {
                            return [['tESt'], ['newval']];
                        }
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should take into account that enviornment variable keys are case insensitive', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'chenlei',
                        'TEST': 'newval'
                    });
                });
            });
            describe('when path is defined in user', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'path': 'shelley'
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH) {
                            return [['path'], ['chenlei;shen;unity;hub']];
                        }
                        if (registryKey === USER_ENVARS_REGISTRY_PATH) {
                            return [['path'], ['hub;hub;team;team']];
                        }
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should concact both user and system path separated by semicolon', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'unityhub',
                        'path': 'chenlei;shen;unity;hub;hub;hub;team;team;'
                    });
                });
            });
            describe('when path in different cases are defined in user', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'path': 'hub;team',
                        'Path': 'is;the;best',
                        'PATH': 'yay'
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH) {
                            return [['path', 'username'], ['unity;hub', 'hubteam']];
                        }
                        if (registryKey === USER_ENVARS_REGISTRY_PATH) {
                            return [['path'], ['chenlei']];
                        }
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should concact both user and system path separated by semicolon', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'hubteam',
                        'path': 'unity;hub;chenlei;',
                        'Path': 'unity;hub;chenlei;',
                        'PATH': 'unity;hub;chenlei;'
                    });
                });
            });
            describe('when environment variable, either in old, user defined, or system, references another variable', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'path': 'shelley',
                        'tmp': '%temp%',
                        'temp': '/users/unityhub/temp'
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH) {
                            return [['PATH'], ['%home%']];
                        }
                        if (registryKey === USER_ENVARS_REGISTRY_PATH) {
                            return [['path'], ['hub;team;%tmp%']];
                        }
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should resolve the referenced variable if that variable is defined', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'unityhub',
                        'path': '/users/unityhub;hub;team;/users/unityhub/temp;',
                        'TMP': '/users/unityhub/temp',
                        'TEMP': '/users/unityhub/temp'
                    });
                });
            });
            describe('when references in environment variable refers to a variable that doesn\'t exist', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'path': 'shelley',
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH) {
                            return [['PATH'], ['%blehbleh%']];
                        }
                        if (registryKey === USER_ENVARS_REGISTRY_PATH) {
                            return [['path'], ['hub;team']];
                        }
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should not resolve the referenced variable', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'unityhub',
                        'path': '%blehbleh%;hub;team;'
                    });
                });
            });
            describe('when references refers to a variable that contains other references', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'refyref': '%ref1%/%ref2%',
                        'ref1': '%home%',
                        'ref2': 'resolve'
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH) {
                            return [['ref_root'], ['%refyref%']];
                        }
                        if (registryKey === USER_ENVARS_REGISTRY_PATH) {
                            return [['somanyrefs'], ['%ref2%']];
                        }
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should resolve all of them to a path', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'unityhub',
                        'REFYREF': '/users/unityhub/resolve',
                        'REF_ROOT': '/users/unityhub/resolve',
                        'REF1': '/users/unityhub',
                        'REF2': 'resolve',
                        'SOMANYREFS': 'resolve'
                    });
                });
            });
            describe('when devious users define references that have a circular dependency at 1st level', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'ref1': '%ref2%',
                        'ref2': '%ref1%'
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH) {
                            return [['ref3'], ['%ref4%']];
                        }
                        if (registryKey === USER_ENVARS_REGISTRY_PATH) {
                            return [['ref4'], ['%ref3%']];
                        }
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should not resolve anything and neither be stuck in infinite loop', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'unityhub',
                        'REF1': '%ref2%',
                        'REF2': '%ref1%',
                        'REF3': '%ref4%',
                        'REF4': '%ref4%'
                    });
                });
            });
            describe('when devious users define references that have a circular dependency at 2nd level', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'ref1': '%ref2%',
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH) {
                            return [['ref2'], ['%ref3%']];
                        }
                        if (registryKey === USER_ENVARS_REGISTRY_PATH) {
                            return [['ref3'], ['%ref1%']];
                        }
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should not resolve anything and neither be stuck in infinite loop', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'unityhub',
                        'REF1': '%ref3%',
                        'REF2': '%ref3%',
                        'REF3': '%ref3%'
                    });
                });
            });
            describe('when a reference is defined in system variables but not in user variables', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'ref1': '%ref2%',
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH) {
                            return [['ref2'], ['resolved']];
                        }
                        if (registryKey === USER_ENVARS_REGISTRY_PATH) {
                            return [['ref100'], ['somevalue']];
                        }
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should be resolved to the one defined in system variable', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'unityhub',
                        'REF1': 'resolved',
                        'REF2': 'resolved',
                        'REF100': 'somevalue'
                    });
                });
            });
            describe('when a reference is referring to itself but defined in system variables', () => {
                beforeEach(() => {
                    sandbox.stub(networkSetup, 'originalEnvVars').value({
                        'home': '/users/unityhub',
                        'username': 'unityhub',
                        'ref1': '%ref1%',
                    });
                    sandbox.stub(Registry, 'getKeyValues').callsFake((registryKey) => {
                        if (registryKey === SYSTEM_ENVARS_REGISTRY_PATH) {
                            return [['ref1', 'ref2'], ['resolved', 'alsoresolved']];
                        }
                        if (registryKey === USER_ENVARS_REGISTRY_PATH) {
                            return [['ref2'], ['%ref2%']];
                        }
                    });
                });
                afterEach(() => {
                    sandbox.restore();
                });
                it('should be resolved to the one defined in system variable', () => {
                    const enVars = launchProcessWin.getMergedEnVars();
                    expect(enVars).to.deep.equal({
                        'HOME': '/users/unityhub',
                        'USERNAME': 'unityhub',
                        'REF1': 'resolved',
                        'REF2': 'alsoresolved'
                    });
                });
            });
        });
        describe('_resolveEnVarToPath (extra, incorporated into above)', () => {
            it('should not do anything when there are 0 variable', () => {
                const nothingToResolve = launchProcessWin._resolveEnVarToPath('somepath\\somepath', { VAR1: 'resolved' }, {});
                expect(nothingToResolve).to.equal('somepath\\somepath');
            });
            it('should not do anything when there are 0 variable but percent sign in path', () => {
                const nothingToResolve = launchProcessWin._resolveEnVarToPath('%var2\\somepath', { VAR1: 'resolved', VAR2: 'path' }, {});
                expect(nothingToResolve).to.equal('%var2\\somepath');
            });
            it('should resolve varibles when there is 1 variable in path', () => {
                const resolved = launchProcessWin._resolveEnVarToPath('%var1%\\somepath', { VAR1: 'resolved', VAR2: 'path' }, {});
                expect(resolved).to.equal('resolved\\somepath');
            });
            it('should resolve varibles when there are multiple variables in path', () => {
                const resolved = launchProcessWin._resolveEnVarToPath('%var1%\\%var2%\\somepath;%var1%\\%var2%;%var1%', { VAR1: 'resolved', VAR2: 'path' }, {});
                expect(resolved).to.equal('resolved\\path\\somepath;resolved\\path;resolved');
            });
            it('should not resolve the variable when the variable does not exist', () => {
                const resolved = launchProcessWin._resolveEnVarToPath('%var1%\\%var2%\\somepath', { VAR1: 'resolved' }, {});
                expect(resolved).to.equal('resolved\\%var2%\\somepath');
            });
            it('should not resolve the variable recursively ish', () => {
                const resolved = launchProcessWin._resolveEnVarToPath('%var1%\\%var2%\\somepath', { VAR1: '%var2%', VAR2: 'resolved' }, {});
                expect(resolved).to.equal('resolved\\resolved\\somepath');
            });
            it('should not be stuck in an infinite loop when variables cross reference', () => {
                const resolved = launchProcessWin._resolveEnVarToPath('%var1%\\%var2%\\somepath', { VAR1: '%var2%', VAR2: '%var1%' }, {});
                expect(resolved).to.equal('%var1%\\%var2%\\somepath');
            });
            it('should take parent variable value if user variable value is not defined', () => {
                const resolved = launchProcessWin._resolveEnVarToPath('%var1%\\%var2%\\somepath', { VAR1: '%var2%', VAR2: '%var2%' }, { VAR2: 'resolved' });
                expect(resolved).to.equal('resolved\\resolved\\somepath');
            });
        });
    });
}
//# sourceMappingURL=launchprocess_win.spec.js.map