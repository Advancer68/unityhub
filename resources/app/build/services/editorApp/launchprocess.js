const childProcess = require('child_process');
const logger = require('../../logger')('LaunchProcess');
const { fs } = require('../../fileSystem');
const tools = require('./platformtools');
const LaunchProcessPlatform = tools.require(`${__dirname}/lib/launchprocess`);
class LaunchProcess extends LaunchProcessPlatform {
    static isPathValid(path) {
        let error;
        try {
            fs.accessSync(path || '', fs.constants.F_OK);
        }
        catch (accessError) {
            error = accessError;
        }
        return error;
    }
    start(path, args) {
        try {
            const accessError = LaunchProcess.isPathValid(path);
            if (accessError) {
                return Promise.reject(accessError);
            }
            const childEnVars = Object.assign(this.getEnVars(), process.env);
            const open = childProcess.spawn(path, args, {
                stdio: ['ignore', 'ignore', 'ignore'],
                detached: true,
                env: childEnVars
            });
            if (open.unref) {
                open.unref();
            }
            open.on('error', data => {
                logger.error(`Launch process error: ${data}`);
            });
            open.on('close', (code) => {
                logger.info(`child process exited with code ${code}`);
            });
            return Promise.resolve(open);
        }
        catch (error) {
            return Promise.reject(error);
        }
    }
}
module.exports = new LaunchProcess();
//# sourceMappingURL=launchprocess.js.map