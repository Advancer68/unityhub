var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const isDev = require('electron-is-dev');
const updateHandlers = require('./updateHandlers');
const electronBuilderUpdater = require('electron-updater').autoUpdater;
const logger = require('../../logger')('AutoUpdater');
const settings = require('../localSettings/localSettings');
const localConfig = require('../localConfig/localConfig');
const windowManager = require('../../windowManager/windowManager');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const systemInfo = require('../localSystemInfo/systemInfo');
const BetaConfig = require('./betaConfig');
const postal = require('postal');
const util = require('util');
const promisify = require('es6-promisify');
const localStorageCallback = require('electron-json-storage');
const localStorage = {
    get: promisify(localStorageCallback.get),
    set: promisify(localStorageCallback.set),
};
const MAIN_PAGE_WAIT_INTERVAL = 10000;
const RELEASE_CHANNEL_STORAGE_KEY = 'hubReleaseChannel';
const BETA_PROMOTION_KEY = 'betaPromotion';
const ELECTRON_CHANNEL = {
    production: 'latest',
    beta: 'beta'
};
const USER_SELECTION = {
    latest: 'production',
    beta: 'beta'
};
class AutoUpdater {
    constructor() {
        this.updateAvailable = false;
        this.postponeMessage = false;
        this.availableVersion = undefined;
        this.online = false;
        this.downloadInProgress = false;
        this.currentVersion = systemInfo.hubVersion();
        this.isUpdateCheckOnly = false;
        this.isBetaTargeted = false;
        this.hasUserSeenBetaPromotionBefore = true;
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            electronBuilderUpdater.autoDownload = false;
            electronBuilderUpdater.allowDowngrade = false;
            logger.info('Init');
            this.updateInterval = settings.get(settings.keys.CHECK_FOR_UPDATE_INTERVAL);
            if (!isDev && !localConfig.isAutoUpdateDisabled()) {
                yield this.setBetaConfigs();
                yield this.initializeUpdateChannel();
                this.setUpdateServerPath();
                updateHandlers.registerUpdateEventHandlers(this, electronBuilderUpdater);
                logger.info('Checking for hub updates');
                yield this.checkForUpdate();
                setInterval(() => {
                    this.checkForUpdate();
                }, this.updateInterval);
                postal.subscribe({
                    channel: 'app',
                    topic: 'connectInfo.changed',
                    callback: (data) => {
                        if (data.connectInfo.online === true && this.online === false) {
                            this.online = data.connectInfo.online;
                            this.checkForUpdate();
                            return;
                        }
                        this.online = data.connectInfo.online;
                    },
                });
            }
            else {
                logger.info('Auto update is disabled');
            }
            return Promise.resolve();
        });
    }
    setBetaConfigs() {
        return __awaiter(this, void 0, void 0, function* () {
            this.isBetaTargeted = yield BetaConfig.isBetaTargeted();
            if (this.isBetaTargeted) {
                this._checkForPreviousBetaPromotionEvent();
            }
        });
    }
    _checkForPreviousBetaPromotionEvent() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { seenBefore } = yield localStorage.get(BETA_PROMOTION_KEY);
                this.hasUserSeenBetaPromotionBefore = !!seenBefore;
            }
            catch (e) {
                logger.warn(`Could not determine if user has been beta promoted before, default to true: ${e}`);
            }
        });
    }
    setUpdateServerPath() {
        const updateServerPath = settings.get(settings.keys.UPDATE_SERVER_PATH);
        if (updateServerPath !== undefined) {
            logger.info(`Look for Hub updates at ${updateServerPath}`);
            electronBuilderUpdater.setFeedURL(updateServerPath);
        }
        else {
            logger.warn('Missing update server path');
        }
    }
    initializeUpdateChannel() {
        return __awaiter(this, void 0, void 0, function* () {
            let currentChannel;
            try {
                currentChannel = yield localStorage.get(RELEASE_CHANNEL_STORAGE_KEY);
                if (currentChannel !== ELECTRON_CHANNEL.production && currentChannel !== ELECTRON_CHANNEL.beta) {
                    currentChannel = ELECTRON_CHANNEL.production;
                }
            }
            catch (err) {
                logger.error('retrieving previous users channel selection failed, defaulting to production');
                currentChannel = ELECTRON_CHANNEL.production;
            }
            this._setElectronUpdaterChannel(currentChannel);
        });
    }
    setHubToBetaChannel() {
        return __awaiter(this, void 0, void 0, function* () {
            logger.info('update to beta channel');
            yield this._setUserSeenBetaPromotionBefore();
            yield this.setUpdateChannel(ELECTRON_CHANNEL.beta);
        });
    }
    setHubToProductionChannel() {
        return __awaiter(this, void 0, void 0, function* () {
            logger.info('roll back to production channel');
            yield this.setUpdateChannel(ELECTRON_CHANNEL.production);
        });
    }
    userDeclinesBetaPromotion() {
        return __awaiter(this, void 0, void 0, function* () {
            logger.info('User chose to opt out of beta promotion');
            yield this._setUserSeenBetaPromotionBefore();
        });
    }
    setUpdateChannel(channel) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield localStorage.set(RELEASE_CHANNEL_STORAGE_KEY, channel);
                this._setElectronUpdaterChannel(channel);
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.UPDATE_CHANNEL_CHANGE,
                    msg: {
                        channel_selection: channel
                    },
                });
            }
            catch (err) {
                logger.error('storing users channel selection failed', err);
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.ERROR,
                    msg: {
                        error_message: `Update channel changed failed, could not switch to ${channel}`,
                        error_type: 'exception'
                    }
                });
            }
            this.checkForUpdate();
        });
    }
    _setUserSeenBetaPromotionBefore() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.hasUserSeenBetaPromotionBefore) {
                return;
            }
            try {
                yield localStorage.set(BETA_PROMOTION_KEY, { seenBefore: true });
                this.hasUserSeenBetaPromotionBefore = true;
                logger.info('User will not see beta promotion again');
            }
            catch (e) {
                logger.warn(`Problem setting user seen promotion before key: ${e}`);
            }
        });
    }
    getPreselectedChannel() {
        return __awaiter(this, void 0, void 0, function* () {
            let currentChannel;
            try {
                currentChannel = yield localStorage.get(RELEASE_CHANNEL_STORAGE_KEY);
            }
            catch (err) {
                logger.error('retrieving users channel selection failed, defaulting to production');
            }
            if (!currentChannel || !USER_SELECTION[currentChannel]) {
                return USER_SELECTION.latest;
            }
            return USER_SELECTION[currentChannel];
        });
    }
    isUpdateFromBetaPending() {
        const isCurrentBuildBeta = this.currentVersion.includes(ELECTRON_CHANNEL.beta);
        return this.updateAvailable && isCurrentBuildBeta;
    }
    restart() {
        logger.info('Restarting to update');
        return cloudAnalytics.quitEvent('Update').then(() => {
            electronBuilderUpdater.quitAndInstall(true, true);
        }).catch((err) => {
            logger.error('An error occurred while sending analytics:', err);
            electronBuilderUpdater.quitAndInstall(true, true);
        });
    }
    quitWithUpdate() {
        logger.info('Restarting to update');
        return cloudAnalytics.quitEvent('Update').then(() => {
            electronBuilderUpdater.quitAndInstall(true, false);
        }).catch((err) => {
            logger.error('An error occurred while sending analytics:', err);
            electronBuilderUpdater.quitAndInstall(true, false);
        });
    }
    remindLater() {
        this.postponeMessage = true;
        setTimeout(() => {
            if (this.updateAvailable === true) {
                this.postponeMessage = false;
                windowManager.broadcastContent('auto-update.update-available', this.availableVersion);
            }
        }, this.updateInterval);
    }
    checkForUpdate() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.downloadInProgress === false) {
                try {
                    yield electronBuilderUpdater.checkForUpdates();
                    yield this._checkForBetaUpdate();
                }
                catch (error) {
                    logger.warn(`Failed to check for updates:  ${util.inspect(error)}`);
                }
            }
        });
    }
    _checkForBetaUpdate() {
        return __awaiter(this, void 0, void 0, function* () {
            if (!this._shouldCheckForBetaUpdate()) {
                return;
            }
            yield this._checkForChannelUpdateWithNoDownload(ELECTRON_CHANNEL.beta);
        });
    }
    _shouldCheckForBetaUpdate() {
        return electronBuilderUpdater.channel === ELECTRON_CHANNEL.production
            && this.isBetaTargeted
            && !this.hasUserSeenBetaPromotionBefore;
    }
    _checkForChannelUpdateWithNoDownload(channel) {
        return __awaiter(this, void 0, void 0, function* () {
            const previousChannelSetting = electronBuilderUpdater.channel;
            this._setElectronUpdaterChannel(channel);
            this.isUpdateCheckOnly = true;
            try {
                const channelUpdateInfo = yield electronBuilderUpdater.checkForUpdates();
                if (this._isChannelVersion(channel, channelUpdateInfo.versionInfo)) {
                    logger.info(`${channel} version was detected: ${channelUpdateInfo.versionInfo.version}`);
                    this._notifyFrontendWhenReady(channel);
                }
            }
            catch (e) {
                logger.error(`Could not check for ${channel} version update: ${e}`);
            }
            this._setElectronUpdaterChannel(previousChannelSetting);
            this.isUpdateCheckOnly = false;
        });
    }
    _isChannelVersion(channel, versionInfo) {
        return versionInfo.version.includes(channel);
    }
    _notifyFrontendWhenReady(channel) {
        const interval = setInterval(() => {
            if (windowManager.mainWindow.isVisible()) {
                windowManager.broadcastContent(`auto-update.${channel}-available`);
                clearInterval(interval);
            }
        }, MAIN_PAGE_WAIT_INTERVAL);
    }
    _setElectronUpdaterChannel(channel) {
        windowManager.broadcastContent('auto-update.channel-changed');
        electronBuilderUpdater.channel = channel;
        this._updateDowngradeAllowed();
    }
    _updateDowngradeAllowed() {
        return __awaiter(this, void 0, void 0, function* () {
            electronBuilderUpdater.allowDowngrade = electronBuilderUpdater.channel === ELECTRON_CHANNEL.production
                && this.currentVersion.includes(ELECTRON_CHANNEL.beta);
        });
    }
}
module.exports = new AutoUpdater();
//# sourceMappingURL=auto-updater.js.map