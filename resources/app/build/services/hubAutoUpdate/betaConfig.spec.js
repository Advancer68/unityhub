var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { app } = require('electron');
const axios = require('axios');
const expect = require('chai').expect;
const sinon = require('sinon');
const BetaConfig = require('./betaConfig');
const cloudConfig = require('../cloudConfig/cloudConfig');
const licenseCore = require('../licenseService/licenseCore');
const settings = require('../localSettings/localSettings');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
describe('BetaConfig', () => {
    let sandbox;
    let licenseCoreGetLicenseKindStub;
    let getLocalCountryCodeStub;
    const testBetaMappings = {
        data: {
            "Unity Pro": ["CA", "JP"],
            "Unity Personal": ["US", "CN"]
        }
    };
    const betaSettingsUndefined = {
        [settings.keys.BETA_CONFIG_PATH]: undefined
    };
    const betaSettingsDefined = {
        [settings.keys.BETA_CONFIG_PATH]: 'abc'
    };
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        axiosGetStub = sandbox.stub(axios, 'get').resolves(testBetaMappings);
        licenseCoreGetLicenseKindStub = sandbox.stub(licenseCore, 'getLicenseKind');
        getLocalCountryCodeStub = sandbox.stub(app, 'getLocaleCountryCode');
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('isBetaTargeted', () => {
        beforeEach(() => {
            cloudConfigGetHubBaseEndpointStub = sandbox.stub(cloudConfig, 'getHubBaseEndpoint').returns('');
            localSettingsStub = sandbox.stub(settings, 'get').callsFake((key) => betaSettingsUndefined[key]);
            sandbox.stub(cloudAnalytics, 'addEvent');
        });
        it('should return true if the specified license type and country code are targeted', () => __awaiter(this, void 0, void 0, function* () {
            licenseCoreGetLicenseKindStub.resolves('Unity Pro');
            getLocalCountryCodeStub.returns('CA');
            const isTargeted = yield BetaConfig.isBetaTargeted();
            expect(isTargeted).to.equal(true);
        }));
        it('should return false for a license type that is not targeted', () => __awaiter(this, void 0, void 0, function* () {
            licenseCoreGetLicenseKindStub.resolves('invalid');
            getLocalCountryCodeStub.returns('US');
            const isTargeted = yield BetaConfig.isBetaTargeted();
            expect(isTargeted).to.equal(false);
        }));
        it('should return false for a country code that is not targeted', () => __awaiter(this, void 0, void 0, function* () {
            licenseCoreGetLicenseKindStub.resolves('Unity Personal');
            getLocalCountryCodeStub.returns('MX');
            const isTargeted = yield BetaConfig.isBetaTargeted();
            expect(isTargeted).to.equal(false);
        }));
        it('should return false on error', () => __awaiter(this, void 0, void 0, function* () {
            licenseCoreGetLicenseKindStub.rejects('cannot get license type');
            const isTargeted = yield BetaConfig.isBetaTargeted();
            expect(isTargeted).to.equal(false);
        }));
        it('should emit the expected analytics event if beta targeted status cannot be determined', () => __awaiter(this, void 0, void 0, function* () {
            licenseCoreGetLicenseKindStub.rejects('cannot get license type');
            yield BetaConfig.isBetaTargeted();
            expect(cloudAnalytics.addEvent).to.have.been.calledWith({
                type: 'hub.error.v1',
                msg: {
                    error_message: 'Could not determine beta targeted status of the user',
                    error_type: 'exception'
                }
            });
        }));
    });
    describe('_getBetaConfigUrl', () => {
        beforeEach(() => {
            cloudConfigGetHubBaseEndpointStub = sandbox.stub(cloudConfig, 'getHubBaseEndpoint').returns('some/url/');
        });
        it('should use value from settings if defined', () => {
            localSettingsStub = sandbox.stub(settings, 'get').callsFake((key) => betaSettingsDefined[key]);
            const url = BetaConfig._getBetaConfigUrl();
            expect(url).to.equal('abc');
        });
        it('should use value from cloudConfig if not defined in settings', () => {
            localSettingsStub = sandbox.stub(settings, 'get').callsFake((key) => betaSettingsUndefined[key]);
            const url = BetaConfig._getBetaConfigUrl();
            expect(url).to.equal('some/url/betaConfig.json');
        });
    });
});
//# sourceMappingURL=betaConfig.spec.js.map