const logger = require('../../logger')('AutoUpdater:EventHandler');
const windowManager = require('../../windowManager/windowManager');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const handlers = new Map([
    [
        'checking-for-update',
        () => {
            logger.info('Checking for update');
        }
    ],
    [
        'update-available',
        (autoUpdater, electronBuilderUpdater) => {
            if (autoUpdater.isUpdateCheckOnly) {
                return;
            }
            logger.info('Update available');
            autoUpdater.downloadInProgress = true;
            try {
                electronBuilderUpdater.downloadUpdate().catch((error) => {
                    logger.warn('Failed to download update (promise catch):', error);
                    autoUpdater.downloadInProgress = false;
                });
            }
            catch (error) {
                logger.warn('Failed to download update (try catch):', error);
                autoUpdater.downloadInProgress = false;
            }
        }
    ],
    [
        'update-not-available',
        () => {
            logger.info('No update available');
        }
    ],
    [
        'error',
        (autoUpdater, error) => {
            autoUpdater.downloadInProgress = false;
            logger.warn(error);
        }
    ],
    [
        'download-progress',
        () => logger.debug('Downloading update...')
    ],
    [
        'update-downloaded',
        (autoUpdater, { updateInfo, downloadedUpdateHelper }) => {
            autoUpdater.updateAvailable = true;
            autoUpdater.postponeMessage = false;
            autoUpdater.downloadInProgress = false;
            if (updateInfo && updateInfo.version) {
                autoUpdater.availableVersion = updateInfo.version;
                logger.info(`Update downloaded: ${updateInfo.version} ${downloadedUpdateHelper ? downloadedUpdateHelper.file : ''}`);
            }
            else if (downloadedUpdateHelper
                && downloadedUpdateHelper.versionInfo
                && downloadedUpdateHelper.versionInfo.version) {
                autoUpdater.availableVersion = downloadedUpdateHelper.versionInfo.version;
                logger.info(`Update downloaded: ${downloadedUpdateHelper.versionInfo.version} ${downloadedUpdateHelper ? downloadedUpdateHelper.file : ''}`);
            }
            else {
                autoUpdater.availableVersion = undefined;
                logger.warn('Update downloaded with incomplete updateInfo');
            }
            logger.info('auto-update.update-available event is triggered');
            windowManager.broadcastContent('auto-update.update-available', { version: autoUpdater.availableVersion });
            if (!autoUpdater.availableVersion) {
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.ERROR,
                    msg: {
                        error_message: 'Could not determine version number from auto-updater',
                        error_type: 'exception'
                    }
                });
            }
            else {
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.UPDATE_DOWNLOADED,
                    msg: {
                        downloaded_version: autoUpdater.availableVersion,
                    },
                });
            }
        }
    ]
]);
function registerUpdateEventHandlers(autoUpdater, electronBuilderUpdater) {
    for (const [eventName, handler] of handlers) {
        electronBuilderUpdater.on(eventName, handler.bind(null, autoUpdater, electronBuilderUpdater));
    }
}
module.exports = {
    registerUpdateEventHandlers
};
//# sourceMappingURL=updateHandlers.js.map