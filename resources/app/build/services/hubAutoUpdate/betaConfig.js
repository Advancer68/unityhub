var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const axios = require('axios');
const cloudConfig = require('../cloudConfig/cloudConfig');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const licenseCore = require('../licenseService/licenseCore');
const settings = require('../localSettings/localSettings');
const { app } = require('electron');
class BetaConfig {
    static isBetaTargeted() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const betaConfigMap = yield this._getBetaConfigMap();
                const licenseType = yield this._getLicenseType();
                const countryCode = this._getCountryCode();
                return this._resolveBetaTargetedStatus(betaConfigMap, licenseType, countryCode);
            }
            catch (err) {
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.ERROR,
                    msg: {
                        error_message: 'Could not determine beta targeted status of the user',
                        error_type: 'exception'
                    }
                });
                return false;
            }
        });
    }
    static _getBetaConfigUrl() {
        return settings.get(settings.keys.BETA_CONFIG_PATH) || `${cloudConfig.getHubBaseEndpoint()}betaConfig.json`;
    }
    static _getBetaConfigMap() {
        return __awaiter(this, void 0, void 0, function* () {
            const betaConfigUrl = this._getBetaConfigUrl();
            const result = yield axios.get(betaConfigUrl);
            return result.data;
        });
    }
    static _getLicenseType() {
        return __awaiter(this, void 0, void 0, function* () {
            const licenseKind = yield licenseCore.getLicenseKind(true);
            return licenseKind;
        });
    }
    static _getCountryCode() {
        return app.getLocaleCountryCode();
    }
    static _resolveBetaTargetedStatus(betaConfigMap, licenseType, countryCode) {
        const eligibleCountries = betaConfigMap[licenseType];
        return Array.isArray(eligibleCountries) && eligibleCountries.includes(countryCode);
    }
}
module.exports = BetaConfig;
//# sourceMappingURL=betaConfig.js.map