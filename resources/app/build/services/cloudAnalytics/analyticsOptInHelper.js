var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const axios = require('axios');
const postal = require('postal');
const cloudConfig = require('../cloudConfig/cloudConfig');
const logger = require('../../logger')('AnalyticsOptinHelper');
const OPT_IN_REQUEST_RETRY_INTERVAL = 5000;
const OPT_IN_REQUEST_RETRY_COUNT = 3;
class AnalyticsOptInHelper {
    init(disableAnalytics) {
        this._analyticsEnabled = !disableAnalytics;
        this.retryCount = 0;
    }
    userSignedOut() {
    }
    userSignedIn(cloudUserId) {
        this.requestForRemoteConfig(cloudUserId);
    }
    requestForRemoteConfig(cloudUserId) {
        if (this._analyticsEnabled === false) {
            return;
        }
        this.retryCount = 0;
        this._sendRequest(cloudUserId);
    }
    get isAnalyticsEnabledOnThisMachine() {
        return this._analyticsEnabled;
    }
    _sendRequest(cloudUserId) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = {
                common: {
                    appid: 'hub',
                    clouduserid: cloudUserId
                }
            };
            const config = { 'Content-Type': 'application/json' };
            try {
                const analyticsEndpoint = this._getAnalyticsEndpoint();
                const response = yield axios.post(analyticsEndpoint, data, config);
                const analyticsEnabled = response.data.analytics.enabled;
                this._sendAnalyticsChangedEvent(cloudUserId, analyticsEnabled);
            }
            catch (error) {
                logger.warn(`Could not check analytics for user ${cloudUserId}: ${error.message}`);
                if (this.retryCount < OPT_IN_REQUEST_RETRY_COUNT) {
                    this.retryCount++;
                    yield this._retryRequest(cloudUserId);
                }
                else {
                    this._sendAnalyticsChangedEvent(cloudUserId, false);
                }
            }
        });
    }
    _getAnalyticsEndpoint() {
        return cloudConfig.urls.analyticsOptOut || cloudConfig.defaultData.analyticsOptOut;
    }
    _retryRequest(cloudUserId) {
        return new Promise(resolve => {
            setTimeout((() => __awaiter(this, void 0, void 0, function* () { return resolve(yield this._sendRequest(cloudUserId)); })), OPT_IN_REQUEST_RETRY_INTERVAL);
        });
    }
    _sendAnalyticsChangedEvent(cloudUserId, analyticsEnabled) {
        postal.publish({
            channel: 'app',
            topic: 'analyticsInfo.changed',
            data: {
                enabled: analyticsEnabled,
                cloudUserId
            }
        });
    }
}
module.exports = new AnalyticsOptInHelper();
//# sourceMappingURL=analyticsOptInHelper.js.map