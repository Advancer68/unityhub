const settings = require('../localSettings/localSettings');
const postal = require('postal');
const analyticsOptInHelper = require('./analyticsOptInHelper');
const ANONYMOUS = 'anonymous';
const UNKNOWN = 'unknown';
class AnalyticsQueue {
    init() {
        this.queue = {};
        this.currentUserId = UNKNOWN;
        this._initializeTheUserQueue(ANONYMOUS, true);
        this._initializeTheUserQueue(UNKNOWN, null);
        this.MAX_QUEUE_SIZE = settings.get(settings.keys.MAX_SIZE_FOR_ANALYTICS_QUEUE);
        this._registerEvents();
    }
    _registerEvents() {
        postal.subscribe({
            channel: 'app',
            topic: 'userInfo.changed',
            callback: (data) => this._onUserInfoChanged(data.userInfo.userId)
        });
        postal.subscribe({
            channel: 'app',
            topic: 'analyticsInfo.changed',
            callback: (data) => this._onAnalyticsInfoChanged(data)
        });
    }
    _onUserInfoChanged(newUserId) {
        if (newUserId === this.currentUserId) {
            return;
        }
        if (newUserId === '') {
            if (this.currentUserId === UNKNOWN) {
                this.addChainOfEvents(this.queue[UNKNOWN].events, ANONYMOUS);
                this.dropOptOutEvents(UNKNOWN);
            }
            this.currentUserId = ANONYMOUS;
            analyticsOptInHelper.userSignedOut();
        }
        else {
            this._initializeTheUserQueue(newUserId);
            if (this.currentUserId === UNKNOWN) {
                this.addChainOfEvents(this.queue[UNKNOWN].events, newUserId);
                this.dropOptOutEvents(UNKNOWN);
            }
            this.currentUserId = newUserId;
            analyticsOptInHelper.userSignedIn(this.currentUserId);
        }
    }
    _onAnalyticsInfoChanged(data) {
        if (this.queue[data.cloudUserId]) {
            this.queue[data.cloudUserId].enabled = data.enabled;
        }
        if (data.enabled === false) {
            this.dropOptOutEvents(data.cloudUserId);
        }
    }
    addEvent(event, cloudUserId = this.currentUserId) {
        if (!event.msg.ts) {
            event.msg.ts = new Date().getTime();
        }
        this._initializeTheUserQueue(cloudUserId);
        this.queue[cloudUserId].events.push(event);
        this._dequeueExtraEvents(cloudUserId);
    }
    addChainOfEvents(events, cloudUserId = this.currentUserId, toEnd = true) {
        this._initializeTheUserQueue(cloudUserId);
        if (toEnd) {
            this.queue[cloudUserId].events = this.queue[cloudUserId].events.concat(events);
        }
        else {
            this.queue[cloudUserId].events = events.concat(this.queue[cloudUserId].events);
        }
        this._dequeueExtraEvents(cloudUserId);
    }
    _dequeueExtraEvents(cloudUserId) {
        const diff = this.queue[cloudUserId].events.length - this.MAX_QUEUE_SIZE;
        if (diff > 0) {
            this.queue[cloudUserId].events = this.queue[cloudUserId].events.splice(diff);
        }
    }
    dropOptOutEvents(cloudUserId) {
        delete this.queue[cloudUserId];
    }
    getOptedInUsers() {
        return Object.keys(this.queue).filter(cloudUserId => this.queue[cloudUserId].enabled === true
            && this.queue[cloudUserId].events.length > 0);
    }
    getEventsCloneForUser(cloudUserId) {
        const queueClone = this.queue[cloudUserId].events.slice(0);
        this.queue[cloudUserId].events = [];
        return queueClone;
    }
    _initializeTheUserQueue(cloudUserId, enabled = null) {
        if (this.queue[cloudUserId]) {
            return;
        }
        this.queue[cloudUserId] = {
            events: [],
            enabled
        };
    }
}
module.exports = new AnalyticsQueue();
//# sourceMappingURL=analyticsQueue.js.map