var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const axios = require('axios');
const os = require('os');
const uuid = require('uuid/v1');
const postal = require('postal');
const settings = require('../localSettings/localSettings');
const logger = require('../../logger')('CloudAnalytics');
const licenseCore = require('../licenseService/licenseCore');
const cloudConfig = require('../cloudConfig/cloudConfig');
const systemInfo = require('../localSystemInfo/systemInfo');
const analyticsOptInHelper = require('./analyticsOptInHelper');
const analyticsQueue = require('./analyticsQueue');
const localConfig = require('../localConfig/localConfig');
const isInTest = typeof global.it === 'function';
class CloudAnalytics {
    constructor() {
        const platform = {
            darwin: {
                name: 'kOSX',
                id: 0,
            },
            win32: {
                name: 'kWindows',
                id: 7,
            },
            linux: {
                name: 'kLinux',
                id: 16,
            },
        }[os.platform()];
        if (!platform) {
            logger.error(`OS is not supported: ${os.platform()}`);
            throw new Error('OS is not supported');
        }
        this.common = {
            appid: 'hub',
            deviceid: '',
            hub_version: `${systemInfo.hubVersion()}-c`,
            machineid: '',
            platform: platform.name,
            platformid: platform.id,
            session_guid: uuid(),
        };
        this.quitTriggered = false;
        this.initialized = false;
        this.online = false;
        this.sendingInProgress = false;
        this.eventTypes = {
            START: 'hub.start.v1',
            ERROR: 'hub.error.v1',
            QUIT: 'hub.quit.v1',
            PAGE_VIEW: 'hub.pageview.v1',
            EXTERNAL_LINK: 'hub.externalLink.v1',
            WINDOW: 'hub.window.v1',
            OPEN_PROJECT: 'hub.projectOpen.v1',
            NEW_PROJECT: 'hub.projectNew.v2',
            TUTORIAL_DOWNLOAD_START: 'hub.tutorialDownloadStart.v1',
            TUTORIAL_DOWNLOAD_END: 'hub.tutorialDownloadEnd.v1',
            EDITOR_DOWNLOAD_START: 'hub.editorDownloadStart.v1',
            EDITOR_DOWNLOAD_END: 'hub.editorDownloadEnd.v1',
            COMPONENT_DOWNLOAD_START: 'hub.componentDownloadStart.v1',
            COMPONENT_DOWNLOAD_END: 'hub.componentDownloadEnd.v1',
            EDITOR_INSTALL_START: 'hub.editorInstallStart.v1',
            EDITOR_INSTALL_END: 'hub.editorInstallEnd.v1',
            COMPONENT_INSTALL_START: 'hub.componentInstallStart.v1',
            COMPONENT_INSTALL_END: 'hub.componentInstallEnd.v1',
            EDITOR_LOCATE: 'hub.editorLocate.v1',
            TIPS_CLICK: 'hub.tipsClick.v1',
            TIPS_HIDE: 'hub.tipsHide.v1',
            LICENSE_ACTIVATE: 'hub.licenseActivate.v1',
            UPDATE_DOWNLOADED: 'hub.updateDownloaded.v1',
            SURVEY: 'hub.survey.v1',
            TEMPLATE_DOWNLOAD: 'hub.templateDownload.v1',
            TEMPLATE_UPDATE: 'hub.templateUpgrade.v1',
            UPM_ERROR: 'hub.upmError.v1',
            UPDATE_CHANNEL_CHANGE: 'hub.releaseChannelUpdated.v1',
            BETA_PROMOTION: 'hub.betaPromotion.v1',
            AGE_GATE: 'hub.ageGate.v1',
        };
    }
    init(startTime, isHeadlessMode, disableAnalytics) {
        logger.info('Init');
        analyticsOptInHelper.init(disableAnalytics || localConfig.isAnalyticsDisabled());
        if (analyticsOptInHelper.isAnalyticsEnabledOnThisMachine === false) {
            return Promise.resolve();
        }
        this.endpoint = `${isInTest ? 'http://localhost:5000' : cloudConfig.urls['cdp-analytics']}/v1/events`;
        this.common.machineid = licenseCore.getMachineId();
        this.interval = setInterval(() => {
            this.sendEvents();
        }, settings.get(settings.keys.SEND_ANALYTICS_EVENTS_INTERVAL));
        this._registerEvents();
        this.initialized = true;
        this.addEvent({
            type: this.eventTypes.START,
            msg: {
                mode: isHeadlessMode ? 'CLI' : 'HUB UI',
                ts: startTime,
            },
        });
        return Promise.resolve();
    }
    addEvent(event) {
        if (this.initialized === false || analyticsOptInHelper.isAnalyticsEnabledOnThisMachine === false) {
            return;
        }
        analyticsQueue.addEvent(event);
    }
    sendEvents() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.online === false || this.sendingInProgress === true || analyticsOptInHelper.isAnalyticsEnabledOnThisMachine === false) {
                return;
            }
            this.sendingInProgress = true;
            analyticsQueue.getOptedInUsers().forEach((userId) => __awaiter(this, void 0, void 0, function* () {
                yield this._sendEventsForUser(userId);
            }));
            this.sendingInProgress = false;
        });
    }
    _sendEventsForUser(userId) {
        const events = analyticsQueue.getEventsCloneForUser(userId);
        const commonObject = Object.assign(this.common);
        commonObject.license_hash = licenseCore.getSerialHash();
        commonObject.cloud_user_id = userId;
        let data = `${JSON.stringify({ common: commonObject })}\n`;
        events.forEach((event) => {
            data += `${JSON.stringify(event)}\n`;
        });
        return axios
            .post(this.endpoint, data, {
            headers: {
                'Content-Type': 'application/json',
            },
            responseType: 'json',
        })
            .catch((err) => {
            logger.info(err);
            analyticsQueue.addChainOfEvents(events, userId, false);
        });
    }
    quitEvent(source) {
        if (this.quitTriggered) {
            return Promise.resolve();
        }
        this.quitTriggered = true;
        this.addEvent({
            type: this.eventTypes.QUIT,
            msg: {
                source,
            },
        });
        return this.sendEvents();
    }
    windowEvent(event, window, source) {
        this.addEvent({
            type: this.eventTypes.WINDOW,
            msg: {
                source,
                window,
                event,
            },
        });
    }
    errorEvent(message, type) {
        this.addEvent({
            type: this.eventTypes.ERROR,
            msg: {
                error_message: message,
                error_type: type,
            },
        });
    }
    _registerEvents() {
        postal.subscribe({
            channel: 'app',
            topic: 'connectInfo.changed',
            callback: (data) => {
                if (data.connectInfo.online === true && this.online === false) {
                    this.online = data.connectInfo.online;
                    this.sendEvents();
                    return;
                }
                this.online = data.connectInfo.online;
            },
        });
    }
}
module.exports = new CloudAnalytics();
//# sourceMappingURL=cloudAnalytics.js.map