const cloudAnalytics = require('./cloudAnalytics');
const licenseCore = require('../licenseService/licenseCore');
const localConfig = require('../localConfig/localConfig');
const settings = require('../localSettings/localSettings');
const analyticsOptInHelper = require('./analyticsOptInHelper');
const analyticsQueue = require('./analyticsQueue');
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const postal = require('postal');
const axios = require('axios');
chai.use(sinonChai);
chai.use(chaiAsPromised);
describe('CloudAnalytics', () => {
    let sandbox;
    let postalSpy;
    let settingsMock;
    let disableAnalyticsStub;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        settingsMock = {
            [settings.keys.MAX_SIZE_FOR_ANALYTICS_QUEUE]: 2,
            [settings.keys.SEND_ANALYTICS_EVENTS_INTERVAL]: 2,
        };
        sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);
        disableAnalyticsStub = sandbox.stub(localConfig, 'isAnalyticsDisabled').returns(false);
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('init', () => {
        beforeEach(() => {
            sandbox.stub(cloudAnalytics, 'addEvent');
            sandbox.stub(licenseCore, 'getMachineId');
            sandbox.stub(analyticsOptInHelper, 'init');
            postalSpy = sandbox.spy(postal, 'subscribe');
        });
        describe('normal start', () => {
            beforeEach(() => {
                cloudAnalytics.init(123456, false, false);
            });
            it('should initialize the OptInHelper', () => {
                expect(analyticsOptInHelper.init).to.have.been.calledWith(false);
            });
            it('should set the endpoint to the test one', () => {
                expect(cloudAnalytics.endpoint).equals('http://localhost:5000/v1/events');
            });
            it('should add the hub.start event', () => {
                expect(cloudAnalytics.addEvent).to.have.been.calledWith({ msg: { mode: "HUB UI", ts: 123456 }, type: 'hub.start.v1' });
            });
            it('should set the machineId information to common header', () => {
                expect(licenseCore.getMachineId).to.have.been.called;
            });
            it('should start the scheduler with settings', () => {
                expect(settings.get).to.have.been.calledWith(settings.keys.SEND_ANALYTICS_EVENTS_INTERVAL);
                expect(cloudAnalytics.interval).to.be.defined;
            });
            it('should subscribe to connectInfo.changed', () => {
                expect(JSON.stringify(postalSpy.getCall(0).args[0])).to.equal(JSON.stringify({
                    callback: function callback() { },
                    channel: 'app',
                    topic: 'connectInfo.changed'
                }));
            });
            it('should set the initialized flag to true', () => {
                expect(cloudAnalytics.initialized).to.equal(true);
            });
        });
        describe('headless mode (CLI)', () => {
            beforeEach(() => {
                cloudAnalytics.init(123456, true, false);
            });
            it('should add the hub.start event', () => {
                expect(cloudAnalytics.addEvent).to.have.been.calledWith({ msg: { mode: "CLI", ts: 123456 }, type: 'hub.start.v1' });
            });
        });
    });
    describe('addEvent', () => {
        beforeEach(() => {
            sandbox.stub(analyticsQueue, 'addEvent');
        });
        it('should not add if the cloud analytics is not initialized', () => {
            cloudAnalytics.initialized = false;
            cloudAnalytics.addEvent({});
            expect(analyticsQueue.addEvent).not.to.have.been.called;
        });
        it('should add the event to the queue', () => {
            cloudAnalytics.initialized = true;
            const event = { msg: { ts: 12345, event: 'test' } };
            cloudAnalytics.addEvent(event);
            expect(analyticsQueue.addEvent).to.have.been.calledWith(event);
        });
        it('should not add events if the analytics is not allowed', () => {
            cloudAnalytics.initialized = true;
            cloudAnalytics.allowAnalytics = false;
            cloudAnalytics.queue = [];
            const event = { msg: { ts: 12345, event: 'test' } };
            cloudAnalytics.addEvent(event);
            expect(cloudAnalytics.queue.length).to.equal(0);
            cloudAnalytics.allowAnalytics = true;
        });
    });
    describe('successful sendEvent', () => {
        beforeEach(() => {
            sandbox.stub(licenseCore, 'getSerialHash').returns('test');
            sandbox.stub(analyticsQueue, 'getOptedInUsers').returns([123]);
            sandbox.stub(axios, 'post').resolves();
        });
        it('should not send if user is not online', () => {
            cloudAnalytics.online = false;
            cloudAnalytics.sendingInProgress = false;
            cloudAnalytics.queue = [{ msg: 1 }, { msg: 2 }];
            return cloudAnalytics.sendEvents().then(() => {
                expect(cloudAnalytics.sendingInProgress).to.equal(false);
            });
        });
        it('should not send if the analytics are disabled machine wide', () => {
            cloudAnalytics.online = true;
            sandbox.stub(analyticsOptInHelper, 'isAnalyticsEnabledOnThisMachine').get(() => false);
            return cloudAnalytics.sendEvents().then(() => {
                expect(cloudAnalytics.sendingInProgress).to.equal(false);
            });
        });
        it('should not send if sending is in progress', () => {
            cloudAnalytics.sendingInProgress = true;
            cloudAnalytics.online = true;
            cloudAnalytics.common.license_hash = 'test2';
            cloudAnalytics.queue = [{ msg: 1 }, { msg: 2 }];
            return cloudAnalytics.sendEvents().then(() => {
                expect(cloudAnalytics.common.license_hash).to.equal('test2');
            });
        });
        it('should send serialized events', () => {
            sandbox.stub(analyticsQueue, 'getEventsCloneForUser').returns([{ msg: 1 }, { msg: 2 }]);
            cloudAnalytics.sendingInProgress = false;
            cloudAnalytics.online = true;
            cloudAnalytics.endpoint = 'http://test.test';
            cloudAnalytics.common.deviceId = 'testId';
            return cloudAnalytics.sendEvents().then(() => {
                expect(axios.post).to.have.been.calledWith(cloudAnalytics.endpoint, sinon.match.string, sinon.match.has('headers', { 'Content-Type': 'application/json' }));
                expect(axios.post).to.have.been.calledWith(cloudAnalytics.endpoint, sinon.match.string, sinon.match.has('responseType', 'json'));
                const data = axios.post.getCall(0).args[1];
                expect(data).to.include('"common"');
                expect(data).to.include('"appid":"hub"');
                expect(data).to.include('"deviceId":"testId"');
                expect(data).to.include('"hub_version"');
                expect(data).to.include('"platform"');
                expect(data).to.include('"platformid"');
                expect(data).to.include('"session_guid"');
                expect(data).to.include('{"msg":1}');
                expect(data).to.include('{"msg":2}');
                expect(data).to.include('"license_hash":"test"');
            });
        });
        it('should set the in progress flag properly', () => {
            sandbox.stub(analyticsQueue, 'getEventsCloneForUser').returns([{ msg: 1 }, { msg: 2 }]);
            cloudAnalytics.sendingInProgress = false;
            cloudAnalytics.online = true;
            cloudAnalytics.endpoint = 'http://test.test';
            cloudAnalytics.common.deviceId = 'testId';
            return cloudAnalytics.sendEvents().then(() => {
                expect(cloudAnalytics.sendingInProgress).to.equal(false);
            });
        });
    });
    describe('failed sendEvent', () => {
        const queue = [{ msg: 1 }, { msg: 2 }];
        const userId = 123;
        beforeEach(() => {
            sandbox.stub(licenseCore, 'getSerialHash').returns('test');
            sandbox.stub(analyticsQueue, 'getEventsCloneForUser').returns(queue);
            sandbox.stub(analyticsQueue, 'getOptedInUsers').returns([userId]);
            sandbox.stub(analyticsQueue, 'addChainOfEvents');
            sandbox.stub(axios, 'post').rejects();
        });
        it('should keep the queue', () => {
            cloudAnalytics.sendingInProgress = false;
            cloudAnalytics.online = true;
            cloudAnalytics.endpoint = 'http://test.test';
            cloudAnalytics.common.deviceId = 'testId';
            return cloudAnalytics.sendEvents().then(() => {
                expect(analyticsQueue.addChainOfEvents).to.have.been.calledWith(queue, userId);
            });
        });
    });
    describe('quitEvent', () => {
        beforeEach(() => {
            sandbox.stub(licenseCore, 'getSerialHash').returns('test');
            sandbox.stub(cloudAnalytics, 'addEvent');
            sandbox.stub(cloudAnalytics, 'sendEvents');
        });
        it('should not add event if quit is already added', () => {
            cloudAnalytics.quitTriggered = true;
            cloudAnalytics.quitEvent();
            expect(cloudAnalytics.addEvent).to.have.not.been.called;
            expect(cloudAnalytics.sendEvents).to.have.not.been.called;
        });
        it('should add quit event', () => {
            cloudAnalytics.quitTriggered = false;
            cloudAnalytics.quitEvent('testSource');
            expect(cloudAnalytics.addEvent).to.have.been.calledWith({ msg: { source: 'testSource' }, type: 'hub.quit.v1' });
            expect(cloudAnalytics.sendEvents).to.have.been.called;
            expect(cloudAnalytics.quitTriggered).to.equal(true);
        });
    });
    describe('windowEvent', () => {
        beforeEach(() => {
            sandbox.stub(licenseCore, 'getSerialHash').returns('test');
            sandbox.stub(cloudAnalytics, 'addEvent');
        });
        it('should add window event', () => {
            cloudAnalytics.windowEvent('testEvent', 'testWindow', 'testSource');
            expect(cloudAnalytics.addEvent).to.have.been.calledWith({ msg: { event: "testEvent", source: "testSource", window: "testWindow" }, type: 'hub.window.v1' });
        });
    });
    describe('errorEvent', () => {
        beforeEach(() => {
            sandbox.stub(licenseCore, 'getSerialHash').returns('test');
            sandbox.stub(cloudAnalytics, 'addEvent');
        });
        it('should add window event', () => {
            cloudAnalytics.errorEvent('testMsg', 'testType', 'test');
            expect(cloudAnalytics.addEvent).to.have.been.calledWith({ msg: { error_message: "testMsg", error_type: "testType" }, type: 'hub.error.v1' });
        });
    });
});
//# sourceMappingURL=cloudAnalytics.spec.js.map