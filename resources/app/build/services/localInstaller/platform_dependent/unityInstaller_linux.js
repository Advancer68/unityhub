var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const path = require('path');
const { exec, spawn } = require('child_process');
const releaseFileIO = require('../lib/releaseFileIO');
const promisify = require('es6-promisify');
const rimraf = require('rimraf');
const sudo = require('sudo-prompt');
const { fs, hubFS } = require('../../../fileSystem');
const logger = require('../../../logger')('UnityInstallerLinux');
const errors = require('../errors.js');
const zipUtility = require('../../../common/zip-utility');
class UnityInstallerLinux {
    constructor() {
        this.errors = errors;
    }
    prepareInstallService() {
        return Promise.resolve();
    }
    closeInstallService() {
    }
    uninstall(version, installationPath) {
        return promisify(rimraf)(installationPath)
            .catch((error) => {
            logger.warn(`error while uninstalling: ${error}`);
            if (error.code === 'EPERM' || error.code === 'EACCES') {
                return new Promise((resolve, reject) => {
                    sudo.exec(`${hubFS.RMRF_CMD} "${installationPath}"`, { name: 'Unity uninstaller' }, (rdError) => {
                        if (rdError) {
                            if (rdError.message === errors.PERMISSION_DENIED) {
                                resolve({ cancelled: true });
                            }
                            else {
                                reject(rdError);
                            }
                        }
                        else {
                            resolve();
                        }
                    });
                });
            }
            throw new Error(error);
        });
    }
    installEditorAndModules(currentInstaller) {
        let responseToReturn = null;
        return this.installEditor(currentInstaller.installerPath, currentInstaller.destinationPath)
            .then(() => this.modulesQueue(currentInstaller.modules))
            .then((response) => this.persistModuleInformation(currentInstaller, response))
            .then((response) => { responseToReturn = response; })
            .then(() => responseToReturn)
            .catch((installError) => {
            logger.warn(installError);
            return Promise.resolve({ error: errors.decodeError(installError) });
        });
    }
    installModules(currentInstaller) {
        return this.modulesQueue(currentInstaller.modules)
            .then((response) => this.persistModuleInformation(currentInstaller, response, false));
    }
    installEditor(installerPath, destination) {
        if (!destination)
            return Promise.reject({ error: this.errors.MISSING_DESTINATION });
        if (path.extname(installerPath) === '.zip') {
            return fs.mkdirs(destination)
                .catch((error) => {
                if (error.code === 'ENOENT' || error.code === 'ENOTDIR') {
                    return Promise.reject({ error: this.errors.INVALID_DESTINATION });
                }
                throw error;
            })
                .then(() => fs.emptyDir(destination))
                .then(() => deployZip(installerPath, destination))
                .catch((err) => {
                logger.warn('Error while deploying zip.', err);
                throw err;
            });
        }
        else if (path.extname(installerPath) === '.xz') {
            return fs.mkdirs(destination)
                .catch((error) => {
                if (error.code === 'ENOENT' || error.code === 'ENOTDIR') {
                    return Promise.reject({ error: this.errors.INVALID_DESTINATION });
                }
                throw error;
            })
                .then(() => fs.emptyDir(destination))
                .then(() => untarFile(installerPath, destination));
        }
        return Promise.reject({ error: this.errors.WRONG_INSTALLER });
    }
    modulesQueue(modules) {
        const successfulInstalls = [];
        const failedInstalls = [];
        let modulesQueue = Promise.resolve();
        modules.forEach((module => {
            modulesQueue = modulesQueue.then(() => this.installModule(module.installerPath, module.destination)
                .then(() => this.afterModuleInstallation(module))
                .then(() => { successfulInstalls.push(module); })
                .catch((installError) => {
                logger.warn(`Error while installing module ${module.name}`, installError);
                module.error = errors.decodeError(installError);
                failedInstalls.push(module);
            }));
        }));
        return modulesQueue.then(() => ({
            modules: {
                successful: successfulInstalls,
                failed: failedInstalls
            }
        }));
    }
    persistModuleInformation(installerInfo, moduleInfo, overrideSelected) {
        return releaseFileIO.write(installerInfo, moduleInfo.modules.successful, overrideSelected)
            .catch(error => {
            error = this.errors.decodeError(error);
            moduleInfo.modules.error = error;
            moduleInfo.modules.successful.forEach(module => {
                module.error = error;
                moduleInfo.modules.failed.push(module);
            });
            moduleInfo.modules.successful = [];
        })
            .then(() => moduleInfo);
    }
    installModule(installerPath, destination) {
        logger.debug(`install module ${installerPath} to ${destination}`);
        if (!destination) {
            return Promise.reject({ error: errors.MISSING_DESTINATION });
        }
        const fileExtension = path.extname(installerPath);
        if (fileExtension === '.pkg') {
            const tmpDestination = path.join(destination, 'tmp');
            return fs.mkdirs(destination)
                .then(() => fs.mkdirs(tmpDestination))
                .then(() => xarPkg(installerPath, tmpDestination))
                .then(() => untarPkg(tmpDestination, destination))
                .then(() => cleanupPkg(tmpDestination));
        }
        else if (fileExtension === '.zip') {
            return fs.mkdirs(destination)
                .then(() => deployZip(installerPath, destination));
        }
        else if (fileExtension === '.xz') {
            const playbackEngineSubdirectoryIndex = destination.indexOf('Editor/Data/PlaybackEngines');
            const finalDestination = (playbackEngineSubdirectoryIndex === -1) ?
                destination :
                destination.substring(0, playbackEngineSubdirectoryIndex);
            return fs.mkdirs(destination)
                .then(() => untarFile(installerPath, finalDestination));
        }
        else if (fileExtension === '.po') {
            destination = path.join(destination, path.basename(installerPath));
            logger.debug(`Move ${installerPath} to ${destination}`);
            return fs.copy(installerPath, destination);
        }
        return Promise.reject({ error: this.errors.WRONG_INSTALLER });
    }
    afterModuleInstallation(module) {
        return __awaiter(this, void 0, void 0, function* () {
            logger.debug('afterModuleInstallation');
            if (module.renameFrom && module.renameTo) {
                if (module.renameFrom.includes(module.renameTo)) {
                    const tempFolder = path.join(path.dirname(module.renameTo), 'temp_parent');
                    yield fs.move(module.renameFrom, tempFolder);
                    yield fs.move(tempFolder, module.renameTo, { overwrite: true }).catch((err) => this._renamingError(err, module));
                }
                else {
                    yield fs.move(module.renameFrom, module.renameTo).catch((err) => this._renamingError(err, module));
                }
            }
        });
    }
    _renamingError(err, module) {
        logger.warn(`Could not rename folder ${err}`);
        return Promise.reject(`Module ${module.name} after installation step failed`);
    }
    isVisualStudioInstalled() {
        return new Promise((resolve) => {
            resolve(false);
        });
    }
}
function cleanupPkg(tmpDestination) {
    return promisify(rimraf)(tmpDestination)
        .catch(err => {
        logger.warn(err);
    });
}
function deployZip(installerPath, destination) {
    return zipUtility.unzip(installerPath, destination)
        .catch((err) => {
        logger.warn('Unzip went wrong', err);
        throw err;
    });
}
function xarPkg(installerPath, destination) {
    return new Promise((resolve, reject) => {
        const relativePathFromLib = path.join('lib', 'linux', '7z', 'linux64', '7z');
        let sevenZipPath = path.join(__dirname, '..', '..', '..', '..', '..', 'app.asar.unpacked', relativePathFromLib);
        if (!fs.existsSync(sevenZipPath)) {
            logger.debug(`7z not found in asar-unpacked ${sevenZipPath}`);
            sevenZipPath = path.join(__dirname, '..', '..', '..', '..', relativePathFromLib);
        }
        exec(`${sevenZipPath} x -y -o"${destination}" "${installerPath}"`, {}, (error) => {
            if (error) {
                reject(error);
                return;
            }
            resolve();
        });
    });
}
function untarPkg(basePayloadPath, destination) {
    return getPayloadPath(basePayloadPath)
        .then((payloadPath) => new Promise((resolve, reject) => {
        let unpackCommand = `gzip -dc "${payloadPath}" | cpio -iu`;
        if (payloadPath.match(/Payload~/)) {
            unpackCommand = `cpio -iu < "${payloadPath}"`;
        }
        exec(`cd ${destination}; ${unpackCommand}`, {}, (error) => {
            if (error) {
                reject(error);
                return;
            }
            resolve();
        });
    }));
}
function untarFile(archivePath, destination) {
    return new Promise((resolve, reject) => {
        const child = spawn('tar', ['-C', destination, '-amxf', archivePath], {
            stdio: ['ignore', 'ignore', 'ignore']
        });
        child.on('close', (code) => {
            if (code === 0) {
                resolve();
                return;
            }
            reject(code);
        });
        child.on('error', (err) => reject(err));
    });
}
function getPayloadPath(basePath) {
    return fs.readdir(basePath)
        .then((files) => {
        const payloadDirectory = files.find((file) => file.match(/.pkg.tmp$/));
        if (payloadDirectory) {
            return path.join(basePath, payloadDirectory, 'Payload');
        }
        const payload = files.find((file) => file === 'Payload~');
        if (!payload) {
            return Promise.reject({ error: this.errors.COULD_NOT_EXTRACT });
        }
        return path.join(basePath, payload);
    });
}
module.exports = new UnityInstallerLinux();
//# sourceMappingURL=unityInstaller_linux.js.map