const errors = require('../errors.js');
const { fs } = require('../../../fileSystem');
const tools = require('../../editorApp/platformtools');
const unityInstaller = tools.require(`${__dirname}/unityInstaller`);
const os = require('os');
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
    .use(require('sinon-chai'))
    .use(require('chai-as-promised'));
if (os.platform() === 'linux') {
    describe('UnityInstaller_linux', () => {
        let sandbox;
        let fsStub;
        beforeEach(() => {
            sandbox = sinon.sandbox.create();
        });
        afterEach(() => {
            sandbox.restore();
        });
        describe('afterModuleInstallation', () => {
            it('should rename the folder if renameFrom and renameTo are given', () => {
                fsStub = sandbox.stub(fs, 'move').resolves();
                return unityInstaller.afterModuleInstallation({ renameFrom: 'from', renameTo: 'to' })
                    .then(() => expect(fsStub).to.have.been.calledWith('from', 'to'))
                    .catch((e) => expect('should not').to.eql(`come here ${e}`));
            });
            it('should rename the folder if renameTo contains the renameFrom', () => {
                fsStub = sandbox.stub(fs, 'move').resolves();
                return unityInstaller.afterModuleInstallation({ renameFrom: 'from/folder/here', renameTo: 'from/folder' })
                    .then(() => expect(fsStub).to.have.been.called)
                    .catch((e) => expect('should not').to.eql(`come here ${e}`));
            });
            it('should not try to rename the folder when renameFrom is missing', () => {
                fsStub = sandbox.stub(fs, 'move').resolves();
                return unityInstaller.afterModuleInstallation({ renameTo: 'to' })
                    .then(() => expect(fsStub).to.not.have.been.called)
                    .catch((e) => expect('should not').to.eql(`come here ${e}`));
            });
            it('should not try to rename the folder when renameTo is invalid', () => {
                fsStub = sandbox.stub(fs, 'move').resolves();
                return unityInstaller.afterModuleInstallation({ renameFrom: 'from', renameTo: '' })
                    .then(() => expect(fsStub).to.not.have.been.called)
                    .catch((e) => expect('should not').to.eql(`come here ${e}`));
            });
            it('if it cannot rename it should give an error', () => {
                fsStub = sandbox.stub(fs, 'move').rejects('error');
                return unityInstaller.afterModuleInstallation({ name: 'mod', renameFrom: 'from', renameTo: 'to' })
                    .then(() => expect('should not').to.eql('come here'))
                    .catch((e) => expect(e).to.eq(`Module mod after installation step failed`));
            });
        });
    });
}
//# sourceMappingURL=unityInstaller_linux.spec.js.map