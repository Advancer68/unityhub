var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const path = require('path');
const { exec, spawn } = require('child_process');
const releaseFileIO = require('../lib/releaseFileIO');
const promisify = require('es6-promisify');
const rimraf = require('rimraf');
const { fs, hubFS } = require('../../../fileSystem');
const logger = require('../../../logger')('UnityInstallerMac');
const errors = require('../errors.js');
const sudo = require('sudo-prompt');
const osascript = require('node-osascript');
const dmg = require('dmg');
class UnityInstallerMac {
    constructor() {
        this.errors = errors;
    }
    prepareInstallService() {
        return Promise.resolve();
    }
    closeInstallService() {
    }
    uninstall(version, installationPath) {
        return promisify(rimraf)(installationPath)
            .catch((error) => {
            logger.warn(`error while uninstalling: ${error}`);
            if (error.code === 'EPERM' || error.code === 'EACCES') {
                return new Promise((resolve, reject) => {
                    sudo.exec(`${hubFS.RMRF_CMD} "${installationPath}"`, { name: 'Unity uninstaller' }, (rdError) => {
                        if (rdError) {
                            if (rdError.message === errors.PERMISSION_DENIED) {
                                resolve({ cancelled: true });
                            }
                            else {
                                reject(rdError);
                            }
                        }
                        else {
                            resolve();
                        }
                    });
                });
            }
            throw new Error(error);
        });
    }
    installEditorAndModules(currentInstaller) {
        let responseToReturn = null;
        return this.installEditor(currentInstaller.installerPath, currentInstaller.destinationPath)
            .then(() => this.modulesQueue(currentInstaller.modules))
            .then((response) => this.persistModuleInformation(currentInstaller, response))
            .then((response) => { responseToReturn = response; })
            .then(() => responseToReturn)
            .catch((installError) => {
            logger.warn(' Error while installing the Editor', installError);
            return Promise.resolve({ error: errors.decodeError(installError) });
        });
    }
    installModules(currentInstaller) {
        return this.modulesQueue(currentInstaller.modules)
            .then((response) => this.persistModuleInformation(currentInstaller, response, false));
    }
    installEditor(installerPath, destination) {
        if (!destination)
            return Promise.reject({ error: this.errors.MISSING_DESTINATION });
        if (path.extname(installerPath) === '.pkg') {
            const tmpDestination = path.join(destination, 'tmp');
            return fs.mkdirs(destination)
                .then(() => fs.mkdirs(tmpDestination))
                .then(() => xarPkg(installerPath, tmpDestination))
                .then(() => untarPkg(tmpDestination, destination))
                .then(() => cleanupEditorPkg(destination))
                .then(() => cleanupPkg(tmpDestination))
                .catch((e) => {
                if (e.code === 'EACCES') {
                    return Promise.reject({ error: this.errors.MISSING_DESTINATION });
                }
                throw e;
            });
        }
        else if (path.extname(installerPath) === '.zip') {
            return fs.mkdirs(destination)
                .then(() => deployZip(installerPath, destination))
                .catch((err) => {
                logger.warn('Error while deploying zip ', err);
                throw err;
            });
        }
        return Promise.reject({ error: this.errors.WRONG_INSTALLER });
    }
    installPlastic(installerPath, destination) {
        if (!destination) {
            return execInstaller(installerPath)
                .then(() => checkAndSetToken('/Applications'));
        }
        if (path.extname(installerPath) === '.pkg') {
            const tmpDestination = path.join(destination, 'tmp');
            return fs.mkdirs(destination)
                .then(() => fs.mkdirs(tmpDestination))
                .then(() => xarPkg(installerPath, tmpDestination))
                .then(() => untarPkgPlastic(tmpDestination, destination))
                .then(() => cleanupPkg(tmpDestination))
                .then(() => checkAndSetToken(destination))
                .catch((e) => {
                if (e.code === 'EACCES') {
                    return Promise.reject({ error: this.errors.MISSING_DESTINATION });
                }
                throw e;
            });
        }
        return Promise.reject({ error: this.errors.WRONG_INSTALLER });
    }
    modulesQueue(modules) {
        const successfulInstalls = [];
        const failedInstalls = [];
        let modulesQueue = Promise.resolve();
        modules.forEach((module => {
            modulesQueue = modulesQueue
                .then(() => this.installModule(module.installerPath, module.destination)
                .then(() => this.afterModuleInstallation(module))
                .then(() => { successfulInstalls.push(module); })
                .catch((installError) => {
                logger.warn(`Error while installing module ${module.name}`, installError);
                module.error = errors.decodeError(installError);
                failedInstalls.push(module);
            }));
        }));
        return modulesQueue.then(() => ({
            modules: {
                successful: successfulInstalls,
                failed: failedInstalls
            }
        }));
    }
    persistModuleInformation(installerInfo, moduleInfo, overrideSelected) {
        return releaseFileIO.write(installerInfo, moduleInfo.modules.successful, overrideSelected)
            .catch(error => {
            error = this.errors.decodeError(error);
            moduleInfo.modules.error = error;
            moduleInfo.modules.successful.forEach(module => {
                module.error = error;
                moduleInfo.modules.failed.push(module);
            });
            moduleInfo.modules.successful = [];
        })
            .then(() => moduleInfo);
    }
    installModule(installerPath, destination) {
        logger.debug(`install module ${installerPath} to ${destination}`);
        const fileExtension = path.extname(installerPath);
        if (fileExtension === '.pkg') {
            if (!destination) {
                return execInstaller(installerPath);
            }
            const tmpDestination = path.join(destination, 'tmp');
            return fs.mkdirs(destination)
                .then(() => fs.mkdirs(tmpDestination))
                .then(() => xarPkg(installerPath, tmpDestination))
                .then(() => untarPkg(tmpDestination, destination))
                .then(() => cleanupPkg(tmpDestination));
        }
        else if (fileExtension === '.zip') {
            if (!destination)
                return Promise.reject({ error: this.errors.MISSING_DESTINATION });
            return fs.mkdirs(destination)
                .then(() => deployZip(installerPath, destination));
        }
        else if (fileExtension === '.po') {
            if (!destination)
                return Promise.reject({ error: this.errors.MISSING_DESTINATION });
            destination = path.join(destination, path.basename(installerPath));
            logger.debug(`Move ${installerPath} to ${destination}`);
            return fs.copy(installerPath, destination);
        }
        else if (fileExtension === '.dmg') {
            return mountDmg(installerPath, destination);
        }
        return Promise.reject({ error: this.errors.WRONG_INSTALLER });
    }
    afterModuleInstallation(module) {
        return __awaiter(this, void 0, void 0, function* () {
            logger.debug('afterModuleInstallation');
            if (module.renameFrom && module.renameTo) {
                if (module.renameFrom.includes(module.renameTo)) {
                    const tempFolder = path.join(path.dirname(module.renameTo), 'temp_parent');
                    yield fs.move(module.renameFrom, tempFolder);
                    yield fs.move(tempFolder, module.renameTo, { overwrite: true }).catch((err) => this._renamingError(err, module));
                }
                else {
                    yield fs.move(module.renameFrom, module.renameTo).catch((err) => this._renamingError(err, module));
                }
            }
        });
    }
    _renamingError(err, module) {
        logger.warn(`Could not rename folder ${err}`);
        return Promise.reject(`Module ${module.name} after installation step failed`);
    }
    isVisualStudioInstalled() {
        return new Promise((resolve) => {
            osascript.execute('id of application "Visual Studio"', (error, result) => {
                const vsInstalled = {
                    isInstalled: false,
                };
                if (error) {
                    resolve(vsInstalled);
                    return;
                }
                if (result === 'com.microsoft.visual-studio') {
                    vsInstalled.isInstalled = true;
                }
                resolve(vsInstalled);
            });
        });
    }
}
function cleanupPkg(tmpDestination) {
    return promisify(rimraf)(tmpDestination)
        .catch(err => {
        logger.warn(`Error while cleanup Pkg ${err}`);
    });
}
function execInstaller(installerPath) {
    return new Promise((resolve, reject) => {
        sudo.exec(`installer -package ${installerPath} -target /`, { name: 'Unity Hub Installer' }, (error) => {
            if (error) {
                reject(error);
                return;
            }
            resolve();
        });
    });
}
function cleanupEditorPkg(destination) {
    const unityDir = path.join(destination, 'Unity');
    return hubFS.moveFilesToParent(unityDir, destination)
        .then(() => promisify(rimraf)(unityDir))
        .catch(err => {
        logger.warn(`Error while cleaning up the Editor pkg ${err}`);
    });
}
function deployZip(installerPath, destination) {
    return new Promise((resolve, reject) => {
        const child = spawn('unzip', ['-d', destination, installerPath], {
            stdio: ['ignore', 'ignore', 'ignore']
        });
        child.on('close', (code) => {
            if (code === 0) {
                resolve();
                return;
            }
            if (code === 1) {
                logger.error(`unzip might have been unable to unzip some files from ${installerPath} to ${destination}`);
                reject();
                return;
            }
            logger.warn(`unzip returned an error code ${code} while trying to unzip ${installerPath} to ${destination}`);
            reject(code);
        });
        child.on('error', (err) => {
            logger.warn(`unzip went wrong ${err}`);
            reject(err);
        });
    });
}
function mountDmg(installerPath, destination = '/Applications') {
    return new Promise((resolve, reject) => {
        logger.debug(`mount ${installerPath}`);
        dmg.mount(installerPath, (error, dmgMountPath) => {
            if (error) {
                logger.warn(`Error while mounting the dmg ${installerPath} ${error}`);
                reject(error);
            }
            fs.readdir(dmgMountPath)
                .then((files) => {
                const filename = files.find(file => file.endsWith('.app'));
                logger.info(`copy ${dmgMountPath}/${filename} to ${destination}`);
                return copyFile(path.join(dmgMountPath, filename), destination);
            })
                .then(() => {
                logger.debug(`unmount ${dmgMountPath}`);
                return dmg.unmount(dmgMountPath, (e) => {
                    if (e) {
                        logger.warn(`Error while unmounting the dmg ${installerPath} ${e}`);
                        reject(e);
                    }
                    resolve();
                });
            })
                .catch((err) => {
                logger.warn(`Error while processing the dmg mount path ${dmgMountPath} ${err}`);
                reject(err);
            });
        });
    });
}
function copyFile(file, destination) {
    return new Promise((resolve, reject) => {
        exec(`cp -a "${path.join(file, '.')}" "${destination}"`, (err) => {
            if (err) {
                logger.warn(`Error while copying the dmg file ${file} to ${destination}`, err);
                reject(err);
            }
            resolve();
        });
    });
}
function xarPkg(installerPath, destination) {
    return new Promise((resolve, reject) => {
        exec(`xar -xf "${installerPath}" -C "${destination}"`, {}, (error) => {
            if (error) {
                reject(error);
                return;
            }
            resolve();
        });
    });
}
function untarPkg(basePayloadPath, destination) {
    return getPayloadPath(basePayloadPath)
        .then((payloadPath) => new Promise((resolve, reject) => {
        const child = spawn('tar', ['-C', destination, '-zmxf', payloadPath], {
            stdio: ['ignore', 'ignore', 'ignore']
        });
        child.on('close', (code) => {
            if (code === 0) {
                resolve();
                return;
            }
            reject(code);
        });
        child.on('error', (err) => reject(err));
    }));
}
function getPayloadPath(basePath) {
    return fs.readdir(basePath)
        .then((files) => {
        const payloadDirectory = files.find((file) => file.match(/.pkg.tmp$/));
        if (!payloadDirectory) {
            return Promise.reject({ error: this.errors.COULD_NOT_EXTRACT });
        }
        return path.join(basePath, payloadDirectory, 'Payload');
    });
}
function untarPkgPlastic(basePayloadPath, destination) {
    return getPlasticPayloadPath(basePayloadPath)
        .then((payloadPath) => new Promise((resolve, reject) => {
        const child = spawn('tar', ['-C', destination, '-zmxf', payloadPath], {
            stdio: ['ignore', 'ignore', 'ignore']
        });
        child.on('close', (code) => {
            if (code === 0) {
                resolve();
                return;
            }
            reject(code);
        });
        child.on('error', (err) => reject(err));
    }));
}
function getPlasticPayloadPath(basePath) {
    return fs.readdir(basePath)
        .then((files) => {
        const payloadDirectory = files.find((file) => file.match(/.macplastic.pkg$/));
        if (!payloadDirectory) {
            return Promise.reject({ error: this.errors.COULD_NOT_EXTRACT });
        }
        return path.join(basePath, payloadDirectory, 'Payload');
    });
}
function checkAndSetToken(basePath) {
    const tokeFile = path.join(basePath, 'PlasticSCM.app', 'Contents', 'MonoBundle', 'cloudedition.token');
    if (!fs.existsSync(tokeFile)) {
        fs.writeFile(tokeFile, '', err => {
            if (err) {
                logger.error('fail to create file cloudedition.token', err);
            }
        });
    }
    return Promise.resolve('');
}
module.exports = new UnityInstallerMac();
//# sourceMappingURL=unityInstaller_mac.js.map