var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { UnityIPCClient } = require('../../../common/common');
const releaseFileIO = require('../lib/releaseFileIO');
const logger = require('../../../logger')('InstallerWinProcess');
const unityInstallSteps = require('./unityInstallSteps_win');
if (process.execPath.indexOf('electron') !== -1) {
    logger.activateWinInstallerProcessOutput();
    logger.info('start install sub process');
}
const ipcClient = new UnityIPCClient('hubInstallServer');
start();
function start() {
    return __awaiter(this, void 0, void 0, function* () {
        logger.info('trying to connect to the ipc server');
        yield ipcClient.connect();
        ipcClient.on('disconnect', () => {
            process.exit(0);
        });
        ipcClient.on('installEditor', (data) => {
            let responseToReturn = null;
            const editor = data[0];
            ipcClient.serverLog(`installEditor ${JSON.stringify(data)}`);
            return unityInstallSteps.beforeEditorInstallation(editor)
                .then(() => unityInstallSteps.execInstaller(editor.installerPath, null, editor.destinationPath))
                .then(() => unityInstallSteps.afterEditorInstallation(editor))
                .then((installData) => {
                ipcClient.serverLog('installation finished');
                data.push(installData);
                ipcClient.emit('done-installEditor', data);
            })
                .then((response) => { responseToReturn = response; })
                .then(() => responseToReturn)
                .catch((e) => {
                ipcClient.serverLog(`error while installing an Editor. ${JSON.stringify(e)}`);
                ipcClient.emit('error-installEditor', e);
            });
        });
        ipcClient.on('installModule', (data) => {
            const module = data;
            ipcClient.serverLog(`installModule ${JSON.stringify(data)}`);
            return unityInstallSteps.beforeModuleInstallation(module)
                .then(() => unityInstallSteps.execInstaller(module.installerPath, module.cmd, module.destination))
                .then(() => unityInstallSteps.afterModuleInstallation(module))
                .then(() => ipcClient.emit('done-installModule', {
                restartRequired: false
            }))
                .catch((e) => {
                moduleInstallErrorHandling(e, module);
            });
        });
        ipcClient.on('writeModulesFile', (data) => {
            const installer = data[0];
            const modules = data[1];
            const override = data[2];
            ipcClient.serverLog(`write modules file for the Editor ${installer.editor.version}`);
            return releaseFileIO.write(installer, modules, override)
                .then(() => ipcClient.emit('done-writeModulesFile', data))
                .catch((e) => ipcClient.emit('error-writeModulesFile', e));
        });
        ipcClient.on('uninstallEditor', (data) => {
            ipcClient.serverLog(`uninstallEditor ${JSON.stringify(data)}`);
            return unityInstallSteps.uninstallEditor(data.version, data.editorRootPath)
                .then(() => ipcClient.emit('done-uninstallEditor', data))
                .catch((e) => ipcClient.emit('error-uninstallEditor', e));
        });
    });
}
function moduleInstallErrorHandling(e, module) {
    if (module.cmd && module.cmd.indexOf('VisualStudio') !== -1) {
        switch (e.code) {
            case 3010:
                ipcClient.emit('done-installModule', { restartRequired: true });
                break;
            case 1602:
            case 5004:
                ipcClient.serverLog('visual studio installation was cancelled');
                ipcClient.emit('done-installModule', { restartRequired: false });
                break;
            default:
                ipcClient.emit('error-installModule', e);
        }
    }
    else {
        ipcClient.emit('error-installModule', e);
    }
}
//# sourceMappingURL=unityInstallerProcess_win.js.map