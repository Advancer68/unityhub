const errors = require('./errors.js');
const { hubFS } = require('../../fileSystem');
const windowManager = require('../../windowManager/windowManager');
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
    .use(require('sinon-chai'))
    .use(require('chai-as-promised'));
const proxyquire = require('proxyquire');
let rimrafStub = {};
const unityInstaller = proxyquire('./unityInstaller', { 'rimraf': rimrafStub });
describe('UnityInstaller', () => {
    const release = {
        "version": "2017.1.2f1"
    };
    let sandbox;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        windowManager.broadcastContent = sandbox.stub();
        unityInstaller.platformDependent.installModules = function () { return Promise.resolve('response'); };
        unityInstaller.platformDependent.installEditorAndModules = function () { return Promise.resolve('response'); };
        unityInstaller.platformDependent.uninstall = function () { return Promise.resolve('response'); };
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('installEditor', () => {
        it('should give an error when version is not found', () => {
            return unityInstaller.installEditor(undefined, 'path/to/installer', 'destination/path').then((response) => {
                expect(response).to.eql({ error: errors.UNKNOWN_INSTALLER });
                expect(unityInstaller.jobs.length).to.eql(0);
            });
        });
        it('should give an error when installation path is invalid', () => {
            sandbox.stub(hubFS, 'isFilePresent').returns(false);
            return unityInstaller.installEditor(release, 'invalid/installer/path', 'invalid/destination').then((response) => {
                expect(response).to.eql({ error: errors.INSTALLER_MISSING });
                expect(unityInstaller.jobs.length).to.eql(0);
            });
        });
        it('should give an error when editor is not defined', () => {
            return unityInstaller.installEditor(undefined, 'invalid/installer/path', 'invalid/destination').then((response) => {
                expect(response).to.eql({ error: errors.UNKNOWN_INSTALLER });
                expect(unityInstaller.jobs.length).to.eql(0);
            });
        });
        it('should return a response and remove from job list when installation succeeds', () => {
            sandbox.stub(hubFS, 'isFilePresent').returns(true);
            return unityInstaller.installEditor(release, 'valid/installer/path', 'valid/destination').then((response) => {
                expect(response).to.eql('response');
                expect(unityInstaller.jobs.length).to.eql(0);
            });
        });
    });
    describe('installModules', () => {
        it('should give an error when editor version is not found', () => {
            return unityInstaller.installModules(undefined, 'unknown/base/install/path', null).then((response) => {
                expect(response).to.eql({ error: errors.UNKNOWN_EDITOR });
                expect(unityInstaller.jobs.length).to.eql(0);
            });
        });
        it('should return a response and remove from job list when installation succeeds', () => {
            const modules = [
                {
                    installerPath: 'module/1/install/path',
                    destination: `${unityInstaller.UNITY_PATH_PLACEHOLDER}/end/of/path`
                }
            ];
            return unityInstaller.installModules(release, 'valid/base/install/path', modules).then((response) => {
                expect(response).to.eql('response');
                expect(unityInstaller.jobs.length).to.eql(0);
            });
        });
    });
    describe('fixModulesPath', () => {
        it('should give a clean install path', () => {
            const modules = [
                {
                    installerPath: 'module/1/install/path',
                    destination: `${unityInstaller.UNITY_PATH_PLACEHOLDER}/end/of/path`
                },
                {
                    installerPath: 'module/2/install/path',
                    destination: ''
                }
            ];
            let response = unityInstaller.fixModulesPath({ modules: modules }, 'base/install/path/of/editor');
            expect(response[0].destination).to.eql('base/install/path/of/editor/end/of/path');
            expect(response[1].destination).to.eql('');
        });
    });
    describe('uninstallEditor', () => {
        it('should remove from job list when done', () => {
            return unityInstaller.uninstall('2017.0.1f1', 'base/path/to/editor/folder').then(() => {
                expect(unityInstaller.jobs.length).to.eql(0);
            });
        });
    });
});
//# sourceMappingURL=unityInstaller.spec.js.map