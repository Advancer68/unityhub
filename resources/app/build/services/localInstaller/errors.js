const os = require('os');
module.exports = {
    CANCEL_FAILED: {
        code: 'CANCEL_FAILED',
        message: 'The installation could not revert installation.'
    },
    COULD_NOT_EXTRACT: {
        code: 'COULD_NOT_EXTRACT',
        message: 'There was an issue extracting the installer content.'
    },
    COULD_NOT_PERSIST_MODULES: {
        code: 'COULD_NOT_PERSIST_MODULES',
        message: 'The modules data could not be saved to disk.'
    },
    DEFAULT: {
        code: 'DEFAULT',
        message: 'An unexpected error occurred during installation.'
    },
    INSTALLER_MISSING: {
        code: 'INSTALLER_MISSING',
        message: 'The Unity installer could not be found.'
    },
    MISSING_DESTINATION: {
        code: 'MISSING_DESTINATION',
        message: 'The install destination was invalid or missing.'
    },
    PERMISSION_DENIED: {
        code: 'User did not grant permission.',
        message: 'The Hub does not have proper permissions to install Unity.'
    },
    WRONG_INSTALLER: {
        code: 'WRONG_INSTALLER',
        message: `The installer given is not a valid format and should be a zip, a ${os.platform() === 'darwin' ? 'pkg' : 'exe, a msi'} or a po.`
    },
    INSTALLER_ERROR: {
        code: 'INSTALL_ERROR',
        message: 'Error happened while executing the installer. Aborting.'
    },
    INVALID_ARCHITECTURE: {
        code: 'INVALID_ARCHITECTURE',
        message: 'Trying to install a 64-bit software on a 32-bit architecture. The installation would work, but running Unity would fail. Aborting.'
    },
    INVALID_DESTINATION: {
        code: 'INVALID_DESTINATION',
        message: 'Cannot install at the given destination'
    },
    WRONG_UNINSTALLER: {
        code: 'WRONG_UNINSTALLER',
        message: `The uninstaller given is not a valid format and should be a ${os.platform() === 'darwin' ? 'pkg' : 'exe'}.`
    },
    UNINSTALLER_MISSING: {
        code: 'UNINSTALLER_MISSING',
        message: 'The Unity uninstaller could not be found.'
    },
    UNKNOWN_INSTALLER: {
        code: 'UNKNOWN_INSTALLER',
        message: 'The Unity installer asked is not part of the list of supported installations. Aborting.'
    },
    UNKNOWN_EDITOR: {
        code: 'UNKNOWN_EDITOR',
        message: 'The Unity Editor could not be found. Aborting.'
    },
    decodeError(installError) {
        let error = this.DEFAULT;
        if (installError === undefined) {
            return error;
        }
        Object.keys(this).forEach((i) => {
            if ((installError.code && installError.code === this[i].code) ||
                (installError.message && installError.message === this[i].message) ||
                (installError.error && installError.error.code && installError.error.code === this[i].code) ||
                (installError.error && installError.error.message && installError.error.message === this[i].message)) {
                error = this[i];
            }
        });
        return error;
    }
};
//# sourceMappingURL=errors.js.map