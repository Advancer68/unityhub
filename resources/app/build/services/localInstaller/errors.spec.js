const errors = require('./errors.js');
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
    .use(require('sinon-chai'))
    .use(require('chai-as-promised'));
describe('Installer errors', () => {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('decode errors', () => {
        it('should give a valid error when giving only the code', () => {
            expect(errors.decodeError({ code: 'UNKNOWN_EDITOR' })).to.eql(errors.UNKNOWN_EDITOR);
        });
        it('should give a valid error when giving only the message', () => {
            expect(errors.decodeError({ message: 'The Unity uninstaller could not be found.' })).to.eql(errors.UNINSTALLER_MISSING);
        });
        it('should give a valid error when giving the error nested under error', () => {
            expect(errors.decodeError({ error: errors.UNINSTALLER_MISSING })).to.eql(errors.UNINSTALLER_MISSING);
        });
        it('should give a default error when giving a string', () => {
            expect(errors.decodeError('string')).to.eql(errors.DEFAULT);
        });
        it('should give a default error when giving an invalid code', () => {
            expect(errors.decodeError({ code: 'INVALID_CODE' })).to.eql(errors.DEFAULT);
        });
        it('should give a default error when giving no params', () => {
            expect(errors.decodeError()).to.eql(errors.DEFAULT);
        });
    });
});
//# sourceMappingURL=errors.spec.js.map