var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { app } = require('electron');
const i18next = require('i18next');
const promisify = require('es6-promisify');
const localStorageAsync = require('electron-json-storage');
const localStorage = {
    get: promisify(localStorageAsync.get, localStorageAsync),
    set: promisify(localStorageAsync.set, localStorageAsync),
};
const LANGUAGE_STORAGE_KEY = 'languageConfig';
const languages = [
    { name: 'English', code: 'en', locale: 'en_US' },
    { name: '日本語', code: 'ja', locale: 'ja_JP' },
    { name: '한국어', code: 'ko', locale: 'ko_FR' },
    { name: '简体中文', code: 'zh_CN', locale: 'zh_CN' },
    { name: '繁體中文', code: 'zh_TW', locale: 'zh_TW' }
];
class i18nConfig {
    getAvailableLanguages() {
        if (app.argv.debug) {
            return languages.concat({ name: 'Pseudo-localization', code: 'ploc' });
        }
        return languages;
    }
    getLocale() {
        const currentLanguage = i18next.language;
        const data = languages.find(x => x.code === currentLanguage);
        return data ? data.locale : 'en_US';
    }
    getLanguage() {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield localStorage.get(LANGUAGE_STORAGE_KEY);
            return data ? data.language : undefined;
        });
    }
    setLanguage(code) {
        return __awaiter(this, void 0, void 0, function* () {
            yield localStorage.set(LANGUAGE_STORAGE_KEY, { language: code });
            return yield i18next.changeLanguage(code);
        });
    }
}
module.exports = new i18nConfig();
//# sourceMappingURL=i18nConfig.js.map