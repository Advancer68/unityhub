const { shell } = require('electron');
const windowManager = require('../../windowManager/windowManager');
const logger = require('../../logger')('Window');
const path = require('path');
class Window {
    showSignInWindow() {
        windowManager.showSignInWindow();
    }
    showLicenseFlowSignIn() {
        windowManager.showSignInWindow(windowManager.signInWindow.pages.SIGN_IN_LICENSE);
    }
    showSignUp() {
        windowManager.showSignInWindow(windowManager.signInWindow.pages.SIGN_UP);
    }
    showNewProjectWindow(editor = {}) {
        windowManager.showNewProjectWindow(editor);
    }
    closeNewProjectWindow() {
        windowManager.newProjectWindow.close();
    }
    showItemInFolder(itemPath) {
        itemPath = path.normalize(itemPath);
        shell.showItemInFolder(itemPath);
    }
    showLogDirectory() {
        const logPath = logger.getLogDirectoryPath();
        shell.openItem(logPath);
    }
    showOnboardingWindow() {
        windowManager.onboardingWindow.show();
    }
    hideOnboardingWindow() {
        windowManager.onboardingWindow.hide();
    }
    showMainWindow() {
        windowManager.mainWindow.show();
    }
    hideMainWindow() {
        windowManager.mainWindow.hide();
    }
    openExternal(url, redirect) {
        return windowManager.openExternal(url, redirect);
    }
    exitDebugMode() {
        windowManager.mainWindow.setTitle();
        windowManager.mainWindow.reload();
    }
}
module.exports = new Window();
//# sourceMappingURL=window.js.map