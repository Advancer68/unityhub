const CACHE_TTL = 5 * 6000;
const hubCache = require('../../common/hubCache');
const REMOTE_TEMPLATES_KEY = 'remoteTemplates';
const TEMPLATE_INFO_KEY = 'templatesInfo';
const UNITY_VERSION_BRANCH_KEY = 'unityVersionBranch';
class unityPackageManagerCache extends hubCache {
    setRemoteTemplates(templates) {
        this.set(this._getRemoteTemplatesKey(), templates, CACHE_TTL);
    }
    getRemoteTemplates() {
        return this.get(this._getRemoteTemplatesKey());
    }
    _getRemoteTemplatesKey() {
        return REMOTE_TEMPLATES_KEY;
    }
    setTemplatesInfo(templateName, editorVersion, templateInfo) {
        this.set(this._getTemplatesInfoKey(templateName, editorVersion), templateInfo, CACHE_TTL);
    }
    getTemplatesInfo(templateName, editorVersion) {
        return this.get(this._getTemplatesInfoKey(templateName, editorVersion));
    }
    _getTemplatesInfoKey(templateName, editorVersion) {
        return `${TEMPLATE_INFO_KEY}:${templateName}:${editorVersion}`;
    }
    setUnityVersionBranch(unityVersion, unityVersionBranch) {
        this.set(this._getUnityVersionBranchKey(unityVersion), unityVersionBranch);
    }
    getUnityVersionBranch(unityVersion) {
        return this.get(this._getUnityVersionBranchKey(unityVersion));
    }
    _getUnityVersionBranchKey(editorVersion) {
        return `${UNITY_VERSION_BRANCH_KEY}:${editorVersion}`;
    }
}
module.exports = new unityPackageManagerCache();
//# sourceMappingURL=unityPackageManagerCache.js.map