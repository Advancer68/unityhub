var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const upm = require('@upm/core');
const UnityVersion = require('@unityhub/unity-version');
const axios = require('axios');
const semver = require('semver');
const path = require('path');
const cloudConfig = require('../cloudConfig/cloudConfig');
const settings = require('../localSettings/localSettings');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const logger = require('../../logger')('UnityPackageManagerService');
const { fs, hubFS } = require('../../fileSystem');
const unityPackageManagerCacheService = require('./unityPackageManagerCache');
const UPM_TEMPLATE_SEARCH_QUERY = 'com.unity.template';
const PARTIAL_DOWNLOAD_SUFFIX = '.part';
class UnityPackageManagerService {
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            const dynamicTemplatesConfigEndpoint = settings.get(settings.keys.DYNAMIC_TEMPLATES_CONFIG_PATH)
                || cloudConfig.getDefaultUrls().templatesConfig;
            logger.debug(`Dynamic templates config will be loaded from: ${dynamicTemplatesConfigEndpoint}`);
            try {
                const response = yield axios.get(dynamicTemplatesConfigEndpoint);
                this.dynamicTemplatesConfig = response.data;
                logger.info(`Dynamic templates config loaded: ${this.dynamicTemplatesConfig}`);
            }
            catch (error) {
                this.dynamicTemplatesConfig = {};
                logger.error(`An error occurred while retrieving the dynamic templates config: ${error}`);
            }
        });
    }
    getDownloadedTemplatesForEditorVersion(destination, unityInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            const allEditorDependencies = yield this._getAllEditorDependencies(destination);
            const editorDependencies = allEditorDependencies[unityInfo.unityVersion] ? allEditorDependencies[unityInfo.unityVersion].dependencies : {};
            const templateNamesAndVersions = Object.entries(editorDependencies);
            const editorCompatibleTemplateInfos = (yield Promise.all(templateNamesAndVersions.map(([templateName, templateVersion]) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const templateInfo = yield this._getTemplateInfo(templateName, unityInfo, true);
                    const downloadLocation = this._getTarDownloadLocation(destination, templateName, templateVersion);
                    if (templateInfo && templateInfo.versions && templateInfo.versions.compatible && templateInfo.versions.compatible.includes(templateVersion)) {
                        const isTemplateMissing = yield this._isFileMissing(downloadLocation);
                        if (!isTemplateMissing) {
                            templateInfo.version = templateVersion;
                            templateInfo.tar = downloadLocation;
                            return templateInfo;
                        }
                    }
                    logger.debug(`Upm returned a badly formatted information for the package ${templateName} with the response ${JSON.stringify(templateInfo)} `);
                    return null;
                }
                catch (error) {
                    logger.debug(`Error while fetching package info for ${templateName} : ${error.message}`);
                    return null;
                }
            })))).filter(templateInfo => templateInfo !== null);
            return editorCompatibleTemplateInfos;
        });
    }
    getDownloadableTemplates(unityInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            const remoteTemplates = yield this._searchForRemoteTemplates(UPM_TEMPLATE_SEARCH_QUERY);
            const unityVersionBranch = this._getUnityVersionBranch(unityInfo.unityVersion);
            const editorCompatibleRemoteTemplateInfos = (yield Promise.all(remoteTemplates.map((template) => __awaiter(this, void 0, void 0, function* () {
                if (!this._isTemplateCompatibleWithEditor(template.name, unityVersionBranch)) {
                    logger.debug(`Template is not supported by config: ${template.name}`);
                    return null;
                }
                try {
                    const templateInfo = yield this._getTemplateInfo(template.name, unityInfo, false);
                    return templateInfo;
                }
                catch (error) {
                    logger.debug(`Downloadable template ${template.name} is not applicable for ${unityInfo.unityVersion} : ${error}`);
                    return null;
                }
            })))).filter((templateInfo) => templateInfo !== null);
            return editorCompatibleRemoteTemplateInfos;
        });
    }
    _searchForRemoteTemplates(searchQuery) {
        return __awaiter(this, void 0, void 0, function* () {
            const remoteTemplatesCache = unityPackageManagerCacheService.getRemoteTemplates();
            if (remoteTemplatesCache) {
                return remoteTemplatesCache;
            }
            try {
                const remoteTemplates = yield this._searchUnderBothHosts(searchQuery);
                unityPackageManagerCacheService.setRemoteTemplates(remoteTemplates);
                return remoteTemplates;
            }
            catch (error) {
                const errorMessage = 'Error while searching for remote templates in upm';
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.UPM_ERROR,
                    msg: {
                        error_message: errorMessage,
                        error_type: 'upm_search'
                    }
                });
                logger.error(`${errorMessage}: ${error.message}`);
                throw error;
            }
        });
    }
    _searchUnderBothHosts(searchQuery) {
        return __awaiter(this, void 0, void 0, function* () {
            const upmHostEditorResults = (yield upm.search('editor', undefined, { type: 'template', query: searchQuery })).packages;
            const upmHostHubResults = (yield upm.search('hub', undefined, { type: 'template', query: searchQuery })).packages;
            const upmHostHubResultNames = upmHostHubResults.map(upmHostHubResult => upmHostHubResult.name);
            const filteredUPMHostEditorResults = upmHostEditorResults
                .filter(upmHostEditorResult => !upmHostHubResultNames.includes(upmHostEditorResult.name));
            return [...filteredUPMHostEditorResults, ...upmHostHubResults];
        });
    }
    downloadRemoteTemplate(templateName, destination, unityInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const templateInfo = yield this._getTemplateInfo(templateName, unityInfo, false);
                const downloadPath = this._getTarDownloadLocation(destination, templateInfo.name, templateInfo.version);
                const tempManifestLocation = path.join(destination, `temp-${templateName}`);
                yield this._verifyAndCleanDownloadState(downloadPath, tempManifestLocation);
                const isTemplateMissing = yield this._isFileMissing(downloadPath);
                if (isTemplateMissing) {
                    yield this._downloadTemplate(templateInfo, downloadPath);
                    yield this._resolveDependencies(templateInfo.name, downloadPath, unityInfo, tempManifestLocation);
                }
                yield this._updateManifest(templateInfo.name, templateInfo.version, unityInfo.unityVersion, destination);
                templateInfo.tar = downloadPath;
                return templateInfo;
            }
            catch (error) {
                logger.error(`An error occurred when downloading ${templateName} from upm : ${error.message}`);
                throw error;
            }
        });
    }
    getUpgradableTemplates(localTemplates, unityInfo) {
        return __awaiter(this, void 0, void 0, function* () {
            return (yield Promise.all(localTemplates.map((template) => __awaiter(this, void 0, void 0, function* () {
                try {
                    const templateInfo = yield this._getTemplateInfo(template.name, unityInfo, false);
                    if (semver.gt(templateInfo.version.split('-')[0], template.version.split('-')[0])) {
                        templateInfo.tar = template.tar;
                        return templateInfo;
                    }
                    return null;
                }
                catch (error) {
                    logger.debug(`Upgradable template ${template.name} is not applicable for ${unityInfo.unityVersion} : ${error}`);
                    return null;
                }
            })))).filter(templateInfo => templateInfo !== null);
        });
    }
    _verifyAndCleanDownloadState(downloadPath, tempManifestLocation) {
        return __awaiter(this, void 0, void 0, function* () {
            const partialDownloadLocation = `${downloadPath}${PARTIAL_DOWNLOAD_SUFFIX}`;
            const isPartialDownloadMissing = yield this._isFileMissing(partialDownloadLocation);
            if (!isPartialDownloadMissing) {
                logger.info(`Removing partially downloaded file ${partialDownloadLocation}`);
                yield fs.remove(partialDownloadLocation);
            }
            const isTempUPMResolveManifestMissing = yield this._isFileMissing(tempManifestLocation);
            if (!isTempUPMResolveManifestMissing) {
                logger.info(`Removing temp upm resolve manifest ${tempManifestLocation}`);
                yield fs.remove(tempManifestLocation);
            }
        });
    }
    _isFileMissing(downloadPath) {
        return new Promise((resolve) => {
            fs.access(downloadPath, fs.F_OK, (error) => {
                if (error) {
                    logger.debug(`Template missing at ${downloadPath}`);
                    return resolve(true);
                }
                logger.debug(`Template already found at ${downloadPath}`);
                return resolve(false);
            });
        });
    }
    _downloadTemplate(templateInfo, downloadPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const hasEnoughSpaceOnDisk = yield this._hasEnoughSpaceOnDisk(templateInfo.tarball.size, downloadPath);
            if (!hasEnoughSpaceOnDisk) {
                this._handleDownloadError('Insufficient space');
            }
            logger.info(`Downloading remote template from ${templateInfo.tarball.url} to location: ${downloadPath}`);
            try {
                yield this._handleDownload(templateInfo, downloadPath);
            }
            catch (error) {
                this._handleDownloadError(`Error while downloading the template ${templateInfo.name}`);
            }
            return templateInfo;
        });
    }
    _hasEnoughSpaceOnDisk(templateSize, downloadPath) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const availableSpace = (yield hubFS.getDiskSpaceAvailable(downloadPath)).available;
                return templateSize < availableSpace;
            }
            catch (error) {
                logger.error('Cannot access the available disk space');
                throw error;
            }
        });
    }
    _handleDownload(templateInfo, downloadPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const response = yield axios.get(templateInfo.tarball.url, { responseType: 'stream' });
            const partialDownloadPath = `${downloadPath}${PARTIAL_DOWNLOAD_SUFFIX}`;
            yield new Promise((resolve, reject) => {
                response.data.pipe(fs.createWriteStream(partialDownloadPath));
                response.data.on('end', () => {
                    logger.debug(`Renaming downloaded file from ${partialDownloadPath} to ${downloadPath}`);
                    fs.rename(partialDownloadPath, downloadPath, (error) => __awaiter(this, void 0, void 0, function* () {
                        if (error) {
                            yield fs.remove(partialDownloadPath);
                            logger.error(`Could not rename file ${partialDownloadPath} to ${downloadPath}`);
                            reject(error);
                        }
                        logger.debug(`Template ${templateInfo.name} finished downloading`);
                        resolve();
                    }));
                });
                response.data.on('error', () => {
                    logger.debug(`Template ${templateInfo.name} could not be downloaded`);
                    reject('File download error');
                });
            });
        });
    }
    _handleDownloadError(errorMessage) {
        cloudAnalytics.addEvent({
            type: cloudAnalytics.eventTypes.ERROR,
            msg: {
                error_message: errorMessage,
                error_type: 'popup'
            }
        });
        logger.error(errorMessage);
        throw new Error(errorMessage);
    }
    _resolveDependencies(templateName, downloadPath, unityInfo, tempManifestLocation) {
        return __awaiter(this, void 0, void 0, function* () {
            const templateManifestLocation = path.join(tempManifestLocation, 'manifest.json');
            yield fs.outputJson(templateManifestLocation, { dependencies: { [templateName]: `file:${downloadPath}` } });
            try {
                yield upm.resolve(tempManifestLocation, unityInfo, { cachingLevel: 'global' });
            }
            catch (error) {
                yield fs.remove(tempManifestLocation);
                const errorMessage = `Error while resolving dependencies of the downloaded template ${templateName}`;
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.UPM_ERROR,
                    msg: {
                        error_message: errorMessage,
                        error_type: 'upm_resolve'
                    }
                });
                logger.error(`${errorMessage}: ${error.message}`);
                throw error;
            }
            yield fs.remove(tempManifestLocation);
        });
    }
    _updateManifest(templateName, templateVersion, editorVersion, destination) {
        return __awaiter(this, void 0, void 0, function* () {
            const allEditorDependencies = yield this._getAllEditorDependencies(destination);
            if (!allEditorDependencies[editorVersion]) {
                allEditorDependencies[editorVersion] = {
                    dependencies: {}
                };
            }
            allEditorDependencies[editorVersion].dependencies[templateName] = templateVersion;
            yield fs.outputJson(path.join(destination, 'manifest.json'), allEditorDependencies);
        });
    }
    _getAllEditorDependencies(destination) {
        return __awaiter(this, void 0, void 0, function* () {
            const manifestLocation = path.join(destination, 'manifest.json');
            try {
                return yield fs.readJson(manifestLocation);
            }
            catch (e) {
                const errorMessage = 'Manifest was corrupted, dependencies are unknown, forced to reset manifest.json';
                cloudAnalytics.addEvent({
                    type: cloudAnalytics.eventTypes.ERROR,
                    msg: {
                        error_message: errorMessage,
                        error_type: 'exception'
                    }
                });
                logger.error(`${errorMessage}: ${e}`);
                yield fs.outputJson(manifestLocation, {});
                return {};
            }
        });
    }
    _getTarDownloadLocation(destination, name, version) {
        return path.join(destination, `${name}-${version}.tgz`);
    }
    _getTemplateInfo(templateName, unityInfo, offlineMode = false) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!offlineMode) {
                const templateInfoCache = unityPackageManagerCacheService.getTemplatesInfo(templateName, unityInfo.unityVersion);
                if (templateInfoCache) {
                    return templateInfoCache;
                }
            }
            try {
                const upmTemplateInfo = (yield upm.getPackageInfo(templateName, unityInfo, { offlineMode })).package;
                const latestCompatibleVersion = this._getLatestCompatibleVersion(templateName, upmTemplateInfo.versions.compatible);
                const latestTemplateInfo = (yield upm.getPackageInfo(latestCompatibleVersion, unityInfo, { offlineMode })).package;
                latestTemplateInfo.dynamicIcon = this._getDynamicIcon(templateName, this._getUnityVersionBranch(unityInfo.unityVersion));
                if (!offlineMode) {
                    unityPackageManagerCacheService.setTemplatesInfo(templateName, unityInfo.unityVersion, latestTemplateInfo);
                }
                return latestTemplateInfo;
            }
            catch (e) {
                throw new Error(`Requested template ${templateName} could not be found by upm: ${e}`);
            }
        });
    }
    _getLatestCompatibleVersion(templateName, allCompatiableVersions) {
        const latestCompatibleVersion = allCompatiableVersions.slice().pop();
        return `${templateName}@${latestCompatibleVersion}`;
    }
    _isTemplateCompatibleWithEditor(templateName, unityVersionBranch) {
        return this.dynamicTemplatesConfig[unityVersionBranch] && !!this.dynamicTemplatesConfig[unityVersionBranch][templateName];
    }
    _getDynamicIcon(templateName, unityVersionBranch) {
        return this.dynamicTemplatesConfig[unityVersionBranch]
            && this.dynamicTemplatesConfig[unityVersionBranch][templateName]
            && this.dynamicTemplatesConfig[unityVersionBranch][templateName].icon;
    }
    _getUnityVersionBranch(unityVersion) {
        const unityVersionBranchFromCache = unityPackageManagerCacheService.getUnityVersionBranch(unityVersion);
        if (unityVersionBranchFromCache) {
            return unityVersionBranchFromCache;
        }
        try {
            const unityVersionBranch = (new UnityVersion(unityVersion)).branch;
            unityPackageManagerCacheService.setUnityVersionBranch(unityVersion, unityVersionBranch);
            return unityVersionBranch;
        }
        catch (error) {
            logger.info(`Unity version not supported by the current configuration: ${unityVersion}`);
            return '';
        }
    }
}
module.exports = new UnityPackageManagerService();
//# sourceMappingURL=unityPackageManagerService.js.map