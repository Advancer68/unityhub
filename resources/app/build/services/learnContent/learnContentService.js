var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const _ = require('lodash');
const UnityVersion = require('@unityhub/unity-version');
const PollingClient = require('../../common/pollingClient');
const localConfig = require('../localConfig/localConfig');
const settings = require('../localSettings/localSettings');
const editorManager = require('../editorManager/editorManager');
const logger = require('../../logger')('learnContentService');
const STORAGE_KEY = 'learnContent.json';
const VALID_SKILL_LEVELS = [
    'beginner',
    'intermediate',
    'advanced'
];
class LearnContentService extends PollingClient {
    init() {
        if (!localConfig.isLearnDisabled()) {
            super.init();
        }
        this.supportedEditorBranches = this._getSupportedEditorBranches();
    }
    setEndpoint() {
        this.endpoint = this._getLearnContentEndpoint();
        logger.info(`Learn content endpoint: ${this.endpoint}`);
    }
    get defaultData() {
        return [{
                projects: [],
                tutorials: []
            }];
    }
    get logger() {
        return logger;
    }
    get jsonStorageKey() {
        return STORAGE_KEY;
    }
    get refreshIntervalKey() {
        return settings.keys.SERVICES_URL_INTERVAL;
    }
    get isInitBlocking() {
        return false;
    }
    get overwriteDefault() {
        return true;
    }
    _getLearnContentEndpoint() {
        const environment = settings.get(settings.keys.CLOUD_ENVIRONMENT);
        let baseUrl = '';
        if (environment === 'production') {
            baseUrl = 'https://learn.unity.com';
        }
        else {
            baseUrl = 'https://connect-next.unity.com';
        }
        return `${baseUrl}/api/learn/hublist`;
    }
    getLearnProjects() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._getContentByCategory('projects');
        });
    }
    getLearnTutorials() {
        return __awaiter(this, void 0, void 0, function* () {
            return yield this._getContentByCategory('tutorials');
        });
    }
    _getContentByCategory(category) {
        return __awaiter(this, void 0, void 0, function* () {
            const learnContentData = yield this.data;
            return this._processLearnContentForFrontend(learnContentData[category]);
        });
    }
    _processLearnContentForFrontend(learnContent) {
        if (!_.isArray(learnContent)) {
            return [];
        }
        return learnContent.filter(this._isValidContent.bind(this)).map(this._verifyDownloadableContent.bind(this));
    }
    _isValidContent(learnContent) {
        const isValidContent = _.isString(learnContent.imageUrl)
            && _.isString(learnContent.title)
            && this._isValidSkillLevel(learnContent.skillLevel)
            && _.isNumber(learnContent.duration)
            && _.isString(learnContent.tutorialUrl);
        if (!isValidContent) {
            logger.warn(`Removing learn content: ${JSON.stringify(learnContent)} because it cannot be consumed by the frontend`);
        }
        return isValidContent;
    }
    _isValidSkillLevel(skillLevel) {
        return VALID_SKILL_LEVELS.includes(skillLevel);
    }
    _verifyDownloadableContent(learnContent) {
        const isDownloadable = _.isObjectLike(learnContent.downloadableContent);
        if (isDownloadable) {
            const isValidDownloadableContent = this._isValidDownloadableContent(learnContent.downloadableContent);
            if (!isValidDownloadableContent) {
                logger.warn(`Removing downloadable learn content: ${JSON.stringify(learnContent.downloadableContent)} because it cannot be consumed by the frontend`);
                delete learnContent.downloadableContent;
            }
        }
        return learnContent;
    }
    _isValidDownloadableContent(downloadableContent) {
        return this._verifySupportedUnityEditorVersion(downloadableContent.supportedEditorVersion)
            && _.isString(downloadableContent.packageId)
            && _.isString(downloadableContent.assetUrl)
            && _.isString(downloadableContent.size)
            && _.isFinite(parseFloat(downloadableContent.size));
    }
    _verifySupportedUnityEditorVersion(editorVersion) {
        const editorBranch = this._getUnityEditorBranch(editorVersion);
        return this.supportedEditorBranches.includes(editorBranch);
    }
    _getSupportedEditorBranches() {
        return editorManager.getReleases()
            .map(editor => this._getUnityEditorBranch(editor.version))
            .filter(editorBranch => editorBranch !== null);
    }
    _getUnityEditorBranch(editorVersion) {
        try {
            return (new UnityVersion(editorVersion)).branch;
        }
        catch (e) {
            return null;
        }
    }
}
module.exports = new LearnContentService();
//# sourceMappingURL=learnContentService.js.map