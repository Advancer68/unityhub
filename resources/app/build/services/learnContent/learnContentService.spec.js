var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const learnContent = require('./learnContentService');
const settings = require('../localSettings/localSettings');
const localStorage = require('electron-json-storage');
const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
chai.use(sinonChai);
describe('LearnContent', () => {
    let sandbox, settingsMock;
    let mockStorage = {};
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        settingsMock = {
            [settings.keys.SERVICES_URL_INTERVAL]: 100
        };
        sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);
        sandbox.stub(localStorage, 'get').callsFake((key, callback) => { callback(null, mockStorage); });
        sandbox.stub(localStorage, 'set').callsFake((key, value, callback) => { callback(); });
        learnContent.supportedEditorBranches = ['2018.3'];
        learnContent.data = {
            projects: [{
                    imageUrl: 'https://res.cloudinary.com/something/image/upload/v1561646725/sample-projects/john-lemon_aoea0z.png',
                    title: 'John Lemon\'s Haunted Jaunt: 3D Beginner',
                    skillLevel: 'beginner',
                    duration: 330,
                    summary: 'Welcome to the John Lemon\'s Haunted Jaunt: 3D Beginner Project! In this project, you won\'t just discover how to create a stealth game - each of the 10 tutorials also explains the principles behind every step. No previous experience is needed, which makes John Lemon\'s Haunted Jaunt the perfect start to your journey with Unity.',
                    tutorialUrl: 'https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner',
                    downloadableContent: {
                        packageId: '143848',
                        assetUrl: 'http://assetstorev1-prd-cdn.unity3d.com/download/1f81b6ad-4f75-42cf-b5bf-9a6e8ca3615a',
                        size: '125600000',
                        supportedEditorVersion: '2018.3.14f1'
                    }
                }],
            tutorials: [{
                    imageUrl: 'https://res.cloudinary.com/something/image/upload/v1561646725/sample-projects/gameplay-scripting_bbdnwr.png',
                    title: 'Beginner Gameplay Scripting',
                    skillLevel: 'beginner',
                    duration: 190,
                    summary: 'Learn about programming for game development, from the very beginning.',
                    tutorialUrl: 'https://learn.unity.com/project/beginner-gameplay-scripting'
                }]
        };
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('getLearnProjects', () => {
        it('should get the correct learn content', () => __awaiter(this, void 0, void 0, function* () {
            const result = yield learnContent.getLearnProjects();
            expect(result).to.deep.equal([{
                    imageUrl: 'https://res.cloudinary.com/something/image/upload/v1561646725/sample-projects/john-lemon_aoea0z.png',
                    title: 'John Lemon\'s Haunted Jaunt: 3D Beginner',
                    skillLevel: 'beginner',
                    duration: 330,
                    summary: 'Welcome to the John Lemon\'s Haunted Jaunt: 3D Beginner Project! In this project, you won\'t just discover how to create a stealth game - each of the 10 tutorials also explains the principles behind every step. No previous experience is needed, which makes John Lemon\'s Haunted Jaunt the perfect start to your journey with Unity.',
                    tutorialUrl: 'https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner',
                    downloadableContent: {
                        packageId: '143848',
                        assetUrl: 'http://assetstorev1-prd-cdn.unity3d.com/download/1f81b6ad-4f75-42cf-b5bf-9a6e8ca3615a',
                        size: '125600000',
                        supportedEditorVersion: '2018.3.14f1'
                    }
                }]);
        }));
    });
    describe('getLearnTutorials', () => {
        it('should get the correct learn content', () => __awaiter(this, void 0, void 0, function* () {
            const result = yield learnContent.getLearnTutorials();
            expect(result).to.deep.equal([{
                    imageUrl: 'https://res.cloudinary.com/something/image/upload/v1561646725/sample-projects/gameplay-scripting_bbdnwr.png',
                    title: 'Beginner Gameplay Scripting',
                    skillLevel: 'beginner',
                    duration: 190,
                    summary: 'Learn about programming for game development, from the very beginning.',
                    tutorialUrl: 'https://learn.unity.com/project/beginner-gameplay-scripting'
                }]);
        }));
    });
    describe('Filtering incorrect learn content', () => {
        it('should remove content if imageUrl is not a string', () => __awaiter(this, void 0, void 0, function* () {
            learnContent.data.projects[0].imageUrl = 0;
            const result = yield learnContent.getLearnProjects();
            expect(result).to.deep.equal([]);
        }));
        it('should remove content if title is not a string', () => __awaiter(this, void 0, void 0, function* () {
            learnContent.data.projects[0].title = 0;
            const result = yield learnContent.getLearnProjects();
            expect(result).to.deep.equal([]);
        }));
        describe('skillLevel', () => {
            it('should not remove content if "beginner"', () => __awaiter(this, void 0, void 0, function* () {
                learnContent.data.projects[0].skillLevel = 'beginner';
                const result = yield learnContent.getLearnProjects();
                expect(result).to.deep.equal([{
                        imageUrl: 'https://res.cloudinary.com/something/image/upload/v1561646725/sample-projects/john-lemon_aoea0z.png',
                        title: 'John Lemon\'s Haunted Jaunt: 3D Beginner',
                        skillLevel: 'beginner',
                        duration: 330,
                        summary: 'Welcome to the John Lemon\'s Haunted Jaunt: 3D Beginner Project! In this project, you won\'t just discover how to create a stealth game - each of the 10 tutorials also explains the principles behind every step. No previous experience is needed, which makes John Lemon\'s Haunted Jaunt the perfect start to your journey with Unity.',
                        tutorialUrl: 'https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner',
                        downloadableContent: {
                            packageId: '143848',
                            assetUrl: 'http://assetstorev1-prd-cdn.unity3d.com/download/1f81b6ad-4f75-42cf-b5bf-9a6e8ca3615a',
                            size: '125600000',
                            supportedEditorVersion: '2018.3.14f1'
                        }
                    }]);
            }));
            it('should not remove content if "intermediate"', () => __awaiter(this, void 0, void 0, function* () {
                learnContent.data.projects[0].skillLevel = 'intermediate';
                const result = yield learnContent.getLearnProjects();
                expect(result).to.deep.equal([{
                        imageUrl: 'https://res.cloudinary.com/something/image/upload/v1561646725/sample-projects/john-lemon_aoea0z.png',
                        title: 'John Lemon\'s Haunted Jaunt: 3D Beginner',
                        skillLevel: 'intermediate',
                        duration: 330,
                        summary: 'Welcome to the John Lemon\'s Haunted Jaunt: 3D Beginner Project! In this project, you won\'t just discover how to create a stealth game - each of the 10 tutorials also explains the principles behind every step. No previous experience is needed, which makes John Lemon\'s Haunted Jaunt the perfect start to your journey with Unity.',
                        tutorialUrl: 'https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner',
                        downloadableContent: {
                            packageId: '143848',
                            assetUrl: 'http://assetstorev1-prd-cdn.unity3d.com/download/1f81b6ad-4f75-42cf-b5bf-9a6e8ca3615a',
                            size: '125600000',
                            supportedEditorVersion: '2018.3.14f1'
                        }
                    }]);
            }));
            it('should not remove content if "advanced"', () => __awaiter(this, void 0, void 0, function* () {
                learnContent.data.projects[0].skillLevel = 'advanced';
                const result = yield learnContent.getLearnProjects();
                expect(result).to.deep.equal([{
                        imageUrl: 'https://res.cloudinary.com/something/image/upload/v1561646725/sample-projects/john-lemon_aoea0z.png',
                        title: 'John Lemon\'s Haunted Jaunt: 3D Beginner',
                        skillLevel: 'advanced',
                        duration: 330,
                        summary: 'Welcome to the John Lemon\'s Haunted Jaunt: 3D Beginner Project! In this project, you won\'t just discover how to create a stealth game - each of the 10 tutorials also explains the principles behind every step. No previous experience is needed, which makes John Lemon\'s Haunted Jaunt the perfect start to your journey with Unity.',
                        tutorialUrl: 'https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner',
                        downloadableContent: {
                            packageId: '143848',
                            assetUrl: 'http://assetstorev1-prd-cdn.unity3d.com/download/1f81b6ad-4f75-42cf-b5bf-9a6e8ca3615a',
                            size: '125600000',
                            supportedEditorVersion: '2018.3.14f1'
                        }
                    }]);
            }));
            it('should remove content if not correct', () => __awaiter(this, void 0, void 0, function* () {
                learnContent.data.projects[0].skillLevel = 'not a valid skillLevel';
                const result = yield learnContent.getLearnProjects();
                expect(result).to.deep.equal([]);
            }));
        });
        it('should remove content if duration is not a number', () => __awaiter(this, void 0, void 0, function* () {
            learnContent.data.projects[0].duration = '1234';
            const result = yield learnContent.getLearnProjects();
            expect(result).to.deep.equal([]);
        }));
        it('should remove content if tutorialUrl is not a string', () => __awaiter(this, void 0, void 0, function* () {
            learnContent.data.projects[0].tutorialUrl = 0;
            const result = yield learnContent.getLearnProjects();
            expect(result).to.deep.equal([]);
        }));
        it('should remove downloadableContent if supportedEditorVersion is not valid', () => __awaiter(this, void 0, void 0, function* () {
            learnContent.data.projects[0].downloadableContent.supportedEditorVersion = 'foo.bar.editor';
            const result = yield learnContent.getLearnProjects();
            expect(result).to.deep.equal([{
                    imageUrl: 'https://res.cloudinary.com/something/image/upload/v1561646725/sample-projects/john-lemon_aoea0z.png',
                    title: 'John Lemon\'s Haunted Jaunt: 3D Beginner',
                    skillLevel: 'beginner',
                    duration: 330,
                    summary: 'Welcome to the John Lemon\'s Haunted Jaunt: 3D Beginner Project! In this project, you won\'t just discover how to create a stealth game - each of the 10 tutorials also explains the principles behind every step. No previous experience is needed, which makes John Lemon\'s Haunted Jaunt the perfect start to your journey with Unity.',
                    tutorialUrl: 'https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner'
                }]);
        }));
        it('should remove downloadContent if the editor is not supported by the hub', () => __awaiter(this, void 0, void 0, function* () {
            learnContent.data.projects[0].downloadableContent.supportedEditorVersion = '2016.1.14f1';
            const result = yield learnContent.getLearnProjects();
            expect(result).to.deep.equal([{
                    imageUrl: 'https://res.cloudinary.com/something/image/upload/v1561646725/sample-projects/john-lemon_aoea0z.png',
                    title: 'John Lemon\'s Haunted Jaunt: 3D Beginner',
                    skillLevel: 'beginner',
                    duration: 330,
                    summary: 'Welcome to the John Lemon\'s Haunted Jaunt: 3D Beginner Project! In this project, you won\'t just discover how to create a stealth game - each of the 10 tutorials also explains the principles behind every step. No previous experience is needed, which makes John Lemon\'s Haunted Jaunt the perfect start to your journey with Unity.',
                    tutorialUrl: 'https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner'
                }]);
        }));
        it('should remove downloadableContent if packageId is not a string', () => __awaiter(this, void 0, void 0, function* () {
            learnContent.data.projects[0].downloadableContent.packageId = 0;
            const result = yield learnContent.getLearnProjects();
            expect(result).to.deep.equal([{
                    imageUrl: 'https://res.cloudinary.com/something/image/upload/v1561646725/sample-projects/john-lemon_aoea0z.png',
                    title: 'John Lemon\'s Haunted Jaunt: 3D Beginner',
                    skillLevel: 'beginner',
                    duration: 330,
                    summary: 'Welcome to the John Lemon\'s Haunted Jaunt: 3D Beginner Project! In this project, you won\'t just discover how to create a stealth game - each of the 10 tutorials also explains the principles behind every step. No previous experience is needed, which makes John Lemon\'s Haunted Jaunt the perfect start to your journey with Unity.',
                    tutorialUrl: 'https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner'
                }]);
        }));
        it('should remove downloadableContent if assetUrl is not a string', () => __awaiter(this, void 0, void 0, function* () {
            learnContent.data.projects[0].downloadableContent.assetUrl = 0;
            const result = yield learnContent.getLearnProjects();
            expect(result).to.deep.equal([{
                    imageUrl: 'https://res.cloudinary.com/something/image/upload/v1561646725/sample-projects/john-lemon_aoea0z.png',
                    title: 'John Lemon\'s Haunted Jaunt: 3D Beginner',
                    skillLevel: 'beginner',
                    duration: 330,
                    summary: 'Welcome to the John Lemon\'s Haunted Jaunt: 3D Beginner Project! In this project, you won\'t just discover how to create a stealth game - each of the 10 tutorials also explains the principles behind every step. No previous experience is needed, which makes John Lemon\'s Haunted Jaunt the perfect start to your journey with Unity.',
                    tutorialUrl: 'https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner'
                }]);
        }));
        it('should remove downloadableContent if size is not a string', () => __awaiter(this, void 0, void 0, function* () {
            learnContent.data.projects[0].downloadableContent.size = 0;
            const result = yield learnContent.getLearnProjects();
            expect(result).to.deep.equal([{
                    imageUrl: 'https://res.cloudinary.com/something/image/upload/v1561646725/sample-projects/john-lemon_aoea0z.png',
                    title: 'John Lemon\'s Haunted Jaunt: 3D Beginner',
                    skillLevel: 'beginner',
                    duration: 330,
                    summary: 'Welcome to the John Lemon\'s Haunted Jaunt: 3D Beginner Project! In this project, you won\'t just discover how to create a stealth game - each of the 10 tutorials also explains the principles behind every step. No previous experience is needed, which makes John Lemon\'s Haunted Jaunt the perfect start to your journey with Unity.',
                    tutorialUrl: 'https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner'
                }]);
        }));
        it('should remove downloadableContent if size is not parse-able into a number', () => __awaiter(this, void 0, void 0, function* () {
            learnContent.data.projects[0].downloadableContent.size = 'NaN';
            const result = yield learnContent.getLearnProjects();
            expect(result).to.deep.equal([{
                    imageUrl: 'https://res.cloudinary.com/something/image/upload/v1561646725/sample-projects/john-lemon_aoea0z.png',
                    title: 'John Lemon\'s Haunted Jaunt: 3D Beginner',
                    skillLevel: 'beginner',
                    duration: 330,
                    summary: 'Welcome to the John Lemon\'s Haunted Jaunt: 3D Beginner Project! In this project, you won\'t just discover how to create a stealth game - each of the 10 tutorials also explains the principles behind every step. No previous experience is needed, which makes John Lemon\'s Haunted Jaunt the perfect start to your journey with Unity.',
                    tutorialUrl: 'https://learn.unity.com/project/john-lemon-s-haunted-jaunt-3d-beginner'
                }]);
        }));
    });
});
//# sourceMappingURL=learnContentService.spec.js.map