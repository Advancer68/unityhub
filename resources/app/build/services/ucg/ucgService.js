const ucgConfig = require('./defaultUCG.json');
class UCGService {
    constructor() {
        this.showUCG = ucgConfig.showUCG;
    }
    setShowUCG(show) {
        this.showUCG = show;
    }
    getShowUCG() {
        return this.showUCG;
    }
}
module.exports = new UCGService();
//# sourceMappingURL=ucgService.js.map