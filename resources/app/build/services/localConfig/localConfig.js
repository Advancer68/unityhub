const logger = require('../../logger')('LocalConfig');
const settings = require('../localSettings/localSettings');
const cloudConfig = require('../cloudConfig/cloudConfig');
const PollingClient = require('../../common/pollingClient');
const STORAGE_KEY = 'hubConfig';
const defaultConfig = {
    [settings.keys.DISABLE_CLOUD_PROJECTS]: true,
    [settings.keys.DISABLE_ANALYTICS]: true,
    [settings.keys.DISABLE_SIGNIN]: true,
    [settings.keys.DISABLE_LEARN]: false,
    [settings.keys.DISABLE_COMMUNITY]: true,
    [settings.keys.DISABLE_PE]: false,
    [settings.keys.DISABLE_VS]: false,
    [settings.keys.DISABLE_AUTO_UPDATE]: true,
    [settings.keys.DISABLE_ELEVATE]: false,
};
class LocalConfig extends PollingClient {
    get logger() {
        return logger;
    }
    get jsonStorageKey() {
        return STORAGE_KEY;
    }
    get refreshIntervalKey() {
        return settings.keys.HUB_CONFIG_REFRESH_INTERVAL;
    }
    get isInitBlocking() {
        return false;
    }
    get defaultData() {
        return defaultConfig;
    }
    get overwriteDefault() {
        return false;
    }
    init() {
        for (const key in defaultConfig) {
            if (settings.get(key) !== undefined) {
                logger.debug(`Default config for ${key} is overwritten by local settings`);
                defaultConfig[key] = settings.get(key);
            }
        }
        return super.init();
    }
    setEndpoint() {
        let baseUrl = cloudConfig.getHubBaseEndpoint();
        if (baseUrl.slice(-1) !== '/') {
            baseUrl += '/';
        }
        this.endpoint = `${baseUrl}hubConfig.json`;
        logger.info(`Local Config endpoint: ${this.endpoint}`);
    }
    get hubConfig() {
        return Object.assign({}, this.data);
    }
    getEnvironment() {
        return Promise.resolve(settings.get(settings.keys.CLOUD_ENVIRONMENT));
    }
    getCommunityUrl() {
        return cloudConfig.getCommunityUrl();
    }
    getUPRUrl() {
        return cloudConfig.getUPRUrl();
    }
    getUCGUrl() {
        return cloudConfig.getUCGUrl();
    }
    getPlasticWebServer() {
        return cloudConfig.getPlasticWebServer();
    }
    isAnalyticsDisabled() {
        return this._getConfig(settings.keys.DISABLE_ANALYTICS);
    }
    isCloudProjectsDisabled() {
        return this._getConfig(settings.keys.DISABLE_CLOUD_PROJECTS);
    }
    isLearnDisabled() {
        return this._getConfig(settings.keys.DISABLE_LEARN);
    }
    isCommunityDisabled() {
        return this._getConfig(settings.keys.DISABLE_COMMUNITY);
    }
    isAutoUpdateDisabled() {
        return this._getConfig(settings.keys.DISABLE_AUTO_UPDATE);
    }
    isSignInDisabled() {
        return this._getConfig(settings.keys.DISABLE_SIGNIN);
    }
    isElevateDisabled() {
        return this._getConfig(settings.keys.DISABLE_ELEVATE);
    }
    isPEDisabled() {
        return this._getConfig(settings.keys.DISABLE_PE);
    }
    isVSDisabled() {
        return this._getConfig(settings.keys.DISABLE_VS);
    }
    getNewUserOnboardingConfig() {
        return this._getConfig(settings.keys.NEW_USER_ONBOARDING_CONFIG);
    }
    getBetaFeedbackUrl() {
        const betaFeedbackUrlFromLocalSettings = settings.get(settings.keys.BETA_FEEDBACK_URL);
        if (betaFeedbackUrlFromLocalSettings) {
            return betaFeedbackUrlFromLocalSettings;
        }
        return this._getConfig(settings.keys.BETA_FEEDBACK_URL);
    }
    _getConfig(key) {
        return this.data ? this.data[key] : this.defaultData[key];
    }
}
module.exports = new LocalConfig();
//# sourceMappingURL=localConfig.js.map