'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const electron = require('electron');
const path = require('path');
const { fs } = require('../../fileSystem');
const editorHelper = require('../../common/editor-helper');
const logger = require('../../logger')('PluginManager');
const windowManager = require('../../windowManager/windowManager');
const upm = require('@upm/core');
const ListStorage = require('../editorManager/lib/listStorage');
const promisify = require('es6-promisify');
const settings = require('../localSettings/localSettings');
const chokidar = { watch: () => { throw new Error('Don\'t run this code!'); } };
const PLUGINS_KEY = 'hub.plugins';
const userDataFolder = editorHelper.getBaseUserDataPath();
const pluginFolder = path.join(userDataFolder, 'UnityHub', 'Plugins');
class PluginManager {
    constructor() {
        this.updateAvailable = false;
        this.resolveInProgress = false;
        this.pluginRegistry = {};
        this.packageListStorage = new ListStorage(PLUGINS_KEY, 'packageId');
        this.logger = logger;
        this.require = (module) => require(module);
    }
    init() {
        this.updateInterval = settings.get(settings.keys.CHECK_FOR_UPDATE_INTERVAL);
        this.watcher = chokidar.watch(path.join(pluginFolder, 'manifest.json'), {
            persistent: true
        });
        this.watcher.on('change', () => this.resolvePackages());
        return Promise.resolve()
            .then(() => this.packageListStorage.init())
            .then(() => {
            this.packages = this.packageListStorage.items;
        })
            .then(() => this.activatePlugins())
            .then(() => this.resolvePackages());
    }
    resolvePackages(forceRefresh = false) {
        return __awaiter(this, void 0, void 0, function* () {
            if (!forceRefresh && !this.resolveInProgress) {
                this.resolveInProgress = true;
                const packages = yield this._getPackages(pluginFolder);
                var a = JSON.stringify(Object.values(this.packages));
                var b = JSON.stringify(packages);
                if (a !== b) {
                    this.logger.info(`Plugins have changed, need to restart Hub, packages: ${packages}`);
                    this.packageListStorage.clear();
                    if (packages !== undefined) {
                        packages.forEach(pkg => {
                            this.packageListStorage.addItem(pkg);
                        });
                    }
                    this.packageListStorage.save();
                    this.updateAvailable = true;
                    windowManager.broadcastContent('plugins.resolved', packages);
                }
                this.resolveInProgress = false;
            }
        });
    }
    _getPackages(manifestFolder) {
        return __awaiter(this, void 0, void 0, function* () {
            const manifest = {};
            const appPath = electron.app.getAppPath();
            const unityInfo = {
                unityVersion: '2018.3',
                editorPath: appPath,
                builtInPackagesPath: path.join(appPath, 'Packages')
            };
            const manifestPath = path.join(manifestFolder, 'manifest.json');
            if (!(yield fs.pathExists(manifestPath))) {
                yield fs.ensureDir(manifestFolder);
                yield fs.writeJSON(manifestPath, manifest);
            }
            try {
                const resolution = yield promisify(upm.resolve)(manifestFolder, unityInfo, 'global');
                return resolution.packages;
            }
            catch (err) {
                this.logger.error(err);
                throw err;
            }
        });
    }
    activatePlugins() {
        return __awaiter(this, void 0, void 0, function* () {
            const promises = Object.keys(this.packages).map((key) => {
                const plugin = this.packages[key];
                return this._activatePlugin(plugin);
            });
            return Promise.all(promises);
        });
    }
    _activatePlugin(plugin) {
        return __awaiter(this, void 0, void 0, function* () {
            const packageInfo = yield fs.readJSON(path.join(plugin.resolvedPath, 'package.json'));
            this.pluginRegistry[packageInfo.name] = plugin;
            if ('main' in packageInfo) {
                try {
                    const pluginEntryModule = path.join(plugin.resolvedPath, packageInfo.main);
                    plugin.module = this.require(pluginEntryModule);
                    plugin.activateFn = plugin.module.activate;
                    plugin.activateFn();
                    windowManager.broadcastContent('plugin.activated', plugin);
                }
                catch (exception) {
                    this.logger.error(`Could not activate plugin ${packageInfo.name} due to exception: ${exception}`);
                }
            }
            else {
                this.logger.warn(`skipping plugin ${packageInfo.name} because of missing "main" field`);
            }
        });
    }
    activateClientSidePlugin(pluginName, pluginPath) {
        windowManager.broadcastContent('plugin.activated', pluginPath);
    }
    restart() {
        electron.app.relaunch();
        electron.app.exit(0);
    }
    remindLater() {
        setTimeout(() => {
            if (this.updateAvailable === true) {
                windowManager.broadcastContent('plugins.resolved');
            }
        }, this.updateInterval);
    }
}
module.exports = new PluginManager();
//# sourceMappingURL=pluginManager.js.map