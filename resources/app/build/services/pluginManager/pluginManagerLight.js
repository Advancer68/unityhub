'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const electron = require('electron');
const path = require('path');
const editorHelper = require('../../common/editor-helper');
const logger = require('../../logger')('PluginManager');
const windowManager = require('../../windowManager/windowManager');
const ListStorage = require('../editorManager/lib/listStorage');
const settings = require('../localSettings/localSettings');
const packageCrawler = require('./lib/packageCrawler');
const { fs } = require('../../fileSystem');
const PLUGINS_KEY = 'hub.plugins';
const userDataFolder = editorHelper.getBaseUserDataPath();
const pluginFolder = path.join(userDataFolder, 'UnityHub', 'Plugins');
class PluginManagerLight {
    constructor() {
        this.updateAvailable = false;
        this.resolveInProgress = false;
        this.pluginRegistry = {};
        this.packageListStorage = new ListStorage(PLUGINS_KEY, 'packageId');
    }
    init() {
        return __awaiter(this, void 0, void 0, function* () {
            this.updateInterval = settings.get(settings.keys.CHECK_FOR_UPDATE_INTERVAL);
            yield this.packageListStorage.init();
            this.packages = this.packageListStorage.items;
            yield this.activatePlugins();
            this.resolvePackages().catch((err) => {
                logger.debug('failed to resolve packages', err);
            });
        });
    }
    activatePlugins() {
        return __awaiter(this, void 0, void 0, function* () {
            const pluginKeys = Object.keys(this.packages);
            yield Promise.all(pluginKeys.map((pluginName) => __awaiter(this, void 0, void 0, function* () {
                const plugin = this.packages[pluginName];
                yield this._activatePlugin(plugin);
            })));
        });
    }
    _activatePlugin(plugin) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const packageInfo = yield fs.readJSON(path.join(plugin.resolvedPath, 'package.json'));
                this.pluginRegistry[packageInfo.name] = plugin;
                if (packageInfo.main === undefined) {
                    logger.warn(`skipping plugin ${packageInfo.name} because of missing "main" field`);
                    return;
                }
                const pluginEntryModule = path.join(plugin.resolvedPath, packageInfo.main);
                plugin.module = require(pluginEntryModule);
                plugin.activateFn = plugin.module.activate;
                yield plugin.activateFn();
                windowManager.broadcastContent('plugin.activated', plugin);
            }
            catch (exception) {
                logger.warn(`Could not activate plugin ${plugin.name} due to exception: ${exception}`);
            }
        });
    }
    resolvePackages() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.resolveInProgress) {
                return;
            }
            this.resolveInProgress = true;
            const packages = yield this._getPackages();
            if (this._hasPackagesChanged(packages)) {
                logger.info(`Plugins have changed, need to restart Hub, packages: ${packages}`);
                this._updatePackageList(packages);
            }
            this.resolveInProgress = false;
        });
    }
    restart() {
        electron.app.relaunch();
        electron.app.exit(0);
    }
    remindLater() {
        setTimeout(() => {
            if (this.updateAvailable === true) {
                windowManager.broadcastContent('plugins.resolved');
            }
        }, this.updateInterval);
    }
    _getPackages() {
        return __awaiter(this, void 0, void 0, function* () {
            yield fs.ensureDir(pluginFolder);
            return yield packageCrawler.getPackages(pluginFolder);
        });
    }
    _hasPackagesChanged(newPackages) {
        if (newPackages === undefined) {
            return true;
        }
        return newPackages.includes((packageInfo) => (this._isPackageNotInList(packageInfo) || this._isPackageDifferentVersion(packageInfo)));
    }
    _isPackageNotInList(packageInfo) {
        return this.packages[packageInfo] === undefined;
    }
    _isPackageDifferentVersion(packageInfo) {
        return this.packages[packageInfo].version !== packageInfo.version;
    }
    _updatePackageList(newPackages) {
        this.packageListStorage.clear();
        if (newPackages !== undefined) {
            newPackages.forEach(pkg => {
                this.packageListStorage.addItem(pkg);
            });
        }
        this.packageListStorage.save();
        this.updateAvailable = true;
        windowManager.broadcastContent('plugins.resolved', newPackages);
    }
}
module.exports = new PluginManagerLight();
//# sourceMappingURL=pluginManagerLight.js.map