var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { fs } = require('../../../fileSystem');
const path = require('path');
const logger = require('../../../logger')('PackageCrawler');
class PackageCrawler {
    getPackages(rootPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const directories = yield this._getDirectoriesInPath(rootPath);
            const packages = yield this._getPackagesFromDirectories(directories);
            return Object.values(packages);
        });
    }
    _getDirectoriesInPath(rootPath) {
        return __awaiter(this, void 0, void 0, function* () {
            const rootPathContents = yield fs.readdir(rootPath);
            const directories = [];
            yield Promise.all(rootPathContents.map((fileName) => __awaiter(this, void 0, void 0, function* () {
                const filePath = path.join(rootPath, fileName);
                const fileStat = yield fs.stat(filePath);
                if (fileStat.isDirectory()) {
                    directories.push(filePath);
                }
            })));
            return directories;
        });
    }
    _getPackagesFromDirectories(directories) {
        return __awaiter(this, void 0, void 0, function* () {
            const packages = {};
            yield Promise.all(directories.map((directoryPath) => __awaiter(this, void 0, void 0, function* () {
                const packageData = yield this._getPackageJson(directoryPath);
                if (packageData === undefined) {
                    return;
                }
                if (packages[packageData.name] !== undefined) {
                    logger.warn(`Duplicate packages are not supported (${packageData.name}).`);
                    return;
                }
                packages[packageData.name] = {
                    packageId: packageData.name,
                    version: packageData.version,
                    resolvedPath: directoryPath
                };
            })));
            return packages;
        });
    }
    _getPackageJson(packagePath) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const packageData = yield fs.readJson(path.join(packagePath, 'package.json'));
                if (packageData === undefined || (packageData.constructor === Object && Object.entries(packageData).length === 0)) {
                    throw new Error('Package.json is empty');
                }
                if (packageData.name === undefined || packageData.version === undefined) {
                    throw new Error('Package.json is missing essential information');
                }
                return packageData;
            }
            catch (error) {
                logger.warn(`package.json could not be read at path ${packagePath}`, error.message);
            }
            return undefined;
        });
    }
}
module.exports = new PackageCrawler();
//# sourceMappingURL=packageCrawler.js.map