var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const settings = require('../localSettings/localSettings');
const pluginManager = require('./pluginManager');
const ListStorage = require('../editorManager/lib/listStorage');
const windowManager = require('../../windowManager/windowManager');
const { fs } = require('../../fileSystem');
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const upm = require('@upm/core');
chai.use(sinonChai);
chai.use(chaiAsPromised);
class FakeWatcher {
    constructor() {
        this.callbacks = new Map();
    }
    on(event, cb) {
        this.callbacks.set(event, cb);
        return this;
    }
    raiseEvent(event, arg) {
        this.callbacks.get(event)(arg);
    }
}
describe.skip('PluginManager', () => {
    let sandbox;
    let chokidarWatchStub;
    let settingsMock;
    let broadcastContentStub;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        settingsMock = {};
        sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);
        fakeWatcherInstance = new FakeWatcher();
        chokidarWatchStub = sandbox.stub(chokidar, 'watch').returns(fakeWatcherInstance);
        broadcastContentStub = windowManager.broadcastContent;
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('constructor', () => {
        it('should set the variables to default values', () => {
            expect(pluginManager.updateAvailable).equals(false);
            expect(pluginManager.resolveInProgress).equals(false);
            expect(pluginManager.pluginRegistry).to.deep.equal({});
            expect(pluginManager.packages).equals(undefined);
        });
    });
    describe('init', () => {
        beforeEach(() => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(ListStorage.prototype, 'init');
            sandbox.stub(pluginManager, 'activatePlugins');
            sandbox.stub(pluginManager, 'resolvePackages');
            sandbox.stub(fakeWatcherInstance, 'on');
            sandbox.useFakeTimers();
            yield pluginManager.init();
        }));
        it('should initialize packageListStorage', () => {
            expect(ListStorage.prototype.init).to.have.been.called;
        });
        it('should activate plugins', () => {
            expect(pluginManager.activatePlugins).to.have.been.called;
        });
        it('should resolve packages', () => {
            expect(pluginManager.resolvePackages).to.have.been.called;
        });
        it('should start watching changes to manifest.json', () => {
            expect(chokidarWatchStub).to.have.been.called;
            expect(fakeWatcherInstance.on).to.have.been.called;
        });
    });
    describe('activatePlugins', () => {
        beforeEach(() => {
            let packages = {};
            sandbox.stub(pluginManager, 'packages').get(() => packages);
        });
        it('with empty packages, should not be expected to call fs.readJSON', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(fs, 'readJSON').returns({
                name: 'pluginName'
            });
            yield pluginManager.activatePlugins();
            expect(fs.readJSON).to.not.have.been.called;
        }));
        it('with one package, should be expected to call fs.readJSON', () => __awaiter(this, void 0, void 0, function* () {
            sandbox.stub(fs, 'readJSON').returns({
                name: 'pluginName'
            });
            let packages = [
                {
                    "packageId": "com.unity.hub.sync.sketchup@0.1.0",
                    "name": "com.unity.hub.sync.sketchup",
                    "resolvedPath": "/usr/cache",
                }
            ];
            sandbox.stub(pluginManager, 'packages').get(() => packages);
            yield pluginManager.activatePlugins();
            expect(Object.keys(pluginManager.pluginRegistry).length).to.equal(1);
            expect(pluginManager.pluginRegistry['pluginName'].name).to.equal('com.unity.hub.sync.sketchup');
        }));
        it('with one package, and a main field it should activate the plugin, with exception', () => __awaiter(this, void 0, void 0, function* () {
            let packages = [
                {
                    "packageId": "com.unity.hub.sync.sketchup@0.1.0",
                    "name": "com.unity.hub.sync.sketchup",
                    "resolvedPath": "/usr/cache"
                }
            ];
            sandbox.stub(fs, 'readJSON').returns({
                'name': 'pluginName',
                'main': 'index.js'
            });
            sandbox.stub(pluginManager, 'packages').get(() => packages);
            sandbox.stub(pluginManager.logger, 'error');
            yield pluginManager.activatePlugins();
            expect(pluginManager.logger.error).to.have.been.called;
        }));
        it('with one package, and a main field it should activate the plugin, w/o exception', () => __awaiter(this, void 0, void 0, function* () {
            let packages = [
                {
                    "packageId": "com.unity.hub.sync.sketchup@0.1.0",
                    "name": "com.unity.hub.sync.sketchup",
                    "resolvedPath": "/usr/cache"
                }
            ];
            sandbox.stub(fs, 'readJSON').returns({
                'name': 'pluginName',
                'main': 'index.js'
            });
            sandbox.stub(pluginManager, 'packages').get(() => packages);
            sandbox.stub(pluginManager.logger, 'error');
            let activateStub = sandbox.stub();
            sandbox.stub(pluginManager, 'require').returns({
                activate: activateStub
            });
            yield pluginManager.activatePlugins();
            expect(pluginManager.require).to.have.been.called;
            expect(activateStub).to.have.been.called;
            expect(pluginManager.logger.error).to.not.have.been.called;
            expect(windowManager.broadcastContent).to.have.been.called;
        }));
    });
    describe('resolvePackages', () => {
        it('should not check for packages if resolve is in progress', () => __awaiter(this, void 0, void 0, function* () {
            pluginManager.resolveInProgress = true;
            sandbox.stub(pluginManager, '_getPackages').resolves();
            yield pluginManager.resolvePackages();
            expect(pluginManager._getPackages).to.not.have.been.called;
        }));
        it('should check for packages if resolve is not in progress (undefined)', () => __awaiter(this, void 0, void 0, function* () {
            let packages = [];
            sandbox.stub(pluginManager, 'packages').get(() => packages);
            pluginManager.resolveInProgress = false;
            sandbox.stub(pluginManager, '_getPackages').resolves(undefined);
            yield pluginManager.resolvePackages();
            expect(pluginManager._getPackages).to.have.been.called;
        }));
        it('should check for packages if resolve is not in progress', () => __awaiter(this, void 0, void 0, function* () {
            let packages = [];
            sandbox.stub(pluginManager, 'packages').get(() => packages);
            pluginManager.resolveInProgress = false;
            sandbox.stub(pluginManager, '_getPackages').resolves([]);
            yield pluginManager.resolvePackages();
            expect(pluginManager._getPackages).to.have.been.called;
        }));
        it('resolve is not in progress, no current packages, one new package', () => __awaiter(this, void 0, void 0, function* () {
            pluginManager.resolveInProgress = false;
            let packages = [];
            sandbox.stub(pluginManager, 'packages').get(() => packages);
            sandbox.stub(pluginManager, '_getPackages').resolves([{
                    "packageId": "com.unity.hub.sync.sketchup@0.1.0",
                    "name": "com.unity.hub.sync.sketchup",
                    "resolvedPath": "/usr/cache"
                }]);
            sandbox.stub(pluginManager.packageListStorage, 'clear');
            sandbox.stub(pluginManager.packageListStorage, 'addItem');
            sandbox.stub(pluginManager.packageListStorage, 'save');
            broadcastContentStub.resetHistory();
            yield pluginManager.resolvePackages();
            expect(pluginManager._getPackages).to.have.been.called;
            expect(pluginManager.packageListStorage.addItem).to.have.been.called;
            expect(windowManager.broadcastContent).to.have.been.called;
        }));
        it('resolve is not in progress, same current packages as resolved packages', () => __awaiter(this, void 0, void 0, function* () {
            pluginManager.resolveInProgress = false;
            packages = [{
                    "packageId": "com.unity.hub.sync.sketchup@0.1.0",
                    "name": "com.unity.hub.sync.sketchup",
                    "resolvedPath": "/usr/cache"
                }];
            sandbox.stub(pluginManager, 'packages').get(() => packages);
            sandbox.stub(pluginManager, '_getPackages').resolves(packages);
            sandbox.stub(pluginManager.packageListStorage, 'clear');
            sandbox.stub(pluginManager.packageListStorage, 'addItem');
            sandbox.stub(pluginManager.packageListStorage, 'save');
            broadcastContentStub.resetHistory();
            yield pluginManager.resolvePackages();
            expect(pluginManager._getPackages).to.have.been.called;
            expect(pluginManager.packageListStorage.addItem).to.not.have.been.called;
            expect(windowManager.broadcastContent).to.not.have.been.called;
        }));
    });
    describe('_getPackages', () => {
        it('should invoke upm.resolve to get the packages', () => __awaiter(this, void 0, void 0, function* () {
            packages = [{
                    "packageId": "com.unity.hub.sync.sketchup@0.1.0",
                    "name": "com.unity.hub.sync.sketchup",
                    "resolvedPath": "/usr/cache"
                }];
            sandbox.stub(upm, 'resolve').resolves({
                packages: packages
            });
            yield pluginManager._getPackages("toto");
            expect(upm.resolve).to.have.been.called;
        }));
    });
});
//# sourceMappingURL=pluginManager.spec.js.map