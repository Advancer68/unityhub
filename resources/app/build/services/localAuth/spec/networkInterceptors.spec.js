const axios = require('axios');
const networkInterceptors = require('../networkInterceptors');
const sinon = require('sinon');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(chaiAsPromised);
chai.use(sinonChai);
describe('NetworkInterceptors', () => {
    let sandbox;
    let responseInterceptors;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        responseInterceptors = [];
        sandbox.stub(axios.interceptors.response, 'use').callsFake((successHandler, failureHandler) => {
            responseInterceptors.push({
                success: successHandler,
                failure: failureHandler
            });
        });
    });
    afterEach(() => {
        sandbox.restore();
    });
    it('should set networkUp to true by default', () => {
        expect(networkInterceptors.networkUp).to.be.true;
    });
    describe('init', () => {
        beforeEach(() => {
            networkInterceptors.init();
        });
        it('should bind a response interceptor', () => {
            expect(axios.interceptors.response.use).to.have.been.called;
        });
    });
    describe('on successful request', () => {
        let response;
        let result;
        beforeEach(() => {
            response = { foo: 'bar' };
            networkInterceptors.init();
        });
        describe('by default', () => {
            beforeEach(() => {
                result = responseInterceptors[0].success(response);
            });
            it('should return the passed response', () => {
                expect(result).to.eq(response);
            });
        });
        describe('when network was not up', () => {
            let networkUpStub;
            beforeEach(() => {
                networkUpStub = sandbox.stub(networkInterceptors, 'networkUp').get(() => false);
                sandbox.stub(networkInterceptors, 'emit');
                result = responseInterceptors[0].success(response);
            });
            it('should emit right event', () => {
                expect(networkInterceptors.emit).to.have.been.calledWith('up');
            });
            it('should set networkUp to true', () => {
                networkUpStub.restore();
                expect(networkInterceptors.networkUp).to.be.true;
            });
        });
    });
    describe('on failed request', () => {
        let error;
        let result;
        beforeEach(() => {
            error = { foo: 'bar', response: { bar: 'baz' } };
            networkInterceptors.init();
        });
        describe('by default', () => {
            beforeEach(() => {
                try {
                    responseInterceptors[0].failure(error);
                }
                catch (err) {
                    result = err;
                }
            });
            it('should return the passed error', () => {
                expect(result).to.eq(error);
            });
        });
        describe('when request did not make it to the server', () => {
            beforeEach(() => {
                sandbox.stub(networkInterceptors, 'emit');
                error.response = undefined;
                try {
                    responseInterceptors[0].failure(error);
                }
                catch (err) {
                    result = err;
                }
            });
            it('should emit right event', () => {
                expect(networkInterceptors.emit).to.have.been.calledWith('down');
            });
            it('should set networkUp to false', () => {
                expect(networkInterceptors.networkUp).to.be.false;
            });
        });
    });
    describe('failedRequest', () => {
        beforeEach(() => {
            sandbox.stub(networkInterceptors, 'emit');
            networkInterceptors.failedRequest();
        });
        it('should emit the right event', () => {
            expect(networkInterceptors.emit).to.have.been.calledWith('down');
        });
        it('should set networkUp to false', () => {
            expect(networkInterceptors.networkUp).to.be.false;
        });
    });
});
//# sourceMappingURL=networkInterceptors.spec.js.map