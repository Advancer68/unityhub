var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
let settings = require('./localSettings');
const featureActivationSettings = require('./featureActivationSettings');
const globalMachineSettings = require('./globalMachineSettings');
const electronSettings = require('electron-settings');
const _ = require('lodash');
const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const proxyquire = require('proxyquire').noPreserveCache();
chai.use(sinonChai);
describe('localSettings', () => {
    let sandbox, defaultSettings, CLIArguments, mockFeatureActivationSettings, mockGlobalMachineSettings;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        defaultSettings = {
            foo: 'bar',
            baz: 'booo',
            hoho: 'haha'
        };
        CLIArguments = {};
        mockFeatureActivationSettings = {
            enableExtraFoo: true
        };
        mockGlobalMachineSettings = {
            enableExtraBar: true
        };
        sandbox.stub(featureActivationSettings, 'init').resolves();
        sandbox.stub(featureActivationSettings, 'settings').get(() => mockFeatureActivationSettings);
        sandbox.stub(globalMachineSettings, 'init').resolves();
        sandbox.stub(globalMachineSettings, 'settings').get(() => mockGlobalMachineSettings);
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('init', () => {
        let electronSettingsData;
        beforeEach(() => {
            electronSettingsData = {};
            sandbox.stub(electronSettings, 'getAll').callsFake(() => electronSettingsData);
        });
        it('should add featureActivationSettings to settings', () => {
            settings = proxyquire('./localSettings', {
                './defaultSettings.js': defaultSettings,
                'yargs': { argv: CLIArguments }
            });
            return settings.init().then(() => {
                _.forEach(mockFeatureActivationSettings, (value, key) => {
                    expect(settings.get(key)).to.equal(value);
                });
            });
        });
        it('when no custom settings or cli args are defined should set default settings', () => {
            settings = proxyquire('./localSettings', {
                './defaultSettings.js': defaultSettings,
                'yargs': { argv: CLIArguments }
            });
            return settings.init().then(() => {
                _.forEach(defaultSettings, (value, key) => {
                    expect(settings.get(key)).to.equal(value);
                });
            });
        });
        describe('when custom settings are defined', () => {
            let customSettingKey, defaultSettingsKey;
            beforeEach(() => {
                const keys = Object.keys(defaultSettings);
                customSettingKey = keys[0];
                defaultSettingsKey = keys[1];
                electronSettingsData[customSettingKey] = 'FOO';
                settings = proxyquire('./localSettings', {
                    './defaultSettings.js': defaultSettings,
                    'yargs': { argv: CLIArguments }
                });
                return settings.init();
            });
            it('should override default settings for provided values', () => {
                expect(settings.get(customSettingKey)).to.equal(electronSettingsData[customSettingKey]);
            });
            it('should not override default settings for omitted values', () => {
                expect(settings.get(defaultSettingsKey)).to.equal(defaultSettings[defaultSettingsKey]);
            });
        });
        describe('when cli args are passed', () => {
            describe('that are not among accepted params', () => {
                beforeEach(() => {
                    CLIArguments.foo = 'bar';
                    settings = proxyquire('./localSettings', {
                        './defaultSettings.js': defaultSettings,
                        'yargs': { argv: CLIArguments }
                    });
                    return settings.init();
                });
                it('should not override default settings', () => {
                    _.forEach(settings.keys, (settingKey) => {
                        expect(settings.get(settingKey)).to.equal(defaultSettings[settingKey]);
                    });
                });
            });
            describe('that are among accepted params', () => {
                it('should override default settings', () => {
                    const servicesUrlInterval = 1;
                    CLIArguments[settings.keys.SERVICES_URL_INTERVAL] = servicesUrlInterval;
                    settings = proxyquire('./localSettings', {
                        './defaultSettings.js': defaultSettings,
                        'yargs': { argv: CLIArguments }
                    });
                    return settings.init().then(() => {
                        expect(settings.get(settings.keys.SERVICES_URL_INTERVAL)).to.equal(servicesUrlInterval);
                    });
                });
            });
        });
        describe('when global machine settings are defined', () => {
            describe('with custom properties', () => {
                beforeEach(() => __awaiter(this, void 0, void 0, function* () {
                    settings = proxyquire('./localSettings', {
                        './defaultSettings.js': defaultSettings,
                        'yargs': { argv: CLIArguments }
                    });
                    yield settings.init();
                }));
                it('should not override default settings', () => {
                    _.forEach(settings.keys, (settingKey) => {
                        expect(settings.get(settingKey)).to.equal(defaultSettings[settingKey]);
                    });
                });
                it('should contain custom global settings', () => {
                    _.forOwn(mockGlobalMachineSettings, (settingsValue, settingKey) => {
                        expect(settings.get(settingKey)).to.equal(settingsValue);
                    });
                });
            });
            describe('with override properties', () => {
                const serviceConfigBaseUrl = 'https://myconfig.com';
                beforeEach(() => __awaiter(this, void 0, void 0, function* () {
                    mockGlobalMachineSettings[settings.keys.SERVICE_CONFIG_BASE_URL] = serviceConfigBaseUrl;
                    settings = proxyquire('./localSettings', {
                        './defaultSettings.js': defaultSettings,
                        'yargs': { argv: CLIArguments }
                    });
                    yield settings.init();
                }));
                it('should override default settings', () => {
                    expect(settings.get(settings.keys.SERVICE_CONFIG_BASE_URL)).to.equal(serviceConfigBaseUrl);
                });
                it('should not override other default settings', () => {
                    _.forEach(settings.keys, (settingKey) => {
                        if (settingKey !== settings.keys.SERVICE_CONFIG_BASE_URL) {
                            expect(settings.get(settingKey)).to.equal(defaultSettings[settingKey]);
                        }
                    });
                });
            });
        });
        describe('when both custom settings and cli args are defined', () => {
            it('should take cli argument over custom and default settings', () => {
                const key = settings.keys.SERVICES_URL_INTERVAL;
                CLIArguments[key] = 1;
                electronSettingsData[key] = 2;
                defaultSettings[key] = 3;
                settings = proxyquire('./localSettings', {
                    './defaultSettings.js': defaultSettings,
                    'yargs': { argv: CLIArguments }
                });
                return settings.init().then(() => {
                    expect(settings.get(key)).to.equal(CLIArguments[key]);
                });
            });
        });
        describe('when both global machine settings and cli args are defined', () => {
            it('should take cli argument over global machine and default settings', () => {
                const key = settings.keys.SERVICES_URL_INTERVAL;
                CLIArguments[key] = 1;
                mockGlobalMachineSettings[key] = 2;
                defaultSettings[key] = 3;
                settings = proxyquire('./localSettings', {
                    './defaultSettings.js': defaultSettings,
                    'yargs': { argv: CLIArguments }
                });
                return settings.init().then(() => {
                    expect(settings.get(key)).to.equal(CLIArguments[key]);
                });
            });
        });
        describe('when both custom settings and global machine settings are defined', () => {
            it('should take custom settings over global machine and default settings', () => {
                const key = settings.keys.SERVICES_URL_INTERVAL;
                electronSettingsData[key] = 1;
                mockGlobalMachineSettings[key] = 2;
                defaultSettings[key] = 3;
                settings = proxyquire('./localSettings', {
                    './defaultSettings.js': defaultSettings,
                    'yargs': { argv: CLIArguments }
                });
                return settings.init().then(() => {
                    expect(settings.get(key)).to.equal(electronSettingsData[key]);
                });
            });
        });
        describe('when custom settings, cli args and global machine settings are defined', () => {
            it('should take cli argument over custom, global machine and default settings', () => {
                const key = settings.keys.SERVICES_URL_INTERVAL;
                CLIArguments[key] = 1;
                electronSettingsData[key] = 2;
                mockGlobalMachineSettings[key] = 3;
                defaultSettings[key] = 4;
                settings = proxyquire('./localSettings', {
                    './defaultSettings.js': defaultSettings,
                    'yargs': { argv: CLIArguments }
                });
                return settings.init().then(() => {
                    expect(settings.get(key)).to.equal(CLIArguments[key]);
                });
            });
        });
    });
    describe('', () => {
        beforeEach(() => {
            settings = proxyquire('./localSettings', {
                './defaultSettings.js': defaultSettings,
                'yargs': { argv: CLIArguments }
            });
        });
        describe('get', () => {
            it('should return value associated with key', () => {
                sandbox.stub(electronSettings, 'getAll').returns({});
                return settings.init().then(() => {
                    let key = settings.keys.CLOUD_ENVIRONMENT;
                    expect(settings.get(key)).to.equal(defaultSettings[key]);
                });
            });
        });
        describe('set', () => {
            let key, value;
            beforeEach(() => {
                sandbox.stub(electronSettings, 'getAll').returns({});
                key = 'foo';
                value = 'bar';
                return settings.init();
            });
            it('should set setting value to key', () => {
                return settings.set(key, value).then(() => {
                    expect(settings.get(key)).to.equal(value);
                });
            });
            it('when persisted is true will persist setting to electron-settings', () => {
                sandbox.stub(electronSettings, 'set');
                return settings.set(key, value, true).then(() => {
                    expect(settings.get(key)).to.equal(value);
                    expect(electronSettings.set).to.have.been.calledWith(key, value);
                });
            });
        });
    });
});
//# sourceMappingURL=localSettings.spec.js.map