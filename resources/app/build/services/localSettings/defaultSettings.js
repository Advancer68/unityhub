module.exports = {
    cloudEnvironment: 'production',
    accessTokenIntervalPeriod: 5000,
    servicesUrlInterval: 900000,
    hubConfigInterval: 3600000,
    releasesRefreshInterval: 3600000,
    recentProjectInterval: 60000,
    downloadClusterInterval: 1000,
    downloadEditorInterval: 1000,
    refreshTokenValidity: 2592000000,
    checkForUpdateInterval: 43200000,
    sendAnalyticsEventsInterval: 60000,
    maxSizeForAnalyticsQueue: 500,
    downloadRequestTimeout: 30000,
};
//# sourceMappingURL=defaultSettings.js.map