const { fs } = require('../../fileSystem');
const path = require('path');
const util = require('util');
const logger = require('../../logger')('GlobalMachineSettings');
const CONFIG_FILE_NAME = 'services-config.json';
class GlobalMachineSettings {
    constructor() {
        this._settings = {};
        this.keys = {
            LICENSING_SERVICE_BASE_URL: 'licensingServiceBaseUrl'
        };
    }
    init() {
        const globalConfigFolder = this.getConfigFolder();
        this._settings.globalConfigFolder = globalConfigFolder;
        const configPath = path.join(globalConfigFolder, CONFIG_FILE_NAME);
        return fs.readJson(configPath, { throws: false })
            .then((storedSettings) => {
            logger.info(`Configurations from services-config ${util.inspect(storedSettings)}`);
            this._settings = Object.assign({}, this._settings, storedSettings);
        }).catch(error => {
            if (error.code === 'ENOENT') {
                logger.debug(`Global config file not found: ${configPath}`);
            }
            else {
                logger.error(`An error occurred while loading the global config file (${configPath}): ${error}`);
            }
        });
    }
    get settings() {
        return this._settings;
    }
    getConfigFolder() {
        if (/^win/.test(process.platform)) {
            return path.join(process.env.ALLUSERSPROFILE, 'Unity', 'config');
        }
        if (process.platform === 'darwin') {
            return path.join('/', 'Library', 'Application Support', 'Unity', 'config');
        }
        if (process.platform === 'linux') {
            return path.join('/', 'usr', 'share', 'unity3d', 'config');
        }
        throw new Error(`Unable to resolve config folder for platform: ${process.platform}`);
    }
}
module.exports = new GlobalMachineSettings();
//# sourceMappingURL=globalMachineSettings.js.map