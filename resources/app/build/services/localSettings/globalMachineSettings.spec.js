var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const globalMachineSettings = require('./globalMachineSettings');
const { fs } = require('../../fileSystem');
const path = require('path');
const expect = require('chai').expect;
const sinon = require('sinon');
describe('globalMachineSettings', () => {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('getConfigFolder function', () => {
        describe('on windows', () => {
            beforeEach(() => {
                sandbox.stub(process, 'platform').value('win32');
                sandbox.stub(process, 'env').value({ ALLUSERSPROFILE: path.join('C:', 'ProgramData'), });
            });
            it('should return the library folder path', () => {
                expect(globalMachineSettings.getConfigFolder()).to.equal(path.join('C:', 'ProgramData', 'Unity', 'config'));
            });
        });
        describe('on linux', () => {
            beforeEach(() => {
                sandbox.stub(process, 'platform').value('linux');
                sandbox.stub(process, 'env').value({ HOME: 'home', });
            });
            it('should return the library folder for $HOME path', () => {
                expect(globalMachineSettings.getConfigFolder()).to.equal(path.join('/', 'usr', 'share', 'unity3d', 'config'));
            });
        });
        describe('on darwin', () => {
            beforeEach(() => {
                sandbox.stub(process, 'platform').value('darwin');
            });
            it('should return the library folder path', () => {
                expect(globalMachineSettings.getConfigFolder()).to.equal(path.join('/', 'Library', 'Application Support', 'Unity', 'config'));
            });
        });
        describe('on unknown platform', () => {
            const platform = 'unknown';
            beforeEach(() => {
                sandbox.stub(process, 'platform').value(platform);
            });
            it('should throw an error', () => {
                expect(() => globalMachineSettings.getConfigFolder()).to.throw(Error, `Unable to resolve config folder for platform: ${platform}`);
            });
        });
    });
    describe('init function', () => {
        let tmpPath, fsMock;
        beforeEach(() => {
            tmpPath = 'foo';
            fsMock = {};
            sandbox.stub(globalMachineSettings, 'getConfigFolder').returns(tmpPath);
            sandbox.stub(fs, 'readJson').callsFake(jsonPath => new Promise((resolve, reject) => {
                const config = fsMock[jsonPath];
                if (config.code) {
                    return reject(config);
                }
                return resolve(config);
            }));
        });
        function saveConfig(config) {
            const filePath = path.join(tmpPath, 'services-config.json');
            fsMock[filePath] = config;
        }
        describe('when config file not present', () => {
            it('should only have globalConfigFolder setting defined', () => __awaiter(this, void 0, void 0, function* () {
                yield globalMachineSettings.init();
                expect(globalMachineSettings.settings).to.deep.equal({ globalConfigFolder: tmpPath });
            }));
        });
        describe('when config file is malformed json', () => {
            it('should only have globalConfigFolder setting defined', () => __awaiter(this, void 0, void 0, function* () {
                saveConfig({ code: 'INVALID' });
                yield globalMachineSettings.init();
                expect(globalMachineSettings.settings).to.deep.equal({ globalConfigFolder: tmpPath });
            }));
        });
        describe('when config file is empty json', () => {
            it('should only have globalConfigFolder setting defined', () => __awaiter(this, void 0, void 0, function* () {
                saveConfig({ code: 'ENOENT' });
                yield globalMachineSettings.init();
                expect(globalMachineSettings.settings).to.deep.equal({ globalConfigFolder: tmpPath });
            }));
        });
        describe('when config file has custom property', () => {
            beforeEach(() => {
                saveConfig({ custom: 'property' });
            });
            it('should contain the custom config property', () => __awaiter(this, void 0, void 0, function* () {
                yield globalMachineSettings.init();
                expect(globalMachineSettings.settings['custom']).to.equal('property');
            }));
        });
    });
});
//# sourceMappingURL=globalMachineSettings.spec.js.map