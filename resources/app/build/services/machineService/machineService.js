const os = require('os');
const Buffer = require('buffer').Buffer;
const logger = require('../../logger')('MachineService');
const editorLicense = require('@unityhub/unity-editor-license');
function hwID(preferUUID) {
    if (os.platform() === 'linux' || preferUUID) {
        return editorLicense.getUuid();
    }
    return editorLicense.getSerial();
}
function formatMAC(mac) {
    const arr = mac.toLowerCase().split('');
    for (let i = (arr.length / 2) - 1; i > 0; i--) {
        arr[i * 2] = `:${arr[i * 2]}`;
    }
    return arr.join('');
}
function getMAC() {
    return formatMAC(editorLicense.getMacAddress().replace(/[-:]/g, ''));
}
function hasMac(macAddr) {
    return Promise.resolve(editorLicense.macAddressExists(macAddr));
}
function getWinProductId() {
    const Registry = require('@unityhub/unity-editor-registry');
    return new Promise((resolve, reject) => {
        const productId = Registry.getString('HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\', 'ProductId', '');
        if (productId !== '') {
            resolve(productId);
        }
        else {
            reject('Key Not Found');
        }
    });
}
function getWinBIOS() {
    return new Promise((resolve) => {
        const biosId = editorLicense.getBiosId();
        resolve(biosId);
    });
}
function getHDDId() {
    return new Promise((resolve) => {
        const hddid = editorLicense.getHddId();
        if (hddid != null) {
            resolve(hddid);
        }
        else {
            resolve();
        }
    });
}
switch (os.platform()) {
    case 'darwin':
        module.exports = {
            getMachineBinding1: hwID.bind(this, true),
            getMachineBinding2: hwID.bind(this, false),
            getMachineBinding5: getMAC,
            getAllBindings() {
                return Promise.all([hwID(true), hwID(), getMAC()]).then((IDs) => ({ 1: IDs[0], 2: IDs[1], 5: IDs[2] }));
            }
        };
        break;
    case 'win32':
        module.exports = {
            getMachineBinding1: getWinProductId,
            getMachineBinding2: getHDDId,
            getMachineBinding4: getWinBIOS,
            getMachineBinding5: getMAC,
            getAllBindings() {
                return Promise.all([getWinProductId(), getHDDId(), getWinBIOS(), getMAC()])
                    .then((IDs) => ({ 1: IDs[0], 2: IDs[1], 4: Buffer.from(IDs[2]).toString('base64'), 5: IDs[3] }));
            },
            hasMac
        };
        break;
    case 'linux':
        module.exports = {
            getMachineBinding1: hwID.bind(this),
            getMachineBinding2: hwID.bind(this),
            getAllBindings() {
                return Promise.all([hwID(), hwID()])
                    .then((IDs) => ({ 1: IDs[0], 2: IDs[1] }));
            }
        };
        break;
    default:
        logger.warn('unsupported platform');
}
//# sourceMappingURL=machineService.js.map