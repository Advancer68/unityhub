const HubWebSocketService = require('../hubWebSocketService');
const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const http = require('http');
chai.use(sinonChai);
describe('HubWebSocketService', function () {
    let ws, sandbox, server;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        ws = new HubWebSocketService();
        server = http.createServer(function (request, response) {
        });
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('constructor', function () {
        it('should have an empty number of connections', function () {
            expect(ws.connections).to.not.be.undefined;
            expect(ws.connections.size).to.be.zero;
        });
    });
    describe('start', function () {
        it('exception expected if connect is not called first', function () {
            expect(ws.start).to.throw(Error);
        });
        it('on("connection") handler should be defined', function () {
            ws.connect(server);
            ws.start();
            expect(ws.connections).to.not.be.undefined;
            expect(ws.server.on).to.not.be.undefined;
        });
    });
    describe('stop', function () {
        it('unsubscribe all channels and clear subscriptions', function () {
            ws.connect(server);
            ws.start();
            ws.stop();
            expect(ws.connections.size).to.be.equal(0);
        });
    });
});
//# sourceMappingURL=hubWebSocketService.ispec.js.map