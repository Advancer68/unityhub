'use strict';
const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');
const electron = require('electron');
const projectEvents = require('../api/events/project');
const app = electron.app;
const expect = chai.expect;
chai.use(sinonChai);
describe('Project Events', () => {
    let sendSpy = sinon.spy();
    let connectionsMock = {
        get: sinon.stub().returns({ send: sendSpy })
    };
    describe('on open', () => {
        let data = { info: 'fake data' };
        let expectedPacket = {
            channel: 'app',
            topic: 'project.opened',
            data: JSON.stringify(data)
        };
        it('should call connections.get with the session id & send the data', () => {
            app.editors = { current: { session_id: 'fake-id' } };
            projectEvents['project.opened'](connectionsMock, data);
            expect(connectionsMock.get).to.have.been.calledWith('fake-id');
            expect(sendSpy).to.have.been.calledWith(JSON.stringify(expectedPacket));
        });
    });
    describe('on create', () => {
        let data = { info: 'fake data' };
        let expectedPacket = {
            channel: 'app',
            topic: 'project.create',
            data: JSON.stringify(data)
        };
        it('should call connections.get with the session id & send the data', () => {
            app.editors = { current: { session_id: 'fake-id' } };
            projectEvents['project.create'](connectionsMock, data);
            expect(connectionsMock.get).to.have.been.calledWith('fake-id');
            expect(sendSpy).to.have.been.calledWith(JSON.stringify(expectedPacket));
        });
    });
});
//# sourceMappingURL=project.event.ispec.js.map