'use strict';
const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;
const utils = require('./utils');
const localProject = require('../../localProject/localProject');
const HubRestService = require('../hubRestService');
describe.skip('Local Project REST Controller', function () {
    let sandbox;
    const restService = new HubRestService();
    const server = restService.server;
    const fakeProjectsData = [
        {
            path: '/Users/stella/code/cool-project',
            containingFolderPath: '/Users/stella/code',
            version: '5.4.0b14',
            title: 'cool project',
            cloudProjectId: '82aac34b-81a6-420c-9a1d-1341d910808b',
            resultorganizationId: 'exampleorg',
            cloudEnabled: true
        },
        {
            path: '/Users/stella/code/another-one',
            containingFolderPath: '/Users/stella/code',
            version: '5.5.0f3',
            title: 'another one',
            cloudEnabled: false
        }
    ];
    before(() => {
        restService.start();
        sandbox = sinon.sandbox.create();
        sandbox.stub(localProject, 'getProjectData', () => fakeProjectsData[0]);
        sandbox.stub(localProject, 'getRecentProjects', () => Promise.resolve(fakeProjectsData));
        sandbox.stub(localProject, 'openProject', () => Promise.resolve('opened'));
    });
    after(() => {
        restService.stop();
        sandbox.restore();
    });
    describe('routes', () => {
        const testSuccess = utils.testRouteSuccess;
        describe('/projects/local/recent', () => {
            const testRoute = testSuccess.bind(this, server, '/projects/local/recent');
            it('GET should call getRecentProjects & return 200 OK', (done) => {
                testRoute('get', 200, fakeProjectsData, localProject.getRecentProjects, done);
            });
        });
        describe('/projects/local/info', () => {
            const projectPath = '/Users/stella/code/unity-project';
            const query = `?path=${projectPath}`;
            const testRoute = testSuccess.bind(this, server, `/projects/local/info${query}`);
            it('GET should call getProjectData with the path parameter & return 201 Created', (done) => {
                testRoute('get', 200, fakeProjectsData[0], localProject.getProjectData, () => {
                    expect(localProject.getProjectData).to.have.been.calledWith(projectPath);
                    done();
                });
            });
        });
    });
});
//# sourceMappingURL=localProject.controller.ispec.js.map