const HubEventRouter = require('../hubEventRouter');
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const postal = require('postal');
chai.use(sinonChai);
describe('HubEventRouter', function () {
    let router, sandbox;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        router = new HubEventRouter();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('constructor', function () {
        it('should subscribe to all defined routes in the event folder', function () {
            expect(router.subscriptions.size).to.be.above(0);
            expect(router.subscriptions.entries().next().value[1].channel).to.be.equal('app');
            expect(router.server).to.be.undefined;
        });
    });
    describe('start', function () {
        it('subscriptions are postal objects', function () {
            sandbox.spy(postal, 'subscribe');
            router.start();
            expect(router.subscriptions.size).to.be.above(0);
            expect(postal.subscribe).to.have.been.called;
        });
    });
    describe('stop', function () {
        it('unsubscribe all channels and clear subscriptions', function () {
            router.start();
            router.stop();
            expect(router.subscriptions.size).to.be.equal(0);
        });
    });
    describe('connect', function () {
        it('set server', function () {
            let httpServer = {};
            router.connect(httpServer);
            expect(router.server).to.be.equal(httpServer);
        });
    });
});
//# sourceMappingURL=hubEventRouter.ispec.js.map