'use strict';
const sinon = require('sinon');
const utils = require('./utils');
const UnityAuth = require('../../localAuth/auth');
const HubRestService = require('../hubRestService');
describe.skip('Auth REST Controller', () => {
    let restService = new HubRestService();
    before(() => {
        restService.start();
    });
    after(() => {
        restService.stop();
    });
    describe('routes', () => {
        let sandbox;
        beforeEach(() => {
            sandbox = sinon.sandbox.create();
        });
        afterEach(() => {
            sandbox.restore();
        });
        describe('/auth/user-info', () => {
            let fakeUserInfo;
            beforeEach(() => {
                fakeUserInfo = JSON.stringify({
                    accessToken: 'abc',
                    displayName: 'Han Solo',
                    organizationForeignKeys: 'rebellion',
                    primaryOrg: 'rebellion',
                    userId: 'hsolo',
                    name: 'Solo',
                    valid: false,
                    whitelisted: true
                });
                sandbox.stub(UnityAuth, 'getUserInfo', () => {
                    return Promise.resolve(fakeUserInfo);
                });
            });
            it('GET should call getUserInfo and return 200 OK', (done) => {
                utils.testRouteSuccess(restService.server, '/auth/user-info', 'get', 200, JSON.parse(fakeUserInfo), UnityAuth.getUserInfo, done);
            });
        });
        describe('/auth/connect-info', () => {
            let fakeConnectInfo;
            beforeEach(() => {
                fakeConnectInfo = JSON.stringify({
                    error: false,
                    initialized: false,
                    loggedIn: false,
                    maintenance: false,
                    online: false,
                    ready: false,
                    showLoginWindow: false,
                    workOffline: false
                });
                sandbox.stub(UnityAuth, 'getConnectInfo', () => {
                    return Promise.resolve(fakeConnectInfo);
                });
            });
            it('GET should call getConnectInfo and return 200 OK', (done) => {
                utils.testRouteSuccess(restService.server, '/auth/connect-info', 'get', 200, JSON.parse(fakeConnectInfo), UnityAuth.getConnectInfo, done);
            });
        });
        describe('/auth/logout', () => {
            beforeEach(() => {
                sandbox.stub(UnityAuth, 'logout');
            });
            it('POST should call logout and return 200 OK', (done) => {
                utils.testRouteSuccess(restService.server, '/auth/logout', 'post', 200, '', UnityAuth.logout, done);
            });
        });
    });
});
//# sourceMappingURL=auth.controller.ispec.js.map