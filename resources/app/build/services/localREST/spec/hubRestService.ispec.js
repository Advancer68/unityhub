'use strict';
const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');
const path = require('path');
const SwaggerConnect = require('swagger-connect');
const HubRestService = require('../hubRestService');
const HubWebSocketService = require('../hubWebSocketService');
const HubEventRouter = require('../hubEventRouter');
const expect = chai.expect;
chai.use(sinonChai);
describe('Hub Rest Service', () => {
    let sandbox, service, connect;
    before(() => {
        sandbox = sinon.sandbox.create();
        connect = { register: sandbox.spy() };
        sandbox.stub(SwaggerConnect, 'create').callsFake((config, callback) => {
            callback(null, connect);
        });
        ['connect', 'start', 'stop'].forEach((methodName) => {
            sandbox.spy(HubWebSocketService.prototype, methodName);
            sandbox.spy(HubEventRouter.prototype, methodName);
        });
        service = new HubRestService;
    });
    describe('constructor', () => {
        it('should set the service port from env var or default', () => {
            expect(service.port).to.equal(process.env.PORT || 39000);
        });
    });
    describe('start()', () => {
        let evStartCount;
        before(() => {
            evStartCount = HubEventRouter.prototype.start.callCount;
            service.start();
        });
        it('should create a swagger service from the config', () => {
            let config = { appRoot: `${__dirname.replace(`${path.sep}spec`, '')}` };
            expect(SwaggerConnect.create.firstCall.args[0]).to.eql(config);
        });
        it('should register the app via swaggerConnect', () => {
            expect(connect.register).to.have.been.calledWith(service.server);
        });
        it('should set this.httpserver', () => {
            expect(service.httpserver).to.exist;
        });
        it('should startup the websocket server & assign it to this.wsServer', () => {
            expect(service.wsServer.connect).to.have.been.calledWith(service.httpserver);
            expect(service.wsServer.start).to.have.been.calledOnce;
        });
        it('should startup the event router & assign it to this.evRouter', () => {
            expect(service.evRouter.connect).to.have.been.calledWith(service.wsServer);
            expect(service.evRouter.start.callCount).to.equal(evStartCount + 1);
        });
    });
    describe('originIsAllowed()', () => {
        it('should determine if the origin is allowed (STUB)', () => {
            expect(service.originIsAllowed()).to.equal(true);
        });
    });
    describe('stop()', () => {
        let evStopCount;
        before(() => {
            evStopCount = service.evRouter.stop.callCount;
            sandbox.spy(service.httpserver, 'close');
            service.stop();
        });
        it('should .close() the http server', () => {
            expect(service.httpserver.close).to.have.been.calledOnce;
        });
        it('should .stop() the websocket server', () => {
            expect(service.wsServer.stop).to.have.been.calledOnce;
        });
        it('should .stop() the event router', () => {
            expect(service.evRouter.stop.callCount).to.equal(evStopCount + 1);
        });
    });
    after(() => {
        service.stop();
        sandbox.restore();
    });
});
//# sourceMappingURL=hubRestService.ispec.js.map