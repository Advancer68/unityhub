'use strict';
const http = require('http');
const HubWebSocketService = require('./hubWebSocketService');
const HubEventRouter = require('./hubEventRouter');
const requireAll = require('require-all');
const path = require('path');
const logger = require('../../logger')('RestServer');
var port = process.env.PORT || 39000;
class HubRestService {
    constructor() {
        this.port = port;
        this.wsServer = new HubWebSocketService();
        this.evRouter = new HubEventRouter();
        this.routes = {
            '/window/show': { POST: 'show' },
            '/window/hide': { POST: 'hide' },
            '/auth/user-info': { GET: 'getUserInfo' },
            '/auth/logout': { POST: 'logout' },
            '/auth/connect-info': { GET: 'getConnectInfo' },
            '/license': { POST: 'activate', PUT: 'refresh', DELETE: 'return' },
            '/license/info': { GET: 'getInfo' },
            '/license/file': { GET: 'load', POST: 'save' },
            '/license/errors': { DELETE: 'clearErrors' },
            '/projects/local/info': { GET: 'getProjectData' },
            '/projects/local/recent': { GET: 'getRecent' },
            '/projects/local/open': { POST: 'openProject' },
            '/config/urls/default': { GET: 'getDefaultUrls' },
            '/config/urls': { GET: 'getUrls' },
            '/health': { GET: 'healthCheck' }
        };
        this.ctrls = new Map();
        requireAll({
            dirname: path.join(__dirname, '/api/controllers'),
            filter: /((?!spec)|.*)\.js$/,
            resolve: (routes) => {
                logger.debug('resolve routes: ', routes);
                for (var routePath of Object.keys(routes)) {
                    this.ctrls.set(routePath, routes[routePath]);
                }
            }
        });
    }
    _returnErrorResponse(res) {
        res.setHeader('Access-Control-Allow-Origin', 'localhost');
        res.statusCode = 401;
        res.end('Unauthorized access');
    }
    _isSameOriginPolicy(req) {
        if (!req.headers) {
            return false;
        }
        if (req.headers.referer) {
            try {
                const refererParsed = new URL(req.headers.referer);
                if (refererParsed.host !== `localhost:${port}`) {
                    return false;
                }
            }
            catch (err) {
                return false;
            }
        }
        if (req.headers.host !== `localhost:${port}`) {
            return false;
        }
        if (req.connection && req.connection.remoteAddress !== req.connection.localAddress) {
            return false;
        }
        return true;
    }
    start() {
        logger.info('Init');
        this.evRouter.connect(this.wsServer);
        this.evRouter.start();
        this.httpserver = http.createServer((req, res) => {
            const route = this.routes[req.url.replace(/\/+$/, '')] || {};
            const methodName = route[req.method];
            const method = this.ctrls.get(methodName);
            if (this._isSameOriginPolicy(req) && route && methodName && method) {
                let body = [];
                const contentType = req.headers['content-type'];
                req.on('error', (err) => {
                    logger.debug('Error on request: ', err);
                    this._returnErrorResponse(res);
                }).on('data', (chunk) => {
                    body.push(chunk);
                }).on('end', () => {
                    body = Buffer.concat(body).toString();
                    if (contentType === 'application/json' && body !== '') {
                        req.body = JSON.parse(body);
                    }
                    else {
                        req.body = body;
                    }
                    try {
                        method(req, res);
                    }
                    catch (err) {
                        logger.debug('Error on method: ', err);
                        this._returnErrorResponse(res);
                    }
                });
                return;
            }
            this._returnErrorResponse(res);
        });
        this.httpserver.listen(port);
        this.wsServer.connect(this.httpserver);
        this.wsServer.start();
    }
    stop() {
        this.evRouter.stop();
        this.wsServer.stop();
        if (this.httpserver !== undefined) {
            this.httpserver.close();
        }
    }
}
module.exports = HubRestService;
//# sourceMappingURL=hubRestService.js.map