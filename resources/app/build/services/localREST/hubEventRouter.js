'use strict';
const postal = require('postal');
const logger = require('../../logger')('RestEventRouter');
const requireAll = require('require-all');
const path = require('path');
class HubEventRouter {
    constructor() {
        this.subscriptions = new Map();
        var self = this;
        requireAll({
            dirname: path.join(__dirname, '/api/events'),
            filter: /((?!spec)|.*)\.js$/,
            resolve: (routes) => {
                logger.debug('resolve routes: ', routes);
                for (var routePath of Object.keys(routes)) {
                    self.addRoute(routePath, routes[routePath]);
                }
            }
        });
    }
    start() {
        for (var [name, subscriptionInfo] of this.subscriptions) {
            logger.info(`subscribing to: ${name}`);
            var subscription = postal.subscribe(subscriptionInfo);
            this.subscriptions.set(name, subscription);
        }
    }
    stop() {
        for (var [name, subscription] of this.subscriptions) {
            logger.info(`unsubscribing from: ${name}`);
            if (subscription.unsubscribe !== undefined) {
                subscription.unsubscribe();
            }
        }
        this.subscriptions.clear();
    }
    connect(server) {
        this.server = server;
    }
    addRoute(routePath, handler) {
        logger.debug(`adding route path: ${routePath}`);
        this.subscriptions.set(routePath, {
            channel: 'app',
            topic: routePath,
            callback: (data) => {
                if (this.server && this.server.connections) {
                    handler(this.server.connections, data);
                }
            }
        });
    }
}
module.exports = HubEventRouter;
//# sourceMappingURL=hubEventRouter.js.map