'use strict';
const WebSocketServer = require('ws').Server;
const logger = require('../../logger')('WebSocketService');
class InvalidConnectionException extends Error {
    constructor() {
        super();
        this.message = 'Please connect to this websocket server prior to starting';
    }
    toString() {
        return `InvalidConnectionException: ${this.message}`;
    }
}
class HubWebSocketService {
    constructor() {
        this.connections = new Map();
    }
    connect(httpServer) {
        this.server = new WebSocketServer({
            server: httpServer
        });
    }
    start() {
        logger.info('starting listening for request');
        if (this.server === undefined) {
            throw new InvalidConnectionException();
        }
        this.server.on('connection', (ws) => {
            logger.info(`incoming connection with session_id ${ws.upgradeReq.headers.session_id}`);
            this.connections.set(ws.upgradeReq.headers.session_id, ws);
        });
    }
    stop() {
        logger.info('stopping web socket server. closing connections');
        for (var [sessionId, connection] of this.connections) {
            logger.info(`closing connection: [ ${sessionId}], ${connection.protocol}`);
            connection.close();
        }
        this.connections.clear();
    }
}
module.exports = HubWebSocketService;
//# sourceMappingURL=hubWebSocketService.js.map