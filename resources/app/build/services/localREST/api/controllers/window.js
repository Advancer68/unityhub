'use strict';
const electron = require('electron');
const postal = require('postal');
const processInfo = require('process');
const os = require('os');
const windowManager = require('../../../../windowManager/windowManager');
const app = electron.app;
function show(req, res) {
    res.setHeader('Access-Control-Allow-Origin', 'localhost');
    if (req.body.url === undefined ||
        req.body.modal === undefined ||
        req.body.sessionId === undefined) {
        res.statusCode = 400;
        res.end();
        return;
    }
    const fragment = req.body.url;
    if (fragment.indexOf('login') !== -1) {
        windowManager.showSignInWindow();
    }
    else if (fragment.indexOf('new') !== -1) {
        const editor = { version: req.headers['x-unity-version'] };
        windowManager.showNewProjectWindow(editor);
    }
    else {
        windowManager.mainWindow.show();
        windowManager.mainWindow.loadFragmentURL(fragment);
        postal.publish({
            channel: 'app',
            topic: 'main-window.change',
            data: {
                state: 'open',
                show: true,
                from: 'windowController:show',
                url: req.body.url
            }
        });
    }
    res.setHeader('Content-Type', 'application/json');
    if (os.platform() === 'win32') {
        const handle = windowManager.mainWindow.getNativeWindowHandle();
        const lHandle = handle.readUIntLE(0, handle.length);
        const aSessionId = processInfo.pid;
        res.end(JSON.stringify({ handle: lHandle, sessionId: aSessionId }));
    }
    else {
        res.end(JSON.stringify({ sessionId: processInfo.pid }));
    }
    res.end();
}
function hide(req, res) {
    res.setHeader('Access-Control-Allow-Origin', 'localhost');
    app.editors.current = null;
    windowManager.mainWindow.hide();
    res.end();
}
module.exports = {
    show,
    hide
};
//# sourceMappingURL=window.js.map