'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const licenseClient = require('../../../licenseService/licenseClientProxy');
module.exports = {
    getInfo(req, res) {
        licenseClient.getLicenseInfo((err, data) => {
            res.setHeader('Access-Control-Allow-Origin', 'localhost');
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(data));
        });
    },
    activate(req, res) {
        licenseClient.activateNewLicense((err, succeeded) => {
            res.setHeader('Access-Control-Allow-Origin', 'localhost');
            res.setHeader('Content-Type', 'application/json');
            res.statusCode = 201;
            res.end(JSON.stringify(succeeded));
        });
    },
    return(req, res) {
        licenseClient.returnLicense(() => {
            res.setHeader('Access-Control-Allow-Origin', 'localhost');
            res.statusCode = 204;
            res.end();
        });
    },
    refresh(req, res) {
        licenseClient.refreshLicense((err, succeeded) => {
            res.setHeader('Access-Control-Allow-Origin', 'localhost');
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(succeeded));
        });
    },
    load(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { succeeded } = yield licenseClient.loadLicenseLegacy();
            res.setHeader('Access-Control-Allow-Origin', 'localhost');
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(succeeded));
        });
    },
    save(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { succeeded } = yield licenseClient.saveLicense();
            res.setHeader('Access-Control-Allow-Origin', 'localhost');
            res.setHeader('Content-Type', 'application/json');
            res.statusCode = 201;
            res.end(JSON.stringify(succeeded));
        });
    },
    clearErrors(req, res) {
        licenseClient.clearErrors(() => {
            res.setHeader('Access-Control-Allow-Origin', 'localhost');
            res.statusCode = 204;
            res.end();
        });
    }
};
//# sourceMappingURL=license.js.map