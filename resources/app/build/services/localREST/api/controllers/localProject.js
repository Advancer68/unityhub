'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const localProject = require('../../../localProject/localProject');
module.exports = {
    getRecent(req, res) {
        res.setHeader('Access-Control-Allow-Origin', 'localhost');
        localProject.getRecentProjects()
            .then((projects) => {
            res.end(JSON.stringify(projects));
        });
    },
    getProjectData(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const data = yield localProject.getProjectData(req.query.path);
            res.setHeader('Access-Control-Allow-Origin', 'localhost');
            res.setHeader('Content-Type', 'application/json');
            res.json(data);
        });
    },
    openProject(req, res) {
        res.setHeader('Access-Control-Allow-Origin', 'localhost');
        if (req.body.path) {
            localProject.openProject(req.body.path, req.body.unityVersion, {});
        }
        else {
            localProject.showOpenOtherDialog();
        }
        res.end();
    }
};
//# sourceMappingURL=localProject.js.map