const electron = require('electron');
const logger = require('../../../../logger')('RestEvent:Window');
const app = electron.app;
function onWindowChange(connections, data) {
    logger.debug('onWindowChange data: ', data, 'connections:', connections);
    if (app.editors.current) {
        const connection = connections.get(app.editors.current.session_id);
        const packet = {
            channel: 'app',
            topic: 'window.change',
            data: JSON.stringify(data)
        };
        connection.send(JSON.stringify(packet));
        if (data.state && data.state === 'all-closed') {
            app.editors.current = null;
        }
    }
}
module.exports = {
    'main-window.change': onWindowChange,
};
//# sourceMappingURL=window.js.map