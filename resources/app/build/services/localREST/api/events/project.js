const electron = require('electron');
const logger = require('../../../../logger')('RestEvent:Project');
const app = electron.app;
function onProjectOpen(connections, data) {
    logger.info('onProjectOpen data:', data);
    if (app.editors.current) {
        const connection = connections.get(app.editors.current.session_id);
        const packet = {
            channel: 'app',
            topic: 'project.opened',
            data: JSON.stringify(data)
        };
        connection.send(JSON.stringify(packet));
    }
}
function onOpenFromCloud(connections, data) {
    logger.info('onOpenFromCloud data:', data);
    if (app.editors.current) {
        const connection = connections.get(app.editors.current.session_id);
        const packet = {
            channel: 'app',
            topic: 'project.openedFromCloud',
            data: JSON.stringify(data)
        };
        connection.send(JSON.stringify(packet));
    }
}
function onProjectCreate(connections, data) {
    logger.info('onProjectCreate data: ', data);
    if (app.editors.current) {
        const connection = connections.get(app.editors.current.session_id);
        const packet = {
            channel: 'app',
            topic: 'project.create',
            data: JSON.stringify(data)
        };
        connection.send(JSON.stringify(packet));
    }
}
function onTempProjectCreate(connections, data) {
    logger.info('onTempProjectCreate data: ', data);
    if (app.editors.current) {
        const connection = connections.get(app.editors.current.session_id);
        const packet = {
            channel: 'app',
            topic: 'project.createTemp',
            data: JSON.stringify(data)
        };
        connection.send(JSON.stringify(packet));
    }
}
module.exports = {
    'project.opened': onProjectOpen,
    'project.create': onProjectCreate,
    'project.createTemp': onTempProjectCreate,
    'project.openedFromCloud': onOpenFromCloud
};
//# sourceMappingURL=project.js.map