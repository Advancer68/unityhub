const logger = require('../../../../logger')('RestEvent:License');
function onLicenseChange(connections, data) {
    logger.info('onLicenseChange', 'data:', data, 'connections: ', connections);
    const packet = {
        channel: 'app',
        topic: 'license.change',
        data: JSON.stringify(data)
    };
    for (var [sessionId, currConnection] of connections) {
        logger.debug(`send connection: [ ${sessionId}] `);
        currConnection.send(JSON.stringify(packet));
    }
}
module.exports = {
    'license.change': onLicenseChange,
};
//# sourceMappingURL=license.js.map