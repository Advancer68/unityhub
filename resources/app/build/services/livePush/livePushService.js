var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const axios = require('axios');
const auth = require('../localAuth/auth');
const promisify = require('es6-promisify');
const localStorageAsync = require('electron-json-storage');
const notificationService = require('../notification/notificationService');
const logger = require('../../logger')('LivePush');
const localStorage = {
    get: promisify(localStorageAsync.get, localStorageAsync),
    set: promisify(localStorageAsync.set, localStorageAsync),
};
const LIVEPUSH_CONNECT_USERID = 'live_push_connect_user_id';
const checkLivePushDuration = 1000 * 60 * 10;
class LivePushService {
    constructor() {
        this.basePath = 'https://connect.unity.cn';
        this.authEndpoint = `${this.basePath}/api/hub/login`;
        this.endpoint = `${this.basePath}/api/hub/livePush`;
    }
    init(windowManager, licenseClient) {
        setInterval(() => {
            this.getLivePushes().then(push => {
                if (push) {
                    this.showNotification(windowManager, licenseClient, push);
                }
            }).catch(err => {
                logger.error('fail to ge live pushes:', err);
            });
        }, checkLivePushDuration);
    }
    showNotification(windowManager, licenseClient, notification) {
        const { title, body, openType, link, showType, inlineAction } = notification;
        if (showType === 'system') {
            notificationService.showsNotification({
                title,
                body,
                click: () => {
                    if (openType === 'external_link') {
                        windowManager.openExternal(link);
                    }
                    else {
                        windowManager.mainWindow.show('community');
                        windowManager.mainWindow.loadPage('community');
                    }
                },
            });
        }
        else if (showType === 'inline') {
            if (inlineAction === 'remove_license') {
                licenseClient.reset();
            }
            const encodedTitle = encodeURIComponent(title);
            const encodedLink = encodeURIComponent(link);
            windowManager.mainWindow.show('community', `showModel=true&title=${encodedTitle}&link=${encodedLink}`);
        }
    }
    getConnectUserIDStoreKey(authId) {
        return `${LIVEPUSH_CONNECT_USERID}_${authId}`;
    }
    getConnectUserID(authId) {
        return __awaiter(this, void 0, void 0, function* () {
            const key = this.getConnectUserIDStoreKey(authId);
            const connectUserId = yield localStorage.get(key);
            return (connectUserId && connectUserId.value);
        });
    }
    setConnectUserID(authId, connectUserId) {
        return __awaiter(this, void 0, void 0, function* () {
            const key = this.getConnectUserIDStoreKey(authId);
            return yield localStorage.set(key, { value: connectUserId });
        });
    }
    getConnectUserIdByToken(accessToken) {
        return __awaiter(this, void 0, void 0, function* () {
            return yield axios.post(this.authEndpoint, null, {
                headers: {
                    Authorization: `Bearer ${accessToken}`,
                },
            });
        });
    }
    getLivePushes() {
        return __awaiter(this, void 0, void 0, function* () {
            let user = yield auth.getUserInfo();
            if (!user) {
                return Promise.resolve();
            }
            user = JSON.parse(user);
            if (!user.userId) {
                return Promise.resolve();
            }
            let connectUserId = yield this.getConnectUserID(user.userId);
            if (!connectUserId) {
                const userIdResponse = yield this.getConnectUserIdByToken(user.accessToken);
                connectUserId = userIdResponse.data;
                this.setConnectUserID(user.userId, connectUserId);
            }
            const buff = Buffer.from(connectUserId);
            const encodedUserId = encodeURIComponent(buff.toString('base64'));
            const livePushResponse = yield axios.get(this.endpoint, {
                params: {
                    userId: encodedUserId
                },
                responseType: 'json'
            });
            return Promise.resolve(livePushResponse.data);
        });
    }
}
module.exports = new LivePushService();
//# sourceMappingURL=livePushService.js.map