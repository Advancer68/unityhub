var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const ipc = require('node-ipc');
const unityIPCNameFormatter = require('./unityIPCNameFormatter');
const logger = require('../logger')('UnityIPCClient');
class UnityIPCClient {
    constructor(serverName) {
        this.name = unityIPCNameFormatter.formatName(serverName);
        this.ipc = new ipc.IPC();
    }
    connect() {
        return __awaiter(this, void 0, void 0, function* () {
            logger.debug(`connecting to IPC Server ${this.name}`);
            this.ipc.config.id = this.name;
            this.ipc.config.retry = 1500;
            this.ipc.config.silent = true;
            yield this._connectToIPCServer();
            this.on('connect', () => {
                this.serverLog('connected');
            });
            this.on('disconnect', () => {
                logger.debug(`disconnected from ${this.name}`);
            });
        });
    }
    _connectToIPCServer() {
        return new Promise(resolve => {
            this.ipc.connectTo(this.name, this.name, () => {
                resolve();
            });
        });
    }
    on(eventName, callback) {
        this.ipc.of[this.name].on(eventName, callback);
    }
    emit(eventName, data) {
        this.ipc.of[this.name].emit(eventName, data);
    }
    serverLog(message) {
        this.ipc.of[this.name].emit('message', message);
    }
}
module.exports = UnityIPCClient;
//# sourceMappingURL=unityIPCClient.js.map