const os = require('os');
const platform = os.platform();
module.exports = {
    formatName(name) {
        if (platform === 'win32') {
            return `Unity-${name}`;
        }
        if (platform === 'darwin' || platform === 'linux') {
            return `/tmp/Unity-${name}.sock`;
        }
        throw Error('Unsupported platform');
    }
};
//# sourceMappingURL=unityIPCNameFormatter.js.map