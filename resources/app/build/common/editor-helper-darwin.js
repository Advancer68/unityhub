const os = require('os');
const path = require('path');
const editorHelper = {
    getUnityFolderInUserData() {
        return 'Unity';
    },
    getBaseUserDataPath() {
        return `${os.homedir()}/Library`;
    },
    getEditorLogPath() {
        return path.join(os.homedir(), 'Library', 'Logs', 'Unity');
    },
};
module.exports = editorHelper;
//# sourceMappingURL=editor-helper-darwin.js.map