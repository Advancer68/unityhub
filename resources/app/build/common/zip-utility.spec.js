const zipUtility = require('./zip-utility');
const { hubFS } = require('../fileSystem');
const path = require('path');
const request = require('request');
const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const chaiAsPromised = require("chai-as-promised");
chai.use(chaiAsPromised);
describe('zipUtility', () => {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });
    afterEach(() => sandbox.restore());
    describe('getUncompressedSize', () => {
        describe('from http url', () => {
            let headCallbacks;
            let headStub;
            beforeEach(() => {
                sandbox.stub(hubFS, 'startsWithHttp').returns(true);
                headCallbacks = {};
                headStub = {
                    on: function (event, callback) {
                        headCallbacks[event] = callback;
                        return this;
                    }
                };
                sandbox.stub(request, 'head').returns(headStub);
            });
            it('when having am invalid http zip should reject', () => {
                const promise = zipUtility.getUncompressedSize('http://invalid.zip', 'destination')
                    .then(() => expect('should not get here').to.equal('false'))
                    .catch((e) => expect(e).to.equal('invalid url'));
                headCallbacks.response({
                    statusCode: 404,
                    headers: {
                        'content-length': 10
                    }
                });
                return promise;
            });
            it('when having a valid http zip should resolve', () => {
                const promise = zipUtility.getUncompressedSize('https://valid.zip', 'destination')
                    .then((size) => expect(size).to.equal(20));
                headCallbacks.response({
                    statusCode: 200,
                    headers: {
                        'content-length': 10
                    }
                });
                return promise;
            });
            it('when getting an error should reject', () => {
                const promise = zipUtility.getUncompressedSize('https://valid.zip', 'destination')
                    .catch((err) => expect(err).to.equal('an error occurred'));
                headCallbacks.error('an error occurred');
                return promise;
            });
        });
        describe('from local url', () => {
            beforeEach(() => {
                sandbox.stub(hubFS, 'startsWithHttp').returns(false);
            });
            it('when having an invalid local zip should reject', () => {
                return zipUtility.getUncompressedSize('invalid.zip', 'destination')
                    .then(() => expect('should not get here').to.equal('false'))
                    .catch((e) => expect(e.code).to.equal('ENOENT'));
            });
        });
    });
    describe('unzip', () => {
        it('when having an invalid zip should reject', () => {
            return zipUtility.unzip('invalid.zip', 'destination').catch((err) => {
                expect(err.code).to.eq('ENOENT');
                expect(err.syscall).to.eq('open');
                expect(path.basename(err.path)).to.eq('invalid.zip');
            });
        });
    });
});
//# sourceMappingURL=zip-utility.spec.js.map