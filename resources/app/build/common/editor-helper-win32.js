const isProcessRunning = require('@unityhub/is-win-process-running');
const hubUtils = require('./hub-utils');
const editorHelper = {
    getUnityFolderInUserData() {
        return 'Unity';
    },
    getBaseUserDataPath() {
        return process.env.APPDATA || '%AppData%';
    },
    getEditorLogPath() {
        return (`${process.env.USERPROFILE}\\AppData\\Local\\Unity\\Editor`);
    },
    isVersionRunning(editor) {
        let isRunning = false;
        if (hubUtils.checkNestedProperties(editor, 'location', 0) === false) {
            return Promise.resolve(isRunning);
        }
        isRunning = isProcessRunning(editor.location[0]);
        return Promise.resolve(isRunning);
    }
};
module.exports = editorHelper;
//# sourceMappingURL=editor-helper-win32.js.map