const settings = require('../services/localSettings/localSettings');
const localStorage = require('electron-json-storage');
const PollingClient = require('./pollingClient');
const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const axios = require('axios');
const postal = require('postal');
chai.use(sinonChai);
describe('PollingClient', () => {
    let sandbox, settingsMock, postalStub, requestStub;
    let pollingClient;
    let customLogger = { debug: () => { }, warn: () => { }, info: () => { } };
    let defaultData = { someJson: 'default', commonKey: 'defaultDataVersion' };
    let cachedData = { blah: 'blah', empty: {}, bad: undefined };
    let cloudData = { data: 'cloudData' };
    let jsonStorageKey = 'KEY';
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        settings.keys.TEST_REFRESH_INTERVAL = 'testRefreshInterval';
        pollingClient = new PollingClient();
        sandbox.stub(pollingClient, 'refreshIntervalKey').get(() => { return settings.keys.TEST_REFRESH_INTERVAL; });
        sandbox.stub(pollingClient, 'logger').get(() => { return customLogger; });
        sandbox.stub(pollingClient, 'defaultData').get(() => { return defaultData; });
        sandbox.stub(pollingClient, 'jsonStorageKey').get(() => { return jsonStorageKey; });
        sandbox.stub(pollingClient, 'isInitBlocking').get(() => { return true; });
        pollingClient.setEndpoint = sandbox.spy();
        settingsMock = {
            [settings.keys.CLOUD_ENVIRONMENT]: settings.cloudEnvironments.DEV,
            [settings.keys.TEST_REFRESH_INTERVAL]: 1000000,
        };
        sandbox.stub(localStorage, 'get').callsFake((key, callback) => {
            if (key === 'exception') {
                throw new Error();
            }
            callback(null, cachedData[key]);
        });
        sandbox.stub(localStorage, 'set').resolves();
        sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);
        postalStub = sandbox.stub(postal, 'subscribe');
    });
    afterEach(() => {
        pollingClient.stopRefreshInterval();
        sandbox.restore();
    });
    describe('init', () => {
        it('should set the cloudEnvironment', () => {
            return pollingClient.init().then(() => {
                expect(pollingClient.cloudEnvironment).equals('dev');
            });
        });
        it('should set the refresh interval if they key to config exists', () => {
            return pollingClient.init().then(() => {
                expect(pollingClient.refreshInterval).equals(1000000);
            });
        });
        it('should call setEndpoint function', () => {
            return pollingClient.init().then(() => {
                expect(pollingClient.setEndpoint).to.have.been.called;
            });
        });
        it('should subscribe to connectInfo change event', () => {
            return pollingClient.init().then(() => {
                expect(JSON.stringify(postalStub.getCall(0).args[0])).to.equal(JSON.stringify({
                    callback: function callback() {
                    }, channel: 'app', topic: 'connectInfo.changed',
                }));
            });
        });
        it('should call loadFromCache', () => {
            sandbox.spy(pollingClient, 'loadFromCache');
            return pollingClient.init().then(() => {
                expect(pollingClient.loadFromCache).to.have.been.called;
            });
        });
        it('should call startRefreshInterval', () => {
            sandbox.spy(pollingClient, 'startRefreshInterval');
            return pollingClient.init().then(() => {
                expect(pollingClient.startRefreshInterval).to.have.been.called;
            });
        });
    });
    describe('loadFromCache', () => {
        it('should load the data from localStorage with the given key', () => {
            jsonStorageKey = 'blah';
            sandbox.stub(pollingClient, 'overwriteDefault').get(() => { return true; });
            return pollingClient.loadFromCache().then(() => {
                expect(pollingClient.data).equal(cachedData['blah']);
            });
        });
        it('should fall back to the default data if the local storage was empty', () => {
            jsonStorageKey = 'empty';
            return pollingClient.loadFromCache().then(() => {
                expect(pollingClient.data).equal(defaultData);
            });
        });
        it('should fall back to the default data if the local storage was undefined', () => {
            jsonStorageKey = 'bad';
            return pollingClient.loadFromCache().then(() => {
                expect(pollingClient.data).equal(defaultData);
            });
        });
        it('should fall back to the default data if an exception happens', () => {
            jsonStorageKey = 'exception';
            return pollingClient.init().then(() => {
                return pollingClient.loadFromCache();
            }).then(() => {
                expect(pollingClient.data).equal(defaultData);
            });
        });
    });
    describe('startRefreshInterval', () => {
        let stub;
        beforeEach(() => {
            stub = sandbox.stub(pollingClient, 'refreshData').returns(Promise.resolve());
        });
        it('should return call refreshData and schedule a interval for calling it', () => {
            let clock = sandbox.useFakeTimers();
            return pollingClient.startRefreshInterval().then(() => {
                expect(stub).to.have.been.called.once;
                clock.tick(settingsMock[settings.keys.TEST_REFRESH_INTERVAL] + 1);
                expect(stub).to.have.been.called.twice;
                pollingClient.stopRefreshInterval();
            });
        });
        it('should set the refreshHandler', () => {
            return pollingClient.startRefreshInterval().then(() => {
                expect(pollingClient.refreshHandler).not.to.equal(null);
            });
        });
    });
    describe('stopRefreshInterval', () => {
        let stub;
        beforeEach(() => {
            stub = sandbox.stub(pollingClient, 'refreshData').returns(Promise.resolve());
            return pollingClient.init();
        });
        it('should set the refreshHandler to null', () => {
            expect(pollingClient.refreshHandler).not.to.equal(null);
            pollingClient.stopRefreshInterval();
            expect(pollingClient.refreshHandler).to.equal(null);
        });
    });
    describe('resetRefreshInterval', () => {
        it('should stop and start the refreshInterval', () => {
            let stopStub = sandbox.stub(pollingClient, 'stopRefreshInterval');
            let startStub = sandbox.stub(pollingClient, 'startRefreshInterval').resolves();
            return pollingClient.resetRefreshInterval().then(() => {
                expect(stopStub).to.have.been.called.once;
                expect(startStub).to.have.been.called.once;
                expect(stopStub).calledBefore(startStub);
            });
        });
    });
    describe('refreshData', () => {
        beforeEach(() => {
            pollingClient.endpoint = 'test';
        });
        it('should not send a request if the status is offline', () => {
            requestStub = sandbox.stub(axios, 'get').resolves({});
            pollingClient.online = false;
            return pollingClient.init()
                .then(() => pollingClient.refreshData()).then(() => {
                expect(requestStub).not.to.have.been.called;
            });
        });
        it('should set the default data if data is undefined and status is offline', () => {
            requestStub = sandbox.stub(axios, 'get').resolves({});
            pollingClient.online = false;
            pollingClient.data = undefined;
            return pollingClient.init()
                .then(() => pollingClient.refreshData()).then(() => {
                expect(pollingClient.data).equal(pollingClient.defaultData);
            });
        });
        it('should not change the data if data is not undefined and status is offline', () => {
            requestStub = sandbox.stub(axios, 'get').resolves({});
            let currentData = { test: 'blahblah' };
            pollingClient.online = false;
            pollingClient.data = currentData;
            return pollingClient.init()
                .then(() => pollingClient.refreshData())
                .then(() => {
                expect(pollingClient.data).to.equals(currentData);
            });
        });
        it('should  send a request if the status is online', () => {
            requestStub = sandbox.stub(axios, 'get').resolves({});
            pollingClient.online = true;
            return pollingClient.init()
                .then(() => { return pollingClient.refreshData(); })
                .then(() => {
                expect(requestStub).to.have.been.calledWith('test', {
                    responseType: 'json'
                });
            });
        });
        it('should set the received data if request was successful', () => {
            sandbox.stub(pollingClient, 'overwriteDefault').get(() => { return true; });
            requestStub = sandbox.stub(axios, 'get').resolves({ data: cloudData });
            pollingClient.online = true;
            pollingClient.data = { data: 'localData' };
            return pollingClient.init()
                .then(() => { return pollingClient.refreshData(); })
                .then(() => {
                expect(JSON.stringify(pollingClient.data)).to.eq(JSON.stringify(cloudData));
            });
        });
        it('should merge the received data with local data if overwriteDefault is false', () => {
            sandbox.stub(pollingClient, 'overwriteDefault').get(() => { return false; });
            pollingClient.online = true;
            requestStub = sandbox.stub(axios, 'get').resolves({ data: { commonKey: 'cloudDataVersion', cloudKey: "cloudDataOnly" } });
            return pollingClient.init()
                .then(() => { return pollingClient.refreshData(); })
                .then(() => {
                expect(JSON.stringify(pollingClient.data)).to.eq(JSON.stringify({ "someJson": "default", "commonKey": "cloudDataVersion", "cloudKey": "cloudDataOnly" }));
            });
        });
        it('should set the last success time if request was successful', () => {
            requestStub = sandbox.stub(axios, 'get').resolves({});
            pollingClient.online = true;
            pollingClient.lastSuccess = 0;
            pollingClient.endpoint = 'test';
            return pollingClient.init()
                .then(() => { return pollingClient.refreshData(); })
                .then(() => {
                expect(pollingClient.lastSuccess).to.not.equal(0);
            });
        });
        it('should not set the data id the received data is empty', () => {
            requestStub = sandbox.stub(axios, 'get').resolves({});
            pollingClient.online = true;
            pollingClient.data = cachedData;
            return pollingClient.init()
                .then(() => { return pollingClient.refreshData(); })
                .then(() => {
                expect(JSON.stringify(pollingClient.data)).to.eq(JSON.stringify(cachedData));
            });
        });
        it('should not set the data id the received data is undefined', () => {
            requestStub = sandbox.stub(axios, 'get').resolves(undefined);
            pollingClient.online = true;
            pollingClient.endpoint = 'test';
            pollingClient.data = cachedData;
            return pollingClient.init()
                .then(() => { return pollingClient.refreshData(); })
                .then(() => {
                expect(JSON.stringify(pollingClient.data)).to.eq(JSON.stringify(cachedData));
            });
        });
        it('should store the data in cache if the jsonStorageKey is defined', () => {
            let localData = { data: 'localData' };
            sandbox.stub(pollingClient, 'overwriteDefault').get(() => { return false; });
            requestStub = sandbox.stub(axios, 'get').resolves(cloudData);
            pollingClient.online = true;
            return pollingClient.init()
                .then(() => { pollingClient.data = localData; })
                .then(() => { return pollingClient.refreshData(); })
                .then(() => {
                expect(localStorage.set).to.have.been.called;
            });
        });
        it('should set default data if the request fails and there was no current data', () => {
            requestStub = sandbox.stub(axios, 'get').rejects();
            pollingClient.online = true;
            pollingClient.data = undefined;
            return pollingClient.init()
                .then(() => { return pollingClient.refreshData(); })
                .then(() => {
                expect(JSON.stringify(pollingClient.data)).to.eq(JSON.stringify(defaultData));
            });
        });
    });
});
//# sourceMappingURL=pollingClient.spec.js.map