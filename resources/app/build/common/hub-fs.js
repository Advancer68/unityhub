var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const os = require('os');
const path = require('path');
const _ = require('lodash');
const uuid = require('uuid/v1');
const { exec } = require('child_process');
const request = require('request-promise-native');
const disk = require('diskusage');
const fs = require('fs-extra');
const hubfsPlatform = require(`./hub-fs-${os.platform()}`);
function getNextSuffix(homonyms) {
    let nextNum = 1;
    homonyms.forEach((homonym) => {
        const suffix = homonym.match(/(?:\((\d+)\))$/);
        if (suffix !== null && suffix[1] !== undefined) {
            const index = parseInt(suffix[1], 10);
            if (index >= nextNum) {
                nextNum = index + 1;
            }
        }
    });
    return nextNum;
}
const hubfs = {
    fakeUserAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
    filenameFromUrl(url) {
        let result;
        const regExResult = /.*\/([^?]*)(\?.*)?$/.exec(url);
        if (regExResult !== null) {
            result = regExResult[1];
        }
        return result;
    },
    hasExtension(url) {
        return /^.*\.[^\\/]+$/.test(url);
    },
    getFinalRedirectUrl(url) {
        const options = {
            uri: url,
            method: 'HEAD',
            followAllRedirects: true,
            followOriginalHttpMethod: true,
            resolveWithFullResponse: true,
            headers: {
                'User-Agent': this.fakeUserAgent
            }
        };
        return request(options)
            .then((response) => {
            if (response.headers['content-disposition'] && response.headers['content-disposition'].indexOf('filename=') >= 0) {
                if (response.headers['content-disposition'].indexOf('filename="') >= 0) {
                    return `/${/.*filename="(.*?[^"])"/.exec(response.headers['content-disposition'])[1]}`;
                }
                else if (response.headers['content-disposition'].indexOf('filename=') >= 0) {
                    return `/${/.*filename=(.*)/.exec(response.headers['content-disposition'])[1]}`;
                }
            }
            return response.request.href;
        })
            .catch(() => url);
    },
    getTmpDirPath() {
        return path.join(os.tmpdir(), `unityhub-${uuid()}`);
    },
    pathExists(basename = '', filename = '') {
        return __awaiter(this, void 0, void 0, function* () {
            return fs.pathExists(path.join(basename, filename));
        });
    },
    mkdirRecursive(dirpath) {
        return __awaiter(this, void 0, void 0, function* () {
            return fs.mkdirp(dirpath);
        });
    },
    readFile(filePath) {
        return __awaiter(this, void 0, void 0, function* () {
            return fs.readFile(filePath);
        });
    },
    isFilePresent(filePath) {
        try {
            fs.lstatSync(filePath);
            return true;
        }
        catch (error) {
            return error.code !== 'ENOENT';
        }
    },
    isFilePresentPromise(filePath) {
        return fs.stat(filePath)
            .catch((err) => {
            if (err.code === 'ENOENT') {
                return Promise.reject();
            }
            return Promise.resolve();
        });
    },
    getUniquePath(candidatePath) {
        try {
            const fileInfo = fs.statSync(candidatePath);
            if (fileInfo.isDirectory()) {
                const parentDir = path.dirname(candidatePath);
                const projectDirName = path.basename(candidatePath);
                const homonyms = fs.readdirSync(parentDir).filter(name => name === path.basename(candidatePath) || (new RegExp(`${_.escapeRegExp(projectDirName)} (?:\\(\\d+\\))$`)).test(name));
                if (homonyms.length === 1) {
                    return `${candidatePath} (1)`;
                }
                const nextSuffix = getNextSuffix(homonyms);
                return `${candidatePath} (${nextSuffix})`;
            }
            return candidatePath;
        }
        catch (error) {
            if (error.code === 'ENOENT') {
                return candidatePath;
            }
            throw error;
        }
    },
    removeFolder(folder) {
        if (folder) {
            if (folder === '') {
                return Promise.reject(`Folder not specified. Cannot remove folder (${folder}).`);
            }
            return new Promise((resolve, reject) => {
                exec(`${this.RMRF_CMD} "${folder}"`, { name: 'Remove folder' }, (error) => {
                    if (error) {
                        return reject(error);
                    }
                    return resolve();
                });
            });
        }
        return Promise.reject(`Folder not specified. Cannot remove folder (${folder}).`);
    },
    getDiskUsage(folder) {
        if (!folder) {
            return Promise.reject('Path not specified. Cannot verify disk usage.');
        }
        return new Promise((resolve, reject) => {
            disk.check(folder, (err, info) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve(info);
                }
            });
        });
    },
    isValidFileName(input) {
        const INVALID_PATH_LETTERS = /[/?<>\\:*|"]/;
        const INVALID_BEGINNING = /^((com|lpt)[1-9]{1}|con|nul|prn)\./;
        if ((typeof input !== 'string')
            || INVALID_PATH_LETTERS.test(input)
            || INVALID_BEGINNING.test(input)) {
            return false;
        }
        if (os.platform() !== 'linux' && input.startsWith('.')) {
            return false;
        }
        return !(input.endsWith('.') || input.endsWith(' ') || input.startsWith(' '));
    },
    isValidPath(input) {
        if ((typeof input !== 'string') || input.search('//') !== -1) {
            return false;
        }
        input = path.normalize(input);
        const pathParts = input.split(path.sep);
        for (let i = 0; i < pathParts.length; i++) {
            const part = pathParts[i];
            if (!this.isValidFileName(part)) {
                if (!(i === 0 && part.length === 2 && part.charAt(1) === ':' && this.isValidFileName(part.substring(0, 1)))) {
                    return false;
                }
            }
        }
        return true;
    },
    startsWithHttp(input) {
        return !!(input.startsWith('http://') || input.startsWith('https://'));
    },
    getDiskSpaceAvailable(folder) {
        return this.getDiskUsage(this.getDiskRootPath(folder));
    },
};
module.exports = Object.assign(hubfs, hubfsPlatform);
//# sourceMappingURL=hub-fs.js.map