const sudo = require('sudo-prompt');
const path = require('path');
const fs = require('fs-extra');
const unityHubFS = {
    RMRF_CMD: 'rd /q /s',
    elevateAndMakeDir(dirPath) {
        return new Promise((resolve, reject) => {
            sudo.exec(`mkdir "${dirPath}"`, { name: 'Unity Hub' }, (error) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve();
                }
            });
        });
    },
    isFileUnlocked(filePath) {
        return fs.open(filePath, 'w').then((fd) => {
            fs.close(fd);
        });
    },
    getDiskRootPath(folder) {
        if (!folder || folder === '') {
            throw Error('Invalid path given');
        }
        return path.parse(folder).root;
    },
    isInRecycleBin(dirPath) {
        return new Promise((resolve, reject) => {
            try {
                resolve(!!dirPath.match(/\$RECYCLE.BIN/gi));
            }
            catch (e) {
                reject(e);
            }
        });
    },
    getEditorFolderFromExecutable(execPath) {
        return path.dirname(path.dirname(execPath));
    }
};
module.exports = unityHubFS;
//# sourceMappingURL=hub-fs-win32.js.map