var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const settings = require('../services/localSettings/localSettings');
const promisify = require('es6-promisify');
const localStorage = require('electron-json-storage');
const axios = require('axios');
const _ = require('lodash');
const postal = require('postal');
class PollingClient {
    constructor() {
        this.endpoint = '';
        this.online = true;
        this.refreshHandler = null;
        this.lastSuccess = 0;
    }
    get logger() {
        throw new Error('You need to define get logger() for the subclasses of PollingClient.');
    }
    get jsonStorageKey() {
        throw new Error('You need to define get jsonStorageKey() for the subclasses of PollingClient. ' +
            'This is the key which is used to store the cache in json storage');
    }
    get refreshIntervalKey() {
        throw new Error('You need to define get refreshIntervalKey() for the subclasses of PollingClient. ' +
            'This is the key to retrieve the refresh interval.');
    }
    get isInitBlocking() {
        throw new Error('You need to define get isInitBlocking() for the subclasses of PollingClient. ' +
            'This flag determines if the init function of this class should block the application init sequence or not');
    }
    get defaultData() {
        throw new Error('You need to define get defaultData() for the subclasses of PollingClient. ' +
            'This is the data to be used if there was no internet and cache');
    }
    get overwriteDefault() {
        throw new Error('You need to define get overwriteDefault() for the subclasses of PollingClient. ' +
            'This flag determines if the default values should be overwritten or merged with the cloud data');
    }
    init() {
        this.cloudEnvironment = settings.get(settings.keys.CLOUD_ENVIRONMENT);
        this.setRefreshInterval(this.refreshIntervalKey);
        this.setEndpoint();
        postal.subscribe({
            channel: 'app',
            topic: 'connectInfo.changed',
            callback: (data) => {
                if (data.connectInfo.online === true && this.online === false) {
                    const currentTime = new Date().getTime();
                    this.online = data.connectInfo.online;
                    if (currentTime - this.lastSuccess > this.refreshInterval) {
                        this.resetRefreshInterval();
                    }
                    return;
                }
                this.online = data.connectInfo.online;
            },
        });
        return this.loadFromCache().then(() => {
            const promise = this.startRefreshInterval();
            if (this.isInitBlocking === true) {
                return promise;
            }
            return Promise.resolve();
        });
    }
    setEndpoint() {
        throw new Error('You need to define setEndpoint() for the subclasses of PollingClient. ' +
            'This function set this.endpoint to the endpoint that needs to be polled.');
    }
    setRefreshInterval(key) {
        this.refreshInterval = settings.get(key);
    }
    loadFromCache() {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.data !== undefined) {
                this.logger.debug('Data was retrieved from the cloud before the cache.');
                return;
            }
            try {
                const storedData = yield promisify(localStorage.get)(this.jsonStorageKey);
                const data = (storedData && Object.keys(storedData).length !== 0) ? storedData : this.defaultData;
                if (this.overwriteDefault === true) {
                    this.data = data;
                }
                else {
                    this.data = Object.assign({}, this.defaultData, data);
                }
            }
            catch (err) {
                this.logger.warn(err);
                this.data = this.defaultData;
            }
        });
    }
    startRefreshInterval() {
        if (this.refreshHandler === null) {
            this.refreshHandler = setInterval(this.refreshData.bind(this), this.refreshInterval);
            return this.refreshData();
        }
        return Promise.resolve();
    }
    stopRefreshInterval() {
        if (this.refreshHandler !== null) {
            clearInterval(this.refreshHandler);
            this.refreshHandler = null;
        }
    }
    resetRefreshInterval() {
        this.stopRefreshInterval();
        return this.startRefreshInterval();
    }
    refreshData() {
        if (!this.online) {
            this.logger.debug('There is no network, so no request will be sent');
            if (this.data === undefined) {
                this.data = this.defaultData;
            }
            return Promise.resolve();
        }
        return axios.get(this.endpoint, {
            responseType: 'json'
        })
            .then((response) => {
            const data = response.data;
            this.lastSuccess = new Date().getTime();
            if (data && Object.keys(data).length !== 0 && !_.isEqual(data, this.data)) {
                if (this.overwriteDefault === true) {
                    this.data = data;
                }
                else {
                    this.data = Object.assign({}, this.defaultData, data);
                }
                return promisify(localStorage.set)(this.jsonStorageKey, this.data);
            }
            return Promise.resolve();
        })
            .catch((reason) => {
            this.logger.info(`Failed to refresh data, fallback to default data. Reason: ${reason.message || reason.response.body}`);
            if (this.data === undefined) {
                this.data = this.defaultData;
            }
        });
    }
}
module.exports = PollingClient;
//# sourceMappingURL=pollingClient.js.map