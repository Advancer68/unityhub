const logger = require('../logger')('hubCache');
class hubCache {
    constructor() {
        this.hubCache = {};
    }
    set(key, value, ttl) {
        if (this.hubCache[key]) {
            logger.warn(`key ${key} already been set and will be reset by new value`);
        }
        this.hubCache[key] = { value: this._getDeepCopy(value) };
        if (ttl) {
            this.hubCache[key].timestamp = Date.now();
            this.hubCache[key].ttl = ttl;
            logger.debug(`Setting key ${key} with ttl: ${ttl} at timestamp ${this.hubCache[key].timestamp}`);
        }
        else {
            logger.debug(`Setting key ${key} with no ttl`);
        }
    }
    get(key) {
        const cachedItem = this.hubCache[key];
        if (cachedItem && cachedItem.value && !this._isCacheExpired(cachedItem)) {
            logger.debug(`Cache hit for key: ${key}`);
            return this._getDeepCopy(cachedItem.value);
        }
        logger.debug(`Cache miss for key: ${key}`);
        delete this.hubCache[key];
        return null;
    }
    _getTimeStamp(cachedItem) {
        if (cachedItem && cachedItem.timestamp) {
            return cachedItem.timestamp;
        }
        return null;
    }
    _getTTL(cachedItem) {
        if (cachedItem && cachedItem.ttl) {
            return cachedItem.ttl;
        }
        return null;
    }
    _isCacheExpired(cachedItem) {
        if (!this._getTTL(cachedItem)) {
            return false;
        }
        if (cachedItem) {
            const lastCacheTime = this._getTimeStamp(cachedItem);
            if (lastCacheTime && (Date.now() - lastCacheTime) < this._getTTL(cachedItem)) {
                return false;
            }
        }
        return true;
    }
    _getDeepCopy(value) {
        return JSON.parse(JSON.stringify(value));
    }
}
module.exports = hubCache;
//# sourceMappingURL=hubCache.js.map