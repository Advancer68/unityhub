const yauzl = require('yauzl');
const path = require('path');
const EventEmitter = require('events');
const request = require('request');
const platform = require('os').platform();
const { fs, hubFS } = require('../fileSystem');
class ZipUtility extends EventEmitter {
    getUncompressedSize(zipPath) {
        if (hubFS.startsWithHttp(zipPath)) {
            return getHttpUncompressedSize(zipPath);
        }
        return getLocalUncompressedSize(zipPath);
    }
    unzip(zipPath, destination) {
        return new Promise((resolve, reject) => {
            yauzl.open(zipPath, { lazyEntries: true }, (err, zipFile) => {
                if (err) {
                    reject(err);
                    return;
                }
                zipFile.readEntry();
                zipFile.on('entry', (entry) => {
                    if (/\/$/.test(entry.fileName)) {
                        mkdirp(path.join(destination, entry.fileName))
                            .then(() => zipFile.readEntry())
                            .catch((e) => reject(e));
                    }
                    else {
                        onFileEntry.bind(this)(reject, entry, destination, zipFile);
                    }
                });
                zipFile.on('error', (e) => reject(e));
                zipFile.on('end', () => resolve());
            });
        });
    }
}
function onFileEntry(reject, entry, destination, zipfile) {
    mkdirp(path.join(destination, path.dirname(entry.fileName)))
        .then(() => {
        zipfile.openReadStream(entry, (e, readStream) => {
            if (e) {
                reject(e);
                return;
            }
            readStream.on('end', () => {
                zipfile.readEntry();
            });
            let writeStream;
            const outputPath = path.join(destination, entry.fileName);
            if (platform === 'linux') {
                const mode = getEntryMode(entry);
                writeStream = fs.createWriteStream(outputPath, { mode });
            }
            else {
                writeStream = fs.createWriteStream(outputPath);
            }
            readStream.pipe(writeStream);
        });
    })
        .catch((e) => reject(e));
}
function getLocalUncompressedSize(zipPath) {
    return new Promise((resolve, reject) => {
        let uncompressedSize = 0;
        yauzl.open(zipPath, { lazyEntries: true }, (err, zipFile) => {
            if (err) {
                reject(err);
                return;
            }
            zipFile.readEntry();
            zipFile.on('entry', (entry) => {
                if (!/\/$/.test(entry.fileName)) {
                    zipFile.openReadStream(entry, (e) => {
                        if (e) {
                            reject(e);
                            return;
                        }
                        uncompressedSize += entry.uncompressedSize;
                        zipFile.readEntry();
                    });
                }
                else {
                    zipFile.readEntry();
                }
            });
            zipFile.on('error', (e) => {
                reject(e);
            });
            zipFile.on('end', () => {
                resolve(uncompressedSize);
            });
        });
    });
}
function getHttpUncompressedSize(zipPath) {
    return new Promise((resolve, reject) => {
        request.head(zipPath)
            .on('response', (res) => {
            if (res.statusCode === 200) {
                resolve(Math.round(res.headers['content-length'] * 2.04));
                return;
            }
            reject('invalid url');
        })
            .on('error', (e) => reject(e));
    });
}
function mkdirp(dir) {
    if (dir === '.' || dir === '..') {
        return Promise.resolve();
    }
    return fs.ensureDir(dir)
        .catch(err => {
        throw err;
    });
}
function getEntryMode(entry) {
    return entry.externalFileAttributes >>> 16;
}
module.exports = new ZipUtility();
//# sourceMappingURL=zip-utility.js.map