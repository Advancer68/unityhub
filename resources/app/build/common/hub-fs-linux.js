const sudo = require('sudo-prompt');
const flock = require('fs-ext').flock;
const promisify = require('es6-promisify');
const { exec } = require('child_process');
const path = require('path');
const mountutil = require('linux-mountutils');
const fs = require('fs-extra');
const flockAsync = promisify(flock);
const unityHubFS = {
    RMRF_CMD: 'rm -rf',
    elevateAndMakeDir(dirPath, mode = 744) {
        return new Promise((resolve, reject) => {
            sudo.exec(`mkdir -p -m ${mode} ${dirPath}`, { name: 'Unity Hub' }, (error) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve();
                }
            });
        });
    },
    moveFilesToParent(dir) {
        return new Promise((resolve, reject) => {
            exec(`rsync -a "${dir}/" "${dir}/.."`, {}, (error) => {
                if (error) {
                    reject(error);
                }
                else {
                    resolve();
                }
            });
        });
    },
    isFileUnlocked(filePath) {
        return new Promise((resolve, reject) => {
            fs.open(filePath, 'w')
                .then((fd) => flockAsync(fd, 'exnb')
                .then(() => flockAsync(fd, 'un')
                .then(resolve)
                .catch(resolve)))
                .catch(reject);
        });
    },
    getDiskRootPath(folder) {
        if (!folder || folder === '') {
            throw Error('Invalid path given');
        }
        return this._getMounted(folder) ? this._getMounted(folder) : path.parse(folder).root;
    },
    _getMounted(folder) {
        while (folder && folder.length > 1) {
            if (mountutil.isMounted(folder).mounted)
                return folder;
            folder = path.parse(folder).dir;
        }
        return path.parse(folder).root;
    },
    getEditorFolderFromExecutable(execPath) {
        return path.dirname(path.dirname(execPath));
    }
};
module.exports = unityHubFS;
//# sourceMappingURL=hub-fs-linux.js.map