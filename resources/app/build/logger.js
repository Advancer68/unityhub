const { app } = require('electron');
const { Logger, transports, config } = require('winston');
const path = require('path');
const postal = require('postal');
const util = require('util');
const { fs } = require('./fileSystem');
const LOG_DIR_PATH = path.join(app.getPath('userData'), 'logs');
fs.ensureDirSync(LOG_DIR_PATH);
const logger = new Logger({
    transports: [
        new transports.File({
            name: 'prod-log',
            level: 'info',
            filename: path.join(LOG_DIR_PATH, 'info-log.json'),
            maxFiles: 5,
            maxsize: 5242880,
            tailable: true
        })
    ]
});
const handleCriticalErrors = (reason) => {
    if (reason) {
        logger.error(`Unhandled promise rejection, reason: ${reason.message}\n\tstack ${reason.stack}`);
    }
};
process.on('unhandledRejection', handleCriticalErrors);
process.on('uncaughtException', handleCriticalErrors);
module.exports = function (moduleName) {
    return {
        activateConsoleOutput() {
            logger.add(transports.File, {
                name: 'dev-log',
                level: 'silly',
                filename: path.join(LOG_DIR_PATH, 'debug-log.json'),
                maxFiles: 5,
                maxsize: 5242880,
                tailable: true,
            });
            logger.add(transports.Console, {
                level: 'silly',
                handleExceptions: true,
                humanReadableUnhandledException: true,
                timestamp() {
                    return new Date().toUTCString();
                },
                formatter({ timestamp, level, message, meta }) {
                    meta.moduleName = meta.moduleName ? `${meta.moduleName}:` : '';
                    return `[${timestamp()}] ${meta.moduleName}${config.colorize(level, level.toUpperCase())}: ${message}`;
                }
            });
        },
        activateWinInstallerProcessOutput() {
            logger.add(transports.File, {
                name: 'install-log',
                level: 'silly',
                filename: path.join(LOG_DIR_PATH, 'install-log.json'),
                maxFiles: 5,
                maxsize: 5242880,
                tailable: true,
            });
        },
        getLogDirectoryPath() {
            return LOG_DIR_PATH;
        },
        error(...contents) {
            const content = this._concatLogs(contents);
            postal.publish({
                channel: 'app',
                topic: 'error',
                data: {
                    moduleName,
                    content
                }
            });
            logger.error(content, { moduleName });
        },
        warn(...contents) {
            logger.warn(this._concatLogs(contents), { moduleName });
        },
        info(...contents) {
            logger.info(this._concatLogs(contents), { moduleName });
        },
        verbose(...contents) {
            logger.verbose(this._concatLogs(contents), { moduleName });
        },
        debug(...contents) {
            logger.debug(this._concatLogs(contents), { moduleName });
        },
        silly(...contents) {
            logger.silly(this._concatLogs(contents), { moduleName });
        },
        _concatLogs(...contents) {
            return contents.map((c) => util.inspect(c)).join('|');
        }
    };
};
//# sourceMappingURL=logger.js.map