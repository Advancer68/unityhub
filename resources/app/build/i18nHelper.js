var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const i18next = require('i18next');
const Backend = require('i18next-node-fs-backend');
const LanguageDetector = require('i18next-electron-language-detector');
const path = require('path');
const logger = require('./logger')('i18n');
const i18nConfig = require('./services/i18nConfig/i18nConfig');
const helper = {
    i18n: {},
    configure() {
        return __awaiter(this, void 0, void 0, function* () {
            const options = {
                saveMissing: true,
                fallbackLng: {
                    default: ['en'],
                    'zh-CN': ['zh_CN']
                },
                missingKeyHandler: (lng, ns, key) => {
                    logger.warn(`Missing i18n key "${key} for language "${lng}"`);
                },
                backend: {
                    loadPath: path.join(__dirname, '..', 'i18n', '{{lng}}', 'backend.json')
                },
                interpolation: { escapeValue: false }
            };
            yield i18next.use(Backend)
                .use(LanguageDetector)
                .init(options);
            const savedLanguage = yield i18nConfig.getLanguage();
            if (savedLanguage) {
                i18next.changeLanguage(savedLanguage);
            }
            helper.i18n.translate = i18next.t.bind(i18next);
        });
    }
};
module.exports = helper;
//# sourceMappingURL=i18nHelper.js.map