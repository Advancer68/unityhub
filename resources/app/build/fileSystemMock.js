class FileSystemMock {
    constructor(fs) {
        this.fs = fs;
        this.fileSystem = {};
    }
    stubWithSandbox(sandbox) {
        sandbox.stub(this.fs, 'mkdir').callsFake((dirPath) => {
            if (this.fileSystem[dirPath]) {
                return Promise.reject({ code: 'File exists' });
            }
            this.fileSystem[dirPath] = {};
            return Promise.resolve();
        });
        sandbox.stub(this.fs, 'existsSync').callsFake((filePath) => this.fileSystem[filePath]);
        sandbox.stub(this.fs, 'startsWithHttp').callsFake((input) => !!(input.startsWith('http://') || input.startsWith('https://')));
    }
    addFile(filePath) {
        this.fileSystem[filePath] = 'file';
    }
    editorListPathExists(path, unavailableEditorLocations) {
        return Promise.resolve(!unavailableEditorLocations.includes(path));
    }
}
module.exports = FileSystemMock;
//# sourceMappingURL=fileSystemMock.js.map