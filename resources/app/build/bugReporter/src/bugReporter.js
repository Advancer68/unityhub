const { app, BrowserWindow, Menu, systemPreferences, shell } = require('electron');
app.argv = require('yargs').argv;
class BugReporter {
    constructor() {
        this.browserWindow = null;
    }
    start() {
        app.on('ready', () => {
            this.createApplicationMenu();
            this.createWindow();
        });
        app.on('window-all-closed', () => {
            app.quit();
        });
    }
    createWindow() {
        const debug = !!app.argv.debugMode;
        this.browserWindow = new BrowserWindow({
            width: 800,
            height: 650,
            resizable: true,
            devTools: debug,
            webPreferences: {
                nodeIntegration: true
            }
        });
        if (debug === true)
            this.browserWindow.openDevTools();
        this.browserWindow.loadURL(`file://${__dirname}/client/index.html`);
        this.browserWindow.on('closed', () => {
            this.browserWindow = null;
        });
    }
    createApplicationMenu() {
        if (process.platform !== 'darwin') {
            Menu.setApplicationMenu(null);
        }
        else {
            const template = [
                {
                    label: app.getName(),
                    submenu: [
                        { role: 'hide', label: 'Hide' },
                        { role: 'hideothers' },
                        { type: 'separator' },
                        { role: 'quit', label: 'Quit' },
                    ],
                },
                {
                    label: 'Edit',
                    submenu: [
                        { role: 'undo' },
                        { role: 'redo' },
                        { type: 'separator' },
                        { role: 'cut' },
                        { role: 'copy' },
                        { role: 'paste' },
                        { role: 'pasteandmatchstyle' },
                        { role: 'delete' },
                        { role: 'selectall' },
                    ],
                },
                {
                    role: 'window',
                    submenu: [
                        { role: 'minimize' },
                        { role: 'zoom' },
                        { type: 'separator' },
                        { role: 'front' },
                    ],
                },
                {
                    role: 'help',
                    submenu: [
                        {
                            label: 'See Manual',
                            click() { shell.openExternal(' https://docs.unity.cn/cn/2019.4/Manual/GettingStartedUnityHub.html'); }
                        },
                    ],
                },
            ];
            const menu = Menu.buildFromTemplate(template);
            systemPreferences.setUserDefault('NSDisabledDictationMenuItem', 'boolean', true);
            systemPreferences.setUserDefault('NSDisabledCharacterPaletteMenuItem', 'boolean', true);
            Menu.setApplicationMenu(menu);
        }
    }
}
module.exports = new BugReporter();
//# sourceMappingURL=bugReporter.js.map