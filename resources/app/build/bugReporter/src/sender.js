var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { app } = require('electron');
const axios = require('axios');
const Archiver = require('./archiver');
const path = require('path');
const FormData = require('form-data');
const { fs, hubFS } = require('../../fileSystem');
const systemInfo = require('../../services/localSystemInfo/systemInfo');
const editorHelper = require('../../common/editor-helper');
class Sender {
    static get ARCHIVE_FILENAME() {
        return 'bugfiles.zip';
    }
    send({ files, title, description, email, timestamp, reproducibility }) {
        return __awaiter(this, void 0, void 0, function* () {
            const archiveFolderPath = hubFS.getTmpDirPath();
            yield fs.mkdirp(archiveFolderPath);
            files = files.concat(yield this.getHubLogs());
            this.archivePath = path.join(archiveFolderPath, Sender.ARCHIVE_FILENAME);
            yield Archiver.createArchive(files, this.archivePath);
            const computer = yield systemInfo.bugReporterSystemInfo();
            const form = new FormData();
            form.append('timestamp', timestamp);
            form.append('sTitle', title);
            form.append('sCustomerEmail', email);
            form.append('sVersion', 'Hub');
            form.append('sEvent', description);
            form.append('sComputer', computer);
            form.append('sBugReproducibility', reproducibility);
            form.append('sBugTypeID', 12);
            form.append('File1', fs.createReadStream(this.archivePath), {
                filename: Sender.ARCHIVE_FILENAME,
                contentType: 'octet/stream',
            });
            try {
                yield axios.post('https://editorbugs.unity3d.com/submit_bug_api.cgi', form, {
                    headers: form.getHeaders(),
                });
            }
            catch (error) {
                throw new Error(`upload failed: ${error}`);
            }
            fs.remove(path.dirname(this.archivePath));
            this.archivePath = undefined;
        });
    }
    getHubLogs() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const hubLogPath = path.join(app.getPath('userData'), 'logs');
                const files = yield fs.readdir(hubLogPath);
                return files.map(filename => path.join(hubLogPath, filename));
            }
            catch (err) {
                return [];
            }
        });
    }
    getEditorLogs() {
        return __awaiter(this, void 0, void 0, function* () {
            const editorLogFiles = ['Editor.log', 'Editor-prev.log', 'upm.log'];
            const result = [];
            try {
                const editorLogPath = editorHelper.getEditorLogPath();
                for (let i = 0; i < editorLogFiles.length; i++) {
                    const logFilePath = path.join(editorLogPath, editorLogFiles[i]);
                    if (fs.existsSync(logFilePath)) {
                        result.push(logFilePath);
                    }
                }
            }
            catch (err) {
            }
            return result;
        });
    }
}
module.exports = new Sender();
//# sourceMappingURL=sender.js.map