var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const yazl = require('yazl');
const { fs } = require('../../fileSystem');
const path = require('path');
class Archiver {
    createArchive(files, archivePath) {
        return __awaiter(this, void 0, void 0, function* () {
            return new Promise((resolve) => {
                this._validateFiles(files);
                const archive = new yazl.ZipFile();
                for (const file of files) {
                    archive.addFile(file, path.basename(file));
                }
                archive.outputStream.pipe(fs.createWriteStream(archivePath)).on('close', () => {
                    resolve();
                });
                archive.end();
            });
        });
    }
    _validateFiles(files) {
        if (!Array.isArray(files)) {
            throw new TypeError();
        }
        for (const file of files) {
            fs.accessSync(file, fs.constants.R_OK);
        }
    }
}
module.exports = new Archiver();
//# sourceMappingURL=archiver.js.map