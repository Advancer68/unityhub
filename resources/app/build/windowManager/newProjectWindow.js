'use strict';
const BaseWindow = require('./baseWindow');
const path = require('path');
const rootPath = `file://${path.resolve(__dirname, '../../client/dist/index-new-project.html')}`;
class NewProjectWindow extends BaseWindow {
    get name() { return 'new-project-window'; }
    get pages() {
        return {
            CREATE: '/create',
        };
    }
    constructor(optionsOverride, parentWindow) {
        const options = Object.assign({
            width: 910,
            height: 491,
            minWidth: 910,
            minHeight: 491,
            center: true,
            resizable: true,
            fullscreen: false,
            fullscreenable: false,
            title: 'Unity Hub New Project',
            webPreferences: {
                textAreasAreResizable: false,
                preload: path.join(__dirname, 'preloads', 'mainWindowPreload.js'),
                nodeIntegration: true,
            },
        }, optionsOverride);
        super(rootPath, options, parentWindow);
        this.basePage = this.pages.CREATE;
    }
}
module.exports = NewProjectWindow;
//# sourceMappingURL=newProjectWindow.js.map