const remote = require('electron').remote;
const path = require('path');
const logger = remote.require(path.join(__dirname, '../../logger'))('Front-End');
window.onerror = function (message, source, lineno, colno, error) {
    logger.error(error.stack);
};
if (window.location.href.startsWith('https://www.facebook.com/')) {
    window.require = null;
}
if (window.location.href.indexOf('.qq.com/') > 0) {
    delete window.module;
}
//# sourceMappingURL=signInWindowPreload.js.map