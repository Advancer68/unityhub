'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const path = require('path');
const os = require('os');
const EventEmitter = require('events');
const { BrowserWindow, dialog, shell } = require('electron');
const postal = require('postal');
const _ = require('lodash');
const windowStateKeeper = require('electron-window-state');
const proxyHelper = require('../proxyHelper');
const logger = require('../logger')('BaseWindow');
const cloudAnalytics = require('../services/cloudAnalytics/cloudAnalytics');
const networkInterceptors = require('../services/localAuth/networkInterceptors');
class BaseWindow extends EventEmitter {
    get name() { return 'base-window'; }
    get pages() { return {}; }
    constructor(rootPath, options, parentWindow) {
        super();
        this.browserWindow = null;
        this.initOptions = options;
        this.rootPath = rootPath;
        this.parentWindow = parentWindow;
        this.basePage = '';
        this.webContentsEventHandlers = {};
        this.registerWebContentsEventHandler('will-navigate', (event, url) => {
            if (url.startsWith('file://')) {
                event.preventDefault();
            }
        });
        this.registerWebContentsEventHandler('new-window', (event, url) => {
            event.preventDefault();
            shell.openExternal(url, (error) => {
                logger.warn(`Failed to open ${url} in default browser.`, error);
                const newWindow = new BrowserWindow({ width: 1028 });
                newWindow.loadURL(url);
                event.newGuest = newWindow;
            });
        });
        this.registerWebContentsEventHandler('did-fail-load', (event, code, message, validatedUrl) => {
            logger.warn(`Failed to load page ${validatedUrl}. Error: ${code} - ${message}`);
            if (code === -106) {
                networkInterceptors.failedRequest();
            }
        });
    }
    openDevTools() {
        this.show();
        this.browserWindow.webContents.openDevTools({ detach: true });
    }
    reload() {
        if (this.browserWindow !== null) {
            this.browserWindow.webContents.reloadIgnoringCache();
        }
    }
    show(page, queryParameters = '') {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.browserWindow !== null) {
                this.browserWindow.show();
                this.browserWindow.focus();
                if (page) {
                    this.loadPage(page, queryParameters);
                }
                return;
            }
            const options = Object.assign({}, this.initOptions);
            if (this.title !== undefined) {
                options.title = this.title;
            }
            if (this.parentWindow && this.parentWindow.browserWindow && this.parentWindow.browserWindow.isVisible()) {
                const parentInfo = getParentSizeAndPosition(this.parentWindow.browserWindow);
                options.x = parentInfo.x + ((parentInfo.width - options.width) / 2);
                options.y = parentInfo.y + ((parentInfo.height - options.height) / 2);
                options.center = false;
            }
            const windowState = windowStateKeeper({
                defaultWidth: options.width,
                defaultHeight: options.height,
                file: `${this.name}.json`
            });
            options.minWidth = options.minWidth || 0;
            options.minHeight = options.minHeight || 0;
            options.x = windowState.x || options.x;
            options.y = windowState.y || options.y;
            options.width = windowState.width > options.minWidth ? windowState.width : options.minWidth;
            options.height = windowState.height > options.minHeight ? windowState.height : options.minHeight;
            if (os.platform() === 'linux') {
                options.icon = path.join(__dirname, '../../images/linux-icon/icon.png');
            }
            cloudAnalytics.windowEvent('Open', this.name);
            this.browserWindow = new BrowserWindow(options);
            windowState.manage(this.browserWindow);
            yield this.setupProxy();
            if (page) {
                this.loadPage(page, queryParameters);
            }
            else {
                this.loadPage(this.basePage, queryParameters);
            }
            this.browserWindow.on('page-title-updated', (e) => {
                e.preventDefault();
            });
            this.browserWindow.on('closed', () => {
                postal.publish({
                    channel: 'app',
                    topic: `${this.name}.change`,
                    data: {
                        state: 'all-closed',
                        from: 'browserWindow:closed'
                    }
                });
                this.browserWindow = null;
                cloudAnalytics.windowEvent('Close', this.name);
            });
            _.each(this.webContentsEventHandlers, (handlers, eventName) => {
                this.browserWindow.webContents.on(eventName, (...args) => {
                    handlers.forEach((handler) => {
                        handler(...args);
                    });
                });
            });
        });
    }
    setupProxy() {
        return new Promise((resolve, reject) => {
            let proxyRules = '';
            if (proxyHelper.http) {
                proxyRules += `http=${proxyHelper.http.stringWithoutAuth};`;
            }
            if (proxyHelper.https) {
                proxyRules += `https=${proxyHelper.https.stringWithoutAuth}`;
            }
            if (proxyRules === '') {
                return resolve();
            }
            return this.browserWindow.webContents.session.setProxy({ proxyRules }, (error) => {
                if (error) {
                    return reject(error);
                }
                return resolve();
            });
        });
    }
    showOpenFileDialog(options) {
        if (!this.browserWindow) {
            return Promise.reject();
        }
        if (options.properties.includes('openDirectory') && options.properties.includes('openFile')) {
            throw new Error('Cannot select both a file and a directory');
        }
        let fileType;
        if (options.properties.includes('openDirectory')) {
            fileType = 'directory';
        }
        else if (options.properties.includes('openFile')) {
            fileType = 'file';
        }
        else {
            throw new Error('No valid file type selection for dialog box');
        }
        return new Promise((resolve, reject) => {
            dialog.showOpenDialog(this.browserWindow, options, (filenames) => {
                if (filenames) {
                    const result = options.properties.includes('multiSelections') ? filenames : filenames[0];
                    resolve(result);
                }
                else {
                    reject(`No ${fileType} selected`);
                }
            });
        });
    }
    hide() {
        if (!this.browserWindow) {
            return;
        }
        this.browserWindow.hide();
        postal.publish({
            channel: 'app',
            topic: `${this.name}.change`,
            data: {
                state: 'all-closed',
                from: 'browserWindow:hide'
            }
        });
    }
    close() {
        if (this.browserWindow !== null) {
            this.browserWindow.close();
        }
    }
    isFocused() {
        return this.browserWindow !== null && this.browserWindow.isFocused();
    }
    isVisible() {
        return this.browserWindow !== null && this.browserWindow.isVisible();
    }
    loadPage(page, queryParameters = '') {
        if (!_.values(this.pages).includes(page)) {
            logger.warn(`Page ${page} is not among supported pages for window ${this.name}`);
        }
        else {
            this.loadFragmentURL(page, queryParameters);
        }
    }
    loadURL(url, queryParameters = '', options) {
        this.browserWindow.loadURL(appendQueryParameters(url, queryParameters), options);
    }
    loadFragmentURL(fragment, queryParameters = '') {
        let newFragment = fragment;
        if (!newFragment.startsWith('#!')) {
            if (newFragment.startsWith('#/')) {
                newFragment = newFragment.substring(1);
            }
            newFragment = `#!${newFragment}`;
        }
        let newUrl = this.rootPath + newFragment;
        newUrl = appendQueryParameters(newUrl, queryParameters);
        if (this.browserWindow.webContents.getURL() !== newUrl) {
            this.browserWindow.loadURL(newUrl);
        }
    }
    sendContent(eventName, ...data) {
        if (this.browserWindow && this.browserWindow.webContents) {
            this.browserWindow.webContents.send(eventName, ...data);
        }
    }
    getNativeWindowHandle() {
        if (this.browserWindow === null)
            return null;
        return this.browserWindow.getNativeWindowHandle();
    }
    registerWebContentsEventHandler(eventName, handler) {
        if (!this.webContentsEventHandlers[eventName]) {
            this.webContentsEventHandlers[eventName] = [];
        }
        this.webContentsEventHandlers[eventName].push(handler);
    }
    setTitle(title) {
        this.title = title;
        if (this.browserWindow === null)
            return;
        this.browserWindow.setTitle(title);
    }
}
function getParentSizeAndPosition(parentBrowserWindow) {
    const [x, y] = parentBrowserWindow.getPosition();
    const [width, height] = parentBrowserWindow.getSize();
    return { x, y, width, height };
}
function appendQueryParameters(url, parameters) {
    let newUrl = url;
    if (parameters) {
        newUrl += `?${parameters}`;
    }
    return newUrl;
}
module.exports = BaseWindow;
//# sourceMappingURL=baseWindow.js.map