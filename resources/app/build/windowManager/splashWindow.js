'use strict';
const BaseWindow = require('./baseWindow');
const postal = require('postal');
class SplashWindow extends BaseWindow {
    constructor(optionsOverride, parentWindow) {
        const options = Object.assign({
            width: 1280,
            height: 720,
            center: true,
            resizable: false,
            minimizable: false,
            maximizable: false,
            fullscreen: false,
            fullscreenable: false,
            frame: false,
            skipStateKipper: true,
            title: 'Unity Hub Splash Screen',
        }, optionsOverride);
        super('', options, parentWindow);
        this.registerEvents();
    }
    get name() { return 'main-window'; }
    loadPage(page, queryParameters = '') {
        this.loadURL(`https://connect.unity.cn/hub/splash-screen?${queryParameters}`, '', { userAgent: 'Chrome' });
    }
    loadFragmentURL() {
        throw new Error(`Cannot load fragment URL in window ${this.name}`);
    }
    registerEvents() {
        this.registerWebContentsEventHandler('page-title-updated', (e, title) => {
            if (title === 'close') {
                postal.publish({
                    channel: 'splash-screen',
                    topic: 'close',
                });
            }
        });
    }
}
module.exports = SplashWindow;
//# sourceMappingURL=splashWindow.js.map