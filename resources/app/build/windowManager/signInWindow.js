'use strict';
const path = require('path');
const _ = require('lodash');
const logger = require('../logger')('SignInWindow');
const BaseWindow = require('./baseWindow');
const cloudConfig = require('../services/cloudConfig/cloudConfig');
const i18nConfig = require('../services/i18nConfig/i18nConfig');
let baseSSOPath;
const windowSizes = {
    signIn: {
        width: 770,
        height: 542
    },
    createAccount: {
        width: 770,
        height: 542
    }
};
const generateURL = Symbol('generateURL');
class SignInWindow extends BaseWindow {
    get name() { return 'sign-in-window'; }
    get redirectUri() { return 'launcher://unity/'; }
    get pages() {
        return {
            SIGN_IN: 'sign-in',
            SIGN_IN_LICENSE: 'sign-in-license',
            SIGN_UP: 'sign-up',
        };
    }
    static init() {
        logger.info('Init');
        baseSSOPath = cloudConfig.urls.identity;
    }
    constructor(optionsOverride, parentWindow) {
        const options = Object.assign({
            width: 770,
            height: 542,
            minWidth: 770,
            minHeight: 542,
            center: true,
            resizable: false,
            minimizable: false,
            maximizable: false,
            fullscreen: false,
            fullscreenable: false,
            title: 'Unity Hub Sign In',
            webPreferences: {
                textAreasAreResizable: false,
                preload: path.join(__dirname, 'preloads', 'signInWindowPreload.js'),
                enableRemoteModule: false
            }
        }, optionsOverride);
        super('', options, parentWindow);
        this.basePage = this.pages.SIGN_IN;
        this.registerWebContentsEventHandler('will-navigate', (event, url) => {
            if (this.browserWindow) {
                if (url.indexOf('view=register') !== -1) {
                    this.browserWindow.setContentSize(windowSizes.createAccount.width, windowSizes.createAccount.height);
                }
                else {
                    this.browserWindow.setContentSize(windowSizes.signIn.width, windowSizes.signIn.height);
                }
            }
            if (url.startsWith(this.redirectUri)) {
                event.preventDefault();
                if (this.browserWindow) {
                    this.browserWindow.webContents.session.clearStorageData({ storages: ['cookies'] });
                }
                const queryString = url.substring(url.indexOf('?') + 1);
                this.emit('sign-in:complete', queryString);
            }
        });
        this.registerWebContentsEventHandler('did-fail-load', () => {
            this.emit('sign-in:did-fail-load');
        });
    }
    loadPage(page) {
        if (!_.values(this.pages).includes(page)) {
            logger.warn(`Page ${page} is not among supported pages for window ${this.name}`);
        }
        else {
            this.loadURL(this[generateURL](page), '', { userAgent: 'Chrome' });
        }
    }
    loadFragmentURL() {
        throw new Error(`Cannot load fragment URL in window ${this.name}`);
    }
    [generateURL](page) {
        const locale = i18nConfig.getLocale();
        let display = 'launcher';
        let isReg = false;
        switch (page) {
            case this.pages.SIGN_IN:
                break;
            case this.pages.SIGN_IN_LICENSE:
                display = 'launcher_license';
                break;
            case this.pages.SIGN_UP:
                display = 'launcher_license';
                isReg = true;
                break;
            default:
                throw new Error(`Invalid page for ${this.name}: ${page}`);
        }
        return `${baseSSOPath}/v1/oauth2/authorize?client_id=launcher&display=${display}&locale=${locale}&response_type=code&redirect_uri=${this.redirectUri}&is_reg=${isReg}`;
    }
}
module.exports = SignInWindow;
//# sourceMappingURL=signInWindow.js.map