'use strict';
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
const { dialog, shell } = require('electron');
const EventEmitter = require('events');
const localConfig = require('../services/localConfig/localConfig');
const cloudCore = require('../services/cloudCore/cloudCore');
const BaseWindow = require('./baseWindow');
const MainWindow = require('./mainWindow');
const SignInWindow = require('./signInWindow');
const OnboardingWindow = require('./onboardingWindow');
const NewProjectWindow = require('./newProjectWindow');
const SplashWindow = require('./splashWindow');
const { i18n } = require('../i18nHelper');
const postal = require('postal');
const splashScreenService = require('../services/splashScreen/splashScreenService');
const logger = require('../logger')('WindowManager');
const privateSymbols = {};
class WindowManager extends EventEmitter {
    constructor() {
        super();
        this.windowList = [];
        this.onboardingWindow = null;
        this.mainWindow = null;
        this.registerWindow('mainWindow', new MainWindow());
        this.registerWindow('onboardingWindow', new OnboardingWindow());
        this.registerWindow('signInWindow', new SignInWindow({}, this[privateSymbols.mainWindow]));
        this.registerWindow('newProjectWindow', new NewProjectWindow({}, this[privateSymbols.mainWindow]));
        this.registerWindow('splashWindow', new SplashWindow());
    }
    init() {
        logger.info('Init');
        this.postalInit();
        return SignInWindow.init();
    }
    postalInit() {
        postal.subscribe({
            channel: 'splash-screen',
            topic: 'close',
            callback: () => {
                if (this.splashScreenTimer) {
                    clearTimeout(this.splashScreenTimer);
                    this.splashWindow.close();
                    this.mainWindow.show();
                    splashScreenService.updateNextShowTime();
                }
            },
        });
        postal.subscribe({
            channel: 'app',
            topic: 'userInfo.changed',
            callback: data => {
                if (data.firstLanch) {
                    this.forceShowProperWindow(data.userInfo);
                }
            },
        });
    }
    registerWindow(name, instance) {
        if (!(instance instanceof BaseWindow)) {
            throw new Error(`Window ${name} is not a BaseWindow`);
        }
        const windowSymbol = Symbol(name);
        privateSymbols[name] = windowSymbol;
        this[windowSymbol] = instance;
        this.windowList.push(instance);
        const windowSpecificMethods = [
            'close',
            'getNativeWindowHandle',
            'hide',
            'isFocused',
            'isVisible',
            'loadFragmentURL',
            'loadPage',
            'openDevTools',
            'reload',
            'sendContent',
            'show',
            'showOpenFileDialog',
            'setTitle',
        ];
        this[name] = {
            pages: instance.pages
        };
        windowSpecificMethods.forEach((methodName) => {
            this[name][methodName] = this[windowSymbol][methodName].bind(instance);
        });
    }
    showOpenFileDialog(options) {
        if (options.properties.includes('openDirectory') && options.properties.includes('openFile')) {
            throw new Error('Cannot select both a file and a directory');
        }
        let fileType;
        if (options.properties.includes('openDirectory')) {
            fileType = 'directory';
        }
        else if (options.properties.includes('openFile')) {
            fileType = 'file';
        }
        else {
            throw new Error('No valid file type selection for dialog box');
        }
        return new Promise((resolve, reject) => {
            dialog.showOpenDialog(options, (filenames) => {
                if (filenames) {
                    const result = options.properties.includes('multiSelections') ? filenames : filenames[0];
                    resolve(result);
                }
                else {
                    reject(`No ${fileType} selected`);
                }
            });
        });
    }
    showSignInWindow(page) {
        if (localConfig.isSignInDisabled()) {
            logger.info('Sign in is not enabled in the environment');
            return;
        }
        const isSignInVisible = this.signInWindow.isVisible();
        this.signInWindow.show(page);
        if (isSignInVisible) {
            return;
        }
        this[privateSymbols.signInWindow].once('sign-in:complete', (queryString) => {
            this.emit('sign-in:complete', queryString, this[privateSymbols.signInWindow].redirectUri);
            this.signInWindow.close();
        });
        this[privateSymbols.signInWindow].once('sign-in:did-fail-load', () => {
            this.broadcastContent('sign-in.did-fail-load');
        });
    }
    showNewProjectWindow(editor) {
        logger.info(`New Project Window is called for editor ${editor.version}`);
        this.newProjectWindow.show(this.newProjectWindow.pages.CREATE, `editorVersion=${editor.version}`);
        const title = i18n.translate('WINDOW.TITLE_NEW_PROJECT', { version: editor.version });
        this.newProjectWindow.setTitle(title);
    }
    broadcastContent(...content) {
        this.windowList.forEach((window) => {
            window.sendContent(...content);
        });
    }
    openExternal(url, redirect = false) {
        return new Promise((fulfill, reject) => {
            function asyncOpen(link) {
                shell.openExternal(link, (err) => {
                    if (!err)
                        fulfill();
                    else
                        reject(`No application available to open url ${link}`);
                });
            }
            if (!redirect)
                asyncOpen(url);
            else {
                cloudCore.getAuthorizedUrl(url).then(asyncOpen).catch(() => {
                    logger.warn(`Bypassing login step url for ${url} failed`);
                    asyncOpen(url);
                });
            }
        });
    }
    showSplashWindow(username) {
        this.splashWindow.show('', `username=${encodeURIComponent(username)}`);
    }
    showProperWindow(user) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.mainWindow.isVisible()) {
                this.mainWindow.show();
                return;
            }
            if (this.splashWindow.isVisible()) {
                this.splashWindow.show();
                return;
            }
            const { userId, displayName } = user;
            if (userId) {
                const shouldShowSplashScreen = yield splashScreenService.shouldShow();
                if (shouldShowSplashScreen) {
                    this.splashWindow.show('', `username=${encodeURIComponent(displayName)}`);
                    this.splashScreenTimer = setTimeout(() => {
                        this.splashWindow.close();
                        this.mainWindow.show();
                    }, 7000);
                    return;
                }
            }
            this.mainWindow.show();
        });
    }
    forceShowProperWindow(user) {
        return __awaiter(this, void 0, void 0, function* () {
            if (this.splashWindow.isVisible()) {
                this.splashWindow.show();
                return;
            }
            const { userId, displayName } = user;
            if (userId) {
                const shouldShowSplashScreen = yield splashScreenService.shouldShow();
                if (shouldShowSplashScreen) {
                    this.splashWindow.show('', `username=${encodeURIComponent(displayName)}`);
                    if (this.mainWindow.isVisible()) {
                        this.mainWindow.hide();
                    }
                    this.splashScreenTimer = setTimeout(() => {
                        this.splashWindow.close();
                        this.mainWindow.show();
                    }, 7000);
                    return;
                }
            }
            this.mainWindow.show();
        });
    }
}
module.exports = new WindowManager();
//# sourceMappingURL=windowManager.js.map