'use strict';
const _ = require('lodash');
const sinon = require('sinon');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinonChai = require('sinon-chai');
const proxyquire = require('proxyquire').noPreserveCache();
const expect = chai.expect;
chai.use(chaiAsPromised);
chai.use(sinonChai);
const cloudConfig = require('../services/cloudConfig/cloudConfig');
const BaseWindow = require('./baseWindow');
const SignInWindow = require('./signInWindow');
describe('WindowManager', () => {
    let sandbox;
    let baseSSOPath, options, parentWindow, signInWindow;
    let webContentsEventCallbacks;
    beforeEach(() => {
        baseSSOPath = 'http://hohoho.net';
        options = { awesome: 'sauce' };
        parentWindow = sinon.createStubInstance(BaseWindow);
        sandbox = sinon.sandbox.create();
        sandbox.stub(cloudConfig, 'urls').get(() => ({ identity: baseSSOPath }));
        webContentsEventCallbacks = {};
        sandbox.stub(BaseWindow.prototype, 'registerWebContentsEventHandler').callsFake((event, callback) => {
            webContentsEventCallbacks[event] = callback;
        });
        SignInWindow.init();
        signInWindow = new SignInWindow(options, parentWindow);
    });
    afterEach(() => {
        sandbox.restore();
    });
    it('should set the base page to the sign in page', () => {
        expect(signInWindow.basePage).to.equal(signInWindow.pages.SIGN_IN);
    });
    it('should bind to the "will-navigate" event', () => {
        expect(signInWindow.registerWebContentsEventHandler).to.have.been.calledWith('will-navigate');
    });
    it('should bind to the "did-fail-load" event', () => {
        expect(signInWindow.registerWebContentsEventHandler).to.have.been.calledWith('did-fail-load');
    });
    describe('on "will-navigate" webContents event', () => {
        let event, url, clearStorageStub;
        beforeEach(() => {
            event = {
                preventDefault: sandbox.stub()
            };
            clearStorageStub = sandbox.stub();
            signInWindow.browserWindow = {
                webContents: { session: { clearStorageData: clearStorageStub } },
                setContentSize: sandbox.stub()
            };
            sandbox.stub(signInWindow, 'emit');
        });
        describe('when url starts with "launcher://unity/', () => {
            let queryParameters;
            beforeEach(() => {
                queryParameters = 'foo=bar&x=1';
                url = `launcher://unity/hohoh?${queryParameters}`;
                webContentsEventCallbacks['will-navigate'](event, url);
            });
            it('should prevent event propagation', () => {
                expect(event.preventDefault).to.have.been.called;
            });
            it('should clear browserWindow storage', () => {
                expect(clearStorageStub).to.have.been.calledWith(sinon.match({ storages: ['cookies'] }));
            });
            it('should emit completion event and pass url\'s query parameters', () => {
                expect(signInWindow.emit).to.have.been.calledWith('sign-in:complete', queryParameters);
            });
        });
        describe('when url does not start with "launcher://unity/', () => {
            beforeEach(() => {
                url = 'lawnncher://unit/foo/bar';
                webContentsEventCallbacks['will-navigate'](event, url);
            });
            it('should not prevent event propagation', () => {
                expect(event.preventDefault).not.to.have.been.called;
            });
            it('should not clear browserWindow storage', () => {
                expect(clearStorageStub).not.to.have.been.called;
            });
            it('should not emit completion event', () => {
                expect(signInWindow.emit).not.to.have.been.called;
            });
        });
    });
    describe('on "did-fail-load" webContents event', () => {
        beforeEach(() => {
            sandbox.stub(signInWindow, 'emit');
            sandbox.stub(signInWindow, 'close');
            webContentsEventCallbacks['did-fail-load']();
        });
        it('should emit a failure event', () => {
            expect(signInWindow.emit).to.have.been.calledWith('sign-in:did-fail-load');
        });
    });
    describe('loadPage', () => {
        let page;
        beforeEach(() => {
            page = 'foo';
            sandbox.stub(signInWindow, 'loadURL');
        });
        it('should not load page if page is not valid', () => {
            signInWindow.loadPage(page);
            expect(signInWindow.loadURL).not.to.have.been.called;
        });
        it('should load launcher sign in url properly formatted if page is "sign in"', () => {
            page = signInWindow.pages.SIGN_IN;
            signInWindow.loadPage(page);
            const url = signInWindow.loadURL.lastCall.args[0];
            expect(url).to.contain(baseSSOPath);
            expect(url).to.contain('display=launcher');
            expect(url).to.contain('redirect_uri=launcher://unity/');
        });
        it('should load launcher license sign in url properly formatted if page is "sign in license"', () => {
            page = signInWindow.pages.SIGN_IN_LICENSE;
            signInWindow.loadPage(page);
            const url = signInWindow.loadURL.lastCall.args[0];
            expect(url).to.contain(baseSSOPath);
            expect(url).to.contain('display=launcher_license');
            expect(url).to.contain('redirect_uri=launcher://unity/');
        });
    });
    describe('loadFragmentURL', () => {
        it('should throw Error', () => {
            expect(signInWindow.loadFragmentURL).to.throw(Error);
        });
    });
});
//# sourceMappingURL=signInWindow.spec.js.map