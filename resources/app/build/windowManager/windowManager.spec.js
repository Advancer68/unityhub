'use strict';
const _ = require('lodash');
const sinon = require('sinon');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(chaiAsPromised);
chai.use(sinonChai);
const { dialog, shell } = require('electron');
const { i18n } = require('../i18nHelper');
const BaseWindow = require('./baseWindow');
const windowManager = require('./windowManager');
const localConfig = require('../services/localConfig/localConfig');
const cloudcoreLib = require('../services/cloudCore/cloudCore');
describe('WindowManager', () => {
    let sandbox;
    beforeEach(() => {
        sandbox = sinon.sandbox.create();
    });
    afterEach(() => {
        sandbox.restore();
    });
    describe('registerWindow', () => {
        let instance, name;
        class BaseWindowSubClass extends BaseWindow {
            get pages() { return { FOO: 'foo', BAR: 'bar' }; }
        }
        beforeEach(() => {
            name = 'subClass';
            instance = new BaseWindowSubClass();
            windowManager.registerWindow(name, instance);
            sandbox.stub(localConfig, 'isSignInDisabled').returns(false);
        });
        it('should add the window to the windowManager API', () => {
            expect(windowManager[name]).to.be.defined;
        });
        it('should expose window pages to the windowManager', () => {
            expect(windowManager[name].pages.FOO).to.equal(instance.pages.FOO);
            expect(windowManager[name].pages.BAR).to.equal(instance.pages.BAR);
        });
        it('should expose key API through ', () => {
            expect(windowManager[name].close).to.be.defined;
            expect(windowManager[name].hide).to.be.defined;
            expect(windowManager[name].loadPage).to.be.defined;
            expect(windowManager[name].show).to.be.defined;
        });
    });
    describe('showOpenFileDialog', () => {
        let options;
        beforeEach(() => {
            options = {
                properties: ['openFile']
            };
            sandbox.stub(dialog, 'showOpenDialog');
            sandbox.stub(localConfig, 'isSignInDisabled').returns(false);
        });
        it('should throw if both file and directory are selected as file types', () => {
            options.properties.push('openDirectory');
            expect(() => windowManager.showOpenFileDialog(options)).to.throw(Error);
        });
        it('should throw if no file type has been selected', () => {
            options.properties = [];
            expect(() => windowManager.showOpenFileDialog(options)).to.throw(Error);
        });
        it('should not throw if file type is openDirectory', () => {
            options.properties = ['openDirectory'];
            dialog.showOpenDialog.callsFake((options, callback) => {
                callback(['hohoh']);
            });
            return expect(() => windowManager.showOpenFileDialog(options)).not.to.throw(Error);
        });
        it('should open a dialog box to have the user select files or folders', () => {
            dialog.showOpenDialog.callsFake((options, callback) => {
                callback(['hohoh']);
            });
            return windowManager.showOpenFileDialog(options)
                .then(() => {
                expect(dialog.showOpenDialog).to.have.been.calledWith(options);
            });
        });
        it('should resolve with single file if user selected file', () => {
            const filenames = ['bob'];
            dialog.showOpenDialog.callsFake((options, callback) => {
                callback(filenames);
            });
            return windowManager.showOpenFileDialog(options)
                .then((result) => {
                expect(result).to.eql(filenames[0]);
            });
        });
        it('should resolve with selected files if user selected files and multiSelection was enabled', () => {
            options.properties.push('multiSelections');
            const filenames = ['bob', 'bill', 'doe'];
            dialog.showOpenDialog.callsFake((options, callback) => {
                callback(filenames);
            });
            return windowManager.showOpenFileDialog(options)
                .then((result) => {
                expect(result).to.eql(filenames);
            });
        });
        it('should reject if user did not select files', () => {
            dialog.showOpenDialog.callsFake((options, callback) => {
                callback();
            });
            return expect(windowManager.showOpenFileDialog(options)).to.be.rejected;
        });
    });
    describe('showNewProjectWindow', () => {
        let newProjectWindowInstance, homePage;
        beforeEach(() => {
            homePage = '';
            newProjectWindowInstance = sinon.createStubInstance(BaseWindow);
            windowManager.registerWindow('newProjectWindow', newProjectWindowInstance);
            windowManager.mainWindow = {
                show: sinon.stub(),
                pages: { HOME: homePage },
            };
            i18n.translate = sandbox.stub().returns('string');
        });
        it('should open the new project window', () => {
            windowManager.showNewProjectWindow({ version: '2017.2' });
            expect(newProjectWindowInstance.show).to.have.been.called;
        });
    });
    describe('showSignInWindow', () => {
        let signInWindowInstance, loginPage;
        beforeEach(() => {
            loginPage = '#/logue-in';
            signInWindowInstance = sinon.createStubInstance(BaseWindow);
            windowManager.registerWindow('signInWindow', signInWindowInstance);
            windowManager.mainWindow = {
                show: sinon.stub(),
                pages: { LOGIN: loginPage },
            };
            windowManager.broadcastContent = sandbox.stub();
        });
        describe('in normal case', () => {
            beforeEach(() => {
                sandbox.stub(localConfig, 'isSignInDisabled').returns(false);
            });
            it('should bring the signInWindow to front when visible', () => {
                signInWindowInstance.isVisible.returns(true);
                windowManager.showSignInWindow();
                expect(signInWindowInstance.show).to.have.been.called;
            });
            it('should not bind to sign-in:complete when visible', () => {
                signInWindowInstance.isVisible.returns(true);
                windowManager.showSignInWindow();
                expect(signInWindowInstance.once).not.to.have.been.called;
            });
            it('should bring the signInWindow to front when not visible', () => {
                signInWindowInstance.isVisible.returns(false);
                windowManager.showSignInWindow();
                expect(signInWindowInstance.show).to.have.been.called;
            });
            it('should bind to the sign-in:complete event when not visible', () => {
                signInWindowInstance.isVisible.returns(false);
                windowManager.showSignInWindow();
                expect(signInWindowInstance.once).to.have.been.calledWith('sign-in:complete');
            });
        });
        describe('when sign-in completes', () => {
            let queryString;
            beforeEach(() => {
                queryString = { foo: 'bar' };
                signInWindowInstance.isVisible.returns(false);
                signInWindowInstance.once.callsFake((event, callback) => {
                    if (event === 'sign-in:complete') {
                        callback(queryString);
                    }
                });
                sandbox.stub(windowManager, 'emit');
                sandbox.stub(localConfig, 'isSignInDisabled').returns(false);
                windowManager.showSignInWindow();
            });
            it('should transfer sign in event and pass querystring and redirect uri', () => {
                expect(windowManager.emit).to.have.been.calledWith('sign-in:complete', queryString, signInWindowInstance.redirectUri);
            });
            it('should close signInWindow', () => {
                expect(signInWindowInstance.close).to.have.been.called;
            });
        });
        describe('when sign-in failed to load', () => {
            beforeEach(() => {
                sandbox.stub(localConfig, 'isSignInDisabled').returns(false);
                signInWindowInstance.once.callsFake((event, callback) => {
                    if (event === 'sign-in:did-fail-load') {
                        callback();
                    }
                });
                windowManager.showSignInWindow();
            });
            it('should send notification IPC to main window', () => {
                expect(windowManager.broadcastContent).to.have.been.calledWith('sign-in.did-fail-load');
            });
        });
        describe('when isSignInDisabled set to true', () => {
            beforeEach(() => {
                sandbox.stub(localConfig, 'isSignInDisabled').returns(true);
                windowManager.showSignInWindow();
            });
            it('should not show the window', () => {
                expect(signInWindowInstance.show).to.have.not.been.called;
            });
        });
    });
    describe('openExternal', () => {
        let shellStub, openExternalErr = null;
        const url = 'www.unity3d.com';
        beforeEach(() => {
            shellStub = sandbox.stub(shell, 'openExternal').callsFake((url, cb) => {
                cb(openExternalErr);
            });
        });
        describe('when no redirect', () => {
            it('should open URL', () => {
                return windowManager.openExternal(url).then(() => {
                    expect(shellStub).to.have.been.calledWith(url);
                });
            });
        });
        describe('when redirect', () => {
            it('should open redirect URL when it succeed to fetch it', () => {
                const redirectUrl = 'https://redirected.cash';
                sandbox.stub(cloudcoreLib, 'getAuthorizedUrl').resolves(redirectUrl);
                return windowManager.openExternal(url, true).then(() => {
                    expect(shellStub).to.have.been.calledWith(redirectUrl);
                });
            });
            it('should open URL if it fails to get redirect URL', () => {
                sandbox.stub(cloudcoreLib, 'getAuthorizedUrl').callsFake(() => Promise.reject());
                return windowManager.openExternal(url, true).then(() => {
                    expect(shellStub).to.have.been.calledWith(url);
                });
            });
            it('should reject if there\'s no application to open the url', () => {
                openExternalErr = new Error('bad!');
                return expect(windowManager.openExternal(url))
                    .to.be.rejectedWith(`No application available to open url ${url}`);
            });
        });
    });
});
//# sourceMappingURL=windowManager.spec.js.map