import { argv } from 'yargs';
import os from 'os';
import windowManager from './windowManager/windowManager';
import unityHubProtocolHandler from './unityhubProtocolHandler';
import cloudAnalytics from './services/cloudAnalytics/cloudAnalytics';
import BaseBootstrap from './bootstrapSequences/baseBootstrap';
import FileAssociationBootstrap from './bootstrapSequences/fileAssociationBootstrap';
import InstallServiceBootstrap from './bootstrapSequences/installServiceBootstrap';
import SecondAppInstanceBootstrap from './bootstrapSequences/secondAppInstanceBootstrap';
import BugReporterBootStrap from './bootstrapSequences/bugReporterBootstrap';
import CliBootStrap from './bootstrapSequences/cliBootstrap';
import onboardingService from './services/onboarding/onboardingService';

export default class Bootstrap {
  private isSecondAppInstance = null;

  constructor(electronApp) {
    if (argv.headless) return;

    this.isSecondAppInstance = !(electronApp.requestSingleInstanceLock());

    electronApp.on('second-instance', async () => {
      if (onboardingService.hasOnboarding) {
        const onboardingState = await onboardingService.getOnboardingState();
        if (onboardingState.isFinished === false) {
          windowManager.onboardingWindow.show();
          return;
        }
      }
      windowManager.mainWindow.show();
    });
  }

  public async start(): Promise<void> {
    let bootstrapSequence = BaseBootstrap;

    if (this.isInstallerProcess()) {
      bootstrapSequence = InstallServiceBootstrap;
    } else if (argv.bugReporter) {
      bootstrapSequence = BugReporterBootStrap;
    } else if (argv.headless) {
      bootstrapSequence = CliBootStrap;
    } else if (this.isSecondAppInstance) {
      await cloudAnalytics.quitEvent('Duplicate');

      if (this.isFileAssociationProcess()) {
        bootstrapSequence = FileAssociationBootstrap;
      } else {
        bootstrapSequence = SecondAppInstanceBootstrap;
      }
    }

    bootstrapSequence.start();
  }

  private isInstallerProcess(): boolean {
    return argv.winInstaller && os.platform() === 'win32';
  }

  private isFileAssociationProcess(): boolean {
    let hasCustomProtocolURL = false;

    process.argv.forEach((arg) => {
      if (arg.includes(unityHubProtocolHandler.fullProtocolName)) {
        hasCustomProtocolURL = true;
      }
    });

    return hasCustomProtocolURL;
  }
}
