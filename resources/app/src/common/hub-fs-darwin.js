const sudo = require('sudo-prompt');
// eslint-disable-next-line import/no-unresolved
const flock = require('fs-ext').flock;
const promisify = require('es6-promisify');
const { exec } = require('child_process');
const path = require('path');

const fs = require('fs-extra');

const flockAsync = promisify(flock);

const unityHubFS = {

  RMRF_CMD: 'rm -rf',

  /**
   * Prompt user for su access and make given dirPath
   * @param dirPath New folder to create which require super user privileges
   * @param mode The Unix mode (permissions), default to 744: rwxr--r--
   * @returns {Promise}
   */
  elevateAndMakeDir(dirPath, mode = 744) {
    return new Promise((resolve, reject) => {
      sudo.exec(`mkdir -p -m ${mode} ${dirPath}`, { name: 'Unity Hub' }, (error) => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    });
  },

  /**
   * Move the files of a folder up one level. Returns a promise
   * @param dir
   * @returns {Promise}
   */
  moveFilesToParent(dir) {
    return new Promise((resolve, reject) => {
      exec(`rsync -a "${dir}/" "${dir}/.."`, {}, (error) => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    });

  },

  /**
   * Given a file path, check if there is an exclusive lock on it
   * In OSX, being able to write in a file doesn't mean that there is no lock on it.
   * Here we try to open the file, lock it and unlock it.
   * If we fail to lock the file, it means there is another process (probably another editor in case of UnityLockFile) is using this file (project)
   * @param filePath
   * @returns {Promise}
   */
  isFileUnlocked(filePath) {
    return new Promise((resolve, reject) => {
      fs.open(filePath, 'w')
        .then((fd) => flockAsync(fd, 'exnb')
          .then(() => flockAsync(fd, 'un')
            .then(resolve)
            // unlock our own lock
            .catch(resolve)))
        .catch(reject);
    });
  },

  /**
   * Returns the base Volume path
   * @param folder
   * @returns {string} The Volume name
   */
  getDiskRootPath(folder) {

    if (!folder || folder === '') {
      throw Error('Invalid path given');
    }

    // When we are on another volume,
    // make sure to return the base volume path
    // ex.: /Volumes/Macintosh HD/
    if (folder.startsWith('/Volumes/')) {
      return folder.substring(0, folder.indexOf('/', 9) + 1);
    }

    return path.parse(folder).root;
  },
  /**
   * Check if path/file is in recycle bin
   * @param {string} dirPath apsolute path
   * @returns {Promise<boolean>}
   */
  isInRecycleBin(dirPath) {
    return new Promise((resolve, reject) => {
      try {
        resolve(!!dirPath.match(/.Trash/gi));
      } catch (e) {
        reject(e);
      }
    });
  },
  /**
   * Get the editor folder path from the executable path
   * @param execPath
   */
  getEditorFolderFromExecutable(execPath) {
    return path.dirname(execPath);
  }
};

module.exports = unityHubFS;
