const sudo = require('sudo-prompt');
const path = require('path');
const fs = require('fs-extra');

const unityHubFS = {

  RMRF_CMD: 'rd /q /s',

  /**
   * Prompt user for su access and make given dirPath
   * @param dirPath New folder to create which require super user privileges
   * @returns {Promise}
   */
  elevateAndMakeDir(dirPath) {
    return new Promise((resolve, reject) => {
      sudo.exec(`mkdir "${dirPath}"`, { name: 'Unity Hub' }, (error) => {
        if (error) {
          reject(error);
        } else {
          resolve();
        }
      });
    });
  },

  /**
   * Given a file path, check if there is an exclusive lock on it
   * In
   * @param filePath
   */
  isFileUnlocked(filePath) {
    return fs.open(filePath, 'w').then((fd) => {
      fs.close(fd);
    });

  },

  /**
   * Returns the drive letter
   * @returns {string} The drive letter: C:\\
   * @param folder
   */
  getDiskRootPath(folder) {
    if (!folder || folder === '') {
      throw Error('Invalid path given');
    }

    return path.parse(folder).root;
  },

  /**
   * Check if path/file is in recycle bin
   * @param {string} dirPath apsolute path
   * @returns {Promise<boolean>} 
   */
  isInRecycleBin(dirPath) {
    return new Promise((resolve, reject) => {
      try {
        resolve(!!dirPath.match(/\$RECYCLE.BIN/gi));
      } catch (e) {
        reject(e);
      }
    });
  },
  /**
   * Get the editor folder path from the executable path
   * @param {string} the editor executable path
   */
  getEditorFolderFromExecutable(execPath) {
    return path.dirname(path.dirname(execPath));
  }

};

module.exports = unityHubFS;
