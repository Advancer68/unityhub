const hubUtils = {
  checkNestedProperties(obj, ...args) {
    for (let i = 0; i < args.length; i++) {
      if (!obj || !Object.prototype.hasOwnProperty.call(obj, args[i])) {
        return false;
      }
      obj = obj[args[i]];
    }
    return true;
  }
};

module.exports = hubUtils;
