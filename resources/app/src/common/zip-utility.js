const yauzl = require('yauzl');
const path = require('path');
const EventEmitter = require('events');
const request = require('request');
const platform = require('os').platform();

const { fs, hubFS } = require('../fileSystem');

class ZipUtility extends EventEmitter {

  getUncompressedSize(zipPath) {
    if (hubFS.startsWithHttp(zipPath)) {
      return getHttpUncompressedSize(zipPath);
    }

    return getLocalUncompressedSize(zipPath);
  }

  unzip(zipPath, destination) {
    // read the entries one at a time (lazyEntries) to avoid
    // filling the memory
    return new Promise((resolve, reject) => {
      yauzl.open(zipPath, { lazyEntries: true }, (err, zipFile) => {
        if (err) {
          reject(err);
          return;
        }
        zipFile.readEntry();
        zipFile.on('entry', (entry) => {
          if (/\/$/.test(entry.fileName)) {
            // Directory file names end with '/'
            mkdirp(path.join(destination, entry.fileName))
              .then(() => zipFile.readEntry())
              .catch((e) => reject(e));
          } else {
            onFileEntry.bind(this)(reject, entry, destination, zipFile);
          }
        });
        zipFile.on('error', (e) => reject(e));
        zipFile.on('end', () => resolve()); // all files were read
      });
    });
  }
}

function onFileEntry(reject, entry, destination, zipfile) {
  // file entry
  mkdirp(path.join(destination, path.dirname(entry.fileName)))
    .then(() => {
      zipfile.openReadStream(entry, (e, readStream) => {
        if (e) {
          reject(e);
          return;
        }
        readStream.on('end', () => {
          zipfile.readEntry();
        });

        let writeStream;
        const outputPath = path.join(destination, entry.fileName);
        if (platform === 'linux') {
          // Writing file with same access permissions: https://github.com/thejoshwolfe/yauzl/issues/101
          const mode = getEntryMode(entry);
          writeStream = fs.createWriteStream(outputPath, { mode });
        } else {
          writeStream = fs.createWriteStream(outputPath);
        }
        readStream.pipe(writeStream);
      });
    })
    .catch((e) => reject(e));
}

function getLocalUncompressedSize(zipPath) {
  return new Promise((resolve, reject) => {
    let uncompressedSize = 0;
    yauzl.open(zipPath, { lazyEntries: true }, (err, zipFile) => {
      if (err) {
        reject(err);
        return;
      }
      zipFile.readEntry();
      zipFile.on('entry', (entry) => {
        if (!/\/$/.test(entry.fileName)) { // not a folder
          zipFile.openReadStream(entry, (e) => {
            if (e) {
              reject(e);
              return;
            }
            uncompressedSize += entry.uncompressedSize;
            zipFile.readEntry();
          });
        } else {
          zipFile.readEntry();
        }
      });
      zipFile.on('error', (e) => {
        reject(e);
      });
      zipFile.on('end', () => {
        resolve(uncompressedSize); // all files were read
      });
    });
  });
}

/**
 * This function gives an approximation of the uncompressed size
 * based on compression benchmarks:
 *                                    Compression ratio (%)
 * ZIP, IZArc 4.1.6                       51.1
 * ZIP, PeaZip 3.8 (7z 9.22 64 bit)       50.8
 * ZIP, WinRar 4.01 64 bit                51.5
 * ZIP, WinZip 15                         51.5
 * RAR, WinRar 4.01 64 bit                46.0
 * ZIPX, WinZip 15                        46.0
 *
 * We use the ratio 2.04 (51.1%), which is what seems to be the case with Unity's compressed files
 * @param zipPath
 * @return {Promise}
 */
function getHttpUncompressedSize(zipPath) {
  return new Promise((resolve, reject) => {
    request.head(zipPath)
      .on('response', (res) => {
        if (res.statusCode === 200) {
          resolve(Math.round(res.headers['content-length'] * 2.04));
          return;
        }
        reject('invalid url');
      })
      .on('error', (e) => reject(e));
  });
}

function mkdirp(dir) {
  if (dir === '.' || dir === '..') {
    return Promise.resolve();
  }

  return fs.ensureDir(dir)
    .catch(err => {
      throw err;
    });
}

// Returns the octal representation of the file mode of a zip entry.
function getEntryMode(entry) {
  // This solution works for ZIP files created by UNIX-like systems.
  // The unix stat mode is stored in the upper 16 bits of an entry's external file attributes.
  // Based on https://github.com/thejoshwolfe/yauzl/issues/102
  return entry.externalFileAttributes >>> 16;
}

module.exports = new ZipUtility();
