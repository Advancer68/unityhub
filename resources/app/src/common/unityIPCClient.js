const ipc = require('node-ipc');
const unityIPCNameFormatter = require('./unityIPCNameFormatter');
const logger = require('../logger')('UnityIPCClient');

class UnityIPCClient {
  constructor(serverName) {
    this.name = unityIPCNameFormatter.formatName(serverName);
    this.ipc = new ipc.IPC();
  }
  
  async connect() {
    logger.debug(`connecting to IPC Server ${this.name}`);
    this.ipc.config.id = this.name;
    this.ipc.config.retry = 1500;
    this.ipc.config.silent = true;
    
    await this._connectToIPCServer();
    
    this.on('connect', () => {
      this.serverLog('connected');
    });
  
    this.on('disconnect', () => {
      logger.debug(`disconnected from ${this.name}`);
    });
  }
  
  _connectToIPCServer() {
    return new Promise(resolve => {
      this.ipc.connectTo(this.name, this.name, () => {
        resolve();
      });
    });
  }
  
  on(eventName, callback) {
    this.ipc.of[this.name].on(eventName, callback);
  }
  
  emit(eventName, data) {
    this.ipc.of[this.name].emit(eventName, data);
  }
  
  serverLog(message) {
    this.ipc.of[this.name].emit('message', message);
  }
}

module.exports = UnityIPCClient;
