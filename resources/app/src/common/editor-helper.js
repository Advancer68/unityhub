const os = require('os');
const path = require('path');
const psnode = require('ps-node');
const promisify = require('es6-promisify');

const editorHelperPlatform = require(`./editor-helper-${os.platform()}`);

const editorHelper = {
  getUserDataPath() {
    return path.join(this.getBaseUserDataPath(), this.getUnityFolderInUserData());
  },
  getUnityFolderInUserData() {
    // Platform dependant.
  },
  getBaseUserDataPath() {
    // Platform dependant.
  },
  getEditorLogPath() {
    // Platform dependant.
  },
  getDefaultAssetStoreFolderName() {
    return 'Asset Store-5.x';
  },
  getDefaultAssetStorePath() {
    return path.join(this.getUserDataPath(), this.getDefaultAssetStoreFolderName());
  },

  // overridden for win-32 (see editor-helper-win32.js)
  isVersionRunning({ version }) {

    if (typeof version !== 'string' || version.trim().length === 0) {
      return Promise.resolve(false);
    }

    return promisify(psnode.lookup)({ psargs: '-x -opid,ppid,command', command: /(unity\.exe|unity)$/i })
      .then((procList) => {
        for (const proc of procList) {
          if (new RegExp(version).test(proc.command) === true) {
            return true;
          }
        }
        return false;
      })
      .catch(() => false);
  }
};

module.exports = Object.assign(editorHelper, editorHelperPlatform);
