const os = require('os');
const path = require('path');
const _ = require('lodash');
const uuid = require('uuid/v1');
const { exec } = require('child_process');
const request = require('request-promise-native');
const disk = require('diskusage');
const fs = require('fs-extra');

const hubfsPlatform = require(`./hub-fs-${os.platform()}`);

function getNextSuffix(homonyms) {
  let nextNum = 1;
  homonyms.forEach((homonym) => {
    const suffix = homonym.match(/(?:\((\d+)\))$/);
    if (suffix !== null && suffix[1] !== undefined) {
      const index = parseInt(suffix[1], 10);
      if (index >= nextNum) {
        nextNum = index + 1;
      }
    }
  });
  return nextNum;
}

const hubfs = {

  fakeUserAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
  filenameFromUrl(url) {
    let result;
    const regExResult = /.*\/([^?]*)(\?.*)?$/.exec(url);
    if (regExResult !== null) {
      result = regExResult[1];
    }
    return result;
  },

  hasExtension(url) {
    return /^.*\.[^\\/]+$/.test(url);
  },

  /**
   * We fake a chrome user agent because some links require
   * a user agent of a valid browser set to actually work
   * (facebook SDK)
   *
   * @param url
   */
  getFinalRedirectUrl(url) {
    const options = {
      uri: url,
      method: 'HEAD',
      followAllRedirects: true,
      followOriginalHttpMethod: true,
      resolveWithFullResponse: true,
      headers: {
        'User-Agent': this.fakeUserAgent
      }
    };

    return request(options)
      .then((response) => {
        // we want to get the final redirect url to know the 'real' filename
        // so, if the filename is specified in content-disposition, use that as the final redirect
        // but content disposition is not always standard
        // language packs = attachment; filename="ja.po"
        // Visual Studio for mac = attachment; filename=VisualStudioForMac-7.4.3.10.pkg
        if (response.headers['content-disposition'] && response.headers['content-disposition'].indexOf('filename=') >= 0) {
          if (response.headers['content-disposition'].indexOf('filename="') >= 0) {
            return `/${/.*filename="(.*?[^"])"/.exec(response.headers['content-disposition'])[1]}`;
          } else if (response.headers['content-disposition'].indexOf('filename=') >= 0) {
            return `/${/.*filename=(.*)/.exec(response.headers['content-disposition'])[1]}`;
          }
        }

        // otherwise, just use the last href
        return response.request.href;
      })
      .catch(() => url);
  },

  getTmpDirPath() {
    return path.join(os.tmpdir(), `unityhub-${uuid()}`);
  },

  async pathExists(basename = '', filename = '') {
    return fs.pathExists(path.join(basename, filename));
  },

  async mkdirRecursive(dirpath) {
    return fs.mkdirp(dirpath);
  },

  async readFile(filePath) {
    return fs.readFile(filePath);
  },

  isFilePresent(filePath) {
    try {
      fs.lstatSync(filePath);
      return true;
    } catch (error) {
      return error.code !== 'ENOENT';
    }
  },

  isFilePresentPromise(filePath) {
    return fs.stat(filePath)
      .catch((err) => {
        if (err.code === 'ENOENT') {
          return Promise.reject();
        }
        return Promise.resolve();
      });
  },

  getUniquePath(candidatePath) {
    try {
      const fileInfo = fs.statSync(candidatePath);

      if (fileInfo.isDirectory()) {
        const parentDir = path.dirname(candidatePath);
        const projectDirName = path.basename(candidatePath);

        // this filter keeps the basename of the candidate path AND all the basenames which are the same as the candidate
        // path's basename with a valid suffix (like myFolderName (1), myFolderName (18), ...)
        const homonyms = fs.readdirSync(parentDir).filter(name => name === path.basename(candidatePath) || (new RegExp(`${_.escapeRegExp(projectDirName)} (?:\\(\\d+\\))$`)).test(name));

        if (homonyms.length === 1) {
          return `${candidatePath} (1)`;
        }

        const nextSuffix = getNextSuffix(homonyms);
        return `${candidatePath} (${nextSuffix})`;
      }
      return candidatePath;
    } catch (error) {
      if (error.code === 'ENOENT') { // if the file does not exist, it's ok, we return the candidatePath
        return candidatePath;
      }
      throw error; // if it's an other kind of error, we propagate the exception
    }
  },

  removeFolder(folder) {
    if (folder) {
      if (folder === '') {
        return Promise.reject(`Folder not specified. Cannot remove folder (${folder}).`);
      }
      return new Promise((resolve, reject) => {
        exec(`${this.RMRF_CMD} "${folder}"`, { name: 'Remove folder' }, (error) => {
          if (error) {
            return reject(error);
          }
          return resolve();
        });
      });
    }
    return Promise.reject(`Folder not specified. Cannot remove folder (${folder}).`);
  },

  getDiskUsage(folder) {
    if (!folder) {
      return Promise.reject('Path not specified. Cannot verify disk usage.');
    }
    return new Promise((resolve, reject) => {
      disk.check(folder, (err, info) => {
        if (err) {
          reject(err);
        } else {
          resolve(info);
        }
      });
    });
  },

  /**
   * Given the file/folder name, this method will check if it is a valid name
   * Requirements are based on CheckValidFileNameDetail function in PathNameUtility.cpp in Editor's code base
   * @param input
   * @returns {boolean}
   */
  isValidFileName(input) {
    const INVALID_PATH_LETTERS = /[/?<>\\:*|"]/;

    // anything up to first dot can't be: com1..com9, lpt1..lpt9, con, nul, prn
    const INVALID_BEGINNING = /^((com|lpt)[1-9]{1}|con|nul|prn)\./;

    if ((typeof input !== 'string')
      || INVALID_PATH_LETTERS.test(input)
      || INVALID_BEGINNING.test(input)) {
      return false;
    }

    if (os.platform() !== 'linux' && input.startsWith('.')) {
      return false; // todo move it to linux specific when we merged the linux PR
    }

    // file/folder name can't end with a dot or a space
    // File names starting with a space will not get preserved correctly when zipping
    // on windows. So don't allow that.
    return !(input.endsWith('.') || input.endsWith(' ') || input.startsWith(' '));

  },

  /**
   * Given the path, this method will check if it is a valid path
   * Requirements are based on CreateProjectCallback function in HomeWindow.cpp in Editor's code base
   * @param input
   * @returns {boolean}
   */
  isValidPath(input) {
    if ((typeof input !== 'string') || input.search('//') !== -1) {
      return false;
    }

    input = path.normalize(input);

    const pathParts = input.split(path.sep);
    for (let i = 0; i < pathParts.length; i++) {
      const part = pathParts[i];
      if (!this.isValidFileName(part)) {
        // we accept e.g. "c:" for the first component
        if (!(i === 0 && part.length === 2 && part.charAt(1) === ':' && this.isValidFileName(part.substring(0, 1)))) {
          return false;
        }
      }
    }
    return true;
  },

  /**
   * Very naive approach to validate if the path is local or not.
   * @param input path to validate
   * @returns {boolean}
   */
  startsWithHttp(input) {
    return !!(input.startsWith('http://') || input.startsWith('https://'));
  },

  getDiskSpaceAvailable(folder) {
    return this.getDiskUsage(this.getDiskRootPath(folder));
  },
};

module.exports = Object.assign(hubfs, hubfsPlatform);
