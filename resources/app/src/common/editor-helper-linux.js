const os = require('os');

const editorHelper = {
  getUnityFolderInUserData() {
    return 'unity3d';
  },

  getBaseUserDataPath() {
    return `${os.homedir()}/.local/share`;
  },

  getEditorLogPath() {
    return `${os.homedir()}/.config/unity3d`;
  },
};

module.exports = editorHelper;
