const logger = require('../logger')('UnityIPCServerPlatform');
const errors = require('../services/localInstaller/errors.js');
const { exec, spawn } = require('child_process');
// eslint-disable-next-line import/no-unresolved
const winUtils = require('windows-registry').utils;

const UnityIPCServerPlatform = {
  /**
   * Spawn a js file as an elevated electron process.
   * @param fileToExecute (string) absolute path of the js file to execute
   * @returns {Promise}
   */
  spawnElevatedChildProcess(fileToExecute) {
    return new Promise((resolve, reject) => {
      logger.info(`spawn elevated child process to ${this.name}`);

      const appPath = `"${process.execPath}"`;

      let param = '-- --winInstaller';
      if (appPath.indexOf('electron') !== -1) {
        // during developement
        param = `"${fileToExecute}"`;
      }

      logger.info(`elevate ${appPath} ${param}`);
      winUtils.elevate(appPath, param, (err, result) => {
        if (result === true && !err) {
          // the user pressed YES in the UAC prompt
          logger.debug(`pressed yes to UAC prompt of ${this.name}`);

          if (this._isSocketInitialized()) {
            resolve();
          } else {
            this.on('connect', () => resolve());
          }

        } else if (err) {
          this.closeSockets();
          logger.error(err);
          reject(err);
        } else {
          this.closeSockets();
          logger.info(`the person did not agree to the ${this.name} UAC and the process was not started`);
          reject(errors.PERMISSION_DENIED);
        }
      });
    });
  },

  /**
   * Spawn a js file as a new electron process.
   * @param fileToExecute (string) absolute path of the js file to execute
   * @returns {Promise}
   */
  spawnChildProcess(fileToExecute) {
    return new Promise((resolve, reject) => {
      logger.info(`spawn child process to ${this.name}`);

      let command = process.execPath;
      let args = [];
      if (command.indexOf('electron') !== -1) {
        // during development
        args = [fileToExecute];

        const proc = spawn(command, args);
        proc.on('error', (err) => {
          this.closeSockets();
          logger.error(err);
          reject(err);
        });
      } else {
        // during production
        command = `"${command}" -- --winInstaller`;

        exec(command, args, (err) => {
          this.closeSockets();
          logger.error(err);
          reject(err);
        });
      }

      // wait for the sub process to respond to resolve
      logger.debug(`sub process ${this.name} started`);
      if (this._isSocketInitialized()) {
        resolve();
      } else {
        this.on('connect', () => resolve());
      }
    });
  }
};

module.exports = UnityIPCServerPlatform;
