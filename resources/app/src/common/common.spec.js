const { UnityIPCServer, hubUtils } = require('./common');
const { fs, hubFS } = require('../fileSystem');
const os = require('os');
const mountutil = require('linux-mountutils');

const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const chaiAsPromised = require("chai-as-promised");

chai.use(chaiAsPromised);

describe('HubCommon', () => {
  let sandbox;

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
  });

  afterEach(() => {
    sandbox.restore();
  });
  describe('HubFS', () => {
    describe('getUniquePath', () => {
      let dirName = 'fun (\\?:-[]~!|&^{}$/.+*)';
      let inputPath = `path/to/${dirName}`;

      describe('when input path is a directory', () => {
        beforeEach(() => {
          sandbox.stub(fs, 'statSync').returns({ isDirectory: () => true });
        });

        it('should return the input path added with the suffix (1) when it exists and does not end with a number suffix', () => {
          sandbox.stub(fs, 'readdirSync').returns([dirName, 'cool']);
          expect(hubFS.getUniquePath(inputPath)).to.equal(`${inputPath} (1)`);
        });

        it('should return the input path with an incremented suffix', () => {
          sandbox.stub(fs, 'readdirSync').returns([`${dirName} (1)`, 'cool', `${dirName} (2)`, `${dirName}(3)`, `${dirName}3`]);
          expect(hubFS.getUniquePath(inputPath)).to.equal(`${inputPath} (3)`);
        });

      });

      it('should return the input path when it does not exist', () => {
        sandbox.stub(fs, 'statSync').throws({ code: 'ENOENT' });
        expect(hubFS.getUniquePath(inputPath)).to.equal(inputPath)
      });

      it('should return the input path when it is not a directory', () => {
        sandbox.stub(fs, 'statSync').returns({ isDirectory: () => false });
        expect(hubFS.getUniquePath(inputPath)).to.equal(inputPath);
      });

      it('should throw an error when it catch an unhandled exception', () => {
        sandbox.stub(fs, 'statSync').throws();
        expect(() => {
          hubFS.getUniquePath(inputPath)
        }).to.throw();
      });
    });

    describe('hasExtension', () => {
      it('should return true if has extension', () => {
        expect(hubFS.hasExtension('file.ext')).to.be.true;
        expect(hubFS.hasExtension('path/to/file.ext')).to.be.true;
        expect(hubFS.hasExtension('path/to.a/file.ext')).to.be.true;
        expect(hubFS.hasExtension('path\\to\\file.ext')).to.be.true;
      });

      it('should return false if has no extension', () => {
        expect(hubFS.hasExtension('path/to/file.ext/')).to.be.false;
        expect(hubFS.hasExtension('path/to/file/name')).to.be.false;
        expect(hubFS.hasExtension('path\\to\\file.ext\\')).to.be.false;
      });
    });

    describe('fileNameFromUrl', () => {
      it('should return undefined when no filename is found', () => {
        expect(hubFS.filenameFromUrl('www.unity3d.com')).to.be.undefined;
      });

      it('should return the filename from the given url', () => {
        expect(hubFS.filenameFromUrl('www.unity3d.com/some/resource')).equal('resource');
      });

      it('should return the filename from a url with query params', () => {
        expect(hubFS.filenameFromUrl('www.unity3d.com/some/resource.ext?query=param')).equal('resource.ext');
      });
    });

    describe('removeFolder', () => {
      it('should return rejecting promise if folder is not given', () => {
        return expect(hubFS.removeFolder()).to.eventually.be.rejectedWith('Folder not specified. Cannot remove folder (undefined).');
      });

      it('should return rejecting promise if folder is empty string', () => {
        return expect(hubFS.removeFolder('')).to.eventually.be.rejectedWith('Folder not specified. Cannot remove folder ().');
      });
    });

    describe('getDiskUsage', () => {
      it('should return rejecting promise if path is not given', () => {
        return expect(hubFS.getDiskUsage()).to.be.rejectedWith('Path not specified. Cannot verify disk usage.');
      });

      it('should return rejecting promise if path is empty string', () => {
        return expect(hubFS.getDiskUsage('')).to.be.rejectedWith('Path not specified. Cannot verify disk usage.');
      });
    });

    describe('getDiskRootPath', () => {
      beforeEach(() => {
        if (os.platform() === 'linux') {
            sandbox.stub(mountutil, 'isMounted').callsFake((folder) => {
              if (folder == '/contains/mounted' || folder == '/is/mounted') return { mounted: true };
              return { mounted: false };
            });
        }
      });

      it('should throw an error if path is not given', () => {
        return expect(() => hubFS.getDiskRootPath()).to.throw('Invalid path given');
      });

      it('should throw an error if path is empty string', () => {
        return expect(() => hubFS.getDiskRootPath('')).to.throw('Invalid path given');
      });

      if (os.platform() === 'darwin') {
        it('should return the first part', () => {
          return expect(hubFS.getDiskRootPath('/Volumes/test/nya/nyu/nyo/')).to.equal('/Volumes/test/');
        });

        it('should return the default', () => {
          return expect(hubFS.getDiskRootPath('/Users/nya/nyu/nyu')).to.equal('/');
        });
      }

      if (os.platform() === 'win32') {
        it('should return the drive letter only', () => {
          return expect(hubFS.getDiskRootPath('C:\\Users\\test\\App Data\\Local\\Temp')).to.equal('C:\\');
        });
      }

      if (os.platform() === 'linux') {
        it('should return the os root directory', () => {
          return expect(hubFS.getDiskRootPath('/not/mounted')).to.equal('/');
        });

        it('should return the mounted directory exactly as given', () => {
          return expect(hubFS.getDiskRootPath('/is/mounted')).to.equal('/is/mounted');
        });

        it('should return the mounted directory', () => {
          return expect(hubFS.getDiskRootPath('/contains/mounted/path')).to.equal('/contains/mounted');
        });
      }
    });

    describe('isValidFileName', () => {
      it('should return false if input is not string', () => {
        expect(hubFS.isValidFileName(123)).to.equals(false);
      });

      it('should return false if input is started with unacceptable beginnings', () => {
        expect(hubFS.isValidFileName('com1.abc')).to.equals(false, 'test com1.');
        expect(hubFS.isValidFileName('com5.abc')).to.equals(false, 'test com5.');
        expect(hubFS.isValidFileName('com9.abc')).to.equals(false, 'test com9.');
        expect(hubFS.isValidFileName('lpt1.abc')).to.equals(false, 'test lpt1.');
        expect(hubFS.isValidFileName('lpt9.abc')).to.equals(false, 'test lpt9.');
        expect(hubFS.isValidFileName('con.abc')).to.equals(false, 'test con.');
        expect(hubFS.isValidFileName('nul.abc')).to.equals(false, 'test nul.');
        expect(hubFS.isValidFileName('prn.abc')).to.equals(false, 'test prn.');
      });

      it('should return false if input has invalid characters', () => {
        expect(hubFS.isValidFileName('test/.json')).to.equals(false, 'test / ');
        expect(hubFS.isValidFileName('test?.json')).to.equals(false, 'test ?');
        expect(hubFS.isValidFileName('test<.json')).to.equals(false, 'test <');
        expect(hubFS.isValidFileName('test>.json')).to.equals(false, 'test >');
        expect(hubFS.isValidFileName('test\\.json')).to.equals(false, 'test \\');
        expect(hubFS.isValidFileName('test:.json')).to.equals(false, 'test :');
        expect(hubFS.isValidFileName('test*.json')).to.equals(false, 'test *');
        expect(hubFS.isValidFileName('test|.json')).to.equals(false, 'test |');
        expect(hubFS.isValidFileName('test".json')).to.equals(false, 'test "');
      });

      it('should return false if input ends with space or dot', () => {
        expect(hubFS.isValidFileName('ends with space ')).to.equals(false);
        expect(hubFS.isValidFileName('ends with dot.')).to.equals(false);
      });

      it('should return false if input starts with space', () => {
        expect(hubFS.isValidFileName(' starts with space')).to.equals(false);
      });

      it('should return true if input is valid', () => {
        expect(hubFS.isValidFileName('c')).to.equals(true, 'single character');
        expect(hubFS.isValidFileName('fancy$')).to.equals(true, 'test $');
        expect(hubFS.isValidFileName('~')).to.equals(true, 'test ~');
        expect(hubFS.isValidFileName('tildaIn~Name')).to.equals(true, 'test ~ in name');
        expect(hubFS.isValidFileName('Cr@zyN@m3w1th.#')).to.equals(true, 'test crazy valid name');
        expect(hubFS.isValidFileName('com1')).to.equals(true, 'test com1');
        expect(hubFS.isValidFileName('com.unity')).to.equals(true, 'test com.unity');
        expect(hubFS.isValidFileName('com10.unity')).to.equals(true, 'test com10.unity');
        expect(hubFS.isValidFileName('nu.unity')).to.equals(true, 'test nu.unity');
        expect(hubFS.isValidFileName('tow..dots')).to.equals(true, 'test two dots');
        expect(hubFS.isValidFileName('some.com1.in.middle')).to.equals(true, 'test com1. in middle');

      });

    });

    describe('isValidPath', () => {
      it('should return false if input is not string', () => {
        expect(hubFS.isValidPath(123)).to.equals(false);
      });

      it('should return false if part of the path is not valid file name', () => {
        expect(hubFS.isValidPath('my/com1.name/path')).to.equals(false);
        expect(hubFS.isValidPath(' my/path with/space in the beginning')).to.equals(false);
        expect(hubFS.isValidPath('my/path with/dot at the end.')).to.equals(false);
      });

      it('should return false if second part of the path has c:', () => {
        expect(hubFS.isValidPath('first/c:')).to.equals(false);
      });

      it('should return false if first part of the path has cd:', () => {
        expect(hubFS.isValidPath('cd:/whatever')).to.equals(false);
      });

      it('should return false if first part of the path has ?:', () => {
        expect(hubFS.isValidPath('?:/whatever')).to.equals(false);
      });

      it('should return true if first part of the path has c:', () => {
        expect(hubFS.isValidPath('c:/whatever')).to.equals(true);
      });

      it('should return false if input has two consecutive backslash', () => {
        expect(hubFS.isValidPath('have//two/backslash')).to.equals(false);
        expect(hubFS.isValidPath('//have/two/backslash/at/start')).to.equals(false);
        expect(hubFS.isValidPath('have/two/backslash/at/end//')).to.equals(false);
        expect(hubFS.isValidPath('have/three////backslash')).to.equals(false);
      });

      it('should return true if path is good', () => {
        expect(hubFS.isValidPath('~/whatever/fancy$')).to.equals(true);
        expect(hubFS.isValidPath('z:/some path with space/and fancy$')).to.equals(true);
        expect(hubFS.isValidPath('/start/with/backslash')).to.equals(true, 'test start with backslash');
        expect(hubFS.isValidPath('ends/with/backslash/')).to.equals(true, 'test ends with backslash');
      });
    });

    describe('startsWithHttp', () => {
      it('should return true if path starts with http or https', () => {
        expect(hubFS.startsWithHttp('http://www.google.com')).to.equals(true);
        expect(hubFS.startsWithHttp('http://www.unity3d.com/download/')).to.equals(true);
        expect(hubFS.startsWithHttp('http://')).to.equals(true);
        expect(hubFS.startsWithHttp('https://www.google.com')).to.equals(true);
        expect(hubFS.startsWithHttp('https://www.unity3d.com/download/')).to.equals(true);
        expect(hubFS.startsWithHttp('https://')).to.equals(true);
      });

      it('should return false if not starting with http or https', () => {
        expect(hubFS.startsWithHttp('C:\\Program Files\\')).to.equals(false);
        expect(hubFS.startsWithHttp('D:\\projects')).to.equals(false);
        expect(hubFS.startsWithHttp('/Volumes/Mac/projects')).to.equals(false);
        expect(hubFS.startsWithHttp('projects')).to.equals(false);
        expect(hubFS.startsWithHttp('projects/')).to.equals(false);
      });
    });

  });

  describe('UnityIPCServer', () => {
    describe('processJob', () => {
      let sandbox;

      beforeEach(() => { sandbox = sinon.sandbox.create() });
      afterEach(() => sandbox.restore());

      describe('when server is not started ', () => {
        it('should return an error if trying to process a job while not started', () => {
          let unityIPCServer = new UnityIPCServer('fakeserver', 'fakefile.js');
          unityIPCServer.started = false;
          return unityIPCServer.processJob('fakejob', []).catch((err) => {
            expect(err).to.equal("The Hub cannot proceed because the installation service is not running. Aborting.");
          });
        });
      });

      describe('when server is started ', () => {
        it('should return a timeout error if processing a job does not respond', () => {
          let unityIPCServer = new UnityIPCServer('fakeserver', 'fakefile.js');
          sandbox.stub(unityIPCServer, 'on');
          sandbox.stub(unityIPCServer, 'emit');
          sandbox.stub(unityIPCServer, 'emitAll');
          unityIPCServer.started = true;
          //timeout of 100 ms
          return unityIPCServer.processJob('fakejob', [], 100).catch((err) => {
            expect(err).to.equal("The operation timed out.");
          });
        });
      });
    });
  });

  describe('HubUtils', () => {
    describe('checkNestedProperties', () => {
      let sandbox;

      beforeEach(() => { sandbox = sinon.sandbox.create() });
      afterEach(() => sandbox.restore());

      describe('when given a js object', () => {
        it('should return false if the object is empty', () => {
          const result = {};
          expect(hubUtils.checkNestedProperties(result, 'level1', 'level2')).equal(false);
        });

        it('should return false if the object does not contain the hierarchy of elements', () => {
          const result = {'ivy-module': {info: [{'$': {'e:packageType': 'PlaybackEngine'}}]}};
          expect(hubUtils.checkNestedProperties(result, 'ivy-module', 'info', 1, '$')).equal(false);
        });

        it('should return true if the object contains the hierarchy of elements', () => {
          const result = {'ivy-module': {info: [{'$': {'e:packageType': 'PlaybackEngine'}}]}};
          expect(hubUtils.checkNestedProperties(result, 'ivy-module', 'info', 0, '$')).equal(true);
        });

        it('should return true if the complex object contains the hierarchy of elements', () => {
          const result = {
            "ivy-module": {
              "$": {
                "version": "2.0"
              },
              "info": [
                {
                  "$": {
                    "version": "2017.1.2",
                    "organisation": "Unity",
                    "module": "Android",
                    "e:packageType": "PlaybackEngine",
                    "e:unityVersion": "2017.1.2f1",
                    "xmlns:e": "http://ant.apache.org/ivy/extra"
                  }
                }
              ]
            }
          };
          expect(hubUtils.checkNestedProperties(result, 'ivy-module', 'info', 0, '$')).equal(true);
        });
      });
    });
  });

  describe('getEditorFolderFromExecutable', () => {

    if (os.platform() === 'darwin') {
      it('should return the folder that is one level above', () => {
        return expect(hubFS.getEditorFolderFromExecutable('C:/test/darwin/2019.2.0f1/Unity.app')).to.equal('C:/test/darwin/2019.2.0f1');
      });
    }

    if (os.platform() === 'win32') {
      it('should return the folder that is two levels above', () => {
        return expect(hubFS.getEditorFolderFromExecutable('C:\\test\\wins\\2019.2.0f1\\Editor\\Unity.exe')).to.equal('C:\\test\\wins\\2019.2.0f1');
      });
    }

    if (os.platform() === 'linux') {
      it('should return the folder that is two levels above', () => {
        return expect(hubFS.getEditorFolderFromExecutable('/test/linux/Editor/2019.3.10f1/Editor/Unity')).to.equal('/test/linux/Editor/2019.3.10f1');
      });
    }
  });
});
