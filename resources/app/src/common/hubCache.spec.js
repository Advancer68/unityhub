const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;

const hubCache = require('./hubCache');

describe('hubCache', () => {
  let sandbox;
  let hubCacheMock;

  before(() => {
    hubCacheMock = new hubCache();
  });

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
  });
  afterEach(() => {
    hubCacheMock.hubCache = {};
    sandbox.restore();
  });

  describe('cache is time indefinite', () => {
    beforeEach(() => {
      const timeStampStub = sandbox.stub(Date, 'now');
      timeStampStub.onCall(0).returns(new Date(0)); //set
      timeStampStub.onCall(1).returns(new Date(0)); //get

      hubCacheMock.set('cacheKey', { cacheItem: 'cacheItem' });
    });

    it('returns cache value by a given key', () => {
      expect(hubCacheMock.get('cacheKey')).to.deep.equal({ cacheItem: 'cacheItem' });
    });

    it('returns null when key does not exist', () => {
      expect(hubCacheMock.get('cache')).to.equal(null);
    });
  });

  describe('cache has a time to live', () => {
    describe('cache is not expired', function () {
      beforeEach(() => {
        const timeStampStub = sandbox.stub(Date, 'now');
        timeStampStub.onCall(0).returns(new Date(0)); //set
        timeStampStub.onCall(1).returns(new Date(0)); //get

        hubCacheMock.set('cacheKey', { cacheItem: 'cacheItem' }, 5000);
      });

      it('should return cache when given a key', () => {
        expect(hubCacheMock.get('cacheKey')).to.deep.equal({ cacheItem: 'cacheItem' });
      });

      it('should return null when key does not exist', () => {
        expect(hubCacheMock.get('cache')).to.equal(null);
      });
    });

    describe('cache expires', () => {
      beforeEach(() => {
        const timeStampStub = sandbox.stub(Date, 'now');
        timeStampStub.onCall(0).returns(new Date(0)); //set
        timeStampStub.onCall(1).returns(new Date(10000)); //get

        hubCacheMock.set('cacheKey', { cacheItem: 'cacheItem' }, 5000);
      });

      it('should return null and not expired cache', () => {
        expect(hubCacheMock.get('cacheKey')).to.deep.equal(null);
      });

      it('should return null when given key does not exist', () => {
        expect(hubCacheMock.get('cache')).to.equal(null);
      });
    });
  });
});
