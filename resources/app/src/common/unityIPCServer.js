const os = require('os');
const ipc = require('node-ipc');
const unityIPCNameFormatter = require('./unityIPCNameFormatter');
const logger = require('../logger')('UnityIPCServer');

const UnityIPCServerPlatform = require(`./unityIPCServer-${os.platform()}`);

/**
 * This class is a helper around node-ipc.
 * Each instance creates a new IPC server ready to spawn a new process.
 * The usage is:
 * create a new instance as an app service
 * getElevatedIPCServer()
 * closeSockets()
 *
 */
class UnityIPCServer {
  /**
   * A UnityIPCServer object encapsulates node-ipc.
   * It will create an object to control communication between a server (named after serverName)
   * and a child process, which is a javascript file ran
   * @param serverName the name of the server that will be reused to connect to it in the child process file
   * @param filePath child process file to run
   */
  constructor(serverName, filePath) {
    this.name = unityIPCNameFormatter.formatName(serverName);
    this.started = false;
    this.filePath = filePath;
    this.ipc = new ipc.IPC();
  }

  getElevatedIPCServer() {
    if (this.started) {
      // already started
      return Promise.resolve(this);
    }

    return this.startIPCServer()
      .then(() => this.spawnElevatedChildProcess(this.filePath))
      .then(() => this);
  }

  getIPCServer() {
    if (this.started) {
      // already started
      return Promise.resolve(this);
    }

    return this.startIPCServer()
      .then(() => this.spawnChildProcess(this.filePath))
      .then(() => this);
  }

  /**
   * Start an IPC Server
   * @returns {Promise}
   */
  startIPCServer() {
    return new Promise((resolve) => {
      logger.debug(`start IPC Server ${this.name}`);
      this.ipc.config.id = this.name;
      this.ipc.config.retry = 1500;
      this.ipc.config.silent = true;
      this.ipc.serve(this.name, () => {
        // general listener, used as a debugger
        this.ipc.server.on('message', (data) => {
          logger.debug(`client process sent msg ${data}`);
        });

        this.ipc.server.on('connect', () => {
          logger.info(`client process has connected to ${this.name}!`);
        });

        logger.info(`IPC Server ${this.name} started`);
        this.started = true;
        resolve();
      });

      this.ipc.server.start();
    });
  }

  /**
   *
   * @param jobName the event name emitted to the child process
   * @param jobParams the event params emitted to the child process
   * @param timeout a timeout (in milliseconds) to avoid waiting forever for actions
   */
  processJob(jobName, jobParams, timeout) {
    if (!this.started) {
      return Promise.reject('The Hub cannot proceed because the installation service is not running. Aborting.');
    }
    return new Promise((resolve, reject) => {
      let timeoutHolder = null;
      this.on(`done-${jobName}`, (data) => {
        if (timeoutHolder !== null) {
          clearTimeout(timeoutHolder);
          timeoutHolder = null;
        }
        resolve(data);
      });
      this.on(`error-${jobName}`, (error) => {
        if (timeoutHolder !== null) {
          clearTimeout(timeoutHolder);
          timeoutHolder = null;
        }
        reject(error);
      });

      this.emitAll(jobName, jobParams);
      if (timeout > 0) {
        timeoutHolder = setTimeout(() => {
          reject('The operation timed out.');
        }, timeout);
      }
    });
  }

  /**
   * Delegate function, for convenience
   * @param socket Socket to which to send message
   * @param message Message label to send to the child process
   * @param params Parameters to send along with the message label
   */
  emit(socket, message, params) {
    this.ipc.server.emit(socket, message, params);
  }

  /**
   * Emits message with params to all available connections.
   * @param message Message label to send to the child process
   * @param params Parameters to send along with the message label
   */
  emitAll(message, params) {
    this.ipc.server.sockets.forEach(socket => {
      this.ipc.server.emit(socket, message, params);
    });
  }

  /**
   * Delegate function, for convenience
   * @param message (string) Message label the child process sent
   * @param callback called when receiving this kind of message
   */
  on(message, callback) {
    this.ipc.server.on(message, callback);
  }
  
  off(message, callback) {
    this.ipc.server.off(message, callback);
  }

  /**
   * Stops the spawnee then stops the server
   */
  closeSockets() {
    return this.stopChildProcess()
      .then(() => {
        this.ipc.server.stop();
        this.started = false;
        logger.info(`IPC server ${this.name} and client closed`);
      });
  }

  stopChildProcess() {
    return new Promise((resolve, reject) => {
      logger.info(`emit stop child process of ${this.name}`);
      if (!this._isSocketInitialized()) {
        resolve();
        return;
      }

      this.emitAll('disconnect');

      this.ipc.server.on(
        'socket.disconnected',
        () => {
          logger.info(`client process has socket.disconnected from ${this.name}`);
          resolve();
        }
      );

      this.ipc.server.on(
        'error',
        (error) => {
          logger.error(`error while disconnecting client process of ${this.name}!`, error);
          reject(error);
        }
      );
    });
  }

  _isSocketInitialized(socket) {
    if (!socket) {
      if (this.ipc.server.sockets.length === 0) return false;

      socket = this.ipc.server.sockets[0];
    }

    return socket && socket.emit !== undefined;
  }
}

Object.assign(UnityIPCServer.prototype, UnityIPCServerPlatform);
module.exports = UnityIPCServer;
