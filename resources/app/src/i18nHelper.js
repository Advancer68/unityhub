const i18next = require('i18next');
const Backend = require('i18next-node-fs-backend');
const LanguageDetector = require('i18next-electron-language-detector');
const path = require('path');
const logger = require('./logger')('i18n');
const i18nConfig = require('./services/i18nConfig/i18nConfig');

const helper = {
  i18n: {},
  async configure() {
    const options = {
      // debug: true,
      saveMissing: true, // Needed for custom handler below
      fallbackLng: { 
        default: ['en'], 
        'zh-CN': ['zh_CN']
      },
      missingKeyHandler: (lng, ns, key) => {
        logger.warn(`Missing i18n key "${key} for language "${lng}"`);
      },
      backend: {
        loadPath: path.join(__dirname, '..', 'i18n', '{{lng}}', 'backend.json')
      },
      interpolation: { escapeValue: false }
    };

    await i18next.use(Backend) // Plugin to use file-system as the data source.
      .use(LanguageDetector) // Plugin to automatically detect os language based on the electron API.
      .init(options);

    // Use language saved in storage if possible
    const savedLanguage = await i18nConfig.getLanguage();
    if (savedLanguage) {
      i18next.changeLanguage(savedLanguage);
    }

    helper.i18n.translate = i18next.t.bind(i18next);
  }
};

module.exports = helper;
