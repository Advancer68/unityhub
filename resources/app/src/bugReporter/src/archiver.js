const yazl = require('yazl');
const { fs } = require('../../fileSystem');
const path = require('path');

class Archiver {
  /**
   * @param files {Array} Array of file paths to append to the zipped archive
   * @param archivePath {string} Absolute path of the archive
   * @return {Promise} Resolve when the archive is properly created
   */
  async createArchive(files, archivePath) {

    return new Promise((resolve) => {
      this._validateFiles(files);
      const archive = new yazl.ZipFile();

      for (const file of files) {
        archive.addFile(file, path.basename(file));
      }

      archive.outputStream.pipe(fs.createWriteStream(archivePath)).on('close', () => {
        resolve();
      });

      archive.end();
    });
  }

  _validateFiles(files) {
    if (!Array.isArray(files)) {
      throw new TypeError();
    }

    for (const file of files) {
      fs.accessSync(file, fs.constants.R_OK);
    }
  }
}

module.exports = new Archiver();
