const bugReporter = require('./bugReporter');
const sender = require('./sender');
const { app, dialog } = require('electron');

module.exports = {
  quit() {
    app.quit();
  },

  send(payload) {
    return sender.send(payload);
  },

  showOpenDialog() {
    return dialog.showOpenDialog(bugReporter.browserWindow, {
      openFile: true,
      properties: ['openFile'],
      title: 'Files attachment',
      buttonLabel: 'Attach',
      message: 'Select files to attach to the report'
    });
  }

};
