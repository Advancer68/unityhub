const { app } = require('electron');
const axios = require('axios');
const Archiver = require('./archiver');
const path = require('path');
const FormData = require('form-data');
const { fs, hubFS } = require('../../fileSystem');
const systemInfo = require('../../services/localSystemInfo/systemInfo');
const editorHelper = require('../../common/editor-helper');

class Sender {

  static get ARCHIVE_FILENAME() {
    return 'bugfiles.zip';
  }

  /**
   * Sends the content of the fields from the given object to the Bug Reporter endpoint
   * @param files {Array<string>} List of absolute file path to be attached. Note that this list should not contain log
   * files, since they'll be automatically attached within the method.
   * @param title {String} Title of the report
   * @param description {String} Description of the report
   * @param email {String} Email of the user
   * @param timestamp {Number} Current time (ms)
   * @param reproducibility {Number}  Unknown = -1, FirstTime = 0, Sometimes = 1, Always = 2
   * @returns {Promise<void>}
   */
  async send({ files, title, description, email, timestamp, reproducibility }) {
    const archiveFolderPath = hubFS.getTmpDirPath();
    await fs.mkdirp(archiveFolderPath);
    files = files.concat(await this.getHubLogs());
    // TODO to be fixed for linux and when the editor log folder does not exist
    //  files = files.concat(await this.getEditorLogs());
    this.archivePath = path.join(archiveFolderPath, Sender.ARCHIVE_FILENAME);
    await Archiver.createArchive(files, this.archivePath);

    const computer = await systemInfo.bugReporterSystemInfo();

    const form = new FormData();
    form.append('timestamp', timestamp);
    form.append('sTitle', title);
    form.append('sCustomerEmail', email);
    form.append('sVersion', 'Hub');
    form.append('sEvent', description);
    form.append('sComputer', computer);
    form.append('sBugReproducibility', reproducibility);
    form.append('sBugTypeID', 12); // 12 is for hub
    form.append('File1', fs.createReadStream(this.archivePath), {
      filename: Sender.ARCHIVE_FILENAME,
      contentType: 'octet/stream',
    });

    try {
      await axios.post(
        'https://editorbugs.unity3d.com/submit_bug_api.cgi',
        form,
        {
          headers: form.getHeaders(),
        }
      );

    } catch (error) {
      throw new Error(`upload failed: ${error}`);
    }

    fs.remove(path.dirname(this.archivePath));
    this.archivePath = undefined;
  }

  async getHubLogs() {
    try {
      const hubLogPath = path.join(app.getPath('userData'), 'logs');
      const files = await fs.readdir(hubLogPath);
      return files.map(filename => path.join(hubLogPath, filename));
    } catch (err) {
      return [];
    }
  }

  async getEditorLogs() {
    const editorLogFiles = ['Editor.log', 'Editor-prev.log', 'upm.log'];
    const result = [];
    try {
      const editorLogPath = editorHelper.getEditorLogPath();
      for (let i = 0; i < editorLogFiles.length; i++) {
        const logFilePath = path.join(editorLogPath, editorLogFiles[i]);
        if (fs.existsSync(logFilePath)) {
          result.push(logFilePath);
        }
      }

    } catch (err) {
      // do nothing
    }

    return result;
  }

}

module.exports = new Sender();
