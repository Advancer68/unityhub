const bugReporterApi = window.require('electron').remote.require('./bugReporter/src/clientApi.js');
const path = window.require('electron').remote.require('path');

let divAttachements;
let sendingForm;
let endPage;
let sendProgress;
let inputEmail;
let inputTitle;
let inputRepro;
let inputDescription1;
let inputDescription2;
let btnSend;
let attachedFiles = [];
let inputFields;
let snackbarContainer;

function quit() {
  bugReporterApi.quit();
}

function setupAttachedFiles() {
  if (attachedFiles.length > 0) {
    let formattedList = '';
    let currIndex = 0;
    attachedFiles.forEach((file) => {
      formattedList += `<span><i class="material-icons">attachment</i>${path.basename(file)}</br><button id="btnRemoveFile" fileIndex="${currIndex}" class="mdl-button mdl-js-button mdl-button--icon btn-remove-file"><i class="material-icons">clear</i></button></span>`;
      currIndex++;
    });
    divAttachements.innerHTML = formattedList;
    divAttachements.hidden = false;
    const btns = document.getElementsByClassName('btn-remove-file');
    for (let i = 0; i < btns.length; i++) {
      btns[i].addEventListener('click', removeFile);
    }
  } else {
    divAttachements.innerHTML = '';
    divAttachements.hidden = true;
  }
}

function showFileSelector() {
  const newFiles = bugReporterApi.showOpenDialog() || [];
  if (newFiles.length > 0) {
    newFiles.forEach((file) => {
      if (!attachedFiles.includes(file)) {
        attachedFiles.push(file);
      }
    });
    setupAttachedFiles();
  }
}

function removeFile() {
  attachedFiles.splice(this.getAttribute('fileIndex'), 1);
  setupAttachedFiles();
}

function notifyError() {
  const data = {
    message: 'Something went wrong',
    actionHandler: send,
    actionText: 'Retry'
  };
  snackbarContainer.MaterialSnackbar.showSnackbar(data);
}

async function send() {
  sendProgress.hidden = false;
  btnSend.disabled = true;
  try {
    await bugReporterApi.send({
      title: inputTitle.value,
      email: inputEmail.value,
      description: `${inputDescription1.value}\r\n${inputDescription2.value}`,
      reproducibility: inputRepro.value,
      files: attachedFiles,
      timestamp: Date.now()
    });
    sendingForm.hidden = true;
    endPage.hidden = false;
  } catch (e) {
    notifyError(e);
  }
  sendProgress.hidden = true;
  btnSend.disabled = false;
  attachedFiles = [];
}

function cleanUpTextField(field) {
  field.className = field.className.replace('is-dirty', '');
}

function showSendingForm() {
  inputFields.forEach(inputField => {
    inputField.value = '';
  });
  document.querySelectorAll('#textfields-container .mdl-textfield').forEach(field => cleanUpTextField(field));
  divAttachements.innerHTML = '';
  divAttachements.hidden = true;
  sendingForm.hidden = false;
  endPage.hidden = true;
  btnSend.disabled = true;
}

function checkAllValidity() {
  return inputFields.every(field => field.checkValidity());
}

function checkFormValidity() {
  btnSend.disabled = !checkAllValidity() || inputRepro.value === '-1';
}

function fetchHtmlElems() {
  divAttachements = document.getElementById('divAttachments');
  sendProgress = document.getElementById('sendProgress');
  inputEmail = document.getElementById('email');
  inputTitle = document.getElementById('title');
  inputRepro = document.getElementById('reproducibility');
  inputDescription1 = document.getElementById('description-part1');
  inputDescription2 = document.getElementById('description-part2');
  btnSend = document.getElementById('btnSend');
  endPage = document.getElementById('endPage');
  sendingForm = document.getElementById('sendingForm');
  snackbarContainer = document.getElementById('error-notifier');
  inputFields = [inputTitle, inputEmail, inputDescription1, inputDescription2, inputRepro];
}

function registerEvents() {
  const closingButtons = document.getElementsByClassName('close-buttons');
  Array.prototype.forEach.call(closingButtons, (button => button.addEventListener('click', quit)));
  document.getElementById('btnAttach').addEventListener('click', showFileSelector);
  document.getElementById('btnSend').addEventListener('click', send);
  document.getElementById('btnNew').addEventListener('click', showSendingForm);
  inputFields.forEach(field => field.addEventListener('input', checkFormValidity));

  sendingForm.addEventListener('submit', (event) => {
    event.preventDefault();
  });

  document.querySelectorAll('input[data-required]').forEach((e) => {
    e.required = true;
  });
  document.querySelectorAll('textarea[data-required]').forEach((e) => {
    e.required = true;
  });

  document.addEventListener('dragover', (event) => {
    event.preventDefault();
  });
  document.addEventListener('drop', (event) => {
    event.preventDefault();
  });
}

window.onload = () => {
  fetchHtmlElems();
  registerEvents();
};
