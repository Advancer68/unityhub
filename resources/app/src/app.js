/* eslint-disable quote-props */
const logger = require('./logger')('App');
const { app, dialog } = require('electron');
require('electron-debug')();
const EventEmitter = require('events');
const machina = require('machina');
const postal = require('postal');
require('machina.postal')(machina, postal);

const auth = require('./services/localAuth/auth');
const licenseCore = require('./services/licenseService/licenseCore');
const licenseClient = require('./services/licenseService/licenseClientProxy');
const localSettings = require('./services/localSettings/localSettings');
const editorManager = require('./services/editorManager/editorManager');
const EditorApp = require('./services/editorApp/editorapp');
const localProject = require('./services/localProject/localProject');
const cloudCollab = require('./services/cloudCollab/cloudCollab');
const localDownload = require('./services/localDownload/unityDownload');
const autoUpdater = require('./services/hubAutoUpdate/auto-updater');
const cloudConfig = require('./services/cloudConfig/cloudConfig');
const localConfig = require('./services/localConfig/localConfig');
const cloudAnalytics = require('./services/cloudAnalytics/cloudAnalytics');
const analyticsQueue = require('./services/cloudAnalytics/analyticsQueue');
const HubRestService = require('./services/localREST/hubRestService');
const HubIPCService = require('./services/localIPC/hubIPCService');
const onboardingService = require('./services/onboarding/onboardingService');
const livePushService = require('./services/livePush/livePushService');
const splashScreenService = require('./services/splashScreen/splashScreenService');
const plasticSCMService = require('./services/plasticSCMService/plasticSCMService');
const windowManager = require('./windowManager/windowManager');
const tokenManager = require('./tokenManager/tokenManager');
const trayMenu = require('./appComponents/trayMenu/trayMenu');
const systemInfo = require('./services/localSystemInfo/systemInfo');
const unityHubProtocolHandler = require('./unityhubProtocolHandler');
const networkInterceptors = require('./services/localAuth/networkInterceptors');
const learnContent = require('./services/learnContent/learnContentService');
const unityPackageManagerService = require('./services/unityPackageManager/unityPackageManagerService');
const openOptionsService = require('./services/openOptions/openOptionsService');
const i18nHelper = require('./i18nHelper');

const argumentParser = require('./appComponents/argumentParser');
const errorBox = require('./appComponents/errorBox');
const applicationMenu = require('./appComponents/applicationMenu');
const applicationRouteHelper = require('./appComponents/applicationRouteHelper');
const networkSetup = require('./networkSetup');
const proxyHelper = require('./proxyHelper');
const pluginManagerLight = require('./services/pluginManager/pluginManagerLight');

let features;

class App extends EventEmitter {

  get UI_RELATED_EVENTS() {
    return ['window-all-closed', 'activate'];
  }

  get events() {
    return {
      'ready': this.initSequence,
      'window-all-closed': this.restoreLicenseFile,
      'activate': this.showProperWindow,
      'before-quit': (event) => {
        if (!this.isQuitting) {
          this.quitGracefully(event);
        }
      },
      'will-finish-launching': this.setupHubProtocolHandler,
      'login': this.handleLogin,
      'web-contents-created': this.handleRemoteWebContent
    };
  }

  get features() {
    return features;
  }

  setFeatureActive(feature, status) {
    if (features[feature] === undefined) {
      throw new Error(`Cannot set feature status for non-existing feature ${feature}`);
    }

    if (typeof status !== 'boolean') {
      throw new Error(`Cannot set feature status for feature ${feature}. ${status} is not a boolean value.`);
    }

    features[feature] = status;
  }

  constructor() {
    super();

    this.unityHubLabel = 'Unity Hub';
    this.appStartTime = new Date().getTime();
    this.editors = app.editors = {};

    features = {
      IPC: true,
      REST: true,
      UI: true,
      HUB_PROTOCOL: true,
      AUTO_UPDATE: true,
      HEADLESS_MODE: false
    };

    app.setAppUserModelId('com.unity3d.unityhub');
    app.setLoginItemSettings({
      openAtLogin: openOptionsService.openAtLoginEnabled(),
    });
  
    argumentParser.setupYargs();
  }
  
  /**
   * Sets up electron application in the same way app.js used to.
   */
  start() {
    networkSetup.start();
    argumentParser.parseArguments();
    this._initCommunicationServices();
    errorBox.replaceElectronAPI();
    this._bindApplicationEventHandlers(this.events);
    this._addLegacyAPI();
  }

  _initCommunicationServices() {
    if (this.features.REST) {
      this.restService = new HubRestService();
    }

    if (this.features.IPC) {
      this.ipcService = new HubIPCService();
    }
  }

  _createApplicationMenu() {
    if (process.platform === 'darwin') {
      applicationMenu.createApplicationMenu(this.unityHubLabel);
    } else {
      applicationMenu.createEmptyApplicationMenu();
    }
  }

  _bindApplicationEventHandlers(mapping) {
    if (!this.features.UI) {
      this.UI_RELATED_EVENTS.forEach(event => { delete mapping[event]; });
    }

    Object.keys(mapping).forEach((event) => {
      app.on(event, mapping[event].bind(this));
    });
  }

  _addLegacyAPI() {
    // WARNING
    // This code is called in the SSO window when clicking "Skip".
    app.workOffline = () => {
      windowManager.signInWindow.close();
    };
  }

  async initSequence() {
    logger.debug('Electron main process is ready. Beginning init sequence..');

    i18nHelper.configure();
    systemInfo.logInfo();
    logger.debug(`Feature flags: ${JSON.stringify(this.features)}`);

    try {
      await this.initializeServices();
      await this.initializeApplication();
    } catch (error) {
      logger.error(`Failed to start Unity Hub, reason: ${error}`);
      if (error.stack !== undefined) {
        logger.error(error.stack);
      }
      dialog.showErrorBox(error.message, error.stack);
    }
  }

  async initializeServices() {
    logger.debug('Initializing services..');

    await localSettings.init();
    await cloudConfig.init();
    await localConfig.init();
    await tokenManager.init();
    await localDownload.init();
    await editorManager.init();
    await EditorApp.init();
    analyticsQueue.init();
    await windowManager.init();
    await auth.init();
    await licenseCore.reset();
    await licenseClient.init();
    await learnContent.init();
    await unityPackageManagerService.init();

    if (this.features.AUTO_UPDATE) {
      await autoUpdater.init();
    }

    if (this.features.REST) {
      await this.restService.start();
    }

    if (this.features.IPC) {
      await this.ipcService.start();
    }

    await localProject.init();
    await cloudCollab.init();
    await cloudAnalytics.init(this.appStartTime, this.features.HEADLESS_MODE);
    await pluginManagerLight.init();
    await livePushService.init(windowManager, licenseClient);
    await splashScreenService.init(auth);
    await plasticSCMService.init(localProject);
    logger.debug('Done initializing services');
  }

  async initializeApplication() {
    logger.debug('Initializing application..');
    networkInterceptors.init();

    if (this.features.UI) {
      await this.initializeUI();
    }

    logger.debug('Application ready');
    this.emit('ready');

    if (this.features.HUB_PROTOCOL) {
      unityHubProtocolHandler.init();
    }
  }

  async initializeUI() {
    logger.debug('Initializing UI..');

    trayMenu.init();
    this._createApplicationMenu();

    const { window, page } = await applicationRouteHelper.getInitialRoute();

    logger.debug('Done initializing UI');

    if (!app.argv.silent) {
      const user = await auth.getUserInfo();
      windowManager.mainWindow.setTitle();
      windowManager.showProperWindow(JSON.parse(user));
      logger.debug(`Showing ${window} with page: ${page}`);
    } else if (app.argv._ && app.argv._.length > 0) {
      // silent flag is sent by the editor when starting the hub.
      // the editor args will be passed after
      // e.g.  --cloudEnvironment production --debugMode --silent -- -a b -c d
      // next editor launch will be using these args.
      logger.info('editor Args', app.argv._);
      EditorApp.editorArgs = app.argv._;
    }
  }

  async restoreLicenseFile() {
    const result = await licenseCore.verifyLicense();

    if (result !== true) {
      licenseCore.restoreBackupLicenseFile();
    }
  }

  async showProperWindow() {
    const onboardingRoute = await applicationRouteHelper.getOnboardingRoute();
    if (onboardingRoute) {
      windowManager.onboardingWindow.show();
    } else {
      const user = await auth.getUserInfo();

      windowManager.showProperWindow(JSON.parse(user));
    }
  }

  async quitGracefully(event) {
    event.preventDefault();

    if (editorManager && editorManager.isJobInProgress()) {
      this.promptQuitDialog();
      return;
    }

    if (onboardingService.hasOnboarding === false) {
      this.quitApp();
      return;
    }

    try {
      const state = await onboardingService.getOnboardingState();
      const onboardingInProgress = state && !state.isFinished;

      if (onboardingInProgress) {
        this.promptQuitDialog();
      } else {
        this.quitApp();
      }

    } catch (error) {
      this.quitApp();
    }
  }

  promptQuitDialog() {
    if (!this.features.UI) {
      // TODO: Find a way to prompt user if UI is disabled.
      return;
    }

    dialog.showMessageBox({
      type: 'error',
      title: i18nHelper.i18n.translate('QUIT_WINDOW.WARNING'),
      message: i18nHelper.i18n.translate('QUIT_WINDOW.QUIT_INSTALL'),
      buttons: [i18nHelper.i18n.translate('QUIT_WINDOW.CANCEL'), i18nHelper.i18n.translate('QUIT_WINDOW.QUIT_ANYWAY')]
    }, (buttonIndex) => {
      if (buttonIndex === 1) {
        this.quitApp();
      }
    });
  }

  async quitApp() {
    // make sure we release all ports
    if (this.restService) { // this is only required for tests
      this.restService.stop();
    }

    if (this.ipcService) {
      this.ipcService.stop();
    }

    // make sure to close subprocess
    editorManager.closeInstallService();

    try {
      await cloudAnalytics.quitEvent('Shortcut');
    } catch (error) {
      // We're quitting anyways.
    }

    this._endProcess();
  }

  _endProcess() {
    this.isQuitting = true;
    process.kill(process.pid, 'SIGINT');
  }

  setupHubProtocolHandler() {
    if (!this.features.HUB_PROTOCOL) {
      return;
    }

    app.on('open-url', (e, url) => {
      logger.info(`Trying to open url ${url}`);

      if (url.includes(unityHubProtocolHandler.fullProtocolName)) {
        unityHubProtocolHandler.handleUrl(url);
      }
    });

    logger.info('Checking arguments for Windows custom URI scheme');
    let foundURIArgument = false;
    process.argv.forEach((arg) => {
      if (arg.includes(unityHubProtocolHandler.fullProtocolName)) {
        logger.info(`Found custom URI argument: ${arg}`);
        unityHubProtocolHandler.handleUrl(arg);
        foundURIArgument = true;
      }
    });

    if (!foundURIArgument) {
      logger.info('No custom URI argument found');
    }
  }

  handleLogin(event, webContents, details, authInfo, callback) {
    if (!proxyHelper.auth) { return; }

    event.preventDefault();
    const { username, password } = proxyHelper.auth;
    callback(username, password);
  }

  handleRemoteWebContent(_event, contents) {
    contents.on('will-attach-webview', (__event, webPreferences) => {
      logger.debug('Disabling node intergration for newly attached webview');
      webPreferences.nodeIntegration = false;
    });

    contents.on('new-window', async (event) => {
      logger.debug('Preventing new window from opening');
      event.preventDefault();
    });
  }
}

module.exports = new App();
