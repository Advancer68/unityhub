const proxyHelper = require('./proxyHelper');

const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;
const sinonChai = require('sinon-chai');
chai.use(sinonChai);

const PROXY_ENV_VARS = {
  http: 'HTTP_PROXY',
  https: 'HTTPS_PROXY'
};

function setEnv(protocol, value) {
  process.env[PROXY_ENV_VARS[protocol]] = value;
}

describe('ProxyHelper', () => {
  let sandbox, env;
  
  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    env = Object.assign({}, process.env);
  });
  
  afterEach(() => {
    sandbox.restore();
    process.env = env;
    // Small hack to reset state between tests.
    proxyHelper.parseEnvironmentVariables();
  });
  
  describe('parseEnvironmentVariables', () => {
  
    describe('when HTTP_PROXY environment variable is defined', () => {
      whenProtocolProxyIsDefined('http');
    });
    
    describe('when HTTPS_PROXY environment variable is defined', () => {
      whenProtocolProxyIsDefined('https');
    });
  
    function whenProtocolProxyIsDefined(protocol) {
      describe('and is empty string', () => {
        it('should set proxy information to undefined', () => {
          setEnv(protocol, '');
          proxyHelper.parseEnvironmentVariables();
          expect(proxyHelper[protocol]).to.be.undefined;
        });
      });
      
      describe('and url does not contain protocol', () => {
        it('should set proxy information to undefined', () => {
          setEnv(protocol, 'test.com');
          proxyHelper.parseEnvironmentVariables();
          expect(proxyHelper[protocol]).to.be.undefined;
        });
      });
      
      describe('and url does not contain host', () => {
        it('should set proxy information to undefined', () => {
          setEnv(protocol, 'http://');
          proxyHelper.parseEnvironmentVariables();
          expect(proxyHelper[protocol]).to.be.undefined;
        });
      });
      
      describe('and url is valid', () => {
        it('should properly set protocol information', () => {
          const string = `${protocol}://username:password@test.com`;
          const stringWithoutAuth = `${protocol}://test.com`;
          const { host, username, password } = new URL(string);
          setEnv(protocol, string);
          proxyHelper.parseEnvironmentVariables();
          expect(proxyHelper[protocol].string).to.equal(string);
          expect(proxyHelper[protocol].stringWithoutAuth).to.equal(stringWithoutAuth);
          expect(proxyHelper[protocol].url.protocol).to.equal(`${protocol}:`);
          expect(proxyHelper[protocol].url.host).to.equal(host);
          expect(proxyHelper[protocol].url.username).to.equal(username);
          expect(proxyHelper[protocol].url.password).to.equal(password);
        });
      });
  
      describe('and does not contains auth information', () => {
        it('should set auth information to undefined', () => {
          setEnv(protocol, 'http://test.com');
          proxyHelper.parseEnvironmentVariables();
          expect(proxyHelper.auth).to.be.undefined;
        });
      });
      
      describe('and contains auth information', () => {
        describe('and misses username', () => {
          it('should set auth information to undefined', () => {
            setEnv(protocol, 'http://:password@test.com');
            proxyHelper.parseEnvironmentVariables();
            expect(proxyHelper.auth).to.be.undefined;
          });
        });
        
        describe('and misses password', () => {
          it('should set auth information to undefined', () => {
            setEnv(protocol, 'http://username@test.com');
            proxyHelper.parseEnvironmentVariables();
            expect(proxyHelper.auth).to.be.undefined;
          });
        });
        
        describe('and auth was already set', () => {
          it('should keep current auth information', () => {
            setEnv(protocol, 'http://username:password@test.com');
            sandbox.stub(proxyHelper, 'auth').get(() => ({ username: 'foo', password: 'bar' }));
            proxyHelper.parseEnvironmentVariables();
            expect(proxyHelper.auth.username).to.equal('foo');
            expect(proxyHelper.auth.password).to.equal('bar');
          });
        });
      });
    }
  });
  
  describe('http', () => {
    protocolGetter('http');
  });
  
  describe('https', () => {
    protocolGetter('https');
  });
  
  function protocolGetter(protocol) {
    describe('when environment variable is set', () => {
      beforeEach(() => {
        setEnv(protocol, 'http://test.com');
      });
      
      describe('and parseEnvironmentVariables was not called', () => {
        it('should be undefined', () => {
          expect(proxyHelper[protocol]).to.be.undefined;
        });
      });
    
      describe('and parseEnvironmentVariables was called', () => {
        it('should have proper environment variables', () => {
          proxyHelper.parseEnvironmentVariables();
          expect(proxyHelper[protocol].string).not.to.be.undefined;
          expect(proxyHelper[protocol].stringWithoutAuth).not.to.be.undefined;
          expect(proxyHelper[protocol].url.protocol).not.to.be.undefined;
          expect(proxyHelper[protocol].url.host).not.to.be.undefined;
        });
      });
    });
    
    describe('when environment variable is not set', () => {
      describe('and parseEnvironmentVariables was not called', () => {
        it('should be undefined', () => {
          expect(proxyHelper[protocol]).to.be.undefined;
        });
      });
    
      describe('and parseEnvironmentVariables was called', () => {
        it('should be undefined', () => {
          proxyHelper.parseEnvironmentVariables();
          expect(proxyHelper[protocol]).to.be.undefined;
        });
      });
    });
  }
  
  describe('auth', () => {
    describe('and no protocol has auth information', () => {
      describe('and parseEnvironmentVariables was not called', () => {
        it('should be undefined', () => {
          expect(proxyHelper.auth).to.be.undefined;
        });
      });
      
      describe('and parseEnvironmentVariables was called', () => {
        it('should be undefined', () => {
          proxyHelper.parseEnvironmentVariables();
          expect(proxyHelper.auth).to.be.undefined;
        });
      });
    });
  
    describe('and http environment variable has auth information', () => {
      beforeEach(() => {
        setEnv('http', 'http://username:password@test.com');
      });
      
      describe('and parseEnvironmentVariables was not called', () => {
        it('should be undefined', () => {
          expect(proxyHelper.auth).to.be.undefined;
        });
      });
    
      describe('and parseEnvironmentVariables was called', () => {
        it('should be set properly', () => {
          proxyHelper.parseEnvironmentVariables();
          expect(proxyHelper.auth.username).to.equal('username');
          expect(proxyHelper.auth.password).to.equal('password');
        });
      });
    });
  
    describe('and https environment variable has auth information', () => {
      beforeEach(() => {
        setEnv('https', 'https://username:password@test.com');
      });
  
      describe('and parseEnvironmentVariables was not called', () => {
        it('should be undefined', () => {
          expect(proxyHelper.auth).to.be.undefined;
        });
      });
    
      describe('and parseEnvironmentVariables was called', () => {
        it('should be set properly', () => {
          proxyHelper.parseEnvironmentVariables();
          expect(proxyHelper.auth.username).to.equal('username');
          expect(proxyHelper.auth.password).to.equal('password');
        });
      });
    });
  
    describe('and http and https environment variable has auth information', () => {
      beforeEach(() => {
        setEnv('http', 'https://user:pass@test.com');
        setEnv('https', 'https://username:password@test.com');
      });
  
      describe('and parseEnvironmentVariables was not called', () => {
        it('should be undefined', () => {
          expect(proxyHelper.auth).to.be.undefined;
        });
      });
    
      describe('and parseEnvironmentVariables was called', () => {
        it('should be set to http values', () => {
          proxyHelper.parseEnvironmentVariables();
          expect(proxyHelper.auth.username).to.equal('user');
          expect(proxyHelper.auth.password).to.equal('pass');
        });
      });
    });
  });

});
