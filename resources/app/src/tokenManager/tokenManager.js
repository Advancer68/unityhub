const storage = require('electron-json-storage');
const promisify = require('es6-promisify');
const logger = require('../logger')('TokenManager');
const keytar = require('keytar');
const os = require('os');

// Constants for keytar usage
const SERVICE_NAME = 'UnityHub';
const ACCESS_TOKEN = 'accessToken';
const REFRESH_TOKEN = 'refreshToken';
const ACCESS_TOKEN_EXPIRATION = 'accessTokenExpiration';
const REFRESH_TOKEN_EXPIRATION = 'refreshTokenExpiration';

function isLinuxPlatform() {
  return os.platform() === 'linux';
}

function resetTokens() {
  this.accessToken = {
    value: '',
    expiration: Date.now()
  };
  this.refreshToken = {
    value: '',
    expiration: Date.now()
  };
}

class TokenManager {

  constructor() {
    resetTokens.bind(this)();
  }

  init() {
    logger.info('Init');

    // Although we now use keytar to store the tokens, we still need to read from the old files for the transition.
    const getAsync = promisify(storage.get);
    const tokensAsync = [
      getAsync(ACCESS_TOKEN),
      getAsync(REFRESH_TOKEN),
      getAsync(ACCESS_TOKEN_EXPIRATION),
      getAsync(REFRESH_TOKEN_EXPIRATION),
      keytar.getPassword(SERVICE_NAME, ACCESS_TOKEN),
      keytar.getPassword(SERVICE_NAME, REFRESH_TOKEN),
      keytar.getPassword(SERVICE_NAME, ACCESS_TOKEN_EXPIRATION),
      keytar.getPassword(SERVICE_NAME, REFRESH_TOKEN_EXPIRATION),
    ];

    return Promise.all(tokensAsync)
      .then((result) => {
        this.setupLocalTokens(result);
        logger.info('Done init');
      })
      .catch((e) => {
        logger.warn('Token storage is corrupted.', e);
      });
  }

  setupLocalTokens([
    storageAccessToken,
    storageRefreshToken,
    storageAccessTokenExpiration,
    storageRefreshTokenExpiration,
    keytarAccessToken,
    keytarRefreshToken,
    keytarAccessTokenExpiration,
    keytarRefreshTokenExpiration
  ]) {

    if (isLinuxPlatform()) {
      this.accessToken = {
        value: typeof storageAccessToken === 'string' ? storageAccessToken : '',
        expiration: typeof storageAccessTokenExpiration === 'number' ? storageAccessTokenExpiration : Date.now()
      };
      this.refreshToken = {
        value: typeof storageRefreshToken === 'string' ? storageRefreshToken : '',
        expiration: typeof storageRefreshTokenExpiration === 'number' ? storageRefreshTokenExpiration : Date.now()
      };
    } else {

      // General procedure for each token value
      // 1- Try reading from keytar
      // 2- If that does not work, try to read from electron electron-json-storage
      // 3- If that does not work either, leave values empty
      // 4- If necessary, wipe out electron-json-storage and use keytar only

      // Access Token
      this._setupToken(keytarAccessToken, storageAccessToken, 'accessToken');
      // Refresh Token
      this._setupToken(keytarRefreshToken, storageRefreshToken, 'refreshToken');
      // Access Token Expiration
      this._setupTokenExpiration(keytarAccessTokenExpiration, storageAccessTokenExpiration, 'accessToken');
      // Refresh Token Expiration
      this._setupTokenExpiration(keytarRefreshTokenExpiration, storageRefreshTokenExpiration, 'refreshToken');

      // Perform the migration if any of the values come from the electron-json-storage files
      const migrationNeeded = (!keytarAccessToken && typeof storageAccessToken === 'string')
        || (!keytarRefreshToken && typeof storageRefreshToken === 'string')
        || (!keytarAccessTokenExpiration && typeof expiration === 'number')
        || (!keytarRefreshTokenExpiration && typeof storageRefreshTokenExpiration === 'number');

      if (migrationNeeded) {
        this._clearElectronJsonStorageTokens();
        this.storeTokens(this.accessToken, this.refreshToken);
      }
    }
  }

  _setupToken(keytarValue, storageValue, field) {
    if (keytarValue) {
      this[field].value = keytarValue;
    } else if (typeof storageValue === 'string') {
      this[field].value = storageValue;
    } else {
      this[field].value = '';
    }
  }

  _setupTokenExpiration(keytarValue, storageValue, field) {
    if (keytarValue) {
      const expiration = Number(keytarValue);
      this[field].expiration = typeof expiration === 'number' ? expiration : Date.now();
    } else if (typeof storageValue === 'number') {
      this[field].expiration = storageValue;
    } else {
      this[field].expiration = Date.now();
    }
  }

  async storeTokens(accessToken = {}, refreshToken = {}) {
    if (!accessToken.value || !accessToken.expiration || !refreshToken.value || !refreshToken.expiration) {
      throw new TypeError();
    }

    Object.assign(this.accessToken, accessToken);
    Object.assign(this.refreshToken, refreshToken);

    if (isLinuxPlatform()) {
      storage.set(ACCESS_TOKEN, accessToken.value);
      storage.set(REFRESH_TOKEN, refreshToken.value);
      storage.set(ACCESS_TOKEN_EXPIRATION, accessToken.expiration);
      storage.set(REFRESH_TOKEN_EXPIRATION, refreshToken.expiration);
    } else {
      try {
        await keytar.setPassword(SERVICE_NAME, ACCESS_TOKEN, accessToken.value);
        await keytar.setPassword(SERVICE_NAME, REFRESH_TOKEN, refreshToken.value);
        await keytar.setPassword(SERVICE_NAME, ACCESS_TOKEN_EXPIRATION, accessToken.expiration.toString());
        await keytar.setPassword(SERVICE_NAME, REFRESH_TOKEN_EXPIRATION, refreshToken.expiration.toString());
      } catch (err) {
        logger.warn('Unable to save tokens.', err);
      }
    }
  }

  clearTokens() {
    resetTokens.bind(this)();

    if (isLinuxPlatform()) {
      this._clearElectronJsonStorageTokens();
    } else {
      this._clearKeytarTokens();
    }
  }

  _clearElectronJsonStorageTokens() {
    [
      ACCESS_TOKEN,
      REFRESH_TOKEN,
      ACCESS_TOKEN_EXPIRATION,
      REFRESH_TOKEN_EXPIRATION,
    ].forEach(key => storage.remove(key));
  }

  _clearKeytarTokens() {
    [
      ACCESS_TOKEN,
      REFRESH_TOKEN,
      ACCESS_TOKEN_EXPIRATION,
      REFRESH_TOKEN_EXPIRATION,
    ].forEach(async key => await keytar.deletePassword(SERVICE_NAME, key));
  }
}

module.exports = new TokenManager();
