const tokenManager = require('./tokenManager');
const storage = require('electron-json-storage');
const keytar = require('keytar');
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const os = require('os');

chai.use(chaiAsPromised);
chai.use(sinonChai);

describe('tokenManager', () => {
  let sandbox;
  let storageGetStub;
  const storageAccessToken = {value: 'a', expiration: 1};
  const storageRefreshToken = {value: 'b', expiration: 2};
  const keytarAccessToken = {value: 'c', expiration: 3};
  const keytarRefreshToken = {value: 'd', expiration: 4};

  const stubTokenAccessors = () => {
    const storageTokenInfo = {};
    const keytarTokenInfo = {};

    storageGetStub = sandbox.stub(storage, 'get');
    storageGetStub.callsFake((key, cbk) => {
      cbk(undefined, storageTokenInfo[key]);
    });

    sandbox.stub(keytar, 'getPassword').callsFake((...args) => {
      // NOTE: We use ...args to avoid lint errors
      // "account" in keytar terms represents the key in "tokenInfo" object
      const account = args[1];
      return Promise.resolve(keytarTokenInfo[account]);;
    });
  };

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    sandbox.stub(storage, 'set');
    sandbox.stub(storage, 'remove');
    sandbox.stub(keytar, 'setPassword');
    sandbox.stub(keytar, 'deletePassword');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('init', () => {

    beforeEach(() => {
      stubTokenAccessors();
    });

    it('should attempt to load tokens from storage', () => {
      return tokenManager.init().then(() => {
        expect(storage.get).to.have.been.calledWith('accessToken');
        expect(storage.get).to.have.been.calledWith('refreshToken');
        expect(storage.get).to.have.been.calledWith('accessTokenExpiration');
        expect(storage.get).to.have.been.calledWith('refreshTokenExpiration');
      });
    });

    it('should attempt to load tokens from keytar', () => {
      return tokenManager.init().then(() => {
        expect(keytar.getPassword).to.have.been.calledWith('UnityHub', 'accessToken');
        expect(keytar.getPassword).to.have.been.calledWith('UnityHub', 'refreshToken');
        expect(keytar.getPassword).to.have.been.calledWith('UnityHub', 'accessTokenExpiration');
        expect(keytar.getPassword).to.have.been.calledWith('UnityHub', 'refreshTokenExpiration');
      });
    });

    it('should call setupLocalTokens', () => {
      sandbox.stub(tokenManager, 'setupLocalTokens');
      return tokenManager.init().then(() => {
        expect(tokenManager.setupLocalTokens).to.have.been.called;
      });
    });
  });

  describe('setupLocalTokens', () => {
    beforeEach(() => {
      stubTokenAccessors();
      sandbox.stub(tokenManager, 'storeTokens');
    });

    describe('when there are tokens in storage only', () => {
      const tokens = [
        storageAccessToken.value,
        storageRefreshToken.value,
        storageAccessToken.expiration,
        storageRefreshToken.expiration,
        undefined,
        undefined,
        undefined,
        undefined
      ];

      it('should set its accessToken and refreshToken with it after resolving', () => {
        tokenManager.setupLocalTokens(tokens);
        expect(tokenManager.accessToken.value).to.equal(storageAccessToken.value);
        expect(tokenManager.accessToken.expiration).to.equal(storageAccessToken.expiration);
        expect(tokenManager.refreshToken.value).to.equal(storageRefreshToken.value);
        expect(tokenManager.refreshToken.expiration).to.equal(storageRefreshToken.expiration);
      });

      if (os.platform() === 'linux') {
        describe('for a linux environment', () => {
          it('should not clear storage tokens', () => {
            tokenManager.setupLocalTokens(tokens);
            expect(storage.remove).to.have.not.been.calledWith('accessToken');
            expect(storage.remove).to.have.not.been.calledWith('accessTokenExpiration');
            expect(storage.remove).to.have.not.been.calledWith('refreshToken');
            expect(storage.remove).to.have.not.been.calledWith('refreshTokenExpiration');
          });

          it('should not call storeTokens', () => {
            tokenManager.setupLocalTokens(tokens);
            expect(tokenManager.storeTokens).to.have.not.been.called;
          });
        })
      } else {
        describe('for a non-linux environment', () => {
          it('should clear storage tokens', () => {
            tokenManager.setupLocalTokens(tokens);
            expect(storage.remove).to.have.been.calledWith('accessToken');
            expect(storage.remove).to.have.been.calledWith('accessTokenExpiration');
            expect(storage.remove).to.have.been.calledWith('refreshToken');
            expect(storage.remove).to.have.been.calledWith('refreshTokenExpiration');
          });

          it('should call storeTokens', () => {
            tokenManager.setupLocalTokens(tokens);
            expect(tokenManager.storeTokens).to.have.been.calledWith(storageAccessToken, storageRefreshToken);
          });
        })
      }
    });

    describe('when there are tokens in keytar only', () => {
      const tokens = [
        undefined,
        undefined,
        undefined,
        undefined,
        keytarAccessToken.value,
        keytarRefreshToken.value,
        keytarAccessToken.expiration,
        keytarRefreshToken.expiration
      ];

      if (os.platform() !== 'linux') {
        it('should set its accessToken and refreshToken with those after resolving', () => {
          tokenManager.setupLocalTokens(tokens);
          expect(tokenManager.accessToken.value).to.equal(keytarAccessToken.value);
          expect(tokenManager.accessToken.expiration).to.equal(keytarAccessToken.expiration);
          expect(tokenManager.refreshToken.value).to.equal(keytarRefreshToken.value);
          expect(tokenManager.refreshToken.expiration).to.equal(keytarRefreshToken.expiration);
        });
      }
    });

    describe('when there are NO token in storage', () => {
      beforeEach(() => {
        sandbox.stub(Date, 'now').returns(42);
      });

      it('should set default properties for accessToken and refreshToken after resolving', () => {
        tokenManager.setupLocalTokens([]);
        expect(tokenManager.accessToken.value).to.equal('');
        expect(tokenManager.accessToken.expiration).to.equal(42);
        expect(tokenManager.refreshToken.value).to.equal('');
        expect(tokenManager.refreshToken.expiration).to.equal(42);
      });
    });

    describe('when storage is corrupted', () => {
      beforeEach(() => {
        storageGetStub.callsFake((key, cbk) => {
          cbk(new Error('BWAAAARGH'));
        });
        sandbox.stub(Date, 'now').returns(42);
      });

      it('should set default properties for accessToken and refreshToken after resolving', () => {
        return tokenManager.init().then(() => {
          expect(tokenManager.accessToken.value).to.equal('');
          expect(tokenManager.accessToken.expiration).to.equal(42);
          expect(tokenManager.refreshToken.value).to.equal('');
          expect(tokenManager.refreshToken.expiration).to.equal(42);
        })
      });
    });
  });

  describe('storeTokens', () => {

    it('should throw a TypeError when tokens are missing or their properties are invalid', () => {
      expect(tokenManager.storeTokens()).to.eventually.throw(TypeError);
      expect(tokenManager.storeTokens({ value: 'a', expiration: 1 })).to.eventually.throw(TypeError);
      expect(tokenManager.storeTokens({ value: 'a', expiration: 1 }, { value: 'b' })).to.eventually.throw(TypeError);
    });

    it('should set the tokenManager accessToken and refreshToken attributes', async () => {
      await tokenManager.storeTokens(storageAccessToken, storageRefreshToken);
      expect(tokenManager.accessToken).to.deep.equal(storageAccessToken);
      expect(tokenManager.refreshToken).to.deep.equal(storageRefreshToken);
    });
    
    if (os.platform() === 'linux') {
      describe('for a linux environment', () => {
        it('should store the accessToken and refreshToken properties in storage', async () => {
          await tokenManager.storeTokens(storageAccessToken, storageRefreshToken);
          expect(storage.set).to.have.been.calledWith('accessToken');
          expect(storage.set).to.have.been.calledWith('accessTokenExpiration');
          expect(storage.set).to.have.been.calledWith('refreshToken');
          expect(storage.set).to.have.been.calledWith('refreshTokenExpiration');
        });

        it('should not store the accessToken and refreshToken properties in keytar', async () => {
          await tokenManager.storeTokens(storageAccessToken, storageRefreshToken);
          expect(keytar.setPassword).to.have.not.been.called;
        });
      })
    } else {
      describe('for a non-linux environment', () => {
        it('should not store the accessToken and refreshToken properties in storage', async () => {
          await tokenManager.storeTokens(storageAccessToken, storageRefreshToken);
          expect(storage.set).to.have.not.been.calledWith('accessToken');
          expect(storage.set).to.have.not.been.calledWith('accessTokenExpiration');
          expect(storage.set).to.have.not.been.calledWith('refreshToken');
          expect(storage.set).to.have.not.been.calledWith('refreshTokenExpiration');
        });

        it('should store the accessToken and refreshToken properties in keytar', async () => {
          await tokenManager.storeTokens(storageAccessToken, storageRefreshToken);
          expect(keytar.setPassword).to.have.been.calledWith('UnityHub', 'accessToken', 'a');
          expect(keytar.setPassword).to.have.been.calledWith('UnityHub', 'refreshToken', 'b');
          expect(keytar.setPassword).to.have.been.calledWith('UnityHub', 'accessTokenExpiration', '1');
          expect(keytar.setPassword).to.have.been.calledWith('UnityHub', 'refreshTokenExpiration', '2');
        });
      })
    }
  });

  describe('clearTokens', () => {

    beforeEach(() => {
      sandbox.stub(Date, 'now').returns(42);
    });

    it('should reset tokenManager access token and refresh token attribute to their default value', () => {
      tokenManager.storeTokens(storageAccessToken, storageRefreshToken);
      tokenManager.clearTokens();
      expect(tokenManager.accessToken).to.deep.equal({value: '', expiration: 42});
      expect(tokenManager.refreshToken).to.deep.equal({value: '', expiration: 42});
    });

    if (os.platform() === 'linux') {
      describe('for a linux environment', () => {
        it('should remove accessToken and refreshToken properties from storage', () => {
          tokenManager.clearTokens();
          expect(storage.remove).to.have.been.calledWith('accessToken');
          expect(storage.remove).to.have.been.calledWith('accessTokenExpiration');
          expect(storage.remove).to.have.been.calledWith('refreshToken');
          expect(storage.remove).to.have.been.calledWith('refreshTokenExpiration');
        });

        it('should not remove accessToken and refreshToken properties from keytar', () => {
          tokenManager.clearTokens();
          expect(keytar.deletePassword).to.have.not.been.calledWith('UnityHub', 'accessToken');
          expect(keytar.deletePassword).to.have.not.been.calledWith('UnityHub', 'accessTokenExpiration');
          expect(keytar.deletePassword).to.have.not.been.calledWith('UnityHub', 'refreshToken');
          expect(keytar.deletePassword).to.have.not.been.calledWith('UnityHub', 'refreshTokenExpiration');
        });
      })
    } else {
      describe('for a non-linux environment', () => {
        it('should not remove accessToken and refreshToken properties from storage', () => {
          tokenManager.clearTokens();
          expect(storage.remove).to.have.not.been.calledWith('accessToken');
          expect(storage.remove).to.have.not.been.calledWith('accessTokenExpiration');
          expect(storage.remove).to.have.not.been.calledWith('refreshToken');
          expect(storage.remove).to.have.not.been.calledWith('refreshTokenExpiration');
        });

        it('should remove accessToken and refreshToken properties from keytar', () => {
          tokenManager.clearTokens();
          expect(keytar.deletePassword).to.have.been.calledWith('UnityHub', 'accessToken');
          expect(keytar.deletePassword).to.have.been.calledWith('UnityHub', 'accessTokenExpiration');
          expect(keytar.deletePassword).to.have.been.calledWith('UnityHub', 'refreshToken');
          expect(keytar.deletePassword).to.have.been.calledWith('UnityHub', 'refreshTokenExpiration');
        });
      })
    }
  });
});