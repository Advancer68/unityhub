const axios = require('axios');
const globalAgent = require('global-agent');
const proxyHelper = require('./proxyHelper');
const logger = require('./logger')('NetworkSetup');

class NetworkSetup {

  start() {
    try {
      // the global tunnel may remove some env variables from the process.env
      // to avoid double proxy in the hub. However, when launching the editor, it causes the packman to fail to proxy
      // the originalEnvVars will be used in the launchprocess.js
      this.originalEnvVars = Object.assign({}, process.env);
      
      proxyHelper.parseEnvironmentVariables();
      this.setupAxiosDefaults();
      this.initializeGlobalAgent();
    } catch (e) {
      logger.warn('failed to setup the network proxy', e);
    }
  }
  setupAxiosDefaults() {
    axios.defaults.proxy = false;
  }
  
  initializeGlobalAgent() {
    globalAgent.bootstrap();
    // Override proxy because global-agent isn't using standard variables.
    if (proxyHelper.http) {
      global.GLOBAL_AGENT.HTTP_PROXY = proxyHelper.http.string;
    }
    
    if (proxyHelper.https) {
      global.GLOBAL_AGENT.HTTPS_PROXY = proxyHelper.https.string;
    }
  }
}

module.exports = new NetworkSetup();
