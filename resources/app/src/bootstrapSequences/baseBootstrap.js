const app = require('../app.js');

class BaseBootstrap {
  static start() {
    app.start();
  }
}

module.exports = BaseBootstrap;
