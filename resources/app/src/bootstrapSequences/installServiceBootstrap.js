/* eslint-disable global-require */

const BaseBootstrap = require('./baseBootstrap');
const localInstaller = require('../services/localInstaller/unityInstaller');
const logger = require('../logger')('installServiceBootstrap');

class InstallServiceBootstrap extends BaseBootstrap {
  static start() {
    logger.info('start install sub process');
    logger.activateWinInstallerProcessOutput();
    
    const entryFilePath = localInstaller.platformDependent.INSTALL_SERVICE_PROCESS_FILEPATH;
    require(entryFilePath);
  }
}

module.exports = InstallServiceBootstrap;
