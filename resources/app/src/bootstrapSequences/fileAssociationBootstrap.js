/* eslint-disable global-require */

const BaseBootstrap = require('./baseBootstrap');
const unityHubProtocolHandler = require('../unityhubProtocolHandler');
const hubIPCService = require('../services/localIPC/hubIPCService');
const logger = require('../logger')('fileAssociationBootstrap');

class FileAssociationBootstrap extends BaseBootstrap {
  static async start() {
    let url;
    process.argv.forEach((arg) => {
      if (arg.includes(unityHubProtocolHandler.fullProtocolName)) {
        url = arg;
      }
    });

    await this._sendURLToMainInstance(url);
  }

  static async _sendURLToMainInstance(url) {
    if (!url) {
      logger.error('Could not find argument with Hub protocol.');
      return;
    }

    const ipcClient = await hubIPCService.connect();

    ipcClient.on('hub-protocol:url-handled', () => {
      logger.info(`${url} was handled, closing the file association process`);
      process.exit(0);
    });

    logger.info(`Sending handle url request to main thread for ${url}`);
    ipcClient.emit('hub-protocol:handle-url', { url });
  }
}

module.exports = FileAssociationBootstrap;
