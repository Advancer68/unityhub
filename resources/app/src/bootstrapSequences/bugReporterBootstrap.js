/* eslint-disable global-require */

const BaseBootstrap = require('./baseBootstrap');
const logger = require('../logger')('bugReporterBootstrap');
const path = require('path');

class BugReporterBootstrap extends BaseBootstrap {
  static start() {
    logger.info('start bug reporter');
    const entryFilePath = path.resolve(__dirname, '..', 'bugReporter', 'src', 'bugReporter.js');
    require(entryFilePath).start();
  }
}

module.exports = BugReporterBootstrap;
