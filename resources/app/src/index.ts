import v8 from 'v8';
import { app } from 'electron';
import Bootstrap from './bootstrap';

v8.setFlagsFromString('--harmony');

new Bootstrap(app).start();
