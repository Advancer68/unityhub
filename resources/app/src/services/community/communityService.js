const communityConfig = require('./defaultCommunity');
const localStorage = require('electron-json-storage');
const axios = require('axios');
const promisify = require('es6-promisify');
const auth = require('../localAuth/auth');

class CommunityService {
  constructor() {
    this.showCommunity = communityConfig.showCommunity;
    this.endpoint = 'https://connect.unity.com/api/hub/lastUpdateTime';
  }

  setShowCommunity(show) {
    this.showCommunity = show;
  }

  getShowCommunity() {
    return this.showCommunity;
  }

  setLastViewTime(time = new Date()) {
    localStorage.set('lastViewTime', { lastViewTime: time });
  }

  async getLastViewTime() {
    const hasKey = await promisify(localStorage.has)('lastViewTime');
    if (hasKey) {
      const lastViewTime = await promisify(localStorage.get)('lastViewTime');
      return Promise.resolve(lastViewTime.lastViewTime);
    }

    const date = new Date();
    this.setLastViewTime(date);
    return Promise.resolve(date.toJSON());
  }

  getLastUpdateTime() {
    return auth.getUserInfo().then(user => {
      var userId = JSON.parse(user).userId;
      const buff = Buffer.from(userId);
      const encodedUserId = encodeURIComponent(buff.toString('base64'));
      return axios.get(this.endpoint, {
        params: {
          userID: encodedUserId
        },
        responseType: 'json'
      });
    })
      .then(response => {
        const data = JSON.parse(JSON.stringify(response.data));
        return Promise.resolve(data.lastUpdateTime);
      });
  }

  updateTimeGreaterThanViewTime() {
    var lastUpdateTime;
    var lastViewTime;

    return this.getLastUpdateTime().then((val) => {
      lastUpdateTime = val;
      return this.getLastViewTime();
    }).then((val) => {
      lastViewTime = val;
      if (lastUpdateTime > lastViewTime) {
        return true;
      }
      return false;
    });
  }
}

module.exports = new CommunityService();
