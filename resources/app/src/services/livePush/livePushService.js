const axios = require('axios');
const auth = require('../localAuth/auth');
const promisify = require('es6-promisify');
const localStorageAsync = require('electron-json-storage');
const notificationService = require('../notification/notificationService');
const logger = require('../../logger')('LivePush');

const localStorage = {
  get: promisify(localStorageAsync.get, localStorageAsync),
  set: promisify(localStorageAsync.set, localStorageAsync),
};
const LIVEPUSH_CONNECT_USERID = 'live_push_connect_user_id';
const checkLivePushDuration = 1000 * 60 * 10;

class LivePushService {
  constructor() {
    this.basePath = 'https://connect.unity.cn';
    this.authEndpoint = `${this.basePath}/api/hub/login`;
    this.endpoint = `${this.basePath}/api/hub/livePush`;
  }

  init(windowManager, licenseClient) {
    setInterval(() => {
      this.getLivePushes().then(push => {
        if (push) {
          this.showNotification(windowManager, licenseClient, push);
        }
      }).catch(err => {
        logger.error('fail to ge live pushes:', err);
      });
    }, checkLivePushDuration);
  }

  showNotification(windowManager, licenseClient, notification) {
    const { title, body, openType, link, showType, inlineAction } = notification;
    if (showType === 'system') {
      notificationService.showsNotification({
        title,
        body,
        click: () => {
          if (openType === 'external_link') {
            windowManager.openExternal(link);
          } else {
            windowManager.mainWindow.show('community');
            windowManager.mainWindow.loadPage('community');
          }
        },
      });
    } else if (showType === 'inline') {
      if (inlineAction === 'remove_license') {
        licenseClient.reset();
      }

      const encodedTitle = encodeURIComponent(title);
      const encodedLink = encodeURIComponent(link);
      windowManager.mainWindow.show('community', `showModel=true&title=${encodedTitle}&link=${encodedLink}`);
    }
  }

  getConnectUserIDStoreKey(authId) {
    return  `${LIVEPUSH_CONNECT_USERID}_${authId}`;
  }

  async getConnectUserID(authId) {
    const key = this.getConnectUserIDStoreKey(authId);
    const connectUserId = await localStorage.get(key);

    return (connectUserId && connectUserId.value);
  }

  async setConnectUserID(authId, connectUserId) {
    const key = this.getConnectUserIDStoreKey(authId);

    return await localStorage.set(key, { value: connectUserId });
  }

  async getConnectUserIdByToken(accessToken) {
    return await axios.post(this.authEndpoint, null, {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    });
  }

  async getLivePushes() {
    let user = await auth.getUserInfo();
    if (!user) {
      return Promise.resolve();
    }

    user = JSON.parse(user);
    if (!user.userId) {
      return Promise.resolve();
    }

    let connectUserId = await this.getConnectUserID(user.userId);

    if (!connectUserId) {
      const userIdResponse = await this.getConnectUserIdByToken(user.accessToken);
      connectUserId = userIdResponse.data;
      this.setConnectUserID(user.userId, connectUserId);
    }

    const buff = Buffer.from(connectUserId);
    const encodedUserId = encodeURIComponent(buff.toString('base64'));

    const livePushResponse = await axios.get(this.endpoint, {
      params: {
        userId: encodedUserId
      },
      responseType: 'json'
    });

    return Promise.resolve(livePushResponse.data);
  }
}

module.exports = new LivePushService();
