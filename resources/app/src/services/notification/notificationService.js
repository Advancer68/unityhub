const { Notification } = require('electron');
const notificationConfig = require('./notificationConfig');
const windowManager = require('../../windowManager/windowManager');

let lastShowTime = 0;

class NotificationService {

  async showsNotification({ title, body, icon, click }) {
    const enabled = await notificationConfig.notificationEnabled();

    if (enabled && Notification.isSupported()) {
      const notification = new Notification({
        title,
        body,
        silent: false,
        icon,
      });
      notification.on('click', () => {
        if (click) {
          click();
        }
      });
      notification.show();
    }
  }

  showSimpleNotification(title, body, duration = 0) {
    const now = new Date().getTime();

    if (!windowManager.mainWindow.isVisible() && now - lastShowTime >= duration) {
      
      lastShowTime = now;

      this.showsNotification({
        title,
        body,
        click: () => {
          windowManager.mainWindow.show();
        },
      });
    }
  }
}

module.exports = new NotificationService();
