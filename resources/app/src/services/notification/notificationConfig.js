const promisify = require('es6-promisify');
const localStorageAsync = require('electron-json-storage');
const i18nHelper = require('../../i18nHelper');

const localStorage = {
  get: promisify(localStorageAsync.get, localStorageAsync),
  set: promisify(localStorageAsync.set, localStorageAsync),
};

const NOTIFICATION_STORAGE_KEY = 'notificationConfig';

class notificationConfig {

  getAvailableSettings() {
    return [
      { name: i18nHelper.i18n.translate('NOTIFICATION.RECEIVE_ALL'), key: 'all' },
      { name: i18nHelper.i18n.translate('NOTIFICATION.RECEIVE_NOTHING'), key: 'nothing' },
    ];
  }

  async getCurrentSetting() {
    const setting = await localStorage.get(NOTIFICATION_STORAGE_KEY);
    return (setting && setting.key) || 'all';
  }

  async setSetting(key) {
    return await localStorage.set(NOTIFICATION_STORAGE_KEY, { key });
  }

  async notificationEnabled() {
    const setting = await this.getCurrentSetting();
    return setting === 'all';
  }
}

module.exports = new notificationConfig();
