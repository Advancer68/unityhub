const releaseFileIO = require('./releaseFileIO');
const { fs } = require('../../../fileSystem');


// test libs
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = chai.expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);
chai.use(chaiAsPromised);

describe('releaseFileIO', () => {
  let sandbox;

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    sandbox.stub(fs, 'writeFile');
    sandbox.stub(fs, 'readFile');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('read', () => {
    describe('when param are valid', () => {
      it('should be fulfilled', () => {
        // todo
      });
    });

    describe('when param are invalid', () => {
      // todo
    });
  });

  describe('write', () => {
    describe('when param are valid', () => {
      it('should be fulfilled', () => {
        // todo
      });
    });

    describe('when param are invalid', () => {
      // todo
    });
  });

});