const { fs } = require('../../../fileSystem');
const promisify = require('es6-promisify');
const _ = require('lodash');
const path = require('path');
const logger = require('../../../logger')('Installer:ReleaseFileIO');
const errors = require('../errors');

const manager = {

  write(installer, installedModules, overrideSelected = true) {
    const destination = installer.destinationPath || installer.baseInstallPath;
    const modulesFilePath = path.join(destination, this.MODULES_FNAME);
    const modules = updateSelected(installer.editor.modules, installedModules, overrideSelected);

    logger.info(`write ${modulesFilePath}`);
    return promisify(fs.writeFile)(modulesFilePath, JSON.stringify(modules))
      .catch(error => {
        logger.warn('Failed to write to modules.json file.', error);

        return Promise.reject({ error: errors.COULD_NOT_PERSIST_MODULES });
      });
  },

  read(filepath) {
    logger.info(`read ${filepath}`);
    return promisify(fs.readFile)(filepath).then(data => JSON.parse(data));
  },

  MODULES_FNAME: 'modules.json'

};

function updateSelected(editorModules, newModules, overrideSelected) {
  const modules = editorModules.map((module) => {
    const moduleSelected = !!_.find(newModules, { id: module.id });
    const selected = overrideSelected ? moduleSelected : module.selected || moduleSelected;
    return Object.assign({}, module, { selected });
  });
  return modules;
}

module.exports = manager;
