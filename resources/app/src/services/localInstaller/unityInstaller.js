const path = require('path');
const rimraf = require('rimraf');
const tools = require('../editorApp/platformtools');
const promisify = require('es6-promisify');
const { hubFS } = require('../../fileSystem');
const logger = require('../../logger')('Installer');
const errors = require('./errors.js');
const windowManager = require('../../windowManager/windowManager');
const outputService = require('../outputService');

class UnityInstaller {
  constructor() {
    this.installerEvents = {
      QUEUED: 'installer.queued',
      START: 'installer.start',
      END: 'installer.end',
      MODULES_END: 'installer.modules.end',
      CANCEL: 'installer.cancel',
      WARNING: 'installer.warning'
    };
    this.uninstallerEvents = {
      QUEUED: 'editor-uninstall.queued',
      START: 'editor-uninstall.start',
      CANCEL: 'editor-uninstall.cancel'
    };

    this.platformDependent = tools.require(`${__dirname}/platform_dependent/unityInstaller`);

    this.jobs = [];
    this.jobQueue = Promise.resolve();
    this.jobStatus = { QUEUED: 'QUEUED', INSTALLING: 'INSTALLING', UNINSTALLING: 'UNINSTALLING' };
    this.errors = errors;
    this.UNITY_PATH_PLACEHOLDER = '{UNITY_PATH}';
  }

  installEditor(editor, installerPath, destinationPath, modules) {
    if (editor === undefined) {
      return Promise.resolve({ error: errors.UNKNOWN_INSTALLER });
    }

    logger.info(`Install ${editor.version}: ${installerPath} to ${destinationPath} started`);
    const id = editor.version;

    this.jobs[id] = {
      editor,
      installerPath,
      destinationPath,
      modules,
      cancelled: false,
      status: this.jobStatus.QUEUED,
      jobType: 'editorInstall'
    };

    this.jobQueue = this.jobQueue.then(() => {
      const currentInstaller = this.jobs[id];
      if (!currentInstaller || currentInstaller.cancelled) {
        delete this.jobs[id];
        return Promise.resolve({ cancelled: true });
      }

      if (!hubFS.isFilePresent(currentInstaller.installerPath)) {
        logger.info('The installer file does not exist');
        delete this.jobs[id];
        return Promise.resolve({ error: this.errors.INSTALLER_MISSING });
      }

      outputService.notifyContent(this.installerEvents.START, id);
      currentInstaller.status = this.jobStatus.INSTALLING;

      currentInstaller.modules = this.fixModulesPath(currentInstaller, currentInstaller.destinationPath);

      return this.platformDependent.installEditorAndModules(currentInstaller)
        .then((response) => this.clean(id, currentInstaller.installerPath).then(() => response));
    });

    if (Object.keys(this.jobs).length > 1) {
      windowManager.broadcastContent(this.installerEvents.QUEUED, id);
    }

    return this.jobQueue;
  }

  installModules(editor, baseInstallPath, modules) {
    if (editor === undefined) {
      return Promise.resolve({ error: errors.UNKNOWN_EDITOR });
    }

    logger.info(`Install modules for the Editor ${editor.version}`);
    const editorId = editor.version;

    this.jobs[editorId] = {
      editor,
      baseInstallPath,
      modules,
      cancelled: false,
      status: this.jobStatus.QUEUED,
      jobType: 'moduleInstall'
    };

    this.jobQueue = this.jobQueue.then(() => {
      const currentInstaller = this.jobs[editorId];

      if (!currentInstaller || currentInstaller.cancelled) {
        delete this.jobs[editorId];
        return Promise.resolve({ cancelled: true });
      }

      windowManager.broadcastContent(this.installerEvents.START, editorId);
      currentInstaller.status = this.jobStatus.INSTALLING;

      currentInstaller.modules = this.fixModulesPath(currentInstaller, currentInstaller.baseInstallPath);

      return this.platformDependent.installModules(currentInstaller)
        .then((response) => this.clean(editorId, currentInstaller.modules[0].installerPath).then(() => response));
    });

    if (Object.keys(this.jobs).length > 1) {
      windowManager.broadcastContent(this.installerEvents.QUEUED, editorId);
    }

    return this.jobQueue;
  }

  fixModulesPath(currentInstaller, baseInstallPath) {
    // Replace unity base install path placeholder.
    if (currentInstaller.modules) {
      currentInstaller.modules.forEach((module) => {
        if (module.destination) {
          module.destination = module.destination.replace(this.UNITY_PATH_PLACEHOLDER, baseInstallPath);
        }
        if (module.renameFrom) {
          module.renameFrom = module.renameFrom.replace(this.UNITY_PATH_PLACEHOLDER, baseInstallPath);
        }
        if (module.renameTo) {
          module.renameTo = module.renameTo.replace(this.UNITY_PATH_PLACEHOLDER, baseInstallPath);
        }
      });
    }

    return currentInstaller.modules;
  }

  cancel(id) {
    if (!this.jobs[id]) {
      return Promise.resolve();
    }
    delete this.jobs[id].status;
    this.jobs[id].cancelled = true;

    let installerDirectory = '';
    if (this.jobs[id].installerPath) {
      installerDirectory = path.dirname(this.jobs[id].installerPath);
    } else if (this.jobs[id].modules && this.jobs[id].modules.length > 0) {
      installerDirectory = path.dirname(this.jobs[id].modules[0].installerPath);
    } else {
      logger.warn('failed to find the installer path.');
      return Promise.resolve();
    }

    return promisify(rimraf)(installerDirectory).catch(err => {
      logger.warn(err);
    });
  }

  clean(installerId, installerPath) {
    delete this.jobs[installerId];
    const installerDirectory = path.dirname(installerPath);
    logger.debug(`Trying to delete the files from ${installerDirectory}`);
    return promisify(rimraf)(installerDirectory)
      .catch(err => {
        logger.debug(`Could not delete the files from ${installerDirectory}`);
        logger.warn(err);
      });
  }

  /**
   *
   * @param version ex.: 2017.1.2f1
   * @param installationPath up to the version (ex.:  C:\Program Files\Unity\Hub\Editor\2017.1.2f1)
   * @returns {*}
   */
  uninstall(version, installationPath) {
    logger.info(`Uninstall the Editor ${version} from ${installationPath} started`);

    // We are using the installationPath instead of version for uninstall, because in case that we have 2 editors with the same version,
    // we want to uninstall both of them. Using the version, the second one reaching this function, will be cancelled.
    // https://jira.hq.unity3d.com/browse/HUB-1267 is suggesting to apply the same logic for install as well
    this.jobs[installationPath] = {
      version,
      installationPath,
      cancelled: false,
      status: this.jobStatus.QUEUED,
      jobType: 'editorUninstall'
    };

    this.jobQueue = this.jobQueue.then(() => {
      const currentUninstaller = this.jobs[installationPath];
      if (!currentUninstaller || currentUninstaller.cancelled) {
        delete this.jobs[installationPath];
        return Promise.resolve({ cancelled: true });
      }

      windowManager.broadcastContent(this.uninstallerEvents.START, version);
      currentUninstaller.status = this.jobStatus.UNINSTALLING;

      return this.platformDependent.uninstall(version, installationPath)
        .catch((error) => {
          delete this.jobs[installationPath];
          return Promise.reject(error);
        })
        .then((response) => {
          delete this.jobs[installationPath];
          return response;
        });
    });

    if (Object.keys(this.jobs).length > 1) {
      windowManager.broadcastContent(this.uninstallerEvents.QUEUED, version);
    }

    return this.jobQueue;
  }
}

module.exports = new UnityInstaller();
