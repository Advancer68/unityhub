const errors = require('../errors.js');
const { fs } = require('../../../fileSystem');
const proc = require('child_process');
const zipUtility = require('../../../common/zip-utility');
const os = require('os');
const ws = require('windows-shortcuts');

const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
  .use(require('sinon-chai'))
  .use(require('chai-as-promised'));

const proxyquire = require('proxyquire');
// test only on windows
if (os.platform() === 'win32') {
  const registry = require('@unityhub/unity-editor-registry');
  
  describe('UnityInstallSteps_win', () => {
    let unityInstallSteps;
    let sandbox;

    beforeEach(() => {
      sandbox = sinon.sandbox.create();
      sandbox.stub(process.env, 'ProgramData').value('C:\\ProgramData');
    });

    afterEach(() => {
      sandbox.restore();
    });


    describe('when on a 32-bit system', () => {
      beforeEach(() => {
        let rimrafStub = {};
        let archStub = function () {
          return 'x86'
        };
        unityInstallSteps = proxyquire('./unityInstallSteps_win', {'rimraf': rimrafStub, 'arch': archStub});
      });

      describe('beforeEditorInstallation', () => {
        it('should give an invalid arch error', () => {
          return unityInstallSteps.beforeEditorInstallation({})
            .then(() => expect('should not').to.eql('come here'))
            .catch((e) => expect(e.error).to.eq(errors.INVALID_ARCHITECTURE));
        });
      });
    });

    describe('when on a 64-bit system with admin privileges', () => {
      let fsStub, isAdminSpy, fsMoveStub;
      beforeEach(() => {
        let rimrafStub = {};
        let archStub = function () {
          return 'x64'
        };
        let isAdmin = function () {
          return Promise.resolve(true);
        };
        isAdminSpy = sandbox.spy(isAdmin);
        unityInstallSteps = proxyquire('./unityInstallSteps_win', {
          'rimraf': rimrafStub,
          'arch': archStub,
          'is-admin': isAdmin
        });
      });

      describe('beforeEditorInstallation', () => {
        it('should create the destination folder', () => {
          fsStub = sandbox.stub(fs, 'mkdirs').resolves();
          return unityInstallSteps.beforeEditorInstallation({destinationPath: 'dest'})
            .then(() => expect(fsStub).to.have.been.calledWith('dest'))
            .catch((e) => expect('should not').to.eql(`come here ${e}`));
        });

        it('if it cannot create the folder, it should give an error', () => {
          sandbox.stub(fs, 'mkdirs').rejects();
          return unityInstallSteps.beforeEditorInstallation({destinationPath: 'dest'})
            .then(() => expect('should not').to.eql('come here'))
            .catch((e) => expect(e.error).to.eq(errors.INVALID_DESTINATION));
        });
      });

      describe('beforeModuleInstallation', () => {
        it('should create the destination folder, if one is given', () => {
          fsStub = sandbox.stub(fs, 'mkdirs').resolves();
          return unityInstallSteps.beforeModuleInstallation({destination: 'dest'})
            .then(() => expect(fsStub).to.have.been.calledWith('dest'))
            .catch((e) => expect('should not').to.eql(`come here ${e}`));
        });

        it('should not create the destination folder, if no one is given', () => {
          fsStub = sandbox.stub(fs, 'mkdirs').resolves();
          return unityInstallSteps.beforeModuleInstallation({other: 'properties'})
            .then(() => expect(fsStub).to.not.have.been.called)
            .catch((e) => expect('should not').to.eql(`come here ${e}`));
        });

        it('if it cannot create the folder, it should give an error', () => {
          sandbox.stub(fs, 'mkdirs').rejects();
          return unityInstallSteps.beforeModuleInstallation({destination: 'dest'})
            .then(() => expect('should not').to.eql('come here'))
            .catch((e) => expect(e.error).to.eq(errors.INVALID_DESTINATION));
        });
      });

      describe('execInstaller', () => {
        it('when no installation file, should give an error', () => {
          return unityInstallSteps.execInstaller('', '', 'dest')
            .then(() => expect('should not').to.eql('come here'))
            .catch((e) => expect(e.error).to.eq(errors.WRONG_INSTALLER));
        });
        it('when an unknown installation file is given, should give an error', () => {
          return unityInstallSteps.execInstaller('unknown.unknown', '', 'dest')
            .then(() => expect('should not').to.eql('come here'))
            .catch((e) => expect(e.error).to.eq(errors.WRONG_INSTALLER));
        });

        it('when an exe installation file is given, should try to install it', () => {
          const execStub = () => {
            const _self = sandbox.stub(proc, 'exec').callsFake((cmd, options, callback) => {
              _self.cmd = cmd;
              process.nextTick(callback);
            });
            return _self;
          };
          const exec = execStub();
          return unityInstallSteps.execInstaller('installer.exe', '', 'destination/path')
            .then(() => expect(exec.cmd).to.eq('"installer.exe" /S /D=destination/path'))
            .catch((e) => expect('should not').to.eql(`come here ${e}`));
        });
        it('when an exe installation file is given with parameters, should try to install it', () => {
          const execStub = () => {
            const _self = sandbox.stub(proc, 'exec').callsFake((cmd, options, callback) => {
              _self.cmd = cmd;
              process.nextTick(callback);
            });
            return _self;
          };
          const exec = execStub();
          return unityInstallSteps.execInstaller('installer.exe', '/Q', 'destination/path')
            .then(() => expect(exec.cmd).to.eq('"installer.exe" /Q /D=destination/path'))
            .catch((e) => expect('should not').to.eql(`come here ${e}`));
        });

        it('when a zip installation file is given, should try to install it', () => {
          const installFn = sandbox.stub(zipUtility, 'unzip').resolves();
          return unityInstallSteps.execInstaller('path\\to\\installer.zip', '', 'destination\\path')
            .then(() => expect(installFn).to.have.been.calledWith('path\\to\\installer.zip', 'destination\\path'))
            .catch((e) => expect('should not').to.eql(`come here ${e}`));
        });
        it('when a zip installation file is given but an error occurs, should give an error', () => {
          sandbox.stub(zipUtility, 'unzip').callsFake(() => {
            return Promise.reject('fail');
          });
          return unityInstallSteps.execInstaller('path\\to\\installer.zip', '', 'destination\\path')
            .then(() => expect('should not').to.eql('come here'))
            .catch((e) => expect(e.error).to.eq(errors.INSTALLER_ERROR));
        });

        it('when a po installation file is given (language packs), should try to install it', () => {
          const installFn = sandbox.stub(fs, 'copy').resolves();
          return unityInstallSteps.execInstaller('path\\to\\installer.po', '', 'destination/path')
            .then(() => expect(installFn).to.have.been.calledWith('path\\to\\installer.po', 'destination\\path\\installer.po'))
            .catch((e) => expect('should not').to.eql(`come here ${e}`));
        });

        it('when a po installation file is given (language packs) without a destination, should give an error', () => {
          return unityInstallSteps.execInstaller('path\\to\\installer.po', '', '')
            .then(() => expect('should not').to.eql('come here'))
            .catch((e) => expect(e.error).to.eq(errors.MISSING_DESTINATION));
        });

        it('when a tgz installation file is given (templates) without a destination, should give an error', () => {
          return unityInstallSteps.execInstaller('path/to/installer.tgz', '', '')
            .then(() => expect('should not').to.eql('come here'))
            .catch((e) => expect(e.error).to.eq(errors.MISSING_DESTINATION));
        });
      });

      describe('afterEditorInstallation', () => {
        it('if no installerPath given, should return an error', () => {
          return unityInstallSteps.afterEditorInstallation({})
            .then(() => expect('should not').to.eql('come here'))
            .catch((e) => expect(e.error).to.eq(errors.INSTALLER_MISSING));
        });

        it('if not a zip installation, should resolve right away', () => {
          sandbox.stub(proc, 'exec').callsFake((cmd, options, callback) => {
            process.nextTick(callback);
          });
          return unityInstallSteps.afterEditorInstallation({installerPath: 'installer.exe'})
            .then(() => expect(isAdminSpy).to.not.have.been.called)
            .catch((e) => expect('should not').to.eql(`come here ${e}`));
        });

        describe('if it is a zip installation, should try to do all what the installer does', () => {
          let mkdirsStub, fsRemoveStub, outputFileStub, exec, wsCreate;
          beforeEach(() => {
            sandbox.stub(registry, 'setString');
            sandbox.stub(registry, 'getString');
            sandbox.stub(registry, 'getUInt32').returns(0);
            sandbox.stub(registry, 'setUInt32');
            mkdirsStub = sandbox.stub(fs, 'mkdirs').resolves();
            fsRemoveStub = sandbox.stub(fs, 'remove').resolves();
            outputFileStub = sandbox.stub(fs, 'outputFile').resolves();
            const execStub = () => {
              const _self = sandbox.stub(proc, 'exec').callsFake((cmd, options, callback) => {
                _self.cmd = cmd;
                process.nextTick(callback);
              });
              return _self;
            };
            exec = execStub();
            const wsCreateStub = () => {
              const _self = sandbox.stub(ws, 'create').callsFake((link, options, callback) => {
                _self.link = link;
                process.nextTick(callback);
              });
              return _self;
            };
            wsCreate = wsCreateStub();
          });

          it('with a beta editor version > 2017', () => {
            return unityInstallSteps.afterEditorInstallation({
              installerPath: 'installer.zip',
              destinationPath: 'destination\\path',
              editor: {
                version: '2018.2.1b1'
              }
            })
              .then(() => {
                const exe = 'destination\\path\\Editor\\Unity.exe';
                expect(mkdirsStub).to.have.been.calledWithMatch('Start Menu\\Programs\\Unity 2018.2.1b1');
                expect(fsRemoveStub).to.have.been.calledWith('destination\\path\\ExternalInstallers');
                expect(wsCreate.link).to.contain('.lnk');
                expect(outputFileStub).to.have.been.calledWithMatch('Unity Release Notes.url');
                expect(exec).to.have.been.calledWithMatch(`delete rule name=all program="${exe}"`);
                expect(exec).to.have.been.calledWithMatch(`add rule name="Unity 2018.2.1b1 Editor" dir=in action=allow program="${exe}" profile=domain`);
                expect(exec).to.have.been.calledWithMatch(`add rule name="Unity 2018.2.1b1 Editor" dir=in action=block program="${exe}" profile=public`);
                expect(exec).to.have.been.calledWithMatch('vcredist2010\\builds\\vcredist_x64.exe');
                expect(exec).to.have.been.calledWithMatch('vcredist2013\\builds\\vcredist_x64.exe');
                expect(exec).to.have.been.calledWithMatch('vcredist2015\\builds\\vc_redist.x64.exe');
                expect(exec).to.have.been.calledWithMatch('featurename:NetFx3');
                expect(exec).to.have.been.calledWithMatch('dotNetFx45_Full_setup.exe');
                expect(registry.setString.callCount).to.eql(23);
                expect(registry.setString).to.have.been.calledWithMatch('Installer\\Unity 2018.2.1b1\\', 'Location x64', exe);
                expect(registry.setString).to.have.been.calledWithMatch('Installer\\Unity 2018.2.1b1\\', 'Version', '2018.2.1b1');
              })
              .catch((e) => expect('should not').to.eql(`come here ${e} ${e.stack}`));
          });
          it('with an editor version > 2017', () => {
            return unityInstallSteps.afterEditorInstallation({
              installerPath: 'installer.zip',
              destinationPath: 'destination\\path',
              editor: {
                version: '2018.2.1f1'
              }
            })
              .then(() => {
                const exe = 'destination\\path\\Editor\\Unity.exe';
                expect(mkdirsStub).to.have.been.calledWithMatch('Start Menu\\Programs\\Unity 2018.2.1f1');
                expect(fsRemoveStub).to.have.been.calledWith('destination\\path\\ExternalInstallers');
                expect(wsCreate.link).to.contain('.lnk');
                expect(outputFileStub).to.have.been.calledWithMatch('Unity Release Notes.url');
                expect(exec).to.have.been.calledWithMatch(`delete rule name=all program="${exe}"`);
                expect(exec).to.have.been.calledWithMatch(`add rule name="Unity 2018.2.1f1 Editor" dir=in action=allow program="${exe}" profile=domain`);
                expect(exec).to.have.been.calledWithMatch(`add rule name="Unity 2018.2.1f1 Editor" dir=in action=block program="${exe}" profile=public`);
                expect(exec).to.have.been.calledWithMatch('vcredist2010\\builds\\vcredist_x64.exe');
                expect(exec).to.have.been.calledWithMatch('vcredist2013\\builds\\vcredist_x64.exe');
                expect(exec).to.have.been.calledWithMatch('vcredist2015\\builds\\vc_redist.x64.exe');
                expect(exec).to.have.been.calledWithMatch('featurename:NetFx3');
                expect(exec).to.have.been.calledWithMatch('dotNetFx45_Full_setup.exe');
                expect(registry.setString.callCount).to.eql(24);
                expect(registry.setString).to.have.been.calledWithMatch('Installer\\Unity 2018.2.1f1\\', 'Location x64', exe);
                expect(registry.setString).to.have.been.calledWithMatch('Installer\\Unity 2018.2.1f1\\', 'Version', '2018.2.1f1');
                expect(registry.setString).to.have.been.calledWithMatch('Installer\\Unity\\', 'Location x64', exe);
              })
              .catch((e) => expect('should not').to.eql(`come here ${e} ${e.stack}`));
          });
          it('with an editor version <= 2017', () => {
            return unityInstallSteps.afterEditorInstallation({
              installerPath: 'installer.zip',
              destinationPath: 'destination\\path',
              editor: {
                version: '2017.3.1f1'
              }
            })
              .then(() => {
                const exe = 'destination\\path\\Editor\\Unity.exe';
                const nodeExe = 'destination\\path\\Editor\\Data\\Tools\\nodejs\\node.exe';
                expect(mkdirsStub).to.have.been.calledWithMatch('Start Menu\\Programs\\Unity 2017.3.1f1');
                expect(fsRemoveStub).to.have.been.calledWith('destination\\path\\ExternalInstallers');
                expect(wsCreate.link).to.contain('.lnk');
                expect(outputFileStub).to.have.been.calledWithMatch('Unity Release Notes.url');
                expect(exec).to.have.been.calledWithMatch(`delete rule name=all program="${exe}"`);
                expect(exec).to.have.been.calledWithMatch(`add rule name="Unity 2017.3.1f1 Editor" dir=in action=allow program="${exe}" profile=domain`);
                expect(exec).to.have.been.calledWithMatch(`add rule name="Unity 2017.3.1f1 Editor" dir=in action=block program="${exe}" profile=public`);
                expect(exec).to.have.been.calledWithMatch(`delete rule name=all program="${nodeExe}"`);
                expect(exec).to.have.been.calledWithMatch(`add rule name="Unity 2017.3.1f1 Package Manager" dir=in action=allow program="${nodeExe}" profile=domain`);
                expect(exec).to.have.been.calledWithMatch(`add rule name="Unity 2017.3.1f1 Package Manager" dir=in action=block program="${nodeExe}" profile=public`);
                expect(exec).to.have.been.calledWithMatch('vcredist2010\\builds\\vcredist_x64.exe');
                expect(exec).to.have.been.calledWithMatch('vcredist2013\\builds\\vcredist_x64.exe');
                expect(exec).to.have.been.calledWithMatch('vcredist2015\\builds\\vc_redist.x64.exe');
                expect(exec).to.have.been.calledWithMatch('featurename:NetFx3');
                expect(exec).to.have.been.calledWithMatch('dotNetFx45_Full_setup.exe');
                expect(registry.setString.callCount).to.eql(24);
                expect(registry.setString).to.have.been.calledWithMatch('Installer\\Unity 2017.3.1f1\\', 'Location x64', exe);
                expect(registry.setString).to.have.been.calledWithMatch('Installer\\Unity 2017.3.1f1\\', 'Version', '2017.3.1f1');
                expect(registry.setString).to.have.been.calledWithMatch('Installer\\Unity\\', 'Location x64', exe);
              })
              .catch((e) => expect('should not').to.eql(`come here ${e} ${e.stack}`));
          });
        });
      });

      describe('afterModuleInstallation', () => {
        it('should rename the folder if renameFrom and renameTo are given', () => {
          const fsMoveStub = sandbox.stub(fs, 'move').resolves();
          return unityInstallSteps.afterModuleInstallation({renameFrom: 'from', renameTo: 'to'})
            .then(() => expect(fsMoveStub).to.have.been.calledWith('from', 'to'))
            .catch((e) => expect('should not').to.eql(`come here ${e}`));
        });

        it('should rename the folder if renameTo contains the renameFrom', () => {
          fsStub = sandbox.stub(fs, 'move').resolves();
          return unityInstallSteps.afterModuleInstallation({renameFrom: 'from/folder/here', renameTo: 'from/folder'})
            .then(() => expect(fsStub).to.have.been.called)
            .catch((e) => expect('should not').to.eql(`come here ${e}`));
        });

        it('should not try to rename the folder when renameFrom is missing', () => {
          const fsMoveStub = sandbox.stub(fs, 'move').resolves();
          return unityInstallSteps.afterModuleInstallation({renameTo: 'to'})
            .then(() => expect(fsMoveStub).to.not.have.been.called)
            .catch((e) => expect('should not').to.eql(`come here ${e}`));
        });

        it('should not try to rename the folder when renameTo is invalid', () => {
          const fsMoveStub = sandbox.stub(fs, 'move').resolves();
          return unityInstallSteps.afterModuleInstallation({renameFrom: 'from', renameTo: ''})
            .then(() => expect(fsMoveStub).to.not.have.been.called)
            .catch((e) => expect('should not').to.eql(`come here ${e}`));
        });

        it('if it cannot rename it should give an error', () => {
          sandbox.stub(fs, 'move').rejects('error');
          return unityInstallSteps.afterModuleInstallation({name: 'mod', renameFrom: 'from', renameTo: 'to'})
            .then(() => expect('should not').to.eql('come here'))
            .catch((e) => expect(e).to.eq(`Module mod after installation step failed`));
        });
      });

      describe('uninstallEditor', () => {
        it('if the editor path is not specified, it should give an error', () => {
          return unityInstallSteps.uninstallEditor('2018.1.0f1', '')
            .then(() => expect('should not').to.eql('come here'))
            .catch((e) => expect(e.error).to.eq(errors.WRONG_UNINSTALLER));
        });

        it('if the version is not specified, it should give an error', () => {
          return unityInstallSteps.uninstallEditor(undefined, 'editor\\path\\')
            .then(() => expect('should not').to.eql('come here'))
            .catch((e) => expect(e.error).to.eq(errors.WRONG_UNINSTALLER));
        });

        it('if both the version and editor paths are not specified, it should give an error', () => {
          return unityInstallSteps.uninstallEditor()
            .then(() => expect('should not').to.eql('come here'))
            .catch((e) => expect(e.error).to.eq(errors.WRONG_UNINSTALLER));
        });

        describe('when the uninstaller exists', () => {
          beforeEach(() => {
            sandbox.stub(fs, 'existsSync').returns(true);
          });

          it('it should use it', () => {
            const fsRemoveStub = sandbox.stub(fs, 'remove').resolves();
            const execStub = () => {
              const _self = sandbox.stub(proc, 'exec').callsFake((cmd, options, callback) => {
                _self.cmd = cmd;
                process.nextTick(callback);
              });
              return _self;
            };
            const exec = execStub();
            return unityInstallSteps.uninstallEditor('2018.1.0f1', 'editor\\path')
              .then(() => {
                expect(exec).to.have.been.calledWith('"editor\\path\\Editor\\Uninstall.exe" /S');
                expect(exec).to.not.have.been.calledWithMatch('reg delete');
                expect(exec).to.not.have.been.calledWithMatch('netsh advfirewall');
                expect(fsRemoveStub.callCount).to.eql(4);
                expect(fsRemoveStub).to.have.been.calledWithMatch('editor\\path');
                expect(fsRemoveStub).to.have.been.calledWithMatch('Start Menu\\Programs\\Unity 2018.1.0f1 (64-bit)');
                expect(fsRemoveStub).to.have.been.calledWithMatch('Start Menu\\Programs\\Unity 2018.1.0f1');
                expect(fsRemoveStub).to.have.been.calledWithMatch('Desktop\\Unity 2018.1.0f1.lnk');
              })
              .catch((e) => expect('should not').to.eql(`come here ${e} ${e.stack}`));
          });

          it('but removing the files failed, it should still resolve', () => {
            const fsRemoveStub = sandbox.stub(fs, 'remove').rejects();
            const execStub = () => {
              const _self = sandbox.stub(proc, 'exec').callsFake((cmd, options, callback) => {
                _self.cmd = cmd;
                process.nextTick(callback);
              });
              return _self;
            };
            const exec = execStub();
            return unityInstallSteps.uninstallEditor('2018.1.0f1', 'editor\\path')
              .then(() => {
                expect(exec).to.have.been.calledWith('"editor\\path\\Editor\\Uninstall.exe" /S');
                expect(exec).to.not.have.been.calledWithMatch('reg delete');
                expect(exec).to.not.have.been.calledWithMatch('netsh advfirewall');
                expect(fsRemoveStub.callCount).to.eql(3);
                expect(fsRemoveStub).to.have.been.calledWithMatch('editor\\path');
                expect(fsRemoveStub).to.have.been.calledWithMatch('Start Menu\\Programs\\Unity 2018.1.0f1');
                expect(fsRemoveStub).to.have.been.calledWithMatch('Desktop\\Unity 2018.1.0f1.lnk');
              })
              .catch((e) => expect('should not').to.eql(`come here ${e} ${e.stack}`));
          });

          it('but uninstaller failed, it should reject', () => {
            const fsRemoveStub = sandbox.stub(fs, 'remove').resolves();
            const execStub = () => {
              const _self = sandbox.stub(proc, 'exec').callsFake((cmd, options, callback) => {
                _self.cmd = cmd;
                process.nextTick(callback('Error'));
              });
              return _self;
            };
            const exec = execStub();
            return unityInstallSteps.uninstallEditor('2018.1.0f1', 'editor\\path')
              .then(() => expect('should not').to.eql('come here'))
              .catch((e) => {
                expect(e).to.eq('Error');
                expect(exec).to.have.been.calledWith('"editor\\path\\Editor\\Uninstall.exe" /S');
                expect(exec).to.not.have.been.calledWithMatch('reg delete');
                expect(exec).to.not.have.been.calledWithMatch('netsh advfirewall');
                expect(fsRemoveStub.callCount).to.eql(0);
              });
          });
        });

        describe('when the uninstaller does not exist', () => {
          beforeEach(() => {
            sandbox.stub(fs, 'existsSync').returns(false);
          });

          it('it should do the steps manually', () => {
            const fsRemoveStub = sandbox.stub(fs, 'remove').resolves();
            const execStub = () => {
              const _self = sandbox.stub(proc, 'exec').callsFake((cmd, options, callback) => {
                _self.cmd = cmd;
                process.nextTick(callback);
              });
              return _self;
            };
            const exec = execStub();
            return unityInstallSteps.uninstallEditor('2018.1.0f1', 'editor\\path')
              .then(() => {
                expect(exec).to.not.have.been.calledWith('"editor\\path\\Editor\\Uninstall.exe" /S');
                expect(exec).to.have.been.calledWithMatch('reg delete');
                expect(exec).to.have.been.calledWithMatch('netsh advfirewall');
                expect(fsRemoveStub.callCount).to.eql(4);
                expect(fsRemoveStub).to.have.been.calledWithMatch('editor\\path');
                expect(fsRemoveStub).to.have.been.calledWithMatch('Start Menu\\Programs\\Unity 2018.1.0f1 (64-bit)');
                expect(fsRemoveStub).to.have.been.calledWithMatch('Start Menu\\Programs\\Unity 2018.1.0f1');
                expect(fsRemoveStub).to.have.been.calledWithMatch('Desktop\\Unity 2018.1.0f1.lnk');
              })
              .catch((e) => expect('should not').to.eql(`come here ${e} ${e.stack}`));
          });

          it('and removing the files failed, it should reject', () => {
            const fsRemoveStub = sandbox.stub(fs, 'remove').rejects('My error');
            const execStub = () => {
              const _self = sandbox.stub(proc, 'exec').callsFake((cmd, options, callback) => {
                _self.cmd = cmd;
                process.nextTick(callback);
              });
              return _self;
            };
            const exec = execStub();
            return unityInstallSteps.uninstallEditor('2018.1.0f1', 'editor\\path')
              .then(() => expect('should not').to.eql('come here'))
              .catch((e) => {
                expect(e.name).to.eq('My error');
                expect(exec).to.not.have.been.called;
                expect(fsRemoveStub.callCount).to.eql(1);
                expect(fsRemoveStub).to.have.been.calledWithMatch('editor\\path');
            });
          });

          it('and it could only erase the files, it should still resolve', () => {
            const fsRemoveStub = sandbox.stub(fs, 'remove').resolves();
            const execStub = () => {
              const _self = sandbox.stub(proc, 'exec').callsFake((cmd, options, callback) => {
                _self.cmd = cmd;
                process.nextTick(callback('Error'));
              });
              return _self;
            };
            const exec = execStub();
            return unityInstallSteps.uninstallEditor('2018.1.0f1', 'editor\\path')
              .then(() => {
                expect(exec.callCount).to.eq(2);
                expect(fsRemoveStub.callCount).to.eql(4);
                expect(fsRemoveStub).to.have.been.calledWithMatch('editor\\path');
              })
              .catch((e) => expect('should not').to.eql(`come here ${e} ${e.stack}`));
          });
        });
      });
    });

    describe('when on a 64-bit system without admin privileges', () => {
      let isAdminSpy;
      beforeEach(() => {
        let rimrafStub = {};
        let archStub = function () {
          return 'x64'
        };
        let isAdmin = function () {
          return Promise.resolve(false);
        };
        isAdminSpy = sandbox.spy(isAdmin);
        unityInstallSteps = proxyquire('./unityInstallSteps_win', {
          'rimraf': rimrafStub,
          'arch': archStub,
          'is-admin': isAdmin
        });
      });

      describe('afterEditorInstallation', () => {
        it('if no installerPath given, should return an error', () => {
          return unityInstallSteps.afterEditorInstallation({})
            .then(() => expect('should not').to.eql('come here'))
            .catch((e) => expect(e.error).to.eq(errors.INSTALLER_MISSING));
        });

        it('if not a zip installation, should resolve right away', () => {
          sandbox.stub(proc, 'exec').callsFake((cmd, options, callback) => {
            process.nextTick(callback);
          });
          return unityInstallSteps.afterEditorInstallation({installerPath: 'installer.exe'})
            .then(() => expect(isAdminSpy).to.not.have.been.called)
            .catch((e) => expect('should not').to.eql(`come here ${e}`));
        });

        describe('if it is a zip installation, should only remove the external installer folders', () => {
          let fsRemoveStub, exec;
          beforeEach(() => {
            fsRemoveStub = sandbox.stub(fs, 'remove').resolves();
            const execStub = () => {
              const _self = sandbox.stub(proc, 'exec').callsFake((cmd, options, callback) => {
                _self.cmd = cmd;
                process.nextTick(callback);
              });
              return _self;
            };
            exec = execStub();
          });

          it('with a beta editor version > 2017', () => {
            return unityInstallSteps.afterEditorInstallation({
              installerPath: 'installer.zip',
              destinationPath: 'destination\\path',
              editor: {
                version: '2018.2.1b1'
              }
            })
              .then(() => {
                expect(fsRemoveStub).to.have.been.calledWith('destination\\path\\ExternalInstallers');
                expect(exec).to.not.have.been.called;
              })
              .catch((e) => expect('should not').to.eql(`come here ${e} ${e.stack}`));
          });
        });
      });
    });
  });
}
