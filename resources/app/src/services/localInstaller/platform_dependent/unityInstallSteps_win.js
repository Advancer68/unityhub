const logger = require('../../../logger')('UnityInstallStepsWindows');
const zipUtility = require('../../../common/zip-utility');
const errors = require('../errors');
const isAdmin = require('is-admin');
const arch = require('arch');
const path = require('path');
const { fs } = require('../../../fileSystem');
const proc = require('child_process');
const registry = require('@unityhub/unity-editor-registry'); // eslint-disable-line import/no-unresolved
const ws = require('windows-shortcuts');
const ini = require('ini');
const _ = require('lodash');
const os = require('os');

/**
 * This object takes care of the implementation of
 * all the install steps on Windows
 *
 */
const unityInstallStepsWindows = {
  /**
   * Before installing an editor,
   * - verify if we are trying to install 64-bit on a 32-bit system
   * - make sure the folder needed is created
   * @param installer object{ "editor":{
   *                            "releaseType", "downloadUrl", "lts", "downloadSize", "installedSize", "modules", "version"
   *                          },
   *                          "installerPath",
   *                          "destinationPath",
   *                          "modules",
   *                          "cancelled",
   *                          "status":"INSTALLING",
   *                          "jobType":"editorInstall"
   *                        }
   * @return {*}
   */
  beforeEditorInstallation: (installer) => {
    logger.debug('beforeEditorInstallation');
    const sysArch = arch();
    if (sysArch === 'x86') {
      logger.warn('Installing a 64-bit on a 32-bit system!');
      return Promise.reject({ error: errors.INVALID_ARCHITECTURE });
    }

    return fs.mkdirs(installer.destinationPath)
      .catch((e) => {
        logger.warn(`Error while trying to create the directory of the installation ${installer.destinationPath} ${e}`);
        return Promise.reject({ error: errors.INVALID_DESTINATION });
      });
  },
  /**
   * Before adding a module to an editor,
   * - make sure the folder needed is created
   */
  beforeModuleInstallation: (module) => {
    logger.debug('beforeModuleInstallation');

    if (!module.destination) {
      logger.debug(`The module ${module.id} does not have a destination`);
      return Promise.resolve();
    }

    logger.debug(`Creating the folder of ${module.id}, if needed: ${module.destination}`);
    return fs.mkdirs(module.destination)
      .catch((e) => {
        logger.warn(`Error while trying to create the directory of the module ${module.destination} ${e}`);
        return Promise.reject({ error: errors.INVALID_DESTINATION });
      });
  },
  execInstaller: (installerPath, parameterStr, destination) => {
    logger.debug('execInstaller');
    const fileExtension = path.extname(installerPath);
    if (!destination && (fileExtension === '.po' || fileExtension === '.tgz')) {
      return Promise.reject({ error: errors.MISSING_DESTINATION });
    }
    return new Promise((resolve, reject) => {
      if (fileExtension === '.exe') {
        return resolve(installFromExe(installerPath, parameterStr, destination));
      } else if (fileExtension === '.zip') {
        return resolve(installFromZip(installerPath, destination));
      } else if (fileExtension === '.msi') {
        return resolve(installFromMsi(installerPath, parameterStr));
      } else if (fileExtension === '.po') {
        destination = path.join(destination, path.basename(installerPath));
        logger.debug(`Move ${installerPath} to ${destination}`);
        return resolve(fs.copy(installerPath, destination));
      }
      return reject({ error: errors.WRONG_INSTALLER });
    })
      .catch((err) => {
        logger.warn(err);

        // Reject with INSTALLER_ERROR if it is not a known error from error.js
        if (!err.error || !err.error.code) {
          return Promise.reject({ error: errors.INSTALLER_ERROR });
        }
        return Promise.reject(err);
      });
  },

  /**
   * if the user has Admin rights and the installation is from a zip,
   * then do what the nsis installer does:
   * - register Unity in HKCU
   * - register extensions in HKCR
   * - register uri in HKCR
   * - Install VS 2010/2013/2015 runtime if missing
   * - Install .net framework 3.5/4.5 if missing
   * - set firewall rules for unity.exe
   * - create start menu shortcuts (?)
   * - create desktop shortcut (?)
   */
  afterEditorInstallation: (installer) => {
    logger.debug('afterEditorInstallation');
    return new Promise((resolve, reject) => {
      if (installer === undefined || installer.installerPath === undefined) {
        reject({ error: errors.INSTALLER_MISSING });
        return;
      }
      if (path.extname(installer.installerPath) !== '.zip') {
        checkDotNetCorePrerequisites().then((data) => {
          resolve(data);
        });
      } else {

        isAdmin().then((admin) => {
          if (admin) {
            logger.info('The process runs with administrative privileges.');
            const exe = path.join(installer.destinationPath, 'Editor', 'Unity.exe');
            const promiseList = [
              registerUnity(installer, exe),
              registerExtension(installer, exe),
              registerUri(installer, exe),
              installVSRuntime(installer),
              installDotNetFramework(installer),
              createStartMenuShortcut(installer, exe),
              createDesktopShortcut(installer, exe),
              setFirewallRules(installer, exe)
            ];

            return Promise.all(promiseList)
              .then(() => removeExternalInstallersFolder(installer))
              .then(() => checkDotNetCorePrerequisites())
              .then((data) => resolve(data))
              .catch((err) => {
                logger.warn(`An error occurred while installing ${installer.editor.version}: ${err}`);
                reject(err);
              });
          }

          logger.info('The process does not run with administrative privileges.');
          return removeExternalInstallersFolder(installer).then(() => resolve());
        }).catch((err) => reject(err));
      }
    });
  },

  afterModuleInstallation: async (module) => {
    logger.debug('afterModuleInstallation');
    if (module.renameFrom && module.renameTo) {
      if (module.renameFrom.includes(module.renameTo)) {
        const tempFolder = path.join(path.dirname(module.renameTo), 'temp_parent');
        await fs.move(module.renameFrom, tempFolder);
        await fs.move(tempFolder, module.renameTo, { overwrite: true }).catch((err) => {
          logger.warn(`The renameTo is the parent of the renameFrom : Could not rename folder ${err}`);
          return Promise.reject(`Module ${module.name} after installation step failed`);
        });
      } else {
        await fs.move(module.renameFrom, module.renameTo).catch((err) => {
          logger.warn(`Could not rename folder ${err}`);
          return Promise.reject(`Module ${module.name} after installation step failed`);
        });
      }
    }
  },

  uninstallEditor: (version, editorRootPath) => {
    logger.info(`uninstall ${version} at ${editorRootPath}`);
    if (!editorRootPath || !version) {
      return Promise.reject({ error: errors.WRONG_UNINSTALLER });
    }

    // with the uninstaller
    const uninstallerPath = path.join(editorRootPath, 'Editor', 'Uninstall.exe');
    if (fs.existsSync(uninstallerPath)) {
      return execUninstaller(uninstallerPath)
        .catch((err) => {
          logger.warn(`An error occurred while running the uninstaller ${uninstallerPath}: ${err}`);
          return Promise.reject({ error: err, from: 'uninstaller' });
        })
        .then(() => Promise.all([
          removeStartMenuShortcut(version),
          removeDesktopShortcut(version)
        ]))
        .then(() => removeEditorFolder(editorRootPath))
        .catch((err) => {
          if (err.from === 'uninstaller') {
            return Promise.reject(err.error);
          }
          logger.warn(`An error occurred while uninstalling ${version}: Ignoring ${err}`);
          return Promise.resolve();
        });
    }

    // without the uninstaller
    return removeEditorFolder(editorRootPath)
      .catch((err) => {
        logger.warn(`An error occurred while removing the files of ${version}: ${err}`);
        return Promise.reject({ error: err, from: 'folderRemove' });
      })
      .then(() => Promise.all([
        removeStartMenuShortcut(version),
        removeDesktopShortcut(version),
        removeRegistryKeys(version, editorRootPath),
        removeFirewallRules(version, editorRootPath)
      ]))
      .catch((e) => {
        if (e.from === 'folderRemove') {
          return Promise.reject(e.error);
        }
        logger.warn(`An error occurred while uninstalling ${version}: Ignoring ${e}`);
        return Promise.resolve();
      });
  }
};

const baseStartMenuFolder = path.join(process.env.ProgramData, 'Microsoft', 'Windows', 'Start Menu', 'Programs');

function execUninstaller(uninstallerPath) {
  logger.info(`exec uninstaller at ${uninstallerPath}`);
  if (path.extname(uninstallerPath) === '.exe') {
    return new Promise((resolve, reject) => {

      proc.exec(`"${uninstallerPath}" /S`, { name: 'Unity uninstaller' },
        (error, stdout, stderr) => {
          if (error) {
            logger.warn(`An error occured while uninstalling ${uninstallerPath} ${error}`);
            reject(error);
            return;
          }

          if (stderr) {
            reject(stderr);
            return;
          }
          resolve();
        });
    });
  }

  return Promise.reject({ error: errors.WRONG_UNINSTALLER });
}

function installFromExe(installerPath, parameterStr, destination) {
  logger.debug('installFromExe');
  return new Promise((resolve, reject) => {
    let parameterOption = '/S';
    if (parameterStr && parameterStr !== '') {
      parameterOption = parameterStr;
    }
    let destinationOption = '';
    if (destination) {
      destinationOption = `/D=${destination}`;
    }
    logger.info(`install ${installerPath} ${parameterOption} ${destinationOption}`);
    proc.exec(`"${installerPath}" ${parameterOption} ${destinationOption}`, { name: 'Unity installer' },
      (error, stderr) => {
        if (error) {
          reject(error);
          return;
        }

        if (stderr) {
          reject(stderr);
          return;
        }

        resolve();
      });
  });
}
function installFromMsi(installerPath, parameterStr) {
  logger.debug('installFromMsi');
  return new Promise((resolve, reject) => {
    // insert the installer path inside the command given
    const cmd = parameterStr.replace('/i', `/i "${installerPath}"`);
    logger.info(`install ${cmd}`);
    proc.exec(`${cmd}`, { name: 'Unity installer' },
      (error, stderr) => {
        if (error) {
          reject(error);
          return;
        }

        if (stderr) {
          reject(stderr);
          return;
        }

        resolve();
      });
  });
}
function installFromZip(installerPath, destination) {
  logger.debug('installFromZip');
  if (!destination) return Promise.reject({ error: errors.MISSING_DESTINATION });

  return zipUtility.unzip(installerPath, destination)
    .then(() => logger.info('unzip finished'))
    .catch((err) => {
      logger.warn('error while deploying zip ', err);
      return Promise.reject(err);
    });
}

/**
 * The NSIS installer will generate 1 key:
 * 'HKCU\\Software\\Unity Technologies\\Installer\\Unity' 'Location x64' ${exe}
 * If the installer is for an alpha/beta version, the key will rather be:
 * 'HKCU\\Software\\Unity Technologies\\Installer\\Unity ${version}' 'Location x64' ${exe}
 *
 * In order to improve on that, this installer will always create both unless it is an alpha/beta.
 *
 * HKCU\\Software\\Unity Technologies
 *   |__ Installer
 *     |__ Unity 'Location x64' ${exe}
 *     |__ Unity 'Version' ${version}
 *   |__ Unity Editor 5.x IsMainWindowMaximized_h1679813210 1
 * HKCU\\Software\\Unity
 *   |__ WebPlayer' 'UnityWebPlayerReleaseChannel' 'Dev'
 *
 * @param installer
 * @param exe
 * @return {Promise}
 */
function registerUnity(installer, exe) {
  return new Promise((resolve, reject) => {
    logger.info(`registerUnity of ${installer.editor.version}`);
    try {
      const sysarch = arch();
      let location = 'Location x64';
      if (sysarch === 'x86') {
        location = 'Location';
      }

      // unlike the nsis, always create the key with version name
      // create HKCU/{unity version} "Location x64" {exe}
      let unityKeyName = `HKEY_CURRENT_USER\\Software\\Unity Technologies\\Installer\\Unity ${installer.editor.version}\\`;
      registry.setString(unityKeyName, location, exe);
      registry.setString(unityKeyName, 'Version', installer.editor.version);

      if (!isBetaVersion(installer.editor.version) || isAlphaVersion(installer.editor.version)) {
        // create HKCU/{unity} "Location x64" {exe} only if not beta
        unityKeyName = 'HKEY_CURRENT_USER\\Software\\Unity Technologies\\Installer\\Unity\\';
        registry.setString(unityKeyName, location, exe);
      }

      // if the registy key IsMainWindowMaximized_h1679813210 is not set, Unity editor opens up "too large".
      // If gets fixed in editor some day, we can remove this from installer
      unityKeyName = 'HKEY_CURRENT_USER\\Software\\Unity Technologies\\Unity Editor 5.x\\';
      if (registry.getUInt32(unityKeyName, 'IsMainWindowMaximized_h1679813210', 0) === 0) {
        registry.setUInt32(unityKeyName, 'IsMainWindowMaximized_h1679813210', 1);
      }

      unityKeyName = 'HKEY_CURRENT_USER\\Software\\Unity\\WebPlayer\\';
      registry.setString(unityKeyName, 'UnityWebPlayerReleaseChannel', 'Dev');

      resolve();
    } catch (e) {
      reject(e);
    }
  });
}
function registerExtension(installer, exe) {
  return new Promise((resolve, reject) => {
    logger.info(`registerExtension of ${installer.editor.version}`);
    try {
      registerFileAssociation('Unity scene file', exe, '.unity', '-open');
      registerFileAssociation('Unity package file', exe, '.unityPackage', '-open');
      resolve();
    } catch (e) {
      reject(e);
    }
  });
}
function registerUri(installer, exe) {
  return new Promise((resolve, reject) => {
    logger.info(`registerUri of ${installer.editor.version}`);
    try {
      registerURIHandler('com.unity3d.kharma', exe, '-openurl');
      resolve();
    } catch (e) {
      reject(e);
    }
  });
}

function installVSRuntime(installer) {
  logger.info(`installVSRuntime of ${installer.editor.version}`);
  return installVS2010Runtime(installer)
    .then(() => installVS2013Runtime(installer))
    .then(() => installVS2015Runtime(installer));
  // TODO: handle the return codes!
}
function installDotNetFramework(installer) {
  logger.info(`installDotNetFramework of ${installer.editor.version}`);
  return installDotNet35Framework()
    .then(() => installDotNet45Framework(installer));
}
function setFirewallRules(installer, exe) {
  logger.info(`setFirewallRules of ${installer.editor.version}`);

  const deleteRules = [
    `netsh advfirewall firewall delete rule name=all program="${exe}"`
  ];
  const addRules = [
    `netsh advfirewall firewall add rule name="Unity ${installer.editor.version} Editor" dir=in action=allow program="${exe}" profile=domain protocol=any`,
    `netsh advfirewall firewall add rule name="Unity ${installer.editor.version} Editor" dir=in action=block program="${exe}" profile=public protocol=any`
  ];

  if (installer.editor.version.substr(0, 4) <= 2017) {
    const nodeExe = path.join(installer.destinationPath, 'Editor', 'Data', 'Tools', 'nodejs', 'node.exe');
    deleteRules.push(`netsh advfirewall firewall delete rule name=all program="${nodeExe}"`);
    addRules.push(
      `netsh advfirewall firewall add rule name="Unity ${installer.editor.version} Package Manager" dir=in action=allow program="${nodeExe}" profile=domain protocol=any`,
      `netsh advfirewall firewall add rule name="Unity ${installer.editor.version} Package Manager" dir=in action=block program="${nodeExe}" profile=public protocol=any`
    );
  }

  return spawnFirewallRules(deleteRules)
    .then(() => spawnFirewallRules(addRules));
}

/**
 * HKCR/{extension} "" {description}
 * HKCR/{description} "" {description}
 *   |__shell "" 'open'
 *     |__open
 *       |__command "" '"{exePath}" {extra} "%1"'
 *     |__edit "" "Edit {description}"
 *       |__command "" '"{exePath}" {extra} "%1"'
 *   |__DefaultIcon "" '"{exePath}",0'
 *
 * @param description
 * @param exePath
 * @param extensionName
 * @param extra
 */
function registerFileAssociation(description, exePath, extensionName, extra) {
  registry.setString(`HKEY_CLASSES_ROOT\\${extensionName}\\`, '', description);
  registry.setString(`HKEY_CLASSES_ROOT\\${description}\\`, '', description);
  registry.setString(`HKEY_CLASSES_ROOT\\${description}\\shell\\`, '', 'open');
  registry.setString(`HKEY_CLASSES_ROOT\\${description}\\shell\\open\\command\\`, '', `${exePath} ${extra} "%1"`);
  registry.setString(`HKEY_CLASSES_ROOT\\${description}\\shell\\edit\\`, '', `Edit ${description}`);
  registry.setString(`HKEY_CLASSES_ROOT\\${description}\\shell\\edit\\command\\`, '', `"${exePath}" ${extra} "%1"`);
  registry.setString(`HKEY_CLASSES_ROOT\\${description}\\DefaultIcon\\`, '', `"${exePath}",0`);
}

/**
 * HKCR/{schemeName} "" URL:{schemeName}
 * HKCR/{schemeName} "URL Protocol" ""
 *   |__DefaultIcon "" {exePath}
 *   |__shell "" ""
 *     |__Open "" ""
 *       |__command "" '"{exePath}" {extra} "%1"'
 * @param schemeName
 * @param exePath
 * @param extra
 */
function registerURIHandler(schemeName, exePath, extra) {
  registry.setString(`HKEY_CLASSES_ROOT\\${schemeName}\\`, '', `URL:${schemeName}`);
  registry.setString(`HKEY_CLASSES_ROOT\\${schemeName}\\`, 'URL Protocol', '');
  registry.setString(`HKEY_CLASSES_ROOT\\${schemeName}\\DefaultIcon\\`, '', exePath);
  registry.setString(`HKEY_CLASSES_ROOT\\${schemeName}\\shell\\`, '', '');
  registry.setString(`HKEY_CLASSES_ROOT\\${schemeName}\\shell\\Open\\`, '', '');
  registry.setString(`HKEY_CLASSES_ROOT\\${schemeName}\\shell\\Open\\command\\`, '', `"${exePath}" ${extra} "%1"`);
}

function spawnFirewallRules(rulesList) {
  return Promise.all(rulesList.map((rule) => new Promise((resolve) => {
    proc.exec(rule, { name: 'Firewall rules' }, (err) => {
      if (err) {
        logger.warn(`There was an error while trying to spawn the firewall rule: ${rule} \nIgnored: ${err}`);
      }
      resolve();
    });
  })));
}

/**
 * Check for VC++ 2010 redist - see http://blogs.msdn.com/b/astebner/archive/2010/10/20/10078468.aspx
 * https://blogs.msdn.microsoft.com/astebner/2010/05/05/mailbag-how-to-detect-the-presence-of-the-visual-c-2010-redistributable-package/
 * https://www.microsoft.com/en-us/download/details.aspx?id=5555
 * Those references states to look at the HKLM\SOFTWARE\Microsoft\VisualStudio\10.0\VC\VCRedist\ registry key,
 * but from observation, it seems to have move to HKLM\SOFTWARE\Microsoft\VisualStudio\10.0_Config\VC\VCRedist\...
 *
 * Possible Return Codes:
 * 0 = success
 * 3010 = Need to restart the machine
 * other = If VC++ installation fails, show warning, but allow installation to continue
 *   1602: The user canceled installation.
 *   1603: A fatal error occurred during installation.
 *   1641: A restart is required to complete the installation. This message indicates success.
 *   5100: The user's computer does not meet system requirements. (It can be because of:
 *          A newer version of Microsoft Visual C++ 2010 Redistributable has been detected on the machine.)
 */
function installVS2010Runtime(installer) {
  return new Promise((resolve) => {
    try {
      // check in both places
      if (shouldInstallRuntime([
        `HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\VisualStudio\\10.0\\VC\\VCRedist\\${arch()}`,
        `HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\VisualStudio\\10.0_Config\\VC\\VCRedist\\${arch()}`
      ])) {
        logger.info('Need to install VS2010 Runtime');
        const runtimePath = path.join(installer.destinationPath, 'ExternalInstallers', 'Microsoft', 'vcredist2010', 'builds', `vcredist_${arch()}.exe`);
        proc.exec(`"${runtimePath}" /q /norestart`, { name: 'VC++ Runtime 2010' },
          (err) => {
            if (err) {
              if (err.code === 3010) {
                logger.warn('Installing VS2010 runtime requires a restart.');
                // TODO: Handle restart!
              } else if (err.code === 0) {
                logger.debug('Installing VS2010 runtime succeeded.');
              } else {
                logger.warn(`There was an error (code: ${err.code}) while trying to install VS2010 runtime. Ignoring ${err}`);
              }
            }
            resolve();
          });
      } else {
        resolve();
      }
    } catch (e) {
      logger.warn(`There was an error while trying to verify or install VS2010 runtime. Ignoring ${e}`);
      resolve();
    }
  });
}
/**
 * Possible Return Codes:
 * 0 = success
 * 3010 = Need to restart the machine
 * other = If VC++ installation fails, show warning, but allow installation to continue
 *   1602: The user canceled installation.
 *   1603: A fatal error occurred during installation.
 *   1641: A restart is required to complete the installation. This message indicates success.
 *   5100: The user's computer does not meet system requirements.
 */
function installVS2013Runtime(installer) {
  return new Promise((resolve) => {
    try {
      if (shouldInstallRuntime([
        `HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\VisualStudio\\12.0\\VC\\VCRedist\\${arch()}`,
        `HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\VisualStudio\\12.0_Config\\VC\\VCRedist\\${arch()}`
      ])) {
        logger.info('Need to install VS2013 Runtime');
        const runtimePath = path.join(installer.destinationPath, 'ExternalInstallers', 'Microsoft', 'vcredist2013', 'builds', `vcredist_${arch()}.exe`);
        proc.exec(`"${runtimePath}" /q /norestart`, { name: 'VC++ Runtime 2013' },
          (err) => {
            if (err) {
              if (err.code === 3010) {
                logger.warn('Installing VS2013 runtime requires a restart.');
                // TODO: Handle restart!
              } else if (err.code === 0) {
                logger.debug('Installing VS2013 runtime succeeded.');
              } else {
                logger.warn(`There was an error (code: ${err.code}) while trying to install VS2013 runtime. Ignoring ${err}`);
              }
            }
            logger.debug('Installing VS2013 runtime succeeded.');
            resolve();
          });
      } else {
        resolve();
      }
    } catch (e) {
      logger.warn(`There was an error while trying to verify or install VS2013 runtime. Ignoring ${e}`);
      resolve();
    }
  });
}
/**
 * 2015 not detectable, but throws an error if a newer version is installed,
 * so just run always and ignore the error
 *
 * Possible Return Codes:
 * 0 = success
 * 3010 = Need to restart the machine
 * 1638 = Newer version already installed
 * other = If VC++ installation fails, show warning, but allow installation to continue
 *   1602: The user canceled installation.
 *   1603: A fatal error occurred during installation.
 *   1641: A restart is required to complete the installation. This message indicates success.
 *   5100: The user's computer does not meet system requirements.
 */
function installVS2015Runtime(installer) {
  return new Promise((resolve) => {
    try {
      logger.info('Install VS2015 Runtime');
      const runtimePath = path.join(installer.destinationPath, 'ExternalInstallers', 'Microsoft', 'vcredist2015', 'builds', `vc_redist.${arch()}.exe`);
      proc.exec(`"${runtimePath}" /q /norestart`, { name: 'VC++ Runtime 2015' },
        (err) => {
          if (err) {
            if (err.code === 3010) {
              logger.warn('Installing VS2015 runtime requires a restart.');
              // TODO: Handle restart!
            } else if (err.code === 0) {
              logger.debug('Installing VS2015 runtime succeeded.');
            } else if (err.code === 1638) {
              logger.debug('VS2015 is already installed.');
            } else {
              logger.warn(`There was an error (code: ${err.code}) while trying to install VS2015 runtime. Ignoring ${err}`);
            }
          }
          resolve();
        });
    } catch (e) {
      logger.warn(`There was an error while trying to install VS2015 runtime. Ignoring ${e}`);
      resolve();
    }
  });
}

function shouldInstallRuntime(runtimeKeyNames) {
  let isInstalled = false;

  try {
    runtimeKeyNames.forEach((runtimeKeyName) => {
      if (!isInstalled) {
        if (registry.getUInt32(runtimeKeyName, 'Installed', 0) === 1) {
          isInstalled = true;
        }
      }
    });
  } catch (e) {
    logger.debug(`There was an error while trying to verify if we should install vc++ runtime : ${e}`);
  }

  return !isInstalled;
}

/**
 * @param installer
 * @param exe
 */
function createStartMenuShortcut(installer, exe) {
  logger.info(`create Start Menu Shortcut of ${installer.editor.version}`);
  return new Promise((resolve) => {
    const startMenuFolder = path.join(baseStartMenuFolder, `Unity ${installer.editor.version}`);
    let releaseNotesUrl = '';
    if (isBetaVersion(installer.editor.version)) {
      releaseNotesUrl = `https://unity3d.com/unity/beta/unity${installer.editor.version}`;
    } else if (!isAlphaVersion(installer.editor.version)) {
      releaseNotesUrl = `https://unity3d.com/unity/whatsnew/unity-${installer.editor.version.substring(0, installer.editor.version.length - 2)}`;
    }
    fs.mkdirs(startMenuFolder)
      .catch((e) => logger.warn(`Could not create the start menu folder ${e}`))
      .then(() => {
        const promiseList = [
          createShortcut(path.join(startMenuFolder, `Unity ${installer.editor.version}.lnk`), exe),
          createShortcut(path.join(startMenuFolder, 'Report a Problem with Unity.lnk'), path.join(installer.destinationPath, 'Editor', 'BugReporter', 'unity.bugreporter.exe')),
        ];
        if (releaseNotesUrl !== '') {
          promiseList.push(createInternetShortcut(path.join(startMenuFolder, 'Unity Release Notes.url'), releaseNotesUrl));
        }
        if ((_.filter(installer.modules, { id: 'documentation', selected: true })).length > 0) {
          promiseList.push(createShortcut(path.join(startMenuFolder, 'Unity Documentation.lnk'), path.join(installer.destinationPath, 'Editor', 'Data', 'Documentation', 'en', 'Manual', 'index.html')));
        }
        Promise.all(promiseList)
          .then(() => resolve());
      })
      .catch((e) => {
        logger.warn(`There was an error while creating the start menu shortcut. Ignored ${e}`);
        resolve();
      });
  });
}
/**
 * @param installer
 * @param exe
 */
function createDesktopShortcut(installer, exe) {
  logger.info(`create Desktop shortcut of ${installer.editor.version}`);
  return new Promise((resolve) => {
    createShortcut(path.join('%USERPROFILE%', 'Desktop', `Unity ${installer.editor.version}.lnk`), exe)
      .then(() => resolve())
      .catch((e) => `There was an error while creating the desktop shortcut. Ignored ${e}`);
  });
}

function createShortcut(link, target) {
  return new Promise((resolve, reject) => {
    ws.create(link, {
      target,
      workingDir: path.dirname(target),
      runStyle: ws.NORMAL
    }, (err) => {
      if (err) {
        logger.warn(`Error while creating shortcut ${link} with target ${target}: ${err}`);
        reject(err);
        return;
      }
      resolve();
    });
  });
}

function createInternetShortcut(link, target) {
  // ini.encode does not handle well the - in a string.
  // So we must enclose it in quotes and remove the quotes after
  return fs.outputFile(link, (ini.encode({ InternetShortcut: { URL: `'${target}'` } })).replace(new RegExp("'", 'g'), ''));
}

function removeExternalInstallersFolder(installer) {
  const externalInstallers = path.join(installer.destinationPath, 'ExternalInstallers');
  logger.info(`Trying to remove ${externalInstallers}`);
  return fs.remove(externalInstallers)
    .then(() => logger.info(`Successfully removed the external installers of ${installer.destinationPath}`))
    .catch((e) => {
      logger.warn(`There was an error while trying to remove the ExternalInstallers folder. Ignored ${e}`);
    });
}

/**
 * on windows7 and forward, we do not check the registry key, but use dism to turn on the windows feature always.
 * this is a pretty fast operation, and testing has shown that use registry key is not a very reliable way to detect
 * if the framework is present or not, especially if it has been turned on by dism / windows features window.
 */
function installDotNet35Framework() {
  return new Promise((resolve) => {
    const cmd = 'dism.exe /NoRestart /online /enable-feature /featurename:NetFx3 /all';
    proc.exec(cmd, { name: 'Enable .Net 3.5 Framework' },
      (err) => {
        if (err) {
          logger.warn(`There was an error while trying to enable the .NET 3.5 Framework. Ignoring ${err}`);
        }
        resolve();
      });
  });
}

function installDotNet45Framework(installer) {
  return new Promise((resolve) => {
    try {
      if (shouldInstallDotNetFramework('HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\NET Framework Setup\\NDP\\v4\\Full')) {
        logger.info('Need to install .Net 4.5 Framework');
        const setup = path.join(installer.destinationPath, 'ExternalInstallers', 'Microsoft', 'dotnet45', 'builds', 'dotNetFx45_Full_setup.exe');
        const cmd = `"${setup}" /passive /norestart /chainingpackage Unity`;
        proc.exec(cmd, { name: 'Install .Net 4.5 Framework' },
          (err) => {
            if (err) {
              logger.warn(`There was an error while trying to install the .NET 4.5 Framework. Ignoring ${err}`);
            }
            resolve();
          });
      } else {
        resolve();
      }
    } catch (e) {
      logger.warn(`There was an error while trying to verify or install .Net 4.5 framework. Ignoring ${e}`);
      resolve();
    }
  });
}
function shouldInstallDotNetFramework(runtimeKeyName) {
  let installFramework = false;

  try {
    if (registry.getUInt32(runtimeKeyName, 'Release', 0) < 378389) {
      installFramework = true;
    }
  } catch (e) {
    installFramework = true;
  }

  return installFramework;
}

function removeEditorFolder(editorRootPath) {
  return fs.remove(editorRootPath)
    .then(() => logger.info(`Successfully removed the editor files at ${editorRootPath}`))
    .catch((e) => {
      logger.warn(`There was an error while trying to remove the editor folder at ${editorRootPath} ${e}`);
      return Promise.reject(e);
    });
}
function removeRegistryKeys(version) {
  return new Promise((resolve) => {
    const unityKeyName = `HKEY_CURRENT_USER\\Software\\Unity Technologies\\Installer\\Unity ${version}`;
    logger.info(`remove registry key of ${version}: ${unityKeyName}`);

    proc.exec(`reg delete "${unityKeyName}" /f`, { name: 'Unity uninstaller (registry)' },
      (error) => {
        if (error) {
          logger.warn(`An error occured while deleting registry key ${unityKeyName}. Ignoring: ${error}`);
        }

        resolve();
      });
  });
}

function removeFirewallRules(version, editorRootPath) {
  const exe = path.join(editorRootPath, 'Editor', 'Unity.exe');
  const deleteRules = [
    `netsh advfirewall firewall delete rule name=all program="${exe}"`
  ];

  if (version.substr(0, 4) <= 2017) {
    const nodeExe = path.join(editorRootPath, 'Editor', 'Data', 'Tools', 'nodejs', 'node.exe');
    deleteRules.push(`netsh advfirewall firewall delete rule name=all program="${nodeExe}"`);
  }

  return spawnFirewallRules(deleteRules)
    .catch((e) => logger.warn(`There was an error while deleting the firewall rules. Ignored ${e}`));
}

function removeStartMenuShortcut(version) {
  return new Promise((resolve) => {
    let startMenuFolder = path.join(baseStartMenuFolder, `Unity ${version}`);
    logger.info(`remove Start Menu Shortcut of ${version} at ${startMenuFolder}`);

    fs.remove(startMenuFolder)
      .then(() => {
        startMenuFolder = path.join(baseStartMenuFolder, `Unity ${version} (64-bit)`);
        logger.info(`remove Start Menu Shortcut of ${version} at ${startMenuFolder}`);
        return fs.remove(startMenuFolder);
      })
      .then(() => resolve())
      .catch((e) => {
        logger.warn(`There was an error while removing the start menu shortcut. Ignored ${e}`);
        resolve();
      });
  });
}

/**
 * @param version
 */
function removeDesktopShortcut(version) {
  return new Promise((resolve) => {
    const desktopShortcutFile = path.join('%USERPROFILE%', 'Desktop', `Unity ${version}.lnk`);
    logger.info(`remove Desktop shortcut of ${version} at ${desktopShortcutFile}`);

    fs.remove(desktopShortcutFile)
      .then(() => resolve())
      .catch((e) => {
        logger.warn(`There was an error while removing the desktop shortcut. Ignored ${e}`);
        resolve();
      });
  });
}

function isBetaVersion(version) {
  return version.indexOf('b') > 0;
}
function isAlphaVersion(version) {
  return version.indexOf('a') > 0;

}
/*
if the version is smaller than or equal to 6.3: we need to notify the user if KB2999226 is not installed

if the version is smaller than or equal to 6.1: we need to notify the user if KB2533623 is not installed

 */
async function checkDotNetCorePrerequisites() {
  const osRelease = os.release().split('.');
  logger.debug('Check DotNet Core Prerequisites, os release', osRelease);
  let major = 0;
  let minor = 0;
  try {
    major = parseInt(osRelease[0], 10);
    minor = parseInt(osRelease[1], 10);

  } catch (err) {
    logger.warn('Could not retrieve the os release', err);
    return Promise.resolve();
  }
  logger.debug('major', major, 'minor', minor);
  let shouldInstallKB2999226 = false;
  let shouldInstallKB2533623 = false;
  if (major < 6 || (major === 6 && minor <= 3)) {
    const isKBInstalled = await checkIfWindowsUpdateIsInstalled('KB2999226');
    logger.debug('major', major, 'minor', minor);
    if (!isKBInstalled) {
      shouldInstallKB2999226 = true;
    }
  }
  if (major < 6 || (major === 6 && minor <= 1)) {
    const isKBInstalled = await checkIfWindowsUpdateIsInstalled('KB2533623');
    if (!isKBInstalled) {
      shouldInstallKB2533623 = true;
    }
  }

  logger.debug('shouldInstallKB2999226: ', shouldInstallKB2999226, 'shouldInstallKB2533623: ', shouldInstallKB2533623);

  if (shouldInstallKB2999226 && shouldInstallKB2533623) {
    return Promise.resolve({ warningCode: 'ERROR.INSTALL.WIN_UPDATE', winUpdate: 'KB2999226, KB2533623' });
  } else if (shouldInstallKB2999226) {
    return Promise.resolve({ warningCode: 'ERROR.INSTALL.WIN_UPDATE', winUpdate: 'KB2999226' });
  } else if (shouldInstallKB2533623) {
    return Promise.resolve({ warningCode: 'ERROR.INSTALL.WIN_UPDATE', winUpdate: 'KB2533623' });
  }
  return Promise.resolve();
}

/**
 * Given a KB (windows update number), it will check if the update is installed.
 * Empty result means not installed.
 * @param kb
 * @returns {Promise<any>}
 */
function checkIfWindowsUpdateIsInstalled(kb) {
  return new Promise((resolve) => {
    const cmd = `dism.exe /online /get-packages | findstr ${kb}`;
    proc.exec(cmd, { name: 'Check Windows Updates' },
      (err, stdout) => {
        if (err) {
          logger.warn(`There was an error while trying to check the windows updates. Ignoring ${err}`);
        }
        logger.debug('std out for ', kb, stdout);
        if (stdout && stdout.trim() !== '') {
          resolve(true);
        }
        resolve(false);
      });
  });
}

module.exports = unityInstallStepsWindows;
