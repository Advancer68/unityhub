const logger = require('../../../logger')('UnityInstallerWindows');
const errors = require('../errors.js');
const path = require('path');
const { fs } = require('../../../fileSystem');
const { UnityIPCServer } = require('../../../common/common');
const windowManager = require('../../../windowManager/windowManager');
const localConfig = require('../../localConfig/localConfig');
const { exec } = require('child_process');

/**
 * This class takes care of installing and uninstalling the editors and modules.
 * It queues the editor installations to avoid problems with concurrent installations.
 */
class UnityInstallerWindows {
  constructor() {
    this.timeouts = {
      INSTALL_EDITOR: 1800000,
      INSTALL_MODULE: 1800000,
      UNINSTALL_EDITOR: 1800000,
      WRITE_MODULE_FILE: 30000
    };
    this.INSTALL_SERVICE_PROCESS_FILEPATH = path.resolve(__dirname, 'unityInstallerProcess_win.js');
    this.installService = new UnityIPCServer('hubInstallServer', this.INSTALL_SERVICE_PROCESS_FILEPATH);
  }

  // --------------- PUBLIC INTERFACE --------------- //
  prepareInstallService() {
    windowManager.broadcastContent('installer.freezeUACActions', true);
    return this.getIPCServer()
      .catch((error) => {
        windowManager.broadcastContent('installer.freezeUACActions', false);
        if (error === errors.PERMISSION_DENIED) {
          return Promise.reject({ cancelled: true });
        }
        return Promise.reject(error);
      })
      .then(() => windowManager.broadcastContent('installer.freezeUACActions', false));
  }
  getIPCServer() {
    if (localConfig.isElevateDisabled()) {
      return this.installService.getIPCServer();
    }
    return this.installService.getElevatedIPCServer();
  }
  closeInstallService() {
    if (this.installService.started) {
      logger.info('closing install service');
      this.installService.closeSockets();
    }
  }
  uninstall(version, rootPath) {
    return this.uninstallEditorIPC(version, rootPath)
      .catch((uninstallError) => {
        logger.warn(`Failed to uninstall editoor ${uninstallError}`);
        return { error: errors.decodeError(uninstallError) };
      });
  }
  installEditorAndModules(currentInstaller) {
    return this.installEditorIPC(currentInstaller)
      .then((installEditorResponse) => {
        logger.debug('The install editor response', installEditorResponse);
        if (installEditorResponse) {
          installEditorResponse.forEach((response) => {
            if (response && response.warningCode) {
              logger.debug('Sending a warning code to the frontend', response.warningCode, response.winUpdate);
              windowManager.broadcastContent('installer.warning',
                { errorCode: response.warningCode, params: { i18n: { winUpdate: response.winUpdate } } });
            }
          });
        }
      })

      .then(() => this.installModulesIPC(currentInstaller, true))
      .catch((installError) => {
        logger.warn(`Failed to install editor and modules ${installError}`);
        return Promise.reject({ error: errors.decodeError(installError) });
      });
  }
  installModules(currentInstaller) {
    return this.installModulesIPC(currentInstaller, false)
      .catch((installError) => {
        logger.warn(`There was an error when installing a module: ${installError}`);
        return Promise.reject({ error: errors.decodeError(installError) });
      });
  }
  // --------------- IMPLEMENTATION --------------- //
  installModulesIPC(currentInstaller, overrideSelected) {
    let successfulInstalls = [];
    const failedInstalls = [];
    let modulesQueue = Promise.resolve();
    let globalModulesError;
    let restartRequired = false;

    if (currentInstaller.modules === undefined || !Array.isArray(currentInstaller.modules)) {
      return {
        restartRequired: false,
        modules: {
          successful: [],
          failed: []
        }
      };
    }
    currentInstaller.modules.forEach((module => {
      modulesQueue = modulesQueue
        .then(() => this.installModuleIPC(module)
          .then((response) => {
            if (response.restartRequired) {
              restartRequired = true;
            }
            successfulInstalls.push(module);
          })
          .catch((installError) => {
            logger.warn(`failed to install a module ${installError}`);
            module.error = errors.decodeError(installError);
            failedInstalls.push(module);
          }));
    }));
    return modulesQueue
      .then(() => this.writeModulesFileIPC(currentInstaller, successfulInstalls, overrideSelected)
        .catch(error => {
          error = errors.decodeError(error);
          // Write higher level error so that frontend can display more general failure message.
          globalModulesError = error;
          successfulInstalls.forEach(module => {
            module.error = error;
            failedInstalls.push(module);
          });
          successfulInstalls = [];
        }))
      .then(() => (
        {
          restartRequired,
          modules: {
            error: globalModulesError,
            successful: successfulInstalls,
            failed: failedInstalls
          }
        }
      ));
  }

  installModuleIPC(module) {
    logger.debug(`tell client to write module ${module.installerPath} to ${module.destination}`);
    return this.installService.processJob('installModule', module, this.timeouts.INSTALL_MODULE)
      .then((data) => {
        logger.info('client process finished installing module', data);
        return data;
      })
      .catch((error) => {
        logger.warn('Error while installing module!', error);
        return Promise.reject(error);
      });
  }
  writeModulesFileIPC(installer, modules, overrideSelected) {
    logger.debug(`tell client to write modules file of ${installer.editor.version}`);
    return this.installService.processJob('writeModulesFile', [installer, modules, overrideSelected], this.timeouts.WRITE_MODULE_FILE)
      .then((data) => {
        logger.info('client process finished writing modules file', data);
        return data;
      })
      .catch((error) => {
        logger.warn('error while writing file!', error);
        return error;
      });
  }
  installEditorIPC(currentInstaller) {
    logger.debug(`tell client to install the Editor ${currentInstaller.editor.version}`);
    return this.installService.processJob('installEditor', [currentInstaller], this.timeouts.INSTALL_EDITOR)
      .then((data) => {
        logger.info('client process finished installing the Editor', data);
        return data;
      })
      .catch((error) => {
      // do not try to delete the folder if it just couldn't be created...
        if (error && error.error && error.error.code === 'INVALID_DESTINATION') {
          return Promise.reject(error);
        }
        logger.warn('error while installing the Editor! Trying to empty the folder! ', error);
        return fs.emptyDir(currentInstaller.destinationPath)// hubFS.removeFolder(currentInstaller.destinationPath)
          .catch((err) => {
            if (err.code === 'ENOENT' || err.code === 'EPERM') {
              return Promise.reject(errors.MISSING_DESTINATION);
            }
            return Promise.reject(`Could not clear failed installation folder. ${err}`);
          })
          .then(() => {
            if (error.code === 'ENOENT') {
              return Promise.reject(errors.MISSING_DESTINATION);
            }
            return Promise.reject(error);
          });
      });
  }
  uninstallEditorIPC(version, editorRootPath) {
    return this.installService.processJob('uninstallEditor', { version, editorRootPath }, this.timeouts.UNINSTALL_EDITOR)
      .then((data) => {
        logger.info(`client process finished uninstalling the Editor ${version}`);
        return data;
      })
      .catch((error) => {
        logger.warn('error while uninstalling the Editor! ', error);
        return error;
      });
  }

  /**
   * Calls an external exe to verify if visual studio 2017 (15) is installed,
   * which license version (Community, Professional, Enterprise) and
   * if the Unity plugin (Workload.ManagedGame) is there as well
   * This exe will always return an error. The error code tells you what you want:
   * If code = 0: no Visual Studio is installed
   * If code = 1: Visual studio is installed with workload
   * If code = 2: Visual Studio Community is installed without workload
   * If code = 3: Visual Studio Professional is installed without workload
   * If code = 4: Visual Studio Enterprise is installed without workload
   * Stdout contains the path to Visual Studio's exe
   */
  isVisualStudioInstalled() {
    return new Promise((resolve) => {
      const relativePathFromLib = path.join('lib', 'win', 'VisualStudioInstallChecker.exe');
      // production env
      let vsInstallChecker = path.join(__dirname, '..', '..', '..', '..', '..', 'app.asar.unpacked', relativePathFromLib);

      // dev env
      if (!fs.existsSync(vsInstallChecker)) {
        logger.debug(`vsChecker not found in asar-unpacked ${vsInstallChecker}`);
        vsInstallChecker = path.join(__dirname, '..', '..', '..', '..', relativePathFromLib);
      }
      const vsVersion = 15;
      const vsWorkload = 'Microsoft.VisualStudio.Workload.ManagedGame';

      exec(`"${vsInstallChecker}" ${vsVersion} ${vsWorkload}`,
        (error, stdout) => {
          if (error && error.code) {
            switch (error.code) {
              case 1: {
                let version = 'Community';
                if (stdout.includes(path.join('Microsoft Visual Studio', '2017', 'Professional'))) {
                  version = 'Professional';
                } else if (stdout.includes(path.join('Microsoft Visual Studio', '2017', 'Enterprise'))) {
                  version = 'Enterprise';
                }

                resolve({
                  isInstalled: true,
                  workloadIsInstalled: true,
                  path: stdout,
                  version
                });
                return;
              }
              case 2:
                resolve({
                  isInstalled: true,
                  workloadIsInstalled: false,
                  path: stdout,
                  version: 'Community'
                });
                return;
              case 3:
                resolve({
                  isInstalled: true,
                  workloadIsInstalled: false,
                  path: stdout,
                  version: 'Professional'
                });
                return;
              case 4:
                resolve({
                  isInstalled: true,
                  workloadIsInstalled: false,
                  path: stdout,
                  version: 'Enterprise'
                });
                return;
              default:
                resolve({
                  isInstalled: false,
                  workloadIsInstalled: false,
                  path: '',
                  version: ''
                });
            }
          }

          resolve({
            isInstalled: false,
            workloadIsInstalled: false,
            path: '',
            version: ''
          });
        });
    });
  }
}
module.exports = new UnityInstallerWindows();
