const localStorageAsync = require('electron-json-storage');
const promisify = require('es6-promisify');
const { isBoolean, isString } = require('lodash');

const onboardingConfig = require('./defaultOnboarding');

const localStorage = {
  get: promisify(localStorageAsync.get, localStorageAsync),
  set: promisify(localStorageAsync.set, localStorageAsync),
  remove: promisify(localStorageAsync.remove, localStorageAsync),
};
const ONBOARDING_KEY = 'onboarding';

class OnboardingService {
  constructor() {
    this.setHasOnboarding();
  }

  setHasOnboarding(hasOnboading = onboardingConfig.hasOnboarding) {
    this.hasOnboarding = hasOnboading;
    // by default we assume it is a normal hub
    // unless we hasOnboarding is set to true in the build process
    if (this.hasOnboarding === true) {
      // by default we assume the onboarding process is not finished yet, unless the localStorage says otherwise
      this.defaultOnboarding = {
        isFinished: false,
        url: '/install'
      };
    } else {
      this.defaultOnboarding = {
        isFinished: true,
        url: undefined
      };
    }
  }

  setOnboardingURL(url) {
    return localStorage.set(ONBOARDING_KEY, { isFinished: false, onboardingURL: url });
  }

  setOnboardingFinished() {
    return localStorage.set(ONBOARDING_KEY, { isFinished: true });
  }

  async getOnboardingState() {
    let storageData = {};
    try {
      storageData = await localStorage.get(ONBOARDING_KEY);
    } catch (err) {
      return this.defaultOnboarding; // no value in local storage
    }
    const data = {};
    if (storageData) {
      data.isFinished = isBoolean(storageData.isFinished) ? storageData.isFinished : this.defaultOnboarding.isFinished;
      data.url = isString(storageData.url) ? storageData.url : this.defaultOnboarding.url;
      return data;
    }
    return this.defaultOnboarding;

  }

  // for development use only
  async clearFlags() {
    await localStorage.remove(ONBOARDING_KEY);
  }
}

module.exports = new OnboardingService();
