const { shell } = require('electron');
const platform = require('os').platform();

// internal
let window = require('./window');
const windowManager = require('../../windowManager/windowManager');

const chai = require('chai');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);

describe('window', () => {
  let sandbox;

  beforeEach(() => {
    sandbox = sinon.sandbox.create();

    sandbox.stub(windowManager, 'showSignInWindow');
    sandbox.stub(windowManager, 'showNewProjectWindow');
    sandbox.stub(shell, 'showItemInFolder');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('showSignInWindow', () => {
    it('should call windowManager.showSignInWindow', () => {
     window.showSignInWindow();
      expect(windowManager.showSignInWindow).to.have.been.called;
    });
  });

  describe('showNewProjectWindow', () => {
    it('should call windowManager.showNewProjectWindow', () => {
      window.showNewProjectWindow();
      expect(windowManager.showNewProjectWindow).to.have.been.called;
    });
  });

  describe('showItemInFolder', () => {
    it('should call shell.showItemInFolder', () => {
      window.showItemInFolder('test');
      expect(shell.showItemInFolder).to.have.been.calledWith('test');
    });
    
    it('should not alter backshasles', () => {
      window.showItemInFolder('some\\path\\somewhere');
      expect(shell.showItemInFolder).to.have.been.calledWith('some\\path\\somewhere');
    });

    if (platform === 'win32') {
      it('should convert forward slashes to backslashes on Windows', () => {
        window.showItemInFolder('some/path/somewhere');
        expect(shell.showItemInFolder).to.have.been.calledWith('some\\path\\somewhere');
      });

    } else {
      it('should not alter forward slashes', () => {
        window.showItemInFolder('some/path/somewhere');
        expect(shell.showItemInFolder).to.have.been.calledWith('some/path/somewhere');
      });
    }
  });
});
