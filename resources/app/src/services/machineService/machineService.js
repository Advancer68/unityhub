const os = require('os');
const Buffer = require('buffer').Buffer;
const logger = require('../../logger')('MachineService');
const editorLicense = require('@unityhub/unity-editor-license');

function hwID(preferUUID) {
  if (os.platform() === 'linux' || preferUUID) {
    return editorLicense.getUuid();
  }
  return editorLicense.getSerial();

}

function formatMAC(mac) {
  const arr = mac.toLowerCase().split('');
  for (let i = (arr.length / 2) - 1; i > 0; i--) {
    arr[i * 2] = `:${arr[i * 2]}`;
  }
  return arr.join('');
}

/**
 * Supports only mac and windows
 * @returns {*}
 */
function getMAC() {
  // module gives MAC containing `:` on *nix & `-` on win
  // set empty iface parameter, otherwise the library will use os.networkInterfaces method
  // to retrieve incomplele list
  return formatMAC(editorLicense.getMacAddress().replace(/[-:]/g, ''));
}

function hasMac(macAddr) {
  return Promise.resolve(editorLicense.macAddressExists(macAddr));
}

function getWinProductId() {
  const Registry = require('@unityhub/unity-editor-registry'); // eslint-disable-line

  return new Promise((resolve, reject) => {
    const productId = Registry.getString('HKEY_LOCAL_MACHINE\\SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\', 'ProductId', '');
    if (productId !== '') {
      resolve(productId);
    } else {
      reject('Key Not Found');
    }
  });
}

// win binding4
function getWinBIOS() {
  return new Promise((resolve) => {
    const biosId = editorLicense.getBiosId();
    resolve(biosId);
  });
}

function getHDDId() {
  return new Promise((resolve) => {
    const hddid = editorLicense.getHddId();
    if (hddid != null) {
      resolve(hddid);
    } else {
      // if the serial number is empty we need to return undefined
      resolve();
    }
  });
}

// mac: 1. uuid, 2. serial, 5. mac address
// win: 1. win product Id, 2. hdd Id, 4. bios id with base64 (serial => base64), 5. mac address
// linux: 1. uuid, 2. uuid

switch (os.platform()) {
  case 'darwin':
    module.exports = {
      getMachineBinding1: hwID.bind(this, true),    // uuid
      getMachineBinding2: hwID.bind(this, false),   // serial #
      getMachineBinding5: getMAC,                   // mac address
      getAllBindings() {
        return Promise.all([hwID(true), hwID(), getMAC()]).then((IDs) => ({ 1: IDs[0], 2: IDs[1], 5: IDs[2] }));
      }
    };
    break;
  case 'win32':
    module.exports = {
      getMachineBinding1: getWinProductId,          // uuid
      getMachineBinding2: getHDDId,                 // serial #
      getMachineBinding4: getWinBIOS,               // bios serial
      getMachineBinding5: getMAC,                   // mac address
      getAllBindings() {
        return Promise.all([getWinProductId(), getHDDId(), getWinBIOS(), getMAC()])
          .then((IDs) => ({ 1: IDs[0], 2: IDs[1], 4: Buffer.from(IDs[2]).toString('base64'), 5: IDs[3] }));
      },
      hasMac
    };
    break;
  case 'linux':
    module.exports = {
      getMachineBinding1: hwID.bind(this),    // uuid
      getMachineBinding2: hwID.bind(this),   // serial #
      getAllBindings() {
        return Promise.all([hwID(), hwID()])
          .then((IDs) => ({ 1: IDs[0], 2: IDs[1] }));
      }
    };
    break;
  default:
    logger.warn('unsupported platform');
}
