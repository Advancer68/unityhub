var Mocha = require('mocha');

var mocha = new Mocha();
var testDir = 'test/';
Mocha.utils.lookupFiles(testDir, ['js'], true).forEach(mocha.addFile.bind(mocha));

mocha.run(process.exit);
