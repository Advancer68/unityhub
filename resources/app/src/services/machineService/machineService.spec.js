let service = require('./machineService');
const expect = require('chai').expect;
const proc = require('child_process');
const os = require('os');
const sinon = require('sinon');

describe('Unity Hub Machine Service', function () {
  const uuidBegin = '[A-Z0-9]{8}-[A-Z0-9]{4}-';
  const uuidEnd = '[A-Z0-9]{4}-[A-Z0-9]{4}-[A-Z0-9]{12}';
  const uuidRx = new RegExp(`(${uuidBegin}${uuidEnd})`);
  const serialRx = /[A-Za-z0-9\/+]{12}/;
  const macRx = /[A-Fa-f0-9]{12}/;

  let sandbox;

  beforeEach(function () {
    sandbox = sinon.sandbox.create();
  });

  afterEach(function () {
    sandbox.restore();
  });

  describe.skip(`Bindings - platform: ${os.platform()}`, function () {
    it(`#1 (UUID) is a 36 char alphanumeric string with hyphens`, () => {
      return service.getMachineBinding1()
        .then((str) => expect(str).to.match(uuidRx));
    });
    it.skip(`#2 (serial #) is a 12 char alphanumeric string`, () => {
      return service.getMachineBinding2()
        .then((str) => expect(str).to.match(serialRx));
    });
    it(`#5 (MAC address) is a 12 char hexadecimal string`, () => {
      return service.getMachineBinding5()
        .then((str) => expect(str).to.match(macRx));
    });
  });

  describe(`.getAllBindings()`, function () {
    it.skip(`should return all machine bindings, in an object by #`, () => {
      return service.getAllBindings()
        .then((bindings) => {
          expect(bindings[1]).to.match(uuidRx);
          expect(bindings[2]).to.match(serialRx);
          expect(bindings[5]).to.match(macRx);
        });
    });
  });

  describe.skip(` windows binding test`, function () {
    it(`should return Index 0 DISKDRIVE`, function (done) {
      let execStub = sandbox.stub(proc, 'exec');
      delete require.cache[require.resolve('./machineService')]
      const serviceWin = require('./machineService');
      // need to require again because os.platform was not stub before first require
      execStub.callsArgWith(1, false, 'Index  SerialNumber\n1      000000000820\n0      S299NYAH613544', '')
      serviceWin.getMachineBinding2()
        .then((str) => expect(str).to.equal('S299NYAH613544'));

      execStub.callsArgWith(1, false, 'Index  SerialNumber\n0      000000000820\n1      S299NYAH613544', '')
      serviceWin.getMachineBinding2()
        .then((str) => expect(str).to.equal('000000000820'));

      execStub.callsArgWith(1, false, 'Index  SerialNumber\n2      000000000820\n1      S299NYAH613544', '')
      serviceWin.getMachineBinding2()
        .then((str) => {console.log(str)})
        .catch((err) => {
          expect(err).to.equal('Failed to get disc Id');
          done();
        });
    });

    beforeEach(() => {
      sandbox.stub(os, 'platform').returns('win32');
    });

    afterEach(() => {
      // reload old service module
      os.platform.restore();
      delete require.cache[require.resolve('./machineService')]
      service = require('./machineService');
    });
  })
});
