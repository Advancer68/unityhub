const request = require('supertest');
const chai = require('chai');

const expect = chai.expect;

function testRouteBase(server, url, verb, query, status, next) {
  return request(server)[verb](url)
    .query(query)
    .set('content-type', 'application/json')
    .expect('Content-Type', /json/)
    .expect(status)
    .end(next);
}

function testRouteSuccess(server, url, verb, status, body, method, done) {
  testRouteBase(server, url, verb, null, status, (err, res) => {
    expect(err).to.equal(null);
    if (body !== undefined) {
      expect(res.body).to.eql(body);
    }
    if (method !== undefined) {
      expect(method).to.have.been.calledOnce;  // eslint-disable-line
    }
    done();
  });
}

function testRouteQuerySuccess(server, url, verb, query, status, body, method, done) {
  testRouteBase(server, url, verb, query, status, (err, res) => {
    expect(err).to.equal(null);
    if (body !== undefined) {
      expect(res.body).to.eql(body);
    }
    if (method !== undefined) {
      expect(method).to.have.been.calledOnce;  // eslint-disable-line
    }
    done();
  });
}
module.exports = {
  testRouteSuccess,
  testRouteQuerySuccess
};
