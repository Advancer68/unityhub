'use strict';
const electron = require('electron');
const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;
const sinonChai = require('sinon-chai');
const request = require('supertest');
const HubRestService = require('../hubRestService');
const http = require('http');
const postal = require('postal');
const os = require('os');
const processId = require('process');
const windowManager = require('../../../windowManager/windowManager');
const app = electron.app;

chai.use(sinonChai);

describe('window REST controller', function () {
  let restService = new HubRestService();
  let server = restService.server;

  before(() => {
    restService.start();
  });

  after(() => {
    restService.stop();
  });

  describe('window REST endpoints', () => {
    let sandbox;

    beforeEach(() => {
      sandbox  = sinon.sandbox.create();
    });

    afterEach(() => {
      sandbox.restore();
    });

    // todo: flaky, fix it
    it.skip('should respond with 200 to POST window/show', (done) => {
      sandbox.stub(postal, 'publish');

      request(server)
        .post('/window/show')
        .send({
          modal: true,
          url: '/',
          sessionId: "123456"
        })
        .set('content-type', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, res) {
          expect(err).to.be.null;
          if (os.platform() === 'win32') {
            expect(res.body.handle).to.eql(app.mainWindow.browserWindow.getNativeWindowHandle().readUIntLE(0, app.mainWindow.browserWindow.getNativeWindowHandle().length));
          }
          expect(res.body.sessionId).to.eql( processId.pid );
          expect(postal.publish).to.have.been.calledOnce;
          done();
        });
    });

    it('should respond with 200 to POST window/hide', (done) => {
      windowManager.mainWindow = { hide: sandbox.stub() };
      request(server)
        .post('/window/hide')
        .set('content-type', 'application/json')
        .expect('Content-Type', /json/)
        .expect(200)
        .end(function(err, res) {
          expect(err).to.be.null;
          expect(res.body).to.eql('');
          expect(windowManager.mainWindow.hide).to.have.been.calledOnce;
          done();
        });
    });

  });
});
