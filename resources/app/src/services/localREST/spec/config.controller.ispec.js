'use strict';
const sinon = require('sinon');
const utils = require('./utils');
const CloudConfig = require('../../cloudConfig/cloudConfig');
const HubRestService = require('../hubRestService');

// todo: fix flaky test
describe.skip('CloudConfig REST Controller', () => {
  let restService = new HubRestService();

  before(() => {
    restService.start();
  });

  after(() => {
    restService.stop();
  });

  describe('routes', () => {
    let sandbox;

    beforeEach(() => {
      sandbox = sinon.sandbox.create();
    });

    afterEach(() => {
      sandbox.restore();
    });

    describe('/config/urls/default', () => {
      let fakeUrls;
      
      beforeEach(() => {
        fakeUrls = JSON.stringify({
          core: 'abc',
          identity: 'Han Solo',
        });
    
          sandbox.stub(CloudConfig, 'getDefaultUrls', (env) => {
          return fakeUrls;
        });
        
      });
      
      it('GET should call getDefaultUrls and return 200 OK', (done) => {
        utils.testRouteQuerySuccess(restService.server, '/config/urls/default', 'get', { env: 'staging' }, 200, fakeUrls, CloudConfig.getDefaultUrls, done);
      })
    });
      
    describe('/config/urls', () => {
      let fakeUrls;
      beforeEach(() => {
        fakeUrls = JSON.stringify({
          core: 'abc',
          indenity: 'Han Solo',
        });
      });

      it('GET should call urls property and return 200 OK', (done) => {
        var stub = sinon.stub(CloudConfig,  'urls', { get: function () { return fakeUrls }});
        utils.testRouteSuccess(restService.server, '/config/urls', 'get', 200, fakeUrls, CloudConfig.urls.get, function() {
          stub.restore();
          done();
        });
      })
    });

  });
});