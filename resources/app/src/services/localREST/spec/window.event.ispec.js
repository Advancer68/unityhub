'use strict';

const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');
const electron = require('electron');
const windowEvents = require('../api/events/window');

const app = electron.app;
const expect = chai.expect;
chai.use(sinonChai);


describe('Window Events', () => {

  let sendSpy = sinon.spy();
  let connectionsMock = {
    get: sinon.stub().returns({ send: sendSpy })
  };

  describe('on change', () => {

    it('should call connections.get with the session id & send the data', () => {
      let data = {  state: 'open' };
      let expectedPacket = {
        channel: 'app',
        topic: 'window.change',
        data: JSON.stringify(data)
      };

      app.editors = { current: { session_id: 'fake-id' } };
      windowEvents['main-window.change'](connectionsMock, data);
      expect(connectionsMock.get).to.have.been.calledWith('fake-id');
      expect(sendSpy).to.have.been.calledWith(JSON.stringify(expectedPacket));
      expect(app.editors.current).not.equal(null);
    });

    it('should close the connection if state is all-closed ', () => {
      let data = { state : 'all-closed' };
      let expectedPacket = {
        channel: 'app',
        topic: 'window.change',
        data: JSON.stringify(data)
      };

      app.editors = { current: { session_id: 'fake-id' } };
      windowEvents['main-window.change'](connectionsMock, data);
      expect(connectionsMock.get).to.have.been.calledWith('fake-id');
      expect(sendSpy).to.have.been.calledWith(JSON.stringify(expectedPacket));
      expect(app.editors.current).to.equal(null);
    });

  });

});

