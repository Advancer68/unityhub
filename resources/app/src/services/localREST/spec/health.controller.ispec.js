'use strict';
const sinon = require('sinon');
const utils = require('./utils');
const HubRestService = require('../hubRestService');

describe.skip('Health REST Controller', () => {
  let restService = new HubRestService();

  before(() => {
    restService.start();
  });

  after(() => {
    restService.stop();
  });

  describe('routes', () => {
    let sandbox;

    beforeEach(() => {
      sandbox = sinon.sandbox.create();
    });

    afterEach(() => {
      sandbox.restore();
    });

    describe('/health', () => {
      let expectedResponse;

      beforeEach(() => {
        expectedResponse = JSON.stringify({
          health: true,
        });
      });

      it('GET should return expected response and 200 OK', (done) => {
        utils.testRouteSuccess(restService.server, '/health', 'get', 200, JSON.parse(expectedResponse), undefined, done);
      })
    });
  });
});
