'use strict';
const sinon = require('sinon');
const utils = require('./utils');
const licenseClient = require('../../licenseService/licenseClientProxy');
const HubRestService = require('../hubRestService');

describe.skip('License REST Controller', function () {
  let restService = new HubRestService();
  let client = licenseClient;

  before(() => {
    restService.start();
  });

  after(() => {
    restService.stop();
  });

  describe('routes', () => {
    let sandbox;
    let testSuccess = utils.testRouteSuccess;
    let fakeLicenseInfo = {
      "activated": false,
      "survey": {
        "answered": false
      },
      "flow": "",
      "transactionId": "dfa1a570b72311e6bb0adb339ec8f9e8",
      "floating": "",
      "valid": true,
      "error": false,
      "lastErrorMsg": "",
      "updated": false,
      "returned": false,
      "maintenance": false,
      "initialized": true,
      "mustReactivate": false,
      "beta": false
    };

    beforeEach(() => {
      sandbox  = sinon.sandbox.create();
      ['loadLicense', 'saveLicense', 'refreshLicense', 'returnLicense', 'activateNewLicense', 'clearErrors']
        .forEach((method) => sandbox.stub(licenseClient, method, (callback) => {
          callback(null, true);
        }));
      sandbox.stub(licenseClient, 'getLicenseInfo', (cb) => cb(null, fakeLicenseInfo));
    });

    afterEach(() => {
      sandbox.restore();
    });

    describe('/license', () => {
      const test = testSuccess.bind(this, restService.server, '/license');

      it('POST should call saveLicense & return 201 Created', (done) => {
        test('post', 201, true, client.activateNewLicense, done);
      });

      it('PUT should call refreshLicense & return 200 OK', (done) => {
        test('put', 200, true, client.refreshLicense, done);
      });

      it('DELETE should call returnLicense & return 204 No Content', (done) => {
        test('delete', 204, '', client.returnLicense, done);
      });
    });

    describe('/license/info', () => {
      const testRoute = testSuccess.bind(this, restService.server, '/license/info');

      it('GET should call getLicenseInfo & return 200 OK', (done) => {
        testRoute('get', 200, fakeLicenseInfo, client.getLicenseInfo, done);
      });
    });

    describe('/license/file', () => {
      const testRoute = testSuccess.bind(this, restService.server, '/license/file');

      it('GET should call loadLicense & return 200 OK', (done) => {
        testRoute('get', 200, true, client.loadLicense, done);
      });

      it('POST should call saveLicense & return 201 Created', (done) => {
        testRoute('post', 201, true, client.saveLicense, done);
      });
    });

    describe('/license/errors', () => {
      const testRoute = testSuccess.bind(this, restService.server, '/license/errors');

      it('DELETE should call clearErrors & return 204 No Content', (done) => {
        testRoute('delete', 204, undefined, client.clearErrors, done);
      });
    });

  });
});
