'use strict';

const UnityAuth = require('../../../localAuth/auth');

function getUserInfo(req, res) {
  UnityAuth.getUserInfo().then((userInfo) => {
    res.setHeader('Access-Control-Allow-Origin', 'localhost');
    res.setHeader('Content-Type', 'application/json');
    res.end(userInfo);
  });
}

function getConnectInfo(req, res) {
  UnityAuth.getConnectInfo().then((connectInfo) => {
    res.setHeader('Access-Control-Allow-Origin', 'localhost');
    res.setHeader('Content-Type', 'application/json');
    res.end(connectInfo);
  });
}

function logout(req, res) {
  UnityAuth.logout();
  res.setHeader('Access-Control-Allow-Origin', 'localhost');
  res.statusCode = 200;
  res.end();
}

module.exports = {
  getUserInfo,
  getConnectInfo,
  logout
};
