'use strict';

const CloudConfig = require('../../../cloudConfig/cloudConfig');

function getDefaultUrls(req, res) {
  res.setHeader('Access-Control-Allow-Origin', 'localhost');
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(CloudConfig.getDefaultUrls()));
}

function getUrls(req, res) {
  res.setHeader('Access-Control-Allow-Origin', 'localhost');
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify(CloudConfig.urls));
}

module.exports = {
  getDefaultUrls,
  getUrls,
};
