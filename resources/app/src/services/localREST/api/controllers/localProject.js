'use strict';
const localProject = require('../../../localProject/localProject');

module.exports = {

  getRecent(req, res) {
    res.setHeader('Access-Control-Allow-Origin', 'localhost');
    localProject.getRecentProjects()
      .then((projects) => {
        res.end(JSON.stringify(projects));
      });
  },

  // use like: GET /projects/info?path=/Users/stella/unity-project
  async getProjectData(req, res) {
    const data = await localProject.getProjectData(req.query.path);
    res.setHeader('Access-Control-Allow-Origin', 'localhost');
    res.setHeader('Content-Type', 'application/json');
    res.json(data);
  },

  openProject(req, res) {
    res.setHeader('Access-Control-Allow-Origin', 'localhost');
    if (req.body.path) {
      localProject.openProject(req.body.path, req.body.unityVersion, {});
    } else {
      localProject.showOpenOtherDialog();
    }
    res.end();
  }

};
