'use strict';
const licenseClient = require('../../../licenseService/licenseClientProxy');

module.exports = {

  getInfo(req, res) {
    licenseClient.getLicenseInfo((err, data) => {
      res.setHeader('Access-Control-Allow-Origin', 'localhost');
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(data));
    });
  },

  activate(req, res) {
    licenseClient.activateNewLicense((err, succeeded) => {
      res.setHeader('Access-Control-Allow-Origin', 'localhost');
      res.setHeader('Content-Type', 'application/json');
      res.statusCode = 201;
      res.end(JSON.stringify(succeeded));
    });
  },

  return(req, res) {
    licenseClient.returnLicense(() => {
      res.setHeader('Access-Control-Allow-Origin', 'localhost');
      res.statusCode = 204;
      res.end();
    });
  },

  refresh(req, res) {
    licenseClient.refreshLicense((err, succeeded) => {
      res.setHeader('Access-Control-Allow-Origin', 'localhost');
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(succeeded));
    });
  },

  async load(req, res) {
    const { succeeded } = await licenseClient.loadLicenseLegacy();
    res.setHeader('Access-Control-Allow-Origin', 'localhost');
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(succeeded));
  },

  async save(req, res) {
    const { succeeded } = await licenseClient.saveLicense();
    res.setHeader('Access-Control-Allow-Origin', 'localhost');
    res.setHeader('Content-Type', 'application/json');
    res.statusCode = 201;
    res.end(JSON.stringify(succeeded));
  },

  clearErrors(req, res) {
    licenseClient.clearErrors(() => {
      res.setHeader('Access-Control-Allow-Origin', 'localhost');
      res.statusCode = 204;
      res.end();
    });
  }

};
