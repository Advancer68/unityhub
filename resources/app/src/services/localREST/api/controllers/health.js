'use strict';

function healthCheck(req, res) {
  res.setHeader('Access-Control-Allow-Origin', 'localhost');
  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify({ health: true }));
}

module.exports = {
  healthCheck,
};
