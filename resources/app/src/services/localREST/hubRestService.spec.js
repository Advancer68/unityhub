const chai = require("chai");
const expect = chai.expect;
const hubRestService = require("./hubRestService");

describe("hubRestService", () => {
  let hubRestServiceMock;

  beforeEach(() => {
    hubRestServiceMock = new hubRestService();
  });

  describe("isSameOriginPolicy", () => {
    let requestMock;

    it("should return false since referer is not local", () => {
      requestMock = {
        hostname: "http://localhost:39000/",
        url: "/auth/user-info",
        method: "GET",
        headers: {
          host: "localhost:39000",
          referer: "http://google.com/",
        },
        connection: {
          remoteAddress: "::ffff:127.0.0.1",
          localAddress: "::ffff:127.0.0.1",
        },
      };
      expect(hubRestServiceMock._isSameOriginPolicy(requestMock)).equals(false);
    });

    it("should return false since request connection remote address is different than local address", () => {
      requestMock = {
        hostname: "http://localhost:39000/",
        url: "/auth/user-info",
        method: "GET",
        headers: {
          host: "localhost:39000",
        },
        connection: {
          remoteAddress: "::ffff:127.0.0.3",
          localAddress: "::ffff:127.0.0.1",
        },
      };
      expect(hubRestServiceMock._isSameOriginPolicy(requestMock)).equals(false);
    });

    it("should return false since request host is not local", () => {
      requestMock = {
        hostname: "http://localhost:39000/",
        url: "/auth/user-info",
        method: "GET",
        headers: {
          host: "google.com",
        },
      };
      expect(hubRestServiceMock._isSameOriginPolicy(requestMock)).equals(false);
    });

    it("should return true", () => {
      requestMock = {
        hostname: "http://localhost:39000/",
        url: "/auth/user-info",
        method: "GET",
        headers: {
          host: "localhost:39000",
        },
        connection: {
          remoteAddress: "::ffff:127.0.0.1",
          localAddress: "::ffff:127.0.0.1",
        },
      };
      expect(hubRestServiceMock._isSameOriginPolicy(requestMock)).equals(true);
    });
  });
});
