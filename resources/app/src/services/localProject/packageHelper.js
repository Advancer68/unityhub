/* eslint-disable no-use-before-define */
const logger = require('../../logger')('PackageHelper');
const { fs } = require('../../fileSystem');
const _ = require('lodash');
const promisify = require('es6-promisify');
const PackageHasNoMetadataException = require('./packageHasNoMetadataException');

const GZIP_HEADER_SIZE = 10;
const GZIP_HAS_EXTRA_FIELDS_FLAG = 0x04;
const GZIP_HEADER_ID1 = 0x1F;
const GZIP_HEADER_ID2 = 0x8B;
const GZIP_EXTRA_FIELD_SIZE = 4;
const GZIP_MAXIMUM_HEADER_SIZE = 10000;

const PACKAGE_METADATA_CACHE_TIME = 3600000; // 1 hour in milliseconds

class PackageHelper {

  constructor() {
    this.packageMetadataCache = {};
  }

  getPackageMetadata(filename) {
    const cachedPackage = this.packageMetadataCache[filename];
    if (cachedPackage && cachedPackage.expiry > Date.now()) return Promise.resolve(_.clone(cachedPackage.info));

    return getPackageMetadata(filename).then((packageInfo) => {
      this.packageMetadataCache[filename] = {
        info: packageInfo,
        expiry: Date.now() + PACKAGE_METADATA_CACHE_TIME
      };

      return packageInfo;
    });
  }
}

function getPackageMetadata(filename) {
  const fsRead = promisify(fs.read);
  let fd;
  let buffer;
  let extraSize;
  let extraField;
  let json;
  let png;

  return promisify(fs.open)(filename, 'r')
    .then((_fd) => {
      fd = _fd;
      buffer = Buffer.alloc(GZIP_HEADER_SIZE);
      return fsRead(fd, buffer, 0, GZIP_HEADER_SIZE, 0);
    })
    .then(() => {
      const headerDetails = new Uint8Array(buffer);

      // if gzip header ids do not match the constants the header is invalid.
      if (headerDetails[0] !== GZIP_HEADER_ID1 || headerDetails[1] !== GZIP_HEADER_ID2) {
        throw new Error(`Package ${filename} is not a valid gzip file.`);
      }

      // if gzip header has no extra fields there is no metadata for this package.
      if (!(headerDetails[3] & GZIP_HAS_EXTRA_FIELDS_FLAG)) {
        throw new PackageHasNoMetadataException(filename);
      }

      // read extra fields length.
      buffer = Buffer.alloc(2);
      return fsRead(fd, buffer, 0, 2, GZIP_HEADER_SIZE);

    }).then(() => {
      extraSize = buffer.readUInt16LE();

      if (extraSize > GZIP_MAXIMUM_HEADER_SIZE) {
        throw new Error(`Package ${filename} header has unusually large extra size`);
      }

      buffer = Buffer.alloc(extraSize);
      return fsRead(fd, buffer, 0, extraSize, GZIP_HEADER_SIZE + 2);

    })
    .then(() => {
      json = png = undefined;

      // loop until no more extra field bytes.
      for (let i = 0; i < extraSize; i += GZIP_EXTRA_FIELD_SIZE) {
        extraField = {};
        // read field IDs
        extraField.id1 = String.fromCharCode(buffer.readUInt8(i));
        extraField.id2 = String.fromCharCode(buffer.readUInt8(i + 1));
        // read field length
        extraField.length = buffer.readUInt16LE(i + 2);

        // if extra field is asset store field parse it
        if (extraField.id1 === 'A' && extraField.id2 === '$') {
          json = JSON.parse(buffer.slice(i + 4, i + 4 + extraField.length).toString());

        } else if (extraField.id1 === 'A' && extraField.id2 === '%') {
          png = buffer.slice(i + 4, i + 4 + extraField.length).toString();
        }

        // skip extra header's length
        i += extraField.length;
      }

      if (json === undefined && png === undefined) {
        throw new Error(`Package ${filename} header has no Asset Store metadata`);
      }

      fs.close(fd);

      return { filename, json, png };
    })
    .catch((error) => {
      logger.warn(error);
      fs.close(fd);

      if (error instanceof PackageHasNoMetadataException) {
        return { filename };
      }

      return {};
    });
}

module.exports = new PackageHelper();
