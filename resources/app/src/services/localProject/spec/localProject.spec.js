// node
const os = require('os');
const path = require('path');
const childProcess = require('child_process');
const _ = require('lodash');

// local
const editorPref = require('../../editorPref/editorPref');
const editorManager = require('../../editorManager/editorManager');
const EditorApp = require('../../editorApp/editorapp');
const localProject = require('./../localProject');
const settings = require('../../localSettings/localSettings');
const fixture = require('./localProject.fixture');
const packageFinder = require('../packageFinder');
const packageHelper = require('../packageHelper');
const { fs, hubFS } = require('../../../fileSystem');
const windowManager = require('../../../windowManager/windowManager');
const templateService = require('../../unityTemplate/unityTemplateService');
const hubIPCState = require('../../localIPC/hubIPCState');
const licenseClient = require('../../licenseService/licenseClientProxy');
const projectCustomEditorHelper = require('../projectCustomEditorHelper');
const outputService = require('../../outputService');
const cloudAnalytics = require('../../cloudAnalytics/cloudAnalytics');

// third-parties
const postal = require('postal');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);
chai.use(chaiAsPromised);

describe('LocalProject', () => {
  let sandbox, settingsMock, cloudAnalyticsStub;
  const invalidPath = 'invalidPath';
  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    cloudAnalyticsStub = sandbox.stub(cloudAnalytics, 'addEvent');

    windowManager.broadcastContent = sandbox.stub();

    sandbox.stub(postal, 'publish');
    sandbox.stub(editorManager, 'getDefaultEditorApp').returns(fixture.editorManager.getDefaultEditorApp());
    sandbox.stub(editorManager, 'getEditorApp').returns(fixture.editorManager.getEditorApp());
    sandbox.stub(hubFS, 'isFilePresentPromise').callsFake((projPath) => {
      if (!projPath) {
        return Promise.resolve();
      }
      if (projPath === 'validPath'
        || projPath === getProjectSettingsPath('validPath')
        || projPath === path.join('projectWithLock', 'Temp', 'UnityLockfile')){
        return Promise.resolve()
      }
      if (projPath === getProjectSettingsPath('nonUnityProject')
          || projPath === invalidPath){
        return Promise.reject();
      }
      return Promise.resolve();

    });

    settingsMock = {
      [settings.keys.RECENT_PROJECT_INTERVAL]: 100
    };
    sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);
    sandbox.stub(outputService, 'notifyContent');
  });

  function getProjectSettingsPath(projPath) {
    return path.join(projPath, 'ProjectSettings', 'ProjectSettings.asset');
  }

  afterEach(() => {
    sandbox.restore();
  });

  describe('init', () => {
    it('should get the recent project and force refresh', async () => {
      sandbox.spy(localProject, 'getRecentProjects');
      await localProject.init();
      expect(localProject.getRecentProjects).to.have.been.calledWith(true)
    });
  });
  describe.skip('getProjectData', () => {
    beforeEach(() => {
      sandbox.stub(fs, 'stat').callsFake((path, cb) => cb(null, { mtime: new Date(0) }));
    });

    it('should load data from a project folder', async () => {
      const result = await localProject.getProjectData(fixture.projects.full.path);
      expect(result).to.eql(fixture.projects.full.expected);
    });

    it('should load data from a project folder with missing version file', async () => {
      const result = await localProject.getProjectData(fixture.projects.missingVersion.path);
      expect(result).to.eql(fixture.projects.missingVersion.expected);
    });

    it('should load data from a project folder with missing settings file', async () => {
      const result = await localProject.getProjectData(fixture.projects.missingSettings.path);
      expect(result).to.eql(fixture.projects.missingSettings.expected);
    });

    it('should get data from a project folder with missing settings file and missing version file', async () => {
      const result = await localProject.getProjectData(fixture.projects.missingAll.path);
      expect(result).to.eql(fixture.projects.missingAll.expected);
    });
  });

  describe('getRecentProjects', () => {
    let recentProjectKeys, projectData;
    beforeEach(() => {
      recentProjectKeys = [
        'Roll a Ball',
        'Survival Game',
        'Tanks',
        'deletedProject'
      ];

      // recent projects mock
      const recentProjects = {};
      recentProjectKeys.forEach((key) => {
        if (key === 'deletedProject') {
          recentProjects[key] = invalidPath;
        } else {
          recentProjects[key] = path.join('my', 'fun', 'path', key);
        }
      });
      localProject.recentProjects = recentProjects;

      // getProjectData mock
      projectData = {};
      recentProjectKeys.forEach((key) => {
        projectData[recentProjects[key]] = {
          title: key,
          path: recentProjects[key],
          foo: 'bar',
          version: key === 'deletedProject' ? undefined : '2018.1',
          lastModified: key === 'deletedProject' ? undefined : new Date(0)
        };
      });
      sandbox.stub(localProject, 'getProjectData').callsFake((projectPath) => projectData[projectPath]);
      sandbox.stub(editorPref, 'getRecentProjects').resolves(Object.values(recentProjects));
      sandbox.stub(fs, 'existsSync').callsFake(() => {
        return true;
      });
    });

    it('should return recent project information', () => {
      let projectDataCopy = Object.assign(projectData);
      delete projectDataCopy[invalidPath];
      return expect(localProject.getRecentProjects(true)).to.eventually.deep.equal(_.values(projectDataCopy));
    });

    it('should return exclude invalid paths', () => {
      return localProject.getRecentProjects(true).then((recentProjects) => {
        expect(recentProjects.length).to.equal(3);
        expect(recentProjects['deletedProject']).to.be.undefined;
      });
    });

    it('should refresh the list if the forceRefresh flag is true', () => {
      return localProject.getRecentProjects(true).then(() => {
        expect(editorPref.getRecentProjects).to.have.been.called.once;
      });
    });

    it('should refresh the list if the the window is focused', () => {
      windowManager.mainWindow.isFocused = () => true;
      return localProject.getRecentProjects().then(() => {
        expect(editorPref.getRecentProjects).to.have.been.called.once;
      });
    });

    it('should not refresh the list if the the window is not focused', () => {
      windowManager.mainWindow.isFocused = () => false;
      return localProject.getRecentProjects().then(() => {
        expect(editorPref.getRecentProjects).not.to.have.been.called;
      });
    });
  });

  describe('showOpenOtherDialog', function () {

    it('should be fulfilled when a folder has been selected', () => {
      sandbox.stub(localProject, 'chooseProjectDialog').resolves('a folder');
      sandbox.stub(localProject, 'openProject').resolves();
      sandbox.stub(localProject, 'getProjectVersion').resolves();
      sandbox.stub(editorManager, 'getDefaultEditorVersion').resolves('foo');
      return expect(localProject.showOpenOtherDialog()).to.be.fulfilled;
    });

    it('should be rejected when no folder has been selected', () => {
      sandbox.stub(localProject, 'chooseProjectDialog').rejects('');
      return expect(localProject.showOpenOtherDialog()).to.be.rejected;
    });
  });

  describe('getProjectVersion', function () {
    const callBacks = {};
    const versionPath = '/ProjectSettings/ProjectVersion.txt';
    beforeEach( () => {
      sandbox.stub(fs, 'readFile').callsFake((path, encoding, cb) => {
        callBacks[path] = cb;
      });

    });

    it('should be fulfilled with version when it exists', (done) => {
      const givenPath = 'valid path and valid version';
      const expectedVersion = 'valid version';
      const promise = localProject.getProjectVersion(givenPath);
      callBacks[path.join(givenPath, versionPath)](undefined, 'm_EditorVersion: ' + expectedVersion);
      promise.then(version => {
        expect(version).to.equal(expectedVersion);
        done();
      })
    });

    it('should be fulfilled with undefined version if error happens', (done) => {
      let expectedVersion;
      const promise = localProject.getProjectVersion(invalidPath);
      callBacks[path.join(invalidPath, versionPath)]({}); //error
      promise.then(version => {
        expect(version).to.equal(expectedVersion);
        done();
      });
    });

    it('should be fulfilled with undefined version if version is missing', (done) => {
      const givenPath = 'valid path and missing version';
      let expectedVersion;
      const promise = localProject.getProjectVersion(givenPath);
      callBacks[path.join(givenPath, versionPath)](undefined, 'no value for m_EditorVersion');
      promise.then(version => {
        expect(version).to.equal(expectedVersion);
        done();
      });
    });
  });

  describe('chooseDirectory', function () {

    beforeEach(() => {
      sandbox.stub(localProject, 'getUserDefault').resolves({});
    });

    it('should be fulfilled when a folder has been selected', () => {
      const projectPath = path.join(os.homedir(), 'coolproject');
      windowManager.newProjectWindow.showOpenFileDialog = sandbox.stub().resolves(projectPath);
      return expect(localProject.chooseDirectory()).to.eventually.equal(projectPath);
    });

    it('should be rejected with message "No directory selected" when no folder has been selected', () => {
      windowManager.newProjectWindow.showOpenFileDialog = sandbox.stub().rejects();
      return expect(localProject.chooseDirectory()).to.eventually.be.rejected;
    });
  });

  describe('openProject', () => {

    beforeEach(() => {
      windowManager.mainWindow.hide = sandbox.stub().resolves();
      localProject.recentProjects = [
        {path: 'validPath'},
        {path: 'projectWithLock'}
        ];
    });

    it('should be rejected when spawning Unity editor failed', () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      sandbox.stub(EditorApp.prototype, 'openProject').throws();
      sandbox.stub(localProject, 'validateOpenProject').resolves();

      return expect(localProject.openProject('validPath')).to.be.rejected;
    });

    it('should be fulfilled when spawning Unity editor succeeded', () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      sandbox.stub(hubIPCState, 'modalEditor').get(() => {});
      sandbox.stub(EditorApp.prototype, 'openProject').resolves();
      sandbox.stub(hubFS, 'isFileUnlocked').resolves();

      return expect(localProject.openProject('validPath')).to.be.fulfilled;
    });

    it('should open project correctly when there is a modal editor open', () => {
      sandbox.stub(hubIPCState, 'modalEditor').get(() => {return {current: {foo: 'bar'}};});
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      sandbox.stub(hubFS, 'isFileUnlocked').resolves();
      return expect(localProject.openProject('validPath')).to.be.fulfilled;
    });

    it('should pass arguments to editorApp', () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      sandbox.stub(EditorApp.prototype, 'start').resolves();
      sandbox.stub(hubFS, 'isFileUnlocked').resolves();
      sandbox.spy(EditorApp.prototype, 'openProject');

      return localProject.openProject('validPath', undefined, {args: ['-testArg']}).then(() => {
        expect(EditorApp.prototype.openProject).to.have.been.calledWith(sinon.match.any, {args: ['-testArg']});
      });
    });

    it('should pass build platform to editorApp and create proper arguments', () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      sandbox.stub(EditorApp.prototype, 'start').resolves();
      sandbox.stub(hubFS, 'isFileUnlocked').resolves();
      sandbox.spy(EditorApp.prototype, 'openProject');

      const options = {
        buildPlatform: {
          buildTarget: 'testTarget',
          name: 'Current Build Target',
          buildTargetGroup: 'testTargetGroup'
        }
      };

      return localProject.openProject('validPath', undefined, options).then(() => {
        expect(EditorApp.prototype.start.firstCall.args[0]).to.contain.members(['-buildTarget', 'testTarget', '-buildTargetGroup', 'testTargetGroup']);
      });
    });

    it('should open custom editor instead of a standard editor', () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      sandbox.stub(projectCustomEditorHelper, 'hasCustomEditor').returns(true);
      sandbox.stub(hubFS, 'isFileUnlocked').resolves();
      sandbox.stub(EditorApp.prototype, 'start').resolves();
      sandbox.stub(EditorApp.prototype, 'openProject').resolves();
      sandbox.stub(projectCustomEditorHelper, 'getCustomEditorForProject').resolves(new EditorApp({ editorPath: 'validPath' }));

      return localProject.openProject('validPath', null, {}).then(() => {
        expect(projectCustomEditorHelper.getCustomEditorForProject).to.have.been.calledWith('validPath');
      });
    });

    it('should be rejected with appropriate error message if selected folder was not found', () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      sandbox.stub(EditorApp.prototype, 'openProject').resolves();
      return expect(localProject.openProject(invalidPath))
        .to.be.eventually.rejected.and.have.property('errorCode', 'ERROR.PROJECT.PATH_NOT_FOUND');
    });

    it('should notify that the project lockfile is locked', async () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      sandbox.stub(EditorApp.prototype, 'openProject').resolves();
      sandbox.stub(hubFS, 'isFileUnlocked').rejects();

      await expect(localProject.openProject('projectWithLock')).to.be.fulfilled;
      return expect(outputService.notifyContent).to.have.been.calledWith('project.lock_file_error', 'projectWithLock');
    });

    it('should open the project if lockFile is not locked', () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      sandbox.stub(EditorApp.prototype, 'openProject').resolves();
      sandbox.stub(hubFS, 'isFileUnlocked').resolves();

      return expect(localProject.openProject('projectWithLock')).to.be.fulfilled;
    });

    it('should publish a project.opened event with the project path as data', () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      sandbox.stub(EditorApp.prototype, 'openProject').resolves();
      sandbox.stub(hubFS, 'isFileUnlocked').resolves();

      return localProject.openProject('validPath').then(() => {
        expect(postal.publish).to.have.been.calledWith({
          channel: 'app',
          topic: 'project.opened',
          data: {projectPath: 'validPath'}
        });
      });
    });

    it('should reject if the license is invalid', () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(false);
      sandbox.stub(EditorApp.prototype, 'openProject').resolves();
      sandbox.stub(localProject, 'validateOpenProject').resolves();

      return expect(localProject.openProject('validPath'))
        .to.be.eventually.rejected.and.have.property('errorCode', 'ERROR.LAUNCH_EDITOR.LICENSE');
    });
  });

  describe('addProject', () => {
    let projectData;

    beforeEach(() => {
      projectData = {
        projectDirectory: 'my/path/to/foo'
      };

      sandbox.stub(localProject, 'chooseProjectDialog').resolves(projectData);
      sandbox.stub(editorPref, 'setMostRecentProject').resolves();
    });

    it('should set most recent project as the one chosen by the user', async () => {
      sandbox.stub(localProject, 'validateOpenProject').resolves();
      await localProject.addProject();
      expect(editorPref.setMostRecentProject).to.have.been.calledWith(projectData.projectDirectory);
    });
  });

  describe('removeProject', () => {
    let projectPath;

    beforeEach(() => {
      projectPath = 'my/path/to/foo';

      sandbox.stub(editorPref, 'deleteProject').resolves();
    });

    it('should delete the selected project', async () => {
      await localProject.removeProject(projectPath);
      expect(editorPref.deleteProject).to.have.been.calledWith(projectPath);
    });
  });

  describe('createProject', () => {

    beforeEach(() => {
      sandbox.stub(templateService, 'defaultTemplateCode').get(() => 0);
      windowManager.mainWindow.hide = sandbox.stub().resolves();
      sandbox.stub(hubIPCState, 'modalEditor').get(() => {});
    });

    it('should publish a project.create event with name, directory, type, packages, & scene info', async () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      sandbox.stub(hubFS, 'isValidPath').returns(true);
      sandbox.stub(EditorApp.prototype, 'createProject').resolves();
      sandbox.stub(hubIPCState, 'modalEditor').get(() => {return {current: {}};});

      const projectName = 'Test Project';
      const projectDirectory = path.join('projects', 'test');
      const projectTemplate = {id: '3D', code: 0};
      const selectedPackages = ['Terrain', 'Vehicles', 'Prototyping'];
      const defaultScene = 'Main';
      const organizationId = '';

      localProject.setOrganizationId(organizationId);

      await localProject.createProject(projectName, projectDirectory, projectTemplate, selectedPackages, defaultScene);

      expect(postal.publish).to.have.been.calledWith({
        channel: 'app',
        topic: 'project.create',
        data: {projectName, projectDirectory, projectType: 0, selectedPackages, defaultScene, organizationId}
      });
    });

    it('should be rejected with appropriate error message if we can\'t find Unity', () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      editorManager.getDefaultEditorApp.restore();
      sandbox.stub(childProcess, 'spawn').callsFake(fixture.spawn);
      sandbox.stub(editorManager, 'getDefaultEditorApp').resolves(fixture.editorManager.invalidEditor);

      return expect(localProject.createProject('testXYZ', os.homedir(), '2D', [], 'Main'))
      .to.be.rejectedWith();
    });
    it('should reject if the license is invalid', () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(false);
      sandbox.stub(hubFS, 'isValidPath').returns(true);
      sandbox.stub(EditorApp.prototype, 'createProject').resolves();
      sandbox.stub(hubIPCState, 'modalEditor').get(() => {return {current: {}};});

      const projectName = 'Test Project';
      const projectDirectory = path.join('projects', 'test');
      const projectTemplate = {id: '3D', code: 0};
      const selectedPackages = ['Terrain', 'Vehicles', 'Prototyping'];
      const defaultScene = 'Main';
      const organizationId = '';

      localProject.setOrganizationId(organizationId);

      return expect(localProject.createProject(projectName, projectDirectory, projectTemplate, selectedPackages, defaultScene)).to.be.rejected;
    });

  });

  describe('createTempProject', () => {
    let packages;

    beforeEach(() => {
      sandbox.stub(hubIPCState, 'modalEditor').get(() => {return {current: {foo: 'bar'}};});
      packages = ['/package 1'];
      sandbox.stub(templateService, 'defaultTemplateCode').get(() => 0);
      sandbox.stub(EditorApp.prototype, 'createTempProject').resolves();
      windowManager.mainWindow.hide = sandbox.stub().resolves();
    });

    it('should publish the correct postal event', () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      return localProject.createTempProject(packages).then(() => {
        expect(postal.publish).to.have.been.calledWith({
          channel: 'app',
          topic: 'project.createTemp',
          data: {packages}
        });
      });
    });

    it('should fulfill the returned promise', () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      return expect(localProject.createTempProject(packages)).to.be.fulfilled;
    });

    it('should reject if the license is invalid', () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(false);
      return expect(localProject.createTempProject(packages)).to.be.rejected;
    });

    it('should hide the window', async () => {
      sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      sandbox.stub(hubIPCState, 'modalEditor').get(() => undefined);

      await localProject.createTempProject(packages);

      expect(windowManager.mainWindow.hide).to.have.been.calledOnce;
    });

    describe('when no editor is currently opened', () => {
      beforeEach(() => {
        sandbox.stub(hubIPCState, 'modalEditor').get(() => undefined);
      });

      it('should call createTempProject from the current editorApp', () => {
        sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
        return localProject.createTempProject(packages).then(() => {
          expect(EditorApp.prototype.createTempProject).to.have.been.calledWith(packages);
        });
      });
    });

    describe('cloudAnalytics', () => {
      beforeEach(() => {
        sandbox.stub(hubIPCState, 'modalEditor').get(() => undefined);
        sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
      });

      it('should emit the cloud analytics success event', async () => {
        await localProject.createTempProject(packages);

        expect(cloudAnalyticsStub).to.have.been.calledWithExactly({
            msg: {
              editor_version: undefined,
              enable_analytics: false,
              localprojectid: undefined,
              organizationid: '',
              packages: ['package 1'],
              project_type: 'temp',
              status: 'Success',
              template_id: '0'
            },
            type: 'hub.projectNew.v2'
          });
      });

      it('should emit the cloud analytics error event', async () => {
        windowManager.mainWindow.hide = () => { throw new Error() };

        try {
          await localProject.createTempProject(packages);
        } catch (e) {
          // Do nothing with error
        }

        expect(cloudAnalyticsStub).to.have.been.calledWithExactly({
          msg: {
            editor_version: undefined,
            enable_analytics: false,
            localprojectid: undefined,
            organizationid: '',
            packages: ['package 1'],
            project_type: 'temp',
            status: 'Error',
            template_id: '0'
          },
          type: 'hub.projectNew.v2'
        });
      });
    });
  });

  describe('getPackageList', function () {
    let originalPackageFinderPaths, customPaths, filePaths, baseFileData, result;

    before(() => {
      originalPackageFinderPaths = Object.assign({}, packageFinder.defaultPaths);
    });

    after(() => {
      packageFinder.defaultPaths = Object.assign({}, originalPackageFinderPaths);
    });

    beforeEach(() => {
      packageFinder.defaultPaths = ['foo/bar', 'ho/bo'];
      customPaths = ['hoho/ho', 'yodelay/heee'];
      filePaths = ['hoho/ho/hehe.unitypackage', 'yodelay/heee/tooo/yodle.unitypackage', 'ho/bo/do/di.unitypackage'];
      baseFileData = {json: {foo: 'bar'}};
      sandbox.stub(packageFinder, 'findMany').resolves(filePaths);
      sandbox.stub(packageHelper, 'getPackageMetadata').callsFake((filename) => (
        Promise.resolve(Object.assign({filename}, baseFileData))
      ));

      result = localProject.getPackageList(customPaths);
    });

    it('should search for packages in default paths', () => {
      expect(packageFinder.findMany.firstCall.args[0]).to.contain.members(packageFinder.defaultPaths);
    });

    it('should search for packages in custom paths', () => {
      expect(packageFinder.findMany.firstCall.args[0]).to.contain.members(customPaths);
    });

    it('should cache retrieved package list', () => {
      return result.then(packageList => {
        expect(localProject.cachedPackageList).to.deep.eq(packageList);
      });
    });

    it('should return correctly formatted package list', () => {
      return expect(result).to.eventually.deep.eq(filePaths.map(path => {
        return {path: path, packageInfo: baseFileData.json}
      }));
    });
  });

  describe('getUniqueProjectPathName', () => {
    it('should call getUniquePath from common hubFS', () => {
      let stub = sandbox.stub(hubFS, 'getUniquePath').returns('');
      localProject.getUniqueProjectName('a/fun/path', 'awesomeName');
      expect(stub).to.have.been.called;
    })
  });

  describe('getProjectsFromMemory', () => {
    it('should return the projects that are in the memory', async () => {
      localProject.recentProjects = [
        {path: 'validPath'},
        {path: 'projectWithLock'}
        ];

      expect(await localProject.getProjectsFromMemory()).to.eql([{path: 'validPath'},{path: 'projectWithLock'}]);
    })
  });
});
