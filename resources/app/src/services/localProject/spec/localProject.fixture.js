const os = require('os');
const path = require('path');
const EditorApp = require('../../editorApp/editorapp');

const resourcePath = 'src/services/localProject/spec/resources';

var fixture = {
  projects: {
    full: {
      path: path.join(resourcePath, 'testProject'),
      expected: {
        path: path.join(resourcePath, 'testProject'),
        containingFolderPath: path.normalize('src/services/localProject/spec/resources'),
        version: '5.5.0b2',
        title: 'testProject',
        cloudProjectId: '524f5698-3ace-4523-ac71-3023be7e5bf1',
        localProjectId: '85ce4b6f4b0f94c018ad595b4b1f3e8a',
        resultorganizationId: 'mathieub',
        cloudEnabled: true,
        lastModified: new Date(0),
        changeset: 'c55537152c3f',
        isCustomEditor: false
      }
    },
    missingAll: {
      path: path.join(resourcePath, 'testProject_missing_all'),
      expected: {
        path: path.join(resourcePath, 'testProject_missing_all'),
        containingFolderPath: path.normalize('src/services/localProject/spec/resources'),
        title: 'testProject_missing_all',
        cloudEnabled: false,
        lastModified: new Date(0),
        isCustomEditor: false
      }
    },
    missingSettings: {
      path: path.join(resourcePath, 'testProject_missing_settings'),
      expected: {
        path: path.join(resourcePath, 'testProject_missing_settings'),
        containingFolderPath: path.normalize('src/services/localProject/spec/resources'),
        version: '5.5.0b2',
        title: 'testProject_missing_settings',
        cloudEnabled: false,
        lastModified: new Date(0),
        isCustomEditor: false
      }
    },
    missingVersion: {
      path: path.join(resourcePath, 'testProject_missing_version'),
      expected: {
        path: path.join(resourcePath, 'testProject_missing_version'),
        containingFolderPath: path.normalize('src/services/localProject/spec/resources'),
        title: 'testProject_missing_version',
        cloudProjectId: '524f5698-3ace-4523-ac71-3023be7e5bf1',
        localProjectId: '85ce4b6f4b0f94c018ad595b4b1f3e8a',
        resultorganizationId: 'mathieub',
        cloudEnabled: true,
        lastModified: new Date(0),
        isCustomEditor: false
      }
    }
  }
};

fixture.recentlyUsed = {
  'RecentlyUsedProjectPaths-0': fixture.projects.full.path,
  'RecentlyUsedProjectPaths-1': fixture.projects.missingVersion.path,
  'RecentlyUsedProjectPaths-2': fixture.projects.missingSettings.path,
  'RecentlyUsedProjectPaths-3': fixture.projects.missingAll.path,
};

fixture.expectedRecentProject = [
  fixture.projects.full.expected,
  fixture.projects.missingAll.expected,
  fixture.projects.missingSettings.expected,
  fixture.projects.missingVersion.expected
];

fixture.spawn = function () {
  return {
    on: (eventName, cb) => {
      if (eventName === 'data') cb('fake stdout content');
    },
    stderr: {
      on: (eventName, cb) => {
        if (eventName === 'error') cb('fake error data');
        if (eventName === 'close') cb(0);
      }
    }
  };
};

const editorPrefValue = {
  kNewProjectsPath: path.join(os.homedir(), 'new projects'),
  kWorkspacePath: path.join(os.homedir(), 'workspace'),
};
fixture.editorPref = {
  editorPrefValue,
  getPrefs() {
    return Promise.resolve(fixture.editorPref.editorPrefValue);
  }
};

const editorPathValue = {
  darwin: '/Applications/Unity/Unity.app/Contents/MacOS/Unity',
  win32: path.join(os.homedir(), 'Program Files', 'Unity', 'Editor', 'Unity.exe'),
  linux: '/opt/Unity/Editor/Unity'
}[os.platform()];

const editorAppValue = new EditorApp({ editorPath: editorPathValue });
const invalidEditor = new EditorApp({ editorPath: 'non_existing_path_to_nowhere' });

fixture.editorManager = {
  editorPathValue,
  editorAppValue,
  invalidEditor,

  getEditorApp() {
    return Promise.resolve(fixture.editorManager.editorAppValue);
  },

  getDefaultEditorApp() {
    return Promise.resolve(fixture.editorManager.editorAppValue);
  },
};

const testPackages = [
  'Prototyping', 'Terrain', 'Vehicles', 'Low Poly', 'ShaderForge', 'Trees', 'Water', 'Weather',
  'Skyboxes', 'Effects', 'Navigation', 'Glitch', 'TXAA', 'TextMesh Pro', 'Characters'
];

fixture.createProject = {
  testCases: [
    // [projectName, projectDirectory, projectType, selectedPackages, defaultScene]
    ['test 2d-0', os.homedir(), '2D', [], 'Open World'],
    ['test 2d-4', path.join(os.homedir(), 'src'), '2D', testPackages.slice(-4), 'Main'],
    ['test 3d-0', path.join(os.homedir(), 'projects'), '3D', [], 'Default'],
    ['test 3d-15', os.homedir(), '3D', testPackages, 'New']
  ]
};

module.exports = fixture;
