class PackageHasNoMetadataException {
  constructor(packagePath) {
    this.packagePath = packagePath;
    this.message = `Package ${packagePath} header has no metadata.`;
  }
}

module.exports = PackageHasNoMetadataException;
