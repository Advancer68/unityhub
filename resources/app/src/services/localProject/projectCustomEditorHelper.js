const { hubFS } = require('../../fileSystem');
const path = require('path');
const EditorApp = require('../editorApp/editorapp');

class projectCustomEditorHelper {
  constructor() {
    this.customEditors = {};
  }

  async _isHubInfoExist(projectPath) {
    const isFileExist = await hubFS.pathExists(projectPath, 'hubInfo.json');
    return isFileExist;
  }

  async getCustomEditorPath(projectPath) {
    if (await this._isHubInfoExist(projectPath)) {
      const hubInfo = JSON.parse(await hubFS.readFile(path.join(projectPath, 'hubInfo.json')));
      if (hubInfo.customEditor) {
        if (hubInfo.customEditor.absolutePath) {
          return hubInfo.customEditor.absolutePath;
        }
        if (hubInfo.customEditor.relativePath) {
          return path.join(projectPath, hubInfo.customEditor.relativePath);
        }
      }
    }
    return '';
  }

  async _addCustomEditor(projectPath, editorPath) {
    if (!editorPath) editorPath = await this.getCustomEditorPath(projectPath);
    this.customEditors[projectPath] = editorPath;
  }

  async checkForAndAddCustomEditorPath(projectPath) {
    const editorPath = await this.getCustomEditorPath(projectPath);
    const isCustomEditor = editorPath !== '';
    if (isCustomEditor) this._addCustomEditor(projectPath, editorPath);
    return isCustomEditor;
  }

  hasCustomEditor(projectPath) {
    return this.customEditors[projectPath];
  }

  getCustomEditorForProject(projectPath) {
    const editorPath = this.customEditors[projectPath];
    if (editorPath) {
      return Promise.resolve(new EditorApp({ editorPath }));
    }
    throw Error('No matching project path in list of custom editor');
  }

}

module.exports = new projectCustomEditorHelper();
