'use strict'; // required by mocha

const localStorageAsync = require('electron-json-storage');
const path = require('path');
const _ = require('lodash');
const os = require('os');
const { fs, hubFS } = require('../../fileSystem');
const editorPref = require('../editorPref/editorPref'); // eslint-disable-line
const yaml = require('js-yaml');
const postal = require('postal');
const tarlib = require('tar');
const logger = require('../../logger')('LocalProject');
const editorManager = require('../editorManager/editorManager');
const promisify = require('es6-promisify');
const packageFinder = require('./packageFinder');
const packageHelper = require('./packageHelper');
const settings = require('../localSettings/localSettings');
const templateService = require('../unityTemplate/unityTemplateService');
const windowManager = require('../../windowManager/windowManager');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const packageService = require('./packages');
const hubIPCState = require('../localIPC/hubIPCState');
const licenseClient = require('../licenseService/licenseClientProxy');
const projectCustomEditorHelper = require('./projectCustomEditorHelper');
const outputService = require('../outputService');
const unityTemplateService = require('../unityTemplate/unityTemplateService');
const plasticSCMService = require('../plasticSCMService/plasticSCMService');
const PlasticSCMUtils = require('../plasticSCMService/plastic_scm_utils');

const kProjectSettingsFolderPath = 'ProjectSettings';
const kProjectAssetsFolderPath = 'Assets';
const kProjectVersionPath = path.join(kProjectSettingsFolderPath, 'ProjectVersion.txt');
const kProjectSettingsPath = path.join(kProjectSettingsFolderPath, 'ProjectSettings.asset');
const kProjectPlasticSCMPath = '.plastic';
const PROJECT_DIR_KEY = 'projectDir';
const PROJECTS_INFO = 'projectsInfo';
const PLASTIC_SCM_PLUGIN = 'com.unity.plasticscm-cn';
const packmanRegistry = 'https://packages-v2.unity.cn/';

const baseUserDefault = {
  projectType: '3D',
  defaultProjectName: 'New Unity Project',
  ShowNewProjectButton: true,
  platformPathNameSeparator: path.sep,
  isLaunching: 'true',
  showBackButtons: true
};

const createProjectResultCodes = {
  success: 0,
  missingProjectName: 1,
  invalidProjectName: 2,
  missingPath: 3,
  invalidPath: 4,
  folderNotEmpty: 5
};

const localStorage = {
  get: promisify(localStorageAsync.get, localStorageAsync),
  set: promisify(localStorageAsync.set, localStorageAsync),
};

/**
 * Wrap an action in a try catch block and returns an error as a promise if any.
 *
 * Used to make sure that any code errors (throw, syntax, etc..) are still caught and reported to a client.
 *
 * @param func Function or Promise to execute
 * @returns {*}
 */
const safeAction = function (func) {
  try {
    let result = func;
    if (!(func instanceof Promise)) {
      result = func();
    }

    return result;
  } catch (error) {
    logger.warn(`Unknown error running action: ${error}`);
    return Promise.reject(error);
  }
};

function onLaunchError() {
  windowManager.mainWindow.show();
}

class LockFileError extends Error {
  constructor(errorCode, projectName) {
    super();
    this.name = 'LockFileError';
    this.errorCode = errorCode;
    this.projectName = projectName;
  }
}

class LocalProject {
  constructor() {
    this.recentProjects = [];
    this.refreshUserDefault();
    this.createProjectResultCodes = createProjectResultCodes;
  }

  init() {
    logger.info('Init');
    this.getRecentProjects(true)
      .then(() => this.refreshUserDefault())
      .then(() => this.registerEvents());
  }

  async plasticSCMEnabled(projectPath) {
    const plasticSCMPath = path.join(projectPath, kProjectPlasticSCMPath);

    return await fs.existsSync(plasticSCMPath);
  }

  async getProjectData(projectPath) {
    const fsStatFunc = promisify(fs.stat);
    const fsReadFileFunc = promisify(fs.readFile);

    const versionPath = path.join(projectPath, kProjectVersionPath);
    const settingsPath = path.join(projectPath, kProjectSettingsPath);

    let projectVersion;
    let projectChangeset;
    let lastModified;

    try {
      const projectStat = await fsStatFunc(projectPath);
      lastModified = projectStat.mtime;
    } catch (e) {
      // Invalid projects will be filtered from the list
      lastModified = undefined;
    }

    try {
      const projectVersionFileContents = await fsReadFileFunc(versionPath, 'utf8');
      const versionDoc = yaml.safeLoad(projectVersionFileContents);
      projectVersion = versionDoc.m_EditorVersion;
      const CHANGESET_REGEX = /\(([^)]+)\)/;
      const matches = CHANGESET_REGEX.exec(versionDoc.m_EditorVersionWithRevision);
      projectChangeset = matches ? matches[1] : null;
    } catch (e) {
      projectVersion = null;
    }

    const result = {
      path: projectPath,
      containingFolderPath: path.dirname(projectPath),
      version: projectVersion,
      changeset: projectChangeset,
      lastModified,
      isCustomEditor: await projectCustomEditorHelper.checkForAndAddCustomEditorPath(projectPath),
      plasticSCMEnabled: await this.plasticSCMEnabled(projectPath),
    };

    try {
      let projectSettingsData = await fsReadFileFunc(settingsPath, 'utf8');
      projectSettingsData = projectSettingsData.split('\n').slice(3).join('\n');
      const playerSettings = yaml.safeLoad(projectSettingsData).PlayerSettings;
      result.title = path.basename(projectPath);
      result.cloudProjectId = playerSettings.cloudProjectId;
      result.resultorganizationId = playerSettings.organizationId;
      result.cloudEnabled = playerSettings.cloudEnabled === 0;
      result.localProjectId = playerSettings.productGUID;

    } catch (e) {
      result.title = path.basename(projectPath);
      result.cloudEnabled = false;
    }

    // remove all properties that are null or undefined
    // this fixes that Swagger can't accept null values for fields of a type
    return _.omitBy(result, _.isNil);

  }

  getRefreshInterval() {
    return settings.get(settings.keys.RECENT_PROJECT_INTERVAL);
  }

  getProjectsFromMemory() {
    return Promise.resolve(this.recentProjects);
  }

  /**
   *
   * @param {String} newProjectDirectory If a new project was created before we get the recent projects this path corresponds to that new project.
   * @returns {Promise<any>|Promise<* | never>}
   */

  async getRecentProjects(forceRefresh = false, newProjectDirectory) {

    if (!forceRefresh && !windowManager.mainWindow.isFocused()) {
      return this.getProjectsFromMemory();
    }

    const recentProjectsPath = await editorPref.getRecentProjects();
    const recentProjectsAll = await Promise.all(recentProjectsPath.map(projectPath => this.getProjectData(projectPath)));
    const recentProjects = recentProjectsAll.filter((project) => project && project.lastModified !== undefined);

    if (!_.isEqual(recentProjects, this.recentProjects)) {
      this.recentProjects = recentProjects;
      windowManager.broadcastContent('recent-project.updated', recentProjects, newProjectDirectory);
    }

    return recentProjects;
  }

  getProjectVersion(projectPath) {
    const fsReadFile = promisify(fs.readFile);

    const versionPath = path.join(projectPath, kProjectVersionPath);
    return fsReadFile(versionPath, 'utf8').then(fileContent => {
      try {
        const versionDoc = yaml.safeLoad(fileContent);
        return Promise.resolve(versionDoc.m_EditorVersion);
      } catch (err) {
        logger.info(err);
        // version not found
        return Promise.resolve();
      }
    }).catch((error) => {
      logger.info(error);
      // version not found
      return Promise.resolve();
    });
  }

  chooseProjectDialog() {
    let projectDirectory;
    return this.getUserDefault().then(userDefault => {
      const args = {
        title: 'Select a project to open...',
        defaultPath: userDefault.projectDirectory || os.homedir(),
        properties: ['openDirectory'],
      };

      return windowManager.mainWindow.showOpenFileDialog(args)
        .then((folderName) => { projectDirectory = folderName; })
        .then(() => localStorage.set(PROJECT_DIR_KEY, path.dirname(projectDirectory)))
        .then(() => this.refreshUserDefault())
        .then(() => this.getProjectVersion(projectDirectory))
        .then((editorVersion) => ({ projectDirectory, editorVersion, projectName: path.basename(projectDirectory) }));

    });
  }

  // getProjectsInfo and setProjectsInfo are functions used by advancedSettings to update the local storage for projects info
  async getProjectsInfo() {
    return await localStorage.get(PROJECTS_INFO);
  }

  async setProjectsInfo(value) {
    return await localStorage.set(PROJECTS_INFO, value);
  }

  async clearProjectInfo(projectPath) {
    const infos = await localStorage.get(PROJECTS_INFO);
    delete infos[projectPath];
    await this.setProjectsInfo(infos);
  }

  showOpenOtherDialog(source) {
    return this.chooseProjectDialog()
      .then(({ projectDirectory, editorVersion }) => {
        const editor = editorManager.availableEditors[editorVersion];
        if (!editor) {
          return editorManager.getDefaultEditorVersion()
            .then((preferredEditorVersion) => ({ projectDirectory, editorVersion, preferredEditorVersion }));
        }

        this.openProject(projectDirectory, editorVersion, { source });
        return false;
      });
  }

  chooseDirectory() {
    return this.getUserDefault().then(userDefault => {
      const args = {
        title: 'Select a directory where to save your project...',
        buttonLabel: 'Select Folder',
        defaultPath: userDefault.projectDirectory || os.homedir(),
        properties: ['openDirectory', 'createDirectory']
      };

      return windowManager.newProjectWindow.showOpenFileDialog(args);
    });
  }

  async openProject(projectPath, editorVersion, options) {
    options = options || {};
    const customArgs = await this.getProjectsInfo();

    const cloudAnalyticsInfo = {};
    let projectData = {};

    try {
      await this.validateOpenProject(projectPath);

      logger.info(`openProject projectPath: ${projectPath}, current editor:`, hubIPCState.modalEditor);
      const isModalEditorUndefined = !hubIPCState.modalEditor;
      if (!licenseClient.isLicenseValid()) {
        // TODO: make custom error class - see https://github.cds.internal.unity3d.com/unity/hub.uw-hub/pull/620/files#r41115
        // eslint-disable-next-line no-throw-literal
        throw { errorCode: 'ERROR.LAUNCH_EDITOR.LICENSE' };
      }
      postal.publish({
        channel: 'app',
        topic: 'project.opened',
        data: { projectPath }
      });

      if (isModalEditorUndefined) {
        let editorAppForProject;
        if (projectCustomEditorHelper.hasCustomEditor(projectPath)) {
          editorAppForProject = await projectCustomEditorHelper.getCustomEditorForProject(projectPath);
        } else {
          editorAppForProject = await editorManager.getEditorApp(editorVersion);
        }
        await editorAppForProject.openProject(projectPath, options, customArgs, onLaunchError);
      }

      windowManager.mainWindow.hide();
      projectData = await this.addToRecentProjects(projectPath, editorVersion);

      let targetPlatform;

      if (options.buildPlatform) {
        targetPlatform = options.buildPlatform.buildTargetGroup ? options.buildPlatform.buildTargetGroup : options.buildPlatform.buildTarget;
      }

      cloudAnalyticsInfo.status = 'Success';
      cloudAnalyticsInfo.target_platform = targetPlatform;
    } catch (err) {
      projectData = this.recentProjects.find((project) => project.path === projectPath);
      if (!projectData) {
        projectData = await this.getProjectData(projectPath);
      }
      cloudAnalyticsInfo.status = 'Error';

      if (err instanceof LockFileError) {
        outputService.notifyContent('project.lock_file_error', err.projectName);
      } else {
        throw err;
      }
    } finally {
      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.OPEN_PROJECT,
        msg: {
          editor_version: editorVersion,
          cloudprojectid: projectData ? projectData.cloudProjectId : '',
          organizationid: projectData ? projectData.organizationId : '',
          localprojectid: projectData ? projectData.localProjectId : '',
          source: options.source,
          ...cloudAnalyticsInfo
        }
      });
    }
  }

  async addProject() {
    const projectData = await this.chooseProjectDialog();
    await this.validateOpenProject(projectData.projectDirectory);
    await editorPref.setMostRecentProject(projectData.projectDirectory);
    this.getRecentProjects(true, projectData.projectDirectory);
  }

  async addProjectWithDir(projectDirectory) {
    try {
      await this.validateOpenProject(projectDirectory);
      await editorPref.setMostRecentProject(projectDirectory);
      this.getRecentProjects(true, projectDirectory);

      return Promise.resolve();
    } catch (error) {
      return Promise.reject(error);
    }
  }


  async removeProject(projectPath) {
    await editorPref.deleteProject(projectPath);
    this.getRecentProjects(true);
  }

  closeHomeWindow() {
    windowManager.mainWindow.hide();
  }

  setOrganizationId(organizationId) {
    this.organizationId = organizationId;
  }

  async getTemplatePath() {
    return unityTemplateService._getTemplateDownloadDestination();
  }

  // name of the template with plasticscm: com.xxx.xxx.xxx-plasticscm-{version}.tgz
  async getTemplateWithPlastic(template) {
    const templateName = path.basename(template);
    const distPath = await unityTemplateService._getTemplateDownloadDestination();
    const nameParts = templateName.split('-');
    if (nameParts.length != 2) {
      return template;
    }
    const templateNameWithPlastic = `${nameParts[0]}-plasticscm-${nameParts[1]}`;
    return path.join(distPath, templateNameWithPlastic);
  }

  async createPlasticTemplate(src, dist) {
    const unpackPath = path.dirname(dist);
    const tempDistUnpackPath = path.join(unpackPath, `${new Date().getTime()}`);

    await fs.ensureDir(tempDistUnpackPath);
    await tarlib.extract({file: src, C: tempDistUnpackPath, sync: true});

    const manifestPath = path.join(tempDistUnpackPath, 'package', 'ProjectData~', 'Packages', 'manifest.json');
    const manifestRawdata = fs.readFileSync(manifestPath);
    const manifest = JSON.parse(manifestRawdata);

    manifest.registry = packmanRegistry;
    manifest.dependencies[PLASTIC_SCM_PLUGIN] = await PlasticSCMUtils.getRemotePluginVerion();
    
    fs.writeFileSync(manifestPath, JSON.stringify(manifest, null, 4));
    tarlib.create({file: dist, C: tempDistUnpackPath, sync: true}, ['package']);
    fs.remove(tempDistUnpackPath);
  }

  
  async addPlasticSCMPlugin(projectPath) {
    const manifestPath = path.join(projectPath, 'Packages', 'manifest.json');
    const manifestRawdata = fs.readFileSync(manifestPath);
    const manifest = JSON.parse(manifestRawdata);

    manifest.registry = packmanRegistry;
    manifest.dependencies[PLASTIC_SCM_PLUGIN] = await PlasticSCMUtils.getRemotePluginVerion();

    fs.writeFileSync(manifestPath, JSON.stringify(manifest, null, 4));
  }


  async toggleEnablePlasticSCM(enablePlasticSCM, template) {
    if (!enablePlasticSCM || !fs.existsSync(template)) {
      return Promise.resolve(template);
    }

    const templateWithPlastic = await this.getTemplateWithPlastic(template);
    if (!fs.existsSync(templateWithPlastic)) {
      await this.createPlasticTemplate(template, templateWithPlastic);
    }
    return Promise.resolve(templateWithPlastic);
  }

  async createProject(projectName, projectDirectory, projectTemplate, selectedPackages = [], defaultScene, editorVersion, enableAnalytics, enablePlasticSCM, regionUrl) {
    let template;
    if (projectTemplate) {
      if (projectTemplate.code !== undefined) {
        template = projectTemplate.code; // pre 2018.1
      } else {
        template = projectTemplate.name; // 2018.1+
        if (projectTemplate.tar) {
          template = projectTemplate.tar; // tarball template
        }
      }
    } else {
      template = templateService.defaultTemplateCode;
    }
    
    if (enablePlasticSCM) {
      try {
        await plasticSCMService.createWorkspaceWithBranch(projectName, projectDirectory, regionUrl);
      } catch (error) {
        return Promise.reject(error);
      }
    }
    
    const selectedPackagesName = selectedPackages.map((selectedPackage) => path.basename(selectedPackage));
    
    return safeAction(() => {
      const fullProjectPath = path.join(projectDirectory, projectName);
      const organizationId = this.organizationId;
      logger.info(`createProject projectPath: ${fullProjectPath}, current editor: `, hubIPCState.modalEditor);
      const isModalEditorUndefined = !hubIPCState.modalEditor;
      if (!licenseClient.isLicenseValid()) {
        return Promise.reject({ errorCode: 'ERROR.LAUNCH_EDITOR.LICENSE' });
      }
      postal.publish({
        channel: 'app',
        topic: 'project.create',
        data: { projectName, projectDirectory, projectType: template, selectedPackages, defaultScene, organizationId }
      });

      let returnPromise = Promise.resolve();
      if (isModalEditorUndefined) {
        this.toggleEnablePlasticSCM(enablePlasticSCM, template).then(newTemplate => {
          logger.info('createProject with template', newTemplate);
          
          returnPromise = editorManager.getEditorApp(editorVersion)
        .then(editorApp => editorApp.createProject(fullProjectPath, selectedPackages, newTemplate, organizationId, onLaunchError));
        });
      }
      return returnPromise
        .then(() => {
          windowManager.mainWindow.hide();
          windowManager.onboardingWindow.hide();
        })
        .then(() => localStorage.set(PROJECT_DIR_KEY, projectDirectory))
        .then(() => this.refreshUserDefault())
        // Temporarily update the recent projects list so frontend is updated earlier than next polling.
        .then(() => this.addToRecentProjects(fullProjectPath, editorVersion))
        .then(() => {
          cloudAnalytics.addEvent({
            type: cloudAnalytics.eventTypes.NEW_PROJECT,
            msg: {
              editor_version: editorVersion,
              template_id: `${projectTemplate.name || projectTemplate.code}`,
              organizationid: organizationId,
              localprojectid: undefined,
              project_type: 'normal',
              packages: selectedPackagesName,
              enable_analytics: enableAnalytics,
              status: 'Success',
            }
          });

          if (enablePlasticSCM && !projectTemplate.tar) {
            this.addProjectToPlasticAfterCreated(fullProjectPath);
          }
        });
    }).catch((err) => {
      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.NEW_PROJECT,
        msg: {
          editor_version: editorVersion,
          template_id: `${template}`,
          organizationid: this.organizationId,
          localprojectid: undefined,
          project_type: 'normal',
          packages: selectedPackagesName,
          enable_analytics: enableAnalytics,
          status: 'Error',
        }
      });

      return Promise.reject(err);
    });
  }

  addProjectToPlasticAfterCreated(fullProjectPath) {
    const maxIteratorTime = 1000 * 20;
    const startTime = new Date().getTime();

    const checkAndSetPlastic = setInterval(() => {
      const manifestPath = path.join(fullProjectPath, 'Packages', 'manifest.json');
      const notSupportedManifestPath = path.join(fullProjectPath, 'UnityPackageManager', 'manifest.json');

      const thisTime = new Date().getTime();

      if (fs.existsSync(manifestPath)) {
        this.addPlasticSCMPlugin(fullProjectPath);
        clearInterval(checkAndSetPlastic);
      }

      if (fs.existsSync(notSupportedManifestPath)) {
        clearInterval(checkAndSetPlastic);
      }

      if (thisTime - startTime >= maxIteratorTime) {
        clearInterval(checkAndSetPlastic);
      }
    }, 500);
  }

  createTempProject(packages = [], editorVersion) {
    const isModalEditorDefined = !!hubIPCState.modalEditor;
    if (!licenseClient.isLicenseValid()) {
      return Promise.reject({ errorCode: 'ERROR.LAUNCH_EDITOR.LICENSE' });
    }
    postal.publish({
      channel: 'app',
      topic: 'project.createTemp',
      data: { packages }
    });

    if (isModalEditorDefined) return Promise.resolve();

    const projectTemplate = templateService.defaultTemplateCode;
    const selectedPackagesName = packages.map((selectedPackage) => path.basename(selectedPackage));

    return editorManager.getEditorApp(editorVersion)
      .then(editorApp => editorApp.createTempProject(packages, projectTemplate, onLaunchError))
      .then(() => windowManager.mainWindow.hide())
      .then(() => {
        cloudAnalytics.addEvent({
          type: cloudAnalytics.eventTypes.NEW_PROJECT,
          msg: {
            editor_version: editorVersion,
            template_id: `${projectTemplate}`,
            organizationid: this.organizationId,
            localprojectid: undefined,
            project_type: 'temp',
            packages: selectedPackagesName,
            enable_analytics: false,
            status: 'Success',
          }
        });
      })
      .catch((err) => {
        cloudAnalytics.addEvent({
          type: cloudAnalytics.eventTypes.NEW_PROJECT,
          msg: {
            editor_version: editorVersion,
            template_id: `${projectTemplate}`,
            organizationid: this.organizationId,
            localprojectid: undefined,
            project_type: 'temp',
            packages: selectedPackagesName,
            enable_analytics: false,
            status: 'Error',
          }
        });

        return Promise.reject(err);
      });
  }

  refreshUserDefault() {
    // while we have the cached user defaults, if we request again, we fetch again
    this.userDefault = baseUserDefault;
    this.userDefaultPromise = localStorage.get(PROJECT_DIR_KEY).then((storedProjectDir) => {
      this.userDefault.projectDirectory = _.isEmpty(storedProjectDir) ? os.homedir() : storedProjectDir;
      return this.userDefault;
    }).catch((err) => {
      logger.warn(err);
      this.userDefault.projectDirectory = os.homedir();
      return this.userDefault;
    });
    return this.userDefaultPromise;
  }

  getUserDefault() {
    return this.userDefaultPromise;
  }

  getUniqueProjectName(projectDir, projectName) {
    return path.basename(hubFS.getUniquePath(path.join(projectDir, projectName)));
  }

  getPackageList(customPaths = []) {
    const paths = packageFinder.defaultPaths.concat(customPaths);
    return packageFinder.findMany(paths)
      .then(files => Promise.all(files.map(filename => packageHelper.getPackageMetadata(filename))))
      .then(filesData => {
        // cache for when we want the list but don't want to search again
        this.cachedPackageList = filesData.map(file => ({ path: file.filename, packageInfo: file.json }));

        return this.cachedPackageList;
      });
  }

  registerEvents() {
    postal.subscribe({
      channel: 'app',
      topic: 'project.open',
      callback: (project) => this.openProject(project.projectPath, project.editorVersion, { source: 'tray' })
    });

    return Promise.resolve();
  }

  // Temporarily update the recent projects list so frontend is updated earlier than next polling.
  async addToRecentProjects(projectPath, editorVersion, cloudProjectId, organizationId) {
    let projectData = this.recentProjects.find((project) => project.path === projectPath);
    if (!projectData) {
      projectData = await this.getProjectData(projectPath);
      projectData.lastModified = new Date();
      projectData.resultorganizationId = organizationId;
      this.recentProjects.unshift(projectData);
    } else {
      projectData.lastModified = new Date();
    }

    if (cloudProjectId) {
      projectData.cloudProjectId = cloudProjectId;
    }

    if (editorVersion) {
      projectData.version = editorVersion;
    }
    windowManager.broadcastContent('recent-project.updated', this.recentProjects, projectPath);
    return Promise.resolve(projectData);
  }

  /**
   * Given the chosen path, validates if the project can be opened
   * @param fullPath
   */
  validateOpenProject(fullPath) {
    return this.validatePath(fullPath)
      .then(() => this.validateAssetPath(fullPath))
      .then(() => this.validateLockFile(fullPath));
  }

  /**
   * Given a path, checks if the path contains an asset folder and throw proper error message if not
   * It returns a resolved promise, in case that the asset folder in the path exists
   * @param fullPath
   */
  validateAssetPath(fullPath) {
    return hubFS.isFilePresentPromise(path.join(fullPath, kProjectAssetsFolderPath))
      .catch(() => Promise.reject({ errorCode: 'ERROR.PROJECT.INVALID_PATH', params: { i18n: { path: fullPath } } }));
  }

  /**
   * Given a path, checks if the path exists and throw proper error message if not
   * It returns a resolved promise, in case that the path exists
   * @param fullPath
   */
  validatePath(fullPath) {
    return new Promise((resolve, reject) => {
      if (os.platform() === 'darwin') {
        // Make sure that this is not a home directory or other user folder and spilling into the /Users/username/Library folder
        // Make sure that some basic folders like Downloads, Libary, Desktop are not allowed as project folders
        if (fullPath.match(/^\/Users\/[^/]+\/?$/) ||
            fullPath.match(/^\/Users\/[^/]+\/(Downloads|Library|Desktop|Documents)\/?$/)) {
          reject({ errorCode: 'ERROR.PROJECT.INVALID_PATH', params: { i18n: { path: fullPath } } });
        }
      }
      resolve();
    }).then(() => hubFS.isFilePresentPromise(fullPath)
      .catch(() => Promise.reject({ errorCode: 'ERROR.PROJECT.PATH_NOT_FOUND', params: { i18n: { path: fullPath } } })));
  }

  /**
   * Given a Unity project path, checks if it is open in another editor by examining its UnityLockFile
   * It throw proper error message if the project is open (== UnityLockFile exists and is locked)
   * It returns a resolved promise, if the proejct is not open (== UnityLockFile does not exist or is not locked)
   * @param fullPath
   * @returns {Promise}
   */
  validateLockFile(fullPath) {
    const lockFilePath = path.join(fullPath, 'Temp', 'UnityLockfile');
    let lockFileExists = true;
    return new Promise((resolve, reject) => hubFS.isFilePresentPromise(lockFilePath).catch(() => {
      lockFileExists = false;
      return resolve();
    }).then(() => {
      if (lockFileExists) {
        return hubFS.isFileUnlocked(lockFilePath)
          .then(resolve)
          .catch(() => {
            const projectName = path.basename(fullPath);
            reject(new LockFileError('ERROR.PROJECT.ALREADY_OPEN', projectName));
          });
      }
      return Promise.resolve();
    }));

  }

  needsManifestRelocation(projectPath) {
    return packageService.needsManifestRelocation(projectPath);
  }

  relocateManifest(projectPath) {
    return packageService.relocateManifest(projectPath);
  }

  // this functionality is re-implemented in the main window frontend.
  // we kept it here for new user onboarding window, until we implement it there as well
  validateCreateProject(name = '', directory = '') {
    name = name.replace(/(\/)\1+$/, ''); // remove all '/' chars at end
    directory = directory.replace(/(\/)\1+$/, ''); // remove all '/' chars at end
    const fullPath = path.join(directory, name);
    if (!name) {
      return createProjectResultCodes.missingProjectName;
    } else if (!hubFS.isValidFileName(name)) {
      return createProjectResultCodes.invalidProjectName;
    } else if (!directory) {
      return createProjectResultCodes.missingPath;
    } else if (!hubFS.isValidPath(directory)) {
      return createProjectResultCodes.invalidPath;
    } else if (fs.existsSync(fullPath)) {
      // project dir exists and isn't empty, bail out
      if (fs.readdirSync(fullPath) !== []) return createProjectResultCodes.folderNotEmpty;
    }
    return createProjectResultCodes.success;
  }

}

module.exports = new LocalProject();
