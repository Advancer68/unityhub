const path = require('path');
const { fs } = require('../../fileSystem');

const projectPackages = {
  oldFolder: 'UnityPackageManager',
  newFolder: 'Packages',
  manifestFile: 'manifest.json'
};

const needsManifestCodes = {
  success: 0,
  migrate: 1,
  packagesFolderExists: 2
};

const relocateManifestCodes = {
  success: 0,
  couldNotCreatePackagesFolder: 1,
  couldNotMoveManifest: 2
};

class Packages {
  /**
   * Check if project's manifest needs relocation (eg: 2017.x when UnityPackageManager was the folder name)
   */
  async needsManifestRelocation(projectPath) {
    if (!projectPath) {
      return false;
    }

    const newPackagesFolderPath = path.join(projectPath, projectPackages.newFolder);
    const oldManifestFilePath = path.join(projectPath, projectPackages.oldFolder, projectPackages.manifestFile);
    const newManifestFilePath = path.join(newPackagesFolderPath, projectPackages.manifestFile);

    const newManifestExists = await fs.pathExists(newManifestFilePath);
    const oldManifestExists = await fs.pathExists(oldManifestFilePath);
    const migrationNeeded = !newManifestExists && oldManifestExists;

    let code = needsManifestCodes.success;
    if (migrationNeeded) {
      code = needsManifestCodes.migrate;

      const exists = await fs.pathExists(newPackagesFolderPath);
      if (exists) {
        code = needsManifestCodes.packagesFolderExists;
      }
    }

    return { code };
  }

  /**
   * Migrate the 2017.x project manifest from UnityPackageManager to Packages folder.
   */
  async relocateManifest(projectPath) {
    const newPackagesFolderPath = path.join(projectPath, projectPackages.newFolder);
    const oldManifestFilePath = path.join(projectPath, projectPackages.oldFolder, projectPackages.manifestFile);
    const newManifestFilePath = path.join(newPackagesFolderPath, projectPackages.manifestFile);

    try {
      await fs.mkdir(newPackagesFolderPath);
    } catch (error) {
      return { code: relocateManifestCodes.couldNotCreatePackagesFolder, error: error.message };
    }

    try {
      await fs.rename(oldManifestFilePath, newManifestFilePath);
    } catch (error) {
      return { code: relocateManifestCodes.couldNotMoveManifest, error: error.message };
    }

    return { code: relocateManifestCodes.success };
  }
}

module.exports = new Packages();
