const deepRead = require('recursive-readdir');
const logger = require('../../logger')('PackageFinder');
const { editorHelper } = require('../../common/common');

class UnityPackageFinder {

  constructor() {
    this.defaultPaths = [editorHelper.getDefaultAssetStorePath()];
  }

  // find all .unitypackage files recursively in a directory
  find(dir) {
    return deepRead(dir)
      .then(files => files.filter(file => file.endsWith('.unitypackage')))
      .catch((error) => {
        logger.warn(error);
        return [];
      });

  }

  // find in multiple dirs & flatten the 2d array of files into 1 ordered one
  findMany(dirs = []) {
    return Promise.all(dirs.map(this.find))
      .then(lists => lists.reduce((a, b) => a.concat(b), []).sort());
  }

}

module.exports = new UnityPackageFinder();
