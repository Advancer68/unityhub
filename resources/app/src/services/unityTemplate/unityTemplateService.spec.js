// internal
const templateService = require('./unityTemplateService');
const unityTemplateCacheService = require('./unityTemplateCache');
const outputService = require('../outputService');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const unityPackageManagerService = require('../unityPackageManager/unityPackageManagerService');
const defaultUnityTemplates = require('./resources/defaultTemplates.json');
const { fs } = require('../../fileSystem');
const path = require('path');
const proxyquire = require('proxyquire');
const hubIPCState = require('../localIPC/hubIPCState');

// third parties
const chai = require('chai');
const sinon = require('sinon');
const expect = chai.expect;

chai.use(require('sinon-chai')).use(require('chai-as-promised'));

describe('Templates', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.sandbox.create();
  });

  beforeEach(() => {
    sandbox.stub(cloudAnalytics, 'addEvent');
    hubIPCState.modalEditor = false;
    hubIPCState.editorLocation = '';
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getLocalTemplatesForEditor', () => {
    let localEditorTemplates;
    let downloadedTemplates;

    beforeEach(() => {
      localEditorTemplates = [];
      downloadedTemplates = [];
      sandbox.stub(templateService, '_getEditor').returns({ location: ['somePath'] });
      sandbox.stub(templateService, '_getTemplatesForEditorPath').withArgs('somePath').callsFake(() => localEditorTemplates);
      sandbox.stub(templateService, '_getDownloadedTemplates').withArgs('version').callsFake(() => downloadedTemplates);
      sandbox.stub(templateService, '_getBuiltInPackagesPath').withArgs('somePath').returns('anotherPath');
    });

    describe('under cache hit', () => {
      beforeEach(() => {
        sandbox.stub(unityTemplateCacheService, 'getLocalTemplatesByEditor').returns([{
          id: 'com.unity.template.test@1.0.2',
          name: 'com.unity.template.test',
          tar: path.join('somePath', 'com.unity.template.test-1.0.2.tgz'),
          version: '1.0.2',
          versions: { compatible: ['1.0.2']}
        }]);
      });

      it('should not have called filesystem module', () => {
        expect(templateService._getTemplatesForEditorPath).to.not.have.been.called;
      });
    });

    describe('under cache miss', () => {
      beforeEach(() => {
        sandbox.stub(unityTemplateCacheService, 'getLocalTemplatesByEditor').returns(null);
      });

      it('should get all the local templates', async () => {
        localEditorTemplates = [{name: 'local1'}, {name: 'local2'}];
        downloadedTemplates = [{name: 'local3'}, {name: 'local4'}];
        const templates = await templateService.getLocalTemplatesForEditor('version');

        expect(templates).to.deep.equal([{name: 'local1'}, {name: 'local2'}, {name: 'local3'}, {name: 'local4'}]);
      });

      it('should get all local templates if launched in debug mode', async () => {
        localEditorTemplates = [{name: 'local1'}, {name: 'local2'}];
        downloadedTemplates = [{name: 'local3'}, {name: 'local4'}];
        hubIPCState.modalEditor = true;
        hubIPCState.editorLocation = 'somePath';

        const templates = await templateService.getLocalTemplatesForEditor('version');

        expect(templates).to.deep.equal([{name: 'local1'}, {name: 'local2'}, {name: 'local3'}, {name: 'local4'}]);
      });

      it('should only return the editor local templates if there are no templates downloaded with upm', async () => {
        localEditorTemplates = [{name: 'local1'}, {name: 'local2'}];
        downloadedTemplates = [];
        const templates = await templateService.getLocalTemplatesForEditor('version');

        expect(templates).to.deep.equal([{name: 'local1'}, {name: 'local2'}]);
      });
    });
  });

  describe('loadRemoteTemplatesAsynchronously', () => {
    let remoteTemplates;
    let upgradableTemplates;

    beforeEach(() => {
      remoteTemplates = [];
      upgradableTemplates = [];
      sandbox.stub(templateService, 'getRemoteTemplatesForEditor')
        .withArgs('version').callsFake(() => remoteTemplates)
        .withArgs('wrongVersion').throws(new Error('This is an error message'));
      sandbox.stub(templateService, 'getUpgradableTemplates').callsFake(() => upgradableTemplates);
      sandbox.stub(outputService, 'notifyContent');
    });

    it('should notify that templates are loaded with an array of remote and upgradable arrays if no errors', async () => {
      remoteTemplates = [{name: 'remote1'}];
      upgradableTemplates = [{name: 'upgradable1'}];

      await templateService.loadRemoteTemplatesAsynchronously('version');

      expect(outputService.notifyContent).to.have.been.calledWith('remote.templates.loaded', { remoteTemplates: [{name: 'remote1'}], upgradableTemplates: [{name: 'upgradable1'}], version: 'version' });
    });

    it('should notify an error if something is wrong', async () => {
      await templateService.loadRemoteTemplatesAsynchronously('wrongVersion');

      expect(outputService.notifyContent).to.have.been.calledWith('remote.templates.load_error', { version: 'wrongVersion' });
    })
  });

  describe('getUpgradableTemplates', () => {
    beforeEach(() => {
      sandbox.stub(templateService, 'getLocalTemplatesForEditor').returns([{name: 'locals'}]);
      sandbox.stub(templateService, '_getEditorPath').returns('somePath');
      sandbox.stub(templateService, '_getRegistryPackagesPath').returns('registryPath');
      sandbox.stub(templateService, '_getBuiltInPackagesPath').returns('builtInPath');
      sandbox.stub(templateService, '_getTemplateDownloadDestination').returns('destination');
      sandbox.stub(templateService, '_processUpgradableTemplates');
      sandbox.stub(unityPackageManagerService, 'getUpgradableTemplates');
    });

    it('should call the function getUpgradableTemplates of Unity Package Manager Service', async () => {
      await templateService.getUpgradableTemplates('version');

      expect(unityPackageManagerService.getUpgradableTemplates).to.have.been.calledWith([{name: 'locals'}], {unityVersion: 'version', editorPath: 'registryPath', builtInPackagesPath: 'builtInPath'});
    });
  });

  describe('downloadRemoteTemplate', () => {
    beforeEach(() => {
      sandbox.stub(templateService, 'getLocalTemplatesForEditor').returns([{name: 'locals'}]);
      sandbox.stub(templateService, '_getEditorPath').returns('somePath');
      sandbox.stub(templateService, '_getRegistryPackagesPath').returns('registryPath');
      sandbox.stub(templateService, '_getBuiltInPackagesPath').returns('builtInPath');
      sandbox.stub(templateService, '_getTemplateDownloadDestination').returns('destination');
      sandbox.stub(outputService, 'notifyContent');
    });

    it('should call the function downloadRemoteTemplate of Unity Package Manager Service', async () => {
      sandbox.stub(unityPackageManagerService, 'downloadRemoteTemplate').returns({ name: 'com.downloaded.template', version: '1.2.3' });

      await templateService.downloadRemoteTemplate('com.downloaded.template','version');

      expect(unityPackageManagerService.downloadRemoteTemplate).to.have.been.calledWith('com.downloaded.template', 'destination', {unityVersion: 'version', editorPath: 'registryPath', builtInPackagesPath: 'builtInPath'});
    });

    it('should emit the expected event to the frontend', async () => {
      sandbox.stub(unityPackageManagerService, 'downloadRemoteTemplate').returns({ name: 'com.downloaded.template', version: '1.2.3' });

      await templateService.downloadRemoteTemplate('com.downloaded.template','version');

      expect(outputService.notifyContent).to.have.been.called.calledWith('template.download.end', { templateInfo: { name: 'com.downloaded.template', version: '1.2.3' }, version: 'version' });
    });

    it('should emit the expected analytics event on download success', async () => {
      sandbox.stub(unityPackageManagerService, 'downloadRemoteTemplate').returns({ name: 'com.downloaded.template', version: '1.2.3' });

      await templateService.downloadRemoteTemplate('com.downloaded.template', 'version' );

      expect(cloudAnalytics.addEvent).to.have.been.calledWithExactly({
        type: 'hub.templateDownload.v1',
        msg: {
          editor_version: 'version',
          template_id: 'com.downloaded.template@1.2.3',
          status: 'Success'
        }
      });
    });

    it('should emit the expected analytics events on download failure', async () => {
      sandbox.stub(unityPackageManagerService, 'downloadRemoteTemplate').throws(new Error('some error'));

      await templateService.downloadRemoteTemplate('com.downloaded.template', 'version' );

      expect(cloudAnalytics.addEvent.firstCall).to.have.been.calledWithExactly({
        type: 'hub.templateDownload.v1',
        msg: {
          editor_version: 'version',
          template_id: 'com.downloaded.template',
          status: 'Error'
        }
      });
      expect(cloudAnalytics.addEvent.secondCall).to.have.been.calledWithExactly({
        type: 'hub.error.v1',
        msg: {
          error_message: 'Error downloading dynamic template: some error',
          error_type: 'exception'
        }
      });
    });
  });

  describe('upgradeTemplate', () => {
    beforeEach(() => {
      sandbox.stub(outputService, 'notifyContent');
    });

    it('should notify the start of the upgrade with the template info', async () => {
      sandbox.stub(unityPackageManagerService, 'downloadRemoteTemplate').returns({ name: 'com.upgraded.template', version: '1.2.3' });

      await templateService.upgradeTemplate('com.upgraded.template', 'version');

      expect(outputService.notifyContent).to.have.been.calledWith('template.upgrade.end', { templateInfo: { name: 'com.upgraded.template', version: '1.2.3' }, version: 'version' });
    });

    it('should emit the expected analytics event on upgrade success', async () => {
      sandbox.stub(unityPackageManagerService, 'downloadRemoteTemplate').returns({ name: 'com.upgraded.template', version: '1.2.3' });

      await templateService.upgradeTemplate('com.upgraded.template', 'version');

      expect(cloudAnalytics.addEvent).to.have.been.calledWithExactly({
        type: 'hub.templateUpgrade.v1',
        msg: {
          editor_version: 'version',
          template_id: 'com.upgraded.template@1.2.3',
          status: 'Success'
        }
      });
    });

    it('should emit the expected analytics events on upgrade failure', async () => {
      sandbox.stub(unityPackageManagerService, 'downloadRemoteTemplate').throws(new Error('some error'));

      await templateService.upgradeTemplate('com.upgraded.template', 'version');

      expect(cloudAnalytics.addEvent.firstCall).to.have.been.calledWithExactly({
        type: 'hub.templateUpgrade.v1',
        msg: {
          editor_version: 'version',
          template_id: 'com.upgraded.template',
          status: 'Error'
        }
      });
      expect(cloudAnalytics.addEvent.secondCall).to.have.been.calledWithExactly({
        type: 'hub.error.v1',
        msg: {
          error_message: 'Error upgrading dynamic template: some error',
          error_type: 'exception'
        }
      });
    });
  });

  describe('_getTemplatesForEditorPath', () => {
    it('should return default templates if path is undefined', async () => {
      const templates = await templateService._getTemplatesForEditorPath();

      expect(templates).to.eq(defaultUnityTemplates.templates);
    });

    describe('if package folder does not exist', () => {
      beforeEach(() => {
        sandbox.stub(fs, 'readdir').rejects();
      });

      it('should return default templates', async () => {
        const templates = await templateService._getTemplatesForEditorPath('somePath');

        expect(templates).to.eq(defaultUnityTemplates.templates);
      });
    });

    describe('if scanTemplates returns', () => {
      let templateFolders = ['t1', 't2'];
      let template = {
        name: 't1'
      };

      beforeEach(() => {
        sandbox.stub(fs, 'readdir').resolves(templateFolders);
        sandbox.stub(fs, 'stat').resolves({isDirectory: () => {return true;}});
        sandbox.stub(fs, 'readFile').resolves(JSON.stringify(template));
      });

      it('should return default templates', async () => {
        const templates = await templateService._getTemplatesForEditorPath('somePath');

        expect(JSON.stringify(templates)).equals(JSON.stringify([{name: 't1', status: 'ready', url: ''}, {name: 't1', status: 'ready', url: ''}]));
      });
    });

    describe('if extractTemplateFromTar returns', () => {
      let templateFolders = ['t1', 't2.zip'];
      let template = {
        name: 't1'
      };

      beforeEach(() => {
        sandbox.stub(fs, 'readdir').resolves(templateFolders);
        sandbox.stub(fs, 'stat').callsFake(d => Promise.resolve({isDirectory: () => d === 't1'}));
        sandbox.stub(fs, 'readFile').resolves(JSON.stringify(template));
        sandbox.stub(fs, 'createReadStream').resolves();
      });

      it('should not try to extract wrong file type', async () => {
        await templateService._getTemplatesForEditorPath('somePath');

        expect(fs.createReadStream).not.to.have.been.called;
      });
    });
  });

  describe('_getEditorPath', () => {
    beforeEach(() => {
      sandbox.stub(templateService, '_getEditor')
        .withArgs('a correct version').returns({ location: ['somePath'] })
        .withArgs('a wrong version').throws(new Error());
    });

    it('should return the correct path in debug mode', async () => {
      hubIPCState.modalEditor = true;
      hubIPCState.editorLocation = 'debugPath';

      const editorPath = await templateService._getEditorPath('a correct version');

      expect(editorPath).to.deep.equal('debugPath');
    });

    it('should return the right editor path if the version is available', async () => {
      const editorPath = await templateService._getEditorPath('a correct version');

      expect(editorPath).to.deep.equal('somePath');
    });

    it('should return an empty path if the editor version is not available', async () => {
      const editorPath = await templateService._getEditorPath('a wrong version');

      expect(editorPath).to.deep.equal('');
    });
  });

  describe('_setPreviewFlag', () => {
    it('should set the isPreview field of the template to true when the template name has a preview tag', async () => {
      const templates = await templateService._setPreviewFlag([{displayName: 'template one (Preview)'}, {displayName: 'template two'}]);

      expect(templates).to.deep.equal([{displayName: 'template one', isPreview: true}, {displayName: 'template two'}]);
    });
  });

  describe('_getTemplateDownloadDestination', () => {
    let ensureDirSyncStub;
    let writeJsonSyncStub;
    let pathExists;

    const unityTemplateService = proxyquire('./unityTemplateService', {
      electron: {
        app: {
          getPath: () => 'somePath'
        }
      }
    });

    beforeEach(() => {
      pathExists = true;

      ensureDirSyncStub = sandbox.stub(fs, 'ensureDir');
      writeJsonSyncStub = sandbox.stub(fs, 'writeJson');

      sandbox.stub(fs, 'pathExists').callsFake(() => pathExists);
    });

    it('should return the template path', async () => {
      const templateDownloadDestination = await unityTemplateService._getTemplateDownloadDestination();

      expect(templateDownloadDestination).to.equal(path.join('somePath', 'Templates'));
    });

    it('should ensure the template directory exists', async () => {
      await unityTemplateService._getTemplateDownloadDestination();

      expect(ensureDirSyncStub).to.have.been.calledWithExactly(path.join('somePath', 'Templates'));
    });

    it('should create the manifest.json file if it doesn\'t exist', async () => {
      pathExists = false;

      await unityTemplateService._getTemplateDownloadDestination();

      expect(writeJsonSyncStub).to.have.been.calledWithExactly(path.join('somePath', 'Templates', 'manifest.json'), { });
    });

    it('should not recreate the manifest.json file if it does exist', async () => {
      await unityTemplateService._getTemplateDownloadDestination();

      expect(writeJsonSyncStub).not.to.have.been.called;
    });
  });
});
