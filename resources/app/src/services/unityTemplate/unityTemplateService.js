/**
 * Manages Editor templates.
 *
 * Template object description:
 *
 * {
 *   "name": "3D",    // The template's unique ID
 *   "displayName": "3D", // The template's visual label
 *   "code": 0,     // The code that will be passed to the editor for pre 2018.1
 * },
 */
const { app } = require('electron');
const path = require('path');
const os = require('os');
const tarlib = require('tar');

const { fs } = require('../../fileSystem');
const unityTemplateCacheService = require('./unityTemplateCache');
const unityPackageManagerService = require('../unityPackageManager/unityPackageManagerService');
const outputService = require('../outputService');
const editorManager = require('../editorManager/editorManager');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const hubIPCState = require('../localIPC/hubIPCState');
const logger = require('../../logger')('Templates');

const defaultUnityTemplates = require('./resources/defaultTemplates.json');

const TEMPLATE_STATES = {
  READY: 'ready',
  UPGRADE_AVAILABLE: 'upgradeAvailable',
  NOT_INSTALLED: 'notInstalled',
  INSTALLING: 'installing',
  UPGRADING: 'upgrading'
};

// TODO: Clean this in HUB-2453
const TEMPLATES_PATH = {
  darwin: path.join('Contents', 'Resources', 'PackageManager', 'ProjectTemplates'),
  win32: path.join('..', 'Data', 'Resources', 'PackageManager', 'ProjectTemplates'),
  linux: path.join('..', 'Data', 'Resources', 'PackageManager', 'ProjectTemplates'),
}[os.platform()];

function extractTemplateFromTar(tarPath) {
  return new Promise((resolve) => {
    if (!/^.*\.tgz$/.test(tarPath)) {
      resolve();
      return;
    }
    fs.createReadStream(tarPath).pipe(
      new tarlib.Parse({
        filter: (p) => p === 'package/package.json'
      }).on('entry', (entry) => {
        let buff = '';
        entry.on('data', (chunk) => {
          buff += chunk;
        });
        entry.on('end', () => {
          const pkg = JSON.parse(buff);
          pkg.tar = tarPath;
          resolve(pkg);
        });
      })
    );
  });
}

function scanTemplates(projectTemplates) {
  return fs.readdir(projectTemplates).then((templateFolders) => {
    const promises = templateFolders.map((fileName) => {
      const fullPath = path.join(projectTemplates, fileName);
      return fs.stat(fullPath).then((stat) => {
        if (!stat.isDirectory()) {
          // is it a tarball?
          return extractTemplateFromTar(fullPath);
        }
        return fs.readFile(path.join(fullPath, 'package.json'), 'utf8')
          .then((data) => Promise.resolve(JSON.parse(data)))
          .catch((err) => {
            logger.warn('No package.json found for the template', err.message);
            // no package.json - should not happen in a healthy editor
            return Promise.resolve();
          });
      });
    });
    return Promise.all(promises).then((templates) => {
      templates = templates.filter(Boolean);
      return Promise.resolve(templates);
    });
  });
}

class Templates {
  get previewSuffix() { return '(Preview)'; }

  get defaultTemplateCode() {
    return defaultUnityTemplates.defaultTemplateCode;
  }

  getTemplateStates() { return TEMPLATE_STATES; }

  async getLocalTemplatesForEditor(version) {
    const localEditorTemplates = await this._getTemplatesForEditorVersion(version);
    const localTemplates = await this._getDownloadedTemplates(version);

    const allLocalTemplates = this._mergeLocalTemplates(localEditorTemplates, localTemplates);

    return allLocalTemplates;
  }

  // To support editors pre 2020.2 we must merge the bundled editor templates and templates downloaded via upm
  _mergeLocalTemplates(localEditorTemplates, localTemplates) {
    // Do not include bundled editor templates if a newer version was downloaded via upm
    const localTemplateNames = localTemplates.map(localTemplate => localTemplate.name);
    const filteredLocalEditorTemplates = localEditorTemplates.filter((localEditorTemplate) => !localTemplateNames.includes(localEditorTemplate.name));

    return filteredLocalEditorTemplates.concat(localTemplates);
  }

  async loadRemoteTemplatesAsynchronously(version) {
    try {
      const remoteTemplates = await this.getRemoteTemplatesForEditor(version);
      const upgradableTemplates = await this.getUpgradableTemplates(version);
      outputService.notifyContent('remote.templates.loaded', { remoteTemplates, upgradableTemplates, version });
    } catch (error) {
      logger.warn(`Could not get available templates: ${error.message}`);
      outputService.notifyContent('remote.templates.load_error', { version });
    }
  }

  async getRemoteTemplatesForEditor(version) {
    const localTemplates = await this.getLocalTemplatesForEditor(version);

    const unityInfo = await this._getUnityInfo(version);
    const downloadableTemplates = await this._getRemoteTemplates(unityInfo);

    return this._filterOutLocalTemplates(downloadableTemplates, localTemplates);
  }

  async getUpgradableTemplates(version) {
    const localTemplates = await this.getLocalTemplatesForEditor(version);

    const unityInfo = await this._getUnityInfo(version);

    return this._processUpgradableTemplates(await unityPackageManagerService.getUpgradableTemplates(localTemplates, unityInfo));
  }

  async downloadRemoteTemplate(templateName, version) {
    const templateInfo = { name: templateName };

    try {
      outputService.notifyContent('template.download.start', { templateInfo, version });
      const unityInfo = await this._getUnityInfo(version);
      const destination = await this._getTemplateDownloadDestination();
      const downloadedTemplate = await unityPackageManagerService.downloadRemoteTemplate(templateName, destination, unityInfo);

      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.TEMPLATE_DOWNLOAD,
        msg: {
          editor_version: version,
          template_id: `${templateName}@${downloadedTemplate.version}`,
          status: 'Success'
        }
      });

      outputService.notifyContent('template.download.end', { templateInfo: downloadedTemplate, version });

      return downloadedTemplate;
    } catch (error) {
      if (error.message === 'insufficient space') {
        outputService.notifyContent('remote.templates.space_error', 'downloading', { templateInfo, version });
      } else {
        outputService.notifyContent('remote.templates.install_error', { templateInfo, version });
      }

      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.TEMPLATE_DOWNLOAD,
        msg: {
          editor_version: version,
          template_id: templateName,
          status: 'Error'
        }
      });
      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.ERROR,
        msg: {
          error_message: `Error downloading dynamic template: ${error.message}`,
          error_type: 'exception'
        }
      });

      logger.error(error.message);
    }
    return {};
  }

  async upgradeTemplate(templateName, version) {
    const templateInfo = { name: templateName };

    try {
      outputService.notifyContent('template.upgrade.start', { templateInfo, version });
      const unityInfo = await this._getUnityInfo(version);
      const destination = await this._getTemplateDownloadDestination();
      const updatedTemplate = await unityPackageManagerService.downloadRemoteTemplate(templateName, destination, unityInfo);

      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.TEMPLATE_UPDATE,
        msg: {
          editor_version: version,
          template_id: `${templateName}@${updatedTemplate.version}`,
          status: 'Success'
        }
      });

      outputService.notifyContent('template.upgrade.end', { templateInfo: updatedTemplate, version });
    } catch (error) {
      if (error.message === 'insufficient space') {
        outputService.notifyContent('remote.templates.space_error', 'upgrading', { templateInfo, version });
      } else {
        outputService.notifyContent('templates.upgrade_error', { templateInfo, version });
      }

      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.TEMPLATE_UPDATE,
        msg: {
          editor_version: version,
          template_id: templateName,
          status: 'Error'
        }
      });
      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.ERROR,
        msg: {
          error_message: `Error upgrading dynamic template: ${error.message}`,
          error_type: 'exception'
        }
      });

      logger.error(error.message);
    }
    return {};
  }

  async _getTemplateDownloadDestination() {
    const templatePath = path.join(app.getPath('userData'), 'Templates');
    const templateManifest = path.join(templatePath, 'manifest.json');
    const templateManifestExists = await fs.pathExists(templateManifest);

    await fs.ensureDir(templatePath);

    if (!templateManifestExists) {
      await fs.writeJson(templateManifest, { });
    }

    return templatePath;
  }

  async _getEditorPath(version) {
    // For internal use cases
    // Running from a custom editor build or debug hub requires this check
    if (hubIPCState.modalEditor && hubIPCState.editorLocation) {
      logger.info('using hub ipc editor location');
      return hubIPCState.editorLocation;
    }

    try {
      const editor = await this._getEditor(version);
      const editorPath = editor.location[0];

      return editorPath;
    } catch (error) {
      logger.warn('failed to find the editor location, are you sure you have this editor installed on your machine ?');
      return '';
    }
  }

  async _getUnityInfo(version) {
    const unityEditorInfoCache = unityTemplateCacheService.getLocalEditorVersionInfo(version);
    if (unityEditorInfoCache) {
      return unityEditorInfoCache;
    }

    const editorPath = await this._getEditorPath(version);
    const registryPackagesPath = await this._getRegistryPackagesPath(editorPath);
    const builtInPackagesPath = this._getBuiltInPackagesPath(editorPath);

    const unityEditorInfo = {
      unityVersion: version,
      editorPath: registryPackagesPath,
      builtInPackagesPath
    };

    unityTemplateCacheService.setLocalEditorVersionInfo(version, unityEditorInfo);

    return unityEditorInfo;
  }

  _filterOutLocalTemplates(remoteTemplates, localTemplates) {
    return remoteTemplates
      .filter(remoteTemplate => !localTemplates.map(localTemplate => localTemplate.name).includes(remoteTemplate.name));
  }

  async _getEditor(version) {
    return await editorManager.getEditor(version);
  }

  _getRegistryPackagesPath(editorPath) {
    return editorManager.getRegistryPackagesPath(editorPath);
  }

  _getBuiltInPackagesPath(editorPath) {
    return editorManager.getBuiltInPackagesPath(editorPath);
  }

  async _getTemplatesForEditorVersion(version) {
    const localTemplatesCache = unityTemplateCacheService.getLocalTemplatesByEditor(version);
    if (localTemplatesCache) {
      return localTemplatesCache;
    }

    const editorPath = await this._getEditorPath(version);
    const localTemplates = await this._getTemplatesForEditorPath(editorPath);
    unityTemplateCacheService.setLocalTemplatesByEditor(version, localTemplates);
    return localTemplates;
  }

  _getTemplatesForEditorPath(editorPath) {
    if (!editorPath) {
      logger.warn('Editor not found, the hub will fall back to default templates');
      return Promise.resolve(defaultUnityTemplates.templates);
    }

    return scanTemplates(path.join(editorPath, TEMPLATES_PATH))
      .then(templates => this._processLocalEditorTemplates(templates))
      .then(templates => {
        if (templates.length > 0) {
          return Promise.resolve(this._setPreviewFlag(templates));
        }
        logger.info('No templates bundled with the editor');
        return Promise.resolve(defaultUnityTemplates.templates);
      }).catch((error) => {
        logger.warn(error);
        return Promise.resolve(defaultUnityTemplates.templates);
      });
  }

  _setPreviewFlag(templates) {
    return templates.map(template => {
      if (template.displayName && template.displayName.includes(this.previewSuffix)) {
        template.displayName = template.displayName.replace(this.previewSuffix, '').trim();
        template.isPreview = true;
      }

      return template;
    });
  }

  async _getDownloadedTemplates(version) {
    const unityInfo = await this._getUnityInfo(version);
    const destination = await this._getTemplateDownloadDestination();
    const localTemplates = await unityPackageManagerService.getDownloadedTemplatesForEditorVersion(destination, unityInfo);

    // If the resolvedPath is empty then the package is not downloaded locally
    return this._processLocalTemplates(localTemplates);
  }

  async _getRemoteTemplates(unityInfo) {
    return this._processDownloadableTemplates(await unityPackageManagerService.getDownloadableTemplates(unityInfo));
  }

  _processUpgradableTemplates(upgradableTemplates) {
    return this._processTemplates(upgradableTemplates, TEMPLATE_STATES.UPGRADE_AVAILABLE);
  }

  _processLocalEditorTemplates(localEditorTemplates) {
    return this._processTemplates(localEditorTemplates, TEMPLATE_STATES.READY);
  }

  _processLocalTemplates(localTemplates) {
    return this._processTemplates(localTemplates, TEMPLATE_STATES.READY);
  }

  _processDownloadableTemplates(downloadableTemplates) {
    return this._processTemplates(downloadableTemplates, TEMPLATE_STATES.NOT_INSTALLED);
  }

  _processTemplates(templates, status) {
    try {
      return templates.map(({ name, displayName, version, description, type, tar, tarball: { url } = { url: '' }, dynamicIcon }) => ({
        name,
        displayName,
        version,
        description,
        type,
        status,
        tar,
        url,
        dynamicIcon
      }));
    } catch (error) {
      logger.warn(`Could not process the templates : ${error.message}`);
      throw error;
    }

  }
}

module.exports = new Templates();
