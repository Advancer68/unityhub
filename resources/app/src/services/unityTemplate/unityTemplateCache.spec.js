const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);

const path = require('path');
const unityTemplateCacheService = require('./unityTemplateCache');

describe('unityTemplateCache', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.sandbox.create();
  });
  afterEach(() => {
    sandbox.restore();
  });

  describe('setLocalTemplatesByEditor', () => {
    beforeEach(() => {
      sandbox.stub(unityTemplateCacheService, 'set');
    });

    it('should called set with key localTemplates:editorVersion', () => {
      unityTemplateCacheService.setLocalTemplatesByEditor('1.0.2', [{
        id: 'com.unity.template.test@1.0.2',
        name: 'com.unity.template.test',
        tar: path.join('somePath', 'com.unity.template.test-1.0.2.tgz'),
        version: '1.0.2',
        versions: { compatible: ['1.0.2']}
      }]);
      expect(unityTemplateCacheService.set)
        .to.have.been.calledWith(
          'localTemplates:1.0.2',
          [{
            id: 'com.unity.template.test@1.0.2',
            name: 'com.unity.template.test',
            tar: path.join('somePath', 'com.unity.template.test-1.0.2.tgz'),
            version: '1.0.2',
            versions: { compatible: ['1.0.2']}
          }]);
    });
  });

  describe('getLocalTemplatesByEditor', () => {
    beforeEach(() => {
      sandbox.stub(unityTemplateCacheService, 'get');
    });

    it('should called set with key localTemplates:editorVersion', () => {
      unityTemplateCacheService.getLocalTemplatesByEditor('1.0.2');
      expect(unityTemplateCacheService.get).to.have.been.calledWith('localTemplates:1.0.2');
    });
  });

  describe('setLocalEditorVersionInfo', () => {
    beforeEach(() => {
      sandbox.stub(unityTemplateCacheService, 'set');
    });

    it('should called set with key editorVersionInfo:', () => {
      unityTemplateCacheService.setLocalEditorVersionInfo('1.0.2',
        {
          unityVersion: '1.0.2',
          editorPath: '/path/to/editor/1.0.2',
          builtInPackagesPath: '/path/to/editor/1.0.2/packages'
        });
      expect(unityTemplateCacheService.set)
        .to.have.been.calledWith(
        'editorVersionInfo:1.0.2',
        {
          unityVersion: '1.0.2',
          editorPath: '/path/to/editor/1.0.2',
          builtInPackagesPath: '/path/to/editor/1.0.2/packages'
        });
    });
  });

  describe('getLocalEditorVersionInfo', () => {
    beforeEach(() => {
      sandbox.stub(unityTemplateCacheService, 'get');
    });

    it('should called get with key setTemplatesInfo:templateName:editorInfo', () => {
      unityTemplateCacheService.getLocalEditorVersionInfo('1.0.2');
      expect(unityTemplateCacheService.get)
        .to.have.been.calledWith('editorVersionInfo:1.0.2');
    });
  });

});
