const axios = require('axios');
const promisify = require('es6-promisify');
const localStorageAsync = require('electron-json-storage');

const localStorage = {
  get: promisify(localStorageAsync.get, localStorageAsync),
  set: promisify(localStorageAsync.set, localStorageAsync),
};
const adsConfigKey = 'ads-config';

class AdsService {
  constructor() {
    this.endpoint = 'https://public-cdn.cloud.unitychina.cn/hub/prod/top-bar';
  }

  init() {
    this.syncConfig();
  }

  async getConfig() {
    return await localStorage.get(adsConfigKey);
  }

  async syncConfig() {
    const response = await axios.get(`${this.endpoint}/ads.json`, null, { responseType: 'json' });
    localStorage.set(adsConfigKey, this.parseConfig(response.data));
  }

  parseConfig(data) {
    if (!data) {
      return {};
    }

    const { ads, url } = data;
    return {
      ads: `${this.endpoint}/${ads}`,
      url
    };
  }
}

module.exports = new AdsService();
