const _ = require('lodash');

class BaseEditorPref {
  
  static get RecentProjectPrefix() { return 'RecentlyUsedProjectPaths'; }
  static get MaxRecentProjectEntries() { return 40; }
  
  /**
   * Platform specific method implemented in platform handler sub-class
   */
  getPrefs() {
    throw Error('method getPrefs not overridden.');
  }
  
  async getRecentProjects() {
    let recentProjects = await this.getPrefs(BaseEditorPref.RecentProjectPrefix);
    
    _.each(recentProjects, (value, key) => {
      value = this._convertPathSeparators(value);
      value = this._trimTrailingSlashes(value);
      recentProjects[key] = value;
    });
    
    recentProjects = this._getSortedRecentProjectPaths(recentProjects);
    
    return _.uniq(recentProjects);
  }
  
  async setMostRecentProject(mostRecentProject) {
    mostRecentProject = this._convertPathSeparators(mostRecentProject);
    let recentProjects = await this.getRecentProjects();
    
    // Remove duplicate of most recent project.
    recentProjects = recentProjects.filter((recentProject) => recentProject !== mostRecentProject);
    
    // Add most recent project as the first element.
    recentProjects.unshift(mostRecentProject);
    
    await this._updateRecentProjectsInEditorPreferences(recentProjects);
  }

  async deleteProject(projectPath) {
    const recentProjects = await this.getRecentProjects();
    const index = recentProjects.indexOf(projectPath);
    if (index >= 0) {
      recentProjects.splice(index, 1);
    }
    await this._updateRecentProjectsInEditorPreferences(recentProjects);
  }
  
  _getSortedRecentProjectPaths(recentProjects) {
    return Object.keys(recentProjects).sort().map(key => recentProjects[key]);
  }
  
  _trimTrailingSlashes(value) {
    return value.replace(/\/$/g, '');
  }
  
  /**
   * Parity function: Paths are converted to forward slashes on all OSes to conform to editor code.
   */
  _convertPathSeparators(value) {
    return value.replace(/\\/g, '/');
  }
  
  /**
   * Platform specific method implemented in platform handler sub-class
   * @param recentProjects
   * @returns {Promise<void>}
   * @private
   */
  async _updateRecentProjectsInEditorPreferences() {
    throw Error('method _updateRecentProjectsInEditorPreferences not overridden.');
  }
}

module.exports = BaseEditorPref;
