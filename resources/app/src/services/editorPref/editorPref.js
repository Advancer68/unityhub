const tools = require('../editorApp/platformtools');

const EditorPref = tools.require(`${__dirname}/editorPref`);
module.exports = new EditorPref();
