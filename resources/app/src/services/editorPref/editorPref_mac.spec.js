const proxyquire = require('proxyquire');
const { hubFS } = require('../../fileSystem');

const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);

/**
 * Mock dependency that is not installed on platforms other than windows.
 * @type {{getKeyValues(), getRegistryKeyName(), setBinaryValue(), deleteKey(), "@noCallThru": boolean}}
 */
const plist = {
  readFile() {},
  writeFile() {},
  '@noCallThru': true
};

const EditorPrefMac = proxyquire('./editorPref_mac', {
  'simple-plist': plist
});

describe('EditorPref Mac', () => {
  let sandbox, editorPref;

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    editorPref = new EditorPrefMac();
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getPrefs', () => {
    let keyPrefix, prefPath, fileSystem, result, errorMessage;

    beforeEach(() => {
      fileSystem = {};
      errorMessage = 'BLAAAAH';

      sandbox.stub(hubFS, 'isFilePresentPromise').returns(Promise.resolve());

      sandbox.stub(plist, 'readFile').callsFake((filePath, callback) => {
        const preferences = fileSystem[filePath];

        if (!preferences) {
          callback(errorMessage);
        } else {
          callback(null, preferences);
        }
      });
    });

    describe('when no argument is passed', () => {
      let expectedResult;
      beforeEach(() => {
        expectedResult = {
          foo: 'bar',
          zoo: 'baz'
        };

        fileSystem[EditorPrefMac.defaultPrefsPaths] = expectedResult;
      });

      it('should fetch all preferences in default path', async () => {
        result = await editorPref.getPrefs(keyPrefix, prefPath);
        expect(result).to.eql(expectedResult);
      });
    });

    describe('when key prefix is passed', () => {
      let expectedResult;

      beforeEach(() => {
        expectedResult = {
          foo: 'bar',
          for: 'foo',
          fow: 'bur'
        };
        keyPrefix = 'fo';

        fileSystem[EditorPrefMac.defaultPrefsPaths] = Object.assign({zoo: 'baz'}, expectedResult);
      });

      it('should fetch all preferences with keys matching the passed prefix', async () => {
        result = await editorPref.getPrefs(keyPrefix, prefPath);
        expect(result).to.eql(expectedResult);
      });
    });

    describe('when key prefix and preferences path is passed', () => {
      let expectedResult;

      beforeEach(() => {
        expectedResult = {
          foo: 'bar',
          for: 'foo',
          fow: 'bur'
        };

        keyPrefix = 'fo';
        prefPath = 'hohoohoh';

        fileSystem[prefPath] = Object.assign({zoo: 'baz'}, expectedResult);
      });

      it('should fetch all prefs with keys matching the passed prefix in passed path', async () => {
        result = await editorPref.getPrefs(keyPrefix, prefPath);
        expect(result).to.eql(expectedResult);
      });
    });

    describe('when the preferences are not found', () => {
      it('should return empty object', async () => {
        result = await editorPref.getPrefs(keyPrefix, prefPath);
        expect(result).to.eql({});
      });
    });
  });

  describe('getPrefs when pref file doesnt exist', () => {
    beforeEach(() => {
      sandbox.stub(hubFS, 'isFilePresentPromise').returns(Promise.reject());
    });

    it('should return empty object', async () => {
      result = await editorPref.getPrefs("foo", "bar");
      expect(result).to.eql({});
    });
  });

  describe('_updateRecentProjectsInEditorPreferences', () => {
    let recentProjects, preferences, plistWriteFileError;

    beforeEach(() => {
      recentProjects = ['foo', 'foo/bar'];
      preferences = {};
      plistWriteFileError = null;

      sandbox.stub(editorPref, 'getPrefs').callsFake(() => preferences);
      sandbox.stub(plist, 'writeFile').callsFake((prefPath, prefs, callback) => callback(plistWriteFileError));
    });

    it('should set properly formatted keys for passed recent projects', async () => {
      await editorPref._updateRecentProjectsInEditorPreferences(recentProjects);
      expect(plist.writeFile).to.have.been.calledWith(sinon.match.string, {
        [`${EditorPrefMac.RecentProjectPrefix}-0`]: 'foo',
        [`${EditorPrefMac.RecentProjectPrefix}-1`]: 'foo/bar'
      });
    });

    it('should not have kept the keys beyond the number of recent projects', async () => {
      preferences = {
        [`${EditorPrefMac.RecentProjectPrefix}-2`]: 'hohoh',
        [`${EditorPrefMac.RecentProjectPrefix}-3`]: 'heheh',
        [`${EditorPrefMac.RecentProjectPrefix}-4`]: 'hihih',
      };
      await editorPref._updateRecentProjectsInEditorPreferences(recentProjects);
      expect(plist.writeFile).to.have.been.calledWith(sinon.match.string, {
        [`${EditorPrefMac.RecentProjectPrefix}-0`]: 'foo',
        [`${EditorPrefMac.RecentProjectPrefix}-1`]: 'foo/bar'
      });
    });

    it('should write the preferences to the default preferences path', async () => {
      await editorPref._updateRecentProjectsInEditorPreferences(recentProjects);
      expect(plist.writeFile).to.have.been.calledWith(EditorPrefMac.defaultPrefsPaths, sinon.match.object);
    });

    it('should not fail if writing the preferences threw an exception', () => {
      plistWriteFileError = new Error('Blaaaaah');
      return expect(() => editorPref._updateRecentProjectsInEditorPreferences(recentProjects)).not.to.throw;
    });
  });
});
