const Registry = require('@unityhub/unity-editor-registry'); // eslint-disable-line
const registry = require('../editorApp/lib/registry_win');
const BaseEditorPref = require('./baseEditorPref');
const logger = require('../../logger')('EditorPrefWin');

class EditorPrefWin extends BaseEditorPref {
  getPrefs(keyPrefix = '', registryKey = registry.keys.prefs5x) {
    return new Promise((resolve) => {
      try {
        const regKeyValues = Registry.getKeyValues(registryKey);
        if (regKeyValues.length === 2) {
          const keys = regKeyValues[0];
          const values = regKeyValues[1];
          const preferences = {};
          for (let i = 0; i < keys.length; i++) {
            const key = keys[i];
            // if keyPrefix exists, add the key only if it starts with prefixKey
            // if keyPrefix was undefined, add all the keys to preferences
            if (key.startsWith(keyPrefix)) {
              preferences[key] = values[i];
            }
          }
          resolve(preferences);
        } else {
          resolve({});
        }
      } catch (err) {
        logger.warn('Exception happened while reading the editor Preferences', err);
        resolve({});
      }
    });
  }
  
  async _updateRecentProjectsInEditorPreferences(recentProjects) {
    for (let i = 0; i < BaseEditorPref.MaxRecentProjectEntries; i++) {
      const key = this._formatRecentProjectKey(i);
      
      if (i < recentProjects.length) {
        this._setKeyValue(key, recentProjects[i]);
      } else {
        this._deleteKey(key);
      }
    }
  }
  
  _formatRecentProjectKey(index) {
    return Registry.getRegistryKeyName(`${BaseEditorPref.RecentProjectPrefix}-${index}`);
  }
  
  _setKeyValue(key, value) {
    Registry.setBinaryValue(`${registry.keys.hkcu}\\${registry.keys.prefs5x}`, key, value);
  }
  
  _deleteKey(key) {
    Registry.deleteKey(`${registry.keys.hkcu}\\${registry.keys.prefs5x}`, key);
  }
}

module.exports = EditorPrefWin;
