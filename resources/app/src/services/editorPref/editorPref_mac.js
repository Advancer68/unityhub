const os = require('os');
const plist = require('simple-plist');
const { promisify } = require('util');
const BaseEditorPref = require('./baseEditorPref');
const logger = require('../../logger')('EditorPrefMac');
const { hubFS } = require('../../fileSystem');

const defaultPrefsPaths = `${os.homedir()}/Library/Preferences/com.unity3d.UnityEditor5.x.plist`;

class EditorPrefMac extends BaseEditorPref {

  static get defaultPrefsPaths() { return defaultPrefsPaths; }

  async getPrefs(keyPrefix = '', prefPath = EditorPrefMac.defaultPrefsPaths) {
    try {
      await hubFS.isFilePresentPromise(prefPath);
    } catch (err) {
      logger.warn(`com.unity3d.UnityEditor5.x.plist does not exist: ${err}`);
      return {};
    }

    try {
      const preferences = await promisify(plist.readFile)(prefPath);
      return Object.keys(preferences)
        .filter((key) => key.startsWith(keyPrefix))
        .reduce((obj, key) => {
          obj[key] = preferences[key];
          return obj;
        }, {});
    } catch (err) {
      logger.warn('Exception happened while reading the editor Preferences', err);
      return {};
    }
  }

  async _updateRecentProjectsInEditorPreferences(recentProjects) {
    const preferences = await this.getPrefs();

    for (let i = 0; i < BaseEditorPref.MaxRecentProjectEntries; i++) {
      const key = this._formatRecentProjectKey(i);

      if (i < recentProjects.length) {
        preferences[key] = recentProjects[i];
      } else {
        delete preferences[key];
      }
    }

    await this._setPrefs(preferences);
  }

  _formatRecentProjectKey(index) {
    return `${BaseEditorPref.RecentProjectPrefix}-${index}`;
  }

  _setPrefs(preferences) {
    return promisify(plist.writeFile)(EditorPrefMac.defaultPrefsPaths, preferences)
      .catch(error => {
        logger.warn('Could not update editor preferences. Error occurred.', error);
      });
  }
}

module.exports = EditorPrefMac;
