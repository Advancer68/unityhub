const _ = require('lodash');
const BaseEditorPref = require('./baseEditorPref');

const chai = require('chai');
const expect = chai.expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);

describe('BaseEditorPref', () => {
  let editorPref, sandbox, updateRecentProjectsStub;
  
  /**
   * Dummy child class to test base.
   *
   * Since base methods depend on child implementation not to
   * throw exceptions (like setMostRecentProject), this basic dummy class is necessary.
   */
  class EditorPref extends BaseEditorPref{
    getPrefs() { }
    async _updateRecentProjectsInEditorPreferences(recentProjects) {
      await updateRecentProjectsStub(recentProjects);
    }
  }
  
  beforeEach(() => {
    editorPref = new EditorPref();
    sandbox = sinon.sandbox.create();
    
    updateRecentProjectsStub = sandbox.stub().resolves();
  });
  
  afterEach(() => {
    sandbox.restore();
  });
  
  describe('getRecentProjects', () => {
    let prefs, projectPaths, result;
    
    beforeEach(() => {
      projectPaths = {
        trailingSlashes: '/fun/path/',
        badPathSeparators: '\\path\\path\\path',
        normalRecentProject: '/path/to/project',
        duplicatePath: '/path/to/project',
      };
      
      prefs = { };
      
      sandbox.stub(editorPref, 'getPrefs').callsFake(prefix => _.omitBy(prefs, (value, key) => !key.startsWith(prefix)));
    });
    
    it('should return recentProjects', async () => {
      prefs = {
        [`${BaseEditorPref.RecentProjectPrefix}-0`]: 'foo/bar',
        [`${BaseEditorPref.RecentProjectPrefix}-1`]: 'foo/baz',
        potato: 5,
        [`${BaseEditorPref.RecentProjectPrefix}-2`]: 'bar/baz',
      };
      
      result = await editorPref.getRecentProjects();
      
      const expectedResults = [
        'foo/bar',
        'foo/baz',
        'bar/baz'
      ];
      
      expect(result).to.eql(expectedResults);
    });
    
    it('should have removed trailing slashes from paths', async () => {
      prefs = {
        [`${BaseEditorPref.RecentProjectPrefix}-0`]: projectPaths.trailingSlashes,
      };
      
      result = await editorPref.getRecentProjects();
      
      expect(_.endsWith(result[0], '/')).to.be.false;
    });
    
    it('should have converted path separators', async () => {
      prefs = {
        [`${BaseEditorPref.RecentProjectPrefix}-0`]: projectPaths.badPathSeparators,
      };
  
      result = await editorPref.getRecentProjects();
      
      expect(result[0]).not.to.include('\\');
    });
    
    it('should have sorted the paths by key name', async () => {
      prefs = {
        [`${BaseEditorPref.RecentProjectPrefix}-1`]: 'Second',
        [`${BaseEditorPref.RecentProjectPrefix}-2`]: 'Last',
        [`${BaseEditorPref.RecentProjectPrefix}-0`]: 'First'
      };
  
      result = await editorPref.getRecentProjects();
  
      expect(result).to.eql(['First', 'Second', 'Last']);
    });
    
    it('should have removed duplicate paths', async () => {
      prefs = {
        [`${BaseEditorPref.RecentProjectPrefix}-0`]: 'foo/bar',
        [`${BaseEditorPref.RecentProjectPrefix}-1`]: 'foo/bar',
        [`${BaseEditorPref.RecentProjectPrefix}-2`]: 'foo/bar',
        [`${BaseEditorPref.RecentProjectPrefix}-3`]: 'foo/foo'
      };
  
      result = await editorPref.getRecentProjects();
      
      expect(result).to.eql(['foo/bar', 'foo/foo']);
    });
  });
  
  describe('setMostRecentProject', () => {
    let recentProjects, mostRecentProject;
    
    beforeEach(() => {
      mostRecentProject = 'most/recent/project';
      recentProjects = ['foo', 'bar', 'foo/bar'];
      sandbox.stub(editorPref, 'getRecentProjects').callsFake(() => recentProjects);
    });
    
    it('should set passed project path as most recent project', async () => {
      await editorPref.setMostRecentProject(mostRecentProject);
      const newRecentProjects = updateRecentProjectsStub.getCall(0).args[0];
      expect(newRecentProjects[0]).to.equal(mostRecentProject);
    });
    
    it('should have converted path separators in passed path', async () => {
      mostRecentProject = 'fe\\fi\\fo';
      
      await editorPref.setMostRecentProject(mostRecentProject);
      
      const newRecentProjects = updateRecentProjectsStub.getCall(0).args[0];
      expect(newRecentProjects[0]).to.equal('fe/fi/fo');
    });
    
    it('should have removed duplicate project from recent project list', async () => {
      recentProjects.push(mostRecentProject);
      
      await editorPref.setMostRecentProject(mostRecentProject);
      
      const newRecentProjects = updateRecentProjectsStub.getCall(0).args[0];
      newRecentProjects.splice(0, 1);
      newRecentProjects.forEach(project => expect(project).not.to.equal(mostRecentProject));
    });
    
    it('should have updated recent projects in editor preferences', async () => {
      await editorPref.setMostRecentProject(mostRecentProject);
      expect(updateRecentProjectsStub).to.have.been.calledWith([mostRecentProject].concat(recentProjects));
    });
  });

  describe('deleteProject', () => {
    let recentProjects, projectPath;
    
    beforeEach(() => {
      projectPath = 'deleted/project';
      recentProjects = ['foo', 'bar', 'foo/bar', 'deleted/project'];
      sandbox.stub(editorPref, 'getRecentProjects').callsFake(() => recentProjects);
    });
    
    it('should delete the selected project', async () => {
      await editorPref.deleteProject(projectPath);
      const newRecentProjects = updateRecentProjectsStub.getCall(0).args[0];
      expect(newRecentProjects.indexOf('deleted/project')).to.equal(-1);
    });
  });
});