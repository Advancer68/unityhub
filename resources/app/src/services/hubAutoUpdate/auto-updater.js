const isDev = require('electron-is-dev');
const updateHandlers = require('./updateHandlers');
const electronBuilderUpdater = require('electron-updater').autoUpdater;
const logger = require('../../logger')('AutoUpdater');
const settings = require('../localSettings/localSettings');
const localConfig = require('../localConfig/localConfig');
const windowManager = require('../../windowManager/windowManager');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const systemInfo = require('../localSystemInfo/systemInfo');
const BetaConfig = require('./betaConfig');
const postal = require('postal');
const util = require('util');
const promisify = require('es6-promisify');
const localStorageCallback = require('electron-json-storage');

const localStorage = {
  get: promisify(localStorageCallback.get),
  set: promisify(localStorageCallback.set),
};

const MAIN_PAGE_WAIT_INTERVAL = 10000;
const RELEASE_CHANNEL_STORAGE_KEY = 'hubReleaseChannel';
const BETA_PROMOTION_KEY = 'betaPromotion';
const ELECTRON_CHANNEL = {
  production: 'latest',
  beta: 'beta'
};
const USER_SELECTION = {
  latest: 'production',
  beta: 'beta'
};

class AutoUpdater {

  constructor() {
    // updateAvailable used in the front end to hide/show the update message
    this.updateAvailable = false;
    // postponeMessage is set to true when user clicks on x in frontend.
    // postponeMessage is set to false when next update check is triggered
    this.postponeMessage = false;
    // the version of available update
    this.availableVersion = undefined;
    // a flag equals to connectInfo.online
    this.online = false;
    // downloadInProgress is true if an update is available but download is not finished (to avoid multiple downloads)
    this.downloadInProgress = false;
    // current version of unity hub, used for deciding to show the link to the release notes or not
    this.currentVersion = systemInfo.hubVersion();
    // auto updater checking for updates (not downloading)
    this.isUpdateCheckOnly = false;

    // user is targeted for beta
    this.isBetaTargeted = false;
    // user has been promoted to before flag
    this.hasUserSeenBetaPromotionBefore = true;
  }

  async init() {
    electronBuilderUpdater.autoDownload = false;
    electronBuilderUpdater.allowDowngrade = false;

    logger.info('Init');
    this.updateInterval = settings.get(settings.keys.CHECK_FOR_UPDATE_INTERVAL);

    if (!isDev && !localConfig.isAutoUpdateDisabled()) {
      await this.setBetaConfigs();
      await this.initializeUpdateChannel();
      this.setUpdateServerPath();
      updateHandlers.registerUpdateEventHandlers(this, electronBuilderUpdater);
      logger.info('Checking for hub updates');
      await this.checkForUpdate();

      setInterval(() => {
        this.checkForUpdate();
      }, this.updateInterval);

      postal.subscribe({
        channel: 'app',
        topic: 'connectInfo.changed',
        callback: (data) => {
          if (data.connectInfo.online === true && this.online === false) {
            this.online = data.connectInfo.online;
            this.checkForUpdate();
            return;
          }
          this.online = data.connectInfo.online;
        },
      });
    } else {
      logger.info('Auto update is disabled');
    }

    return Promise.resolve();
  }

  async setBetaConfigs() {
    this.isBetaTargeted = await BetaConfig.isBetaTargeted();

    if (this.isBetaTargeted) {
      this._checkForPreviousBetaPromotionEvent();
    }
  }

  async _checkForPreviousBetaPromotionEvent() {
    try {
      const { seenBefore } = await localStorage.get(BETA_PROMOTION_KEY);
      this.hasUserSeenBetaPromotionBefore = !!seenBefore;
    } catch (e) {
      logger.warn(`Could not determine if user has been beta promoted before, default to true: ${e}`);
    }
  }

  setUpdateServerPath() {
    const updateServerPath = settings.get(settings.keys.UPDATE_SERVER_PATH);

    if (updateServerPath !== undefined) {
      logger.info(`Look for Hub updates at ${updateServerPath}`);
      electronBuilderUpdater.setFeedURL(updateServerPath);
    } else {
      logger.warn('Missing update server path');
    }
  }

  async initializeUpdateChannel() {
    let currentChannel;

    try {
      currentChannel = await localStorage.get(RELEASE_CHANNEL_STORAGE_KEY);
      if (currentChannel !== ELECTRON_CHANNEL.production && currentChannel !== ELECTRON_CHANNEL.beta) {
        currentChannel = ELECTRON_CHANNEL.production;
      }
    } catch (err) {
      logger.error('retrieving previous users channel selection failed, defaulting to production');
      currentChannel = ELECTRON_CHANNEL.production;
    }

    this._setElectronUpdaterChannel(currentChannel);
  }

  async setHubToBetaChannel() {
    logger.info('update to beta channel');
    await this._setUserSeenBetaPromotionBefore();
    await this.setUpdateChannel(ELECTRON_CHANNEL.beta);
  }

  async setHubToProductionChannel() {
    logger.info('roll back to production channel');
    await this.setUpdateChannel(ELECTRON_CHANNEL.production);
  }

  async userDeclinesBetaPromotion() {
    logger.info('User chose to opt out of beta promotion');
    await this._setUserSeenBetaPromotionBefore();
  }

  async setUpdateChannel(channel) {
    try {
      await localStorage.set(RELEASE_CHANNEL_STORAGE_KEY, channel);
      this._setElectronUpdaterChannel(channel);
      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.UPDATE_CHANNEL_CHANGE,
        msg: {
          channel_selection: channel
        },
      });
    } catch (err) {
      logger.error('storing users channel selection failed', err);
      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.ERROR,
        msg: {
          error_message: `Update channel changed failed, could not switch to ${channel}`,
          error_type: 'exception'
        }
      });
    }
    this.checkForUpdate();
  }

  async _setUserSeenBetaPromotionBefore() {
    // Do not keep setting this value if there is no need
    if (this.hasUserSeenBetaPromotionBefore) {
      return;
    }

    try {
      await localStorage.set(BETA_PROMOTION_KEY, { seenBefore: true });
      this.hasUserSeenBetaPromotionBefore = true;
      logger.info('User will not see beta promotion again');
    } catch (e) {
      logger.warn(`Problem setting user seen promotion before key: ${e}`);
    }
  }

  async getPreselectedChannel() {
    let currentChannel;
    try {
      currentChannel = await localStorage.get(RELEASE_CHANNEL_STORAGE_KEY);
    } catch (err) {
      logger.error('retrieving users channel selection failed, defaulting to production');
    }
    if (!currentChannel || !USER_SELECTION[currentChannel]) {
      return USER_SELECTION.latest;
    }
    return USER_SELECTION[currentChannel];
  }

  isUpdateFromBetaPending() {
    const isCurrentBuildBeta = this.currentVersion.includes(ELECTRON_CHANNEL.beta);
    return this.updateAvailable && isCurrentBuildBeta;
  }

  restart() {
    logger.info('Restarting to update');
    return cloudAnalytics.quitEvent('Update').then(() => {
      electronBuilderUpdater.quitAndInstall(true, true);
    }).catch((err) => {
      logger.error('An error occurred while sending analytics:', err);
      electronBuilderUpdater.quitAndInstall(true, true);
    });
  }

  quitWithUpdate() {
    logger.info('Restarting to update');
    return cloudAnalytics.quitEvent('Update').then(() => {
      electronBuilderUpdater.quitAndInstall(true, false);
    }).catch((err) => {
      logger.error('An error occurred while sending analytics:', err);
      electronBuilderUpdater.quitAndInstall(true, false);
    });
  }

  remindLater() {
    this.postponeMessage = true;
    setTimeout(() => {
      if (this.updateAvailable === true) {
        this.postponeMessage = false;
        windowManager.broadcastContent('auto-update.update-available', this.availableVersion);
      }
    }, this.updateInterval);
  }

  async checkForUpdate() {
    if (this.downloadInProgress === false) {
      try {
        await electronBuilderUpdater.checkForUpdates();
        await this._checkForBetaUpdate();
      } catch (error) {
        logger.warn(`Failed to check for updates:  ${util.inspect(error)}`);
      }
    }
  }

  async _checkForBetaUpdate() {
    if (!this._shouldCheckForBetaUpdate()) {
      return;
    }

    await this._checkForChannelUpdateWithNoDownload(ELECTRON_CHANNEL.beta);
  }

  _shouldCheckForBetaUpdate() {
    return electronBuilderUpdater.channel === ELECTRON_CHANNEL.production
      && this.isBetaTargeted
      && !this.hasUserSeenBetaPromotionBefore;
  }

  async _checkForChannelUpdateWithNoDownload(channel) {
    const previousChannelSetting = electronBuilderUpdater.channel;

    this._setElectronUpdaterChannel(channel);
    this.isUpdateCheckOnly = true;

    try {
      const channelUpdateInfo = await electronBuilderUpdater.checkForUpdates();

      if (this._isChannelVersion(channel, channelUpdateInfo.versionInfo)) {
        logger.info(`${channel} version was detected: ${channelUpdateInfo.versionInfo.version}`);
        this._notifyFrontendWhenReady(channel);
      }
    } catch (e) {
      logger.error(`Could not check for ${channel} version update: ${e}`);
    }

    this._setElectronUpdaterChannel(previousChannelSetting);
    this.isUpdateCheckOnly = false;
  }

  _isChannelVersion(channel, versionInfo) {
    return versionInfo.version.includes(channel);
  }

  _notifyFrontendWhenReady(channel) {
    // Must wait for the page to be visible before notifying the mainWindow
    const interval = setInterval(() => {
      if (windowManager.mainWindow.isVisible()) {
        windowManager.broadcastContent(`auto-update.${channel}-available`);
        clearInterval(interval);
      }
    }, MAIN_PAGE_WAIT_INTERVAL);
  }

  // This is required because under the hood, electron updater will update the allowDowngrade flag
  // We always must check if downgrade is allowed to prevent unwanted side effects
  _setElectronUpdaterChannel(channel) {
    windowManager.broadcastContent('auto-update.channel-changed');
    electronBuilderUpdater.channel = channel;
    this._updateDowngradeAllowed();
  }

  async _updateDowngradeAllowed() {
    // Only allow downgrade for users that are on the production channel but using beta
    electronBuilderUpdater.allowDowngrade = electronBuilderUpdater.channel === ELECTRON_CHANNEL.production
      && this.currentVersion.includes(ELECTRON_CHANNEL.beta);
  }
}

module.exports = new AutoUpdater();
