const axios = require('axios');
const cloudConfig = require('../cloudConfig/cloudConfig');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const licenseCore = require('../licenseService/licenseCore');
const settings = require('../localSettings/localSettings');
// const logger = require('../../logger')('BetaBuildConfig');

const { app } = require('electron');

class BetaConfig {
  static async isBetaTargeted() {
    try {
      const betaConfigMap = await this._getBetaConfigMap();
      const licenseType = await this._getLicenseType();
      const countryCode = this._getCountryCode();

      return this._resolveBetaTargetedStatus(betaConfigMap, licenseType, countryCode);
    } catch (err) {
      // logger.error(err);
      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.ERROR,
        msg: {
          error_message: 'Could not determine beta targeted status of the user',
          error_type: 'exception'
        }
      });
      return false;
    }
  }

  static _getBetaConfigUrl() {
    return settings.get(settings.keys.BETA_CONFIG_PATH) || `${cloudConfig.getHubBaseEndpoint()}betaConfig.json`;
  }

  static async _getBetaConfigMap() {
    const betaConfigUrl = this._getBetaConfigUrl();
    const result = await axios.get(betaConfigUrl);
    return result.data;
  }

  static async _getLicenseType() {
    const licenseKind = await licenseCore.getLicenseKind(true);
    return licenseKind;
  }

  static _getCountryCode() {
    return app.getLocaleCountryCode();
  }

  static _resolveBetaTargetedStatus(betaConfigMap, licenseType, countryCode) {
    const eligibleCountries = betaConfigMap[licenseType];
    return Array.isArray(eligibleCountries) && eligibleCountries.includes(countryCode);
  }
}

module.exports = BetaConfig;
