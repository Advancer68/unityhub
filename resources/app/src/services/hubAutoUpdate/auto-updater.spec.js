// internal
const updateHandlers = require('./updateHandlers');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const settings = require('../localSettings/localSettings');
const electronBuilderUpdater = require('electron-updater').autoUpdater;
const BetaConfig = require('./betaConfig');
const postal = require('postal');
const localConfig = require('../localConfig/localConfig');
const windowManager = require('../../windowManager/windowManager');

// third parties
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const proxyquire = require('proxyquire');
const localStorage = require('electron-json-storage');

chai.use(sinonChai);
chai.use(chaiAsPromised);

describe('AutoUpdater', () => {
  let sandbox;
  let autoUpdater;
  let postalStub;
  let settingsMock;
  let registerUpdateEventHandlersStub;
  let checkForUpdatesStub;
  let localSettingsStub;
  let localStorageSetStub;
  let localStorageGetValue;
  let localStorageSetValue;
  let checkForUpdatesValue;

  describe('without custom server path', () => {
    beforeEach(() => {
      sandbox = sinon.sandbox.create();

      settingsMock = {
        [settings.keys.CHECK_FOR_UPDATE_INTERVAL]: 20000
      };
      localStorageGetValue = undefined;
      localStorageSetValue = undefined;
      checkForUpdatesValue = undefined;

      windowManager.broadcastContent = sandbox.stub();
      localSettingsStub = sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);
      localStorageSetStub = sandbox.stub(localStorage, 'set').callsFake(() => localStorageSetValue);
      registerUpdateEventHandlersStub = sandbox.stub(updateHandlers, 'registerUpdateEventHandlers').returns();
      checkForUpdatesStub = sandbox.stub(electronBuilderUpdater, 'checkForUpdates').callsFake(() => checkForUpdatesValue);
      postalStub = sandbox.stub(postal, 'subscribe');
      sandbox.stub(localConfig, 'isAutoUpdateDisabled').returns(false);
      sandbox.stub(localStorage, 'get').callsFake(() => localStorageGetValue);
      sandbox.stub(cloudAnalytics, 'addEvent');

      autoUpdater = proxyquire('./auto-updater', {
        'electron-is-dev': false,
        updateHandlers: {registerUpdateEventHandlers: registerUpdateEventHandlersStub},
        electronBuilderUpdater: { checkForUpdates: checkForUpdatesStub },
        '../localSettings/localSettings': {get: localSettingsStub},
        'es6-promisify': f => f
      });
    });

    afterEach(() => {
      sandbox.restore();
    });

    describe('constructor', () => {
      it('should set the variables to default values', () => {
        expect(autoUpdater.updateAvailable).equals(false);
        expect(autoUpdater.postponeMessage).equals(false);
        expect(autoUpdater.availableVersion).equals(undefined);
        expect(autoUpdater.online).equals(false);
        expect(autoUpdater.downloadInProgress).equals(false);
      });
    });

    describe('init', () => {
      beforeEach(async () => {
        sandbox.stub(electronBuilderUpdater, 'setFeedURL');
        sandbox.stub(BetaConfig, 'isBetaTargeted').resolves(true);
        sandbox.useFakeTimers();
      });

      it('should not call electronBuilderUpdater.setFeedUrl if not set in the config', async () => {
        settingsMock[settings.keys.UPDATE_SERVER_PATH] = undefined;

        await autoUpdater.init();

        expect(electronBuilderUpdater.setFeedURL).not.to.have.been.called;
      });

      it('should set electronBuilderUpdater.autoDownload to false', async () => {
        await autoUpdater.init();

        expect(electronBuilderUpdater.autoDownload).equals(false);
      });

      it('should set the updateInterval from settings', async () => {
        await autoUpdater.init();

        expect(autoUpdater.updateInterval).equals(20000);
      });

      it('should initiate update event handlers', async () => {
        await autoUpdater.init();

        expect(updateHandlers.registerUpdateEventHandlers).to.have.been.calledWith(autoUpdater, electronBuilderUpdater);
      });

      it('should check for updates', async () => {
        sandbox.stub(autoUpdater, 'checkForUpdate');

        await autoUpdater.init();

        expect(autoUpdater.checkForUpdate).to.have.been.called;
      });

      it('should subscribe to connectInfo.changed', async () => {
        await autoUpdater.init();

        expect(JSON.stringify(postalStub.getCall(0).args[0])).to.equal(JSON.stringify({
          callback: function callback() {
          }, channel: 'app', topic: 'connectInfo.changed'
        }));
      });

      describe('beta feature initialization', () => {
        it('should set the beta targeted status from betaConfig', async () => {
          await autoUpdater.init();

          expect(BetaConfig.isBetaTargeted).to.have.been.called;
          expect(autoUpdater.isBetaTargeted).to.equal(true);
        });

        it('should set that the user has not seen the beta promotion before if key not set on machine', async () => {
          // {} is returned if key is not set
          localStorageGetValue = {};

          await autoUpdater.init();

          expect(autoUpdater.hasUserSeenBetaPromotionBefore).to.equal(false);
        });

        it('should set that the user has seen the beta promotion before', async () => {
          localStorageGetValue = { seenBefore: true };

          await autoUpdater.init();

          expect(autoUpdater.hasUserSeenBetaPromotionBefore).to.equal(true);
        });

        it('should set that the user has seen the beta promotion before on backend error', async() => {
          localStorageGetValue = Promise.reject('some error');

          await autoUpdater.init();

          expect(autoUpdater.hasUserSeenBetaPromotionBefore).to.equal(true);
        });

        it('should set the update channel to latest', async () => {
          localStorageGetValue = 'latest';

          await autoUpdater.init();

          expect(electronBuilderUpdater.channel).to.equal('latest');
        });

        it('should set the update channel to beta', async () => {
          localStorageGetValue = 'beta';

          await autoUpdater.init();

          expect(electronBuilderUpdater.channel).to.equal('beta');
        });

        it('should set the update channel to latest on error', async () => {
          localStorageGetValue = Promise.reject('some error');

          await autoUpdater.init();

          expect(electronBuilderUpdater.channel).to.equal('latest');
        });

        it('should not allow downgrade if the user is on production but using a standard version', async () => {
          localStorageGetValue = 'latest';
          autoUpdater.currentVersion = '1.2.3';

          await autoUpdater.init();

          expect(electronBuilderUpdater.allowDowngrade).to.equal(false);
        });

        it('should not allow downgrade if user is on the beta channel', async () => {
          localStorageGetValue = 'beta';
          autoUpdater.currentVersion = '1.2.3';

          await autoUpdater.init();

          expect(electronBuilderUpdater.allowDowngrade).to.equal(false);
        });

        it('should allow downgrade if user is on the production channel and using a beta version', async () => {
          localStorageGetValue = 'latest';
          autoUpdater.currentVersion  = '1.2.3-beta';

          await autoUpdater.init();

          expect(electronBuilderUpdater.allowDowngrade).to.equal(true);
        });
      });
    });

    describe('isUpdateFromBetaPending', () => {
      let currentVersionStub;
      let updateAvailableStub;

      beforeEach(() => {
        currentVersionStub = sandbox.stub(autoUpdater, 'currentVersion').value('2.3.4-beta');
        updateAvailableStub = sandbox.stub(autoUpdater, 'updateAvailable').value(true);
      });

      afterEach(() => {
        sandbox.restore();
      });

      it('should return true if the current version is a beta', () => {
        const result = autoUpdater.isUpdateFromBetaPending();
        expect(result).equals(true);
      });

      it('should return false if the current version is not a beta', () => {
        currentVersionStub.value('2.3.4');
        const result = autoUpdater.isUpdateFromBetaPending();
        expect(result).equals(false);
      });

      it('should return false if there is no update available', () => {
        updateAvailableStub.value(false);
        const result = autoUpdater.isUpdateFromBetaPending();
        expect(result).equals(false);
      });
    });

    describe('restart', () => {
      beforeEach(() => {
        sandbox.stub(electronBuilderUpdater, 'quitAndInstall');
        sandbox.stub(cloudAnalytics, 'quitEvent').resolves();
        autoUpdater.restart();

      });
      it('should add a quit event to cloudAnalytics', () => {
        autoUpdater.restart().then(() => {
          expect(cloudAnalytics.quitEvent).to.have.been.called;
        });
      });
      it('should quitAndInstall', () => {
        autoUpdater.restart().then(() => {
          expect(electronBuilderUpdater.quitAndInstall).to.have.been.called;
        });
      });
    });

    describe('remindLater', () => {
      beforeEach(() => {
        autoUpdater.remindLater();
      });
      it('should set postponeMessage to true', () => {
        expect(autoUpdater.postponeMessage).equals(true);
      });
    });

    describe('checkForUpdate', () => {

      it('should check for updates if download is not in progress', () => {
        autoUpdater.downloadInProgress = false;

        autoUpdater.checkForUpdate();
        expect(electronBuilderUpdater.checkForUpdates).to.have.been.called;
      });
      it('should not check for updates if download is in progress', () => {
        autoUpdater.downloadInProgress = true;

        autoUpdater.checkForUpdate();
        expect(electronBuilderUpdater.checkForUpdates).to.have.not.been.called;
      });
    });

    describe('getPreselectedChannel', () => {
      describe('local storage returns beta as preselected channel', () => {
        it('should return beta', async () => {
          localStorageGetValue = 'beta';

          const channel = await autoUpdater.getPreselectedChannel();

          expect(channel).to.equal('beta');
        });
      });

      describe('local storage returns production as preselected channel', () => {
        it('should return production', async () => {
          localStorageGetValue = 'production';

          const channel = await autoUpdater.getPreselectedChannel();

          expect(channel).to.equal('production');
        });
      });

      describe('local storage returns null as preselected channel', () => {
        it('should return production', async () => {
          localStorageGetValue = undefined;

          const channel = await autoUpdater.getPreselectedChannel();

          expect(channel).to.equal('production');
        });
      });

      describe('local storage throws error', () => {
        it('should return production', async () => {
          localStorageGetValue = Promise.reject('some error');

          const channel = await autoUpdater.getPreselectedChannel();

          expect(channel).to.equal('production');
        });
      });
    });

    describe('setHubToBetaChannel', () => {
      describe('local storage stores user channel selection', () => {
        it('should set electron update channel to beta', async () => {
          await autoUpdater.setHubToBetaChannel();

          expect(electronBuilderUpdater.channel).to.equal('beta');
        });

        it('should set electron update channel to latest', async () => {
          await autoUpdater.setHubToBetaChannel();

          expect(electronBuilderUpdater.checkForUpdates).to.have.been.called;
        });

        it('should set that the user has seen beta promotion before to true', async () => {
          autoUpdater.hasUserSeenBetaPromotionBefore = false;

          await autoUpdater.setHubToBetaChannel();

          expect(autoUpdater.hasUserSeenBetaPromotionBefore).to.equal(true);
        });

        it('should emit the expected analytics event on channel switch to beta', async () => {
          await autoUpdater.setHubToBetaChannel();

          expect(cloudAnalytics.addEvent).to.have.been.calledWith({
            type: 'hub.releaseChannelUpdated.v1',
            msg: {
              channel_selection: 'beta'
            }
          });
        });
      });

      describe('local storage throws error', () => {
        beforeEach(() => {
          localStorageSetValue = Promise.reject('some error');
        });

        it('should set electron update channel to beta', async () => {
          await autoUpdater.setHubToBetaChannel();

          expect(electronBuilderUpdater.channel).to.equal('beta');
        });

        it('should set electron update channel to latest', async () => {
          await autoUpdater.setHubToBetaChannel();

          expect(electronBuilderUpdater.checkForUpdates).to.have.been.called;
        });

        it('should emit the expected analytics event on channel switch error', async () => {
          await autoUpdater.setHubToBetaChannel();

          expect(cloudAnalytics.addEvent).to.have.been.calledWith({
            type: 'hub.error.v1',
            msg: {
              error_message: 'Update channel changed failed, could not switch to beta',
              error_type: 'exception'
            }
          });
        });
      });
    });

    describe('setHubToProductionChannel', () => {
      describe('local storage stores user channel selection', () => {
        it('should set electron update channel to latest', async () => {
          await autoUpdater.setHubToProductionChannel();

          expect(electronBuilderUpdater.channel).to.equal('latest');
        });

        it('should set electron update channel to latest', async () => {
          await autoUpdater.setHubToProductionChannel();

          expect(electronBuilderUpdater.checkForUpdates).to.have.been.called;
        });

        it('should emit the expected analytics event on channel switch to production', async () => {
          await autoUpdater.setHubToProductionChannel();

          expect(cloudAnalytics.addEvent).to.have.been.calledWith({
            type: 'hub.releaseChannelUpdated.v1',
            msg: {
              channel_selection: 'latest'
            }
          });
        });
      });

      describe('local storage throws error', () => {
        beforeEach(() => {
          localStorageSetValue = Promise.reject('some error');
        });

        it('should set electron update channel to latest', async () => {
          await autoUpdater.setHubToProductionChannel();

          expect(electronBuilderUpdater.channel).to.equal('latest');
        });

        it('should set electron update channel to latest', async () => {
          await autoUpdater.setHubToProductionChannel();

          expect(electronBuilderUpdater.checkForUpdates).to.have.been.called;
        });

        it('should emit the expected analytics event on channel switch error', async () => {
          await autoUpdater.setHubToProductionChannel();

          expect(cloudAnalytics.addEvent).to.have.been.calledWith({
            type: 'hub.error.v1',
            msg: {
              error_message: 'Update channel changed failed, could not switch to latest',
              error_type: 'exception'
            }
          });
        });
      });
    });

    describe('userDeclinesBetaPromotion', () => {
      it('should set that the user has seen beta promotion before to true', async () => {
        autoUpdater.hasUserSeenBetaPromotionBefore = false;

        await autoUpdater.userDeclinesBetaPromotion();

        expect(autoUpdater.hasUserSeenBetaPromotionBefore).to.equal(true);
      });

      it('should not call electron-json-storage if beta promotion was already seen', async () => {
        autoUpdater.hasUserSeenBetaPromotionBefore = true;

        await autoUpdater.userDeclinesBetaPromotion();

        expect(localStorageSetStub).to.not.have.been.called;
      });

      it('should not set that the user has seen beta promotion before if there is an error', async () => {
        autoUpdater.hasUserSeenBetaPromotionBefore = false;
        localStorageSetValue = Promise.reject('some error');

        await autoUpdater.userDeclinesBetaPromotion();

        expect(autoUpdater.hasUserSeenBetaPromotionBefore).to.equal(false);
      });
    });

    describe('checkForUpdate', () => {
      describe('regular update check', () => {
        it('should call electron updater checkForUpdates', async () => {
          await autoUpdater.checkForUpdate();

          expect(electronBuilderUpdater.checkForUpdates).to.have.been.called;
        });

        if('should call electron updater checkForUpdates once if the user is on the beta channel', async () => {
          electronBuilderUpdater.channel = 'beta';

          await autoUpdater.checkForUpdate();

          expect(electronBuilderUpdater.checkForUpdates).to.have.been.calledOnce;
        });

        it('should call electron updater checkForUpdates once if user is not beta targeted', async () => {
          electronBuilderUpdater.channel = 'latest';
          autoUpdater.isBetaTargeted = false;

          await autoUpdater.checkForUpdate();

          expect(electronBuilderUpdater.checkForUpdates).to.have.been.calledOnce;
        });

        it('should call electron updater checkForUpdates once if user has seen beta promotion before', async () => {
          electronBuilderUpdater.channel = 'latest';
          autoUpdater.isBetaTargeted = true;
          autoUpdater.hasUserSeenBetaPromotionBefore = true;

          await autoUpdater.checkForUpdate();

          expect(electronBuilderUpdater.checkForUpdates).to.have.been.calledOnce;
        });
      });

      describe('beta update check', () => {
        beforeEach(() => {
          electronBuilderUpdater.channel = 'latest';
          autoUpdater.isBetaTargeted = true;
          autoUpdater.hasUserSeenBetaPromotionBefore = false;
          autoUpdater.isUpdateCheckOnly = false;
        });

        it('should call electron updater checkForUpdates twice if appropriate', async () => {
          await autoUpdater.checkForUpdate();

          expect(electronBuilderUpdater.checkForUpdates).to.have.been.calledTwice;
        });

        it('should leave the system in the expected state if beta is available', async () => {
          checkForUpdatesValue = { versionInfo: { version: '1.2.3-beta' } };

          await autoUpdater.checkForUpdate();

          expect(electronBuilderUpdater.channel).to.equal('latest');
          expect(autoUpdater.isUpdateCheckOnly).to.equal(false);
        });

        it('should leave the system in the expected state if beta is not available', async () => {
          checkForUpdatesValue = { versionInfo: { version: '1.2.3' } };

          await autoUpdater.checkForUpdate();

          expect(electronBuilderUpdater.channel).to.equal('latest');
          expect(autoUpdater.isUpdateCheckOnly).to.equal(false);
        });

        it('should leave the system in the expected state if beta update check fails', async () => {
          checkForUpdatesValue = Promise.reject('some error');

          await autoUpdater.checkForUpdate();

          expect(electronBuilderUpdater.channel).to.equal('latest');
          expect(autoUpdater.isUpdateCheckOnly).to.equal(false);
        });

        describe('beta-available event', () => {
          const intervalWaitTime = 10001;
          let clock;
          let mainWindowReady;

          beforeEach(() => {
            clock = sandbox.useFakeTimers();
            mainWindowReady = true;

            sandbox.stub(windowManager.mainWindow, 'isVisible').callsFake(() => mainWindowReady);
          });

          it('should emit the beta-available event', async () => {
            checkForUpdatesValue = { versionInfo: { version: '1.2.3-beta' } };

            await autoUpdater.checkForUpdate();

            clock.tick(intervalWaitTime);

            expect(windowManager.broadcastContent).to.have.been.calledWithExactly('auto-update.beta-available');
          });

          it('should emit the beta-available event only once because setInterval is cleared away', async () => {
            checkForUpdatesValue = { versionInfo: { version: '1.2.3-beta' } };

            await autoUpdater.checkForUpdate();

            clock.tick(intervalWaitTime * 2);

            expect(windowManager.broadcastContent.withArgs('auto-update.beta-available')).to.have.been.calledOnce;
          });

          it('should call the frontend twice if the frontend is not ready after the first interval', async () => {
            checkForUpdatesValue = { versionInfo: { version: '1.2.3-beta' } };
            mainWindowReady = false;

            await autoUpdater.checkForUpdate();

            clock.tick(intervalWaitTime);

            mainWindowReady = true;

            clock.tick(intervalWaitTime);

            expect(windowManager.mainWindow.isVisible).to.have.been.calledTwice;
          });

          it('should not emit the beta-available event if checkForUpdates errors', async () => {
            checkForUpdatesValue = Promise.reject('some error');

            await autoUpdater.checkForUpdate();

            expect(windowManager.broadcastContent).to.not.have.been.called;
          });
        });
      });
    })
  });


  describe('with custom server path', () => {
    beforeEach(() => {
      sandbox = sinon.sandbox.create();
      settingsMock = {
        [settings.keys.CHECK_FOR_UPDATE_INTERVAL]: 20000,
          [settings.keys.UPDATE_SERVER_PATH]: 'http://www.customserver.com/'
      };
      let localSettingsStub = sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);
      let registerUpdateEventHandlersStub = sandbox.stub(updateHandlers, 'registerUpdateEventHandlers').returns();
      let checkForUpdatesStub = sandbox.stub(electronBuilderUpdater, 'checkForUpdates').resolves();
      let localStorageStub = sandbox.stub(localStorage, 'get').resolves();
      postalStub = sandbox.stub(postal, 'subscribe');
      sandbox.stub(localConfig, 'isAutoUpdateDisabled').returns(false);

      autoUpdater = proxyquire('./auto-updater', {
        'electron-is-dev': false,
        updateHandlers: {registerUpdateEventHandlers: registerUpdateEventHandlersStub},
        electronBuilderUpdater: {checkForUpdates: checkForUpdatesStub},
        '../localSettings/localSettings': {get: localSettingsStub},
        localStorage: localStorageStub
      });

    });

    afterEach(() => {
      sandbox.restore();
    });

    describe('init', () => {
      beforeEach(async () => {
        sandbox.stub(autoUpdater, 'checkForUpdate');
        sandbox.stub(electronBuilderUpdater, 'setFeedURL');
        sandbox.useFakeTimers();
        await autoUpdater.init();
      });

      it('should call electronBuilderUpdater.setFeedUrl if set in the config', () => {
        expect(electronBuilderUpdater.setFeedURL).to.have.been.calledWith(settingsMock[settings.keys.UPDATE_SERVER_PATH]);
      });
    });
  });
});
