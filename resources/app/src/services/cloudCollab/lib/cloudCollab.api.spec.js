// internal
const cloudConfig = require('../../cloudConfig/cloudConfig');
const collabApiClient = require('./cloudCollab.api');


// third parties
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const nock = require('nock');

chai.use(chaiAsPromised);
chai.use(sinonChai);

describe('cloudCollabApiClient', () => {
  let sandbox, collabApiMock, token;

  beforeEach(() => {
    token = 'abc';
    sandbox = sinon.sandbox.create();
    sandbox.stub(cloudConfig, 'urls').get(() => {
      return { collab: 'http://fake/collab/url' };
    });

    collabApiMock = nock(cloudConfig.urls.collab);
  });

  afterEach(() => {
    sandbox.restore();
    nock.cleanAll();
  });

  describe('getProjectVersions', () => {
    describe.skip('when the request succeeds', () => {
      it('should resolve with the value of project_versions property of the response', () => {
        const expectedResolve = {a: {b: ['a', 1, 'b']}};
        collabApiMock.get('/api/project_versions')
          .matchHeader('AUTHORIZATION', `Bearer ${token}`)
          .reply(200, { project_versions: expectedResolve });

        return expect(collabApiClient.getProjectVersions(token)).to.eventually.eql(expectedResolve);
      });
    });

    describe('when the request fails', () => {
      it('should resolve with an empty array', () => {
        collabApiMock.get('/api/project_versions')
          .matchHeader('AUTHORIZATION', `Bearer ${token}`)
          .reply(400);

        return expect(collabApiClient.getProjectVersions(token)).to.eventually.eql([]);
      });
    });
  });

});