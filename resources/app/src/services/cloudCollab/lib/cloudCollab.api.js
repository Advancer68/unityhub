// internal
const cloudConfig = require('../../cloudConfig/cloudConfig');

// third parties
const axios = require('axios');

const cloudCollabApiClient = {

  // GET /api/project_versions
  getProjectVersions(accessToken) {
    return axios.get(`${cloudConfig.urls.collab}/api/project_versions`, {
      headers: {
        AUTHORIZATION: `Bearer ${accessToken}`,
        'Content-Type': 'application/json'
      },
      responseType: 'json'
    })
      .then(response => response.data.project_versions)
      .catch(() => []);
  }
};

module.exports = cloudCollabApiClient;
