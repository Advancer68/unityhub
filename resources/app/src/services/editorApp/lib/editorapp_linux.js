const path = require('path');

class EditorAppLinux {

  // path to Unity executable for linux
  constructor(appOptions) {
    if (appOptions.editorPath === undefined) {
      throw new TypeError('missing mandatory editorPath options');
    } else {
      this.editorPath = appOptions.editorPath;
      this.exec = appOptions.editorPath; // on Linux, the executable is the application
      this.path = path.join(path.dirname(this.exec), '..');
      this.resources = path.resolve(this.path, 'Data', 'Resources');
    }
  }
}

module.exports = EditorAppLinux;
