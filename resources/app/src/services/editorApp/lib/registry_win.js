const registryKeys = {
  hkcu: 'HKEY_CURRENT_USER',
  hklm: 'HKEY_LOCAL_MACHINE',
  root: 'Software\\Unity Technologies',
  prefs5x: 'Software\\Unity Technologies\\Unity Editor 5.x',
  install: 'Software\\Unity Technologies\\Installer',
  uninstall: 'Software\\Wow6432Node\\Microsoft\\Windows\\CurrentVersion\\Uninstall',
  envar: 'Environment',
  sysEnVar: 'SYSTEM\\CurrentControlSet\\Control\\Session Manager\\Environment'
};

module.exports = {
  keys: registryKeys
};
