const path = require('path');

class EditorAppWin {

  // path to Unity.exe for windows
  constructor(appOptions) {
    if (appOptions.editorPath === undefined) {
      throw new TypeError('missing mandatory editorPath options');
    } else {
      this.editorPath = appOptions.editorPath;
      this.exec = appOptions.editorPath; // on Windows, the executable is the application
      this.path = path.join(path.dirname(this.exec), '..');
      this.resources = path.resolve(this.path, 'Data', 'Resources');
    }
  }
}

module.exports = EditorAppWin;
