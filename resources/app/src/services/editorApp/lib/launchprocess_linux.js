const networkSetup = require('../../../networkSetup');

class LaunchProcessLinux {
  getEnVars() {
    return Object.assign({}, networkSetup.originalEnvVars);
  }
}

module.exports = LaunchProcessLinux;
