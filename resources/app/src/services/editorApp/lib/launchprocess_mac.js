const networkSetup = require('../../../networkSetup');

class LaunchProcessMac {
  getEnVars() {
    return Object.assign({}, networkSetup.originalEnvVars);
  }
}

module.exports = LaunchProcessMac;
