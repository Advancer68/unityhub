const os = require('os');

const req = require;

const platform = os.platform();

function requireFile(filename) {
  let content;
  if (platform === 'win32') {
    content = req(`${filename}_win`);
  } else if (platform === 'darwin') {
    content = req(`${filename}_mac`);
  } else if (platform === 'linux') {
    content = req(`${filename}_linux`);
  } else {
    throw Error('Unsupported platform');
  }

  return content;
}

module.exports = {
  require: requireFile
};
