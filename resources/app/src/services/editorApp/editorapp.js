const _ = require('lodash');
const util = require('util');
const electron = require('electron');

const tools = require('./platformtools');
const launchProcess = require('./launchprocess');
const logger = require('../../logger')('EditorApp');
const settings = require('../localSettings/localSettings');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const licenseClient = require('../licenseService/licenseClientProxy');
const tokenManager = require('../../tokenManager/tokenManager');

const EditorAppPlatform = tools.require(`${__dirname}/lib/editorapp`);
let cloudEnvironment;

class EditorApp extends EditorAppPlatform {
  /**
   * Represents a unity editor application
   *
   * @param appOptions Object containing arguments
   */
  constructor(appOptions) {
    super(appOptions);

    this.version = appOptions.version;                // Editor version
    this.main = appOptions.main;                      // Main app (user selection)
  }

  static init() {
    logger.info('Init');
    cloudEnvironment = settings.get(settings.keys.CLOUD_ENVIRONMENT);
    EditorApp.editorArgs = null;
  }

  /**
   * Gives a user-friendly error from a process spawn error.
   */
  processError(error, args) {
    let errorCode = 'ERROR.LAUNCH_EDITOR.GENERIC';

    // Handle system-level error codes
    if (error && error.code) {
      switch (error.code) {
        case 'ENOTDIR':
        case 'EPERM':
        case 'ENOENT':
        case 'EACCES':
        case 'LICENSE':
          errorCode = `ERROR.LAUNCH_EDITOR.${error.code}`;
          break;
        default:
          errorCode = 'ERROR.LAUNCH_EDITOR.GENERIC';
      }
    }

    logger.warn(`Launch error app.editor: ${this.exec}, args: ${args}`, errorCode);

    return { errorCode, params: { i18n: { path: this.exec } }, error };
  }

  /**
   * Get optional arguments to start the editor
   * @param options
   * @param customArgs
   * @returns string[]
   */
  getOptionalArguments(options, customArgs) {
    options = options || {};
    customArgs = customArgs || [];

    const args = ['-useHub', '-hubIPC'];
    if (cloudEnvironment && customArgs.indexOf('-cloudEnvironment') <= -1) {
      args.push('-cloudEnvironment', cloudEnvironment);
    }

    if (options.buildPlatform !== undefined && options.buildPlatform.buildTarget !== undefined && customArgs.indexOf('-buildTarget') <= -1) {
      args.push('-buildTarget', options.buildPlatform.buildTarget);
      if (options.buildPlatform.buildTargetGroup !== undefined && customArgs.indexOf('-buildTargetGroup') <= -1) {
        args.push('-buildTargetGroup', options.buildPlatform.buildTargetGroup);
      }
    }

    if (Array.isArray(options.args)) {
      args.push(...options.args);
    }

    return args;
  }

  /**
   * Start this unity app
   * @param args Array arguments to be passed to process
   * @param onError Function called if the process returns an error post-launch (asynchronously).
   */
  start(args, onError) {
    onError = onError || _.noop;
    args.push('-hubSessionId', cloudAnalytics.common.session_guid);
    if (tokenManager.accessToken && tokenManager.accessToken.value) {
      args.push('-accessToken', tokenManager.accessToken.value);
    }
    if (EditorApp.editorArgs) {
      args = args.concat(EditorApp.editorArgs);
      EditorApp.editorArgs = null;
    }
    if (!licenseClient.isLicenseValid()) {
      return Promise.reject(this.processError({ code: 'LICENSE' }));
    }
    return launchProcess.start(this.exec, args)
      .then(process => {
        process.on('error', error => {
          logger.error(`Error launching the Editor: ${error}`);
          onError(error);

          // Until we can send messages to the UI, async errors like this will appear as a dialog
          electron.dialog.showMessageBox({
            type: 'error',
            title: 'Unity Launch Error',
            message: this.processError(error, args).message,
            buttons: []
          });
        });
        return process;
      })
      .catch(error => Promise.reject(this.processError(error, args)));
  }

  /**
   * Open a project
   */
  async openProject(projectPath, options, customArgs, onError) {
    if (customArgs && customArgs[projectPath] && customArgs[projectPath].cliArgs) {
      customArgs = customArgs[projectPath].cliArgs.split(/\n| /);
    } else {
      customArgs = [];
    }
    const optionalArgs = this.getOptionalArguments(options, customArgs);
    let args = ['-projectpath', projectPath];
    args = [...args, ...customArgs, ...optionalArgs];
    logger.debug(`opening the project with args ${util.inspect(args)}`);
    return this.start(args, onError);
  }

  /**
   * Create a project
   */
  async createProject(projectPath, selectedPackages, projectTemplate, organizationId, onError) {
    const templateKey = projectTemplate === 0 || projectTemplate === 1 ? 'projectTemplate' : 'cloneFromTemplate';
    const args = ['-createproject', projectPath, ...selectedPackages, `-${templateKey}`, projectTemplate, '-cloudOrganization', organizationId,
      '-cloudEnvironment', cloudEnvironment, '-useHub', '-hubIPC'];
      
    logger.info('createProject with args: ', args);
    return this.start(args, onError);
  }

  /**
   * Create a temporary project
   */
  createTempProject(selectedPackages, projectTemplate, onError) {
    const templateKey = projectTemplate === 0 || projectTemplate === 1 ? 'projectTemplate' : 'cloneFromTemplate';
    const args = ['-createproject', ...selectedPackages, `-${templateKey}`, projectTemplate, '-temporary',
      '-cloudEnvironment', cloudEnvironment, '-useHub', '-hubIPC', '-skipUpgradeDialogs'];
    return this.start(args, onError);
  }

  /**
   * Open a cloud project
   */
  openCloudProject(projectPath, projectId, organizationId, options, onError) {
    let args = ['-createProject', projectPath, '-cloudProject', projectId, '-cloudOrganization', organizationId];
    args = [...args, ...this.getOptionalArguments(options)];

    return this.start(args, onError);
  }
}

module.exports = EditorApp;
