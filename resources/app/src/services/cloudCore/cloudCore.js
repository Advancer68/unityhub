// third parties
const cloudCoreApiClient = require('./lib/cloudCore.api');
const _ = require('lodash');
const tokenManager = require('../../tokenManager/tokenManager');

const clientId = 'unity_editor';

class CloudCore {

  fetchUserInfo(accessToken) {
    return cloudCoreApiClient.getUser('me', accessToken).then(response => response.data);
  }

  loginWithAuthCodeAndRedirectUri(code, redirectUri) {
    return cloudCoreApiClient.postLoginWithAuthCodeAndRedirectUri(code, redirectUri).then(response => response.data);
  }

  refreshToken(refreshToken) {
    return cloudCoreApiClient.postRefreshAccessToken(refreshToken).then((response) => response.data);
  }

  getOrganizations() {
    return cloudCoreApiClient.getOrganizations(tokenManager.accessToken.value)
      .then((response) => response.data.orgs);
  }

  getAuthorizedUrl(uri) {
    return cloudCoreApiClient.postAuthorizedRedirect({
      access_token: tokenManager.accessToken.value,
      client_id: clientId,
      redirect_uri: uri
    });
  }

  getProjects() {
    return cloudCoreApiClient.getProjects(tokenManager.accessToken.value).then(response => response.data.projects);
  }

  getTeamsSeatOrganizations(userId) {
    return cloudCoreApiClient.getTeamsSeatOrganizations(tokenManager.accessToken.value, userId).then(
      (response) => _.groupBy(response.data.results, 'assignFrom'),
      () => null
    );
  }

}

module.exports = new CloudCore();
