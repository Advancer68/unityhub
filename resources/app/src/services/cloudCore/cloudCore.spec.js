// internal
const cloudCore = require('./cloudCore');
const cloudConfig = require('../cloudConfig/cloudConfig');
const cloudApiClient = require('./lib/cloudCore.api');
const settings = require('../localSettings/localSettings');

// third parties
const storage = require('electron-json-storage');
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');


chai.use(chaiAsPromised);
chai.use(sinonChai);

// HELPERS
function getFakeCredentials() {
  let credentials = {};
  ['access_token', 'refresh_token', 'expires_in'].forEach((fieldName) => {
    credentials[fieldName] = 'faked';
  });
  return { data: credentials };
}

// TEST SUITES
describe('CloudCore', () => {
  let sandbox;

  beforeEach(() => {
    sandbox = sinon.sandbox.create();

    // these avoid messing arround with the local storage
    sandbox.stub(storage, 'set');
    sandbox.stub(storage, 'remove');
  
    settingsMock = {
      [settings.keys.CLOUD_ENVIRONMENT]: settings.cloudEnvironments.DEV,
      [settings.keys.SERVICES_URL_INTERVAL]: 100
    };
    sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);
    sandbox.stub(cloudConfig, 'refreshConfigUrls').resolves();

  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('refreshToken', () => {

    it('should return a resolving promise with the request response when call to cloud core api succeeds', () => {
      sandbox.stub(cloudApiClient, 'postRefreshAccessToken').resolves(getFakeCredentials());

      return expect(cloudCore.refreshToken()).to.eventually.be.deep.equal(getFakeCredentials().data);
    });

    it('should return a rejecting promise with the request response when call to cloud core api fails', () => {
      sandbox.stub(cloudApiClient, 'postRefreshAccessToken').callsFake(() => {
        return Promise.reject('fail');
      });

      return expect(cloudCore.refreshToken()).to.eventually.be.rejectedWith('fail');
    });
  });

  describe('getOrganizations', () => {
    let getOrganizationsResponse;
    beforeEach(() => {
      getOrganizationsResponse = {
        orgs: [
          {foo: 'bar'}
        ]
      };

      sandbox.stub(cloudApiClient, 'getOrganizations').resolves({ data: getOrganizationsResponse });
    });

    it('should return organizations', () => {
      return expect(cloudCore.getOrganizations()).to.eventually.equal(getOrganizationsResponse.orgs);
    });
  });

  describe('getAuthorizedUrl', () => {
    it('should call cloudApiClient postAuthorizedRedirect method', () => {
      sandbox.stub(cloudApiClient, 'postAuthorizedRedirect').resolves();
      cloudCore.getAuthorizedUrl();
      expect(cloudApiClient.postAuthorizedRedirect).to.have.been.called;
    });

    it('should return a resolving promise when succeeds to get authorized url', () => {
      sandbox.stub(cloudApiClient, 'postAuthorizedRedirect').resolves();
      return expect(cloudCore.getAuthorizedUrl()).to.eventually.be.fullfilled;
    });

    it('should return a rejecting promise when fails to get authorized url', () => {
      sandbox.stub(cloudApiClient, 'postAuthorizedRedirect').rejects();
      return expect(cloudCore.getAuthorizedUrl()).to.eventually.be.rejected;
    });
  });

  describe('getProjects', () => {
    it('should call cloudApiClient getProjects method', () => {
      sandbox.stub(cloudApiClient, 'getProjects').resolves({ data: { projects: [] } });
      cloudCore.getProjects();
      expect(cloudApiClient.getProjects).to.have.been.called;
    });

    it('should resolve with a project list when succeeds to fetch projects list from cloud', () => {
      let projectList = [];
      sandbox.stub(cloudApiClient, 'getProjects').resolves({ data: { projects: projectList } });
      return expect(cloudCore.getProjects()).to.eventually.equal(projectList);
    });

    it('should return a rejecting promise when fails to fetch projects list from cloud ', () => {
      sandbox.stub(cloudApiClient, 'getProjects').rejects();
      return expect(cloudCore.getProjects()).to.eventually.be.rejected;
    });
  });

});