const outputService = require('../../outputService');
const { fs } = require('../../../fileSystem');
const path = require('path');

/*
  This is a temporary solution to provide help to the users.
  Ultimately, this should be removed and we should use yargs functionality for help
 */
class HelpController {

  constructor() {
    this.commands = ['help', 'h'];
  }

  handleArgs() {
    const content = fs.readFileSync(path.join(__dirname, '..', 'lib', 'help.txt'));
    outputService.logForCli(content);
  }

}

module.exports = HelpController;
