const editorManager = require('../../editorManager/editorManager');
const outputService = require('../../outputService');
const { hubFS } = require('../../../fileSystem');

class InstallPathController {

  constructor() {
    this.commands = ['install-path', 'ip'];
  }

  async handleArgs(args) {
    if (args.set && args.get) {
      throw new Error('Please provide either \'set\' or \'get\' arguments for the \'install-path\' command');
    }

    if (!args.set) {
      const installPath = editorManager.getSecondaryInstallLocation();
      outputService.logForCli(installPath);
      return;
    }

    if (!hubFS.isValidPath(args.set)) {
      throw new Error('This path is not valid.');
    }

    const isPathExists = await hubFS.pathExists(args.set);
    if (!isPathExists) {
      throw new Error('This directory does not exist.');
    }

    if (!editorManager.validateInstallPath(args.set)) {
      throw new Error('You cannot install Unity Editors in the hub directory.');
    }

    await editorManager.setSecondaryInstallLocation(args.set);
    outputService.logForCli(`All Unity Editors will be installed to ${args.set}`);
  }

}

module.exports = InstallPathController;
