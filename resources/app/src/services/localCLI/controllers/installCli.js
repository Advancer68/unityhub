const editorManager = require('../../editorManager/editorManager');
const unityReleaseService = require('../../editorManager/unityReleaseService');
const LocalCliHelper = require('../lib/localCliHelper');

class InstallController {

  constructor() {
    this.commands = ['install', 'i'];
  }
  
  async handleArgs(args) {

    if (!args.version) {
      throw new Error('Missing [-v|--version] argument.');
    }
    if (args.changeset) {
      await this.installVersionChangeset(args.version, args.changeset, args.module);
    } else {
      this.installVersion(args.version.toString(), args.module);
    }
  }

  async installVersionChangeset(version, changeset, modules = []) {
    await unityReleaseService.setCustomEditorInfo(version, changeset);
    const editor = unityReleaseService.getCustomEditorInfo();
    const components = LocalCliHelper.findModules(editor.modules, modules);
    editorManager.downloadUnityEditor(editor, components);
  }

  installVersion(version, modules = []) {
    const releases = editorManager.getReleases();
    let editor = releases.find(release => release.version.startsWith(version));
    if (!editor) { throw new Error('No editor version matched'); }
    editor = Object.assign({}, editor, { id: editor.version, progress: 0 });
    const components = LocalCliHelper.findModules(editor.modules, modules);
    editorManager.downloadUnityEditor(editor, components);
  }

}

module.exports = InstallController;
