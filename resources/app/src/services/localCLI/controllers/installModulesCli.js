const editorManager = require('../../editorManager/editorManager');
const LocalCliHelper = require('../lib/localCliHelper');
const inquirer = require('inquirer');

class InstallModulesController {

  constructor() {
    this.commands = ['install-modules', 'im'];
  }

  async handleArgs(args) {
    if (!args.version) {
      throw new Error('Missing [-v|--version] argument.');
    }

    if (!this._isVersionInstalled(args.version)) {
      throw new Error('This version of the Editor could not be found. Have you tried installing it using the hub?');
    }

    if (!args.module) {
      throw new Error('Missing [-m|--module] argument.');
    }

    const editor = editorManager.availableEditors[args.version];

    editor.id = args.version;
    editor.progress = 0;

    return this._askUserChildModules(editor.modules, [args.module], args.childModules)
      .then((cliModules) => {
        editorManager.downloadEditorModules(editor, LocalCliHelper.findModules(editor.modules, cliModules, true));
      });
  }

  _isVersionInstalled(version) {
    const availableEditors = editorManager.availableEditors;
    for (const key of Object.keys(availableEditors)) {
      if (key === version && !availableEditors[key].manual) {
        return true;
      }
    }
    return false;
  }

  _getChildrenModules(editorModules, cliModules) {
    const childrenModules = [];

    cliModules.forEach(moduleId => {
      const component = editorModules.find(module => module.id === moduleId);
      if (component) {
        const childComponents = editorModules.filter((module) => module.parent === component.id);
        if (childComponents.length > 0) {
          childrenModules.push(...childComponents);
        }
      }
    });
    return childrenModules;
  }

  async _askUserChildModules(editorModules, cliModules, installAllChildren) {
    const childModules = this._getChildrenModules(editorModules, cliModules);

    if (installAllChildren) {
      childModules.forEach(component => cliModules.push(component.id));
    } else {
      for (const component of childModules) {
        // eslint-disable-next-line no-await-in-loop
        await inquirer
          .prompt([
            {
              name: 'answer',
              type: 'confirm',
              message: `Would you also like to install the child module ${component.id} of the parent module ${component.parent}`,
              default: true,
            },
          ])
          .then(input => {
            if (input.answer) {
              cliModules.push(component.id);
            }
          });
      }
    }
    return cliModules;
  }
}

module.exports = InstallModulesController;
