const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

// Internal
const localCLI = require('./localCLI');

chai.use(sinonChai);
chai.use(chaiAsPromised);

class FakeController {
  constructor() {
    this.commands = ['test'];
  }

  handleArgs(arg) {
    return arg;
  }
}

describe('LocalCLI', () => {
  let sandbox;
  const fakeController = new FakeController();

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    sandbox.stub(fakeController, 'handleArgs');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('bindControllers', () => {
    it('should find the right commands list', () => {
      localCLI.bindControllers([fakeController]);
      localCLI.commands['test']();
      expect(fakeController.handleArgs).to.have.been.called;
    });
  });

  describe('handleArgs', () => {
    beforeEach(() => {
      localCLI.commands['test'] = fakeController.handleArgs.bind(fakeController);
    });

    it('should execute the right command', () => {
      localCLI.handleArgs({ 'headless': 'test' });
      expect(fakeController.handleArgs).to.have.been.called;
    });
    it('should throw the correct error', async () => {
      expect(localCLI.handleArgs({ 'headless': 'nothing' })).to.be.rejectedWith('\'nothing\' is not a Hub command.');
    });
  });
});