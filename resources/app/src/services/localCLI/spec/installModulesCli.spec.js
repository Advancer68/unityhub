const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const expect = require('chai').expect;
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const inquirer = require('inquirer')
chai.use(sinonChai);
chai.use(chaiAsPromised);

const InstallModulesCli = require('../controllers/installModulesCli');
const editorManager = require('../../editorManager/editorManager');
const LocalCliHelper = require('../lib/localCliHelper');

describe('InstallModulesCli', () => {
    let sandbox;
    let installModulesCli;

    beforeEach(() => {
        sandbox = sinon.sandbox.create();
        installModulesCli = new InstallModulesCli();
    });
    afterEach(() => {
        sandbox.restore();
    });

    describe('handleArgs', () => {
        const goodArgs = { version: 'someVersion', module: 'someModule' };
        const missingVerMsg = 'This version of the Editor could not be found. Have you tried installing it using the hub?';
        const availableEditors = {
            someVersion: {
                modules: ['mod1', 'mod2']
            },
            someManualVersion: {
                manual: true
            }
        };
        const availableEditorsWithParent = {
            someVersion: {
                modules: [{ id: 'someModule' },
                {
                    id: 'childModule',
                    parent: 'someModule'
                }]
            }
        };
        const finalModulesWithChild = {
            id: "someVersion",
            modules: [{ id: "someModule" }, { id: "childModule", parent: "someModule" }],
            progress: 0
        }

        beforeEach(() => {
            sandbox.stub(LocalCliHelper, 'findModules').callsFake(() => { return "someModules" });
            sandbox.stub(editorManager, 'downloadEditorModules');
        });

        it('should execute correctly', async () => {
            sandbox.stub(editorManager, 'availableEditors').value(availableEditors);
            await installModulesCli.handleArgs(goodArgs);
            expect(editorManager.downloadEditorModules).to.have.been.calledWith(availableEditors[goodArgs.version], "someModules");
        });
        it('should throw missing version error', () => {
            expect(installModulesCli.handleArgs({ module: 'someModule' })).to.be.rejectedWith('Missing [-v|--version] argument.')
        });
        it('should throw missing module error', () => {
            expect(installModulesCli.handleArgs({ version: 'someVersion' })).to.be.rejectedWith('Missing [-m|--module] argument.')
        });
        it('should throw an error when finding a manual editor', () => {
            expect(installModulesCli.handleArgs({ version: 'someManualVersion' })).to.be.rejectedWith(missingVerMsg)
        });
        it('should throw an error when unable to find the editor', () => {
            expect(installModulesCli.handleArgs({ version: 'someFakeVersion' })).to.be.rejectedWith(missingVerMsg)
        });
        it('should add the child module if responded with yes', async () => {
            sandbox.stub(inquirer, 'prompt').onCall(0).resolves(true);
            sandbox.stub(editorManager, 'availableEditors').value(availableEditorsWithParent);
            await installModulesCli.handleArgs(goodArgs);
            expect(editorManager.downloadEditorModules).to.have.been.calledWith(finalModulesWithChild);
        });
        it('should not add the child module if responded with no', async () => {
            sandbox.stub(inquirer, 'prompt').onCall(0).resolves(false);
            sandbox.stub(editorManager, 'availableEditors').value(availableEditors);
            await installModulesCli.handleArgs(goodArgs);
            expect(editorManager.downloadEditorModules).to.have.been.calledWith(availableEditors[goodArgs.version], "someModules");
        });
        it('if childModules flag is set to true, should skip prompt', async () => {
            sandbox.stub(editorManager, 'availableEditors').value(availableEditorsWithParent);
            await installModulesCli.handleArgs({ version: 'someVersion', module: 'someModule', childModules: true});
            expect(editorManager.downloadEditorModules).to.have.been.calledWith(finalModulesWithChild);
        });
    });
});