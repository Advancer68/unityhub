const path = require('path');
const requireAll = require('require-all');
const _ = require('lodash');

const controllersPath = path.join(__dirname, 'controllers');

class LocalCLI {

  constructor() {
    this.controllers = {};
    this.commands = {};
  }

  init() {
    requireAll({
      dirname: controllersPath,
      filter: /((?!spec)|.*)\.js$/,
      resolve: (ControllerClass) => {
        this.controllers[this._formatName(ControllerClass.name)] = new ControllerClass();
      }
    });
    this.bindControllers(this.controllers);
  }

  _formatName(className) {
    const trimmed = className.substr(0, className.lastIndexOf('Controller'));
    return _.lowerFirst(trimmed);
  }

  bindControllers(controllers) {
    _.each(controllers, controller => {
      _.each(controller.commands, command => {
        this.commands[command] = controller.handleArgs.bind(controller);
      });
    });
  }

  async handleArgs(cliArgs) {
    let goodInstruction = false;
    await Promise.all(_.map(Object.keys(this.commands), async command => {
      if (cliArgs.headless === command) {
        goodInstruction = true;
        await this.commands[command](cliArgs);
      }
    }));
    if (!goodInstruction) {
      throw new Error(`'${cliArgs.headless}' is not a Hub command.`);
    }
  }
}

module.exports = new LocalCLI();
