const DownloadsCluster = require('./downloadsCluster');
const downloader = require('../../../downloadManager/downloadManager');
const EventEmitter = require('events');
const path = require('path');
const promisify = require('es6-promisify');
const getRemoteSize = promisify(require('remote-file-size'));
const logger = require('../../../logger')('EditorDownloader');
const { fs, hubFS } = require('../../../fileSystem');
const settings = require('../../localSettings/localSettings');
const { HUB_DL_STATUS } = require('./constants');
const numeral = require('numeral');

class EditorDownloader extends EventEmitter {

  constructor(editor) {
    super();

    if (!editor || editor.id === undefined || editor.downloadUrl === undefined) {
      throw new TypeError('Invalid Editor parameters');
    }

    this.id = editor.id;
    this.checksum = editor.checksum;
    this.downloadDest = hubFS.getTmpDirPath();
    this.downloadsTotalSize = 0; // bytes
    this.totalProgress = 0; // percentage
    this.downloaded = 0; // bytes
    this.status = HUB_DL_STATUS.NOT_STARTED;
    this.progressInterval = null;
    this.ready = false;
  }

  static createEditorDownloader(editor, modules = []) {
    if (!editor || editor.id === undefined || editor.downloadUrl === undefined) {
      return Promise.reject(new TypeError('Invalid Editor parameters'));
    }

    const editorDownloader = new EditorDownloader(editor);
    return DownloadsCluster.createDownloadsCluster(`cluster-${editor.id}`, modules, editorDownloader.downloadDest)
      .then((dlCluster) => {
        editorDownloader.downloadsCluster = dlCluster;

        // setup editor download
        const dlFileName = hubFS.filenameFromUrl(editor.downloadUrl);
        if (dlFileName === undefined) {
          return Promise.reject(new TypeError('Invalid download url for the Editor'));
        }
        editorDownloader.editorDownload = downloader.download(editor.downloadUrl, path.join(editorDownloader.downloadDest, dlFileName));
        editorDownloader.editorDownload.finished = false;
        registerDownloadEvents(editorDownloader);
        editorDownloader.ready = true;

        return editorDownloader;
      });
  }

  destroy() {
    this.editorDownload.destroy();
    this.downloadsCluster.destroy();
    this.status = HUB_DL_STATUS.CANCELED;
    clearInterval(this.progressInterval);
    this.emit('destroy', this.id);
    logger.info(`Editor Download ${this.id}: Canceled`);
  }

  start() {
    if (!this.ready) {
      logger.warn(`Editor Downloader ${this.id}: not ready!`);
      return Promise.reject(`Editor Downloader ${this.id}: not ready!`);
    }

    logger.info(`Editor Downloader ${this.id}: starting`);
    return this.downloadsCluster.start()
      .then(() => getRemoteSize(this.editorDownload.url))
      .then((editorSize) => { this.downloadsTotalSize = this.downloadsCluster.downloadsTotalSize + editorSize; })
      .then(() => promisify(fs.mkdir)(this.downloadDest))
      .catch(e => { if (e.code !== 'EEXIST') throw e; })
      .then(() => {
        this.editorDownload.start();
        this.status = HUB_DL_STATUS.STARTED;
        startProgressMonitoring(this);
        this.emit('start', this.id);
        logger.info(`Editor Download ${this.id}: downloads starting, total size: ${numeral(this.downloadsTotalSize).format('0.00b')})`);
      })
      .catch(e => {
        this.downloadsCluster.destroy();
        logger.warn(`Editor Download ${this.id}: Start Error: ${e.message}`);
        throw e;
      });
  }

  getEditorInstallPath() {
    return this.editorDownload.filePath;
  }

  getDownloadedModules() {
    return this.downloadsCluster.getDownloadedModules();
  }

  getStats() {
    return { total: {
      completed: this.totalProgress * 100, // * 100 to comply with mt-files-downloader interface
      downloaded: this.downloaded
    } };
  }
}

// private

function registerDownloadEvents(editorDownloader) {
  editorDownloader.editorDownload.on('end', onEnd.bind(null, editorDownloader, editorDownloader.editorDownload));
  editorDownloader.downloadsCluster.on('end', onEnd.bind(null, editorDownloader, editorDownloader.downloadsCluster));

  editorDownloader.editorDownload.on('error', onEditorError.bind(null, editorDownloader));
  editorDownloader.downloadsCluster.on('error', onClusterError.bind(null, editorDownloader));
}

function startProgressMonitoring(editorDownloader) {
  if (editorDownloader.status === HUB_DL_STATUS.NOT_STARTED) {
    throw Error('Can\'t monitor progress on not started cluster');
  }
  const period = settings.get(settings.keys.DL_EDITOR_INTERVAL);
  editorDownloader.progressInterval = setInterval(() => {
    editorDownloader.downloaded = editorDownloader.editorDownload.getStats().total.downloaded + editorDownloader.downloadsCluster.getStats().total.downloaded;
    editorDownloader.totalProgress = editorDownloader.downloaded / editorDownloader.downloadsTotalSize;

    logger.debug(`Editor downloader ${editorDownloader.id} download progress: ${numeral(editorDownloader.totalProgress).format('0.00%')}`);
  }, period);
}

function onEnd(editorDownloader, download) {
  let downloadKind = 'cluster';

  if (download === editorDownloader.editorDownload) {
    downloadKind = 'editor';
    download.finished = true;
  }

  logger.info(`Editor Downloader ${editorDownloader.id}: end event for ${downloadKind}`);

  if (editorDownloader.downloadsCluster.status === HUB_DL_STATUS.FINISHED && editorDownloader.editorDownload.finished) {
    logger.info(`Editor Download ${editorDownloader.id}: Finished`);
    editorDownloader.status = HUB_DL_STATUS.FINISHED;
    clearInterval(editorDownloader.progressInterval);
    editorDownloader.emit('end', editorDownloader);
  }
}

function onEditorError(editorDownloader) {
  if (editorDownloader.status !== HUB_DL_STATUS.CANCELED) { // prevent fake errors to be handled (due to our version of mt-file-downloader)
    logger.warn(`Editor Downloader ${editorDownloader.id}: Failed to download the Editor, stopping all pending module downloads`);
    logger.warn(`Download error: ${editorDownloader.editorDownload.error}`);

    clearInterval(editorDownloader.progressInterval);
    editorDownloader.status = HUB_DL_STATUS.FAILED;
    editorDownloader.emit('error.editor', editorDownloader);
  }
}

function onClusterError(editorDownloader, download) {
  logger.warn(`Editor Downloader ${editorDownloader.id}: Cluster error, skipping ${download.moduleId} `);
  editorDownloader.downloadsTotalSize = editorDownloader.editorDownload.stats.total.size + editorDownloader.downloadsCluster.downloadsTotalSize;
}

module.exports = EditorDownloader;
