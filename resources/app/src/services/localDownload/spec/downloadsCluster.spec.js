const Downloader = require('../../../downloadManager/downloadManager');
const proxyquire = require('proxyquire');

const { HUB_DL_STATUS } = require('../lib/constants');
const settings = require('../../localSettings/localSettings');
const { getFakeDownload } = require('./helper');
const { fs } = require('../../../fileSystem');

// test
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(chaiAsPromised);
chai.use(sinonChai);

describe("DownloadsCluster", () => {
  const version = '2077.3.0f1',
    modules = [
      {id: 'm1', downloadUrl: 'www.unity3d.com/m1.exe'},
      {id: 'm2', downloadUrl: 'www.unity3d.com/m2.exe'},
      {id: 'm3', downloadUrl: 'www.unity3d.com/m3.exe'},
    ];
  let DownloadsCluster, cluster, sandbox, clock;

  function getSuccessDownloadCluster() {
    DownloadsCluster = proxyquire('../lib/downloadsCluster', {
      'remote-file-size': () => Promise.resolve(10)
    });
  }

  let remoteCount = 0;
  function getPartiallyValidDownloadCluster() {
    DownloadsCluster = proxyquire('../lib/downloadsCluster', {
      'remote-file-size': () => {
        remoteCount += 1;
        if (remoteCount % 2 === 0) {
          return Promise.reject();
        } else {
          return Promise.resolve(10);
        }
      }
    });
  }

  function getInvalidDownloadCluster() {
    DownloadsCluster = proxyquire('../lib/downloadsCluster', {
      'remote-file-size': () => Promise.reject()
    });
  }

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    sandbox.stub(Downloader, 'download').callsFake(getFakeDownload);
    sandbox.stub(fs, 'mkdir').callsFake((path, callback) => callback(undefined, path));

  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('constructor', () => {
    it('should throw a TypeError exception when editor param is invalid', () => {
      expect(() => new DownloadsCluster()).to.throw(TypeError);
    });
  });

  describe('createDownloadsCluster', () => {
    beforeEach(() => {
      getSuccessDownloadCluster();
    });

    it('should reject with a TypeError when no editor param are given', () => {
      return expect(DownloadsCluster.createDownloadsCluster()).to.be.rejectedWith(TypeError);
    });

    it('should reject with a TypeError when editor param is invalid', () => {
      return expect(DownloadsCluster.createDownloadsCluster('some id', 'another param')).to.be.rejectedWith(TypeError);
    });

    it('should reject with a TypeError when one module has invalid properties', () => {
      return expect(DownloadsCluster.createDownloadsCluster('id', [{id: '123', downloadUrl: 'invalid url'}])).to.be.rejectedWith(TypeError);
    });

    it('should reject with a TypeError when one module has missing properties', () => {
      return expect(DownloadsCluster.createDownloadsCluster('id', [{downloadUrl: 'www.hostname.com/file.exe'}])).to.be.rejectedWith(TypeError);
    });

  });

  describe('destroy', () => {
    beforeEach(() => {
      getSuccessDownloadCluster();
      return DownloadsCluster.createDownloadsCluster(version, modules)
        .then((clust) => {
          cluster = clust;
          sandbox.spy(cluster, 'emit');
          clock = sandbox.useFakeTimers();
        });
    });

    it('should set the status of DownloadsCluster instance to CANCELED', () => {
      cluster.destroy();
      expect(cluster.status).to.equal(HUB_DL_STATUS.CANCELED);
    });

    it('should emit a \'destroy\' event with its id', () => {
      cluster.destroy();
      expect(cluster.emit).to.have.been.calledWith('destroy', version);
    });

  });

  describe('start', () => {
    describe('when all urls are good', () => {
      beforeEach(() => {
        sandbox.stub(settings, 'get').returns(1000);
        getSuccessDownloadCluster();
        return DownloadsCluster.createDownloadsCluster(version, modules)
          .then((clust) => {
            cluster = clust;
            sandbox.spy(cluster, 'emit');
            clock = sandbox.useFakeTimers();
          });
      });

      it('should emit a \'start\' event with its id', () => {
        return cluster.start().then(() => {
          expect(cluster.emit).to.have.been.calledWith('start', version);
          expect(cluster.downloadsTotalSize).to.eq(30);
        })
      });

      it('should change the status of the DownloadsCluster instance to STARTED', () => {
        return cluster.start().then(() => {
          expect(cluster.status).to.equal(HUB_DL_STATUS.STARTED);
        });
      });

      it('should start to track download progress', () => {

        cluster.downloads.forEach((dl) => {
          dl.getStats.callsFake(() => {
            return {total: {downloaded: 1000}}
          });
        });

        return cluster.start().then(() => {
          expect(cluster.downloaded).equal(0);
          clock.tick(1000);
          expect(cluster.downloaded).equal(3000);
        });
      });
    });

    describe('when all urls are failing', () => {
      beforeEach(() => {
        sandbox.stub(settings, 'get').returns(1000);
        getInvalidDownloadCluster();
        return DownloadsCluster.createDownloadsCluster(version, modules)
          .then((clust) => {
            cluster = clust;
            sandbox.spy(cluster, 'emit');
            clock = sandbox.useFakeTimers();
          });
      });

      it('start should have 3 invalid downloads and emit \'end\' event', () => {
        return cluster.start()
          .then(() => {
            expect(cluster.emit).to.have.been.calledWith('end', version);
            expect(cluster.invalidDownloads.length).to.eq(3);
            expect(cluster.downloadsTotalSize).to.eq(0);
          })
          .catch((e) => expect('should not').to.eq(`come here error: ${e}`));
      });
    });

    describe('when some urls are failing', () => {
      beforeEach(() => {
        sandbox.stub(settings, 'get').returns(1000);
        getPartiallyValidDownloadCluster();
        return DownloadsCluster.createDownloadsCluster(version, modules)
          .then((clust) => {
            cluster = clust;
            sandbox.spy(cluster, 'emit');
            clock = sandbox.useFakeTimers();
          });
      });

      it('start should have 1 invalid download and emit \'start\' event', () => {
        return cluster.start()
          .then(() => {
            expect(cluster.emit).to.have.been.calledWith('start', version);
            expect(cluster.invalidDownloads.length).to.eq(1);
            expect(cluster.downloadsTotalSize).to.eq(20);
          })
          .catch((e) => expect('should not').to.eq(`come here error: ${e}`));
      });
    });
  });

  describe('getDownloadedModules', () => {
    beforeEach(() => {
      getSuccessDownloadCluster();
      return DownloadsCluster.createDownloadsCluster(version, modules)
        .then((clust) => {
          cluster = clust;
          sandbox.spy(cluster, 'emit');
          clock = sandbox.useFakeTimers();
        });
    });

    it('should return an array of [id, installerPath] tuple of each module', () => {
      expect(cluster.getDownloadedModules()).to.deep.equal(cluster.downloads.map((dl) => {
        return { id: dl.moduleId, installerPath: dl.filePath };
      }));
    });
  });

  describe('getStats', () => {
    beforeEach(() => {
      getSuccessDownloadCluster();
      return DownloadsCluster.createDownloadsCluster(version, modules)
        .then((clust) => {
          cluster = clust;
          sandbox.spy(cluster, 'emit');
          clock = sandbox.useFakeTimers();

          cluster.downloaded = 1000;
          cluster.totalProgress = 0.20;
        });
    });

    it('should returns the total of downloaded bytes', () => {
      expect(cluster.getStats().total.downloaded).equal(1000);
    });

    it('should returns percentage of download total progress', () => {
      expect(cluster.getStats().total.completed).equal(20);
    });
  });

});