'use strict';

const os = require('os');
const request = require('request');
const path = require('path');
const DownloadsCluster = require('./lib/downloadsCluster');
const EditorDownloader = require('./lib/editorDownloader');
const uuid = require('uuid/v1');
const logger = require('../../logger')('Download');
const _ = require('lodash');
const storage = require('electron-json-storage');
const downloader = require('../../downloadManager/downloadManager');
const promisify = require('es6-promisify');
const { editorHelper } = require('../../common/common');
const windowManager = require('../../windowManager/windowManager');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const outputService = require('../outputService');
const { fs, hubFS } = require('../../fileSystem');
const unityInstallState = require('../editorManager/unityInstallState');

const EventEmitter = require('events');
const md5File = require('md5-file/promise');

const downloadsData = {};
let downloadStorage = {};
const DOWNLOAD_STORAGE_KEY = 'downloads';

function clearDownload(downloadId) {
  clearInterval(downloadsData[downloadId].interval);
  storage.set('downloads', downloadStorage);
  delete downloadsData[downloadId];
}

function onStart(id, download) {
  download.on('start', () => {
    downloadsData[id].interval = setInterval(() => {
      if (!downloadsData[id]) return;
      const stats = download.getStats();
      outputService.notifyContent('download.progress', id, stats);
    }, 2000);
    windowManager.broadcastContent('download.start', id);
  });
}

function onError(id, download) {
  if (download.editorDownload !== undefined) {

    download.on('error.editor', () => {
      if (!downloadsData[id]) return;
      const uniqueDownloadId = downloadsData[id].uniqueDownloadId;
      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.EDITOR_DOWNLOAD_END,
        msg: {
          download_id: uniqueDownloadId,
          status: 'Error'
        },
      });
      logger.warn(`Editor downloader ${id} critical error, failed to download the Editor, warning the whole App`);
      logger.warn(`Error: ${download.editorDownload.error}`);
      this.cancelDownload(id, false);

      unityInstallState.installError(id);
      this.emit('download.error.editor', id, download);
      windowManager.broadcastContent('download.error.editor', 'ERROR.DOWNLOAD_FAIL', {
        id,
        i18n: { fileUrl: download.editorDownload.url } // this is required as error.message is non-enumerable (thus ignored by electron remote)
      });

      if (download.downloadsCluster) {
        download.downloadsCluster.on('error', (module) => {
          if (!downloadsData[id]) return;
          cloudAnalytics.addEvent({
            type: cloudAnalytics.eventTypes.COMPONENT_DOWNLOAD_END,
            msg: {
              download_id: uniqueDownloadId,
              component_id: module.moduleId,
              status: 'Error'
            },
          });
          windowManager.broadcastContent('download.error.cluster', 'ERROR.DOWNLOAD_FAIL', {
            i18n: { fileUrl: download.downloadsCluster.url },
            id
          });
        });
      }
    });
  } else {
    download.on('error', (dl) => {
      if (!downloadsData[id]) return;
      const uniqueDownloadId = downloadsData[id].uniqueDownloadId;

      logger.warn(`Download with download ${id}`);
      logger.warn(`Error: ${download.error}`);

      if (dl.moduleId === undefined) { // only cancel if it's not a cluster download
        this.cancelDownload(id, false);
        cloudAnalytics.addEvent({
          type: cloudAnalytics.eventTypes.TUTORIAL_DOWNLOAD_END,
          msg: {
            download_id: uniqueDownloadId,
            status: 'Error',

          },
        });
        windowManager.broadcastContent('download.error', 'ERROR.DOWNLOAD_FAIL', {
          i18n: { fileUrl: download.url },
          id
        });
      } else {
        cloudAnalytics.addEvent({
          type: cloudAnalytics.eventTypes.COMPONENT_DOWNLOAD_END,
          msg: {
            download_id: uniqueDownloadId,
            component_id: dl.moduleId,
            status: 'Error'
          },
        });
        unityInstallState.installModulesError(id);
        windowManager.broadcastContent('download.error.cluster', 'ERROR.DOWNLOAD_FAIL', {
          i18n: { fileUrl: download.url },
          id
        });
      }

    });
  }
}

/**
 * This method sends proper events for missing and successful component downloads.
 * It also removes the invalid ones from downloads array
 * @param invalidList
 * @param downloads
 * @param uniqueDownloadId
 */
function processInvalidList(invalidList, downloads, uniqueDownloadId) {
  for (let j = 0; j < downloads.length; j++) {
    if (invalidList.includes(downloads[j].moduleId)) {
      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.COMPONENT_DOWNLOAD_END,
        msg: {
          download_id: uniqueDownloadId,
          component_id: downloads[j].moduleId,
          status: 'ChecksumError'
        },
      });
      delete downloads[j];
    } else {
      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.COMPONENT_DOWNLOAD_END,
        msg: {
          download_id: uniqueDownloadId,
          component_id: downloads[j].moduleId,
          status: 'Success'
        },
      });
    }
  }
  _.omitBy(downloads, _.isNil);
}

function componentDownloadOnEnd(id, download, uniqueDownloadId) {
  validateDownload(download)
    .then((invalidList) => {
      processInvalidList(invalidList, download.downloads, uniqueDownloadId);
      if (invalidList.length === 0 && download.downloads.length <= 0) { // nothing was downloaded (urls returned 404)
        const componentsListing = _.map(download.invalidDownloads, 'name').join(', ');
        logger.info(`Components download ended with error. Download id: ${id}. ${componentsListing} could not be downloaded at the moment`);
        unityInstallState.installModulesError(id);
        windowManager.broadcastContent('download.error.cluster', 'ERROR.DOWNLOAD_COMPONENT_FAIL', {
          i18n: { componentsListing },
          id
        });
      } else if (invalidList.length === 0) { // all good and there was something to download
        logger.info(`Components download ended successfully. Download id: ${id}`);
        this.emit('download.end.cluster', id, download);
        windowManager.broadcastContent('download.end.cluster', id);
      } else if (invalidList.length < (download.downloads.length + download.invalidDownloads.length)) { // some good
        // remove the failed downloads from the list to install
        logger.info(`Components download ended with warning. Download id: ${id}. Some downloaded file(s) failed and were skipped : ${invalidList.join(', ')}`);
        windowManager.broadcastContent('download.warning.cluster', 'ERROR.DOWNLOADS_FAIL', {
          i18n: { filesListing: invalidList.join(', ') },
          id
        });
        this.emit('download.end.cluster', id, download);
      } else { // no good
        // all the required modules were bad
        const fileListing = invalidList.join(', ');
        logger.info(`Components download ended with error. Download id: ${id}. All downloaded file(s) were incomplete or corrupted : ${fileListing}`);
        unityInstallState.installModulesError(id);
        windowManager.broadcastContent('download.error.cluster', 'ERROR.DOWNLOADS_FAIL_ALL', {
          i18n: { fileListing },
          id
        });
      }
    });
}

function editorDownloadOnEnd(id, download, uniqueDownloadId) {
  validateDownload(download)
    .then((invalidList) => {
      if (invalidList.includes('unity')) {
        // editor fails => components fail
        cloudAnalytics.addEvent({
          type: cloudAnalytics.eventTypes.EDITOR_DOWNLOAD_END,
          msg: {
            download_id: uniqueDownloadId,
            status: 'ChecksumError'
          },
        });
        if (download.downloadsCluster) {
          for (let j = 0; j < download.downloadsCluster.downloads.length; j++) {
            cloudAnalytics.addEvent({
              type: cloudAnalytics.eventTypes.COMPONENT_DOWNLOAD_END,
              msg: {
                download_id: uniqueDownloadId,
                component_id: download.downloadsCluster.downloads[j].moduleId,
                status: (invalidList.includes(download.downloadsCluster.downloads[j].moduleId)) ? 'ChecksumError' : 'Success'
              },
            });
          }
        }
        unityInstallState.installError(id);
        this.emit('download.error.editor', id, download);
        windowManager.broadcastContent('download.error.editor', 'ERROR.INCOMPLETE_DOWNLOAD', { id });

      } else {
        cloudAnalytics.addEvent({
          type: cloudAnalytics.eventTypes.EDITOR_DOWNLOAD_END,
          msg: {
            download_id: id,
            status: 'Success'
          },
        });

        if (download.downloadsCluster) {
          processInvalidList(invalidList, download.downloadsCluster.downloads, uniqueDownloadId);
        }

        if (invalidList.length === 0 && download.downloadsCluster && download.downloadsCluster.downloads.length <= 0 && download.downloadsCluster.invalidDownloads.length > 0) { // nothing was downloaded (urls returned 404)
          const componentsListing = _.map(download.downloadsCluster.invalidDownloads, 'name').join(', ');
          logger.info(`Components download ended with error. Download id: ${id}. ${componentsListing} could not be downloaded at the moment`);
          this.emit('download.end.editor', id, download);
          windowManager.broadcastContent('download.error.cluster', 'ERROR.DOWNLOAD_COMPONENT_FAIL', {
            i18n: { componentsListing },
            id
          });
        } else if (invalidList.length === 0) { // all good
          logger.info(`Editor download ended successfully. Download id: ${id}`);
          this.emit('download.end.editor', id, download);
          windowManager.broadcastContent('download.end.editor', id);
        } else { // some components have failed to download
          const filesListing = invalidList.join(', ');
          logger.info(`Editor download ended with error. Download id: ${id}. Incomplete or corrupted download file: ${filesListing}`);
          windowManager.broadcastContent('download.error.cluster', 'ERROR.DOWNLOADS_FAIL_SOME', {
            i18n: { filesListing },
            id
          });
          this.emit('download.end.editor', id, download);
        }
      }
    });
}

function onEnd(id, download) {
  download.on('end', () => {
    if (!downloadsData[id]) return;
    const uniqueDownloadId = downloadsData[id].uniqueDownloadId;

    if (isComponentDownload(download)) {
      componentDownloadOnEnd.bind(this)(id, download, uniqueDownloadId);
      clearDownload(id);
    } else if (isEditorDownload(download)) {
      editorDownloadOnEnd.bind(this)(id, download, uniqueDownloadId);
      clearDownload(id);
    } else if (isTutorialDownload(download)) {
      logger.info(`Download ended successfully. Download id: ${id}`);
      fs.move(downloadsData[id].handle.filePath, download.destinationPath).then(() => {
        windowManager.broadcastContent('download.end', id, downloadsData[id].handle.destinationPath);
        cloudAnalytics.addEvent({
          type: cloudAnalytics.eventTypes.TUTORIAL_DOWNLOAD_END,
          msg: {
            download_id: id,
            status: 'Success'
          },
        });
      }).then(() => {
        clearDownload(id);
      });
    } else {
      clearDownload(id);
    }
  });
}

/**
 *
 * @param download
 * @returns Array an array containing the invalid moduleIds or []
 */
function validateDownload(download) {
  if (download.editorDownload) {
    return validateEditorDownload(download).then((invalidList) => {
      if (download.downloadsCluster) {
        return validateModuleDownload(download.downloadsCluster.downloads, download.downloadsCluster.invalidDownloads)
          .then((componentInvalidList) => Promise.resolve(invalidList.concat(componentInvalidList)));
      }
      return Promise.resolve(invalidList);
    });
  }

  if (download.downloads) {
    return validateModuleDownload(download.downloads, download.invalidDownloads);
  }

  return Promise.resolve([]);
}

function validateEditorDownload(download) {
  if (download.checksum) {
    return md5File(download.editorDownload.filePath)
      .then(hash => {
        if (hash !== download.checksum) {
          return Promise.resolve(['unity']);
        }
        return Promise.resolve([]);
      });
  }

  return Promise.resolve([]);
}

function validateModuleDownload(downloads, invalidDownloads) {
  const promiseList = [];
  for (let i = 0; i < downloads.length; i++) {
    if (downloads[i].checksum) {
      promiseList.push(md5File(downloads[i].filePath)
        .then(hash => {
          if (hash !== downloads[i].checksum) {
            logger.warn(`The checksum does not match for ${downloads[i].moduleId}`);
            return Promise.resolve(downloads[i].moduleId);
          }
          return Promise.resolve(true);
        }));
    } else {
      return Promise.resolve([]); // cannot validate
    }
  }

  return Promise.all(promiseList)
    .then((values) => {
      let invalidModuleIds = values.filter(item => item !== true);
      invalidModuleIds = invalidModuleIds.concat(_.map(invalidDownloads, 'id'));
      return invalidModuleIds;
    })
    .catch((e) => `Something went wrong while verifying the checksum ${e}`);
}

function setupDownload(id, download, uniqueDownloadId) {
  downloadsData[id] = {};

  downloadsData[id].handle = download;
  // An id the identify this download (multiple download of same thing have different uniqueDownloadId)
  downloadsData[id].uniqueDownloadId = uniqueDownloadId;
  onStart(id, download);
  onEnd.bind(this)(id, download);
  onError.bind(this)(id, download);
}

// Based on https://gist.github.com/bpedro/742162.
function createRecursivePath(basePath, directoryPath) {
  const segments = directoryPath.split(path.sep);
  let current = basePath;
  let i = 0;
  while (i < segments.length) {
    current = path.join(current, segments[i]);
    try {
      fs.statSync(current);
    } catch (e) {
      fs.mkdirSync(current);
    }
    i++;
  }
  return current;
}

function createDirectoryTree(packageInfo) {
  const pathArray = [editorHelper.getDefaultAssetStoreFolderName()];
  if (packageInfo.publisher) pathArray.push(packageInfo.publisher);
  if (packageInfo.category) pathArray.push(packageInfo.category);
  return createRecursivePath(editorHelper.getBaseUserDataPath(), path.join(editorHelper.getUnityFolderInUserData(), ...pathArray));
}

function isComponentDownload(download) {
  return download.downloads !== undefined;
}

function isEditorDownload(download) {
  return download.downloadsCluster !== undefined;
}

function isTutorialDownload(download) {
  return download.downloads === undefined && download.downloadsCluster === undefined;
}

class UnityDownload extends EventEmitter {

  init() {
    logger.info('Init');
    return promisify(storage.get)(DOWNLOAD_STORAGE_KEY).then((unfinishedDownloads) => {
      const obsoleteKeys = [];
      if (!unfinishedDownloads) return;

      downloadStorage = unfinishedDownloads;
      _.each(unfinishedDownloads, (storedDownload, id) => {
        try {
          const download = downloader.resumeDownload(storedDownload).start();
          setupDownload(id, download, uuid());
        } catch (error) {
          if (error && error.context && error.context.error && error.context.error.message && error.context.error.message.indexOf('Invalid file path') !== -1) {
            logger.warn(`Failed to resume download ${id}`);
            logger.warn(`${id} will be deleted from downloads storage keys`);
            obsoleteKeys.push(id);
          }
        }
      });

      obsoleteKeys.forEach((downloadId) => {
        delete unfinishedDownloads[downloadId];
      });
      promisify(storage.set)(DOWNLOAD_STORAGE_KEY, unfinishedDownloads);
    }).catch((error) => {
      logger.warn(`Could not fetch downloads in storage.: ${error}`);
    });
  }

  /**
   *
   * @param packageInfo
   * @param packageInfo.url,
   * @param packageInfo.publisher,
   * @param packageInfo.category,
   * @param packageInfo.filename,
   * @param packageInfo.id
   */
  async downloadPackage(packageInfo) {
    // create directory tree
    const packagePath = createDirectoryTree(packageInfo);
    const filename = `${packageInfo.filename}.unitypackage`;

    const destinationPath = path.join(packagePath, filename);
    const downloadPath = path.join(os.tmpdir(), filename);

    // check if there is enough space on disk
    try {
      const packageSize = await this._getPackageSize(packageInfo.url);
      const haveEnoughSpaceForDownload = await this._checkAvailableSpace(downloadPath, destinationPath, packageSize);
      if (!(haveEnoughSpaceForDownload)) {
        windowManager.broadcastContent('download.error', 'ERROR.NOT_ENOUGH_SPACE_SPECIFIC', {
          i18n: { itemName: packageInfo.title },
          id: packageInfo.id
        });
        return;
      }
    } catch (err) {
      logger.warn('failed to get package size. PackageInfo:', packageInfo, err);
      windowManager.broadcastContent('download.error', 'ERROR.CANNOT_GET_SIZE',
        {
          i18n: { name: packageInfo.title },
          id: packageInfo.id
        });
      return;
    }

    // start download
    const download = downloader.download(packageInfo.url, downloadPath);
    download.destinationPath = destinationPath;

    // add storage entry
    downloadStorage[packageInfo.id] = downloadPath;
    storage.set('downloads', downloadStorage);

    const uniqueDownloadId = uuid();
    setupDownload.bind(this)(packageInfo.id, download, uniqueDownloadId);
    cloudAnalytics.addEvent({
      type: cloudAnalytics.eventTypes.TUTORIAL_DOWNLOAD_START,
      msg: {
        tutorial_id: packageInfo.id,
        download_id: uniqueDownloadId,
        tutorial_category: packageInfo.category,
        tutorial_name: packageInfo.filename,
        url: packageInfo.url,
      },
    });
    download.start();
  }

  async _checkAvailableSpace(downloadPath, destinationPath, packageSize) {
    if (hubFS.getDiskRootPath(downloadPath) === hubFS.getDiskRootPath(destinationPath)) {
      const destDiskUsage = await hubFS.getDiskSpaceAvailable(destinationPath);
      return destDiskUsage.available > packageSize;
    }
    const [downloadDiskUsage, destDiskUsage] =
      await Promise.all([hubFS.getDiskSpaceAvailable(downloadPath), hubFS.getDiskSpaceAvailable(destinationPath)]);
    return destDiskUsage.available > packageSize && downloadDiskUsage.available > packageSize;
  }

  _getPackageSize(url) {
    return new Promise((resolve, reject) => {
      request.head(url)
        .on('response', (res) => {
          if (res.statusCode === 200) {
            resolve(res.headers['content-length']);
            return;
          }
          reject('invalid url');
        })
        .on('error', (e) => reject(e));
    });
  }

  downloadEditorModules(editor, modules, downloadDest = hubFS.getTmpDirPath()) {
    return DownloadsCluster.createDownloadsCluster(editor.id, modules, downloadDest)
      .then((downloadsCluster) => {
        const uniqueDownloadId = uuid();
        setupDownload.bind(this)(editor.id, downloadsCluster, uniqueDownloadId);

        modules.forEach((module) => {
          cloudAnalytics.addEvent({
            type: cloudAnalytics.eventTypes.COMPONENT_DOWNLOAD_START,
            msg: {
              editor_version: editor.id,
              download_id: uniqueDownloadId,
              component_id: module.id,
              url: module.downloadUrl
            },
          });
        });

        return downloadsCluster.start();
      })
      .catch((e) => {
        logger.warn('Something went wrong with downloadEditorModules', e);
        throw e;
      });
  }

  downloadUnityEditor(editor, modules, downloadDest = hubFS.getTmpDirPath()) {
    return EditorDownloader.createEditorDownloader(editor, modules, downloadDest)
      .then((editorDownloader) => {
        const uniqueDownloadId = uuid();
        setupDownload.bind(this)(editor.id, editorDownloader, uniqueDownloadId);
        cloudAnalytics.addEvent({
          type: cloudAnalytics.eventTypes.EDITOR_DOWNLOAD_START,
          msg: {
            editor_version: editor.id,
            download_id: uniqueDownloadId,
            selected_components: modules ? modules.map((module) => module.id) : [],
            url: editor.downloadUrl,
            release_type: editor.releaseType
          },
        });

        modules.forEach((module) => {
          cloudAnalytics.addEvent({
            type: cloudAnalytics.eventTypes.COMPONENT_DOWNLOAD_START,
            msg: {
              editor_version: editor.id,
              download_id: uniqueDownloadId,
              component_id: module.id,
              url: module.downloadUrl
            },
          });
        });

        return editorDownloader.start();
      })
      .catch((e) => {
        logger.warn('Something went wrong with downloadUnityEditor', e);
        throw e;
      });
  }

  copyUnityEditor(editor, modules, destination = hubFS.getTmpDirPath()) {
    const uniqueDownloadId = uuid();
    cloudAnalytics.addEvent({
      type: cloudAnalytics.eventTypes.EDITOR_DOWNLOAD_START,
      msg: {
        editor_version: editor.id,
        download_id: uniqueDownloadId,
        selected_components: modules ? modules.map((module) => module.id) : [],
        url: editor.downloadUrl,
        release_type: editor.releaseType
      },
    });

    modules.forEach((module) => {
      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.COMPONENT_DOWNLOAD_START,
        msg: {
          editor_version: editor.id,
          download_id: uniqueDownloadId,
          component_id: module.id,
          url: module.downloadUrl
        },
      });
    });

    const filename = path.basename(editor.downloadUrl);
    const editorDestinationFilePath = path.join(destination, filename);

    windowManager.broadcastContent('download.start', editor.id);
    const cpCount = 1 + modules.length;
    const cpCurrentDone = 1;
    return fs.copy(editor.downloadUrl, editorDestinationFilePath)
      .catch((e) => {
        logger.warn('Could not copy the Editor', e);
        windowManager.broadcastContent('download.error.editor', 'ERROR.COPY_EDITOR_FAIL', { id: editor.id });
        throw e;
      })
      .then(() => {
        windowManager.broadcastContent('download.progress', editor.id, { total: { completed: (cpCurrentDone / cpCount) * 100 } });

        return this.copyModules(editor, modules, destination, cpCount, cpCurrentDone)
          .then((modulesCopied) => [editorDestinationFilePath, modulesCopied]);
      });
  }

  copyEditorModules(editor, modules, destination = hubFS.getTmpDirPath()) {
    const uniqueDownloadId = uuid();

    modules.forEach((module) => {
      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.COMPONENT_DOWNLOAD_START,
        msg: {
          editor_version: editor.id,
          download_id: uniqueDownloadId,
          component_id: module.id,
          url: module.downloadUrl
        },
      });
    });

    windowManager.broadcastContent('download.start', editor.id);
    const cpCount = modules.length;
    const cpCurrentDone = 0;
    return this.copyModules(editor, modules, destination, cpCount, cpCurrentDone)
      .then((modulesCopied) => modulesCopied);
  }

  copyModules(editor, modules, destination, cpCount, cpCurrentDone) {
    const promiseList = [];
    const failedModules = [];
    modules.forEach((module) => {
      const filename = path.basename(module.downloadUrl);
      const destinationFilePath = path.join(destination, filename);
      promiseList.push(
        fs.copy(module.downloadUrl, destinationFilePath)
          .then(() => {
            module.installerPath = destinationFilePath;
            cpCurrentDone += 1;
            windowManager.broadcastContent('download.progress', editor.id, { total: { completed: (cpCurrentDone / cpCount) * 100 } });
          })
          .catch((e) => {
            logger.warn('Could not copy module', e);
            failedModules.push(module);
          })
      );
    });

    return Promise.all(promiseList)
      .then(() => {
        if (failedModules.length > 0) {
          if (failedModules.length === modules.length) {
            modules = [];
          } else {
            // get the difference
            modules = modules.filter(x => !failedModules.includes(x));
            windowManager.broadcastContent('download.warning.cluster', 'ERROR.COPY_FAIL_SOME', {
              i18n: { filesListing: _.map(failedModules, 'name').join(', ') },
              id: editor.id
            });
          }
        }
      })
      .then(() => modules);
  }

  cancelDownload(downloadId, userTriggered = true) {
    const download = downloadsData[downloadId];
    if (!download) return;
    const uniqueDownloadId = download.uniqueDownloadId;
    if (userTriggered === true) {

      if (isComponentDownload(download.handle)) {
        download.handle.downloads.forEach((module) => {
          cloudAnalytics.addEvent({
            type: cloudAnalytics.eventTypes.COMPONENT_DOWNLOAD_END,
            msg: {
              download_id: uniqueDownloadId,
              component_id: module.moduleId,
              status: 'Cancel'
            },
          });
        });
      } else if (isEditorDownload(download.handle)) {
        cloudAnalytics.addEvent({
          type: cloudAnalytics.eventTypes.EDITOR_DOWNLOAD_END,
          msg: {
            download_id: uniqueDownloadId,
            status: 'Cancel'
          },
        });

        download.handle.downloadsCluster.downloads.forEach((module) => {
          cloudAnalytics.addEvent({
            type: cloudAnalytics.eventTypes.COMPONENT_DOWNLOAD_END,
            msg: {
              download_id: uniqueDownloadId,
              component_id: module.moduleId,
              status: 'Cancel'
            },
          });
        });

      } else {
        cloudAnalytics.addEvent({
          type: cloudAnalytics.eventTypes.TUTORIAL_DOWNLOAD_END,
          msg: {
            download_id: uniqueDownloadId,
            status: 'Cancel'
          },
        });
      }
    }

    downloadsData[downloadId].handle.destroy();
    clearDownload(downloadId);
  }

  isInProgress(downloadId) {
    return downloadsData[downloadId] !== undefined;
  }

  getDownloadProgress(downloadId) {
    if (!downloadsData[downloadId] || !downloadsData[downloadId].handle) {
      return 0;
    }
    return downloadsData[downloadId].handle.getStats().total.completed;
  }
}

module.exports = new UnityDownload();
