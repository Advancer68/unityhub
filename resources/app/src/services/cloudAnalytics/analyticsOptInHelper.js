const axios = require('axios');
const postal = require('postal');
const cloudConfig = require('../cloudConfig/cloudConfig');
const logger = require('../../logger')('AnalyticsOptinHelper');

const OPT_IN_REQUEST_RETRY_INTERVAL = 5000;
const OPT_IN_REQUEST_RETRY_COUNT = 3;

class AnalyticsOptInHelper {
  init(disableAnalytics) {
    this._analyticsEnabled = !disableAnalytics;
    this.retryCount = 0;
  }

  userSignedOut() {
    // todo clear the cache
  }

  userSignedIn(cloudUserId) {
    // by default we assume the analytics is disabled until we overwrite it by cache or result from the server.
    this.requestForRemoteConfig(cloudUserId);

  }

  requestForRemoteConfig(cloudUserId) {
    if (this._analyticsEnabled === false) {
      return;
    }

    // todo if cache available for the same cloudUserId, send postal event with cache value
    this.retryCount = 0;
    this._sendRequest(cloudUserId);

  }

  get isAnalyticsEnabledOnThisMachine() {
    return this._analyticsEnabled;
  }

  async _sendRequest(cloudUserId) {
    const data = {
      common: {
        appid: 'hub',
        clouduserid: cloudUserId
      }
    };
    const config = { 'Content-Type': 'application/json' };
    
    try {
      const analyticsEndpoint = this._getAnalyticsEndpoint();
      const response = await axios.post(analyticsEndpoint, data, config);
      const analyticsEnabled = response.data.analytics.enabled;
  
      this._sendAnalyticsChangedEvent(cloudUserId, analyticsEnabled);
    } catch (error) {
      logger.warn(`Could not check analytics for user ${cloudUserId}: ${error.message}`);
      
      if (this.retryCount < OPT_IN_REQUEST_RETRY_COUNT) {
        this.retryCount++;
        await this._retryRequest(cloudUserId);
        
      } else {
        this._sendAnalyticsChangedEvent(cloudUserId, false);
      }
    }
  }
  
  _getAnalyticsEndpoint() {
    return cloudConfig.urls.analyticsOptOut || cloudConfig.defaultData.analyticsOptOut;
  }
  
  _retryRequest(cloudUserId) {
    return new Promise(resolve => {
      setTimeout((async () => resolve(await this._sendRequest(cloudUserId))), OPT_IN_REQUEST_RETRY_INTERVAL);
    });
  }
  
  _sendAnalyticsChangedEvent(cloudUserId, analyticsEnabled) {
    postal.publish({
      channel: 'app',
      topic: 'analyticsInfo.changed',
      data: {
        enabled: analyticsEnabled,
        cloudUserId
      }
    });
  }
}

module.exports = new AnalyticsOptInHelper();
