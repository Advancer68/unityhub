const settings = require('../localSettings/localSettings');
const postal = require('postal');
const analyticsOptInHelper = require('./analyticsOptInHelper');

const ANONYMOUS = 'anonymous'; // the placeholder for the cloudUserId when the user is signed out
const UNKNOWN = 'unknown'; // the placeholder for the cloudUserId while hub confirms if the user is signed in or not (at startup)

class AnalyticsQueue {

  /**
   * Initialized the queue and register to the events that matters
   */
  init() {

    // queue is an object, where each key represent the cloudUserId and the value is an object with the enabled flag and events list
    // Special keys: ANONYMOUS: when the user is signed out
    //               UNKNOWN: when the application is started, and we don't know the status of the user yet
    this.queue = {};
    this.currentUserId = UNKNOWN;

    // Analytics are enabled for the users who are signed out
    this._initializeTheUserQueue(ANONYMOUS, true);

    // Analytics are non-determined before the user's login is known
    this._initializeTheUserQueue(UNKNOWN, null);

    this.MAX_QUEUE_SIZE = settings.get(settings.keys.MAX_SIZE_FOR_ANALYTICS_QUEUE);
    this._registerEvents();
  }

  /**
   * register to the events that cause changes in the analytics.
   * @private
   */
  _registerEvents() {

    // subscribe to events that matters!
    postal.subscribe({
      channel: 'app',
      topic: 'userInfo.changed',
      callback: (data) => this._onUserInfoChanged(data.userInfo.userId)
    });

    postal.subscribe({
      channel: 'app',
      topic: 'analyticsInfo.changed',
      callback: (data) => this._onAnalyticsInfoChanged(data)
    });

  }

  /**
   * Callback when userInfo is changed
   * @param newUserId
   * @private
   */

  _onUserInfoChanged(newUserId) {
    if (newUserId === this.currentUserId) {
      return;
    }
    // user signed out
    if (newUserId === '') {
      // unknown --> signed out => move the unknown events to anonymous and call the userSignedOut
      if (this.currentUserId === UNKNOWN) {
        this.addChainOfEvents(this.queue[UNKNOWN].events, ANONYMOUS);
        this.dropOptOutEvents(UNKNOWN);
      }
      this.currentUserId = ANONYMOUS;
      analyticsOptInHelper.userSignedOut();
    } else {
      // user signed in
      this._initializeTheUserQueue(newUserId);

      // unknown --> signed in => move the unknown events to userId and call the userSignedIn
      if (this.currentUserId === UNKNOWN) {
        this.addChainOfEvents(this.queue[UNKNOWN].events, newUserId);
        this.dropOptOutEvents(UNKNOWN);
      }
      this.currentUserId = newUserId;
      analyticsOptInHelper.userSignedIn(this.currentUserId);
    }

  }

  /**
   * Callback when analytics is changed
   * @param data
   * @private
   */
  _onAnalyticsInfoChanged(data) {
    if (this.queue[data.cloudUserId]) {
      this.queue[data.cloudUserId].enabled = data.enabled;
    }
    if (data.enabled === false) {
      // remove the events for opted out user
      this.dropOptOutEvents(data.cloudUserId);

    }
  }

  /**
   * Adds a single event to a give userId.
   * @param event
   * @param cloudUserId
   */

  addEvent(event, cloudUserId = this.currentUserId) {
    if (!event.msg.ts) {
      event.msg.ts = new Date().getTime();
    }
    this._initializeTheUserQueue(cloudUserId);

    // add the event for the user
    this.queue[cloudUserId].events.push(event);

    this._dequeueExtraEvents(cloudUserId);

  }

  /**
   ** Adds an array of event to a give userId to the events array
   * @param events
   * @param cloudUserId
   * @param toEnd if true adds to the end of the array and if false to the beginning
   */
  addChainOfEvents(events, cloudUserId = this.currentUserId, toEnd = true) {
    this._initializeTheUserQueue(cloudUserId);
    if (toEnd) {
      this.queue[cloudUserId].events = this.queue[cloudUserId].events.concat(events);
    } else {
      this.queue[cloudUserId].events = events.concat(this.queue[cloudUserId].events);
    }
    this._dequeueExtraEvents(cloudUserId);
  }

  /**
   * Makes sure the number of unsent events for a given user does not exceed the max
   * @param cloudUserId
   * @private
   */

  _dequeueExtraEvents(cloudUserId) {
    const diff = this.queue[cloudUserId].events.length - this.MAX_QUEUE_SIZE;
    if (diff > 0) {
      this.queue[cloudUserId].events = this.queue[cloudUserId].events.splice(diff);
    }
  }

  /**
   * Removes events for the users who opt out
   */
  dropOptOutEvents(cloudUserId) {
    delete this.queue[cloudUserId];
  }

  /**
   * Returns an array of cloudUserIds for the users who opt in for analytics and have events to be sent
   * @returns {string[]}
   */
  getOptedInUsers() {
    return Object.keys(this.queue).filter(cloudUserId => this.queue[cloudUserId].enabled === true
      && this.queue[cloudUserId].events.length > 0);
  }

  /**
   * empty the events for the given user and returns the array (to be sent)
   * @param cloudUserId
   * @returns {*}
   */
  getEventsCloneForUser(cloudUserId) {
    const queueClone = this.queue[cloudUserId].events.slice(0);
    this.queue[cloudUserId].events = [];
    return queueClone;

  }

  /**
   * initialized the queue object for a given userId.
   * @param cloudUserId
   * @param enabled
   * @private
   */
  _initializeTheUserQueue(cloudUserId, enabled = null) {
    if (this.queue[cloudUserId]) {
      return;
    }
    this.queue[cloudUserId] = {
      events: [],
      enabled
    };

  }
}

module.exports = new AnalyticsQueue();
