// internal
const settings = require('../localSettings/localSettings');
const analyticsOptInHelper = require('./analyticsOptInHelper');
const analyticsQueue = require('./analyticsQueue');

// third parties
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const postal = require('postal');

chai.use(sinonChai);
chai.use(chaiAsPromised);

describe('AnalyticsQueue', () => {
  let sandbox;
  let postalSpy;
  let settingsMock;
  const ANONYMOUS = 'anonymous';
  const UNKNOWN = 'unknown';
  const someUserId = '123aadas';
  const someEvents = [1,2,3,4];

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    settingsMock = {
      [settings.keys.MAX_SIZE_FOR_ANALYTICS_QUEUE]: 2,
    };
    sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);

  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('init', () => {
    beforeEach(() => {
      sandbox.spy(analyticsQueue, '_initializeTheUserQueue');
      sandbox.spy(analyticsQueue, '_registerEvents');
      analyticsQueue.init();
    });

    it('should set the current user id to UNKNOWN', () => {
      expect(analyticsQueue.currentUserId).equals(UNKNOWN);
    });

    it('should initialize the Queue with ANONYMOUS user Id', () => {
      expect(analyticsQueue._initializeTheUserQueue).to.have.been.calledWith(ANONYMOUS, true);
    });

    it('should initialize the Queue with UNKNOWN user Id', () => {
      expect(analyticsQueue._initializeTheUserQueue).to.have.been.calledWith(UNKNOWN);
    });

    it('should initialize the MAX_QUEUE_SIZE', () => {
      expect(analyticsQueue.MAX_QUEUE_SIZE).equals(2);
    });

    it('should call the _registerEvents', () => {
      expect(analyticsQueue._registerEvents).to.have.been.called;
    });

  });

  describe('_registerEvents', () => {
    beforeEach(() => {
      postalSpy = sandbox.spy(postal, 'subscribe');
      analyticsQueue._registerEvents();
    });

    it('should subscribe to userInfo.changed', () => {
      expect(JSON.stringify(postalSpy.getCall(0).args[0])).to.equal(JSON.stringify({
        callback: function callback() {},
        channel: 'app',
        topic: 'userInfo.changed'
      }));
    });

    it('should subscribe to analyticsInfo.changed', () => {
      expect(JSON.stringify(postalSpy.getCall(1).args[0])).to.equal(JSON.stringify({
        callback: function callback() {},
        channel: 'app',
        topic: 'analyticsInfo.changed'
      }));
    });
  });

  describe('_onUserInfoChanged', () => {

    beforeEach(() => {
      sandbox.stub(analyticsOptInHelper, 'userSignedOut');
      sandbox.stub(analyticsOptInHelper, 'userSignedIn');
      sandbox.stub(analyticsQueue, 'addChainOfEvents');
      sandbox.stub(analyticsQueue, 'dropOptOutEvents');
      sandbox.stub(analyticsQueue, '_initializeTheUserQueue');
    });

    it('should do nothing if the new userId did not change', () => {
      analyticsQueue.currentUserId = someUserId;
      analyticsQueue._onUserInfoChanged(someUserId);
      expect(analyticsQueue.addChainOfEvents).not.have.been.called;
      expect(analyticsQueue.dropOptOutEvents).not.have.been.called;
      expect(analyticsOptInHelper.userSignedOut).not.have.been.called;
      expect(analyticsOptInHelper.userSignedIn).not.have.been.called;
      expect(analyticsQueue.currentUserId).equals(someUserId);

      expect(analyticsQueue._initializeTheUserQueue).not.have.been.called;
    });

    it('should call the sign out method in helper and set the user to anonymous if the new user id is empty', () => {
      analyticsQueue.currentUserId = someUserId;
      analyticsQueue._onUserInfoChanged('');
      expect(analyticsQueue.addChainOfEvents).not.have.been.called;
      expect(analyticsQueue.dropOptOutEvents).not.have.been.called;
      expect(analyticsOptInHelper.userSignedOut).have.been.called;
      expect(analyticsOptInHelper.userSignedIn).not.have.been.called;
      expect(analyticsQueue.currentUserId).equals(ANONYMOUS);
      expect(analyticsQueue._initializeTheUserQueue).not.have.been.called;
    });

    it('should add the unknown events to anonymous if the current user is unknown and the new user id is empty', () => {
      analyticsQueue.currentUserId = UNKNOWN;
      analyticsQueue.queue = {};
      analyticsQueue.queue[UNKNOWN] = {events: someEvents};

      analyticsQueue._onUserInfoChanged('');
      expect(analyticsQueue.addChainOfEvents).have.been.calledWith(someEvents, ANONYMOUS);
      expect(analyticsQueue.dropOptOutEvents).have.been.calledWith(UNKNOWN);
      expect(analyticsOptInHelper.userSignedOut).have.been.called;
      expect(analyticsOptInHelper.userSignedIn).not.have.been.called;
      expect(analyticsQueue.currentUserId).equals(ANONYMOUS);
      expect(analyticsQueue._initializeTheUserQueue).not.have.been.called;
    });

    it('should call call the sign in method in helper and initialize the user\'s queue if the new user id is not empty', () => {
      analyticsQueue.currentUserId = ANONYMOUS;
      analyticsQueue._onUserInfoChanged(someUserId);
      expect(analyticsQueue.addChainOfEvents).not.have.been.calledWith(someEvents, ANONYMOUS);
      expect(analyticsQueue.dropOptOutEvents).not.have.been.calledWith(UNKNOWN);
      expect(analyticsOptInHelper.userSignedOut).not.have.been.called;
      expect(analyticsOptInHelper.userSignedIn).have.been.called;
      expect(analyticsQueue.currentUserId).equals(someUserId);
      expect(analyticsQueue._initializeTheUserQueue).have.been.calledWith(someUserId);
    });

    it('should add the unknown events to new user id if the current user is unknown and the new user id is not empty', () => {
      analyticsQueue.currentUserId = UNKNOWN;
      analyticsQueue.queue = {};
      analyticsQueue.queue[UNKNOWN] = {events: someEvents};

      analyticsQueue._onUserInfoChanged(someUserId);
      expect(analyticsQueue.addChainOfEvents).have.been.calledWith(someEvents, someUserId);
      expect(analyticsQueue.dropOptOutEvents).have.been.calledWith(UNKNOWN);
      expect(analyticsOptInHelper.userSignedOut).not.have.been.called;
      expect(analyticsOptInHelper.userSignedIn).have.been.called;
      expect(analyticsQueue.currentUserId).equals(someUserId);
      expect(analyticsQueue._initializeTheUserQueue).have.been.calledWith(someUserId);
    });

  });

  describe('_onAnalyticsInfoChanged', () => {
    beforeEach(() => {
      sandbox.stub(analyticsQueue, 'dropOptOutEvents');
    });
    it('should set the enabled flag if the user id was in the queue', () => {
      analyticsQueue.queue = {};
      analyticsQueue.queue[someUserId] = {events: someEvents, enabled: null};
      analyticsQueue._onAnalyticsInfoChanged({
        cloudUserId: someUserId,
        enabled: true
      });
      expect(analyticsQueue.queue[someUserId].enabled).equal(true);
    });

    it('should remove the events if analytics was disabled', () => {
      analyticsQueue.queue = {};
      analyticsQueue.queue[someUserId] = {events: someEvents, enabled: null};
      analyticsQueue._onAnalyticsInfoChanged({
        cloudUserId: someUserId,
        enabled: false
      });
      expect(analyticsQueue.dropOptOutEvents).have.been.calledWith(someUserId);
    });

  });

  describe('addEvent', () => {
    beforeEach(() => {
      sandbox.spy(analyticsQueue, '_initializeTheUserQueue');
      sandbox.stub(analyticsQueue, '_dequeueExtraEvents');
    });

    it('should push the event to the queue', () => {
      const theEvent = {id: 5, msg:{}};
      analyticsQueue.queue = {};
      analyticsQueue.queue[someUserId] = {events: someEvents, enabled: null};
      analyticsQueue.addEvent(theEvent, someUserId);
      expect(analyticsQueue.queue[someUserId].events).includes(theEvent);
    });

    it('should call the _dequeueExtraEvents', () => {
      const theEvent = {id: 5, msg:{}};
      analyticsQueue.queue = {};
      analyticsQueue.queue[someUserId] = {events: someEvents, enabled: null};
      analyticsQueue.addEvent(theEvent, someUserId);
      expect(analyticsQueue._dequeueExtraEvents).to.have.been.calledWith(someUserId);
    });

    it('should use the currentUserId if no user id is passed', () => {
      const theEvent = {id: 5, msg:{}};
      analyticsQueue.currentUserId = someUserId;
      analyticsQueue.queue = {};
      analyticsQueue.queue[someUserId] = {events: someEvents, enabled: null};
      analyticsQueue.addEvent(theEvent);
      expect(analyticsQueue._dequeueExtraEvents).to.have.been.calledWith(someUserId);
    });

    it('should call _initializeTheUserQueue if it is the first event for the user id', () => {
      const theEvent = {id: 5, msg:{}};
      analyticsQueue.queue = {};
      analyticsQueue.addEvent(theEvent, someUserId);
      expect(analyticsQueue._initializeTheUserQueue).have.been.calledWith(someUserId);
    });
  });

  describe('addChainOfEvents', () => {
    beforeEach(() => {
      sandbox.spy(analyticsQueue, '_initializeTheUserQueue');
      sandbox.stub(analyticsQueue, '_dequeueExtraEvents');
    });

    it('should push the events to the end of the queue if toEnd flag is not passed', () => {
      const someOtherEvents = [6, 7, 8];
      const initialLength = someEvents.length;
      analyticsQueue.queue = {};
      analyticsQueue.queue[someUserId] = {events: someEvents, enabled: null};
      analyticsQueue.addChainOfEvents(someOtherEvents, someUserId);
      someOtherEvents.forEach((event, index) => {
        expect(analyticsQueue.queue[someUserId].events[initialLength + index]).equals(event);
      });
    });

    it('should call the _dequeueExtraEvents', () => {
      const someOtherEvents = [6, 7, 8];
      analyticsQueue.queue = {};
      analyticsQueue.queue[someUserId] = {events: someEvents, enabled: null};
      analyticsQueue.addChainOfEvents(someOtherEvents, someUserId);
      expect(analyticsQueue._dequeueExtraEvents).to.have.been.calledWith(someUserId);
    });

    it('should use the currentUserId if no user id is passed', () => {
      const someOtherEvents = [6, 7, 8];
      analyticsQueue.currentUserId = someUserId;
      analyticsQueue.queue = {};
      analyticsQueue.queue[someUserId] = {events: someEvents, enabled: null};
      analyticsQueue.addChainOfEvents(someOtherEvents);
      expect(analyticsQueue._dequeueExtraEvents).to.have.been.calledWith(someUserId);
    });

    it('should call _initializeTheUserQueue if it is the first event for the user id', () => {
      const someOtherEvents = [6, 7, 8];
      analyticsQueue.queue = {};
      analyticsQueue.addChainOfEvents(someOtherEvents, someUserId);
      expect(analyticsQueue._initializeTheUserQueue).have.been.calledWith(someUserId);
    });
  });

  describe('_dequeueExtraEvents', () => {
    beforeEach(() => {
      analyticsQueue.init();
    });
    it('should older events for the user if the number of events exceed the max', () => {
      analyticsQueue._initializeTheUserQueue(someUserId);
      analyticsQueue.queue[someUserId].events = [1, 2, 3];
      analyticsQueue._dequeueExtraEvents(someUserId);
      expect(analyticsQueue.queue[someUserId].events).eql([2, 3]);
    });

    it('should not touch the events if the number of events does not exceed the max', () => {
      analyticsQueue._initializeTheUserQueue(someUserId);
      analyticsQueue.queue[someUserId].events = [1];
      analyticsQueue._dequeueExtraEvents(someUserId);
      expect(analyticsQueue.queue[someUserId].events).eql([1]);
    });
  });

  describe('dropOptOutEvents', () => {
    beforeEach(() => {
      analyticsQueue.init();
    });
    it('should remove the given user id from the events queue', () => {
      analyticsQueue.dropOptOutEvents(UNKNOWN);
      expect(analyticsQueue.queue[UNKNOWN]).equals(undefined);
    });
  });

  describe('getOptedInUsers', () => {
    beforeEach(() => {
      analyticsQueue.init();
    });
    it('should return the userIds of the user who are opted in and have events', () => {
      analyticsQueue.addEvent({msg: {}}, ANONYMOUS);
      analyticsQueue.queue.optOutUser = {
          enabled: false,
          events: someEvents
        };
      analyticsQueue.queue.optInUserWithNoEvents = {
        enabled: true,
        events: []
      };
      analyticsQueue.queue.unknownOptInStatus = {
        enabled: null,
        events: someEvents
      };
      analyticsQueue.queue.optInUserWithSomeEvents = {
        enabled: true,
        events: someEvents
      };

      expect(analyticsQueue.getOptedInUsers()).eql([ANONYMOUS, 'optInUserWithSomeEvents']);
    });
  });

  describe('getEventsCloneForUser', () => {
    it('should remove the given user id from the events queue', () => {
      analyticsQueue.queue = {};
      analyticsQueue.queue[someUserId] = {events: someEvents, enabled: null};
      expect(analyticsQueue.getEventsCloneForUser(someUserId)).eql(someEvents);
      expect(analyticsQueue.queue[someUserId].events).eql([]);
    });
  });

  describe('_initializeTheUserQueue', () => {
    beforeEach(() => {
    });
    it('should create the queue object for the given user id', () => {
      analyticsQueue.queue = {};
      analyticsQueue._initializeTheUserQueue(someUserId, true);
      expect(analyticsQueue.queue[someUserId]).eql({events: [], enabled: true});
    });

    it('should set the enabled to null if not given', () => {
      analyticsQueue.queue = {};
      analyticsQueue._initializeTheUserQueue(someUserId);
      expect(analyticsQueue.queue[someUserId]).eql({events: [], enabled: null});
    });

    it('should not overwrite the object if the user id already exists', () => {
      analyticsQueue.queue = {};
      analyticsQueue.queue[someUserId] = {events: someEvents, enabled: null};
      analyticsQueue._initializeTheUserQueue(someUserId);
      expect(analyticsQueue.queue[someUserId]).eql({events: someEvents, enabled: null});
    });
  });
});
