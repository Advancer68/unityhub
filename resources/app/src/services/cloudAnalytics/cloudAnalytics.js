const axios = require('axios');
const os = require('os');
const uuid = require('uuid/v1');
const postal = require('postal');

const settings = require('../localSettings/localSettings');
const logger = require('../../logger')('CloudAnalytics');
const licenseCore = require('../licenseService/licenseCore');
const cloudConfig = require('../cloudConfig/cloudConfig');
const systemInfo = require('../localSystemInfo/systemInfo');
const analyticsOptInHelper = require('./analyticsOptInHelper');
const analyticsQueue = require('./analyticsQueue');
const localConfig = require('../localConfig/localConfig');

const isInTest = typeof global.it === 'function';

class CloudAnalytics {
  constructor() {
    const platform = {
      darwin: {
        name: 'kOSX',
        id: 0,
      },
      win32: {
        name: 'kWindows',
        id: 7,
      },
      linux: {
        name: 'kLinux',
        id: 16,
      },
    }[os.platform()];

    if (!platform) {
      logger.error(`OS is not supported: ${os.platform()}`);
      throw new Error('OS is not supported');
    }

    this.common = {
      appid: 'hub',
      deviceid: '',
      // Adding suffix to differentiate in analytics regular Hub and chinese Hub
      // For regular Hub: '-g'
      // For chinese Hub: '-c'
      // Please be mindful and add suffix corresponding to your usage
      hub_version: `${systemInfo.hubVersion()}-c`,
      machineid: '',
      platform: platform.name,
      platformid: platform.id,
      session_guid: uuid(),
    };

    this.quitTriggered = false;
    this.initialized = false;
    this.online = false;
    this.sendingInProgress = false;

    this.eventTypes = {
      START: 'hub.start.v1',
      ERROR: 'hub.error.v1',
      QUIT: 'hub.quit.v1',
      PAGE_VIEW: 'hub.pageview.v1',
      EXTERNAL_LINK: 'hub.externalLink.v1',
      WINDOW: 'hub.window.v1',
      OPEN_PROJECT: 'hub.projectOpen.v1',
      NEW_PROJECT: 'hub.projectNew.v2',
      TUTORIAL_DOWNLOAD_START: 'hub.tutorialDownloadStart.v1',
      TUTORIAL_DOWNLOAD_END: 'hub.tutorialDownloadEnd.v1',
      EDITOR_DOWNLOAD_START: 'hub.editorDownloadStart.v1',
      EDITOR_DOWNLOAD_END: 'hub.editorDownloadEnd.v1',
      COMPONENT_DOWNLOAD_START: 'hub.componentDownloadStart.v1',
      COMPONENT_DOWNLOAD_END: 'hub.componentDownloadEnd.v1',
      EDITOR_INSTALL_START: 'hub.editorInstallStart.v1',
      EDITOR_INSTALL_END: 'hub.editorInstallEnd.v1',
      COMPONENT_INSTALL_START: 'hub.componentInstallStart.v1',
      COMPONENT_INSTALL_END: 'hub.componentInstallEnd.v1',
      EDITOR_LOCATE: 'hub.editorLocate.v1',
      TIPS_CLICK: 'hub.tipsClick.v1',
      TIPS_HIDE: 'hub.tipsHide.v1',
      LICENSE_ACTIVATE: 'hub.licenseActivate.v1',
      UPDATE_DOWNLOADED: 'hub.updateDownloaded.v1',
      SURVEY: 'hub.survey.v1',
      TEMPLATE_DOWNLOAD: 'hub.templateDownload.v1',
      TEMPLATE_UPDATE: 'hub.templateUpgrade.v1',
      UPM_ERROR: 'hub.upmError.v1',
      UPDATE_CHANNEL_CHANGE: 'hub.releaseChannelUpdated.v1',
      BETA_PROMOTION: 'hub.betaPromotion.v1',
      AGE_GATE: 'hub.ageGate.v1',
    };
  }

  init(startTime, isHeadlessMode, disableAnalytics) {
    logger.info('Init');
    analyticsOptInHelper.init(disableAnalytics || localConfig.isAnalyticsDisabled());

    if (analyticsOptInHelper.isAnalyticsEnabledOnThisMachine === false) {
      return Promise.resolve();
    }
    this.endpoint = `${isInTest ? 'http://localhost:5000' : cloudConfig.urls['cdp-analytics']}/v1/events`;

    // filling missing common data
    this.common.machineid = licenseCore.getMachineId();

    // scheduler for sending events
    this.interval = setInterval(() => {
      this.sendEvents();
    }, settings.get(settings.keys.SEND_ANALYTICS_EVENTS_INTERVAL));

    this._registerEvents();

    // this flag is to avoid other parts of the code to add events before initialization.
    this.initialized = true;
    // initial event with the timestamp of start time
    this.addEvent({
      type: this.eventTypes.START,
      msg: {
        mode: isHeadlessMode ? 'CLI' : 'HUB UI',
        ts: startTime,
      },
    });
    return Promise.resolve();
  }

  /**
   * The generic add event function. All the events should be added directly or indirectly through this function.
   * @param event
   */
  addEvent(event) {
    if (this.initialized === false || analyticsOptInHelper.isAnalyticsEnabledOnThisMachine === false) {
      return;
    }
    analyticsQueue.addEvent(event);
  }

  /**
   * Sending a batch of events for each user who is opt in
   */
  async sendEvents() {
    if (this.online === false || this.sendingInProgress === true || analyticsOptInHelper.isAnalyticsEnabledOnThisMachine === false) {
      return;
    }

    this.sendingInProgress = true;

    // send the event for opted in users
    analyticsQueue.getOptedInUsers().forEach(async (userId) => {
      await this._sendEventsForUser(userId);
    });
    this.sendingInProgress = false;
  }

  /**
   * sends events for a given userId, and return a promise
   * @param userId
   * @returns
   * @private
   */
  _sendEventsForUser(userId) {
    const events = analyticsQueue.getEventsCloneForUser(userId);

    // some parts of the common data are dynamic
    const commonObject = Object.assign(this.common);
    commonObject.license_hash = licenseCore.getSerialHash();
    commonObject.cloud_user_id = userId;

    let data = `${JSON.stringify({ common: commonObject })}\n`;
    events.forEach((event) => {
      data += `${JSON.stringify(event)}\n`;
    });
    return axios
      .post(this.endpoint, data, {
        headers: {
          'Content-Type': 'application/json',
        },
        responseType: 'json',
      })
      .catch((err) => {
        logger.info(err);
        // add back to the beginning of the queue to be retried for the next round
        analyticsQueue.addChainOfEvents(events, userId, false);
      });
  }

  /**
   * A specific function just to add quit events.
   * @param source
   */
  quitEvent(source) {
    if (this.quitTriggered) {
      return Promise.resolve();
    }
    this.quitTriggered = true;
    this.addEvent({
      type: this.eventTypes.QUIT,
      msg: {
        source,
      },
    });
    return this.sendEvents();
  }

  /**
   * A specific function just to add window events
   * @param event
   * @param window
   * @param source
   */
  windowEvent(event, window, source) {
    this.addEvent({
      type: this.eventTypes.WINDOW,
      msg: {
        source,
        window,
        event,
      },
    });
  }

  /**
   * A specific function just to add error events
   * @param message
   * @param type
   */
  errorEvent(message, type) {
    this.addEvent({
      type: this.eventTypes.ERROR,
      msg: {
        error_message: message,
        error_type: type,
      },
    });
  }

  /**
   * subscribe to the events that cause changes in the analytics.
   * @private
   */
  _registerEvents() {
    postal.subscribe({
      channel: 'app',
      topic: 'connectInfo.changed',
      callback: (data) => {
        if (data.connectInfo.online === true && this.online === false) {
          this.online = data.connectInfo.online;
          this.sendEvents();
          return;
        }
        this.online = data.connectInfo.online;
      },
    });

    // Afshin: I commented out this listener, since we are not really using the error events and it can have some potential privacy
    // and scalability issues. Later, if we want to use the error data, we can enable this listener
    // postal.subscribe({
    //   channel: 'app',
    //   topic: 'error',
    //   callback: (data) => {
    //     this.errorEvent(util.inspect(data), 'exception');
    //   }
    // });
  }
}

module.exports = new CloudAnalytics();
