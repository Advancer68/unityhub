const { fs } = require('../../../fileSystem');
const path = require('path');
const logger = require('../../../logger')('PackageCrawler');

class PackageCrawler {
  async getPackages(rootPath) {
    const directories = await this._getDirectoriesInPath(rootPath);
    const packages = await this._getPackagesFromDirectories(directories);
    
    return Object.values(packages);
  }
  
  async _getDirectoriesInPath(rootPath) {
    const rootPathContents = await fs.readdir(rootPath);
    const directories = [];
    
    await Promise.all(rootPathContents.map(async (fileName) => {
      const filePath = path.join(rootPath, fileName);
      const fileStat = await fs.stat(filePath);
      
      if (fileStat.isDirectory()) {
        directories.push(filePath);
      }
    }));
    
    return directories;
  }
  
  async _getPackagesFromDirectories(directories) {
    const packages = {};
  
    await Promise.all(directories.map(async (directoryPath) => {
      const packageData = await this._getPackageJson(directoryPath);
      
      if (packageData === undefined) {
        return;
      }
  
      if (packages[packageData.name] !== undefined) {
        logger.warn(`Duplicate packages are not supported (${packageData.name}).`);
        return;
      }
  
      packages[packageData.name] = {
        packageId: packageData.name,
        version: packageData.version,
        resolvedPath: directoryPath
      };
    }));
  
    return packages;
  }
  
  async _getPackageJson(packagePath) {
    try {
      const packageData = await fs.readJson(path.join(packagePath, 'package.json'));
    
      if (packageData === undefined || (packageData.constructor === Object && Object.entries(packageData).length === 0)) {
        throw new Error('Package.json is empty');
      }
    
      if (packageData.name === undefined || packageData.version === undefined) {
        throw new Error('Package.json is missing essential information');
      }
      
      return packageData;
    
    } catch (error) {
      logger.warn(`package.json could not be read at path ${packagePath}`, error.message);
    }
    
    return undefined;
  }
}

module.exports = new PackageCrawler();
