'use strict'; // required by mocha

const electron = require('electron'); // eslint-disable-line
const path = require('path');
const { fs } = require('../../fileSystem');
const editorHelper = require('../../common/editor-helper');
const logger = require('../../logger')('PluginManager');
const windowManager = require('../../windowManager/windowManager');
const upm = require('@upm/core');
const ListStorage = require('../editorManager/lib/listStorage');
const promisify = require('es6-promisify');
const settings = require('../localSettings/localSettings');
// todo
//  https://jira.hq.unity3d.com/browse/HUB-2260
// Add this line back to package.json when Bokken/Yamato setup works with 'picomatch' dependency "chokidar": "^3.0.0",
// const chokidar = require('chokidar');
const chokidar = { watch: () => { throw new Error('Don\'t run this code!'); } }; // Mock dependency. See above.

const PLUGINS_KEY = 'hub.plugins';
const userDataFolder = editorHelper.getBaseUserDataPath();
const pluginFolder = path.join(userDataFolder, 'UnityHub', 'Plugins');

class PluginManager {
  constructor() {
    this.updateAvailable = false;
    this.resolveInProgress = false;
    this.pluginRegistry = {};
    this.packageListStorage = new ListStorage(PLUGINS_KEY, 'packageId');
    this.logger = logger;
    this.require = (module) => require(module); // eslint-disable-line
  }

  init() {
    this.updateInterval = settings.get(settings.keys.CHECK_FOR_UPDATE_INTERVAL);
    this.watcher = chokidar.watch(path.join(pluginFolder, 'manifest.json'), {
      persistent: true
    });
    // check for any change to the manifest.json file
    this.watcher.on('change', () => this.resolvePackages());
    // Start with just using the current plugin cache to activate existin plugins
    return Promise.resolve()
      .then(() => this.packageListStorage.init())
      .then(() => {
        // load existing plugin cache
        this.packages = this.packageListStorage.items;
      })
      .then(() => this.activatePlugins())
      .then(() => this.resolvePackages());
  }

  async resolvePackages(forceRefresh = false) {
    if (!forceRefresh && !this.resolveInProgress) {
      this.resolveInProgress = true;
      // check if upm thinks it is up-to-date
      const packages = await this._getPackages(pluginFolder);

      // check if our plugin folder is in sync with upm cache
      var a = JSON.stringify(Object.values(this.packages));
      var b = JSON.stringify(packages);

      if (a !== b) {
        this.logger.info(`Plugins have changed, need to restart Hub, packages: ${packages}`);
        this.packageListStorage.clear();

        if (packages !== undefined) {
          packages.forEach(pkg => {
            this.packageListStorage.addItem(pkg);
          });
        }

        this.packageListStorage.save();
        this.updateAvailable = true;
        windowManager.broadcastContent('plugins.resolved', packages);
      }

      this.resolveInProgress = false;
    }
  }

  async _getPackages(manifestFolder) {
    const manifest = {};
    const appPath = electron.app.getAppPath();
    const unityInfo = {
      unityVersion: '2018.3',
      editorPath: appPath,
      builtInPackagesPath: path.join(appPath, 'Packages')
    };

    const manifestPath = path.join(manifestFolder, 'manifest.json');

    if (!(await fs.pathExists(manifestPath))) {
      await fs.ensureDir(manifestFolder);
      await fs.writeJSON(manifestPath, manifest);
    }

    try {
      const resolution = await promisify(upm.resolve)(manifestFolder, unityInfo, 'global');
      return resolution.packages;

    } catch (err) {
      this.logger.error(err);
      throw err;
    }
  }

  async activatePlugins() {
    const promises = Object.keys(this.packages).map((key) => {
      const plugin = this.packages[key];
      return this._activatePlugin(plugin);
    });
    return Promise.all(promises);
  }

  async _activatePlugin(plugin) {
    const packageInfo = await fs.readJSON(path.join(plugin.resolvedPath, 'package.json'));
    this.pluginRegistry[packageInfo.name] = plugin;

    // only activate plugin if it has main property
    if ('main' in packageInfo) {
      try {
        const pluginEntryModule = path.join(plugin.resolvedPath, packageInfo.main);
        plugin.module = this.require(pluginEntryModule);
        plugin.activateFn = plugin.module.activate;
        plugin.activateFn();
        windowManager.broadcastContent('plugin.activated', plugin);
      } catch (exception) {
        this.logger.error(`Could not activate plugin ${packageInfo.name} due to exception: ${exception}`);
        // This is not fatal (just move on to the next plugin)
      }
    } else {
      this.logger.warn(`skipping plugin ${packageInfo.name} because of missing "main" field`);
    }
  }

  activateClientSidePlugin(pluginName, pluginPath) {
    windowManager.broadcastContent('plugin.activated', pluginPath);
  }

  restart() {
    electron.app.relaunch();
    electron.app.exit(0);
  }

  remindLater() {
    setTimeout(() => {
      if (this.updateAvailable === true) {
        windowManager.broadcastContent('plugins.resolved');
      }
    }, this.updateInterval);
  }
}

module.exports = new PluginManager();
