/* eslint-disable global-require */
'use strict'; // required by mocha

const electron = require('electron');
const path = require('path');
const editorHelper = require('../../common/editor-helper');
const logger = require('../../logger')('PluginManager');
const windowManager = require('../../windowManager/windowManager');
const ListStorage = require('../editorManager/lib/listStorage');
const settings = require('../localSettings/localSettings');
const packageCrawler = require('./lib/packageCrawler');
const { fs } = require('../../fileSystem');

const PLUGINS_KEY = 'hub.plugins';
const userDataFolder = editorHelper.getBaseUserDataPath();
const pluginFolder = path.join(userDataFolder, 'UnityHub', 'Plugins');

class PluginManagerLight {
  constructor() {
    this.updateAvailable = false;
    this.resolveInProgress = false;
    this.pluginRegistry = {};
    this.packageListStorage = new ListStorage(PLUGINS_KEY, 'packageId');
  }
  
  async init() {
    this.updateInterval = settings.get(settings.keys.CHECK_FOR_UPDATE_INTERVAL);
  
    await this.packageListStorage.init();
    this.packages = this.packageListStorage.items;
    
    await this.activatePlugins();
  
    // Resolve packages asynchronously so as not to block the init sequence.
    this.resolvePackages().catch((err) => {
      logger.debug('failed to resolve packages', err);
    });
  }
  
  async activatePlugins() {
    const pluginKeys = Object.keys(this.packages);
    await Promise.all(pluginKeys.map(async (pluginName) => {
      const plugin = this.packages[pluginName];
      await this._activatePlugin(plugin);
    }));
  }
  
  async _activatePlugin(plugin) {
    try {
      const packageInfo = await fs.readJSON(path.join(plugin.resolvedPath, 'package.json'));
      this.pluginRegistry[packageInfo.name] = plugin;
  
      if (packageInfo.main === undefined) {
        logger.warn(`skipping plugin ${packageInfo.name} because of missing "main" field`);
        return;
      }
  
      const pluginEntryModule = path.join(plugin.resolvedPath, packageInfo.main);
      plugin.module = require(pluginEntryModule);
      plugin.activateFn = plugin.module.activate;
      await plugin.activateFn();
      windowManager.broadcastContent('plugin.activated', plugin);
    } catch (exception) {
      logger.warn(`Could not activate plugin ${plugin.name} due to exception: ${exception}`);
      // This is not fatal (just move on to the next plugin)
    }
  }
  
  async resolvePackages() {
    if (this.resolveInProgress) {
      return;
    }
  
    this.resolveInProgress = true;
    const packages = await this._getPackages();
  
    if (this._hasPackagesChanged(packages)) {
      logger.info(`Plugins have changed, need to restart Hub, packages: ${packages}`);
      this._updatePackageList(packages);
    }
  
    this.resolveInProgress = false;
  }
  
  restart() {
    electron.app.relaunch();
    electron.app.exit(0);
  }
  
  remindLater() {
    setTimeout(() => {
      if (this.updateAvailable === true) {
        windowManager.broadcastContent('plugins.resolved');
      }
    }, this.updateInterval);
  }
  
  async _getPackages() {
    await fs.ensureDir(pluginFolder);
    return await packageCrawler.getPackages(pluginFolder);
  }
  
  _hasPackagesChanged(newPackages) {
    if (newPackages === undefined) {
      return true;
    }
    
    return newPackages.includes((packageInfo) => (
      this._isPackageNotInList(packageInfo) || this._isPackageDifferentVersion(packageInfo)
    ));
  }
  
  _isPackageNotInList(packageInfo) {
    return this.packages[packageInfo] === undefined;
  }
  
  _isPackageDifferentVersion(packageInfo) {
    return this.packages[packageInfo].version !== packageInfo.version;
  }
  
  _updatePackageList(newPackages) {
    this.packageListStorage.clear();
  
    if (newPackages !== undefined) {
      newPackages.forEach(pkg => {
        this.packageListStorage.addItem(pkg);
      });
    }
  
    this.packageListStorage.save();
    this.updateAvailable = true;
    windowManager.broadcastContent('plugins.resolved', newPackages);
  }
}

module.exports = new PluginManagerLight();
