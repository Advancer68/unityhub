const sinon = require('sinon');
const chai = require('chai');
const sinonChai = require('sinon-chai');
const expect = chai.expect;
chai.use(sinonChai);

const path = require('path');
const unityPackageManagerCacheService = require('./unityPackageManagerCache');

describe('unityPackageManagerCacheService', () => {
  let sandbox;

  before(() => {
    sandbox = sinon.sandbox.create();
  });

  beforeEach(() => {
    unityPackageManagerCacheService.hubCache = {};
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('setRemoteTemplates', () => {
    beforeEach(() => {
      sandbox.stub(unityPackageManagerCacheService, 'set');
    });

    it('should called set with key remoteTemplates', () => {
      unityPackageManagerCacheService.setRemoteTemplates([{name: 'remote1'}, {name: 'remote2'}]);
      expect(unityPackageManagerCacheService.set)
        .to.have.been.calledWith('remoteTemplates', [{name: 'remote1'}, {name: 'remote2'}], 5 * 6000);
    });
  });

  describe('getRemoteTemplates', () => {
    beforeEach(() => {
      sandbox.stub(unityPackageManagerCacheService, 'get');
    });

    it('should called set with key remoteTemplates', () => {
      unityPackageManagerCacheService.getRemoteTemplates('remoteTemplates');
      expect(unityPackageManagerCacheService.get).to.have.been.calledWith('remoteTemplates');
    });
  });

  describe('setTemplatesInfo', () => {
    beforeEach(() => {
      sandbox.stub(unityPackageManagerCacheService, 'set');
    });

    it('should called set with key setTemplatesInfo:templateName:editorInfo', () => {
      unityPackageManagerCacheService.setTemplatesInfo('com.unity.template.test', '1.0.2',
        {
          id: 'com.unity.template.test@1.0.2',
          name: 'com.unity.template.test',
          tar: path.join('somePath', 'com.unity.template.test-1.0.2.tgz'),
          version: '1.0.2',
          versions: { compatible: ['1.0.2']}
        });
      expect(unityPackageManagerCacheService.set)
        .to.have.been.calledWith(
          'templatesInfo:com.unity.template.test:1.0.2',
          {
            id: 'com.unity.template.test@1.0.2',
            name: 'com.unity.template.test',
            tar: path.join('somePath', 'com.unity.template.test-1.0.2.tgz'),
            version: '1.0.2',
            versions: { compatible: ['1.0.2']}
          },
          5 * 6000);
    });
  });

  describe('getTemplatesInfo', () => {
    beforeEach(() => {
      sandbox.stub(unityPackageManagerCacheService, 'get');
    });

    it('should called get with key setTemplatesInfo:templateName:editorInfo', () => {
      unityPackageManagerCacheService.getTemplatesInfo('com.unity.template.test', '1.0.2');
      expect(unityPackageManagerCacheService.get)
        .to.have.been.calledWith('templatesInfo:com.unity.template.test:1.0.2');
    });
  });

  describe('setUnityVersionBranch', () => {
    it('should set the cache to the expected state', () => {
      unityPackageManagerCacheService.setUnityVersionBranch('unityVersion', 'unityVersionBranch');

      expect(unityPackageManagerCacheService.hubCache).to.deep.equal({ 'unityVersionBranch:unityVersion' : { value: 'unityVersionBranch' } });
    });
  });

  describe('getUnityVersionBranch', () => {
    it('should get nothing from the cache', () => {
      const unityVersionBranch = unityPackageManagerCacheService.getUnityVersionBranch('unityVersion');

      expect(unityVersionBranch).to.equal(null);
    });

    it('should get the expected value from the cache', () => {
      unityPackageManagerCacheService.hubCache = { 'unityVersionBranch:unityVersion' : { value: 'unityVersionBranch' } };

      const unityVersionBranch = unityPackageManagerCacheService.getUnityVersionBranch('unityVersion');

      expect(unityVersionBranch).to.equal('unityVersionBranch');
    });
  });
});
