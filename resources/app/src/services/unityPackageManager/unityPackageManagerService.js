const upm = require('@upm/core');
const UnityVersion = require('@unityhub/unity-version');
const axios = require('axios');
const semver = require('semver');
const path = require('path');
const cloudConfig = require('../cloudConfig/cloudConfig');
const settings = require('../localSettings/localSettings');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const logger = require('../../logger')('UnityPackageManagerService');
const { fs, hubFS } = require('../../fileSystem');
const unityPackageManagerCacheService = require('./unityPackageManagerCache');

const UPM_TEMPLATE_SEARCH_QUERY = 'com.unity.template';
const PARTIAL_DOWNLOAD_SUFFIX = '.part';

class UnityPackageManagerService {
  async init() {
    const dynamicTemplatesConfigEndpoint = settings.get(settings.keys.DYNAMIC_TEMPLATES_CONFIG_PATH)
      || cloudConfig.getDefaultUrls().templatesConfig;
    logger.debug(`Dynamic templates config will be loaded from: ${dynamicTemplatesConfigEndpoint}`);

    try {
      const response = await axios.get(dynamicTemplatesConfigEndpoint);

      this.dynamicTemplatesConfig = response.data;
      logger.info(`Dynamic templates config loaded: ${this.dynamicTemplatesConfig}`);
    } catch (error) {
      this.dynamicTemplatesConfig = {};
      logger.error(`An error occurred while retrieving the dynamic templates config: ${error}`);
    }
  }

  async getDownloadedTemplatesForEditorVersion(destination, unityInfo) {
    const allEditorDependencies = await this._getAllEditorDependencies(destination);
    const editorDependencies = allEditorDependencies[unityInfo.unityVersion] ? allEditorDependencies[unityInfo.unityVersion].dependencies : { };
    const templateNamesAndVersions = Object.entries(editorDependencies);
    const editorCompatibleTemplateInfos = (await Promise.all(templateNamesAndVersions.map(async ([templateName, templateVersion]) => {
      try {
        const templateInfo = await this._getTemplateInfo(templateName, unityInfo, true);
        const downloadLocation = this._getTarDownloadLocation(destination, templateName, templateVersion);

        if (templateInfo && templateInfo.versions && templateInfo.versions.compatible && templateInfo.versions.compatible.includes(templateVersion)) {
          const isTemplateMissing = await this._isFileMissing(downloadLocation);

          if (!isTemplateMissing) {
            templateInfo.version = templateVersion;
            templateInfo.tar = downloadLocation;

            return templateInfo;
          }
        }

        logger.debug(`Upm returned a badly formatted information for the package ${templateName} with the response ${JSON.stringify(templateInfo)} `);

        return null;
      } catch (error) {
        // The error here is when the upm fails to fetch the package info (offline mode/template not compatible with editor version)
        // The error here is self explanatory (it's whether there's an invalid parameter (request error) or upm can't find info for that template with that specific editor)
        logger.debug(`Error while fetching package info for ${templateName} : ${error.message}`);
        return null;
      }
    }))).filter(templateInfo => templateInfo !== null);

    return editorCompatibleTemplateInfos;
  }

  async getDownloadableTemplates(unityInfo) {
    const remoteTemplates = await this._searchForRemoteTemplates(UPM_TEMPLATE_SEARCH_QUERY);
    const unityVersionBranch = this._getUnityVersionBranch(unityInfo.unityVersion);

    const editorCompatibleRemoteTemplateInfos = (await Promise.all(remoteTemplates.map(async (template) => {
      if (!this._isTemplateCompatibleWithEditor(template.name, unityVersionBranch)) {
        logger.debug(`Template is not supported by config: ${template.name}`);
        return null;
      }

      try {
        const templateInfo = await this._getTemplateInfo(template.name, unityInfo, false);
        return templateInfo;
      } catch (error) {
        logger.debug(`Downloadable template ${template.name} is not applicable for ${unityInfo.unityVersion} : ${error}`);
        return null;
      }
    }))).filter((templateInfo) => templateInfo !== null);

    return editorCompatibleRemoteTemplateInfos;
  }

  async _searchForRemoteTemplates(searchQuery) {
    const remoteTemplatesCache = unityPackageManagerCacheService.getRemoteTemplates();
    if (remoteTemplatesCache) {
      return remoteTemplatesCache;
    }

    try {
      const remoteTemplates = await this._searchUnderBothHosts(searchQuery);
      unityPackageManagerCacheService.setRemoteTemplates(remoteTemplates);
      return remoteTemplates;
    } catch (error) {
      const errorMessage = 'Error while searching for remote templates in upm';

      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.UPM_ERROR,
        msg: {
          error_message: errorMessage,
          error_type: 'upm_search'
        }
      });

      logger.error(`${errorMessage}: ${error.message}`);

      throw error;
    }
  }

  // This will search for both host:editor and host:hub until all dynamic templates have the correct host set
  // Any duplicates will be removed to prevent showing the same template twice
  // TODO: HUBARCH-2987 will change this to 'await upm.search('hub', undefined, { type: 'template', query: searchQuery });' only
  async _searchUnderBothHosts(searchQuery) {
    const upmHostEditorResults = (await upm.search('editor', undefined, { type: 'template', query: searchQuery })).packages;
    const upmHostHubResults = (await upm.search('hub', undefined, { type: 'template', query: searchQuery })).packages;

    const upmHostHubResultNames = upmHostHubResults.map(upmHostHubResult => upmHostHubResult.name);
    const filteredUPMHostEditorResults = upmHostEditorResults
      .filter(upmHostEditorResult => !upmHostHubResultNames.includes(upmHostEditorResult.name));

    return [...filteredUPMHostEditorResults, ...upmHostHubResults];
  }

  async downloadRemoteTemplate(templateName, destination, unityInfo) {
    try {
      const templateInfo = await this._getTemplateInfo(templateName, unityInfo, false);
      const downloadPath = this._getTarDownloadLocation(destination, templateInfo.name, templateInfo.version);
      const tempManifestLocation = path.join(destination, `temp-${templateName}`);

      await this._verifyAndCleanDownloadState(downloadPath, tempManifestLocation);

      const isTemplateMissing = await this._isFileMissing(downloadPath);
      if (isTemplateMissing) {
        await this._downloadTemplate(templateInfo, downloadPath);
        await this._resolveDependencies(templateInfo.name, downloadPath, unityInfo, tempManifestLocation);
      }

      await this._updateManifest(templateInfo.name, templateInfo.version, unityInfo.unityVersion, destination);

      templateInfo.tar = downloadPath;
      return templateInfo;
    } catch (error) {
      logger.error(`An error occurred when downloading ${templateName} from upm : ${error.message}`);
      throw error;
    }
  }

  async getUpgradableTemplates(localTemplates, unityInfo) {
    return (await Promise.all(localTemplates.map(async (template) => {
      try {
        const templateInfo = await this._getTemplateInfo(template.name, unityInfo, false);
        if (semver.gt(templateInfo.version.split('-')[0], template.version.split('-')[0])) {
          // Sets the tar to the old location of the template so it can still be launched
          templateInfo.tar = template.tar;
          return templateInfo;
        }
        return null;
      } catch (error) {
        logger.debug(`Upgradable template ${template.name} is not applicable for ${unityInfo.unityVersion} : ${error}`);
        return null;
      }
    }))).filter(templateInfo => templateInfo !== null);
  }

  async _verifyAndCleanDownloadState(downloadPath, tempManifestLocation) {
    const partialDownloadLocation = `${downloadPath}${PARTIAL_DOWNLOAD_SUFFIX}`;
    const isPartialDownloadMissing = await this._isFileMissing(partialDownloadLocation);
    if (!isPartialDownloadMissing) {
      logger.info(`Removing partially downloaded file ${partialDownloadLocation}`);
      await fs.remove(partialDownloadLocation);
    }

    const isTempUPMResolveManifestMissing = await this._isFileMissing(tempManifestLocation);
    if (!isTempUPMResolveManifestMissing) {
      logger.info(`Removing temp upm resolve manifest ${tempManifestLocation}`);
      await fs.remove(tempManifestLocation);
    }
  }

  _isFileMissing(downloadPath) {
    return new Promise((resolve) => {
      fs.access(downloadPath, fs.F_OK, (error) => {
        if (error) {
          logger.debug(`Template missing at ${downloadPath}`);
          return resolve(true);
        }

        logger.debug(`Template already found at ${downloadPath}`);
        return resolve(false);
      });
    });
  }

  async _downloadTemplate(templateInfo, downloadPath) {
    const hasEnoughSpaceOnDisk = await this._hasEnoughSpaceOnDisk(templateInfo.tarball.size, downloadPath);

    if (!hasEnoughSpaceOnDisk) {
      this._handleDownloadError('Insufficient space');
    }

    logger.info(`Downloading remote template from ${templateInfo.tarball.url} to location: ${downloadPath}`);

    try {
      await this._handleDownload(templateInfo, downloadPath);
    } catch (error) {
      this._handleDownloadError(`Error while downloading the template ${templateInfo.name}`);
    }

    return templateInfo;
  }

  async _hasEnoughSpaceOnDisk(templateSize, downloadPath) {
    try {
      const availableSpace = (await hubFS.getDiskSpaceAvailable(downloadPath)).available;
      return templateSize < availableSpace;
    } catch (error) {
      logger.error('Cannot access the available disk space');
      throw error;
    }
  }

  async _handleDownload(templateInfo, downloadPath) {
    const response = await axios.get(templateInfo.tarball.url, { responseType: 'stream' });
    const partialDownloadPath = `${downloadPath}${PARTIAL_DOWNLOAD_SUFFIX}`;

    await new Promise((resolve, reject) => {
      response.data.pipe(fs.createWriteStream(partialDownloadPath));

      response.data.on('end', () => {
        logger.debug(`Renaming downloaded file from ${partialDownloadPath} to ${downloadPath}`);
        fs.rename(partialDownloadPath, downloadPath, async (error) => {
          if (error) {
            await fs.remove(partialDownloadPath);
            logger.error(`Could not rename file ${partialDownloadPath} to ${downloadPath}`);
            reject(error);
          }

          logger.debug(`Template ${templateInfo.name} finished downloading`);
          resolve();
        });
      });

      response.data.on('error', () => {
        logger.debug(`Template ${templateInfo.name} could not be downloaded`);
        reject('File download error');
      });
    });
  }

  _handleDownloadError(errorMessage) {
    cloudAnalytics.addEvent({
      type: cloudAnalytics.eventTypes.ERROR,
      msg: {
        error_message: errorMessage,
        error_type: 'popup'
      }
    });

    logger.error(errorMessage);

    throw new Error(errorMessage);
  }

  async _resolveDependencies(templateName, downloadPath, unityInfo, tempManifestLocation) {
    const templateManifestLocation = path.join(tempManifestLocation, 'manifest.json');

    await fs.outputJson(templateManifestLocation, { dependencies: { [templateName]: `file:${downloadPath}` } });
    try {
      await upm.resolve(tempManifestLocation, unityInfo, { cachingLevel: 'global' });
    } catch (error) {
      // Clean up temp location
      await fs.remove(tempManifestLocation);

      const errorMessage = `Error while resolving dependencies of the downloaded template ${templateName}`;

      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.UPM_ERROR,
        msg: {
          error_message: errorMessage,
          error_type: 'upm_resolve'
        }
      });

      logger.error(`${errorMessage}: ${error.message}`);

      throw error;
    }
    await fs.remove(tempManifestLocation);
  }

  async _updateManifest(templateName, templateVersion, editorVersion, destination) {
    const allEditorDependencies = await this._getAllEditorDependencies(destination);

    // If no dependencies then this is the first template downloaded
    // Must setup the manifest for the editor version
    if (!allEditorDependencies[editorVersion]) {
      allEditorDependencies[editorVersion] = {
        dependencies: { }
      };
    }

    allEditorDependencies[editorVersion].dependencies[templateName] = templateVersion;

    await fs.outputJson(path.join(destination, 'manifest.json'), allEditorDependencies);
  }

  async _getAllEditorDependencies(destination) {
    const manifestLocation = path.join(destination, 'manifest.json');

    try {
      return await fs.readJson(manifestLocation);
    } catch (e) {
      const errorMessage = 'Manifest was corrupted, dependencies are unknown, forced to reset manifest.json';

      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.ERROR,
        msg: {
          error_message: errorMessage,
          error_type: 'exception'
        }
      });

      logger.error(`${errorMessage}: ${e}`);

      await fs.outputJson(manifestLocation, { });

      return { };
    }
  }

  _getTarDownloadLocation(destination, name, version) {
    return path.join(destination, `${name}-${version}.tgz`);
  }

  async _getTemplateInfo(templateName, unityInfo, offlineMode = false) {
    // If the request is made offline then bypass the cache, the request should already be fast enough
    if (!offlineMode) {
      const templateInfoCache = unityPackageManagerCacheService.getTemplatesInfo(templateName, unityInfo.unityVersion);
      if (templateInfoCache) {
        return templateInfoCache;
      }
    }

    try {
      const upmTemplateInfo = (await upm.getPackageInfo(templateName, unityInfo, { offlineMode })).package;
      const latestCompatibleVersion = this._getLatestCompatibleVersion(templateName, upmTemplateInfo.versions.compatible);
      const latestTemplateInfo = (await upm.getPackageInfo(latestCompatibleVersion, unityInfo, { offlineMode })).package;

      latestTemplateInfo.dynamicIcon = this._getDynamicIcon(templateName, this._getUnityVersionBranch(unityInfo.unityVersion));

      if (!offlineMode) {
        unityPackageManagerCacheService.setTemplatesInfo(templateName, unityInfo.unityVersion, latestTemplateInfo);
      }

      return latestTemplateInfo;
    } catch (e) {
      throw new Error(`Requested template ${templateName} could not be found by upm: ${e}`);
    }
  }

  // UPM will only return the latest none preview package
  // We must return the lastest package even if it is preview to support templates in production
  _getLatestCompatibleVersion(templateName, allCompatiableVersions) {
    const latestCompatibleVersion = allCompatiableVersions.slice().pop();
    return `${templateName}@${latestCompatibleVersion}`;
  }

  _isTemplateCompatibleWithEditor(templateName, unityVersionBranch) {
    return this.dynamicTemplatesConfig[unityVersionBranch] && !!this.dynamicTemplatesConfig[unityVersionBranch][templateName];
  }

  _getDynamicIcon(templateName, unityVersionBranch) {
    return this.dynamicTemplatesConfig[unityVersionBranch]
      && this.dynamicTemplatesConfig[unityVersionBranch][templateName]
      && this.dynamicTemplatesConfig[unityVersionBranch][templateName].icon;
  }

  _getUnityVersionBranch(unityVersion) {
    const unityVersionBranchFromCache = unityPackageManagerCacheService.getUnityVersionBranch(unityVersion);
    if (unityVersionBranchFromCache) {
      return unityVersionBranchFromCache;
    }

    try {
      const unityVersionBranch = (new UnityVersion(unityVersion)).branch;
      unityPackageManagerCacheService.setUnityVersionBranch(unityVersion, unityVersionBranch);
      return unityVersionBranch;
    } catch (error) {
      logger.info(`Unity version not supported by the current configuration: ${unityVersion}`);
      return '';
    }
  }
}

module.exports = new UnityPackageManagerService();
