const upm = require('@upm/core');
const axios = require('axios');
const path = require('path');

const chai = require('chai');
const sinon = require('sinon');
const proxyquire = require('proxyquire');
const expect = chai.expect;

const { fs } = require('../../fileSystem');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const logger = require('../../logger')('UnityPackageManagerService');
const unityPackageManagerCacheService = require('./unityPackageManagerCache');
const unityPackageManagerService = require('./unityPackageManagerService');

chai.use(require('sinon-chai')).use(require('chai-as-promised'));

describe('unityPackageManagerService', () => {
  let sandbox;
  let upmPackages;
  let packageInfo;
  let randomNewPackage;
  let randomOldPackage;
  let randomPackageInfo;
  let upgradablePackage;
  let localTemplates;

  before(() => {
    sandbox = sinon.sandbox.create();
  });

  beforeEach(() => {
    sandbox.stub(logger, 'warn');
    sandbox.stub(cloudAnalytics, 'addEvent');
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('init', () => {
    let axiosStub;
    let settingsReturn;
    let cloudConfigReturn;
    let upmService;

    beforeEach(() => {
      settingsReturn = null;
      cloudConfigReturn = null;

      axiosStub = sandbox.stub();
      axiosStub
        .withArgs('https://some-url.com/dynamicTemplatesConfig.json').resolves({ data: { some: 'config' } })
        .withArgs('https://another-url.com/dynamicTemplatesConfig.json').resolves({ data: { some: 'config' } })
        .withArgs('https://error-endpoint.com/dynamicTemplatesConfig.json').rejects('some error');

      upmService = proxyquire('./unityPackageManagerService', {
        'axios': {
          get: axiosStub
        },
        '../localSettings/localSettings': {
          get: sandbox.stub().callsFake(() => settingsReturn)
        },
        '../cloudConfig/cloudConfig': {
          getDefaultUrls: sandbox.stub().callsFake(() => cloudConfigReturn)
        },
      });
    });

    it('should get the dynamic templates config from url set in local settings', async () => {
      settingsReturn = 'https://some-url.com/dynamicTemplatesConfig.json';

      await upmService.init();

      expect(axiosStub).to.have.been.calledWithExactly('https://some-url.com/dynamicTemplatesConfig.json');
    });

    it('should get the dynamic templates config from the expected url', async () => {
      cloudConfigReturn = { templatesConfig: 'https://another-url.com/dynamicTemplatesConfig.json' };

      await upmService.init();

      expect(axiosStub).to.have.been.calledWithExactly('https://another-url.com/dynamicTemplatesConfig.json');
    });

    it('should set the dynamic templates config', async () => {
      settingsReturn = 'https://some-url.com/dynamicTemplatesConfig.json';

      await upmService.init();

      expect(upmService.dynamicTemplatesConfig).to.deep.equal({ some: 'config' });
    });

    it('should handle axios error and set the dynamic templates config as empty', async () => {
      settingsReturn = 'https://error-endpoint.com/dynamicTemplatesConfig.json';

      await upmService.init();

      expect(upmService.dynamicTemplatesConfig).to.deep.equal({});
    });
  });

  describe('getDownloadedTemplatesForEditorVersion', () => {
    let fsFileTestTemplateMissingError = false;
    let fsOutputJsonStub;

    beforeEach(() => {
      unityPackageManagerService.dynamicTemplatesConfig = {
        '2019.2': {
          'com.unity.template.test': {
            icon: 'dynamic icon',
          },
          'com.unity.template.random': {
            icon: 'dynamic icon',
          },
        },
        '2019.4': {
          'com.unity.template.upgradable': {
            icon: 'dynamic icon',
          },
        },
      }

      packageInfo = { package:[] };
      randomNewPackage = { package:[] };
      randomOldPackage = { package:[] };
      randomNewPackage.package = { id: 'com.unity.template.random@1.3.0', name: 'com.unity.template.random', version: '1.3.0', versions: { compatible: ['1.3.0'] }};
      packageInfo.package = { id: 'com.unity.template.test@1.0.2', name: 'com.unity.template.test', version: '1.0.2', versions: { compatible: ['1.0.2']} };
      randomOldPackage.package = { id: 'com.unity.template.random@1.3.0', name: 'com.unity.template.random', version: '1.3.0', versions: { compatible: [] }};
      upgradablePackage = {
        package: {
          id: 'com.unity.template.upgradable@1.4.1',
          name: 'com.unity.template.upgradable',
          version: '1.4.1',
          versions: { compatible: ['1.4.0', '1.4.1'] }
        }
      }

      fsOutputJsonStub = sandbox.stub(fs, 'outputJson');

      sandbox.stub(fs, 'access').callsFake((filePath, mode, cb) => {
        switch (filePath) {
          case path.join('somePath', 'com.unity.template.test-1.0.2.tgz'):
            return cb(fsFileTestTemplateMissingError);
          default:
            return cb(false);
        }
      });
      sandbox.stub(fs, 'readJson')
        .withArgs(path.join('somePath', 'manifest.json')).returns({
          '2019.4.0f1': {
            dependencies: {
              'com.unity.template.upgradable': '1.4.0',
            }
          },
          '2019.2.0f1': {
            dependencies: {
              'com.unity.template.test': '1.0.2',
              'com.unity.template.random': '1.3.0'
            }
          },
          '2018.4.0f1': {
            dependencies: {
              'com.unity.template.test': '1.0.2'
            }
          }
        })
        .withArgs(path.join('brokenManifest', 'manifest.json')).throws(new Error('manifest.json is corrupted'));
      sandbox.stub(upm, 'getPackageInfo')
        .withArgs('com.unity.template.test', {"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}).returns(packageInfo)
        .withArgs('com.unity.template.test@1.0.2', {"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}).returns(packageInfo)
        .withArgs('com.unity.template.test', {"unityVersion": '2018.4.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}).returns(packageInfo)
        .withArgs('com.unity.template.random', {"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}).returns(randomNewPackage)
        .withArgs('com.unity.template.random@1.3.0', {"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}).returns(randomNewPackage)
        .withArgs('com.unity.template.random', {"unityVersion": '2018.4.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}).returns(randomOldPackage)
        .withArgs('com.unity.template.upgradable', {"unityVersion": '2019.4.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}).returns(upgradablePackage)
        .withArgs('com.unity.template.upgradable@1.4.1', {"unityVersion": '2019.4.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}).returns(upgradablePackage);
    });

    it('should return all the downloaded packages for the specific editor', async () => {
      const packages = await unityPackageManagerService.getDownloadedTemplatesForEditorVersion(
        'somePath',
        {
          unityVersion: '2019.2.0f1',
          editorPath: 'somePath',
          builtInPackagesPath: 'anotherPath'
        }
      );

      expect(packages).to.deep.equal([{
        dynamicIcon: 'dynamic icon',
        id: 'com.unity.template.test@1.0.2',
        name: 'com.unity.template.test',
        tar: path.join('somePath', 'com.unity.template.test-1.0.2.tgz'),
        version: '1.0.2',
        versions: { compatible: ['1.0.2'] }
      }, {
        dynamicIcon: 'dynamic icon',
        id: 'com.unity.template.random@1.3.0',
        name: 'com.unity.template.random',
        version: '1.3.0',
        tar: path.join('somePath', 'com.unity.template.random-1.3.0.tgz'),
        versions: { compatible: ['1.3.0'] }
      }]);
    });

    it('should only return the packages if they are local to the machine', async () => {
      fsFileTestTemplateMissingError = true;

      const packages = await unityPackageManagerService.getDownloadedTemplatesForEditorVersion(
        'somePath',
        {
          unityVersion: '2019.2.0f1',
          editorPath: 'somePath',
          builtInPackagesPath: 'anotherPath'
        }
      );

      expect(packages).to.deep.equal([{
        dynamicIcon: 'dynamic icon',
        id: 'com.unity.template.random@1.3.0',
        name: 'com.unity.template.random',
        version: '1.3.0',
        tar: path.join('somePath', 'com.unity.template.random-1.3.0.tgz'),
        versions: { compatible: ['1.3.0'] }
      }]);
    });

    it('should only return the package version that is currently downloaded and can be upgraded later', async () => {
      const packages = await unityPackageManagerService.getDownloadedTemplatesForEditorVersion(
        'somePath',
        {
          unityVersion: '2019.4.0f1',
          editorPath: 'somePath',
          builtInPackagesPath: 'anotherPath'
        }
      );

      expect(packages).to.deep.equal([{
        dynamicIcon: 'dynamic icon',
        id: 'com.unity.template.upgradable@1.4.1',
        name: 'com.unity.template.upgradable',
        tar: path.join('somePath', 'com.unity.template.upgradable-1.4.0.tgz'),
        version: '1.4.0',
        versions: { compatible: ['1.4.0', '1.4.1']}
      }]);
    });

    it('should return nothing if there are no templates for the editor', async () => {
      const packages = await unityPackageManagerService.getDownloadedTemplatesForEditorVersion(
        'somePath',
        {
          unityVersion: '2019.1.9f1',
          editorPath: 'somePath',
          builtInPackagesPath: 'anotherPath'
        }
      );

      expect(packages).to.deep.equal([]);
    });

    it('should reset the manifest.json if it is corrupted', async () => {
      await unityPackageManagerService.getDownloadedTemplatesForEditorVersion(
        'brokenManifest',
        {
          unityVersion: '2019.2.0f1',
          editorPath: 'somePath',
          builtInPackagesPath: 'anotherPath'
        }
      );

      expect(fsOutputJsonStub).to.have.been.calledWith(path.join('brokenManifest', 'manifest.json'), { });
    });

    it('should return nothing if the manifest.json is corrupted', async () => {
      const packages = await unityPackageManagerService.getDownloadedTemplatesForEditorVersion(
        'brokenManifest',
        {
          unityVersion: '2019.2.0f1',
          editorPath: 'somePath',
          builtInPackagesPath: 'anotherPath'
        }
      );

      expect(packages).to.deep.equal([]);
    });

    it('should emit the expected analytics event if the manifest.json is corrupted', async () => {
      await unityPackageManagerService.getDownloadedTemplatesForEditorVersion(
        'brokenManifest',
        {
          unityVersion: '2019.2.0f1',
          editorPath: 'somePath',
          builtInPackagesPath: 'anotherPath'
        }
      );

      expect(cloudAnalytics.addEvent).to.have.been.calledWithExactly({
        type: 'hub.error.v1',
        msg: {
          error_message: 'Manifest was corrupted, dependencies are unknown, forced to reset manifest.json',
          error_type: 'exception'
        }
      });
    });
  });

  describe('getDownloadableTemplates', () => {
    let legoPackageInfo;
    let legoUpmPackages;

    beforeEach(() => {
      upmPackages = { packages: [{ name: 'com.unity.template.test' }] };
      legoUpmPackages = { packages: [{ name: 'com.unity.template.test' }, { name: 'com.unity.template.lego' }] }
      packageInfo = {
        package: {
          id: 'com.unity.template.test@1.0.2',
          name: 'com.unity.template.test',
          version: '1.0.2',
          versions: { compatible: ['1.0.2'] }
        }
      };
      legoPackageInfo = {
        package: {
          id: 'com.unity.template.lego@1.0.1',
          name: 'com.unity.template.lego',
          version: '1.0.1',
          versions: { compatible: ['1.0.1'] }
        }
      },
      sandbox.stub(upm, 'getPackageInfo')
        .withArgs('com.unity.template.test', { unityVersion: '2019.2.0f1', editorPath: 'somePath', builtInPackagesPath: 'anotherPath'}).returns(packageInfo)
        .withArgs('com.unity.template.test@1.0.2', { unityVersion: '2019.2.0f1', editorPath: 'somePath', builtInPackagesPath: 'anotherPath'}).returns(packageInfo)
        .withArgs('com.unity.template.lego', { unityVersion: "2019.4.4f1", editorPath: 'somePath', builtInPackagesPath: 'anotherPath' }).returns(legoPackageInfo)
        .withArgs('com.unity.template.lego@1.0.1', { unityVersion: "2019.4.4f1", editorPath: 'somePath', builtInPackagesPath: 'anotherPath' }).returns(legoPackageInfo)
        .withArgs('com.unity.template.test', { unityVersion: '2019.4.4f1', editorPath: 'somePath', builtInPackagesPath: 'anotherPath'}).returns(packageInfo)
        .withArgs('com.unity.template.test@1.0.2', { unityVersion: '2019.4.4f1', editorPath: 'somePath', builtInPackagesPath: 'anotherPath'}).returns(packageInfo)
        .withArgs('com.unity.template.lego', { unityVersion: "2020.1.4f1", editorPath: 'somePath', builtInPackagesPath: 'anotherPath' }).returns(legoPackageInfo)
        .withArgs('com.unity.template.lego@1.0.1', { unityVersion: "2020.1.4f1", editorPath: 'somePath', builtInPackagesPath: 'anotherPath' }).returns(legoPackageInfo)
        .withArgs('com.unity.template.test', { unityVersion: '2020.1.4f1', editorPath: 'somePath', builtInPackagesPath: 'anotherPath'}).returns(packageInfo)
        .withArgs('com.unity.template.test@1.0.2', { unityVersion: '2020.1.4f1', editorPath: 'somePath', builtInPackagesPath: 'anotherPath'}).returns(packageInfo)
        .withArgs('com.unity.template.test', { unityVersion: '2018.4.0f1', editorPath: 'somePath', builtInPackagesPath: 'anotherPath'}).throws(new Error());
    });

    describe('under cache misses', () => {
      beforeEach(()=> {
        sandbox.stub(unityPackageManagerCacheService, 'getRemoteTemplates').returns(null);
        sandbox.stub(unityPackageManagerCacheService, 'getTemplatesInfo').returns(null);

        unityPackageManagerService.dynamicTemplatesConfig = {
          '2019.2': {
            'com.unity.template.test': {
              icon: 'dynamic icon',
            },
          },
          '2019.4': {
            'com.unity.template.lego': {
              icon: 'dynamic icon',
            },
          },
          '2020.1': {
            'com.unity.template.test': {
              icon: 'dynamic icon',
            },
          },
        };
      });

      it('should return all the templates info from upm', async () => {
        sandbox.stub(upm, 'search').returns(upmPackages);

        const testInfo = await unityPackageManagerService.getDownloadableTemplates({"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'});

        expect(testInfo).to.deep.equal([{
          dynamicIcon: 'dynamic icon',
          id: 'com.unity.template.test@1.0.2',
          name: 'com.unity.template.test',
          version: '1.0.2',
          versions: { compatible: ['1.0.2'] }
        }]);
      });

      it('should call upm with the correct arguments', async () => {
        sandbox.stub(upm, 'search').returns(upmPackages);

        await unityPackageManagerService.getDownloadableTemplates({"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'});

        expect(upm.search).to.have.been.calledWith('editor', undefined, { type: 'template', query: 'com.unity.template' });
        expect(upm.getPackageInfo).to.have.been.calledWith('com.unity.template.test', {"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'});
      });

      it('should return nothing if upm errors', async () => {
        sandbox.stub(upm, 'search').returns(upmPackages);

        const packagesInfos = await unityPackageManagerService.getDownloadableTemplates({"unityVersion": '2018.4.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'});

        expect(packagesInfos).to.deep.equal([]);
      });

      it('should emit the expected analytics event if upm search errors', async () => {
        sandbox.stub(upm, 'search').throws('some error');

        try {
          await unityPackageManagerService.getDownloadableTemplates({
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          });
        } catch(e) { /* Do nothing with the error */ }

        expect(cloudAnalytics.addEvent).to.have.been.calledWithExactly({
          type: 'hub.upmError.v1',
          msg: {
            error_message: 'Error while searching for remote templates in upm',
            error_type: 'upm_search',
          }
        });
      });

      it('should return the lego microgame for 2019 LTS based on the dynamic templates config', async () => {
        sandbox.stub(upm, 'search').returns(legoUpmPackages);

        const testInfo = await unityPackageManagerService.getDownloadableTemplates({
          unityVersion: '2019.4.4f1',
          editorPath: 'somePath',
          builtInPackagesPath: 'anotherPath'
        });

        expect(testInfo).to.deep.equal([{
          dynamicIcon: 'dynamic icon',
          id: 'com.unity.template.lego@1.0.1',
          name: 'com.unity.template.lego',
          version: '1.0.1',
          versions: { compatible: ['1.0.1'] }
        }]);
      });

      it('should not return the lego microgame for 2020.1 based on the dynamic templates config', async () => {
        sandbox.stub(upm, 'search').returns(legoUpmPackages);

        const testInfo = await unityPackageManagerService.getDownloadableTemplates({
          unityVersion: '2020.1.4f1',
          editorPath: 'somePath',
          builtInPackagesPath: 'anotherPath'
        });

        expect(testInfo).to.deep.equal([{
          dynamicIcon: 'dynamic icon',
          id: 'com.unity.template.test@1.0.2',
          name: 'com.unity.template.test',
          version: '1.0.2',
          versions: { compatible: ['1.0.2'] }
        }]);
      });

      it('should return only the results with host hub', async () => {
        sandbox.stub(upm, 'search')
          .withArgs('hub').returns(upmPackages)
          .withArgs('editor').returns({ packages: [] });

        const testInfo = await unityPackageManagerService.getDownloadableTemplates({"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'});

        expect(testInfo).to.deep.equal([{
          dynamicIcon: 'dynamic icon',
          id: 'com.unity.template.test@1.0.2',
          name: 'com.unity.template.test',
          version: '1.0.2',
          versions: { compatible: ['1.0.2'] }
        }]);
      });

      it('should return only the results with host editor', async () => {
        sandbox.stub(upm, 'search')
          .withArgs('hub').returns({ packages: [] })
          .withArgs('editor').returns(upmPackages);

        const testInfo = await unityPackageManagerService.getDownloadableTemplates({"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'});

        expect(testInfo).to.deep.equal([{
          dynamicIcon: 'dynamic icon',
          id: 'com.unity.template.test@1.0.2',
          name: 'com.unity.template.test',
          version: '1.0.2',
          versions: { compatible: ['1.0.2'] }
        }]);
      });

      it('should filter out duplicates if host editor and hub contain the same result', async () => {
        sandbox.stub(upm, 'search')
          .withArgs('hub').returns(upmPackages)
          .withArgs('editor').returns(upmPackages);

        const testInfo = await unityPackageManagerService.getDownloadableTemplates({"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'});

        expect(testInfo).to.deep.equal([{
          dynamicIcon: 'dynamic icon',
          id: 'com.unity.template.test@1.0.2',
          name: 'com.unity.template.test',
          version: '1.0.2',
          versions: { compatible: ['1.0.2'] }
        }]);
      });

      it('should return nothing if the config is broken', async () => {
        sandbox.stub(upm, 'search').returns(upmPackages);
        unityPackageManagerService.dynamicTemplatesConfig = {};

        const testInfo = await unityPackageManagerService.getDownloadableTemplates({"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'});

        expect(testInfo).to.deep.equal([]);
      });
    });

    describe('under cache hit', () => {
      beforeEach(()=> {
        sandbox.stub(unityPackageManagerCacheService, 'getRemoteTemplates').returns([{name: 'remote1'}, {name: 'remote2'}]);
        sandbox.stub(unityPackageManagerCacheService, 'getTemplatesInfo').returns({
          id: 'com.unity.template.test@1.0.2',
          name: 'com.unity.template.test',
          tar: path.join('somePath', 'com.unity.template.test-1.0.2.tgz'),
          version: '1.0.2',
          versions: { compatible: ['1.0.2']}
        });
      });

      it('should have used cache instead of upm', () => {
        expect(upm.getPackageInfo).to.not.have.been.called;
      });
    });
  });

  describe('downloadRemoteTemplate', () => {
    let oldManifest;
    let createWriteStreamStub;
    let outputJsonStub;
    let unityPackageManager;
    let fsFileMissing;
    let fsPartialDownloadFileMissing;
    let fsUPMManifestFileMissing;
    let upmGetPackageInfoStub;
    let availableSpace;
    let templateSpace;
    let renameFileError;
    let upmPackageInfoSuccess;
    let upmPackageInfoFailure;
    let upmResolveStub;
    let fsRemoveStub;
    let fsRenameStub;

    beforeEach(() => {
      fsFileMissing = true;
      fsPartialDownloadFileMissing = true;
      fsUPMManifestFileMissing = true;
      availableSpace = 1000;
      templateSpace = 100;
      renameFileError = null;
      upmPackageInfoSuccess = {
        package: {
          name: 'com.unity.template.test',
          version: '1.0.2',
          versions: { compatible: ['1.0.2-preview', '1.0.2'] },
          tarball: {
            url: 'url',
            size: templateSpace
          }
        }
      };
      upmPackageInfoFailure = {
        package: {
          name: 'com.unity.template.failure',
          version: '1.0.2',
          versions: { compatible: ['1.0.2'] },
          tarball: {
            url: 'url',
            size: templateSpace
          }
        }
      };

      oldManifest = {};
      createWriteStreamStub = sandbox.stub(fs, 'createWriteStream');
      upmResolveStub = sandbox.stub(upm, 'resolve');
      fsRemoveStub = sandbox.stub(fs, 'remove').returns({});
      outputJsonStub = sandbox.stub(fs, 'outputJson');
      upmGetPackageInfoStub = sandbox.stub(upm, 'getPackageInfo');
      fsRenameStub = sandbox.stub(fs, 'rename').callsFake((oldName, newName, cb) => {
        cb(renameFileError);
      });

      createWriteStreamStub
        .withArgs(path.join('somePath', 'com.unity.template.test-1.0.2.tgz.part')).returns({})
        .withArgs(path.join('somePath', 'com.unity.template.failure-1.0.2.tgz.part')).throws('some error');
      upmResolveStub
        .withArgs(path.join('somePath', 'temp-com.unity.template.test'), {"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}, { cachingLevel: 'global' }).returns({})
        .withArgs(path.join('anotherPath', 'temp-com.unity.template.test'), {"unityVersion": '2019.2.0f1', "editorPath": 'anotherPath', "builtInPackagesPath": 'anotherPath'}, { cachingLevel: 'global' }).throws(new Error());
      upmGetPackageInfoStub
        .withArgs('com.unity.template.test', sinon.match.object, sinon.match.object).callsFake(() => upmPackageInfoSuccess)
        .withArgs('com.unity.template.test@1.0.2', sinon.match.object, sinon.match.object).callsFake(() => upmPackageInfoSuccess)
        .withArgs('com.unity.template.test@1.0.2-preview', sinon.match.object, sinon.match.object).callsFake(() => ({ package: { ...upmPackageInfoSuccess.package, version: '1.0.2-preview' } }))
        .withArgs('com.unity.template.test@2.0.2', sinon.match.object, sinon.match.object).callsFake(() => upmPackageInfoSuccess)
        .withArgs('com.unity.template.failure', sinon.match.object, sinon.match.object).returns(upmPackageInfoFailure)
        .withArgs('com.unity.template.failure@1.0.2', sinon.match.object, sinon.match.object).returns(upmPackageInfoFailure);

      sandbox.stub(axios, 'get').returns(Promise.resolve({
        data: {
          pipe: () => ({}),
          on: (event, cb) => {
            if (event === 'end') {
              cb();
            }
          }
        }
      }));
      sandbox.stub(fs, 'readJson').callsFake(() => oldManifest);
      sandbox.stub(fs, 'access').callsFake((downloadPath, mode, cb) => {
        if (downloadPath === path.join('somePath', 'com.unity.template.test-1.0.2.tgz.part')) {
          cb(fsPartialDownloadFileMissing);
        } else if (downloadPath === path.join('somePath', 'temp-com.unity.template.test')) {
          cb(fsUPMManifestFileMissing);
        } else {
          cb(fsFileMissing);
        }
      });

      unityPackageManager = proxyquire('./unityPackageManagerService', {
        '../../fileSystem': {
          hubFS: {
            getDiskSpaceAvailable: () => ({
              available: availableSpace
            })
          }
        }
      });

      unityPackageManager.dynamicTemplatesConfig = {
        '2019.2': {
          'com.unity.template.test': {
            icon: 'dynamic icon',
          },
        },
      };
    });

    describe('under cache hit', () => {
      beforeEach(() => {
        sandbox.stub(unityPackageManagerCacheService, 'getTemplatesInfo').returns({
          id: 'com.unity.template.test@1.0.2',
          name: 'com.unity.template.test',
          tar: path.join('somePath', 'com.unity.template.test-1.0.2.tgz'),
          version: '1.0.2',
          versions: { compatible: ['1.0.2']}
        });
      });

      it('should have used cache instead of upm', () => {
        expect(upm.getPackageInfo).to.not.have.been.called;
      });
    });

    describe("under cache miss", () => {
      beforeEach(() => {
        sandbox.stub(unityPackageManagerCacheService, 'getTemplatesInfo').returns(null);
      });

      it('should return the package info from upm', async () => {
        const packages = await unityPackageManager.downloadRemoteTemplate(
          'com.unity.template.test',
          'somePath',
          {
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          }
        );

        expect(packages).to.deep.equal({
          dynamicIcon: 'dynamic icon',
          name: 'com.unity.template.test',
          version: '1.0.2',
          versions: { compatible: ['1.0.2-preview', '1.0.2'] },
          tar: path.join('somePath', 'com.unity.template.test-1.0.2.tgz'),
          tarball: {
            size: 100,
            url: 'url'
          }
        });
      });

      it('should return the latest package', async () => {
        upmPackageInfoSuccess = {
          package: {
            name: 'com.unity.template.test',
            version: '2.0.2',
            versions: { compatible: ['1.0.2', '2.0.2'] },
            tarball: {
              url: 'url',
              size: templateSpace
            }
          }
        };

        const packages = await unityPackageManager.downloadRemoteTemplate(
          'com.unity.template.test',
          'somePath',
          {
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          }
        );

        expect(packages).to.deep.equal({
          dynamicIcon: 'dynamic icon',
          name: 'com.unity.template.test',
          version: '2.0.2',
          versions: { compatible: ['1.0.2', '2.0.2'] },
          tar: path.join('somePath', 'com.unity.template.test-2.0.2.tgz'),
          tarball: {
            size: 100,
            url: 'url'
          }
        });
      });

      it('should not return the preview package if a non-preview version is available', async () => {
        const packages = await unityPackageManager.downloadRemoteTemplate(
          'com.unity.template.test',
          'somePath',
          {
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          }
        );

        expect(packages.version).to.not.equal('1.0.2-preview');
      });

      it('should remove existing download files if the download only partially completed', async () => {
        fsPartialDownloadFileMissing = false;

        await unityPackageManager.downloadRemoteTemplate(
          'com.unity.template.test',
          'somePath',
          {
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          }
        );

        expect(fsRemoveStub.firstCall).to.have.been.calledWithExactly(path.join('somePath', 'com.unity.template.test-1.0.2.tgz.part'));
      });

      it('should remove existing upm resolution manifest if it exists', async () => {
        fsUPMManifestFileMissing = false;

        await unityPackageManager.downloadRemoteTemplate(
          'com.unity.template.test',
          'somePath',
          {
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          }
        );

        expect(fsRemoveStub.firstCall).to.have.been.calledWithExactly(path.join('somePath', 'temp-com.unity.template.test'));
      });

      it('should resolve the dependencies for the downloaded package', async () => {
        await unityPackageManager.downloadRemoteTemplate(
          'com.unity.template.test',
          'somePath',
          {
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          }
        );

        expect(outputJsonStub.firstCall).to.have.been.calledWithExactly(
          path.join('somePath', 'temp-com.unity.template.test', 'manifest.json'),
          {
            dependencies: {
              'com.unity.template.test': `file:${path.join('somePath', 'com.unity.template.test-1.0.2.tgz')}`
            }
          }
        );
      });

      it('should add the new editor and template to the manifest.json', async () => {
        await unityPackageManager.downloadRemoteTemplate(
          'com.unity.template.test',
          'somePath',
          {
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          }
        );

        expect(outputJsonStub.secondCall).to.have.been.calledWithExactly(
          path.join('somePath', 'manifest.json'),
          {
            '2019.2.0f1': {
              dependencies: {
                'com.unity.template.test': '1.0.2'
              }
            }
          }
        );
      });

      it('should add the template to the existing editor in the manifest.json', async () => {
        oldManifest = {
          '2019.2.0f1': {
            dependencies: {
              'com.unity.template.existing': '9.9.9'
            }
          }
        };

        await unityPackageManager.downloadRemoteTemplate(
          'com.unity.template.test',
          'somePath',
          {
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          }
        );

        expect(outputJsonStub.secondCall).to.have.been.calledWithExactly(
          path.join('somePath', 'manifest.json'),
          {
            '2019.2.0f1': {
              dependencies: {
                'com.unity.template.existing': '9.9.9',
                'com.unity.template.test': '1.0.2'
              }
            }
          }
        );
      });

      it('should download the template to the correct partial location', async () => {
        await unityPackageManager.downloadRemoteTemplate(
          'com.unity.template.test',
          'somePath',
          {
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          }
        );

        expect(createWriteStreamStub).to.have.been.calledWithExactly(path.join('somePath', 'com.unity.template.test-1.0.2.tgz.part'));
      });

      it('should rename the partial file on download success', async () => {
        await unityPackageManager.downloadRemoteTemplate(
          'com.unity.template.test',
          'somePath',
          {
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          }
        );

        expect(fsRenameStub).to.have.been.calledWithExactly(path.join('somePath', 'com.unity.template.test-1.0.2.tgz.part'), path.join('somePath', 'com.unity.template.test-1.0.2.tgz'), sinon.match.any);
      });

      it('should remove the partial file on download error', async () => {
        renameFileError = 'some error';

        try {
          await unityPackageManager.downloadRemoteTemplate(
            'com.unity.template.test',
            'somePath',
            {
              unityVersion: '2019.2.0f1',
              editorPath: 'somePath',
              builtInPackagesPath: 'anotherPath'
            }
          );
        } catch (e) { /* Do nothing with the error */}

        expect(fsRemoveStub).to.have.been.calledWithExactly(path.join('somePath', 'com.unity.template.test-1.0.2.tgz.part'));
      });

      it('should error on download failure', async () => {
        renameFileError = 'some error';

        let error = null;
        try {
          await unityPackageManager.downloadRemoteTemplate(
            'com.unity.template.failure',
            'somePath',
            {
              unityVersion: '2019.2.0f1',
              editorPath: 'somePath',
              builtInPackagesPath: 'anotherPath'
            }
          );
        } catch (e) {
          error = e;
        }

        expect(error).to.be.an('error');
      });

      it('should call upm.resolve to download additional dependencies', async () => {
        await unityPackageManager.downloadRemoteTemplate(
          'com.unity.template.test',
          'somePath',
          {
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          }
        );

        expect(upmResolveStub).to.have.been.calledOnce;
      });

      it('should not call upm.resolve to download additional dependencies if the template was not missing', async () => {
        fsFileMissing = false;

        await unityPackageManager.downloadRemoteTemplate(
          'com.unity.template.test',
          'somePath',
          {
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          }
        );

        expect(upmResolveStub).not.to.have.been.calledOnce;
      });

      it('should clear away the temp upm manifest', async () => {
        await unityPackageManager.downloadRemoteTemplate(
          'com.unity.template.test',
          'somePath',
          {
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          }
        );

        expect(fsRemoveStub.firstCall).to.have.been.calledWithExactly(path.join('somePath', 'temp-com.unity.template.test'));
      });

      it('should return nothing if upm resolve errors', async () => {
        let error = null;
        try {
          await unityPackageManager.downloadRemoteTemplate(
            'com.unity.template.test',
            'anotherPath',
            {
              unityVersion: '2019.2.0f1',
              editorPath: 'anotherPath',
              builtInPackagesPath: 'anotherPath'
            });
        } catch (e) {
          error = e;
        }

        expect(error).to.be.an('error');
      });

      it('should emit the expected analytics event if upm resolve errors', async () => {
        try {
          await unityPackageManager.downloadRemoteTemplate(
            'com.unity.template.test',
            'anotherPath',
            {
              unityVersion: '2019.2.0f1',
              editorPath: 'anotherPath',
              builtInPackagesPath: 'anotherPath'
            });
        } catch (e) { /* Do nothing with the error */ }

        expect(cloudAnalytics.addEvent).to.have.been.calledWithExactly({
          type: 'hub.upmError.v1',
          msg: {
            error_message: 'Error while resolving dependencies of the downloaded template com.unity.template.test',
            error_type: 'upm_resolve',
          }
        });
      });

      it('should emit the expected analytics event if the template name could not be found by upm', async () => {
        try {
          await unityPackageManager.downloadRemoteTemplate(
            'com.unity.template.failure',
            'somePath',
            {
              unityVersion: '2019.2.0f1',
              editorPath: 'somePath',
              builtInPackagesPath: 'anotherPath'
            }
          );
        } catch (e) { /* Do nothing with the error */ }

        expect(cloudAnalytics.addEvent).to.have.been.calledWithExactly({
          type: 'hub.error.v1',
          msg: {
            error_message: 'Error while downloading the template com.unity.template.failure',
            error_type: 'popup'
          }
        });
      });

      it('should not download the remote template if it is already downloaded', async () => {
        fsFileMissing = false;

        await unityPackageManager.downloadRemoteTemplate(
          'com.unity.template.test',
          'somePath',
          {
            unityVersion: '2019.2.0f1',
            editorPath: 'somePath',
            builtInPackagesPath: 'anotherPath'
          }
        );

        expect(createWriteStreamStub).not.to.have.been.called;
      });

      it('should not download the remote template if there is not enough space', async () => {
        availableSpace = 0;
        templateSpace = 1000;

        let error;
        try {
          await unityPackageManager.downloadRemoteTemplate(
            'com.unity.template.test',
            'somePath',
            {
              unityVersion: '2019.2.0f1',
              editorPath: 'somePath',
              builtInPackagesPath: 'anotherPath'
            }
          )
        } catch (e) {
          error = e;
        }

        expect(error.message).to.equal('Insufficient space');
      });

      it('should emit the expected analytics event if there is not enough space', async () => {
        availableSpace = 0;
        templateSpace = 1000;

        try {
          await unityPackageManager.downloadRemoteTemplate(
            'com.unity.template.test',
            'somePath',
            {
              unityVersion: '2019.2.0f1',
              editorPath: 'somePath',
              builtInPackagesPath: 'anotherPath'
            }
          )
        } catch (e) { /* Do nothing with the error */ }

        expect(cloudAnalytics.addEvent).to.have.been.calledWithExactly({
          type: 'hub.error.v1',
          msg: {
            error_message: 'Insufficient space',
            error_type: 'popup'
          }
        });
      });
    });
  });

  describe('getUpgradableTemplates', () => {
    beforeEach(() => {
      unityPackageManagerService.dynamicTemplatesConfig = {
        '2019.2': {
          'com.unity.template.test': {
            icon: 'dynamic icon',
          },
          'com.unity.template.random': {
            icon: 'dynamic icon',
          },
        },
      };

      packageInfo = {
        package: {
          id: 'com.unity.template.test@1.1.0',
          name: 'com.unity.template.test',
          version: '1.1.0',
          versions: { compatible: ['1.1.0'] }
        }
      };
      randomPackageInfo = {
        package: {
          id: 'com.unity.template.random@1.3.1',
          name: 'com.unity.template.random',
          version: '1.3.1',
          versions: { compatible: ['1.3.0', '1.3.1'] }
        }
      };
      localTemplates = [{ name: "com.unity.template.test", version: "1.0.2", tar: "some-local-path"} , { name: "com.unity.template.random", version: "1.3.0", tar: "some-local-path" }]
      sandbox.stub(upm, 'getPackageInfo')
        .withArgs('com.unity.template.test', {"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}).returns(packageInfo)
        .withArgs('com.unity.template.test@1.1.0', {"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}).returns(packageInfo)
        .withArgs('com.unity.template.random', {"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}).returns(randomPackageInfo)
        .withArgs('com.unity.template.random@1.3.1', {"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}).returns(randomPackageInfo);
    });

    describe("under cache misses", () => {
      beforeEach(() => {
        sandbox.stub(unityPackageManagerCacheService, 'getTemplatesInfo').returns(null);
      });

      it('should return the upgradable packages', async () => {
        const packages = await unityPackageManagerService.getUpgradableTemplates(localTemplates, {"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}, 'somePath');

        expect(packages).to.deep.equal([{
          dynamicIcon: 'dynamic icon',
          id: 'com.unity.template.test@1.1.0',
          name: 'com.unity.template.test',
          tar: 'some-local-path',
          version: '1.1.0',
          versions: { compatible: ['1.1.0'] }
        }, {
          dynamicIcon: 'dynamic icon',
          id: 'com.unity.template.random@1.3.1',
          name: 'com.unity.template.random',
          tar: 'some-local-path',
          version: '1.3.1',
          versions: { compatible: ['1.3.0', '1.3.1'] }
        }]);
      });

      it('should return nothing if we have the latest versions', async () => {
        packageInfo.package.version = '1.0.2';
        randomPackageInfo.package.version = '1.3.0';
        const packages = await unityPackageManagerService.getUpgradableTemplates(localTemplates, {"unityVersion": '2019.2.0f1', "editorPath": 'somePath', "builtInPackagesPath": 'anotherPath'}, 'somePath');

        expect(packages).to.deep.equal([]);
      });
    });
  });
});
