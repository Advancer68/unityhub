const uprConfig = require('./defaultUPR.json');

class UPRService {
  constructor() {
    this.showUPR = uprConfig.showUPR;
  }

  setShowUPR(show) {
    this.showUPR = show;
  }

  getShowUPR() {
    return this.showUPR;
  }
}

module.exports = new UPRService();
