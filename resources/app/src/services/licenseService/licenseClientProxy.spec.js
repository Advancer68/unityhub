const settings = require('../localSettings/localSettings');
const IpcLicenseClient = require('./ipcLicenseClient');
const licenseClientProxy = require('./licenseClientProxy');

// third parties
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);
chai.use(chaiAsPromised);

describe('LicenseClientProxy', () => {
  let sandbox;
  let settingsMock;

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    settingsMock = { };
    sandbox.stub(settings, 'get').callsFake((key) => settingsMock[key]);

    sandbox.stub(licenseClientProxy.fileBaseClient, 'init');
    sandbox.stub(licenseClientProxy.fileBaseClient, 'setActivationId');
    sandbox.stub(licenseClientProxy.fileBaseClient, 'reset');
    sandbox.stub(licenseClientProxy.fileBaseClient, 'isLicenseValid');
    sandbox.stub(licenseClientProxy.fileBaseClient, 'getLicenseInfo');
    sandbox.stub(licenseClientProxy.fileBaseClient, 'activateNewLicense');
    sandbox.stub(licenseClientProxy.fileBaseClient, 'returnLicense');
    sandbox.stub(licenseClientProxy.fileBaseClient, 'loadLicense');
    sandbox.stub(licenseClientProxy.fileBaseClient, 'saveLicense');
    sandbox.stub(licenseClientProxy.fileBaseClient, 'refreshLicense');
    sandbox.stub(licenseClientProxy.fileBaseClient, 'clearErrors');

    sandbox.stub(licenseClientProxy.ipcClient, 'init');
    sandbox.stub(licenseClientProxy.ipcClient, 'setActivationId');
    sandbox.stub(licenseClientProxy.ipcClient, 'reset');
    sandbox.stub(licenseClientProxy.ipcClient, 'isLicenseValid');
    sandbox.stub(licenseClientProxy.ipcClient, 'getLicenseInfo');
    sandbox.stub(licenseClientProxy.ipcClient, 'activateNewLicense');
    sandbox.stub(licenseClientProxy.ipcClient, 'returnLicense');
    sandbox.stub(licenseClientProxy.ipcClient, 'loadLicense');
    sandbox.stub(licenseClientProxy.ipcClient, 'saveLicense');
    sandbox.stub(licenseClientProxy.ipcClient, 'refreshLicense');
    sandbox.stub(licenseClientProxy.ipcClient, 'clearErrors');

  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('licenseKinds', () => {
    it('should be the same for ipc and file based clients', () => {
      var fromProxy = licenseClientProxy.licenseKinds;
      var fromFileBaseClient = licenseClientProxy.fileBaseClient.licenseKinds;
      var fromIpcClient = licenseClientProxy.ipcClient.licenseKinds;

      expect(fromProxy).to.deep.equal(fromFileBaseClient);
      expect(fromProxy).to.deep.equal(fromIpcClient);
    });
  });

  describe('when not using entitlement based licensing', () => {
    beforeEach(() => {
      settingsMock = {
        [settings.keys.ENABLE_ENTITLEMENT_LICENSING]: false,
      };
    });

    describe('init', () => {
      it('should be called on normal licensing client', () => {
        licenseClientProxy.init();
        expect(licenseClientProxy.fileBaseClient.init).to.have.been.called;
        expect(licenseClientProxy.ipcClient.init).to.not.have.been.called;
      });
    });
    describe('setActivationId', () => {
      it('should be called on normal licensing client', () => {
        licenseClientProxy.setActivationId();
        expect(licenseClientProxy.fileBaseClient.setActivationId).to.have.been.called;
        expect(licenseClientProxy.ipcClient.setActivationId).to.not.have.been.called;
      });
    });
    describe('reset', () => {
      it('should be called on normal licensing client', () => {
        licenseClientProxy.reset();
        expect(licenseClientProxy.fileBaseClient.reset).to.have.been.called;
        expect(licenseClientProxy.ipcClient.reset).to.not.have.been.called;
      });
    });
    describe('isLicenseValid', () => {
      it('should be called on normal licensing client', () => {
        licenseClientProxy.isLicenseValid();
        expect(licenseClientProxy.fileBaseClient.isLicenseValid).to.have.been.called;
        expect(licenseClientProxy.ipcClient.isLicenseValid).to.not.have.been.called;
      });
    });
    describe('getLicenseInfo', () => {
      it('should be called on normal licensing client', () => {
        licenseClientProxy.getLicenseInfo();
        expect(licenseClientProxy.fileBaseClient.getLicenseInfo).to.have.been.called;
        expect(licenseClientProxy.ipcClient.getLicenseInfo).to.not.have.been.called;
      });
    });
    describe('activateNewLicense', () => {
      it('should be called on normal licensing client', () => {
        licenseClientProxy.activateNewLicense();
        expect(licenseClientProxy.fileBaseClient.activateNewLicense).to.have.been.called;
        expect(licenseClientProxy.ipcClient.activateNewLicense).to.not.have.been.called;
      });
    });
    describe('returnLicense', () => {
      it('should be called on normal licensing client', () => {
        licenseClientProxy.returnLicense();
        expect(licenseClientProxy.fileBaseClient.returnLicense).to.have.been.called;
        expect(licenseClientProxy.ipcClient.returnLicense).to.not.have.been.called;
      });
    });
    describe('loadLicense', () => {
      it('should be called on normal licensing client', () => {
        licenseClientProxy.loadLicense();
        expect(licenseClientProxy.fileBaseClient.loadLicense).to.have.been.called;
        expect(licenseClientProxy.ipcClient.loadLicense).to.not.have.been.called;
      });
    });
    describe('saveLicense', () => {
      it('should be called on normal licensing client', () => {
        licenseClientProxy.saveLicense();
        expect(licenseClientProxy.fileBaseClient.saveLicense).to.have.been.called;
        expect(licenseClientProxy.ipcClient.saveLicense).to.not.have.been.called;
      });
    });
    describe('refreshLicense', () => {
      it('should be called on normal licensing client', () => {
        licenseClientProxy.refreshLicense();
        expect(licenseClientProxy.fileBaseClient.refreshLicense).to.have.been.called;
        expect(licenseClientProxy.ipcClient.refreshLicense).to.not.have.been.called;
      });
    });
    describe('clearErrors', () => {
      it('should be called on normal licensing client', () => {
        licenseClientProxy.clearErrors();
        expect(licenseClientProxy.fileBaseClient.clearErrors).to.have.been.called;
        expect(licenseClientProxy.ipcClient.clearErrors).to.not.have.been.called;
      });
    });

  });

  describe('when using entitlement based licensing', () => {
    beforeEach(() => {
      settingsMock = {
        [settings.keys.ENABLE_ENTITLEMENT_LICENSING]: true,
      };
    });

    describe('init', () => {
      it('should be called on ipc licensing client', () => {
        licenseClientProxy.init();
        expect(licenseClientProxy.fileBaseClient.init).to.not.have.been.called;
        expect(licenseClientProxy.ipcClient.init).to.have.been.called;
      });
    });
    describe('setActivationId', () => {
      it('should be called on ipc licensing client', () => {
        licenseClientProxy.setActivationId();
        expect(licenseClientProxy.fileBaseClient.setActivationId).to.not.have.been.called;
        expect(licenseClientProxy.ipcClient.setActivationId).to.have.been.called;
      });
    });
    describe('reset', () => {
      it('should be called on ipc licensing client', () => {
        licenseClientProxy.reset();
        expect(licenseClientProxy.fileBaseClient.reset).to.not.have.been.called;
        expect(licenseClientProxy.ipcClient.reset).to.have.been.called;
      });
    });
    describe('isLicenseValid', () => {
      it('should be called on ipc licensing client', () => {
        licenseClientProxy.isLicenseValid();
        expect(licenseClientProxy.fileBaseClient.isLicenseValid).to.not.have.been.called;
        expect(licenseClientProxy.ipcClient.isLicenseValid).to.have.been.called;
      });
    });
    describe('getLicenseInfo', () => {
      it('should be called on ipc licensing client', () => {
        licenseClientProxy.getLicenseInfo();
        expect(licenseClientProxy.fileBaseClient.getLicenseInfo).to.not.have.been.called;
        expect(licenseClientProxy.ipcClient.getLicenseInfo).to.have.been.called;
      });
    });
    describe('activateNewLicense', () => {
      it('should be called on ipc licensing client', () => {
        licenseClientProxy.activateNewLicense();
        expect(licenseClientProxy.fileBaseClient.activateNewLicense).to.not.have.been.called;
        expect(licenseClientProxy.ipcClient.activateNewLicense).to.have.been.called;
      });
    });
    describe('returnLicense', () => {
      it('should be called on ipc licensing client', () => {
        licenseClientProxy.returnLicense();
        expect(licenseClientProxy.fileBaseClient.returnLicense).to.not.have.been.called;
        expect(licenseClientProxy.ipcClient.returnLicense).to.have.been.called;
      });
    });
    describe('loadLicense', () => {
      it('should be called on ipc licensing client', () => {
        licenseClientProxy.loadLicense();
        expect(licenseClientProxy.fileBaseClient.loadLicense).to.not.have.been.called;
        expect(licenseClientProxy.ipcClient.loadLicense).to.have.been.called;
      });
    });
    describe('saveLicense', () => {
      it('should be called on ipc licensing client', () => {
        licenseClientProxy.saveLicense();
        expect(licenseClientProxy.fileBaseClient.saveLicense).to.not.have.been.called;
        expect(licenseClientProxy.ipcClient.saveLicense).to.have.been.called;
      });
    });
    describe('refreshLicense', () => {
      it('should be called on ipc licensing client', () => {
        licenseClientProxy.refreshLicense();
        expect(licenseClientProxy.fileBaseClient.refreshLicense).to.not.have.been.called;
        expect(licenseClientProxy.ipcClient.refreshLicense).to.have.been.called;
      });
    });
    describe('clearErrors', () => {
      it('should be called on ipc licensing client', () => {
        licenseClientProxy.clearErrors();
        expect(licenseClientProxy.fileBaseClient.clearErrors).to.not.have.been.called;
        expect(licenseClientProxy.ipcClient.clearErrors).to.have.been.called;
      });
    });

  });
});