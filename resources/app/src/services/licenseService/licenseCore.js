'use strict'; // required by mocha

// unity
const cloudConfig = require('../cloudConfig/cloudConfig');
const machineService = require('../machineService/machineService');
const tokenManager = require('../../tokenManager/tokenManager');
const { fs, hubFS } = require('../../fileSystem');
const LICENSE_STATUS = require('./licenseStatus');
const editorLicense = require('@unityhub/unity-editor-license');

/* eslint-disable no-console */
const crypto = require('crypto');
const logger = require('../../logger')('UnityLicenseCore');
const Buffer = require('buffer').Buffer;
const DOMParser = require('xmldom').DOMParser;
const os = require('os');
const path = require('path');
const select = require('xml-crypto').xpath;
const SignedXml = require('xml-crypto').SignedXml;
const axios = require('axios');
const uuid = require('uuid');
const postal = require('postal');
const systemInfo = require('../localSystemInfo/systemInfo');
const { retryCount } = require('../cloudAnalytics/analyticsOptInHelper');

SignedXml.CanonicalizationAlgorithms['http://www.w3.org/TR/2001/REC-xml-c14n-20010315#WithComments'] = SignedXml.CanonicalizationAlgorithms['http://www.w3.org/2001/10/xml-exc-c14n#WithComments'];

function getLicenseHost() {
  return `${cloudConfig.urls.license}/`;
}

function getActivationHost() {
  return `${cloudConfig.urls.activation}/`;
}

const LICENSE_COMMAND = {
  LICENSE_NEW_COMMAND: 9,
  LICENSE_RETURN_COMMAND: 3,
  LICENSE_UPDATE_COMMAND: 2
};

const LicenseInfoFlag = {
  lf_pro_version: 0,
  lf_unused: 1,                    // Was used to detect if one should show watermarks, which did not work for trial activations. Use lm.Flag(lf_activated) && ! lm.Flag(lf_trial) instead
  lf_team_license: 2,              // This used to be lf_maint_client.
  lf_iphone_basic: 3,
  lf_iphone_pro: 4,
  lf_xbox_360: 5,
  lf_ps3: 6,
  lf_mmo: 7,
  lf_psp2: 9,
  lf_nokia_phone: 10,
  lf_sony_ericsson_phone: 11,
  lf_android_basic: 12,
  lf_android_pro: 13,
  //  14 and 15 are gone after 4.5: lf_flash_basic=14, lf_flash_pro=15,
  lf_embedded: 16,
  //  17 and 18 are gone after 5.4: lf_bb10_basic=17, lf_bb10_pro=18,
  lf_winrt_basic: 19,
  lf_winrt_pro: 20,
  lf_ps4: 21,
  lf_xbox_one: 22,
  lf_wiiu: 23,
  lf_stv_basic: 24,
  lf_stv_pro: 25,
  lf_cluster_rendering: 30,        // Flag to enable cluster rendering build
  lf_gis: 31,
  lf_closed_network: 32,
  lf_tizen_basic: 33,
  lf_tizen_pro: 34,
  //  lf_multi_display=35,        // Update: 5.3 does not require separate license for Multi-Display in StandalonePlayers.
  lf_psm: 36,
  lf_nintendo_3ds: 39,

  lf_no_watermark: 60,             // Used to disable the trial watermark. No features enabled/disabled
  lf_prototyping_watermark: 61,    // Used to enable the prototyping watermark. No features enabled/disabled
  lf_unity_free: 62,               // Should be shown as simply Unity (no Pro nor Indie) in the about dialogue,
  lf_edu_watermark: 63,            // Used to enable the educational watermark. No features enabled/disabled

  lf_trial: 64,                    // Set to true if the current license is a trial license (and not only an unactivated demo)
};

class MyKeyInfo {
  getKeyInfo(key, prefix) { // eslint-disable-line
    return '<X509Data></X509Data>';
  }

  getKey(keyInfo) { // eslint-disable-line
    return new Uint8Array(Buffer.from(`-----BEGIN CERTIFICATE-----
MIIE7zCCA9egAwIBAgIRAJZpZsDepakv5CaXJit+yx4wDQYJKoZIhvcNAQEFBQAwVDELMAkGA1UE
BhMCQkUxGTAXBgNVBAoTEEdsb2JhbFNpZ24gbnYtc2ExKjAoBgNVBAMTIUdsb2JhbFNpZ24gUGVy
c29uYWxTaWduIDIgQ0EgLSBHMjAeFw0xMjA4MDcxMzA3MzdaFw0xMzA4MDgxMzA3MzdaMIGHMQsw
CQYDVQQGEwJESzETMBEGA1UECBMKQ29wZW5oYWdlbjETMBEGA1UEBxMKQ29wZW5oYWdlbjEfMB0G
A1UEChMWVW5pdHkgVGVjaG5vbG9naWVzIEFwczELMAkGA1UEAxMCSVQxIDAeBgkqhkiG9w0BCQEW
EWFkbWluQHVuaXR5M2QuY29tMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA18DUjObN
s/rPbTGfVbQkt0FVOEhBb3FF90ZzkLs5dApGvtuKTUmo94Xoha5rkXBWFTnRGRXSANuqAljfHFiJ
3QtXB4l9SzrwGrvswWHh3hZh/AIhbwBanDWT02NEd92hOPOoCkMzHxGcJWT+dKYgSTgTmDx28tsG
urvgkdETqO8Ueo/Y0hIXRTQMtJ0wih6U6WQ1RxY+qTo6ImrAz/CjhtpfkyZ+yj8iZbW5uCJ8/bjO
MmTpO/awDcrkooFxd16/hMuCuhkq4Iejuk8/9i48DyqtA7Q3utQJ2FA97NONuWdOz7lms/MeHHNG
Izhhe+vyAbZuSrtr8gQF3Rw5Edu1bwIDAQABo4IBhjCCAYIwDgYDVR0PAQH/BAQDAgWgMEwGA1Ud
IARFMEMwQQYJKwYBBAGgMgEoMDQwMgYIKwYBBQUHAgEWJmh0dHBzOi8vd3d3Lmdsb2JhbHNpZ24u
Y29tL3JlcG9zaXRvcnkvMBwGA1UdEQQVMBOBEWFkbWluQHVuaXR5M2QuY29tMAkGA1UdEwQCMAAw
HQYDVR0lBBYwFAYIKwYBBQUHAwIGCCsGAQUFBwMEMEMGA1UdHwQ8MDowOKA2oDSGMmh0dHA6Ly9j
cmwuZ2xvYmFsc2lnbi5jb20vZ3MvZ3NwZXJzb25hbHNpZ24yZzIuY3JsMFUGCCsGAQUFBwEBBEkw
RzBFBggrBgEFBQcwAoY5aHR0cDovL3NlY3VyZS5nbG9iYWxzaWduLmNvbS9jYWNlcnQvZ3NwZXJz
b25hbHNpZ24yZzIuY3J0MB0GA1UdDgQWBBS77TdJmqER3qBFAT+yU4zBB8a1xTAfBgNVHSMEGDAW
gBQ/FdJtfC/nMZ5DCgaolGwsO8XuZTANBgkqhkiG9w0BAQUFAAOCAQEAC//kW1Pu07FhYo0Wmju5
ZCDaolsieAQpnDeJC1P76dfQxnGGibscK1xRk3lhiFx6cDoxnOyCiKt/CdWWtAMO6bnO4UmFEtO3
UljKg4VmayvGMhW5dup3M7FRn/CDM6UJl3dHJ3PmclbDZEQ0ctiXxBwIEPFy1Y1X9b3SwznX3pWJ
/UsQ270DtKuVz3kUqSpZEhBo8Gb1m+FoGsnGQb+8vnfEGgD9/bxURhTUeteQ1N+CGyfTCd0QVqKx
zPO43SpWwQ50SDtQT0bEZeA+UOdqSH04W4XCkcmx+1zZ8GtHihaefyxDceZOKKPq4Gi+02JbwWuX
JXFP96+m73xQXG96dg==
-----END CERTIFICATE-----`, 'utf8'));
  }
}

const UnityLicenseVersion = '6.x';
const UnityLicenseOldVersion = '5.x';
const UnityVersionMajor = '2017';
const UnityVersionMinor = '2';
const UnityVersionPatch = '0';
const UnityVersionFull = `${UnityVersionMajor}.${UnityVersionMinor}.${UnityVersionPatch}`;
const UnityLicenseFileName = 'Unity_lic.ulf';
const UnityLicenseOldFileName = `Unity_v${UnityLicenseOldVersion}.ulf`;

function getLicensePath(fileName) {
  if (!fileName) {
    return '';
  }

  if (os.platform() === 'win32') {
    return path.join(path.join(process.env.ProgramData, 'Unity'), fileName);
  } else if (os.platform() === 'darwin') {
    return path.join('/Library/Application Support/Unity', fileName);
  } else if (os.platform() === 'linux') {
    return path.join(`${os.homedir()}/.local/share/unity3d/Unity`, fileName);
  }
  return '';
}

function isWindows10OrGreater() {
  if (os.platform() === 'win32') {
    var releaseNo = parseInt(os.release().split('.')[0], 10);
    if (!isNaN(releaseNo) && releaseNo >= 10) {
      return true;
    }
  }
  return false;
}

class UnityLicenseCore {

  get licenseKinds() {
    return {
      PRO: 'Unity Pro',
      PLUS: 'Unity Plus',
      EDU: 'Unity Edu',
      PERSONAL: 'Unity Personal',
      TRIAL: 'Unity Trial',
      UNKNOWN: 'Unity'
    };
  }

  constructor() {
    this.licenseStatus = LICENSE_STATUS.kLicenseStatus_Unknown;
  }

  reset() {
    this.licenseFile = getLicensePath(UnityLicenseFileName);
    this.backupLicenseFile = `${this.licenseFile}.backup`;
    this.systemInfo = `<SystemInfo><IsoCode>en</IsoCode><UserName>${this._escapeXML(os.userInfo().username)}</UserName><OperatingSystem>${os.platform()}</OperatingSystem><OperatingSystemNumeric></OperatingSystemNumeric><ProcessorType>${os.cpus()[0].model}</ProcessorType><ProcessorSpeed>${os.cpus()[0].speed}</ProcessorSpeed><ProcessorCount>${os.cpus().length}</ProcessorCount><ProcessorCores>0</ProcessorCores><PhysicalMemoryMB>${os.totalmem() / 1024 / 1024}</PhysicalMemoryMB><ComputerName>${this._escapeXML(os.hostname())}</ComputerName><ComputerModel>N/A</ComputerModel><UnityVersion>${UnityVersionFull}</UnityVersion><SupportedLicenseVersion>${UnityLicenseVersion}</SupportedLicenseVersion></SystemInfo>`;
    this.txID = '';
    this.rxID = '';
    this.mSerialHash = '';
    this.mCommand = LICENSE_COMMAND.LICENSE_UPDATE_COMMAND;
    this.mPostedLicenseData = '';
    this.mMustUpdate = false;
    this.mMustUpdateSetByVersion = false;
    this.machineBindings = null;
    this.offlineDisabled = false;
    this.mHasExpirationDate = false;
    this.hasPlusEntitlement = false;
    this.lastAttemptUpdateDate = null;
    this.mToken = [];
    return machineService.getAllBindings()
      .then((mb) => {
        this.machineBindings = mb;
      });
  }

  _escapeXML(unsafe) {
    return unsafe.replace(/[<>&'"]/g, ((c) => {
      switch (c) {
        case '<': return '&lt;';
        case '>': return '&gt;';
        case '&': return '&amp;';
        case '\'': return '&apos;';
        default: return '&quot;';
      }
    }));
  }
  rawLicenseData() {
    var data = `<?xml version="1.0" encoding="UTF-8"?><root><License id="Terms"><MachineID Value="${this.getMachineId()}" /><MachineBindings><Binding Key="1" Value="${this.machineBindings[1]}" />`;
    if (this.machineBindings[2] && this.machineBindings[2].length > 0) {
      data += `<Binding Key="2" Value="${this.machineBindings[2]}" />`;
    }
    if (os.platform() === 'win32') {
      data += `<Binding Key="4" Value="${this.machineBindings[4]}" />`;
    }
    if ((os.platform() === 'win32' && isWindows10OrGreater()) || this.machineBindings[2] === null || this.machineBindings[2] === '') {
      data += `<Binding Key="5" Value="${this.machineBindings[5]}" />`;
    }
    return `${data}</MachineBindings><UnityVersion Value="5.6.0" /></License></root>`;
  }

  getMachineId() {
    // mac is binding1 + binding2 (if binding2 is not there, use binding5)
    let mb = this.machineBindings[2];
    if (mb === null || mb === '') {
      mb = this.machineBindings[5];
    }

    var id = this.machineBindings[1] + mb;
    if (os.platform() === 'win32') {
      id = this.machineBindings[1] + this.machineBindings[2] + this.machineBindings[4];
    }

    var sha1 = crypto.createHash('sha1').update(id).digest('hex');
    return Buffer.from(sha1, 'hex').toString('base64');
  }

  getMachineId2() {
    var id = this.machineBindings[5] + this.machineBindings[2] + this.machineBindings[4];
    var sha1 = crypto.createHash('sha1').update(id).digest('hex');
    return Buffer.from(sha1, 'hex').toString('base64');
  }

  getSerialHash() {
    return this.mSerialHash;
  }

  getLicenseFile(file) {
    return new Promise((resolve, reject) => {
      file = typeof file !== 'undefined' ? file : this.licenseFile;
      fs.readFile(file, 'utf8', (err, data) => {
        if (typeof err === 'undefined' || err === null) {
          resolve(data.toString());
        } else {
          if (err instanceof Error && err.code === 'ENOENT') {
            logger.warn('License File is not available.');
            // return empty string
            resolve('');
          }
          reject(err);
        }
      });
    });
  }

  deleteLicense(file) {
    file = typeof file !== 'undefined' ? file : this.licenseFile;
    logger.debug('delete license file');
    try {
      logger.info(`Deleting file: ${file}`);
      fs.unlinkSync(file);
    } catch (e) {
      if (e instanceof Error) {
        if (e.code === 'ENOENT' || e.code === 'EPERM') {
          logger.debug(`Failed to delete license file: ${file}.`, e);
        } else {
          logger.warn(`Failed to delete license file: ${file}.`, e);
        }
      }
    }
  }

  clearLicense() {
    var filepath = this.licenseFile;
    try {
      logger.info(`Clear file: ${filepath}`);
      fs.closeSync(fs.openSync(filepath, 'w'));
    } catch (e) {
      if (e instanceof Error) {
        logger.warn(`Failed to clear license file: ${filepath}.'`);
      }
    }
  }

  appendXMLTime(xmlName, time, licensedata, hardwareId) {
    var doc = new DOMParser().parseFromString(licensedata);
    var root = doc.getElementsByTagName('root')[0];
    if (root == null) {
      logger.warn('root is empty');
      return '';
    }
    var timeNode = root.getElementsByTagName(xmlName);
    var encryptedValue = editorLicense.encrypt(parseInt(time, 10), hardwareId);
    if (timeNode.length > 0) {
      timeNode[0].setAttribute('Value', encryptedValue);
    } else {
      var node = doc.createElement(xmlName);
      node.setAttribute('Value', encryptedValue);
      root.appendChild(node);
    }
    return doc.toString();
  }

  injectSystemInfo(licenseData) {
    return licenseData.replace('<root>', `<root>${this.systemInfo}`);
  }

  moveReplaceLicenseFile() {
    try {
      var stats = fs.statSync(this.licenseFile);
      if (stats !== undefined) {
        fs.renameSync(this.licenseFile, this.backupLicenseFile);
      }
    } catch (e) {
      // ignore
    }
  }

  restoreBackupLicenseFile() {
    return new Promise((resolve, reject) => {
      if (fs.existsSync(this.backupLicenseFile)) {
        fs.rename(this.backupLicenseFile, this.licenseFile, (err) => {
          if (err) {
            reject();
          } else {
            this.verifyLicense()
              .then((result) => {
                resolve(result);
              })
              .catch(() => {
                reject();
              });
          }
        });
      } else {
        reject();
      }
    });
  }

  verifySignature(doc, xml) {
    var signature = select(doc, '/*/*[local-name(.)=\'Signature\' and namespace-uri(.)=\'http://www.w3.org/2000/09/xmldsig#\']')[0];
    if (signature === undefined) {
      return false;
    }
    var sig = new SignedXml();
    sig.idAttributes = ['id'];
    sig.keyInfoProvider = new MyKeyInfo();
    sig.loadSignature(signature);
    var res = sig.checkSignature(xml);

    if (!res) {
      logger.warn(sig.validationErrors);
    }
    return res;
  }

  getValueByTagName(doc, tagName) {
    var nodes = doc.getElementsByTagName(tagName);
    if (nodes.length > 0 && nodes[0].hasAttribute('Value')) {
      return nodes[0].getAttribute('Value');
    }
    return '';
  }

  mktime(hour, minute, month, day, year) {
    return (new Date(year, month, day, hour, minute, 0)).getTime() / 1000;
  }

  getGMTDate() {
    var dt = new Date();
    return this.mktime(dt.getUTCHours(), dt.getUTCMinutes(), dt.getUTCMonth(), dt.getUTCDate(), dt.getUTCFullYear());
  }

  verifyXmlTime(doc, xmlName, hardwareId, passIfNA) {
    var currentTime = this.getGMTDate();

    var root = doc.getElementsByTagName('root')[0];
    if (root == null) {
      return false;
    }

    var timeNode = root.getElementsByTagName(xmlName);
    if (timeNode.length > 0) {
      var storedTime = parseInt(editorLicense.decrypt(timeNode[0].getAttribute('Value'), hardwareId), 10);
      if (isNaN(storedTime) || storedTime === 0) {
        return false;
      }
      if (currentTime > (storedTime - 86400)) { // 3600 * 24
        return true;
      }

      if (currentTime < storedTime) {
        return false;
      }
    } else if (passIfNA) {
      return true;
    }

    return false;
  }

  verifyLicenseVersion(doc) {
    var licenseVersionNodes = doc.getElementsByTagName('LicenseVersion');
    if (licenseVersionNodes.length > 0) {
      var licenseVersionValue = licenseVersionNodes[0].getAttribute('Value');
      if (licenseVersionValue === UnityLicenseVersion) {
        this.mMustUpdateSetByVersion = false;
        return true;
      }
      if (licenseVersionValue === UnityLicenseOldVersion) {
        this.mMustUpdateSetByVersion = true;
        return true;
      }
    }
    this.licenseStatus = LICENSE_STATUS.kLicenseErrorFlag_VersionMismatch;
    return false;
  }

  convertStringToUTCDate(aDate) {
    if (aDate.match(/\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d/) !== null) {
      return new Date(`${aDate}.000Z`);
    }
    return new Date(aDate);
  }

  getInfinityDate() {
    return new Date('2112-01-27T08:00:00.000Z');
  }

  getNextPaymentDate(startString) {    
    var startDate = this.convertStringToUTCDate(startString);
    var currentDate = new Date();
    var nextPaymentDate = new Date(currentDate);

    nextPaymentDate.setDate(startDate.getDate());
    if (nextPaymentDate <= currentDate) {
      nextPaymentDate.setMonth(nextPaymentDate.getMonth() + 1);
    }

    nextPaymentDate.setHours(startDate.getHours());
    nextPaymentDate.setMinutes(startDate.getMinutes());
    nextPaymentDate.setSeconds(startDate.getSeconds());
    nextPaymentDate.setMilliseconds(startDate.getMilliseconds());

    var nextPaymentDateStr = nextPaymentDate.toJSON();
    return nextPaymentDateStr.substr(0, nextPaymentDateStr.length - 5);
  }

  verifyDates(doc) {
    var startString = this.getValueByTagName(doc, 'StartDate');
    var stopString = this.getValueByTagName(doc, 'StopDate');
    var updateString = this.getValueByTagName(doc, 'UpdateDate');
    var currentDate = new Date();
    this.startDate = currentDate;
    this.stopDate = this.getInfinityDate();
    this.displayedStopDate = this.stopDate;

    if (startString.length > 0 || stopString.length > 0) {
      if (startString.length > 0) {
        this.startDate = this.convertStringToUTCDate(startString);
      }

      if (currentDate < this.startDate) {
        this.licenseStatus = LICENSE_STATUS.kLicenseErrorFlag_LicenseNotReadyForUse;
        return false;
      }
      this.mHasExpirationDate = false;
      
      if (stopString.length > 0) {
        this.mHasExpirationDate = true;
        this.stopDate = this.convertStringToUTCDate(stopString);

        if (startString.length > 0) {
          var startYear = (new Date(startString)).getFullYear();
          var stopYear = (new Date(stopString)).getFullYear();
          var yearDelta = stopYear - startYear;
          if (yearDelta > 99) {
            stopString = this.getNextPaymentDate(startString);
          }
        }
        this.displayedStopDate = this.convertStringToUTCDate(stopString);
      }

      if (this.stopDate < currentDate) {
        this.licenseStatus = LICENSE_STATUS.kLicenseErrorFlag_LicenseExpired;
        return false;
      }
    }

    this.updateDate = this.convertStringToUTCDate(updateString);
    if (this.updateDate < currentDate) {
      this.mMustUpdate = true;
    } else {
      this.mMustUpdate = false;
    }
    return true;
  }

  verifyPlusEntitlement(doc) {
    this.hasPlusEntitlement = false;
    const entitlementNodes = doc.getElementsByTagName('Entitlement');

    if (entitlementNodes.length > 0) {
      for (let i = 0; i < entitlementNodes.length; i++) {
        const tag = entitlementNodes[i].getAttribute('Tag');

        if (tag.length > 0 && tag === 'UnityPersonalPlus') {
          this.hasPlusEntitlement = true;
          break;
        }
      }
    }
  }

  verifyMachineBindings(doc) {
    return new Promise((resolve) => {
      var binding = {};
      var nodes = doc.getElementsByTagName('Binding');

      for (var i = 0; i < nodes.length; i++) {
        var node = nodes[i];
        var elementKey = node.getAttribute('Key');
        var value = node.getAttribute('Value');
        binding[elementKey] = value;
      }

      machineService.getAllBindings()
        .then((mb) => {
          var currentLicenseStatus = this.licenseStatus;
          if (binding['1'] !== mb[1]) {
            logger.info(`Binding1 mismatch: ${binding['1']} and ${mb[1]}`);
            this.licenseStatus = LICENSE_STATUS.kLicenseErrorFlag_MachineBinding1;
          }

          // harddisk serial number could be empty
          if ('2' in binding && binding['2'] !== mb[2] && this._escapeXML(binding['2']) !== mb[2]) {
            logger.info(`Binding2 mismatch: ${binding['2']} and ${mb[2]}`);
            this.licenseStatus = LICENSE_STATUS.kLicenseErrorFlag_MachineBinding2;
            resolve(false);
            return;
          }

          if (os.platform() === 'win32' && '4' in binding && binding['4'] !== mb[4]) {
            logger.info(`Binding4 mismatch: ${binding['4']} and ${mb[4]}`);
            this.licenseStatus = LICENSE_STATUS.kLicenseErrorFlag_MachineBinding4;
            resolve(false);
            return;
          }

          if (isWindows10OrGreater()) {
            if (binding['5'] !== undefined && binding['5'] !== null) {
              machineService.hasMac(binding['5']).then((result) => {
                if (result) {
                  this.machineBindings[5] = binding['5'];

                  // reset machinebinding1 failure
                  this.licenseStatus = currentLicenseStatus;
                  resolve(true);
                  return;
                }

                logger.info(`Cannot find binding5 (MAC Address): ${binding['5']}`);
                this.licenseStatus = LICENSE_STATUS.kLicenseErrorFlag_MachineBinding5;
                resolve(false);
              });
              return;
            }
          }

          if (this.licenseStatus === LICENSE_STATUS.kLicenseErrorFlag_MachineBinding1) {
            resolve(false);
            return;
          }

          resolve(true);
        });
    });
  }

  readElementValueByTag(doc, tag) {
    var root = doc.getElementsByTagName('root')[0];
    if (root == null) {
      return null;
    }

    var nodes = doc.getElementsByTagName(tag);
    if (nodes.length > 0) {
      return nodes[0].getAttribute('Value');
    }

    return null;
  }

  readDeveloperData(doc) {
    var developerDataValue = this.readElementValueByTag(doc, 'DeveloperData');
    if (developerDataValue === null) {
      return false;
    }
    var developerData = Buffer.from(developerDataValue, 'base64').toString('utf-8');
    if (developerData.length >= 20 && developerData.length <= 63) {
      this.mSerialNumber = developerData.substring(4);
      return true;
    }
    return false;
  }

  readOfflineDisabled(doc) {
    var offlineDisabled = this.readElementValueByTag(doc, 'AlwaysOnline');
    this.offlineDisabled = false;

    if (offlineDisabled === 'true') {
      this.offlineDisabled = true;
    }
  }

  readToken(doc) {
    var root = doc.getElementsByTagName('root')[0];
    if (root == null) {
      return false;
    }

    var featuresRoot = root.getElementsByTagName('Features');
    if (featuresRoot.length !== 1) {
      logger.info('Failed to read features node in license file');
      return false;
    }

    var nodes = featuresRoot[0].getElementsByTagName('Feature');
    this.mToken = [];
    for (var i = 0; i < nodes.length; i++) {
      var node = nodes[i];
      var feature = parseInt(node.getAttribute('Value'), 10);
      if (!this.mToken.includes(feature)) {
        this.mToken.push(feature);
      }
    }
    return true;
  }

  verifyLicenseData(xml, newfile = false) {
    return new Promise((resolve, reject) => {
      //resolve(true);//新增这行
      logger.warn("crack succesk")
      this.licenseStatus = LICENSE_STATUS.kLicenseStatus_Valid;
      resolve(true);
      return;
      if (xml === '') {
        this.licenseStatus = LICENSE_STATUS.kLicenseErrorFlag_NoLicense;
        reject();
        return;
      }

      var doc = new DOMParser().parseFromString(xml);

      if (!this.verifySignature(doc, xml)) {
        this.licenseStatus = LICENSE_STATUS.kLicenseErrorFlag_FileCompromised;
        reject();
        return;
      }

      if (!this.verifyLicenseVersion(doc)) {
        reject();
        return;
      }

      if (!this.verifyDates(doc)) {
        reject();
        return;
      }

      this.verifyPlusEntitlement(doc);

      this.verifyMachineBindings(doc)
        .then((valid) => {
          
          this.licenseStatus = LICENSE_STATUS.kLicenseStatus_Valid;
          resolve(true);
          return;

          if (!valid) {
            reject();
            return;
          }

          this.readOfflineDisabled(doc);

          // Read Token
          if (!this.readToken(doc)) {
            this.licenseStatus = LICENSE_STATUS.kLicenseErrorFlag_Tokens;
            reject();
            return;
          }

          // Read Developer Data
          if (!this.readDeveloperData(doc)) {
            this.licenseStatus = LICENSE_STATUS.kLicenseErrorFlag_DeveloperData;
            reject();
            return;
          }

          // Read Serial Hash
          var serialHashNodes = doc.getElementsByTagName('SerialHash');
          if (serialHashNodes.length > 0) {
            this.mSerialHash = serialHashNodes[0].getAttribute('Value');
          }

          // Verify XML Time
          if (this.mHasExpirationDate && !newfile) {
            if (!this.verifyXmlTime(doc, 'TimeStamp', this.getMachineId()) ||
                (isWindows10OrGreater() && !this.verifyXmlTime(doc, 'TimeStamp2', this.getMachineId2(), true))
            ) {
              this.licenseStatus = LICENSE_STATUS.kLicenseErrorFlag_LicenseExpired;
              this.deleteLicense();
              reject();
              return;
            }
            this.writeLicenseFile(xml)
              .then(() => {
                this.licenseStatus = LICENSE_STATUS.kLicenseStatus_Valid;
                resolve(true);
              })
              .catch((reason) => {
                reject(reason);
              });
          } else {
            // validation passed
            this.licenseStatus = LICENSE_STATUS.kLicenseStatus_Valid;
            resolve(true);
          }
        });
    });
  }

  verifyLicense() {
    return new Promise((resolve, reject) => {
      this.handleOldLicense().then(() => {
        this.getLicenseFile().then((xml) => {
          this.verifyLicenseData(xml)
            .then((result) => {
              resolve(result);
            })
            .catch(() => {
              resolve(false);
            });
        }).catch((err) => {
          reject(err);
        });
      }).catch((err) => {
        reject(err);
      });
    });
  }

  writeAndPublish(licenseData) {
    fs.writeFileSync(this.licenseFile, licenseData);
    postal.publish({
      channel: 'app',
      topic: 'license.change',
      data: {
        kind: this.getLicenseKind()
      }
    });
  }

  writeLicenseFile(licenseData) {
    const resultingPromise = new Promise((resolve, reject) => {
      if (licenseData === '') {
        reject(false);
      }

      // make sure licenseData is string
      if (typeof licenseData !== 'string' && !(licenseData instanceof String)) {
        reject(false);
      }

      this.deleteLicense(this.backupLicenseFile);
      var cdate = this.getGMTDate();
      licenseData = this.appendXMLTime('TimeStamp', Math.floor(cdate).toString(), licenseData, this.getMachineId());
      if (isWindows10OrGreater()) {
        licenseData = this.appendXMLTime('TimeStamp2', Math.floor(cdate).toString(), licenseData, this.getMachineId2());
      }

      const doc = new DOMParser().parseFromString(licenseData);
      if (this.verifySignature(doc, licenseData)) {
        try {
          logger.info(`Writing file: ${this.licenseFile}`);
          this.writeAndPublish(licenseData);
          resolve(true);
        } catch (writeFileError) {
          if (writeFileError.code === 'ENOENT' || writeFileError.code === 'EPERM' || writeFileError.code === 'EACCES') {
            // ok, license directory was probably missing or for some reason the permissiona sre not correct, let's (re)create it an retry
            const licenseDir = path.dirname(this.licenseFile);

            if (os.platform() === 'darwin') {
              hubFS.elevateAndMakeDir(licenseDir.replace(/\s/g, '\\ '), 777)
                .then(() => {
                  this.writeAndPublish(licenseData);
                  resolve(true);
                })
                .catch(() => {
                  reject(false);
                });
            } else {
              try {
                const licenseParent = path.dirname(licenseDir);
                try {
                  fs.accessSync(licenseParent);
                } catch (accessError) {
                  fs.mkdirSync(licenseParent);
                }
                fs.mkdirSync(licenseDir);
                this.writeAndPublish(licenseData);
                resolve(true);
              } catch (makeDirError) {
                logger.warn(`Fail to write license file, reason: ${makeDirError.message}`);
                reject('Fail to write license file, please restart Hub and try again.');
              }
            }

          } else {
            logger.warn(`Fail to write license file, reason: ${writeFileError.message}`);
            reject('Fail to write license file, please restart Hub and try again.');
          }
        }
      } else {
        reject(false);
      }
    });
    return resultingPromise;
  }

  getAccessToken() {
    if (tokenManager.accessToken.value !== undefined) {
      return tokenManager.accessToken.value;
    }
    return '';
  }

  initLicense() {
    this.txID = uuid.v1().replace(/-/g, '');
    this.rxID = '';
    this.mPostedLicenseData = this.injectSystemInfo(this.rawLicenseData(), this.systemInfo);
    this.mCommand = LICENSE_COMMAND.LICENSE_NEW_COMMAND;

    const url = `${getLicenseHost()}update/poll?cmd=${this.mCommand}&tx_id=${this.txID}`;

    return axios.post(url, this.mPostedLicenseData, {
      headers: {
        'User-Agent': `hub ${systemInfo.hubVersion()}.cn`,
        'Content-Type': 'text/xml',
        AUTHORIZATION: `Bearer ${this.getAccessToken()}`
      }
    }).then(response => response.data);
  }

  updateLicense() {
    this.lastAttemptUpdateDate = new Date();
    this.txID = uuid.v1().replace(/-/g, '');
    this.rxID = '';
    return new Promise((resolve, reject) => {
      this.getLicenseFile().then((xml) => {
        if (!xml || xml.length === 0) {
          reject('empty license');
        }
        var licensedata = xml;
        this.mCommand = LICENSE_COMMAND.LICENSE_UPDATE_COMMAND;
        this.mPostedLicenseData = this.injectSystemInfo(licensedata);

        const url = `${getLicenseHost()}update/poll?cmd=${this.mCommand}&tx_id=${this.txID}`;

        axios.post(url, this.mPostedLicenseData, {
          headers: {
            'User-Agent': `hub ${systemInfo.hubVersion()}.cn`,
            'Content-Type': 'text/xml',
            AUTHORIZATION: `Bearer ${this.getAccessToken()}`
          }
        })
          .then((response) => { resolve(response.data); })
          .catch((err) => { reject(err); });
      }).catch((err) => { reject(err); });
    });
  }

  returnLicense() {
    this.txID = uuid.v1().replace(/-/g, '');
    this.rxID = '';

    return new Promise((resolve, reject) => {
      this.getLicenseFile().then((xml) => {
        var licensedata = xml;
        this.mCommand = LICENSE_COMMAND.LICENSE_RETURN_COMMAND;
        this.mPostedLicenseData = this.injectSystemInfo(licensedata);

        const url = `${getLicenseHost()}update/poll?cmd=${this.mCommand}&tx_id=${this.txID}`;

        axios.post(url, this.mPostedLicenseData, {
          headers: {
            'User-Agent': `hub ${systemInfo.hubVersion()}.cn`,
            'Content-Type': 'text/xml',
            AUTHORIZATION: `Bearer ${this.getAccessToken()}`
          }
        }).then((response) => {
          resolve(response.data);
        }).catch((err) => {
          reject(err);
        });
      }).catch((err) => {
        reject(err);
      });
    });
  }

  downloadLicense() {
    const url = `${getActivationHost()}license.fcgi?CMD=${this.mCommand}&TX=${this.txID}&RX=${this.rxID}`;

    return axios.post(url, this.mPostedLicenseData, {
      headers: {
        'User-Agent': `hub ${systemInfo.hubVersion()}.cn`,
        'Content-Type': 'text/xml',
        AUTHORIZATION: `Bearer ${this.getAccessToken()}`
      }
    }).then(response => response.data);
  }

  setRx(rx) {
    this.rxID = rx;
  }

  getLicenseStartDate() {
    return this.startDate;
  }

  getLicenseUpdateDate() {
    return this.updateDate;
  }

  getLicenseStopDate() {
    return this.stopDate;
  }

  getDisplayedLicenseStopDate() {
    return this.displayedStopDate;
  }

  getMustUpdate() {
    if (this.mMustUpdate || this.mMustUpdateSetByVersion) {
      if (this.getAccessToken() === '') {
        logger.warn('Please login Hub so that the license could be updated');
      }
    }
    return (this.mMustUpdate || this.mMustUpdateSetByVersion) && this.getAccessToken() !== '';
  }

  getLicenseToken() {
    return this.mToken;
  }

  getTransactionId() {
    return this.txID;
  }

  clearActivation() {
    this.mToken = [];
  }

  checkFlag(flag) {
    if (flag < LicenseInfoFlag.lf_trial) {
      return this.mToken.includes(flag);
    }
    return false;
  }

  isPro() {
    return this.checkFlag(LicenseInfoFlag.lf_pro_version);
  }

  isPlus() {
    return this.hasPlusEntitlement;
  }

  isEdu() {
    return this.checkFlag(LicenseInfoFlag.lf_edu_watermark);
  }

  isFree() {
    return this.checkFlag(LicenseInfoFlag.lf_unity_free);
  }

  isTrial() {
    return this.checkFlag(LicenseInfoFlag.lf_trial);
  }

  /**
   * @param differentiatePlusLicense Temporary argument to avoid breaking the editor that does not yet support Plus licenses.
   * @returns {*}
   */
  getLicenseKind(differentiatePlusLicense) {
    if (this.isPlus() && differentiatePlusLicense) {
      return this.licenseKinds.PLUS;
    } else if (this.isPro()) {
      return this.licenseKinds.PRO;
    } else if (this.isEdu()) {
      return this.licenseKinds.EDU;
    } else if (this.isFree()) {
      return this.licenseKinds.PERSONAL;
    } else if (this.isTrial()) {
      return this.licenseKinds.TRIAL;
    }

    return this.licenseKinds.UNKNOWN;
  }

  handleOldLicense() {
    return new Promise((resolve, reject) => {
      var file = getLicensePath(UnityLicenseOldFileName);
      if (fs.existsSync(file) && !fs.existsSync(this.licenseFile)) {
        fs.copy(file, this.licenseFile, (err) => {
          if (err) {
            reject(err);
          } else {
            resolve();
          }
        });
      } else {
        resolve();
      }
    });
  }

  resetUpdateFlag() {
    this.mMustUpdate = false;
    this.mMustUpdateSetByVersion = false;
  }

  submitTransaction(serialNumber) {
    let body = '';
    if (serialNumber) {
      body = `{"transaction":{"survey_answer":{"skipped":true}, "serial":{"type":"serial_number","serial_number":"${serialNumber}"}}}`;
    } else {
      body = '{"transaction":{"survey_answer":{"skipped":true}, "serial":{"type":"personal"}}}';
    }
    return axios({
      url: `${getLicenseHost()}api/transactions/${this.txID}`,
      method: 'PUT',
      headers: {
        'User-Agent': `hub ${systemInfo.hubVersion()}.cn`,
        'Content-Type': 'application/json',
        AUTHORIZATION: `Bearer ${this.getAccessToken()}`,
        Accept: 'application/json, text/plain, */*',

      },
      data: body
    }).then(response => response.data);
  }
}

module.exports = new UnityLicenseCore();
