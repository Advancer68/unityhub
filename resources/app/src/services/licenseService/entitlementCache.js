'use strict'; // required by mocha

const settings = require('../localSettings/localSettings');

// third parties
const _ = require('lodash');

class EntitlementCache {
  constructor() {
    this._maxCachedDurationInMs = this._getCacheTimeoutInMs();
    this._cachedEntitlements = {};
  }

  static get defaultCacheTimeoutInSec() { return 60; }

  static get minCacheTimeoutInSec() { return 10; }

  static get maxCacheTimeoutInSec() { return 4 * 60 * 60; } // 4 hours

  cacheResponse(requestedEntitlements, response) {
    const now = _.now();
    const entitlements = _.mapValues(response.entitlements, (value, key) => ({ entitlement: key, hasEntitlement: true, entitlementGroupId: value, responseTimestamp: now }));
    for (let i = 0; i < requestedEntitlements.length; i++) {
      const requestedEntitlement = requestedEntitlements[i];
      if (!_.has(entitlements, requestedEntitlement)) {
        entitlements[requestedEntitlement] = { entitlement: requestedEntitlement, hasEntitlement: false, entitlementGroupId: '', responseTimestamp: now };
      }
    }

    _.merge(this._cachedEntitlements, entitlements);
  }

  getCachedResponseForEntitlements(entitlementsArray) {
    const cachedEntitlements = _.values(_.pick(this._cachedEntitlements, entitlementsArray));
    if (cachedEntitlements.length !== entitlementsArray.length) {
      return null;
    }

    const result = {
      entitlements: {}
    };
    const now = _.now();
    for (let i = 0; i < cachedEntitlements.length; i++) {
      const entitlement = cachedEntitlements[i];
      if (now - entitlement.responseTimestamp > this._maxCachedDurationInMs) {
        return null;
      }
      if (entitlement.hasEntitlement === true) {
        result.entitlements[entitlement.entitlement] = entitlement.entitlementGroupId;
      }
    }

    return result;
  }

  _getCacheTimeoutInMs() {
    const timeoutFromConfig = settings.get(settings.keys.ENTITLEMENT_CACHE_TIMEOUT_IN_SEC);
    const clampedValue = _.clamp(timeoutFromConfig, EntitlementCache.minCacheTimeoutInSec, EntitlementCache.maxCacheTimeoutInSec);
    const timeout = _.isNaN(clampedValue) ? EntitlementCache.defaultCacheTimeoutInSec : clampedValue;
    return timeout * 1000;
  }
}

module.exports = EntitlementCache;
