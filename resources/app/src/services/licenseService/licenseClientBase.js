const licenseCore = require('./licenseCore');

class LicenseClientBase {
  static getDefaultLicenseInfo() {
    return {
      activated: false,
      survey: { answered: false },
      flow: '',
      transactionId: '',
      floating: '',
      valid: false,
      error: false,
      updated: false,
      returned: false,
      maintenance: false,
      initialized: false,
      mustReactivate: false,
      beta: false,
      offlineDisabled: false,
      isLicenseActionInProgress: false
    };
  }

  get licenseKinds() { return licenseCore.licenseKinds; }
}

module.exports = LicenseClientBase;
