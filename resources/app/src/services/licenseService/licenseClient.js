'use strict'; // required by mocha

const LicenseClientBase = require('./licenseClientBase');
const licenseServiceFsm = require('./licenseServiceFsm');
const licenseCore = require('./licenseCore');
const LICENSE_STATUS = require('./licenseStatus');
const windowManager = require('../../windowManager/windowManager');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const settings = require('../localSettings/localSettings');

const { fs } = require('../../fileSystem');
const logger = require('../../logger')('LicenseClient');

/* eslint-disable import/no-unresolved, no-console, no-use-before-define */
const remote = require('electron').remote;
const dialog = remote !== undefined ? require('electron').remote.dialog : require('electron').dialog;

let licenseInfo;

// user state machine handlers
const licenseStatesHandlers = new Map([
  ['error', function (error) {
    logger.warn('Handle license fsm error ', error);
    // update licenseInfo
    licenseInfo.error = true;
    licenseInfo.isLicenseActionInProgress = false;
    windowManager.broadcastContent('license.error', error);

  }],
  ['initialized', function () {
    logger.info('License initialized');

    licenseInfo.initialized = true;
    windowManager.broadcastContent('license.initialized');

  }],
  ['survey', function (survey) {
    logger.info('Set Survey value');

    licenseInfo.survey = survey;
    windowManager.broadcastContent('license.survey');

  }],
  ['licenseNew', function () {
    logger.info('New license flow started');
    // the valid means it is ready
    licenseInfo.valid = true;
    licenseInfo.updated = false;
    windowManager.broadcastContent('license.licenseNew');

  }],
  ['licenseValid', function () {
    logger.info('New license verified');
    licenseInfo.valid = true;
    licenseInfo.activated = true;
    licenseInfo.error = false;
    windowManager.broadcastContent('license.valid');
    licenseInfo.isLicenseActionInProgress = false;

  }],
  ['licenseInvalid', function () {
    licenseInfo.isLicenseActionInProgress = false;
    logger.info(`License error: ${licenseCore.licenseStatus.toString()}`);
    if (licenseCore.licenseStatus === LICENSE_STATUS.kLicenseErrorFlag_FileCompromised) {
      licenseInfo.error = true;
      logger.info('Unity license information is invalid.');
      windowManager.broadcastContent('license.invalid', 'ERROR.LICENSE.INVALID_LICENSE_INFO');
    } else if (licenseCore.licenseStatus === LICENSE_STATUS.kLicenseErrorFlag_MachineBinding1 ||
               licenseCore.licenseStatus === LICENSE_STATUS.kLicenseErrorFlag_MachineBinding2 ||
               licenseCore.licenseStatus === LICENSE_STATUS.kLicenseErrorFlag_MachineBinding4 ||
               licenseCore.licenseStatus === LICENSE_STATUS.kLicenseErrorFlag_MachineBinding5) {
      licenseInfo.error = true;
      logger.info('Machine identification is invalid for current license.');
      windowManager.broadcastContent('license.invalid', 'ERROR.LICENSE.INVALID_MACHINE_IDENTIFICATION');
    } else if (licenseCore.licenseStatus === LICENSE_STATUS.kLicenseErrorFlag_VersionMismatch) {
      licenseInfo.error = true;
      logger.info('License is not for this version of Unity.');
      windowManager.broadcastContent('license.invalid', 'ERROR.LICENSE.INVALID_UNITY_VERSION');
    } else if (licenseCore.licenseStatus === LICENSE_STATUS.kLicenseErrorFlag_LicenseExpired) {
      licenseInfo.error = true;
      logger.info('License is expired.');
      licenseInfo.valid = false;
      licenseCore.deleteLicense();
      windowManager.broadcastContent('license.invalid', 'ERROR.LICENSE.EXPIRED_LICENSE');
      return;
    }

    windowManager.broadcastContent('license.invalid');
    licenseInfo.valid = true;
  }],
  ['maintenance', function () {
    logger.info('License server maintenance');
    licenseInfo.maintenance = true;
    windowManager.broadcastContent('license.maintenance');

  }],
  ['issued', function ({ activationMethod }) {
    logger.info('New license issued');
    licenseInfo.valid = true;

    windowManager.broadcastContent('license.issued');
    licenseInfo.isLicenseActionInProgress = false;
    cloudAnalytics.addEvent({
      type: cloudAnalytics.eventTypes.LICENSE_ACTIVATE,
      msg: {
        is_survey: false,
        activation_method: activationMethod,
      },
    });

  }],
  ['returned', function () {
    logger.info('License returned');
    licenseInfo.returned = true;
    licenseCore.clearActivation();
    windowManager.broadcastContent('license.returned');
    licenseInfo.isLicenseActionInProgress = false;

  }],
  ['updated', function () {
    logger.info('License updated');
    windowManager.broadcastContent('license.updated');
    licenseInfo.updated = true;
    licenseInfo.isLicenseActionInProgress = false;
  }]
]);

var fakeLicense = true;

class LicenseClient extends LicenseClientBase {

  constructor() {
    super();

    fakeLicense = false;
    licenseInfo = LicenseClientBase.getDefaultLicenseInfo();

    for (const [eventName, handler] of licenseStatesHandlers) {
      licenseServiceFsm.on(eventName, handler);
    }
  }

  init() {
    logger.info('Init');
    return new Promise((resolve, reject) => {
      try {
        const listener = licenseServiceFsm.on('initialized', () => {
          listener.off();
          resolve();
        });
        licenseServiceFsm.init();
      } catch (e) {
        reject(e);
      }
    });
  }

  // new activation will invoke this method, the rxID will be sent from launcher
  setActivationId(rx, callback) {
    licenseServiceFsm.setActivationId(rx);
    if (callback) {
      callback(true);
    }
  }

  reset() {
    licenseCore.reset();
    licenseInfo = LicenseClientBase.getDefaultLicenseInfo();
  }

  setFakeLicense(fake) {
    fakeLicense = fake;
  }

  isLicenseValid() {
    if (settings.get(settings.keys.globalMachineSettings.LICENSING_SERVICE_BASE_URL)) return true;
    return licenseInfo.activated;
  }

  getLicenseInfo(callback) {
    // load license
    // get latest data from licenseCore
    licenseInfo.activated = true;//licenseCore.getLicenseToken().length > 0;
    logger.warn("activated = true")
    licenseInfo.flow = licenseCore.getLicenseKind();
    licenseInfo.label = licenseCore.getLicenseKind(true);
    licenseInfo.offlineDisabled = false;
    logger.warn("offlineDisabled = true")
    licenseInfo.transactionId = licenseCore.getTransactionId();

    const startDate = licenseCore.getLicenseStartDate();
    if (startDate !== undefined) {
      licenseInfo.startDate = startDate;
    }

    const updateDate = licenseCore.getLicenseUpdateDate();
    if (updateDate !== undefined) {
      licenseInfo.updateDate = updateDate;
    }

    const stopDate = licenseCore.getLicenseStopDate();
    if (stopDate !== undefined) {
      licenseInfo.stopDate = stopDate;
    }

    const displayedStopDate = licenseCore.getDisplayedLicenseStopDate();
    if (displayedStopDate !== undefined) {
      licenseInfo.displayedStopDate = displayedStopDate;
    }

    licenseInfo.canExpire = stopDate < licenseCore.getInfinityDate();

    if (fakeLicense) {
      licenseInfo.valid = true;
      licenseInfo.activated = true;
    }

    const licenseInfoString = JSON.stringify(licenseInfo);

    // Legacy support. callback argument should be removed in the future.
    if (callback !== undefined) {
      callback(undefined, licenseInfoString);
    }

    return Promise.resolve(licenseInfoString);
  }

  /**
   * This function is developed for new user onboarding
   * @returns {Promise<void>}
   */
  async createPersonalLicense() {
    let isNUO = true;
    if (this.isLicenseValid()) {
      logger.info('There is an existing valid license, skip creating a Personal license');
      return;
    }
    logger.info('No valid license found, create a Personal license');
    await this.activateNewLicense();
    licenseServiceFsm.on('licenseNew', () => {
      if (isNUO === true) {
        this.submitLicense();
        // this flag is to avoid submitting license in normal hub
        isNUO = false;
      }
    });

  }

  activateNewLicense(callback) {
    licenseServiceFsm.activateNewLicense();
    if (callback !== undefined) {
      callback(undefined, true);
    }
  }

  resetLicenseState() {
    licenseServiceFsm.reset();
  }

  // Send Return Request to server ==> TransmitLicenseFile ==> (LicenseRequestMessage ==> update/poll?cmd=3 [LICENSE_CMD_RETURN]);
  returnLicense(callback) {
    licenseServiceFsm.returnLicense();
    if (callback !== undefined) {
      callback(undefined, true);
    }
  }

  async loadLicenseLegacy() {
    const fileNames = this.chooseLicenseFileToLoad();
    return await this.loadLicense(fileNames);
  }

  chooseLicenseFileToLoad() {
    return dialog.showOpenDialog({ properties: ['openFile'] });
  }

  async loadLicense(fileNames) {
    licenseInfo.isLicenseActionInProgress = true;
    // fileNames is an array that contains all the selected
    if (fileNames === undefined) {
      return { succeeded: false };
    }
    try {
      const licenseData = await licenseCore.getLicenseFile(fileNames[0]);
      const response = await licenseServiceFsm.loadLicense(licenseData);
      if (response.errorCode) {
        licenseInfo.isLicenseActionInProgress = false;
      }
      return response;
    } catch (err) {
      logger.warn(err);
      licenseInfo.isLicenseActionInProgress = false;
      return { errorCode: 'ERROR.LICENSE.FAILED_SAVING_LICENSE_FILE', succeeded: false };
    }

  }

  saveLicense() {
    return new Promise((resolve) => {
      const name = 'Unity_lic.alf';
      const newLicenseDeviceData = licenseCore.injectSystemInfo(licenseCore.rawLicenseData(), licenseCore.systemInfo);

      const fileName = dialog.showSaveDialog({
        title: 'Save license information for offline activation.',
        defaultPath: name,
        filters: [
          { name: 'Unity Activation File', extensions: ['alf'] },
        ],
      });
      if (fileName === undefined) {
        logger.info('You didn\'t save the file');
        resolve({ succeeded: false });
        return;
      }

      // fileName is a string that contains the path and filename created in the save file dialog.
      fs.writeFile(fileName, newLicenseDeviceData, (err) => {
        if (err) {
          if (err instanceof Error && err.code === 'ENOENT') {
            logger.warn(`An error occurred creating the file ${err.message}`);
          }
          resolve({ succeeded: false, errorCode: 'ERROR.LICENSE.FAILED_SAVING_LICENSE_FILE' });
        } else {
          resolve({ succeeded: true });
        }
      });
    });
  }

  // QueryLicenseUpdate
  //   Send Update Request to server ==> TransmitLicenseFile ==> (LicenseRequestMessage ==> update/poll?cmd=2 [LICENSE_CMD_UPDATE]);
  async refreshLicense(callback) {
    licenseInfo.isLicenseActionInProgress = true;
    licenseServiceFsm.updateLicense();

    if (callback !== undefined) {
      callback(undefined, true);
    }

    await this._waitForEvent('updated');
  }

  clearErrors(callback) {
    licenseInfo.error = false;
    if (callback !== undefined) {
      callback(undefined, true);
    }
  }

  async validateSerialNumber(serialNumber) {
    try {
      return await licenseCore.submitTransaction(serialNumber);
    } catch (error) {
      logger.warn(error);
      throw error;
    }
  }

  async submitLicense(serialNumber) {
    let data;
    try {
      data = await licenseCore.submitTransaction(serialNumber);
    } catch (error) {
      logger.warn(error);
      if (error.response) {
        if (error.response.status === 503) {
          throw new Error('ERROR.LICENSE.SERVER.MAINTENANCE');
        }
      }
      throw new Error('ERROR.LICENSE.SERVER.GENERIC');
    }

    if (data.transaction && !data.transaction.rx) {
      logger.warn(data);
      licenseInfo.isLicenseActionInProgress = false;
      throw new Error('ERROR.LICENSE.CONTACT_US');
    }
    this.setActivationId(data.transaction.rx);
  }

  _waitForEvent(event) {
    return new Promise((resolve, reject) => {
      const eventListener = licenseServiceFsm.on(event, () => {
        eventListener.off();
        resolve();
      });

      const errorListener = licenseServiceFsm.on('error', () => {
        eventListener.off();
        errorListener.off();
        reject();
      });
    });
  }
}

module.exports = new LicenseClient();
