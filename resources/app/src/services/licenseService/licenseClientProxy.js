'use strict'; // required by mocha

const electron = require('electron'); // eslint-disable-line
const licenseClient = require('./licenseClient');
const IpcLicenseClient = require('./ipcLicenseClient');
const settings = require('../localSettings/localSettings');

class LicenseClientProxy {

  get fileBaseClient() { return licenseClient; }

  get ipcClient() {
    if (typeof this._ipcLicenseClient === 'undefined') {
      // Setting the discard handshake and entitlement flag temporarly to true.
      // Please see the IpcLicenseClient constructor comment to know what this flag does.
      const discardHandshakeAndEntitlementRequests = true;
      this._ipcLicenseClient = new IpcLicenseClient(discardHandshakeAndEntitlementRequests);
    }

    return this._ipcLicenseClient;
  }

  get licenseKinds() { return this._getLicenseClient().licenseKinds; }

  init(options) {
    return this._getLicenseClient().init(options);
  }

  // new activation will invoke this method, the rxID will be sent from launcher
  setActivationId(rx, callback) {
    this._getLicenseClient().setActivationId(rx, callback);
  }

  reset() {
    this._getLicenseClient().reset();
  }

  isLicenseValid() {
    return this._getLicenseClient().isLicenseValid();
  }

  getLicenseInfo(callback) {
    return this._getLicenseClient().getLicenseInfo(callback);
  }

  async createPersonalLicense() {
    return this._getLicenseClient().createPersonalLicense();
  }

  activateNewLicense(callback) {
    this._getLicenseClient().activateNewLicense(callback);
  }

  resetLicenseState() {
    this._getLicenseClient().resetLicenseState();
  }

  // Send Return Request to server ==> TransmitLicenseFile ==> (LicenseRequestMessage ==> update/poll?cmd=3 [LICENSE_CMD_RETURN]);
  returnLicense(callback) {
    this._getLicenseClient().returnLicense(callback);
  }

  loadLicenseLegacy() {
    return this._getLicenseClient().loadLicenseLegacy();
  }

  chooseLicenseFileToLoad() {
    return this._getLicenseClient().chooseLicenseFileToLoad();
  }

  async loadLicense(fileNames) {
    return this._getLicenseClient().loadLicense(fileNames);
  }

  saveLicense() {
    return this._getLicenseClient().saveLicense();
  }

  // QueryLicenseUpdate
  //   Send Update Request to server ==> TransmitLicenseFile ==> (LicenseRequestMessage ==> update/poll?cmd=2 [LICENSE_CMD_UPDATE]);
  refreshLicense(callback) {
    return this._getLicenseClient().refreshLicense(callback);
  }

  async validateSerialNumber(serialNumber) {
    return await this._getLicenseClient().validateSerialNumber(serialNumber);

  }

  async submitLicense(serialNumber) {
    return await this._getLicenseClient().submitLicense(serialNumber);
  }

  clearErrors(callback) {
    this._getLicenseClient().clearErrors(callback);
  }

  isLicenseEntitlementEnabled() {
    return (settings.get(settings.keys.ENABLE_ENTITLEMENT_LICENSING));
  }
  _getLicenseClient() {
    if (!this.isLicenseEntitlementEnabled()) {
      return this.fileBaseClient;
    }
    return this.ipcClient;
  }
}

module.exports = new LicenseClientProxy();
