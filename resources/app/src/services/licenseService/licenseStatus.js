const LICENSE_STATUS = {
  kLicenseStatus_Valid: 0x00000000,
  kLicenseStatus_New: 0x00000001,    // Issue new license.
  kLicenseStatus_Issued: 0x00000004,    // New license issued.
  kLicenseStatus_Update: 0x00000020,    // License must be updated.
  kLicenseStatus_Updated: 0x00000028,    // License is updated.
  kLicenseStatus_Return: 0x00000030,
  kLicenseStatus_Returned: 0x00000038,
  kLicenseStatus_Invalid: 0x00000040,
  kLicenseStatus_WaitForLogin: 0x00000048,
  kLicenseStatus_Unknown: 0x00000050,
  kLicenseErrorFlag_NotSpecified: 0x00000061,
  kLicenseErrorFlag_NoLicense: 0x00000062,
  kLicenseErrorFlag_FileCompromised: 0x00000063,
  kLicenseErrorFlag_MachineBinding1: 0x00000064,
  kLicenseErrorFlag_MachineBinding2: 0x00000065,
  kLicenseErrorFlag_MachineBinding4: 0x00000066,
  kLicenseErrorFlag_MachineBinding5: 0x00000067,
  kLicenseErrorFlag_Tokens: 0x00000068,
  kLicenseErrorFlag_DeveloperData: 0x00000069,
  kLicenseErrorFlag_LicenseNotReadyForUse: 0x00000070,
  kLicenseErrorFlag_LicenseExpired: 0x00000071,
  kLicenseErrorFlag_Initialization: 0x00000072,
  kLicenseErrorFlag_VersionMismatch: 0x00000073,
  kLicenseErrorFlag_AllowReactivation: 0x00000074,
  kLicenseErrorFlag_LeaseExpired: 0x00000075
};

module.exports = LICENSE_STATUS;
