const MessageFormatter = require('./messageFormatter.js');
let messageFormatter = new MessageFormatter('utf8');

// third parties
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);
chai.use(chaiAsPromised);

describe('MessageFormatter', () => {
  describe('format', () => {
    it('should format message in the intended format', () => {
      let message = {
        prop1: 'hello',
        prop2: 3712
      };
      let formattedMessage = messageFormatter.format(message);
      expect(formattedMessage).to.equal(JSON.stringify(message)+MessageFormatter.delimiter);
    });
  });
  describe('parse', () => {
    it('should parse buffer and return object message', () => {
      let message = {
        prop1: 'hello',
        prop2: 3712
      };
      let buffer = Buffer.from(JSON.stringify(message)+MessageFormatter.delimiter, 'utf8');
      let parsedMessage = messageFormatter.parse(buffer);
      expect(parsedMessage).to.deep.equal(message);
    });
    it('should accumulate buffer while no delimiter encountered and parse + return object message when delimiter finally found', () => {
      let message = {
        prop1: 'hello',
        prop2: 3712
      };
      let buffer = Buffer.from(JSON.stringify(message)+MessageFormatter.delimiter, 'utf8');
      let parsedMessage = messageFormatter.parse(buffer.slice(0, buffer.length / 2));
      expect(parsedMessage).to.be.undefined;
      parsedMessage = messageFormatter.parse(buffer.slice(buffer.length / 2), buffer.length);
      expect(parsedMessage).to.deep.equal(message);
    });
  });
});