const IpcMessenger = require('./ipcMessenger');
const singletonIPC = require('node-ipc');
const MessageFormatter = require('./messageFormatter.js');
let messageFormatter = new MessageFormatter('utf8');

// third parties
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');

chai.use(sinonChai);
chai.use(chaiAsPromised);

describe('IpcMessenger', () => {
  let sandbox;
  let ipc;
  let pipeName;
  let messenger;

  describe('with server running',() => {

    beforeEach(() => {
      sandbox = sinon.sandbox.create();
      pipeName = Math.random().toString(36).substring(2, 15);
      ipc = new singletonIPC.IPC();
      ipc.config.retry = 100;
      ipc.config.rawBuffer = true;
      ipc.config.encoding = 'utf8';
      ipc.config.silent = true;

      ipc.serve(pipeName);
      ipc.server.start();
    });

    afterEach(() => {
      sandbox.restore();
      messenger.disconnect();
      ipc.server.stop();
    });

    describe('when discarding handshake', () => {
      beforeEach(() => {
        messenger = new IpcMessenger(true);
      });

      describe('init', () => {
        it('should not send handshake message when connecting to server', async () => {
          let receivedMessages = [];
          ipc.server.on('data', buffer => {
            receivedMessages.push(messageFormatter.parse(buffer));
          });

          let initPromise = messenger.init(pipeName);
          await initPromise;
          expect(initPromise).to.be.resolved;
          expect(receivedMessages).to.be.empty;
          expect(messenger.isConnected).to.be.true;
        });
      });
    });

    describe('when not discarding handshake', () => {

      beforeEach(() => {
        messenger = new IpcMessenger();
      });

      describe('init', () => {
        it('should send handshake message when connecting to server', () => {
          const onDataPromise = new Promise(resolve => {
            ipc.server.on('data', buffer => {
              resolve(messageFormatter.parse(buffer));
            });
          });
          messenger.init(pipeName);
          return expect(onDataPromise).to.eventually.deep.equal({
            messageType: 'HandshakeRequest',
            ProtocolVersion: '1.0',
            UserAgent: 'Unity-Hub'
          });
        });

        it('should be rejected when handshake failed', () => {
          ipc.server.on('data', (buffer, s) => {
            ipc.server.emit(s, messageFormatter.format({responseCode: 505}))
          });
          return expect(messenger.init(pipeName)).to.eventually.be.rejectedWith(IpcMessenger.errorKinds.handshakeFailed);
        });
      });

      describe('sendMessage', () => {
        beforeEach(done => {
          sandbox.stub(messenger, '_doHandshake').resolves();
          messenger.init(pipeName).then(() => {
              done();
            },
            e => {
              done(e);
            });
        });

        it('should send message and return a promise that successfully executes', () => {
          let expectedResponse = {responseCode: 200, hello: 42};

          ipc.server.on('data', (buffer, client) => {
            ipc.server.emit(client, messageFormatter.format(expectedResponse));
          });

          expect(messenger.sendMessage({hello: 42})).to.eventually.deep.equal(expectedResponse);
        });

        it('should send message and return a promise that executes reject callback when ResponseCode is not 200', () => {
          let expectedResponse = {responseCode: 500, responseStatus: 'Something went wrong', hello: 42};

          ipc.server.on('data', (buffer, client) => {
            ipc.server.emit(client, messageFormatter.format(expectedResponse));
          });

          expect(messenger.sendMessage({hello: 42})).to.eventually.deep.equal(new Error(`unexpected response code: ${expectedResponse.responseCode}, status: ${expectedResponse.responseStatus}`));
        });
      });

    });
  });
  describe('without server running', () => {
    describe('init', () => {
      it('should timeout', () => {
        messenger = new IpcMessenger();
        const initPromise = messenger.init('some-pipe-boi');
        return expect(initPromise).to.eventually.be.rejectedWith(IpcMessenger.errorKinds.endpointDisconnected);
      });
    });
    describe('send message', () => {
      it('should return an error when messenger is not connected to server', () => {
        messenger = new IpcMessenger();
        return expect(messenger.sendMessage({hello: 42})).to.eventually.deep.equal(new Error('IPCMessenger not connected to license client'));
      });
    });
  });
});
