const logger = require('../../../logger')('MessageFormatter');

class MessageFormatter {
  static get delimiter() { return '\f'; }

  constructor(configurationEncoding) {
    this._encoding = configurationEncoding;
    this._encodingDelimiterCode = Buffer.from(MessageFormatter.delimiter, this._encoding)[0];
    this._accumulator = Buffer.alloc(0);
  }

  format(messageObject) {
    return JSON.stringify(messageObject) + MessageFormatter.delimiter;
  }

  parse(buffer) {
    this._accumulator = Buffer.concat([this._accumulator, buffer]);
    const delimiterPos = this._accumulator.indexOf(this._encodingDelimiterCode);
    if (delimiterPos === -1) {
      logger.debug('Received partial message, waiting for the rest of the message to parse it');
      return undefined;
    }
    const messageString = this._accumulator.toString(this._encoding, 0, delimiterPos);
    const messageObject = JSON.parse(messageString);

    if (delimiterPos < this._accumulator.length - 1) {
      logger.warn('Found data after delimiter, this data should not be here and will be ignored');
    }

    this._accumulator = Buffer.alloc(0);
    return messageObject;
  }
}

module.exports = MessageFormatter;
