const EventEmitter = require('events');
const singletonIPC = require('node-ipc');
const MessageFormatter = require('./messageFormatter');
const logger = require('../../../logger')('IpcMessenger');
const _ = require('lodash');
const os = require('os');

const platform = os.platform();

class IpcMessenger extends EventEmitter {

  // This discard handshake flag is temporary.
  // It allows the hub to connect to the license client
  // without requiring a floating license acquisition.
  // It will be removed when an explicit
  // acquire_license message will be implemented
  constructor(discardHandshake) {
    super();

    this._discardHandshake = discardHandshake;
    this._ipc = new singletonIPC.IPC();
    this._ipc.config.retry = 100;
    this._ipc.config.maxRetries = 5;
    this._ipc.config.rawBuffer = true;
    this._ipc.config.encoding = 'utf8';
    this._ipc.config.silent = true;

    this._currentRequestCallback = undefined;
    this._connected = false;
    this._queue = Promise.resolve();
    this._messageFormatter = new MessageFormatter(this._ipc.config.encoding);
  }

  init(pipeName, useExtendedTimeout) {
    if (useExtendedTimeout === true) {
      this._ipc.config.retry = 100;
      this._ipc.config.maxRetries = 50;
    } else {
      this._ipc.config.retry = 100;
      this._ipc.config.maxRetries = 5;
    }

    const pipePath = platform === 'win32' ? pipeName : `/tmp/${pipeName}.sock`;

    return new Promise((resolve, reject) => {
      this._ipc.connectTo(
        'licensingClient',
        pipePath,
        () => {
          this._ipc.of.licensingClient.on('connect', () => this.handleSocketConnected(resolve, reject));
          this._ipc.of.licensingClient.on('data', b => this.handleSocketData(b));
          this._ipc.of.licensingClient.on('error', e => this.handleSocketError(e));
          this._ipc.of.licensingClient.on('destroy', () => this.handleDestroyedSocket(reject));
          this._ipc.of.licensingClient.on('end', () => this.disconnect());
        }
      );
    });
  }

  static get errorKinds() {
    return {
      endpointDisconnected: 'endpoint disconnected',
      endpointPermissionDenied: 'endpoint permission denied',
      handshakeFailed: 'handshake step failed'
    };
  }

  static get knownErrorCodes() {
    return {
      permissionDenied: 'EPERM'
    };
  }

  get isConnected() {
    return this._connected;
  }

  handleSocketConnected(resolve, reject) {
    this._connected = true;
    this._lastError = null;

    if (this._discardHandshake) {
      logger.info('Socket connected, resolving init without handshake');
      resolve();
    } else {
      this._doHandshake()
        .then(handshakeResponse => {
          if (handshakeResponse instanceof Error) {
            logger.warn('Handshake failed');
            reject(IpcMessenger.errorKinds.handshakeFailed);
          } else {
            logger.info('Handshake successful with IPC licensing client');
            resolve();
          }
        },
        error => {
          logger.warn(`Handshake failed. Error handled while waiting for handshake response with IPC licensing client. Error: ${error}`);
          reject(error);
        });
    }
  }

  handleSocketData(buffer) {
    logger.debug(`Received buffer of ${buffer.length} bytes from IPC Licensing client`);
    try {
      const responseObject = this._messageFormatter.parse(buffer);
      if (responseObject === undefined) {
        return;
      }
      if (responseObject.responseCode !== 200) {
        const errorMessage = `Received message with unexpected response code: ${responseObject.responseCode}, status: ${responseObject.responseStatus}`;
        logger.warn(errorMessage);
        this._onReceivedErrorResponse(new Error(errorMessage));
        return;
      }
      logger.debug(`Successfully received and parsed message of type ${responseObject.messageType}`);
      this._onReceivedSuccessfulResponse(responseObject);
    } catch (ex) {
      logger.warn(`Error while handling socket data: ${ex}`);
      this._onReceivedErrorResponse(ex);
    }
  }

  handleSocketError(error) {
    this._lastError = error;
    logger.warn('error happened in ipc messenger', error);
  }

  handleDestroyedSocket(reject) {
    this._connected = false;
    const message = 'IPC socket destroyed, licensing client is unreachable.';
    logger.warn(message);
    if (!_.isUndefined(this._lastError) && _.has(this._lastError, 'code') && this._lastError.code === IpcMessenger.knownErrorCodes.permissionDenied) {
      reject(IpcMessenger.errorKinds.endpointPermissionDenied);
    } else {
      reject(IpcMessenger.errorKinds.endpointDisconnected);
    }
    this.emit('connectionLost');
  }

  sendMessage(message) {
    if (!this._connected) {
      return Promise.resolve(new Error('IPCMessenger not connected to license client'));
    }

    this._queue = this._queue.then(() => new Promise((resolve) => {
      try {
        logger.debug(`Sending message of type ${message.messageType} to IPC Licensing Client`);
        this._currentRequestCallback = (response) => {
          if (response instanceof Error) {
            logger.warn(response);
          }
          resolve(response);
        };

        this._ipc.of.licensingClient.emit(this._messageFormatter.format(message));
      } catch (ex) {
        resolve(ex);
      }
    }));
    return this._queue;
  }

  disconnect() {
    if (!this._connected) {
      return;
    }

    this._ipc.disconnect('licensingClient');
    this._connected = false;
  }

  _doHandshake() {
    return this.sendMessage({
      messageType: 'HandshakeRequest',
      ProtocolVersion: '1.0',
      UserAgent: 'Unity-Hub'
    });
  }

  _onReceivedSuccessfulResponse(response) {
    if (this._currentRequestCallback !== undefined) {
      this._currentRequestCallback(response);
    }
  }

  _onReceivedErrorResponse(error) {
    if (this._currentRequestCallback !== undefined) {
      this._currentRequestCallback(error);
    }
  }
}

module.exports = IpcMessenger;
