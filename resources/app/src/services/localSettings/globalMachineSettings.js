const { fs } = require('../../fileSystem');
const path = require('path');
const util = require('util');
const logger = require('../../logger')('GlobalMachineSettings');

const CONFIG_FILE_NAME = 'services-config.json';

class GlobalMachineSettings {
  constructor() {
    this._settings = {};

    this.keys = {
      LICENSING_SERVICE_BASE_URL: 'licensingServiceBaseUrl'
    };
  }

  init() {
    // Get the config folder
    const globalConfigFolder = this.getConfigFolder();

    // Store the folder into config
    this._settings.globalConfigFolder = globalConfigFolder;

    // Try to read config file (without throwing)
    const configPath = path.join(globalConfigFolder, CONFIG_FILE_NAME);
    return fs.readJson(configPath, { throws: false })
      .then((storedSettings) => {
        // Override current settings with stored settings
        logger.info(`Configurations from services-config ${util.inspect(storedSettings)}`);
        this._settings = Object.assign({},
          this._settings, storedSettings);
      }).catch(error => {
        if (error.code === 'ENOENT') {
          logger.debug(`Global config file not found: ${configPath}`);
        } else {
          logger.error(`An error occurred while loading the global config file (${configPath}): ${error}`);
        }
      });
  }

  get settings() {
    return this._settings;
  }

  /**
   * Returns the path where the config file should be stored.
   * This should be a central path for all users.
   *
   * @return {String} The path where the config file should be stored.
   */
  getConfigFolder() {
    if (/^win/.test(process.platform)) {
      return path.join(process.env.ALLUSERSPROFILE, 'Unity', 'config');
    }
    if (process.platform === 'darwin') {
      return path.join('/', 'Library', 'Application Support', 'Unity', 'config');
    }
    if (process.platform === 'linux') {
      return path.join('/', 'usr', 'share', 'unity3d', 'config');
    }
    throw new Error(
      `Unable to resolve config folder for platform: ${process.platform}`
    );
  }
}

module.exports = new GlobalMachineSettings();
