module.exports = {
  cloudEnvironment: 'production',
  accessTokenIntervalPeriod: 5000, // 5 seconds
  servicesUrlInterval: 900000, // 15 minutes in ms
  hubConfigInterval: 3600000, // 1hr in ms
  releasesRefreshInterval: 3600000, // 1hr in ms
  recentProjectInterval: 60000, // 1 min
  downloadClusterInterval: 1000,
  downloadEditorInterval: 1000,
  refreshTokenValidity: 2592000000, // 30 days in ms
  checkForUpdateInterval: 43200000, // 12 hrs in ms
  sendAnalyticsEventsInterval: 60000, // 1 min
  maxSizeForAnalyticsQueue: 500,
  downloadRequestTimeout: 30000, // 30 sec
};
