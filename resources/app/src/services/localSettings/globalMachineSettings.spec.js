// internal
const globalMachineSettings = require('./globalMachineSettings');
const { fs } = require('../../fileSystem');

// common
const path = require('path');

// tests
const expect = require('chai').expect;
const sinon = require('sinon');

describe('globalMachineSettings', () => {
  let sandbox;
  beforeEach(() => {
    sandbox = sinon.sandbox.create();
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('getConfigFolder function', () => {
    describe('on windows', () => {
      beforeEach(() => {
        sandbox.stub(process, 'platform').value('win32');
        sandbox.stub(process, 'env').value({ALLUSERSPROFILE: path.join('C:', 'ProgramData'),})
      });

      it('should return the library folder path', () => {
        expect(globalMachineSettings.getConfigFolder()).to.equal(
          path.join('C:', 'ProgramData', 'Unity', 'config')
        )
      })
    });

    describe('on linux', () => {
      beforeEach(() => {
        sandbox.stub(process, 'platform').value('linux');
        sandbox.stub(process, 'env').value({HOME: 'home',})
      });

      it('should return the library folder for $HOME path', () => {
        expect(globalMachineSettings.getConfigFolder()).to.equal(
          path.join('/', 'usr', 'share', 'unity3d', 'config')
        )
      })
    });

    describe('on darwin', () => {
      beforeEach(() => {
        sandbox.stub(process, 'platform').value('darwin');
      });

      it('should return the library folder path', () => {
        expect(globalMachineSettings.getConfigFolder()).to.equal(
          path.join('/', 'Library', 'Application Support', 'Unity', 'config')
        )
      })
    });

    describe('on unknown platform', () => {
      const platform = 'unknown';
      beforeEach(() => {
        sandbox.stub(process, 'platform').value(platform);
      });

      it('should throw an error', () => {
        expect(() => globalMachineSettings.getConfigFolder()).to.throw(
          Error,
          `Unable to resolve config folder for platform: ${platform}`
        )
      })
    })
  });

  describe('init function', () => {
    let tmpPath, fsMock;
    beforeEach(() => {
      tmpPath = 'foo';
      fsMock = {};
      
      sandbox.stub(globalMachineSettings, 'getConfigFolder').returns(tmpPath);
      sandbox.stub(fs, 'readJson').callsFake(jsonPath => new Promise((resolve, reject) => {
        const config = fsMock[jsonPath];
        
        if (config.code) {
          return reject(config);
        }
  
        return resolve(config);
      }));
    });

    function saveConfig(config) {
      const filePath = path.join(tmpPath, 'services-config.json');
      fsMock[filePath] = config;
    }

    describe('when config file not present', () => {
      it('should only have globalConfigFolder setting defined', async () => {
        await globalMachineSettings.init();
        expect(globalMachineSettings.settings).to.deep.equal({globalConfigFolder: tmpPath});
      });
    });

    describe('when config file is malformed json', () => {
      it('should only have globalConfigFolder setting defined', async () => {
        saveConfig({code: 'INVALID'});
        await globalMachineSettings.init();
        expect(globalMachineSettings.settings).to.deep.equal({globalConfigFolder: tmpPath});
      });
    });

    describe('when config file is empty json', () => {
      it('should only have globalConfigFolder setting defined', async () => {
        saveConfig({code: 'ENOENT'});
        await globalMachineSettings.init();
        expect(globalMachineSettings.settings).to.deep.equal({globalConfigFolder: tmpPath});
      });
    });

    describe('when config file has custom property', () => {
      beforeEach(() => {
        saveConfig({custom: 'property'});
      });

      it('should contain the custom config property', async () => {
        await globalMachineSettings.init();
        expect(globalMachineSettings.settings['custom']).to.equal('property');
      });
    });
  });
});


