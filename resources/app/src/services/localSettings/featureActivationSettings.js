const localStorage = require('electron-json-storage');
const promisify = require('es6-promisify');
const logger = require('../../logger')('FeatureActivationSettings');

const SETTINGS_FILE_NAME = 'settings';

const settings = Symbol();

class FeatureActivationSettings {
  constructor() {
    this[settings] = {};
    
    this.keys = {
    };
  }
  
  init() {
    return promisify(localStorage.get)(SETTINGS_FILE_NAME).then((storedSettings) => {
      this[settings] = storedSettings;
    }).catch((err) => {
      logger.warn(err);
      this[settings] = {};
    });
  }
  
  get settings() { return this[settings]; }
}

module.exports = new FeatureActivationSettings();
