'use strict';

const electronSettings = require('electron-settings');
const defaultSettings = require('./defaultSettings.js');
const featureActivationSettings = require('./featureActivationSettings');
const globalMachineSettings = require('./globalMachineSettings');
const argv = require('yargs').argv;
const _ = require('lodash');
const logger = require('../../logger')('HubSettings');

let settings;

/**
 * See package electron-settings for API documentation: https://github.com/nathanbuchar/electron-settings/wiki/API-documentation
 */
class HubSettings {

  constructor() {
    this.cloudEnvironments = {
      DEV: 'dev',
      STAGING: 'staging',
      PRODUCTION: 'production',
    };

    this.keys = {
      CLOUD_ENVIRONMENT: 'cloudEnvironment',
      ACCESS_TOKEN_INTERVAL_PERIOD: 'accessTokenIntervalPeriod',
      SERVICES_URL_INTERVAL: 'servicesUrlInterval',
      HUB_CONFIG_REFRESH_INTERVAL: 'hubConfigInterval',
      RELEASES_REFRESH_INTERVAL: 'releasesRefreshInterval',
      RECENT_PROJECT_INTERVAL: 'recentProjectInterval',
      DL_CLUSTER_INTERVAL: 'downloadClusterInterval',
      DL_EDITOR_INTERVAL: 'downloadEditorInterval',
      REFRESH_TOKEN_VALIDITY: 'refreshTokenValidity',
      CHECK_FOR_UPDATE_INTERVAL: 'checkForUpdateInterval',
      SEND_ANALYTICS_EVENTS_INTERVAL: 'sendAnalyticsEventsInterval',
      MAX_SIZE_FOR_ANALYTICS_QUEUE: 'maxSizeForAnalyticsQueue',
      DOWNLOAD_REQUEST_TIMEOUT: 'downloadRequestTimeout',
      SERVICE_CONFIG_BASE_URL: 'servicesConfigBaseUrl',
      GLOBAL_CONFIG_FOLDER: 'globalConfigFolder',
      DISABLE_CLOUD_PROJECTS: 'hubDisableCloudProjects',
      DISABLE_SIGNIN: 'hubDisableSignin',
      DISABLE_ANALYTICS: 'hubDisableAnalytics',
      DISABLE_LEARN: 'hubDisableLearn',
      DISABLE_COMMUNITY: 'hubDisableCommunity',
      DISABLE_AUTO_UPDATE: 'hubDisableAutoUpdate',
      DISABLE_PE: 'hubDisablePersonalLicense',
      DISABLE_VS: 'hubDisableVisualStudioDownload',
      CONNECTIVITY_CONFIG: 'connectivityConfig',
      DISABLE_ELEVATE: 'hubDisableElevate',
      UPDATE_SERVER_PATH: 'hubUpdateServerPath',
      BETA_CONFIG_PATH: 'hubBetaConfigPath',
      ENABLE_ENTITLEMENT_LICENSING: 'enableEntitlementLicensing',
      ENTITLEMENT_CACHE_TIMEOUT_IN_SEC: 'entitlementCacheTimeoutInSeconds',
      MACHINE_WIDE_SECONDARY_INSTALL_LOCATION: 'machineWideSecondaryInstallLocation',
      LICENSE_CLIENT_APPLICATION_PATH: 'licenseClientApplicationPath',
      USE_LEGACY_LICENSING_CLIENT_PIPE_NAME: 'useLegacyLicensingClientPipeName',
      NEW_USER_ONBOARDING_CONFIG: 'newUserOnboarding',
      BETA_FEEDBACK_URL: 'betaFeedbackUrl',
      DYNAMIC_TEMPLATES_CONFIG_PATH: 'dynamicTemplatesConfigPath'
    };

    this.keys.featureActivation = featureActivationSettings.keys;
    this.keys.globalMachineSettings = globalMachineSettings.keys;
  }

  init() {
    return Promise.all([featureActivationSettings.init(), globalMachineSettings.init()])
      .then(() => {
        const allowedCloudEnvironments = _.values(this.cloudEnvironments);
        const commandLineSettings = getCommandLineSettings(allowedCloudEnvironments);
        const persistedSettings = electronSettings.getAll();
        settings = Object.assign({},
          defaultSettings,
          featureActivationSettings.settings,
          globalMachineSettings.settings,
          persistedSettings,
          commandLineSettings);

        logger.info(settings);
      });
  }

  get(key) {
    return settings[key];
  }

  set(key, value, isPersisted) {
    settings[key] = value;

    if (isPersisted) {
      return new Promise((resolve) => {
        electronSettings.set(key, value);
        resolve();
      });
    }

    return Promise.resolve();
  }
}

function getCommandLineSettings(allowedCloudEnvironments) {
  const cliSettings = {};

  if (argv.servicesUrlInterval) {
    cliSettings.servicesUrlInterval = argv.servicesUrlInterval;
  }

  if (argv.cloudEnvironment && _.includes(allowedCloudEnvironments, argv.cloudEnvironment)) {
    cliSettings.cloudEnvironment = argv.cloudEnvironment;
  }

  return cliSettings;
}

module.exports = new HubSettings();
