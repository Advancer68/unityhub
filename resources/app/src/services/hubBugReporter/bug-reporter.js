const logger = require('../../logger')('BugReporter');
const path = require('path');
const { exec, spawn } = require('child_process');
const { app } = require('electron');

class BugReporter {

  /**
   * A function to start the bug reporter window.
   * It is in the app level to make sure we can start it even if the
   * init chain failed.
   */
  showBugReporter() {
    logger.info('spawn child process for bug reporter');

    let command = process.execPath;
    const param = [];
    if (command.indexOf('electron') !== -1) {
      // during development
      param.push(`${path.resolve(__dirname, '..', '..', 'index.js')}`, '--bugReporter', '--inspect', `${app.argv.debug ? '--debugMode' : ''}`);

      const proc = spawn(command, param);
      proc.on('error', (err) => {
        if (err) {
          logger.warn(err);
        }
      });
    } else {
      // during production
      command = `"${command}" -- --bugReporter ${app.argv.debug ? '--debugMode' : ''}`;

      exec(command, (err) => {
        if (err) {
          logger.warn(err);
        }
      });
    }
  }
}

module.exports = new BugReporter();
