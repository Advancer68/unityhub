const path = require('path');
const { UnityIPCServer, UnityIPCClient } = require('../../common/common');
const logger = require('../../logger')('HubIPCService');
const IpcControllerHelper = require('./ipcControllerHelper');
const hubIPCState = require('./hubIPCState');
const windowManager = require('../../windowManager/windowManager');

const ipcServerName = 'hubIPCService';
const controllersPath = path.join(__dirname, 'controllers');

class HubIPCService {
  
  constructor() {
    this.state = hubIPCState;
    this.ipcServer = new UnityIPCServer(ipcServerName);
    this.ipcControllerHelper = new IpcControllerHelper(this.ipcServer, this.state);
    this.controllers = this.ipcControllerHelper.getAllControllers(controllersPath);
  }

  async start() {
    logger.info('Init');
    await this.ipcServer.startIPCServer();
    this.ipcServer.on('socket.disconnected', this.onSocketDisconnect.bind(this));
    this.ipcControllerHelper.bindControllers(this.controllers);
  }

  onSocketDisconnect(socket, destroyedSocketID) {
    logger.info(`socket has disconnected: ${socket} , ${destroyedSocketID}!`);
    if (this.state.modalEditor === socket) {
      this.state.clearModalEditor();
      windowManager.mainWindow.hide();
    }
  }

  stop() {
    this.ipcServer.closeSockets();
  }

  static async connect() {
    const ipcClient = new UnityIPCClient(ipcServerName);
    await ipcClient.connect();
    await this._waitForConnection(ipcClient);
    return ipcClient;
  }

  static _waitForConnection(ipcClient) {
    return new Promise(resolve => {
      ipcClient.on('connect', () => resolve());
    });
  }
}

module.exports = HubIPCService;
