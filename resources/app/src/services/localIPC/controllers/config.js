const BaseController = require('../baseController');
const CloudConfig = require('../../cloudConfig/cloudConfig');

class ConfigController extends BaseController {
  
  get routes() {
    return {
      'config:get': this.getConfig,
      'config:get-default': this.getDefaultUrls,
      'config:get-urls': this.getUrls
    };
  }
  
  /**
   * Returns the urls for the current environment.
   */
  getConfig(data, socket, eventName) {
    this.server.emit(socket, eventName, CloudConfig.urls);
  }
  
  /**
   * Returns the default urls for the specified environment
   * @param {String} data.env The environment for which to get the urls.
   */
  getDefaultUrls(data, socket, eventName) {
    const config = CloudConfig.getDefaultUrls(data.env);
    this.server.emit(socket, eventName, config);
  }
  
  /**
   * Returns the urls for the current environment. Please use getConfig.
   * @deprecated
   */
  getUrls(data, socket, eventName) {
    this.server.emit(socket, eventName, CloudConfig.urls);
  }
}

module.exports = ConfigController;
