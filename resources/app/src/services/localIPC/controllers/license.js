const BaseController = require('../baseController');
const licenseClient = require('../../licenseService/licenseClientProxy');

class LicenseController extends BaseController {
  
  get routes() {
    return {
      'license:info': this.getInfo,
      'license:activate': this.activate,
      'license:return': this.returnLicense,
      'license:refresh': this.refresh,
      'license:load': this.load,
      'license:save': this.save,
      'license:clear-errors': this.clearErrors
    };
  }
  
  get events() {
    return {
      'license.change': this.onLicenseChange
    };
  }
  
  /**
   * Get the current local license information.
   */
  getInfo(data, socket, eventName) {
    licenseClient.getLicenseInfo((err, licenseData) => {
      this.server.emit(socket, eventName, licenseData);
    });
  }
  
  /**
   * Start new activation.
   */
  activate(data, socket, eventName) {
    licenseClient.activateNewLicense((err, succeeded) => {
      this.server.emit(socket, eventName, succeeded);
    });
  }
  
  /**
   * Return a local license file.
   */
  returnLicense(data, socket, eventName) {
    licenseClient.returnLicense(() => {
      this.server.emit(socket, eventName, '');
    });
  }
  
  /**
   * Refresh a local license file
   */
  refresh(data, socket, eventName) {
    licenseClient.refreshLicense((err, succeeded) => {
      this.server.emit(socket, eventName, succeeded);
    });
  }
  
  /**
   * Load a local license file
   * */
  async load(data, socket, eventName) {
    const { succeeded } = await licenseClient.loadLicenseLegacy();
    this.server.emit(socket, eventName, succeeded);
  }
  
  /**
   * Save a local license file
   */
  async save(data, socket, eventName) {
    const { succeeded } = await licenseClient.saveLicense();
    this.server.emit(socket, eventName, succeeded);
  }
  
  /**
   * Clear existing license errors
   */
  clearErrors(data, socket, eventName) {
    licenseClient.clearErrors(() => {
      this.server.emit(socket, eventName, '');
    });
  }
  
  /**
   * Handler for license change postal event.
   */
  onLicenseChange(data, eventName, socket) {
    if (socket) {
      this.server.emit(socket, eventName, data);
    } else {
      this.server.emitAll(eventName, data);
    }
  }
}

module.exports = LicenseController;
