const BaseController = require('../baseController');
const localProject = require('../../localProject/localProject');

class LocalProjectController extends BaseController {
  get routes() {
    return {
      'project:get-recent': this.getRecent,
      'project:get-data': this.getProjectData,
      'project:open': this.openProject,
      'project:set-editor-args': this.setEditorArgs
    };
  }
  
  get events() {
    return {
      'project.opened': this._forwardPostalEventToModalEditor,
      'project.create': this._forwardPostalEventToModalEditor,
      'project.createTemp': this._forwardPostalEventToModalEditor,
      'project.openedFromCloud': this._forwardPostalEventToModalEditor
    };
  }
  
  /**
   * Gets the recent projects.
   */
  getRecent(data, socket, eventName) {
    localProject.getRecentProjects().then(recentProjects => {
      this.server.emit(socket, eventName, recentProjects);
    });
  }
  
  /**
   * Gets project data at given path.
   * @param {String} data.path The project's path.
   */
  async getProjectData(data, socket, eventName) {
    const projectData = await localProject.getProjectData(data.path);
    this.server.emit(socket, eventName, projectData);
  }
  
  /**
   * Opens a project from the given path.
   * @param {String} data.path The project's path.
   */
  async openProject(data, socket, eventName) {
    let response;
    if (data.path) {
      response = await localProject.openProject(data.path, undefined, { source: 'localIPC' });
    } else {
      response = await localProject.showOpenOtherDialog('localIPC');
    }
    
    this.server.emit(socket, eventName, response);
  }

  _forwardPostalEventToModalEditor(data, eventName) {
    if (!this.state.modalEditor) return;
    this.server.emit(this.state.modalEditor, eventName, data);
  }
}

module.exports = LocalProjectController;
