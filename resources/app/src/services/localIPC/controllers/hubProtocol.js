const BaseController = require('../baseController');
const logger = require('../../../logger')('IPCServer:HubProtocol');
const unityHubProtocolHandler = require('../../../unityhubProtocolHandler');

class HubProtocolController extends BaseController {

  get routes() {
    return {
      'hub-protocol:handle-url': this.handleUrl
    };
  }

  /**
   * Consumes url with custom Hub protocol.
   * @param {String} data.url The url to handle
   */
  handleUrl(data, socket) {
    const url = data.url;
    logger.info(`Handling url ${url}`);
    unityHubProtocolHandler.handleUrl(url, socket)
      .then(() => {
        this.server.emit(socket, 'hub-protocol:url-handled');
      });
  }
}

module.exports = HubProtocolController;
