const BaseController = require('../baseController');
const UnityAuth = require('../../localAuth/auth');

class AuthController extends BaseController {

  get routes() {
    return {
      'connectInfo:get': this.getConnectInfo,
      'userInfo:get': this.getUserInfo,
      'orgInfo:get': this.getOrganizationsInfo,
      'auth:logout': this.logout
    };
  }

  get events() {
    return {
      'userInfo.changed': this.onUserInfoChanged,
      'connectInfo.changed': this.onConnectInfoChanged
    };
  }

  /**
   * Gets the connection info from the cloudCore Web Service.
   */
  getConnectInfo(data, socket) {
    UnityAuth.getConnectInfo().then((connectInfo) => {
      this.server.emit(socket, 'connectInfo:changed', JSON.parse(connectInfo));
    });
  }

  /**
   * Gets the user info from the cloudCore Web Service.
   */
  getUserInfo(data, socket) {
    UnityAuth.getUserInfo().then((userInfo) => {
      this.server.emit(socket, 'userInfo:changed', JSON.parse(userInfo));
    });
  }

  /**
   * Gets the user organizations list from the cloudCore Web Service.
   */
  getOrganizationsInfo(data, socket) {
    UnityAuth.getOrganizations().then((orgInfo) => {
      this.server.emit(socket, 'orgInfo:changed', orgInfo);
    });
  }

  /**
   * Logs out the user.
   */
  logout() {
    UnityAuth.logout();
  }

  /**
   * Handler for user info change postal event.
   * @param data.userInfo
   */
  onUserInfoChanged(data) {
    this.server.emitAll('userInfo:changed', data.userInfo);
  }

  /**
   * Handler for connect info change postal event.
   * @param data.connectInfo
   */
  onConnectInfoChanged(data) {
    this.server.emitAll('connectInfo:changed', data.connectInfo);
  }
}

module.exports = AuthController;
