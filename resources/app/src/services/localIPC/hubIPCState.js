let modalEditor;
let sessionId;
let editorVersion;
let editorLocation;

class HubIPCState {
  
  /**
   * @returns {NodeIPC.Socket} The socket corresponding to the modalEditor's IPC connection.
   */
  get modalEditor() { return modalEditor; }
  
  /**
   * @param {NodeIPC.Socket} socket The socket corresponding to the modalEditor's IPC connection.
   */
  set modalEditor(socket) { modalEditor = socket; }
  
  /**
   * @returns {} The socket corresponding to the modalEditor's IPC connection.
   */
  get sessionId() { return sessionId; }
  
  /**
   * @param {NodeIPC.Socket} socket The socket corresponding to the modalEditor's IPC connection.
   */
  set sessionId(id) { sessionId = id; }
  
  /**
   * @returns {String} The current editor's version.
   */
  get editorVersion() { return editorVersion; }
  
  /**
   * @param {String} version The current editor's version.
   */
  set editorVersion(version) { editorVersion = version; }
  
  /**
   * @returns {String} The current editor's location.
   */
  get editorLocation() { return editorLocation; }
  
  /**
   * @param {String} location The current editor's location.
   */
  set editorLocation(location) { editorLocation = location; }
  
  clearModalEditor() {
    this.modalEditor = undefined;
    this.sessionId = undefined;
    this.editorVersion = undefined;
    this.editorLocation = undefined;
  }
}

module.exports = new HubIPCState();
