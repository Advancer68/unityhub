class BaseController {
  get routes() { return {}; }
  get events() { return {}; }

  constructor(server, state) {
    this.server = server;
    this.state = state;
  }
}

module.exports = BaseController;
