const { app } = require('electron');
const promisify = require('es6-promisify');
const localStorageAsync = require('electron-json-storage');
const i18nHelper = require('../../i18nHelper');

const localStorage = {
  get: promisify(localStorageAsync.get, localStorageAsync),
  set: promisify(localStorageAsync.set, localStorageAsync),
};

const OPEN_OPTIONS_STORAGE_KEY = 'open-options-config';
const keys = {
  enable: 'enable',
  disable: 'disable',
};

class OpenOptions {

  getAvailableSettings() {
    return [
      { name: i18nHelper.i18n.translate('OPEN_OPTIONS.ENABLE'), key: keys.enable },
      { name: i18nHelper.i18n.translate('OPEN_OPTIONS.DISABLE'), key: keys.disable },
    ];
  }

  async getCurrentSetting() {
    const setting = await localStorage.get(OPEN_OPTIONS_STORAGE_KEY);
    return (setting && setting.value) || keys.enable;
  }

  async setSetting(value) {
    app.setLoginItemSettings({
      openAtLogin: value === keys.enable,
    });

    return await localStorage.set(OPEN_OPTIONS_STORAGE_KEY, { value });
  }

  async openAtLoginEnabled() {
    const setting = await this.getCurrentSetting();
    return setting === keys.enable;
  }
}

module.exports = new OpenOptions();
