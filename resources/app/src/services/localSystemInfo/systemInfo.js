const logger = require('../../logger')('SystemInfo');
const isAdmin = require('is-admin');
const os = require('os');
const { exec } = require('child_process');
const localStorage = require('electron-json-storage');
const { app } = require('electron');

const HUB_INFO = 'hubInfo';

class SystemInfo {
  logInfo() {
    const version = this.hubVersion();
    const executablePath = os.platform() === 'linux' ? process.env.APPIMAGE : app.getPath('exe');

    this.storeHubInfo({ version, executablePath });
    logger.info(`Hub version: ${version}`);
    logger.info(`Hub path: ${executablePath}`);

    this.bugReporterSystemInfo().then((info) => {
      logger.info(info);
    });
    if (os.platform() === 'win32') {
      isAdmin().then(admin => {
        if (admin) {
          logger.info('Hub process is running in Administrator mode');
        } else {
          logger.info('Hub process is running in User mode');
        }
      });
    }
  }

  hubVersion() {
    return app.getVersion();
  }

  osPlatform() {
    return os.platform();
  }

  storeHubInfo(hubInfo) {
    localStorage.set(HUB_INFO, hubInfo, () => {});
  }

  // this method mostly copy the output from the Bug reporter C++ code
  bugReporterSystemInfo() {
    const currPlatform = os.platform();
    if (currPlatform === 'win32') {
      const r = os.release().split('.');
      let info = '';
      if (r[0] === '4') {
        info = 'Windows NT';
      } else if (r[0] === '5' && r[1] === '0') {
        info = 'Windows 2000';
      } else if (r[0] === '5' && r[1] === '1') {
        info = 'Windows XP';
      } else if (r[0] === '5' && r[1] === '2') {
        info = 'Windows 2003 Server';
      } else if (r[0] === '6' && r[1] === '0') {
        info = 'Windows Vista';
      } else if (r[0] === '6' && r[1] === '1') {
        info = 'Windows 7';
      } else if (r[0] === '6' && r[1] === '2') {
        info = 'Windows 8';
      } else if (r[0] === '6' && r[1] === '3') {
        info = 'Windows 8.1';
      } else if (r[0] === '10' && r[1] === '0') {
        info = 'Windows 10';
      } else {
        info = 'unknown Windows version';
      }
      info += ` (${r[0]}.${r[1]}.0)`;
      if (os.arch() === 'x64') {
        info += ' 64bit';
      }
      info = `${os.cpus()[0].model}; ${info}`;
      return new Promise((resolve) => {
        resolve(info);
      });

    } else if (currPlatform === 'darwin') {

      return getHardwareDataTypeMac().then((cpuInfo) => getOSInfoMac().then((osInfo) => (`${cpuInfo}; ${osInfo}`))).catch((err) => {
        logger.warn(err);
        return '';
      });

    }

    // linux
    return new Promise((resolve) => {
      resolve(`${os.cpus()[0].model}; Linux (${os.release()})`);
    });
  }
}

function getHardwareDataTypeMac() {
  return new Promise((resolve, reject) => {
    exec('system_profiler SPHardwareDataType | perl -p -e \'s/^\\\\s+|\\\\s+$//g; if (/(?:Machine Model|Model Name|Processor Speed|Processor Name|CPU Speed|Memory): (.*)/) { $_ = $1 . " " } elsif (/(?:Total Number Of Cores|Number Of CPUs): (.*)/ && $1 > 1) { $_ = $1 . " CPU cores " } else { $_ = "" }\'', (error, stdout) => {
      if (error) {
        return reject(error);
      }
      return resolve(stdout);
    });
  });
}

function getOSInfoMac() {
  return new Promise((resolve, reject) => {
    let result = '';
    exec('sw_vers', (error, stdout) => {
      if (error) {
        return reject(error);
      }
      const lines = stdout.split('\n');
      lines.forEach((line) => {
        const data = line.split('\t')[1];
        if (data !== undefined) {
          result += `${data.trim()} `;
        }
      });
      result += `Darwin ${os.release()}`;
      return resolve(result);
    });
  });
}

module.exports = new SystemInfo();
