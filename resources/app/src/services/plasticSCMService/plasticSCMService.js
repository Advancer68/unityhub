const axios = require('axios');
const promisify = require('es6-promisify');
const localStorageAsync = require('electron-json-storage');
const auth = require('../localAuth/auth');
const systemInfo = require('../localSystemInfo/systemInfo');
const logger = require('../../logger')('PlasticSCMService');
const postal = require('postal');
const { fs } = require('../../fileSystem');
const path = require('path');
const os = require('os');
const downloader = require('../../downloadManager/downloadManager');
const { MT_FILE_DL_STATUS } = require('../localDownload/lib/constants');
const localConfig = require('../localConfig/localConfig');
const PlasticSCMUtils = require('./plastic_scm_utils');

const localStorage = {
  get: promisify(localStorageAsync.get, localStorageAsync),
  set: promisify(localStorageAsync.set, localStorageAsync),
};
const downloadTempName = 'plastic.tmp';
const plasticRemoteFileBasePath = 'https://public-cdn.cloud.unitychina.cn/hub/prod/plastic';
const plasticConfigKeys = {
  key: 'plastic-scm-config',
  authentication: 'authentication',
  repositories: 'repositories',
  organizations: 'organizations',
  templateVersion: 'templateVersion',
  disabled: 'disabled',
  disableSetLanguageAuto: 'disableSetLanguageAuto',
};
let download;
let execPath;
let versionFilePath;

const plasticAPI = {
  authentication: '/api/hub/authentication/login-by-token',
  organizations: '/api/hub/cloud/organizations',
  repositories: '/api/hub/cloud/repositories',
};
const checkDownloadDuration = 1000 * 60 * 60;

class PlasticSCMService {
  constructor() {
  }

  async init(localProject) {
    this.endpoint = await localConfig.getPlasticWebServer();
    this.localProject = localProject;
    postal.subscribe({
        channel: 'app',
        topic: 'userInfo.changed',
        callback: data => this.userInfoChanged(data),
    });
    
    this.addPlasticDownload();
    this.asyncDownload();
    this.checkDownload();

    setTimeout(() => this.clearTemplates(), 5000);
    
    PlasticSCMUtils.setIgnoreList();
    
    if (PlasticSCMUtils.setupEnv) {
      PlasticSCMUtils.setupEnv();
    }

    const setLanguageDisabled = await this.isSetLanguageAutoDisabled();
    if (!setLanguageDisabled) {
      PlasticSCMUtils.setPlasticClientLanguage();
    }
  }

  async userInfoChanged(data) {
    if (data.userInfo && data.userInfo.userId) {
      try {

        await this.getPlasticSCMAuthentication();
        await this.getRepositories();

      } catch (error) {
        logger.error(error);
      }
    }
  }

  checkDownload() {
    setInterval(() => {
      if (!PlasticSCMUtils.getInstallPath()) {
        this.asyncDownload();
        PlasticSCMUtils.syncVersionData();
      }
    }, checkDownloadDuration);
  }

  addPlasticDownload() {
    const execName = PlasticSCMUtils.getInstallFilename();
    const versionFile = PlasticSCMUtils.getVersionFileName();
    const downloadPath = path.join(os.tmpdir(), downloadTempName);

    execPath = path.join(os.tmpdir(), execName);
    versionFilePath = path.join(os.tmpdir(), versionFile);

    download = downloader.download(`${plasticRemoteFileBasePath}/${execName}`, downloadPath);
    download.destinationPath = downloadPath;
  }

  async clearTemplates() {
    const currentVersion = await this.getTemplateVersion();
    const remoteVersion = await PlasticSCMUtils.getRemotePluginVerion();

    if (currentVersion != remoteVersion) {
      this.updateTemplateVersion(remoteVersion);

      const templatesPath = await this.localProject.getTemplatePath();

      if (fs.existsSync(templatesPath)) {
        fs.readdirSync(templatesPath).forEach(file => {
          if (file.includes('-plasticscm-')) {
              fs.unlink(path.join(templatesPath, file));
          }
        });
      }
    }
  }

  async getConfig() {
    return await localStorage.get(plasticConfigKeys.key);
  }

  async updateConfig(userId, key ,data) {
    const config = await this.getConfig();
    const currentUserConfig = config[userId] || {};

    currentUserConfig[key] = data;
    config[userId] = currentUserConfig;

    return await localStorage.set(plasticConfigKeys.key, config);
  }

  async getTemplateVersion() {
    const config = await this.getConfig();
    const version = config[plasticConfigKeys.templateVersion] || '';
    return Promise.resolve(version);
  }

  async updateTemplateVersion(version) {
    const config = await this.getConfig();
    config[plasticConfigKeys.templateVersion] = version;

    return localStorage.set(plasticConfigKeys.key, config);
  }

  async isDisabled() {
    const config = await this.getConfig();
    const disabled = !!config[plasticConfigKeys.disabled];
    return Promise.resolve(disabled);
  }

  async toggleDisabled(disabled) {
    const config = await this.getConfig();
    config[plasticConfigKeys.disabled] = disabled;
    return localStorage.set(plasticConfigKeys.key, config);
  }

  async isSetLanguageAutoDisabled() {
    const config = await this.getConfig();
    const disabled = !!config[plasticConfigKeys.disableSetLanguageAuto];
    return Promise.resolve(disabled);
  }

  async toggleDisableSetLanguageAuto(disable) {
    const config = await this.getConfig();
    config[plasticConfigKeys.disableSetLanguageAuto] = disable;

    if (!disable) {
      PlasticSCMUtils.setPlasticClientLanguage();
    }

    return localStorage.set(plasticConfigKeys.key, config);
  }

  async getCurrentUserAuthentication(userId) {
    const config = await this.getConfig();
    const currentUserConfig = config[userId] || {};
    return Promise.resolve(currentUserConfig[plasticConfigKeys.authentication]);
  }

  async updateCurrentUserAuthentication(userId, authentication) {
    return await this.updateConfig(userId, plasticConfigKeys.authentication, authentication);
  }

  async getCurrentUserRepositories(userId) {
    const config = await this.getConfig();
    const currentUserConfig = config[userId] || {};
    return Promise.resolve(currentUserConfig[plasticConfigKeys.repositories]);
  }

  async updateCurrentUserRepositories(userId, repositories) {
    return await this.updateConfig(userId, plasticConfigKeys.repositories, repositories);
  }

  async getCurrentUserOrganizations(userId) {
    const config = await this.getConfig();
    const currentUserConfig = config[userId] || {};
    return Promise.resolve(currentUserConfig[plasticConfigKeys.organizations]);
  }

  async updateCurrentUserOrganizations(userId, orgs) {
    return await this.updateConfig(userId, plasticConfigKeys.organizations, orgs);
  }

  async getPlasticSCMAuthentication() {
    const userPlain = await auth.getUserInfo();
    const user = JSON.parse(userPlain);
    if (!user.accessToken) {
      return Promise.reject('need login');
    }

    const plasticSCMAuthentication = await this.getCurrentUserAuthentication(user.userId);
    if (plasticSCMAuthentication) {
      return Promise.resolve(plasticSCMAuthentication);
    }

    const response = await axios.post(`${this.endpoint}${plasticAPI.authentication}`, null, this.getCommonHeader(user.accessToken));
    this.updateCurrentUserAuthentication(user.userId, response.data);
    return Promise.resolve(response.data);
  }

  async getPlasticSCMOrganizations() {
    const userPlain = await auth.getUserInfo();
    const user = JSON.parse(userPlain);
    if (!user.accessToken) {
      return Promise.reject('need login');
    }

    return this.syncOrganizationsFromRemote(user.userId, user.accessToken);
  }

  async syncOrganizationsFromRemote(userId, token) {
    const response = await axios.get(this.endpoint + plasticAPI.organizations, this.getCommonHeader(token));
    const data = response.data;
    this.updateCurrentUserOrganizations(userId, data.organizations);
    return Promise.resolve(data.organizations);
  }

  async createPlasticOrganization(orgId) {
    const userPlain = await auth.getUserInfo();
    const user = JSON.parse(userPlain);
    if (!user.accessToken) {
      return Promise.reject('NEED_LOGIN');
    }

    await this.getPlasticSCMAuthentication();
    
    try {
      await axios.post(this.endpoint + plasticAPI.organizations, {name: orgId}, this.getCommonHeader(user.accessToken));
      const response = await axios.get(this.endpoint + plasticAPI.organizations, this.getCommonHeader(user.accessToken));
      const organizations = response.data.organizations;

      this.updateCurrentUserOrganizations(user.userId, organizations);

      return Promise.resolve(organizations);
    } catch (error) {
      logger.error(error);
      if (error.response.status === 403) {
        return Promise.reject('PERMISSION_DENIED');
      }
      return Promise.reject(error);
    }
  }

  async getRepositories() {
    const userPlain = await auth.getUserInfo();
    const user = JSON.parse(userPlain);
    if (!user.accessToken) {
      return Promise.reject('need login');
    }

    await this.getPlasticSCMAuthentication();

    const repositories = await this.getCurrentUserRepositories(user.userId);
    if (repositories && repositories.length > 0) {
      this.syncRepositoriesFromRemote(user.userId, user.accessToken);
      return Promise.resolve(repositories);
    }

    const response = await this.syncRepositoriesFromRemote(user.userId, user.accessToken);
    return Promise.resolve(response);
  }

  async syncRepositoriesFromRemote(userId, token) {
    const response = await axios.get(this.endpoint + plasticAPI.repositories, this.getCommonHeader(token));
    this.updateCurrentUserRepositories(userId, this.processRepositories(response.data.repositories));
    return Promise.resolve(this.processRepositories(response.data.repositories));
  }

  processRepositories(repos) {
    return repos.map(repo => ({
      ...repo,
      serverUrl: PlasticSCMUtils.ensureSSL(repo.serverUrl),
    })).sort((a, b) => {
      const re = a.repository.localeCompare(b.repository);

      if (re === 0) {
        return a.serverUrl.localeCompare(b.serverUrl);
      }

      return re;
    });
  }

  async getOrganizations() {
    const userPlain = await auth.getUserInfo();
    const user = JSON.parse(userPlain);
    if (!user.accessToken) {
      return Promise.reject('need login');
    }

    const orgs = await auth.getOrganizations();
    const myOrgs = getMyCreatedOrganizations(user.userId, orgs);
    
    return Promise.resolve(myOrgs)
  }

  async getPlasticStatus() {
    const isInstalled = !!PlasticSCMUtils.getInstallPath();
    let isValidEdition = false;
    let isValidVersion = false;

    if (isInstalled) {
      isValidEdition = await PlasticSCMUtils.isValidEdition();
      isValidVersion = await PlasticSCMUtils.isValidVersion();
    }

    if (isValidEdition && !isValidVersion) {
      isValidVersion = await PlasticSCMUtils.isValidVersion(true);
    }
    
    return Promise.resolve({ isInstalled, isValidEdition, isValidVersion });
  }

  async install() {
    return new Promise((resolve, reject) => {
      this.asyncDownload().then(() => {

        fs.accessSync(execPath, fs.constants.F_OK);

        PlasticSCMUtils.install(execPath).then(() => resolve()).catch(error => {
          logger.error('failed to install plastic', error);
          return reject(error);
        });
      }).catch(error => {
        logger.error('failed to install plastic', error);
        return reject(error);
      });
    });
  }

  async asyncDownload() {
    const remoteVersion = await PlasticSCMUtils.getRemoteVerion();
    const shouldDownload = await this.shouldDownload(remoteVersion);
    const isDownloading = download.status === MT_FILE_DL_STATUS.PENDING;
    const self = this;

    return new Promise(function(resolve, reject) {
      const downloadPath = download.destinationPath;

      if (!shouldDownload) {
        return resolve();
      }

      if (!isDownloading) {
        self.clearFileDownloadFile();
      }
      
      download.on('end', () => {
        if (isDownloading) {
          return setTimeout(resolve, 1500);
        } else {
          if (fs.existsSync(downloadPath)) {

            fs.renameSync(downloadPath, execPath);
            fs.writeFileSync(versionFilePath, remoteVersion);
  
            return resolve();
          }

          return reject();
        }
      });

      download.on('error', (d, error) => {
        logger.error('failed to download plastic', error);
        return reject(error);
      });

      if (!isDownloading) {
        logger.info('start downloading plastic client');
        download.start();
      }
    });
  }

  async shouldDownload(remoteVersion) {
    const versionFileExists = fs.existsSync(versionFilePath);
    const execFileExists = fs.existsSync(execPath);

    if (!versionFileExists || !execFileExists) {
      return Promise.resolve(true);
    } else {
      let currentVersion = fs.readFileSync(versionFilePath, 'utf8') || '';
      currentVersion = currentVersion.trim();
      
      if (currentVersion != remoteVersion) {
        return Promise.resolve(true);
      }
    }

    return Promise.resolve(false);
  }

  clearFileDownloadFile() {
    if (fs.existsSync(versionFilePath)) {
      fs.unlinkSync(versionFilePath);
    }

    if (fs.existsSync(execPath)) {
      fs.unlinkSync(execPath);
    }
  }

  async enablePlastic() {
    const isDisabled = await this.isDisabled();
    if (isDisabled) {
      return Promise.reject('disabled in settings');
    }

    const userPlain = await auth.getUserInfo();
    const user = JSON.parse(userPlain);
    if (user.accessToken) {
      return Promise.resolve('enable plastic');
    }

    return Promise.reject('login to enable plastic');
  }

  async cloneProject(id, projectName, repoPath, repo, serverUrl) {
    const plasticAuth = await this.getPlasticSCMAuthentication();
    const localProject = this.localProject;
    return new Promise((resolve, reject) => {
      PlasticSCMUtils.cloneRepo({id, projectName, repoPath, serverUrl, email: plasticAuth.email, token: plasticAuth.token}).then(workspacePath => {
        localProject.addProjectWithDir(workspacePath).then(() => {
          PlasticSCMUtils.setClonedProjectType(id, true);
          return resolve({isUnityProject: true});
        }).catch(err => {
          logger.error('fail add to projects', err);
          PlasticSCMUtils.setClonedProjectType(id, false);
          return resolve({isUnityProject: false});
        });
      }).catch(error => {
        return reject(error);
      });
    });
  }

  async addProjectToPlasticSCM(projectName, projectPath, serverUrl, addToExistingRepo) {
    const localProject = this.localProject;
    const plasticSCMEnabled = await localProject.plasticSCMEnabled(projectPath);
    
    if (plasticSCMEnabled) {
      localProject.getRecentProjects(true);
      return Promise.resolve();
    }
    
    const { email, token } = await this.getPlasticSCMAuthentication();

    return new Promise((resolve, reject) => {
      PlasticSCMUtils.addProjectToPlasticSCM(projectName, projectPath, serverUrl, email, token, addToExistingRepo).then(() => {
        localProject.addPlasticSCMPlugin(projectPath);
        return resolve();
      }).catch(error => {
        PlasticSCMUtils.log(error);
        return reject(error);
      });
    });
  }

  async checkinAndMerge(projectPath) {
    const { email } = await this.getPlasticSCMAuthentication();

    try {
      await PlasticSCMUtils.checkinProject(projectPath);
      await PlasticSCMUtils.mergeToMain(email, projectPath);
    } catch (error) {
      logger.info('failed merge to main branch:', error);
      this.localProject.getRecentProjects(true);
      
      return Promise.reject();
    }

    this.localProject.getRecentProjects(true);
    return Promise.resolve();
  }

  async createWorkspaceWithBranch(projectName, projectDirectory, serverUrl, workingMode = 'LDAPWorkingMode') {
    serverUrl = PlasticSCMUtils.ensureSSL(serverUrl);
    
    const {email, token} = await this.getPlasticSCMAuthentication();
    const branchName = PlasticSCMUtils.getBranchName(email);

    await PlasticSCMUtils.updateClientConfig(workingMode, email, token, serverUrl);

    const repo = await PlasticSCMUtils.createPlasticSCMRepository(projectName, serverUrl, email, token);

    const fullProjectPath = path.join(projectDirectory, projectName);
    if (!fs.existsSync(fullProjectPath)) {
      fs.mkdirSync(fullProjectPath);
    }

    const plasticIniFilePath = path.join(fullProjectPath, 'plastic.ini');
    fs.closeSync(fs.openSync(plasticIniFilePath, 'w'));

    
    const workspaceName = await PlasticSCMUtils.ensureWorkspaceName(projectName);
    const workspacePath = path.join(projectDirectory, workspaceName);

    await PlasticSCMUtils.createWorkspace({name: workspaceName, workspacePath, repo});

    await PlasticSCMUtils.createBranch(branchName, fullProjectPath);

    await PlasticSCMUtils.switchBranch(branchName, fullProjectPath);
  }

  async resetAccount() {
    try {
      const {email, token} = await this.getPlasticSCMAuthentication();
      const repos = await this.getRepositories()

      if (repos.length > 0) {
        const serverUrl = repos[0].serverUrl;
        PlasticSCMUtils.updateClientConfig('LDAPWorkingMode', email, token, serverUrl);
      }
      return Promise.resolve();

    } catch (error) {
      logger.error(error);
      return Promise.reject(error);
    }
  }

  hideClonedProjectProgressBar(id) {
    PlasticSCMUtils.hideClonedProjectProgressBar(id);
  }

  hideCheckinProjectProgressBar(id) {
    PlasticSCMUtils.hideCheckinProjectProgressBar(id);
  }

  getCurrentCloneInfo() {
    return JSON.stringify(PlasticSCMUtils.cloneInfo);
  }

  getCurrentUploadInfo() {
    return JSON.stringify(PlasticSCMUtils.uploadInfo);
  }

  openLog() {
    PlasticSCMUtils.openLog();
  }

  openProjectInFolder(id) {
    PlasticSCMUtils.openProjectInFolder(id);
  }

  getCommonHeader(accessToken) {
    return    {
        headers: {
            'User-Agent': `hub ${systemInfo.hubVersion()}.cn`,
            date: (new Date()).toUTCString(),
            'Content-Type': 'application/json',
            AUTHORIZATION: `Bearer ${accessToken}`,
        },
        responseType: 'json'
    };
  }
}

module.exports = new PlasticSCMService();

function getMyCreatedOrganizations(userId, organizations) {
  return organizations.filter(org => `${org.billable_user_fk}` === userId).map(org => ({id: org.id, name: org.name}));
}
