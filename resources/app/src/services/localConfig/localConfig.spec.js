const localSettings = require('../localSettings/localSettings');
const localConfig = require('./localConfig');
const localStorage = require('electron-json-storage');
const cloudConfig = require('../cloudConfig/cloudConfig');

const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const nock = require('nock');


chai.use(chaiAsPromised);
chai.use(sinonChai);

describe('LocalConfig', () => {
  let sandbox;
  let mockStorage = {};
  let settingsMock;

  beforeEach(() => {
    sandbox = sinon.sandbox.create();

    sandbox.stub(localSettings, 'get').callsFake((key) => settingsMock[key]);
    sandbox.stub(localStorage, 'get').callsFake((key, callback) => { callback(null, mockStorage); });
    sandbox.stub(localStorage, 'set').callsFake((key, value, callback) => { callback(); });
    sandbox.stub(cloudConfig, 'getHubBaseEndpoint').returns('http://test.com/');

    settingsMock = {
      [localSettings.keys.DISABLE_CLOUD_PROJECTS]: 'cloudFlag',
      [localSettings.keys.DISABLE_ANALYTICS]: 'analyticsFlag',
      [localSettings.keys.DISABLE_SIGNIN]: 'signinFlag',
      [localSettings.keys.DISABLE_LEARN]: 'learnFlag',
      [localSettings.keys.DISABLE_COMMUNITY]: 'communityFlag',
      [localSettings.keys.DISABLE_AUTO_UPDATE]: 'autoupdateFlag',
      [localSettings.keys.DISABLE_ELEVATE]: 'elevateFlag',
      [localSettings.keys.DISABLE_PE]: 'peFlag',
      [localSettings.keys.DISABLE_VS]: 'vsFlag'
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('init', () => {

    beforeEach(() => {
      localConfig.init();
      sandbox.stub(localConfig, 'startRefreshInterval').resolves();
    });

    it('should call the startRefreshInterval', () => {
      return localConfig.init().then(() => {
        expect(localConfig.startRefreshInterval).to.have.been.called;
      });
    });

    it('should set the local storage as hubConfig when there is no cache', () => {
      localConfig.data = undefined;
      mockStorage = null;

      return localConfig.init().then(() => {
        expect(localConfig.hubConfig[localSettings.keys.DISABLE_CLOUD_PROJECTS]).to.equal(settingsMock[localSettings.keys.DISABLE_CLOUD_PROJECTS]);
        expect(localConfig.hubConfig[localSettings.keys.DISABLE_ANALYTICS]).to.equal(settingsMock[localSettings.keys.DISABLE_ANALYTICS]);
        expect(localConfig.hubConfig[localSettings.keys.DISABLE_LEARN]).to.equal(settingsMock[localSettings.keys.DISABLE_LEARN]);
        expect(localConfig.hubConfig[localSettings.keys.DISABLE_SIGNIN]).to.equal(settingsMock[localSettings.keys.DISABLE_SIGNIN]);
        expect(localConfig.hubConfig[localSettings.keys.DISABLE_AUTO_UPDATE]).to.equal(settingsMock[localSettings.keys.DISABLE_AUTO_UPDATE]);
        expect(localConfig.hubConfig[localSettings.keys.DISABLE_ELEVATE]).to.equal(settingsMock[localSettings.keys.DISABLE_ELEVATE]);
      });
    });


    it('should set the cached hubConfig as hubConfig when there is cache', () => {
      localConfig.data = undefined;
      mockStorage = {[localSettings.keys.DISABLE_AUTO_UPDATE]: 'AnotherFlag'};
      return localConfig.init().then(() => {
        expect(localConfig.hubConfig).to.deep.equal( Object.assign({}, settingsMock, mockStorage));
      });
    });

  });

  describe('refreshHubConfig', () => {
    let httpMock;
    beforeEach(() => {
      mockStorage = null;
      localConfig.init();
      httpMock = nock(/test.com/);
    });

    afterEach(() => {
      nock.cleanAll();
    });

    it.skip('should return a resolving promise when request succeeded', () => {
      httpMock.get(/.*/).reply(200);
      let promise = localConfig.refreshData();
      return expect(promise).to.eventually.be.fulfilled;
    });

    it.skip('should return a resolving promise when request fails', () => {
      httpMock.get(/.*/).reply(400);
      let promise = localConfig.refreshData();
      return expect(promise).to.eventually.be.fulfilled;
    });
  });

  describe('getEnvironment', () => {
    it('should return cloudEnvironment setting', () => {
      localConfig.init();
      return expect(localConfig.getEnvironment()).to.eventually.equal(settingsMock[localSettings.keys.CLOUD_ENVIRONMENT]);
    });
  });


  describe('isAnalyticsDisabled', () => {
    it('should return isAnalyticsDisabled setting', () => {
      localConfig.init();
      return expect(localConfig.isAnalyticsDisabled()).to.equal(settingsMock[localSettings.keys.DISABLE_ANALYTICS]);
    });
    it('should return the default isAnalyticsDisabled setting if data is undefined', () => {
      let result = 'defaultDisableAnalytics';
      sandbox.stub(localConfig, 'defaultData').get(() => {
        return {
          [localSettings.keys.DISABLE_ANALYTICS]: result
        }
      });
      sandbox.stub(localConfig, 'data').get(() => undefined);
      localConfig.data = undefined;
      return expect(localConfig.isAnalyticsDisabled()).to.equal(result);
    });
  });

  describe('isCloudProjectsDisabled', () => {
    it('should return isCloudProjectsDisabled setting', () => {
      localConfig.init();
      return expect(localConfig.isCloudProjectsDisabled()).to.equal(settingsMock[localSettings.keys.DISABLE_CLOUD_PROJECTS]);
    });
    it('should return the default isCloudProjectsDisabled setting if data is undefined', () => {
      let result = 'defaultDisableCLoudProjects';
      sandbox.stub(localConfig, 'defaultData').get(() => {
        return {
          [localSettings.keys.DISABLE_CLOUD_PROJECTS]: result
        }
      });
      sandbox.stub(localConfig, 'data').get(() => undefined);
      return expect(localConfig.isCloudProjectsDisabled()).to.equal(result);
    });
  });

  describe('isLearnDisabled', () => {
    it('should return isLearnDisabled setting', () => {
      localConfig.init();
      return expect(localConfig.isLearnDisabled()).to.equal(settingsMock[localSettings.keys.DISABLE_LEARN]);
    });
    it('should return the default isLearnDisabled setting if data is undefined', () => {
      let result = 'defaultDisableLearn';
      sandbox.stub(localConfig, 'defaultData').get(() => {
        return {
          [localSettings.keys.DISABLE_LEARN]: result
        }
      });
      sandbox.stub(localConfig, 'data').get(() => undefined);
      return expect(localConfig.isLearnDisabled()).to.equal(result);
    });
  });

  describe('isCommunityDisabled', () => {
    it('should return isCommunityDisabled setting', () => {
      localConfig.init();
      return expect(localConfig.isCommunityDisabled()).to.equal(settingsMock[localSettings.keys.DISABLE_COMMUNITY]);
    });
    it('should return the default isCommunityDisabled setting if data is undefined', () => {
      let result = 'defaultDisableCommunity';
      sandbox.stub(localConfig, 'defaultData').get(() => {
        return {
          [localSettings.keys.DISABLE_COMMUNITY]: result
        }
      });
      sandbox.stub(localConfig, 'data').get(() => undefined);
      return expect(localConfig.isCommunityDisabled()).to.equal(result);
    });
  });

  describe('isAutoUpdateDisabled', () => {
    it.skip('should return isAutoUpdateDisabled setting', () => {
      localConfig.init();
      return expect(localConfig.isAutoUpdateDisabled()).to.equal(settingsMock[localSettings.keys.DISABLE_AUTO_UPDATE]);
    });
    it('should return the default isAutoUpdateDisabled setting if data is undefined', () => {
      let result = 'defaultDisableAutoUpdate';
      sandbox.stub(localConfig, 'defaultData').get(() => {
        return {
          [localSettings.keys.DISABLE_AUTO_UPDATE]: result
        }
      });
      sandbox.stub(localConfig, 'data').get(() => undefined);
      return expect(localConfig.isAutoUpdateDisabled()).to.equal(result);
    });
  });

  describe('isSignInDisabled', () => {
    it('should return isSignInDisabled setting', () => {
      localConfig.init();
      return expect(localConfig.isSignInDisabled()).to.equal(settingsMock[localSettings.keys.DISABLE_SIGNIN]);
    });
    it('should return the default isSignInDisabled setting if data is undefined', () => {
      let result = 'defaultDisableSignIn';
      sandbox.stub(localConfig, 'defaultData').get(() => {
        return {
          [localSettings.keys.DISABLE_SIGNIN]: result
        }
      });
      sandbox.stub(localConfig, 'data').get(() => undefined);
      return expect(localConfig.isSignInDisabled()).to.equal(result);
    });
  });

  describe('isElevateDisabled', () => {
    it('should return isElevateDisabled setting', () => {
      localConfig.init();
      return expect(localConfig.isElevateDisabled()).to.equal(settingsMock[localSettings.keys.DISABLE_ELEVATE]);
    });
    it('should return the default isElevateDisabled setting if data is undefined', () => {
      let result = 'defaultDisableElevate';
      sandbox.stub(localConfig, 'defaultData').get(() => {
        return {
          [localSettings.keys.DISABLE_ELEVATE]: result
        }
      });
      sandbox.stub(localConfig, 'data').get(() => undefined);
      return expect(localConfig.isElevateDisabled()).to.equal(result);
    });
  });

  describe('isPEDisabled', () => {
    it('should return isPEDisabled setting', () => {
      localConfig.init();
      return expect(localConfig.isPEDisabled()).to.equal(settingsMock[localSettings.keys.DISABLE_PE]);
    });
    it('should return the default isPEDisabled setting if data is undefined', () => {
      let result = 'defaultDisablePE';
      sandbox.stub(localConfig, 'defaultData').get(() => {
        return {
          [localSettings.keys.DISABLE_PE]: result
        }
      });
      sandbox.stub(localConfig, 'data').get(() => undefined);
      return expect(localConfig.isPEDisabled()).to.equal(result);
    });
  });

  describe('isVSDisabled', () => {
    it('should return isVSDisabled setting', () => {
      localConfig.init();
      return expect(localConfig.isVSDisabled()).to.equal(settingsMock[localSettings.keys.DISABLE_VS]);
    });
    it('should return the default isVSDisabled setting if data is undefined', () => {
      let result = 'defaultDisableVS';
      sandbox.stub(localConfig, 'defaultData').get(() => {
        return {
          [localSettings.keys.DISABLE_VS]: result
        }
      });
      sandbox.stub(localConfig, 'data').get(() => undefined);
      return expect(localConfig.isVSDisabled()).to.equal(result);
    });
  });

  describe('getNewUserOnboardingConfig', () => {
    it('should return getNewUserOnboardingConfig setting', () => {
      localConfig.init();
      return expect(localConfig.getNewUserOnboardingConfig()).to.equal(settingsMock[localSettings.keys.NEW_USER_ONBOARDING_CONFIG]);
    });
    it('should return the default getNewUserOnboardingConfig setting if data is undefined', () => {
      let result = 'defaultNUOConfig';
      sandbox.stub(localConfig, 'defaultData').get(() => {
        return {
          [localSettings.keys.NEW_USER_ONBOARDING_CONFIG]: result
        }
      });
      sandbox.stub(localConfig, 'data').get(() => undefined);
      return expect(localConfig.getNewUserOnboardingConfig()).to.equal(result);
    });
  });

  describe('getBetaFeedbackUrl', () => {
    beforeEach(() => {
      localConfig.data = undefined;
    });

    it('should return the localSettings value (locally configured scenario)', () => {
      settingsMock[localSettings.keys.BETA_FEEDBACK_URL] = 'localFeedbackUrl';
      localConfig.data = {
        betaFeedbackUrl: 'remoteFeedbackUrl'
      };

      expect(localConfig.getBetaFeedbackUrl()).to.equal('localFeedbackUrl');
    });

    it('should return the data value (remotely configured scenario)', () => {
      localConfig.data = {
        betaFeedbackUrl: 'remoteFeedbackUrl'
      };

      expect(localConfig.getBetaFeedbackUrl()).to.equal('remoteFeedbackUrl');
    });

    it('should return undefined if no configs present', () => {
      expect(localConfig.getBetaFeedbackUrl()).to.equal(undefined);
    });
  });
});
