// internal
const unityAuth = require('../auth');
const cloudCore = require('../../cloudCore/cloudCore');
const tokenManager = require('../../../tokenManager/tokenManager');
const windowManager = require('../../../windowManager/windowManager');
const cloudAnalytics = require('../../cloudAnalytics/cloudAnalytics');


// third parties
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const postal = require('postal');


chai.use(chaiAsPromised);
chai.use(sinonChai);

describe('UnityAuth', () => {

  let sandbox;

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    sandbox.stub(postal, 'publish');
    unityAuth.userInfo = unityAuth.constructor.getDefaultUserInfo();
    unityAuth.connectInfo = unityAuth.constructor.getDefaultConnectInfo();
    windowManager.broadcastContent = sandbox.stub();
  });
  afterEach(() => {
    unityAuth.logout();
    sandbox.restore();
  });

  describe('getOrganizations', () => {
    it('when user is not logged in should reject promise', () => {
      unityAuth.connectInfo.loggedIn = false;
      return expect(unityAuth.getOrganizations()).to.eventually.be.rejected;
    });
  
    it('when user is logged in should return cloudCore.getOrganizations', () => {
      const foo = "bar";
      unityAuth.connectInfo.loggedIn = true;
      sandbox.stub(cloudCore, 'getOrganizations').resolves(foo);
      return expect(unityAuth.getOrganizations()).to.eventually.equal(foo);
    });
  });

  describe('getUserInfo', () => {
    it('should resolve as a string', () => {
      return expect(unityAuth.getUserInfo()).to.eventually.be.a('string');
    });

    it('should resolve with the user info structure as stringified JSON', () => {
      let userInfo = unityAuth.userInfo;

      return expect(unityAuth.getUserInfo()
        .then((strUserInfo) => {
          return JSON.parse(strUserInfo);
        })).to.eventually.deep.equal(userInfo);
    });

  });

  describe('getConnectInfo', () => {
    it('should resolve as a string', () => {
      return expect(unityAuth.getConnectInfo()).to.eventually.be.a('string');
    });

    it('should resolve with the connect info structure as stringified JSON', () => {
      let connectInfo = unityAuth.connectInfo;

      return expect(unityAuth.getConnectInfo()
        .then((strConnectInfo) => {
          return JSON.parse(strConnectInfo);
        })).to.eventually.deep.equal(connectInfo);
    });
  });
  
  describe('logout', () => {
    
    beforeEach(() => {
      sandbox.stub(tokenManager, 'clearTokens');
      unityAuth.logout();
    });
    
    it('should update connectInfo flags to the proper values for logged out state', () => {
      expect(unityAuth.connectInfo.ready).to.be.true;
      expect(unityAuth.connectInfo.loggedIn).to.be.false;
      expect(unityAuth.connectInfo.initialized).to.be.true;
      expect(unityAuth.connectInfo.workOffline).to.be.true;
    });

    it('should update userInfo to its default value', () => {
      expect(unityAuth.userInfo).to.eql(unityAuth.constructor.getDefaultUserInfo());
    });

    it('should clean tokens', () => {
      expect(tokenManager.clearTokens).to.have.been.called;
    });

    it('should notify windows for userInfo and connectInfo changes', () => {
      expect(windowManager.broadcastContent).to.have.been.calledWith('connectInfo.changed', unityAuth.connectInfo);
      expect(windowManager.broadcastContent).to.have.been.calledWith('userInfo.changed', unityAuth.userInfo);
    })

  });

});
