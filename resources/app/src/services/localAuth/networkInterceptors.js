const axios = require('axios');
const logger = require('../../logger')('NetworkInterceptors');
const EventEmitter = require('events');
const equal = require('fast-deep-equal');

let networkUp;
let lastNetworkUpTime = 0;
const emitNetworkUpPeriodThreshold = 60 * 1000;

var requestCache = {
  /**
   * timeout for Throttle in millis
   * @type {number}
   */
  timeout: 500,
  /** 
   * @type {{_: number, data: {}}}
   */
  configSet: {},
  data: {},
  key(config) {
    if (config === null || config === undefined || config.method === null || config.method === undefined) {
      return '';
    }
    return `${config.method}.${config.url}`;
  },
  remove(config) {
    var k = requestCache.key(config);
    delete requestCache.configSet[k];
    delete requestCache.data[k];
  },
  shouldThrottle(config) {
    var k = requestCache.key(config);
    
    var result = false;
    if (!!requestCache.data[k]) { // eslint-disable-line
      if (config.method === 'get') {
        result = equal(requestCache.configSet[k].params, config.params) && ((new Date().getTime() - requestCache.data[k]._) < requestCache.timeout);
      } else if (config.method === 'post' || config.method === 'put') {
        result = equal(requestCache.configSet[k].data, config.data) && ((new Date().getTime() - requestCache.data[k]._) < requestCache.timeout);
      }
    }
    if (!result) {
      requestCache.set(config);
    }
    
    return result;
  },
  set(config, cachedData) {
    var k = requestCache.key(config);
    requestCache.remove(k);
    requestCache.configSet[k] = config;
    requestCache.data[k] = {
      _: new Date().getTime(),
      data: cachedData
    };
  }
};

class NetworkInterceptors extends EventEmitter {
  
  get networkUp() { return networkUp; }
  
  constructor() {
    super();
    
    networkUp = true;
  }
  
  init() {
    logger.info('Init');
    axios.interceptors.response.use(
      ((response) => this._handleSuccessfulRequest(response)),
      ((error) => this._handleFailedRequest(error))
    );
    axios.interceptors.request.use((config) => {
      if (requestCache.shouldThrottle(config)) {
        return false;
      }
      return config;
    });
  }
  
  failedRequest() {
    this.emit('down');
    networkUp = false;
  }
  
  _handleSuccessfulRequest(response) {
    if (!this.networkUp) {
      const currentTime = new Date().getTime();
      if (currentTime - lastNetworkUpTime >= emitNetworkUpPeriodThreshold) {
        this.emit('up');
        lastNetworkUpTime = currentTime;
        networkUp = true;
      }
    }
    
    return response;
  }
  
  _handleFailedRequest(error) {
    if (!this._didRequestMakeItToTheServer(error)) {
      this.emit('down');
      networkUp = false;
    }
    
    throw error;
  }
  
  _didRequestMakeItToTheServer(error) {
    return error.response !== undefined;
  }
}

module.exports = new NetworkInterceptors();
