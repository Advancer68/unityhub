const axios = require('axios');
const promisify = require('es6-promisify');
const localStorageAsync = require('electron-json-storage');
const cloudConfig = require('../cloudConfig/cloudConfig');
const systemInfo = require('../localSystemInfo/systemInfo');
const postal = require('postal');

const localStorage = {
  get: promisify(localStorageAsync.get, localStorageAsync),
  set: promisify(localStorageAsync.set, localStorageAsync),
};
const splashScreenKey = 'splash-screen';
const durationKey = 'duration';
const lastShowTimeKey = 'lastShowTime';
const entitlementKey = 'hasEntitlement';
const nextShowTimeKey = 'nextShowTime';
const dayInMilliSeconds = 24 * 60 * 60 * 1000;

class SplashScreenService {
  constructor() {
    this.endpoint = 'https://connect.unity.cn/api/hub/splashScreenDuration';
  }

  init(auth) {
    postal.subscribe({
      channel: 'app',
      topic: 'userInfo.changed',
      callback: data => this.onUserInfoChanged(data),
    });
    this.auth = auth;
  }

  onUserInfoChanged(data) {
    if (data.userInfo && data.userInfo.userId) {
      this.syncDurationAndEntitlement();
    }
  }

  async getConfig() {
    return await localStorage.get(splashScreenKey);
  }

  async updateLastShowTime() {
    const conf = await this.getConfig();
    conf[lastShowTimeKey] = new Date().getTime();
    conf[nextShowTimeKey] = 0;

    return await localStorage.set(splashScreenKey, conf);
  }

  async updateNextShowTime() {
    const conf = await this.getConfig();
    conf[nextShowTimeKey] = new Date().getTime() + dayInMilliSeconds;

    return await localStorage.set(splashScreenKey, conf);
  } 

  async setDurationAndEntitlement(duration, hasEntitlement) {
    const conf = await this.getConfig();
    conf[durationKey] = duration;
    conf[entitlementKey] = hasEntitlement;

    return await localStorage.set(splashScreenKey, conf);
  }

  async syncDurationAndEntitlement() {
    const user = await this.auth.getUserInfo();
    const { userId, accessToken } = JSON.parse(user);
    
    if (userId && accessToken) {
      const duration = await this.getDuration();
      const hasEntitlement = await this.checkEntitlement(userId, accessToken);
  
      this.setDurationAndEntitlement(duration, hasEntitlement);
    }
  }

  async getDuration() {
    const response = await axios.get(this.endpoint, null, { responseType: 'json' });
    const result = JSON.parse(JSON.stringify(response.data));
    return result.durationInMinutes;
  }

  async checkEntitlement(userId, accessToken) {
    const url = `${cloudConfig.urls.identity}/v1/entitlements?isActive=true&userId=${userId}&namespace=unity_reward`;
    const conf = {
      headers: {
        'User-Agent': `hub ${systemInfo.hubVersion()}.cn`,
        date: (new Date()).toUTCString(),
        'Content-Type': 'application/json',
        AUTHORIZATION: `Bearer ${accessToken}`,
      },
      responseType: 'json'
    };
  
    const response = await axios.get(url, conf);
    const data = JSON.parse(JSON.stringify(response.data));
    
    return data.results.length > 0;
  }

  async shouldShow() {
    const now = new Date().getTime();
    const conf = await localStorage.get(splashScreenKey);
    const hasEntitlement = conf && conf[entitlementKey];
    let show = false;

    if (hasEntitlement) {
      const nextShowTime = (conf && conf[nextShowTimeKey]) || 0;

      if (nextShowTime !== 0) {
        if (nextShowTime <= now) {
          show = true;
        }
      } else {
        const lastShowTime = (conf && conf[lastShowTimeKey]) || 0;
        const durationInMinutes = conf && conf[durationKey];
        const duration = parseInt(durationInMinutes, 10);
    
        if (duration <= 0 || (now - lastShowTime >= duration * 60 * 1000)) {
          show = true;
        }
      }
    }


    if (show) {
      this.updateLastShowTime();
    }

    


    return show;
  }
}

module.exports = new SplashScreenService();
