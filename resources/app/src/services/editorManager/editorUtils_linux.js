const path = require('path');

const { fs, hubFS } = require('../../fileSystem');

const editorUtils = {

  makeEditorInstallsDir(installsPath) {
    return hubFS.elevateAndMakeDir(installsPath);
  },

  getEditorPathFromFolder(editorFolder) {
    return path.join(editorFolder, 'Editor', 'Unity');
  },

  findUnityVersion(editorPath) {
    let fullVersion = path.basename(path.dirname(path.dirname(editorPath)));
    // Allow users to manually specify their Unity-XXXX.yy.zzfn directories from the old installer
    fullVersion = fullVersion.replace(/^Unity-/, '');
    return {
      version: fullVersion,
    };
  },

  validateEditorFile(editorPath) {
    return new Promise((resolve, reject) => {
      if (!editorPath) {
        reject({ errorCode: 'ERROR.INVALID_PATH' });
      } else if (!fs.existsSync(editorPath)) {
        reject({ errorCode: 'ERROR.FILE_NOT_FOUND' });
      } else {
        resolve(true);
      }
    });
  },

  validateInstallPath() {
    return true;
  },

  /**
   * Returns constant for Unity Editor placement
   * @returns {Object} constant 'EDITOR'.
   */
  getEditorLocationPatterns() {
    return ''; // not implemented yet
  },

  getSaihaiModuleNames() {
    return []; // not implemented yet
  },

  getSaihaiSettings() {
    return {}; // not implemented yet
  },

  /**
   * Returns the 'default' build target of an OS
   * This is currently only used when the buildTargetGroup is set (this is done for facebook and requires WebGL)
   * If we add another buildTargetGroup to the buildTarget config then this logic will need to be re-evaluated
   *
   * @returns {string}
   */
  getOSBuildTarget() {
    return 'WebGL';
  },

  getRegistryPackagesPath(editorPath) {
    return path.join(editorPath, '..', 'Data', 'Resources', 'PackageManager', 'Editor');
  },

  getBuiltInPackagesPath(editorPath) {
    return path.join(editorPath, '..', 'Data', 'Resources', 'PackageManager', 'BuiltInPackages');
  },

  /**
   *
   * @param editorFolder path to an editor (up to .../2017.1.0f1/Editor)
   * @return array of paths (to match mac)
   */
  getPlaybackEnginesPathsFromFolder(editorFolder) {
    return [
      path.join(editorFolder, 'Data', 'PlaybackEngines')
    ];
  },
};

module.exports = editorUtils;
