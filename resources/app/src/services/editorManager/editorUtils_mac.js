const plist = require('plist'); // eslint-disable-line import/no-unresolved
const path = require('path');
const { spawn } = require('child_process');

const { fs, hubFS } = require('../../fileSystem');

function getLicenseInfo(editorPath) {
  return new Promise((resolve, reject) => {
    let result = '';
    const codesign = spawn('codesign', ['-dvvv', editorPath]);

    codesign.stdout.on('data', (data) => {
      result += data;
    });

    codesign.stderr.on('data', (data) => {
      result += data;
    });

    codesign.on('close', () => {
      resolve(result);
    });
    codesign.on('error', (err) => {
      reject(err);
    });
  });

}

function getEditorInformation(editorPath) {
  return new Promise((resolve, reject) => {
    try {
      const plistContent = fs.readFileSync(`${editorPath}/Contents/Info.plist`, 'utf8');
      const editorInformation = plist.parse(plistContent);
      resolve(editorInformation);
    } catch (error) {
      reject(error);
    }
  });
}

const editorUtils = {
  /**
   * Returns true if directory path is Editor installed with HUB
   * @param {string} editorLocation e.g. ./Applications/UnityFolder/Unity.app/Contents/MacOS/Unity
   * @returns {Promise<boolean>}
   */
  isEditorInstalledWithHub(editorLocation) {
    return new Promise((resolve, reject) => {
      editorLocation = editorLocation.split(/\\|\//g);
      const index = editorLocation.indexOf('Unity.app');
      if (index >= 0) {
        editorLocation = path.join.apply([], editorLocation.splice(0, index));
        fs.pathExists(path.join(editorLocation, 'modules.json'))
          .then((exists) => resolve(exists))
          .catch((error) => reject(error));
      } else {
        reject('Not Editor path');
      }
    });
  },

  makeEditorInstallsDir(installsPath) {
    return hubFS.elevateAndMakeDir(installsPath);
  },

  getEditorPathFromFolder(editorFolder) {
    return path.join(editorFolder, 'Unity.app');
  },

  /**
   * Returns constant for Unity Editor placement
   * @returns {Object} constant 'EDITOR'.
   * @param {string} key to get constant value directly from object
   * EDITOR - 'Unity.app/Contents/MacOS/Unity'
   */
  getEditorLocationPatterns(key) {
    const editorLocationPatters = {
      EDITOR: path.join('Unity.app', 'Contents', 'MacOS', 'Unity'),
    };
    return key ? editorLocationPatters[key.toUpperCase()] : editorLocationPatters;
  },

  findUnityVersion(editorPath) {
    return getEditorInformation(editorPath).then((editorInformation) => ({
      version: editorInformation.CFBundleVersion,
    }));
  },

  /**
   *
   * @param editorFolder path to an editor (up to .../2017.1.0f1/Editor)
   * @return array of paths (to match mac)
   */
  getPlaybackEnginesPathsFromFolder(editorFolder) {
    return [
      path.join(editorFolder, 'PlaybackEngines'),
      path.join(editorFolder, 'Unity.app', 'Contents', 'PlaybackEngines')
    ];
  },

  /**
   * Returns the 'default' build target of an OS
   * @returns {string}
   */
  getOSBuildTarget() {
    return 'OSXUniversal';
  },

  getRegistryPackagesPath(editorPath) {
    return path.join(editorPath, 'Contents', 'Resources', 'PackageManager', 'Editor');
  },

  getBuiltInPackagesPath(editorPath) {
    return path.join(editorPath, 'Contents', 'Resources', 'PackageManager', 'BuiltInPackages');
  },

  validateEditorFile(editorPath, skipSignatureCheck) {
    return new Promise((resolve, reject) => {
      if (!editorPath) {
        reject({ errorCode: 'ERROR.INVALID_PATH' });
      } else if (!editorPath.endsWith('.app')) {
        reject({ errorCode: 'ERROR.NOT_AN_APP' });
      } else if (!fs.existsSync(editorPath)) {
        reject({ errorCode: 'ERROR.FILE_NOT_FOUND' });
      } else if (skipSignatureCheck) {
        getEditorInformation(editorPath).then((editorInformation) => {
          if (!editorInformation.CFBundleIdentifier.includes('com.unity3d.UnityEditor')) {
            reject({ errorCode: 'ERROR.NOT_AN_UNITY_APP' });
          } else {
            resolve(true);
          }
        }).catch(() => {
          reject({ errorCode: 'ERROR.FAILED_GET_INFO' });
        });
      } else {
        getLicenseInfo(editorPath).then((result) => {
          try {
            const fields = result.split('\n');
            let executable = '';
            let identifier = '';
            let developer = '';
            fields.forEach(line => {
              if (line.startsWith('Executable')) {
                executable = line;
              } else if (line.startsWith('Identifier')) {
                identifier = line;
              } else if (line.startsWith('Authority=Developer ID Application:')) {
                developer = line;
              }
            });

            if (fields.length === 2) {
              reject({ errorCode: 'ERROR.NOT_SIGNED' });
            } else if (!developer.includes('Unity Technologies ApS')) {
              reject({ errorCode: 'ERROR.NOT_SIGNED' });
            } else if (!executable.includes(`${editorPath}/Contents/MacOS/Unity`) ||
              !identifier.includes(('com.unity3d.UnityEditor'))) {
              reject({ errorCode: 'ERROR.NOT_AN_UNITY_APP' });
            } else {
              resolve(true);
            }
          } catch (error) {
            reject({ errorCode: 'ERROR.FAILED_LICENSE_INFO' });
          }
        });
      }
    });
  },

  validateInstallPath() {
    return true;
  },

  getSaihaiSettings() {
    return {
      unityPrefix: '',
      setupPostfix: '',
      platformName: 'Mac',
      platformNamePostfix: '',
      installerFormat: 'pkg',
      targetSupportString: 'MacEditorTargetInstaller',
      configurationPlatformName: 'osx'
    };
  },

  getSaihaiModuleNames() {
    return [
      {
        name: 'Editor',
        nameFull: 'Unity Editor'
      },
      {
        name: 'Linux',
        nameFull: 'Linux Support'
      },
      {
        name: 'WebGL',
        nameFull: 'WebGL Support'
      },
      {
        name: 'Android',
        nameFull: 'Android Support'
      },
      {
        name: 'AppleTV',
        nameFull: 'AppleTV Support'
      },
      {
        name: 'iOS',
        nameFull: 'iOS Support'
      },
      {
        name: 'Windows',
        nameFull: 'Windows Support'
      },
      {
        name: 'Mac-IL2CPP',
        nameFull: 'Mac-IL2CPP Support'
      },
      {
        name: 'Facebook-Games',
        nameFull: 'Facebook-Games Support'
      },
      {
        name: 'Samsung-TV',
        nameFull: 'Samsung-TV Support'
      },
      {
        name: 'Tizen',
        nameFull: 'Tizen Support'
      },
      {
        name: 'Vuforia-AR',
        nameFull: 'Vuforia-AR Support'
      },
      {
        name: 'StandardAssets',
        nameFull: 'Standard Assets'
      },
      {
        name: 'Documentation',
        nameFull: 'Documentation'
      }
    ];
  }
};

module.exports = editorUtils;
