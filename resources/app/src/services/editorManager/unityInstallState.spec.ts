import sinon from 'sinon';
import chai from 'chai';
import sinonChai from 'sinon-chai';
import unityInstallState from './unityInstallState';

chai.use(sinonChai);
const { expect } = chai;

describe('UnityInstallState', () => {
  let sandbox;
  beforeEach(() => {
    sandbox = sinon.sandbox.create();
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('addInstall', () => {
    let installId;
    beforeEach(() => {
      installId = '123';
      unityInstallState.addInstall(installId);
    });

    afterEach(() => {
      unityInstallState.removeInstall(installId);
    });

    it('should add install to list', () => {
      unityInstallState.installError(installId);
      expect(unityInstallState.getFailedInstalls()[0].version).to.eq(installId);
    });
  });

  describe('removeInstall', () => {
    let installId;
    beforeEach(() => {
      installId = '123';
      unityInstallState.addInstall(installId);
      unityInstallState.installError(installId);
    });

    it('should remove install from list', () => {
      expect(unityInstallState.getFailedInstalls()[0].version).to.equal(installId);

      unityInstallState.removeInstall(installId);
      expect(unityInstallState.getFailedInstalls().length).to.equal(0);
    });
  });

  describe('installError', () => {
    let installId;
    beforeEach(() => {
      installId = '123';
      unityInstallState.addInstall(installId);
    });

    afterEach(() => {
      unityInstallState.removeInstall(installId);
    });

    it('should mark install as failed', () => {
      expect(unityInstallState.getFailedInstalls().length).to.equal(0);

      unityInstallState.installError(installId);
      const firstFailedInstall = unityInstallState.getFailedInstalls()[0];
      expect(firstFailedInstall.version).to.equal(installId);
      expect(firstFailedInstall.error).to.be.true;
      expect(firstFailedInstall.errorIsForModules).to.be.false;
    });
  });

  describe('installModulesError', () => {
    let installId;
    beforeEach(() => {
      installId = '123';
      unityInstallState.addInstall(installId);
    });

    afterEach(() => {
      unityInstallState.removeInstall(installId);
    });

    it('should mark install as failed and error is in modules', () => {
      expect(unityInstallState.getFailedInstalls().length).to.equal(0);

      unityInstallState.installModulesError(installId);
      const firstFailedInstall = unityInstallState.getFailedInstalls()[0];
      expect(firstFailedInstall.version).to.equal(installId);
      expect(firstFailedInstall.error).to.be.true;
      expect(firstFailedInstall.errorIsForModules).to.be.true;
    });
  });

  describe('getFailedInstalls', () => {
    it('when no installs are available should return no installs', () => {
      expect(unityInstallState.getFailedInstalls().length).to.equal(0);
    });

    it('when one install is available but have not failed should return no installs', () => {
      unityInstallState.addInstall('123');
      expect(unityInstallState.getFailedInstalls().length).to.equal(0);
      unityInstallState.removeInstall('123');
    });

    it('when one install is available and failed should return failed install', () => {
      const installId = '123';
      unityInstallState.addInstall(installId);
      expect(unityInstallState.getFailedInstalls().length).to.equal(0);

      unityInstallState.installError(installId);
      expect(unityInstallState.getFailedInstalls()[0].version).to.equal(installId);

      unityInstallState.removeInstall(installId);
    });

    it('when two installs are available and none have errors should return no installs', () => {
      const installIds = ['123', '456'];
      installIds.forEach((id) => unityInstallState.addInstall(id));

      expect(unityInstallState.getFailedInstalls().length).to.equal(0);

      installIds.forEach((id) => unityInstallState.removeInstall(id));
    });

    it('when two installs are available and all have errors should return all installs', () => {
      const installIds = ['123', '456'];
      installIds.forEach((id) => unityInstallState.addInstall(id));
      expect(unityInstallState.getFailedInstalls().length).to.equal(0);

      installIds.forEach((id) => unityInstallState.installError(id));

      expect(unityInstallState.getFailedInstalls().length).to.equal(2);

      installIds.forEach((id) => unityInstallState.removeInstall(id));
    });

    it('when two installs are available and one has errors should return right install', () => {
      const installIds = ['123', '456'];
      installIds.forEach((id) => unityInstallState.addInstall(id));
      expect(unityInstallState.getFailedInstalls().length).to.equal(0);

      unityInstallState.installError(installIds[0]);

      expect(unityInstallState.getFailedInstalls().length).to.equal(1);
      expect(unityInstallState.getFailedInstalls()[0].version).to.equal(installIds[0]);

      installIds.forEach((id) => unityInstallState.removeInstall(id));
    });
  });
});
