class UnityInstallInfo {
  version: string;

  error: boolean;

  errorIsForModules: boolean;

  constructor(version: string) {
    this.version = version;
    this.error = false;
    this.errorIsForModules = false;
  }
}

export = UnityInstallInfo;
