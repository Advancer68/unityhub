'use strict'; // mocha

// third parties
const _ = require('lodash');
const os = require('os');
const path = require('path');

const EditorApp = require('../editorApp/editorapp');
const outputService = require('../outputService');
const unityDownload = require('../localDownload/unityDownload');
const unityInstaller = require('../localInstaller/unityInstaller');
const windowManager = require('../../windowManager/windowManager');
const localSettings = require('../localSettings/localSettings');
const { editorHelper } = require('../../common/common');
const { hubFS } = require('../../fileSystem');
const downloadProgressManager = require('./lib/downloadProgressManager');

const EditorList = require('./lib/editorList');
const editorUtils = require('./editorUtils');
const releaseService = require('./unityReleaseService');
const cloudAnalytics = require('../cloudAnalytics/cloudAnalytics');
const storage = require('electron-json-storage');
const logger = require('../../logger')('EditorManager');
const util = require('util');
const hubIPCState = require('../localIPC/hubIPCState');
const eulaFetcher = require('./eulaFetcher');
const unityInstallState = require('./unityInstallState');

const APP_PATH = {
  darwin: path.join('/Applications'),
  win32: process.env.PROGRAMFILES || path.join('C:', 'Program Files'),
  linux: process.env.HOME
}[os.platform()];

const INSTALL_PATH = {
  darwin: path.join(APP_PATH, 'Unity', 'Hub', 'Editor'),
  win32: path.join(APP_PATH, 'Unity', 'Hub', 'Editor'),
  linux: path.join(APP_PATH, 'Unity', 'Hub', 'Editor')
}[os.platform()];

const REMEMBERED_MODULES_KEY = 'moduleSelection';

const errorMessageMapper = {
  0: 'ERROR.NO_EDITOR_LEARN',
  1: '',
  2: ''
};

function generateEditorAppInstance(editor) {
  if (!editor) return undefined;
  return new EditorApp({ editorPath: editor.location[0], version: editor.version });
}

class EditorManager {

  get availableEditors() {
    return this.editorList.availableEditors;
  }

  get currentInstallPath() {
    return this.editorList.secondaryInstallPath || INSTALL_PATH;
  }

  get defaultEditorVersion() {
    return this.editorList.latest ? this.editorList.latest.version : undefined;
  }

  constructor() {
    this.errorCodes = {
      NO_EDITORS_FOUND: 0,
      NO_DEFAULT_EDITOR_FOUND: 1,
      VERSION_NOT_FOUND: 2
    };

    this.editorList = new EditorList(INSTALL_PATH);
  }

  init() {
    logger.info('Init');
    // Initialize internal dependencies.
    return releaseService.init()
      .then(() => this.editorList.init())
      .then(() => this.registerDownloadEvents())
      .then(() => this._initSecondaryInstallLocation());
  }

  registerDownloadEvents() {
    unityDownload.on('download.end.editor', (id, download) => {
      const defaultModules = this.getDefaultReleasesModules(id);
      const downloadedModules = download.getDownloadedModules();
      downloadedModules.forEach((obj) => _.assign(obj, _.find(defaultModules, ['id', obj.id])));
      return this.installEditor(id, download.getEditorInstallPath(), downloadedModules)
        .catch((e) => { logger.warn('There was an unhandled error while installing the Editor.', e); });
    });

    unityDownload.on('download.end.cluster', (id, download) => {
      const defaultModules = this.getEditorDefaultModules(id);
      const downloadedModules = download.getDownloadedModules();
      downloadedModules.forEach((obj) => _.assign(obj, _.find(defaultModules, ['id', obj.id])));
      return this.installModules(id, downloadedModules)
        .catch((e) => { logger.warn('There was an unhandled error while installing modules.', e); });
    });

    unityDownload.on('download.error.editor', (id) => {
      downloadProgressManager.setEditorDownloadProgressToComplete(id);
    });
  }

  /**
   * Opens dialog for selecting the editor
   * @returns {Promise}
   */
  chooseEditor() {
    const args = {
      title: 'Select the Editor you wish to add',
      buttonLabel: 'Select Editor',
      defaultPath: APP_PATH,
      properties: ['openFile']
    };

    return windowManager.mainWindow.showOpenFileDialog(args);
  }

  /**
   * Validate if a path/location is a Unity Editor file or not.
   * @param location
   * @param {Boolean} skipSignatureCheck Whether to skip Unity signature checking
   * @returns {Promise}
   */
  validateEditorFile(location, skipSignatureCheck) {
    return editorUtils.validateEditorFile(location, skipSignatureCheck).catch((err) => {
      logger.info(`failed to validate the Editor file ${err}`);
      return Promise.reject(err);
    });
  }

  validateInstallPath(location) {
    return editorUtils.validateInstallPath(location);
  }

  getJobsInProgress() {

    const inProgressJobs = [];
    Object.values(unityInstaller.jobs)
      .forEach((job) => {
        if (!job.editor || !job.editor.version) {
          logger.info('Could not find the editor of this job', job);
          return;
        }
        const editor = {
          version: job.editor.version,
          location: job.destinationPath,
          manual: false,
          modules: job.modules
        };
        switch (job.status) {
          case unityInstaller.jobStatus.QUEUED:
            switch (job.jobType) {
              case 'editorUninstall':
                editor.status = 'queued_uninstalling';
                break;
              case 'editorInstall':
                editor.status = 'queued_installing';
                break;
              case 'moduleInstall':
                editor.status = 'modules_queued_installing';
                break;
              default:
                logger.warn('invalid editor status');
            }
            break;
          case unityInstaller.jobStatus.INSTALLING:
            editor.status = 'installing';
            break;
          case unityInstaller.jobStatus.UNINSTALLING:
            editor.status = 'uninstalling';
            break;
          default:
            logger.warn('invalid editor status');

        }
        if (editor.status) {
          inProgressJobs.push(editor);
        }
      });

    return inProgressJobs;

  }

  isJobInProgress() {
    const jobs = this.getJobsInProgress();
    return jobs.length > 0;
  }

  /**
   * For a given editor location, finds the version
   * @param editorPath
   * @returns {Promise}
   */
  findVersion(editorPath) {
    return editorUtils.findUnityVersion(editorPath);
  }

  getBuiltInPackagesPath(editorPath) {
    return editorUtils.getBuiltInPackagesPath(editorPath);
  }

  getRegistryPackagesPath(editorPath) {
    return editorUtils.getRegistryPackagesPath(editorPath);
  }

  /**
   * Given a path, adds/overwrites that editor to local storage if it is a valid
   * editor and version is found.
   * It set the default editor if this is the first editor added.
   * @param location
   * @param {Boolean} skipSignatureCheck Whether to skip Unity signature checking
   * @returns {Promise}
   */
  addEditor(location, skipSignatureCheck) {
    return this.validateEditorFile(location, skipSignatureCheck)
      .then(() => this.findVersion(location))
      .then((verInfo) => {
        const newEditor = {
          version: verInfo.version,
          location: [location],
          manual: true,
        };

        const editorWithSameId = _.find(this.editorList.availableEditors, { version: newEditor.version });
        if (editorWithSameId) {
          return Promise.reject({ errorCode: 'ERROR.EDITOR_ALREADY_IN_LIST', params: { version: newEditor.version } });
        }

        return this.editorList.addLocatedEditor(newEditor)
          .then(() => {
            cloudAnalytics.addEvent({
              type: cloudAnalytics.eventTypes.EDITOR_LOCATE,
              msg: {
                editor_version: newEditor.version,
                used_shift_key: skipSignatureCheck,
                status: 'Success',
              },
            });
          })
          .then(() => newEditor)
          .catch((err) => {
            cloudAnalytics.addEvent({
              type: cloudAnalytics.eventTypes.EDITOR_LOCATE,
              msg: {
                editor_version: newEditor.version,
                used_shift_key: skipSignatureCheck,
                status: 'Error',
              },
            });
            return Promise.reject(err);
          });
      });
  }

  /**
   * Given a version, it removes a manually added editor from local storage
   * @param version
   * @returns {Promise}
   */
  removeEditor(version) {
    const editor = this.availableEditors[version];
    return this.editorList.removeLocatedEditor(editor);
  }

  async removeCorruptEditorFolder(editor) {
    return await this.editorList.removeCorruptEditor(editor);
  }

  setFailedDeleteOnEditor(version) {
    const editor = Object.values(this.editorList.availableEditors).find(edt => edt.version === version);
    editor.failedDelete = true;
  }

  isEditorRunning(editor) {
    return editorHelper.isVersionRunning(editor);
  }

  uninstallEditor(version) {
    const editor = this.availableEditors[version];
    if (!editor) {
      return Promise.reject('This Editor cannot be uninstalled.');
    }

    return unityInstaller.platformDependent.prepareInstallService()
      .then(() => Promise.all(editor.location.map((editorPath) => {
        const editorApp = new EditorApp({ editorPath, version: editor.version });
        return unityInstaller.uninstall(version, editorApp.path);
      })))
      .then((responses) => {
        let uninstallCancelled = false;
        responses.forEach((response) => {
          if (response && response.cancelled) {
            uninstallCancelled = true;
          }
        });

        if (uninstallCancelled) {
          outputService.notifyContent('editor-uninstall.cancel', version);
          return Promise.resolve({ cancelled: true });
        }

        return this.editorList.removeInstalledEditor(editor);
      })
      .catch(() => Promise.reject(`The Editor ${version} could not be uninstalled.`));
  }

  /**
   * Opens a dialog for choosing an editor and then adding it
   * @param {Boolean} skipSignatureCheck Whether to skip Unity signature checking
   * @returns {Promise}
   */
  chooseAndAddEditor(skipSignatureCheck) {
    return this.chooseEditor().then((location) => this.addEditor(location, skipSignatureCheck))
      .catch((err) => {
        if (err === 'No file selected') {
          return Promise.reject();
        }
        return Promise.reject(err);
      });
  }

  /**
   * Automatically locate the editor that is connected to the hub
   * @private
   * @param location
   */
  async checkAutoLocate(location) {
    const { version } = await this.findVersion(location);
    const editorWithTheSameVersion = this.availableEditors[version];
    if (!editorWithTheSameVersion || editorWithTheSameVersion.location.indexOf(location) < 0) {
      return this.addEditor(location, true).catch((err) => Promise.reject(err));
    }
    return Promise.resolve();
  }

  /**
   * Given a version returns the information of the editor.
   * Result format:
   * {
   *   version: 2017.1f1,
   *   location: /path/to/editor.app (or .exe)
   * }
   * @param version
   * @returns {Promise}
   */
  getEditor(version) {
    return Promise.resolve()
      .then(() => {
        const editor = this.availableEditors[version];
        if (!editor) {
          const code = _.isEmpty(this.availableEditors) ? this.errorCodes.NO_EDITORS_FOUND : this.errorCodes.VERSION_NOT_FOUND;
          const error = new Error(`Unity ${version} could not be retrieved.`);
          error.code = code;
          error.errorCode = errorMessageMapper[code];

          throw error;
        }

        return editor;
      });
  }

  /**
   * Returns the default editor version.
   * @deprecated use getter instead
   * @returns {*}
   */
  getDefaultEditorVersion() {
    if (this.editorList && this.editorList.latest) {
      return Promise.resolve(this.editorList.latest.version);
    }
    return Promise.resolve();

  }

  /**
   * Returns the default editor. Format the same as getEditor
   */
  getDefaultEditor() {
    return Promise.resolve(this.editorList.latest);
  }

  /**
   * Gets the default editor app
   * @returns {EditorApp} The editor app selected by default.
   */
  getDefaultEditorApp() {
    return this.getDefaultEditor()
      .then(editor => generateEditorAppInstance(editor));
  }

  /**
   * Gets specific version of editor if available. Otherwise just the default.
   * @param version {String} The version to fetch.
   * @returns {EditorApp} The editor app of the given version or the default.
   */
  getEditorApp(version) {
    return this.getEditor(version)
      .then(editor => generateEditorAppInstance(editor))
      .catch((error) => {
        if (error.code === this.errorCodes.NO_EDITORS_FOUND) {
          throw error;
        }
        return this.getDefaultEditorApp();
      });
  }

  /**
   * For now, this code only considers that either the editor and the modules are found on a local drive
   * or all of them are downloaded from the web.
   * @param editor
   * @param modules
   * @returns {Promise.<T>|Q.Promise<any>|promise.Promise<any>|*}
   */
  downloadUnityEditor(editor, modules) {
    let downloadDest;

    return downloadProgressManager.startTrackingEditorDownloadProgress(editor.version)
      .then(() => {
        unityInstallState.addInstall(editor.id);
        downloadDest = hubFS.getTmpDirPath();
        return this.hasEnoughSpaceOnDisk(editor, modules, downloadDest, false);
      })
      .then(() => unityInstaller.platformDependent.prepareInstallService())
      .then(() => {
        this.addToCustomEditorInProgress(editor);
        if (!hubFS.startsWithHttp(editor.downloadUrl)) {
          return unityDownload.copyUnityEditor(editor, modules, downloadDest)
            .then(([editorInstallPath, copiedModules]) => {
              const defaultModules = this.getDefaultReleasesModules(editor.id);
              copiedModules.forEach((obj) => _.assign(obj, _.find(defaultModules, ['id', obj.id])));
              return this.installEditor(editor.id, editorInstallPath, copiedModules)
                .catch((e) => {
                  unityInstallState.installError(editor.id);
                  logger.error('There was an unhandled error while downloading/installing the editor:', e);
                  releaseService.removeCustomEditorInProgress(editor);
                });
            })
            .catch((e) => {
              logger.error('Something went wrong while trying to copy and install files');
              throw e;
            });
        }
        return unityDownload.downloadUnityEditor(editor, modules, downloadDest);
      })
      .catch((e) => {
        unityInstallState.installError(editor.id);
        outputService.notifyContent('download.cancel', editor.id);
        logger.error('editor download/install cancelled because of', e);
      });
  }

  /**
   * For now, this implies that we either have modules from a local network (and we only copy the files)
   * or all of them need to be downloaded.
   * Therefore, this code does not take into account a case where some downloadUrl
   * would be local and others on the web.
   * @param editor
   * @param modules
   * @returns {Promise.<T>|Q.Promise<any>|promise.Promise<any>|*}
   */
  downloadEditorModules(editor, modules) {
    unityInstallState.addInstall(editor.id);
    const downloadDest = hubFS.getTmpDirPath();
    return this.hasEnoughSpaceOnDisk(editor, modules, downloadDest, true)
      .then(() => unityInstaller.platformDependent.prepareInstallService())
      .then(() => {
        if (!hubFS.startsWithHttp(modules[0].downloadUrl)) {
          return unityDownload.copyEditorModules(editor, modules)
            .then((copiedModules) => {
              if (copiedModules && copiedModules.length > 0) {
                const defaultModules = this.getEditorDefaultModules(editor.id);
                copiedModules.forEach((obj) => _.assign(obj, _.find(defaultModules, ['id', obj.id])));
                return this.installModules(editor.id, copiedModules)
                  .catch((e) => {
                    unityInstallState.installModulesError(editor.id);
                    logger.error('There was an unhandled error while installing modules:', e);
                  });
              }
              const filesListing = _.map(modules, 'name').join(', ');
              logger.info(`${editor.id} Components copy ended with error. All files could not be copied (${filesListing})`);
              unityInstallState.installModulesError(editor.id);
              outputService.notifyContent('download.error.cluster', 'ERROR.COPY_FAIL_ALL', {
                id: editor.id,
                i18n: { filesListing }
              });

              return Promise.reject();
            });
        }

        return unityDownload.downloadEditorModules(editor, modules);
      })
      .catch((e) => {
        unityInstallState.installModulesError(editor.id);
        outputService.notifyContent('download.cancelModule', editor.id);
        logger.error('module download/install cancelled because of', e);
      });
  }

  /**
   * if the editor asked for is the custom editor
   * and it is not already part of the list of downloadable editors,
   * then add it to the list of custom editors in progress
   * @param editor
   */
  addToCustomEditorInProgress(editor) {
    const currentCustomEditor = releaseService.getCustomEditorInfo();
    if (currentCustomEditor.version && currentCustomEditor.version === editor.version && !this.availableEditors[editor.version]) {
      logger.debug(JSON.stringify(editor));
      releaseService.addCustomEditorInProgress(editor);
    }
  }
  /**
   * Download and installation could be done on 2 different disks
   * If space is missing for either, we should warn the user and reject the promise.
   * @param editor Information about the current editor to install or to add modules to. Knows where the installs will be
   * @param modules Information about the modules to install. Could be empty
   * @param downloadDest Where to download the required files
   * @param onlyModules Whether we should add the dl/install size of the editor or not to the disk size required
   */
  hasEnoughSpaceOnDisk(editor, modules, downloadDest, onlyModules) {
    return this.getRequiredSizesPerDisk(editor, modules, downloadDest, onlyModules)
      .then((diskList) => {
        for (const disk of Object.keys(diskList)) {
          if (diskList[disk].available < diskList[disk].totalSizeRequired) {
            outputService.notifyContent('download.nospace', 'ERROR.NOT_ENOUGH_SPACE', { id: editor.id });
            return Promise.reject('Not enough space on disk to download and install');
          }
        }

        return Promise.resolve();
      });
  }

  getRequiredSizesPerDisk(editor, modules, downloadDest, onlyModules) {
    const diskInfo = {};
    let downloadDiskRootPath = '';
    let installDiskRootPath = '';

    try {
      downloadDiskRootPath = hubFS.getDiskRootPath(downloadDest);
      const editorPath = editor.location ? editor.location[0] : this.currentInstallPath;
      installDiskRootPath = hubFS.getDiskRootPath(editorPath);
    } catch (e) {
      logger.error('Could not validate size on disk for download because of ', e);
      return Promise.resolve();
    }

    return hubFS.getDiskUsage(downloadDiskRootPath)
      .then((info) => {
        diskInfo[downloadDiskRootPath] = {
          totalSizeRequired: 0,
          available: parseInt(info.available, 10)
        };
      })
      .then(() => hubFS.getDiskUsage(installDiskRootPath))
      .then((info) => {
        if (!diskInfo[installDiskRootPath]) {
          diskInfo[installDiskRootPath] = {
            totalSizeRequired: 0,
            available: parseInt(info.available, 10)
          };
        }
      })
      .then(() => {
        _.each(modules, (module) => {
          diskInfo[downloadDiskRootPath].totalSizeRequired += parseInt(module.downloadSize, 10);
          diskInfo[installDiskRootPath].totalSizeRequired += parseInt(module.installedSize, 10);
        });

        if (!onlyModules) {
          diskInfo[downloadDiskRootPath].totalSizeRequired += parseInt(editor.downloadSize, 10);
          diskInfo[installDiskRootPath].totalSizeRequired += parseInt(editor.installedSize, 10);
        }

        return Promise.resolve(diskInfo);
      })
      .catch((e) => {
        logger.error('Could not calculate required size on disk for download because of', e);
        return Promise.resolve({});
      });
  }

  /**
   * Gets the disk usage information for the path given
   * If no path is given, it will return the usage information for the default install path
   * @param folder
   * @returns {*}
   */
  getDiskSpaceAvailable(folder) {
    folder = folder || this.currentInstallPath;
    return hubFS.getDiskSpaceAvailable(folder);
  }

  cancelDownload(editor) {
    releaseService.removeCustomEditorInProgress(editor);
    unityDownload.cancelDownload(editor.version);
  }

  installEditor(version, installerPath, modules) {
    const destinationPath = path.join(this.currentInstallPath, version);
    const release = _.find(releaseService.releases, { version });
    cloudAnalytics.addEvent({
      type: cloudAnalytics.eventTypes.EDITOR_INSTALL_START,
      msg: {
        editor_version: version,
        download_id: version, // todo Afshin: This should be replaced by download unique id
        downloaded_components: modules ? modules.map((module) => module.id) : [],
        destination: 'Primary'// todo should be changed to primary or secondary
      },
    });
    modules.forEach((module) => {
      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.COMPONENT_INSTALL_START,
        msg: {
          component_id: module.id,
          editor_version: version,
          download_id: version, // todo Afshin: This should be replaced by download unique id
          destination: 'Primary'// todo should be changed to primary or secondary
        },
      });
    });

    return unityInstaller.installEditor(release, installerPath, destinationPath, modules)
      .catch((e) => {
        logger.warn('There was an unhandled error while installing the editor:', e);
        unityInstallState.installError(version);
        releaseService.removeCustomEditorInProgress(release);
        return e;
      })
      .then(async (response) => {
        // Editor failed to install or installation was cancelled. Abort the install.
        if (response && (response.error || response.cancelled)) {
          if (response.error) {
            unityInstallState.installError(version);
          } else {
            unityInstallState.removeInstall(version);
          }

          cloudAnalytics.addEvent({
            type: cloudAnalytics.eventTypes.EDITOR_INSTALL_END,
            msg: {
              download_id: version, // todo Afshin: This should be replaced by download unique id
              status: response.error ? 'Error' : 'Cancel'
            },
          });
          modules.forEach((module) => {
            cloudAnalytics.addEvent({
              type: cloudAnalytics.eventTypes.COMPONENT_INSTALL_END,
              msg: {
                download_id: version, // todo Afshin: This should be replaced by download unique id
                component_id: module.moduleId,
                status: 'Error'
              },
            });
          });
          outputService.notifyContent(unityInstaller.installerEvents.END, version, response);
          return _.pick(response, ['error', 'cancelled']);
        }
        releaseService.removeCustomEditorInProgress(release);
        await downloadProgressManager.setEditorDownloadProgressToComplete(version);
        return this.editorList.addInstalledEditor(version)
          .then((editor) => {

            const ret = {
              editorPath: editor.location[0],
              isDefault: editor.isDefault,
              modules: response.modules,
              updatedModules: editor.modules,
              buildPlatforms: editor.buildPlatforms,
              restartRequired: response.restartRequired
            };
            response.modules.successful.forEach((module) => {
              cloudAnalytics.addEvent({
                type: cloudAnalytics.eventTypes.COMPONENT_INSTALL_END,
                msg: {
                  download_id: version, // todo Afshin: This should be replaced by download unique id
                  component_id: module.id,
                  status: 'Success'
                },
              });
            });

            response.modules.failed.forEach((module) => {
              cloudAnalytics.addEvent({
                type: cloudAnalytics.eventTypes.COMPONENT_INSTALL_END,
                msg: {
                  download_id: version, // todo Afshin: This should be replaced by download unique id
                  component_id: module.id,
                  status: 'Error'
                },
              });
            });

            cloudAnalytics.addEvent({
              type: cloudAnalytics.eventTypes.EDITOR_INSTALL_END,
              msg: {
                download_id: version, // todo Afshin: This should be replaced by download unique id
                component_id: module.id,
                status: 'Success'
              },
            });
            unityInstallState.removeInstall(version);
            outputService.notifyContent(unityInstaller.installerEvents.END, version, ret);
            return ret;

          }).catch((err) => {
            cloudAnalytics.addEvent({
              type: cloudAnalytics.eventTypes.EDITOR_INSTALL_END,
              msg: {
                download_id: version, // todo Afshin: This should be replaced by download unique id
                status: 'Error'
              },
            });
            modules.forEach((module) => {
              cloudAnalytics.addEvent({
                type: cloudAnalytics.eventTypes.COMPONENT_INSTALL_END,
                msg: {
                  download_id: version, // todo Afshin: This should be replaced by download unique id
                  component_id: module.id,
                  status: 'Error'
                },
              });
            });
            throw err;
          });
      });
  }

  installModules(version, modules) {
    const editor = this.availableEditors[version];
    const editorApp = generateEditorAppInstance(editor);
    const baseInstallPath = editorApp.path;
    modules.forEach((module) => {
      cloudAnalytics.addEvent({
        type: cloudAnalytics.eventTypes.COMPONENT_INSTALL_START,
        msg: {
          component_id: module.id,
          editor_version: version,
          download_id: version, // todo Afshin: This should be replaced by download unique id
          destination: 'Primary' // todo should be changed to primary or secondary
        },
      });
    });
    return unityInstaller.installModules(editor, baseInstallPath, modules)
      .catch((e) => {
        unityInstallState.installModulesError(version);
        logger.warn('There was an unhandled error while installing the modules', e);
        return e;
      })
      .then((response) => {
        // Editor failed to install or installation was cancelled. Abort the install.
        if (response && (response.error || response.cancelled)) {
          if (response.error) {
            unityInstallState.installModulesError(version);
          } else {
            unityInstallState.removeInstall(version);
          }

          modules.forEach((module) => {
            cloudAnalytics.addEvent({
              type: cloudAnalytics.eventTypes.COMPONENT_INSTALL_END,
              msg: {
                download_id: version, // todo Afshin: This should be replaced by download unique id
                component_id: module.id,
                status: 'Error'
              },
            });
          });
          return _.pick(response, ['error', 'cancelled']);
        }

        return this.editorList.updateInstalledEditor(version)
          .then((updatedEditor) => {

            const ret = {
              modules: response.modules,
              updatedModules: updatedEditor.modules,
              buildPlatforms: updatedEditor.buildPlatforms,
              restartRequired: response.restartRequired
            };

            response.modules.successful.forEach((module) => {
              cloudAnalytics.addEvent({
                type: cloudAnalytics.eventTypes.COMPONENT_INSTALL_END,
                msg: {
                  download_id: version, // todo Afshin: This should be replaced by download unique id
                  component_id: module.id,
                  status: 'Success'
                },
              });
            });

            response.modules.failed.forEach((module) => {
              cloudAnalytics.addEvent({
                type: cloudAnalytics.eventTypes.COMPONENT_INSTALL_END,
                msg: {
                  download_id: version, // todo Afshin: This should be replaced by download unique id
                  component_id: module.id,
                  status: 'Error'
                },
              });
            });

            unityInstallState.removeInstall(version);
            outputService.notifyContent(unityInstaller.installerEvents.MODULES_END, editor.version, ret);

            return ret;
          }).catch((err) => {
            modules.forEach((module) => {
              cloudAnalytics.addEvent({
                type: cloudAnalytics.eventTypes.COMPONENT_INSTALL_END,
                msg: {
                  download_id: version, // todo Afshin: This should be replaced by download unique id
                  component_id: module.moduleId,
                  status: 'Error'
                },
              });
            });
            throw err;
          });
      });
  }

  cancelInstallEditor(version) {
    return unityInstaller.cancel(version);
  }

  /**
   * @returns {Array} The modules list from the modules.json or releases file for the given editor version
   * @param version
   */
  getEditorDefaultModules(version) {
    // try to see if there is a modules.json
    if (this.availableEditors[version]) {
      return this.availableEditors[version].modules;
    }

    // otherwise, revert to default releases list
    return this.getDefaultReleasesModules(version);
  }

  /**
   * @returns {Array} The modules list from the releases file for the given editor version
   * @param version
   */
  getDefaultReleasesModules(version) {
    const release = _.find(releaseService.releases, { version });
    return release ? release.modules : [];
  }

  /**
   * Returns the list of releases available (official, beta)
   */
  getReleases() {
    return releaseService.releases;
  }

  /**
   * Make sure to close any subprocess, when needed
   */
  closeInstallService() {
    unityInstaller.platformDependent.closeInstallService();
  }

  getSecondaryInstallLocation() {
    return this.editorList.secondaryInstallPath;
  }

  setSecondaryInstallLocation(installPath) {
    return this.editorList.setSecondaryInstallLocation(installPath);
  }

  chooseSecondaryInstallLocation(window = 'mainWindow') {
    const args = {
      title: 'Select a location to install Unity',
      buttonLabel: 'Select Folder',
      defaultPath: APP_PATH,
      properties: ['openDirectory']
    };

    return windowManager[window].showOpenFileDialog(args);
  }

  isVisualStudioInstalled() {
    return unityInstaller.platformDependent.isVisualStudioInstalled();
  }

  async getEulaText(module) {
    try {
      const eulaText = await eulaFetcher.fetch(module);
      return eulaText;
    } catch (err) {
      logger.warn(`Could not fetch eula text for ${module.name}, reason: ${err}`);
      return '';
    }
  }

  /**
   * Persists the modules selection in local storage to make this modules automatically selected on future installations
   * @param {array} selection Array of selected modules
   */
  async rememberSelectedModules(selection = []) {
    if (Array.isArray(selection) === true) {
      try {
        await util.promisify(storage.set)(REMEMBERED_MODULES_KEY, selection.map(module => module.id));
      } catch (e) {
        logger.warn(`failed to persist module selection, details ${e}`);
      }
    }
  }

  async getPersistedModuleSelection() {
    try {
      let selection = await util.promisify(storage.get)(REMEMBERED_MODULES_KEY);
      selection = Array.isArray(selection) ? selection : null;
      return selection;
    } catch (e) {
      logger.warn(e);
      return null;
    }
  }

  getModalEditor() {
    if (!hubIPCState.modalEditor) return undefined;

    return {
      version: hubIPCState.editorVersion,
      location: hubIPCState.editorLocation
    };
  }

  clearModalEditor() {
    hubIPCState.clearModalEditor();
  }

  _initSecondaryInstallLocation() {
    if (_.isEmpty(this.getSecondaryInstallLocation())) {
      const secondaryInstallLocation = localSettings.get(localSettings.keys.MACHINE_WIDE_SECONDARY_INSTALL_LOCATION);
      if (!_.isEmpty(secondaryInstallLocation)) {
        logger.info(`Machine wide secondary install location is configured. Setting secondary install location to: ${secondaryInstallLocation}`);
        this.setSecondaryInstallLocation(secondaryInstallLocation);
      }
    }

    return Promise.resolve();
  }

  getFailedInstalls() {
    const failedInstalls = unityInstallState.getFailedInstalls();

    failedInstalls.forEach((installInfo) => {
      unityInstallState.removeInstall(installInfo.version);
    });

    return failedInstalls;
  }
}

module.exports = new EditorManager();
