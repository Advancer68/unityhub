// internal
const editorUtils = require('../editorUtils.js');
const { fs } = require('../../../fileSystem');

// third parties
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const os = require('os');
const path = require('path');

// TODO: These tests should be fixed with HUB-2456, they only run if you are developing with linux (or in the CI pipeline)
if (os.platform() === 'linux') { // test linux only
  chai.use(chaiAsPromised);
  chai.use(sinonChai);

  describe('EditorUtils_linux', () => {
    let sandbox;

    beforeEach(() => {
      sandbox = sinon.sandbox.create();
    });

    afterEach(() => {
      sandbox.restore();
    });

    describe('findBuildPlatforms', () => {
      it('should find the build platform buildTarget, buildTargetGroup and name of a given editor when it has the facebook platform', () => {
        sandbox.stub(fs, 'readdir')
          .onFirstCall().returns(Promise.resolve(['Facebook']))
          .onSecondCall().returns(Promise.resolve([]));

        return editorUtils.findBuildPlatforms('/home/user/Editor/2018.1.0b5')
          .then(buildPlatforms => {
            expect(buildPlatforms).to.eql([
              {
                "buildTarget": "WebGL",
                "buildTargetGroup": "facebook",
                "dirName": "Facebook",
                "name": "Facebook"
              }
            ]);
          });
      });
    });

    describe('getRegistryPackagesPath', () => {
      it('should get the correct path for the registry packages', () => {
        const editorPath = path.join('testing', 'path');

        const builtInPackagesPath = editorUtils.getRegistryPackagesPath(editorPath);

        expect(builtInPackagesPath).to.equal('testing/Data/Resources/PackageManager/Editor');
      });
    });

    describe('getBuiltInPackagesPath', () => {
      it('should get the correct path for the built in packages', () => {
        const editorPath = path.join('testing', 'path');

        const builtInPackagesPath = editorUtils.getBuiltInPackagesPath(editorPath);

        expect(builtInPackagesPath).to.equal('testing/Data/Resources/PackageManager/BuiltInPackages');
      });
    });
  });
}
