// internal
const EditorSearch = require('./../lib/editorSearch');
const editorUtils = require('./../editorUtils');
const { fs } = require('../../../fileSystem');
// third parties
const platform = require("os").platform();
const pathutil = require('path');
const postal = require('postal');
const os = require('os');

// Mocks
const FsMock = require('./fsMock');

//tests
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
  .use(require('sinon-chai'))
  .use(require('chai-as-promised'));

if (os.platform() !== 'linux') {//test only on Mac & wind

  describe('EditorSearch', () => {
    let sandbox,
      fsMock,
      basePaths,
      levels,
      basePath,
      isEditorInstalledWithHubStub,
      postalSpy;

    beforeEach(() => {
      sandbox = sinon.sandbox.create();
      fsMock = new FsMock(fs);
      fsMock.stubWithSandbox(sandbox);
      sandbox.stub(postal, 'publish');
      /**
       *  EDITOR_OLD: editorUtils.getEditorLocationPatterns('editor_old'), // 4.7 and lower
       EDITOR: editorUtils.getEditorLocationPatterns('editor')
       */
      let gelp = sandbox.stub(editorUtils, 'getEditorLocationPatterns');
      if (platform === 'win32') {
        gelp.withArgs('editor').returns('Unity.exe');
        gelp.withArgs('editor_old').returns(pathutil.join('Editor', 'Unity.exe'));
      } else {
        gelp.withArgs('editor').returns(pathutil.join('Unity.app', 'Contents', 'MacOS', 'Unity'));
        gelp.withArgs('editor_old').returns(undefined);
      }

      basePath = 'fakePath';
      basePaths = ['fakePath', 'fakePath2'];
      levels = 4;
    });

    afterEach(() => {
      sandbox.restore();
    });

    describe('new instance', () => {
      let editorSearch, editorSearch2;
      beforeEach(() => {
        editorSearch = new EditorSearch(basePath, levels);
        editorSearch2 = new EditorSearch(basePaths, levels);
      })

      it('editorPaths should be set to empty Array', () => {
        expect(editorSearch.editorPaths).to.eql([]);
      });

      it('levels property should be set to ' + levels, () => {
        expect(editorSearch.levels).to.eql(levels);
      });

      it('startPath property should be set to ' + basePath, () => {
        expect(editorSearch.startPath).to.eql(basePath);
      });
      // when Array is passed as first parameter and set to startPath property
      it('editorPaths should be set to empty Array', () => {
        expect(editorSearch2.editorPaths).to.eql([]);
      });

      it('levels property should be set to ' + levels, () => {
        expect(editorSearch2.levels).to.eql(levels);
      });

      it('startPath property should be set to ' + basePaths, () => {
        expect(editorSearch2.startPath).to.eql(basePaths);
      });

    });

    describe('setting values after creating new instance', () => {
      let editorSearch;
      beforeEach(() => {
        editorSearch = new EditorSearch(basePath, levels);
      });

      it('it should set new values of instance properties', () => {
        editorSearch.startPath = "newPath";
        editorSearch.levels = 6;
        expect(editorSearch.startPath).to.eql("newPath");
        expect(editorSearch.levels).to.eql(6);
      });
    });

    describe('editor search', () => {
      let editorSearch;
      beforeEach(() => {
        editorSearch = new EditorSearch(basePath, levels);
      });

      describe('search for editors when basePath doesn\'t exist', () => {
        it('should return empty Array in promise', (done) => {
          editorSearch.search().then((res) => {
            try {
              expect(res).to.eql([]);
              expect(editorSearch.getEditorPaths()).to.eql([]);
              done();
            } catch (e) {
              done(e);
            }
          });
        });

        it('should return empty Array in callback', (done) => {
          editorSearch.search((err, res) => {
            try {
              expect(res).to.eql([]);
              expect(editorSearch.getEditorPaths()).to.eql([]);
              done();
            } catch (e) {
              done(e);
            }
          });
        });

        it('should return empty Array in via postal event "done"', (done) => {
          try {
            editorSearch.search().then(() => {
              expect(postal.publish).to.have.been.calledWith({
                channel: 'EditorSearch',
                topic: 'done',
                data: []
              });
              done();
            });
          } catch (e) {
            done(e);
          }
        });
      });

      // Error gets logged with logger class
      describe('search for editors when given basePath throws error', () => {
        const error = {
          code: 'foo',
          message: 'Bar'
        };

        beforeEach(() => {
          fsMock.setPathThatThrowsError(basePath, error);
        });

        it('should return empty Array', (done) => {
          editorSearch.search().then(res => {
            try {
              expect(res).to.eql([]);
              done();
            } catch (e) {
              done(e);
            }
          })
        });
      });

      describe('search for editors when fs is simulated in the way that editors exist', () => {
        const trashFolderName = platform === 'win32' ? "$RECYCLE.BIN" : ".Trash";
        beforeEach(() => {
          fsMock.setDir({
            "fakePath": {
              "random1": null,
              "random2": null,
              "UnityInstallations": {
                "UnityOld": {
                  "Unity.exe": null
                },
                "UnityNew": {
                  "Editor": {
                    "Unity.exe": null
                  }
                },
                "UnityInstaledWithHub": {
                  "Editor": {
                    "Unity.exe": null,
                  },
                  "modules.json": null
                },
                "UnityMac": {
                  "Unity.app": {
                    "Contents": {
                      "MacOS": {
                        "Unity": null
                      }
                    }
                  }
                },
                "UnityMacInstalledWithHub": {
                  "modules.json": null,
                  "Unity.app": {
                    "Contents": {
                      "MacOS": {
                        "Unity": null
                      }
                    }
                  }
                }
              },
              "random3": {
                "random_sub1": {
                  "random_sub2": {
                    "Editor": {
                      "no_unity_here.exe": null
                    }
                  }
                }
              },
              "random4": null,
              [trashFolderName]: {
                "win": {
                  "Editor": {
                    "Unity.exe": null
                  }
                },
                "mac": {
                  "Unity.app": {
                    "Contents": {
                      "MacOS": {
                        "Unity": null
                      }
                    }
                  }
                }
              }
            }
          });
        });

        it('Find all Editors', (done) => {
          /**
           * If there is more Editors returned than expected,
           * Maybe editors in recycle bin got included, that wouldn't be expected behaviour
           */
          editorSearch.search().then(res => {
            if (platform === 'win32') {
              try {
                expect(res.length).to.eql(3);
                done();
              } catch (e) {
                done(e);
              }

            } else if (platform === 'darwin') {
              try {
                expect(res.length).to.eql(2);
                done();
              } catch (e) {
                done(e);
              }
            } else {
              done(); // todo implement the test for linux
            }
          });
        });

        it('Check if editor paths were cached', (done) => {
          if (platform === 'win32') {
            editorSearch.search().then((res) => {
              try {
                expect(editorSearch.getEditorPaths().length).to.eql(3);
                done();
              } catch (e) {
                done(e);
              }
            });
          } else if (platform === 'darwin') {
            editorSearch.search().then((res) => {
              try {
                expect(editorSearch.getEditorPaths().length).to.eql(2);
                done();
              } catch (e) {
                done(e);
              }
            });
          } else {
            done(); // todo implement the test for linux
          }
        });

        describe("Get editors installed only with hub", () => {

          beforeEach(async () => {
            isEditorInstalledWithHubStub = sinon.stub(editorUtils, 'isEditorInstalledWithHub').returns(true);
            return await editorSearch.search();
          });

          afterEach(() => {
            isEditorInstalledWithHubStub.restore();
          });

          it('promise without passing paths', async () => {
            let res = await editorSearch.getEditorsInstalledWithHub()
            expect(res.length).to.eql(platform === 'win32' ? 3 : 2);
          });

          it('promise with paths as argument', async () => {
            let res = await editorSearch.getEditorsInstalledWithHub(editorSearch.getEditorPaths());
            expect(res.length).to.eql(platform === 'win32' ? 3 : 2);
          });

          it('callback without passing paths', (done) => {
            editorSearch.getEditorsInstalledWithHub((err, res) => {
              try {
                expect(res.length).to.eql(platform === 'win32' ? 3 : 2);
                done();
              } catch (e) {
                done(e);
              }
            });
          });

          it('callback with paths as argument', (done) => {
            editorSearch.getEditorsInstalledWithHub(editorSearch.getEditorPaths(), (err, res) => {
              try {
                expect(res.length).to.eql(platform === 'win32' ? 3 : 2);
                done();
              } catch (e) {
                done(e);
              }
            });
          });
        });

        it('Should find Editor when given install folder too', (done) => {
          if (platform === 'win32') {
            new EditorSearch(pathutil.join('fakePath', 'UnityInstallations', 'UnityOld'), 2).search().then((res) => {
              try {
                expect(res.length).to.eql(1);
                done();
              } catch (e) {
                done(e);
              }
            });
          } else if (platform === 'darwin') {
            new EditorSearch(pathutil.join('fakePath', 'UnityInstallations', 'UnityMac'), 2).search().then((res) => {
              try {
                expect(res.length).to.eql(1);
                done();
              } catch (e) {
                done(e);
              }
            })
          } else {
            done(); // todo implement for linux
          }
        });
      });
    });
  });
}
