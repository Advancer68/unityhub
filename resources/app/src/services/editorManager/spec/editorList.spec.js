// internal
const EditorList = require('../lib/editorList');
const editorUtils = require('../editorUtils');
const { fs } = require('../../../fileSystem');
const downloadProgressManager = require('../lib/downloadProgressManager');

// Mocks
const InstalledEditorListMock = require('./installedEditorListMock');
const LocatedEditorStorageMock = require('./locatedEditorStorageMock');

// third parties
const _ = require('lodash');

const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
  .use(require('sinon-chai'))
  .use(require('chai-as-promised'));


describe('EditorList', () => {
  let sandbox,
    installPath,
    editorList,
    installedEditorListMock,
    locatedEditorStorageMock;

  beforeEach(() => {
    sandbox = sinon.sandbox.create();

    installPath = 'my/install/path';

    sandbox.stub(editorUtils, 'findBuildPlatforms');

    installedEditorListMock = new InstalledEditorListMock();
    installedEditorListMock.stubWithSandbox(sandbox);

    locatedEditorStorageMock = new LocatedEditorStorageMock();
    locatedEditorStorageMock.stubWithSandbox(sandbox);

    editorList = new EditorList(installPath);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('new instance', () => {
    it('should initialize availableEditors to empty list', () => {
      expect(editorList.availableEditors).to.eql({});
    });

    it('should initialize secondaryInstallPath to empty string', () => {
      expect(editorList.secondaryInstallPath).to.eql('');
    });
  });


  describe('init', () => {

    describe('when no editors are on machine', () => {
      beforeEach(() => {
        sandbox.stub(downloadProgressManager, 'isEditorDownloadComplete').resolves(true);
        return editorList.init();
      });

      it('should not update available editors', () => {
        expect(editorList.availableEditors).to.eql({});
      });
    });

    describe('when editors are installed', () => {
      let editor;
      beforeEach(() => {
        sandbox.stub(downloadProgressManager, 'isEditorDownloadComplete').resolves(true);
        editor = { version: 'foo', location: ['fake'] };
        installedEditorListMock.editors[editor.version] = editor;
        return editorList.init();
      });

      it('should include installed editors in available editors', () => {
        expect(editorList.availableEditors[editor.version]).to.eql(editor);
      });
    });

    describe('when editors are located', () => {
      let editor;
      beforeEach(() => {
        sandbox.stub(downloadProgressManager, 'isEditorDownloadComplete').resolves(true);
        editor = { version: 'foo', location: ['fake'] };
        locatedEditorStorageMock.editors[editor.version] = editor;
        return editorList.init();
      });

      it('should include located editors in available editors', () => {
        expect(editorList.availableEditors[editor.version]).to.eql(editor);
      });
    });

    describe('when editors are both installed and located', () => {
      let installedEditor,
        locatedEditor;
      beforeEach(() => {
        sandbox.stub(downloadProgressManager, 'isEditorDownloadComplete').resolves(true);
        installedEditor = { version: 'foo', location: ['fake'] };
        locatedEditor = { version: 'bar', location: ['fake'] };

        installedEditorListMock.editors[installedEditor.version] = installedEditor;
        locatedEditorStorageMock.editors[locatedEditor.version] = locatedEditor;

        return editorList.init();
      });

      it('should include installed editors in available editors', () => {
        expect(editorList.availableEditors[installedEditor.version]).to.eql(installedEditor);
      });

      it('should include located editors in available editors', () => {
        expect(editorList.availableEditors[locatedEditor.version]).to.eql(locatedEditor);
      });
    });

    describe('when editor last download status was in progress', () => {
      let editor;
      beforeEach(() => {
        sandbox.stub(downloadProgressManager, 'isEditorDownloadComplete').resolves(false);
        editor = { version: 'foo', location: ['fake'] };
        installedEditorListMock.editors[editor.version] = editor;
        return editorList.init();
      });

      it('should set editor download status to corrupted', () => {
        expect(editorList.availableEditors[editor.version].isDownloadCorrupted).to.eql(true);
      });
    });

    describe('when editor last download status was in progress but the editor is located', () => {
      let editor;
      beforeEach(() => {
        sandbox.stub(downloadProgressManager, 'isEditorDownloadComplete').resolves(false);
        sandbox.stub(downloadProgressManager, 'setEditorDownloadProgressToComplete');
        editor = { version: 'foo', location: ['fake'], manual: true };
        installedEditorListMock.editors[editor.version] = editor;
        return editorList.init();
      });

      it('should not set editor download status to corrupted', () => {
        expect(editorList.availableEditors[editor.version].isDownloadCorrupted).to.be.false;
      });

      it('should set download as complete', () => {
        expect(downloadProgressManager.setEditorDownloadProgressToComplete).to.have.been.calledWith(editor.version);
      });
    });

    describe('when editor last download status was complete', () => {
      let editor;
      beforeEach(() => {
        sandbox.stub(downloadProgressManager, 'isEditorDownloadComplete').resolves(true);
        editor = { version: 'foo', location: ['fake'] };
        installedEditorListMock.editors[editor.version] = editor;
        return editorList.init();
      });

      it('should not set editor download status to corrupted', () => {
        expect(editorList.availableEditors[editor.version].isDownloadCorrupted).to.eql(false);
      });
    });

  });

  describe('latest', () => {
    describe('when editors are available', () => {
      it('should return the latest editor', () => {
        editorList.availableEditors = { '2017.1.0b5': {}, '2077.4.2f12': {}, '5.6.4p9': {} };
        expect(editorList.latest).to.equal(editorList.availableEditors['2077.4.2f12']);
      });
    });

    describe('when no editor is available', () => {
      it('should return null', () => {
        editorList.availableEditors = {};
        expect(editorList.latest).to.be.null;
      })
    });
  });

  describe('validateEditorsAreAvailable', () => {
    let editors,
      unavailableEditorLocations;

    beforeEach(() => {
      editors = [
        { version: 'foo1', location: ['bar1'] },
        { version: 'foo2', location: ['bar2'] },
        { version: 'foo3', location: ['bar3'] }
      ];
      editors.forEach((editor) => editorList.availableEditors[editor.version] = editor);

      unavailableEditorLocations = [];
      sandbox.stub(fs, 'pathExists')
        .callsFake((path) => Promise.resolve(!unavailableEditorLocations.includes(path)));
    });

    describe('when all editors are available', () => {
      let unavailableEditors;
      beforeEach(() => {
        return editorList.validateEditorsAreAvailable()
          .then((editors) => unavailableEditors = editors);
      });

      it('should return an empty array', () => {
        expect(unavailableEditors).to.eql([]);
      });
    });

    describe('when some editors are unavailable', () => {
      let unavailableEditors;
      beforeEach(() => {
        unavailableEditorLocations = [editors[0].location[0], editors[2].location[0]];
        return editorList.validateEditorsAreAvailable()
          .then((editors) => unavailableEditors = editors);
      });

      it('should return the unavailable editors', () => {
        expect(unavailableEditors).to.include(editors[0]);
        expect(unavailableEditors).to.include(editors[2]);
      });

      it('should not include valid editors', () => {
        expect(unavailableEditors).not.to.include(editors[1]);
      });
    });

    describe('when no editor is available', () => {
      let unavailableEditors;
      beforeEach(() => {
        unavailableEditorLocations = editors.map((editor) => editor.location[0]);
        return editorList.validateEditorsAreAvailable()
          .then((editors) => unavailableEditors = editors);
      });

      it('should return all editors', () => {
        expect(unavailableEditors).to.eql(editors);
      });
    });
  });

  describe('addLocatedEditor', () => {
    let editor;
    beforeEach(() => {
      editor = { version: 'foo' };
    });

    describe('when editor is already in list', () => {
      beforeEach(() => {
        editorList.availableEditors[editor.version] = editor;
      });

      it('should throw an error', () => {
        return expect(editorList.addLocatedEditor(editor)).to.be.rejected;
      });
    });

    describe('when editor is not already in list', () => {
      let result;
      beforeEach(() => {
        sandbox.stub(editorList, 'initEditorsBuildPlatform');
        return editorList.addLocatedEditor(editor).then((_result) => result = _result);
      });

      it('should add editor to the list', () => {
        expect(editorList.availableEditors[editor.version]).to.eql(editor);
      });

      it('should return the newly added editor', () => {
        expect(result).to.eql(editor);
      });
    });
  });

  describe('removeLocatedEditor', () => {
    let editor;
    beforeEach(() => {
      editor = { version: 'foo' };
      sandbox.stub(editorList, 'initEditorsBuildPlatform');

      return editorList.addLocatedEditor(editor)
        .then(() => expect(editorList.availableEditors[editor.version]).to.eql(editor))
        .then(() => editorList.removeLocatedEditor(editor));
    });

    it('should remove editor from list', () => {
      expect(editorList.availableEditors[editor.version]).not.to.be.defined;
    });
  });

  describe('addInstalledEditor', () => {
    let editor,
      result;
    beforeEach(() => {
      editor = { version: 'foo' };

      return editorList.addInstalledEditor(editor.version)
        .then((_result) => result = _result);
    });

    it('should add editor to list', () => {
      const addedEditor = editorList.availableEditors[editor.version];
      expect(addedEditor.version).to.equal(editor.version);
    });


    it('should return the newly added editor', () => {
      expect(result.version).to.equal(editor.version);
    });
  });

  describe('updateInstalledEditor', () => {
    let editor,
      addedEditorUpdate,
      result;
    beforeEach(() => {
      editor = { version: 'foo' };

      return editorList.addInstalledEditor(editor.version)
        .then((addedEditor) => {
          addedEditorUpdate = addedEditor.update;
          return editorList.updateInstalledEditor(editor.version)
        })
        .then((_result) => result = _result);
    });

    it('should keep editor to list', () => {
      const addedEditor = editorList.availableEditors[editor.version];
      expect(addedEditor.version).to.equal(editor.version);
    });

    it('should update editor', () => {
      expect(addedEditorUpdate).to.be.below(result.update);
    });

    it('should return the newly added editor', () => {
      expect(result.version).to.equal(editor.version);
    });
  });

  describe('removeCorruptEditor', () => {
    let editor;
    beforeEach(() => {
      editor = { version: 'foo', location: ['bar0'], folderPath: 'bar1' };
      sandbox.stub(fs, 'remove');

      return editorList.addInstalledEditor(editor.version)
        .then(() => expect(editorList.availableEditors[editor.version].version).to.eql(editor.version))
        .then(() => editorList.removeCorruptEditor(editor));
    });

    it('should remove editor from list', () => {
      expect(editorList.availableEditors[editor.version]).not.to.be.defined;
    });

    it('should call filesystem to remove editor folder', () => {
      expect(fs.remove).to.have.been.calledWith('bar1');
    });
  });

  describe('removeInstalledEditor', () => {
    let editor;
    beforeEach(() => {
      editor = { version: 'foo' };

      return editorList.addInstalledEditor(editor.version)
        .then(() => expect(editorList.availableEditors[editor.version].version).to.eql(editor.version))
        .then(() => editorList.removeInstalledEditor(editor));
    });

    it('should remove editor from list', () => {
      expect(editorList.availableEditors[editor.version]).not.to.be.defined;
    });
  });

  describe('setSecondaryInstallLocation', () => {
    let secondaryInstallPath;
    beforeEach(() => {
      secondaryInstallPath = 'my/secondary/install/location';
    });

    describe('when secondary install location is empty', () => {
      beforeEach(() => {
        return editorList.setSecondaryInstallLocation(secondaryInstallPath);
      });

      itShouldSetSecondaryInstallLocation();
    });

    describe('when secondary install location has editors', () => {
      let editors;
      beforeEach(() => {
        editors = {
          'foo': { version: 'foo' },
          'bar': { version: 'bar' }
        };
        sandbox.stub(editorList, 'initEditorsBuildPlatform');

        installedEditorListMock.setEditorsForPath(editors, secondaryInstallPath);

        return editorList.setSecondaryInstallLocation(secondaryInstallPath);
      });

      itShouldSetSecondaryInstallLocation();

      it('should update available editors', () => {
        Object.keys(editors).forEach((version) => {
          expect(editorList.availableEditors[version]).to.eql(editors[version]);
        });
      });
    });

    function itShouldSetSecondaryInstallLocation() {
      it('should set secondary install location', () => {
        expect(editorList.secondaryInstallPath).to.equal(secondaryInstallPath);
      });
    }
  });
});
