const InstalledEditorLocation = require('../lib/installedEditorLocation');

// Private Symbols
const updateEditorVersion = Symbol();
const removeEditor = Symbol();

class InstalledEditorLocationMock {
  constructor() {
    this.editors = {};
  }
  
  stubWithSandbox(sandbox) {
    sandbox.stub(InstalledEditorLocation.prototype, 'editors').get(() => this.editors);
    this.scanForInstallsAndGenerateEditors =
      sandbox.stub(InstalledEditorLocation.prototype, 'scanForInstallsAndGenerateEditors').resolves();
    
    sandbox.stub(InstalledEditorLocation.prototype, 'updateEditorVersion')
      .callsFake((version) => this[updateEditorVersion](version));
    
    sandbox.stub(InstalledEditorLocation.prototype, 'removeEditor')
      .callsFake((editor) => this[removeEditor](editor));
  }
  
  [updateEditorVersion](version) {
    let editor = this.editors[version];
  
    if (!editor) {
      editor = {
        version,
        location: ['foo'],
        update: 0
      };
    }
    
    editor.update++;
    
    this.editors[version] = editor;
    
    return Promise.resolve();
  }
  
  [removeEditor](editor) {
    delete this.editors[editor.version];
    return Promise.resolve();
  }
}

module.exports = InstalledEditorLocationMock;
