// internal
const InstalledEditorList = require('../lib/installedEditorList');

// Mocks
const InstalledEditorLocationMock = require('./installedEditorLocationMock');
const CustomInstallLocationMock = require('./customInstallLocationMock');

//tests
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
  .use(require('sinon-chai'))
  .use(require('chai-as-promised'));


describe('InstalledEditorList', () => {
  let sandbox,
    installedEditorLocationMock,
    customInstallLocationMock,
    defaultInstallPath;
  
  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    
    installedEditorLocationMock = new InstalledEditorLocationMock();
    installedEditorLocationMock.stubWithSandbox(sandbox);
    
    customInstallLocationMock = new CustomInstallLocationMock();
    customInstallLocationMock.stubWithSandbox(sandbox);
    
    defaultInstallPath = '/default/install/path';
  });
  
  afterEach(() => {
    sandbox.restore();
  });
  
  describe('new instance', () => {
    let installedEditorList;
    
    beforeEach(() => {
      installedEditorList = new InstalledEditorList(defaultInstallPath);
    });
    
    it('should initialize editors to empty object', () => {
      expect(installedEditorList.editors).to.eql({});
    });
    
    it('should initialize secondary install path to empty string', () => {
      expect(installedEditorList.secondaryInstallPath).to.eql('');
    });
  });
  
  describe('findInstalledEditors', () => {
    let installedEditorList;
    
    beforeEach(() => {
      installedEditorList = new InstalledEditorList(defaultInstallPath);
    });
    
    describe('when no editor is found', () => {
      beforeEach(() => {
        return installedEditorList.findInstalledEditors();
      });
      
      it('should set editor list to empty object', () => {
        expect(installedEditorList.editors).to.eql({});
      });
    });
    
    describe('when editors are present in base install location', () => {
      beforeEach(() => {
        populateUniqueBaseEditorList();
        return installedEditorList.findInstalledEditors();
      });
      
      itShouldAddBaseEditorsToEditorList();
      
      itShouldNotAddBaseEditorsByReferenceToEditorList();
    });
    
    describe('when editors are present in secondary install location', () => {
      beforeEach(() => {
        populateUniqueSecondaryEditorList();
        return installedEditorList.findInstalledEditors();
      });
      
      itShouldAddSecondaryEditorsToEditorList();
      
      itShouldNotAddSecondaryEditorsByReferenceToEditorList();
    });
    
    describe('when editors are present in both locations without conflicting versions', () => {
      beforeEach(() => {
        populateUniqueBaseEditorList();
        populateUniqueSecondaryEditorList();
        return installedEditorList.findInstalledEditors();
      });
  
      itShouldAddBaseEditorsToEditorList();
  
      itShouldNotAddBaseEditorsByReferenceToEditorList();
  
      itShouldAddSecondaryEditorsToEditorList();
  
      itShouldNotAddSecondaryEditorsByReferenceToEditorList();
    });
    
    describe('when editors are present in both locations with all conflicting versions', () => {
      beforeEach(() => {
        populateUniqueBaseEditorList();
        populateConflictingSecondaryEditorList();
        return installedEditorList.findInstalledEditors();
      });
      
      it('should add a merged list with both locations in location array of each conflicting editor', () => {
        Object.keys(installedEditorList.editors).forEach((version) => {
          const editor = installedEditorList.editors[version];
          expect(editor).to.be.defined;
          expect(editor.location).to.include(installedEditorLocationMock.editors[version].location[0]);
          expect(editor.location).to.include(customInstallLocationMock.editors[version].location[0]);
        });
      });
    });
    
    describe('when editors are present in both locations with some conflicting versions', () => {
      beforeEach(() => {
        populateUniqueBaseEditorList();
        populateUniqueSecondaryEditorList();
        addSingleConflictingEditorToSecondaryLocation();
        
        return installedEditorList.findInstalledEditors();
      });
      
      it('should add unconflicted editors from base location', () => {
        Object.keys(installedEditorLocationMock.editors).forEach((version) => {
          if (!customInstallLocationMock.editors[version]) {
            expect(installedEditorList.editors[version]).to.eql(installedEditorLocationMock.editors[version]);
          }
        });
      });
  
      it('should add unconflicted editors from secondary location', () => {
        Object.keys(customInstallLocationMock.editors).forEach((version) => {
          if (!installedEditorLocationMock.editors[version]) {
            expect(installedEditorList.editors[version]).to.eql(customInstallLocationMock.editors[version]);
          }
        });
      });
  
      it('should add conflicted editors with a merged location field', () => {
        Object.keys(customInstallLocationMock.editors).forEach((version) => {
          if (installedEditorLocationMock.editors[version]) {
            const baseEditor = installedEditorLocationMock.editors[version];
            const secondaryEditor = customInstallLocationMock.editors[version];
            
            expect(installedEditorList.editors[version].location).to.include(baseEditor.location[0]);
            expect(installedEditorList.editors[version].location).to.include(secondaryEditor.location[0]);
          }
        });
      });
    });
    
    function populateUniqueBaseEditorList() {
      installedEditorLocationMock.editors = {
        foo: {version: 'foo', location: ['my/path/to/foo/unity.app']},
        bar: {version: 'bar', location: ['my/path/to/bar/unity.app']}
      };
    }
    function populateUniqueSecondaryEditorList() {
      const editors = {
        secondaryFoo: {version: 'secondaryFoo', location: ['my/path/to/secondaryFoo/unity.app']},
        secondaryBar: {version: 'secondaryBar', location: ['my/path/to/secondaryBar/unity.app']}
      };
      const installPath = 'my/install/path';
      
      customInstallLocationMock.installPath = installPath;
      customInstallLocationMock.setEditorsForPath(editors, installPath);
    }
  
    function populateConflictingSecondaryEditorList() {
      const editors = {};
      Object.keys(installedEditorLocationMock.editors).forEach((version) => {
        editors[version] = installedEditorLocationMock.editors[version];
        editors[version].location = [`my/path/to/secondary/${version}/unity.app`];
      });
    
      const installPath = 'my/install/path';
    
      customInstallLocationMock.installPath = installPath;
      customInstallLocationMock.setEditorsForPath(editors, installPath);
    }
    
    function addSingleConflictingEditorToSecondaryLocation() {
      const editors = customInstallLocationMock.editors;
      const conflictingVersion = Object.keys(installedEditorLocationMock.editors)[0];
      
      editors[conflictingVersion] = installedEditorLocationMock.editors[conflictingVersion];
      editors[conflictingVersion].location = [`my/path/to/secondary/${conflictingVersion}/unity.app`];
  
      customInstallLocationMock.setEditorsForPath(editors, customInstallLocationMock.installPath);
    }
  
    function itShouldAddBaseEditorsToEditorList() {
      it('should set editor list to base editor list', () => {
        const baseEditorLocationList = installedEditorLocationMock.editors;
      
        Object.keys(baseEditorLocationList).forEach((version) => {
          expect(installedEditorList.editors[version]).to.eql(baseEditorLocationList[version]);
        });
      });
    }
  
    function itShouldNotAddBaseEditorsByReferenceToEditorList() {
      it('should not set editors in editor list to references of base editor list', () => {
        const baseEditorLocationList = installedEditorLocationMock.editors;
      
        Object.keys(baseEditorLocationList).forEach((version) => {
          expect(installedEditorList.editors[version]).not.to.equal(baseEditorLocationList[version]);
        });
      });
    }
  
    function itShouldAddSecondaryEditorsToEditorList() {
      it('should set editor list to secondary editor list', () => {
        const secondaryEditorLocationList = customInstallLocationMock.editors;
  
        Object.keys(secondaryEditorLocationList).forEach((version) => {
          expect(installedEditorList.editors[version]).to.eql(secondaryEditorLocationList[version]);
        });
      });
    }
  
    function itShouldNotAddSecondaryEditorsByReferenceToEditorList() {
      it('should not set editors in editor list to references of secondary editor list', () => {
        const secondaryEditorLocationList = customInstallLocationMock.editors;
  
        Object.keys(secondaryEditorLocationList).forEach((version) => {
          expect(installedEditorList.editors[version]).not.to.equal(secondaryEditorLocationList[version]);
        });
      });
    }
  });
  
  describe('addEditorVersion', () => {
    let installedEditorList, version;
  
    beforeEach(() => {
      installedEditorList = new InstalledEditorList(defaultInstallPath);
      version = '2017.2.1f3';
    });
    
    it('should update the editor list', () => {
      return installedEditorList.addEditorVersion(version)
        .then(() => {
          expect(installedEditorList.editors[version]).to.be.defined;
        });
    });
    
    describe('when secondary location is not set', () => {
      beforeEach(() => {
        return installedEditorList.addEditorVersion(version);
      });
      
      it('should set editor in base location', () => {
        expect(installedEditorLocationMock.editors[version]).to.be.defined;
      });
      
      it('should not set editor in secondary location', () => {
        expect(customInstallLocationMock.editors[version]).not.to.be.defined;
      });
    });
    
    describe('when secondary location is set', () => {
      beforeEach(() => {
        setSecondaryInstallPath();
        return installedEditorList.addEditorVersion(version);
      });
  
      it('should set editor in secondary location', () => {
        expect(customInstallLocationMock.editors[version]).to.be.defined;
      });
  
      it('should not set editor in base location', () => {
        expect(installedEditorLocationMock.editors[version]).not.to.be.defined;
      });
    });
  });
  
  describe('updateEditorVersion', () => {
    let installedEditorList,
      version;
  
    beforeEach(() => {
      installedEditorList = new InstalledEditorList(defaultInstallPath);
      version = '2017.2.1f3';
    });
    
    describe('when editor is in base location', () => {
      let addedEditorUpdate;
      
      beforeEach(() => {
        return installedEditorList.addEditorVersion(version)
          .then(() => addedEditorUpdate = installedEditorList.editors[version].update)
          .then(() => installedEditorList.updateEditorVersion(version));
      });
  
      it('should keep editor in list', () => {
        const addedEditor = installedEditorList.editors[version];
        expect(addedEditor.version).to.equal(version);
      });
      
      it('should not set editor in secondary location', () => {
        expect(customInstallLocationMock.editors[version]).not.to.be.defined;
      });
  
      it('should update editor', () => {
        expect(addedEditorUpdate).to.be.below(installedEditorList.editors[version].update);
      });
    });
    
    describe('when editor is in secondary location', () => {
      let addedEditorUpdate;
      
      beforeEach(() => {
        setSecondaryInstallPath();
        
        return installedEditorList.addEditorVersion(version)
          .then(() => addedEditorUpdate = installedEditorList.editors[version].update)
          .then(() => installedEditorList.updateEditorVersion(version));
      });
  
      it('should keep editor in list', () => {
        const addedEditor = installedEditorList.editors[version];
        expect(addedEditor.version).to.equal(version);
      });
  
      it('should not set editor in base location', () => {
        expect(installedEditorLocationMock.editors[version]).not.to.be.defined;
      });
  
      it('should update editor', () => {
        expect(addedEditorUpdate).to.be.below(installedEditorList.editors[version].update);
      });
    });
  
    describe('when editor is in both locations', () => {
      let addedEditorUpdate;
      beforeEach(() => {
        return installedEditorList.addEditorVersion(version)
          .then(() => {
            setSecondaryInstallPath();
            return installedEditorList.addEditorVersion(version);
          })
          .then(() => addedEditorUpdate = installedEditorList.editors[version].update)
          .then(() => installedEditorList.updateEditorVersion(version));
      });
  
      it('should keep editor in list', () => {
        const addedEditor = installedEditorList.editors[version];
        expect(addedEditor.version).to.equal(version);
      });
      
      it('should update base location editor only', () => {
        expect(addedEditorUpdate).to.be.below(installedEditorList.editors[version].update);
        expect(customInstallLocationMock.editors[version].update)
          .to.be.below(installedEditorLocationMock.editors[version].update);
      });
    });
  });
  
  describe('removeEditor', () => {
    let installedEditorList,
      version;
  
    beforeEach(() => {
      installedEditorList = new InstalledEditorList(defaultInstallPath);
      version = '2017.2.1f3';
    });
    
    describe('when editor is in base location', () => {
      beforeEach(() => {
        return installedEditorList.addEditorVersion(version)
          .then(() => installedEditorList.removeEditor(version));
      });
      
      itShouldRemoveEditorFromList();
      
      itShouldRemoveEditorFromBaseLocation();
    });
    
    describe('when editor is in secondary location', () => {
      beforeEach(() => {
        setSecondaryInstallPath();
        return installedEditorList.addEditorVersion(version)
          .then(() => installedEditorList.removeEditor(version));
      });
      
      itShouldRemoveEditorFromList();
      
      itShouldRemoveEditorFromSecondaryLocation();
    });
    
    describe('when editor is in both locations', () => {
      beforeEach(() => {
        return installedEditorList.addEditorVersion(version)
          .then(() => {
            setSecondaryInstallPath();
            return installedEditorList.addEditorVersion(version);
          })
          .then(() => installedEditorList.removeEditor(version));
      });
      
      itShouldRemoveEditorFromList();
      
      itShouldRemoveEditorFromBaseLocation();
      
      itShouldRemoveEditorFromSecondaryLocation();
    });
    
    function itShouldRemoveEditorFromList() {
      it('should remove editor from list', () => {
        expect(installedEditorList.editors[version]).not.to.be.defined;
      });
    }
    
    function itShouldRemoveEditorFromBaseLocation() {
      it('should remove editor from base location', () => {
        expect(installedEditorLocationMock.editors[version]).not.to.be.defined;
      });
    }
    
    function itShouldRemoveEditorFromSecondaryLocation() {
      it('should remove editor from secondary location', () => {
        expect(customInstallLocationMock.editors[version]).not.to.be.defined;
      });
    }
  });
  
  describe('setSecondaryInstallLocation', () => {
    let installedEditorList,
        installPath;
  
    beforeEach(() => {
      installedEditorList = new InstalledEditorList(defaultInstallPath);
      installPath = 'my/new/installPath';
    });
    
    describe('when base location is empty', () => {
      describe('and there is no current secondary path', () => {
        describe('and the new one matches the base install path', () => {
          beforeEach(() => {
            installPath = defaultInstallPath;
            
            return installedEditorList.setSecondaryInstallLocation(installPath)
          });
          
          it('should set secondary path to empty string', () => {
            expect(customInstallLocationMock.installPath).to.equal('');
          });
  
          it('should set secondary location editors to empty object', () => {
            expect(customInstallLocationMock.editors).to.eql({});
          });
          
          it('should not add editors to list', () => {
            expect(installedEditorList.editors).to.eql({});
          });
        });
        
        describe('and the new one is empty', () => {
          beforeEach(() => {
            return installedEditorList.setSecondaryInstallLocation(installPath);
          });
      
          it('should set secondary path for future use', () => {
            expect(installedEditorList.secondaryInstallPath).to.equal(installPath);
          });
        });
    
        describe('and the new one has editors', () => {
          let editors;
          beforeEach(() => {
            editors = {
              '5.6.1p4': { version: '5.6.1p4', location: ['my/path/somewhere'] },
              '2017.2.3b5': { version: '2017.2.3b5', location: ['my/path/somewhere/else'] }
            };
            customInstallLocationMock.setEditorsForPath(editors, installPath);
        
            return installedEditorList.setSecondaryInstallLocation(installPath);
          });
      
          it('should add editors to list', () => {
            Object.keys(editors).forEach((version) => {
              expect(installedEditorList.editors[version]).to.be.defined;
            });
          });
        });
      });
  
      describe('and there is a current secondary path', () => {
        let currentPath,
            editorsInCurrentPath;
        
        beforeEach(() => {
          currentPath = 'my/current/path';
          editorsInCurrentPath = {
            '3.5.1b6': { version: '3.5.1b6' },
            '4.6.1a3': { version: '4.6.1a3' }
          };
          
          customInstallLocationMock.setEditorsForPath(editorsInCurrentPath, currentPath);
        });
        
        describe('and the new one matches the base install path', () => {
          beforeEach(() => {
            installPath = defaultInstallPath;
      
            return installedEditorList.setSecondaryInstallLocation(installPath)
          });
    
          it('should set secondary path to empty string', () => {
            expect(customInstallLocationMock.installPath).to.equal('');
          });
    
          it('should set secondary location editors to empty object', () => {
            expect(customInstallLocationMock.editors).to.eql({});
          });
    
          itShouldRemoveEditorsFromCurrentLocation();
        });
        
        describe('and the new one is empty', () => {
          beforeEach(() => {
            return installedEditorList.setSecondaryInstallLocation(installPath);
          });
  
          it('should set secondary path for future use', () => {
            expect(installedEditorList.secondaryInstallPath).to.equal(installPath);
          });
          
          itShouldRemoveEditorsFromCurrentLocation();
        });
        
        describe('and new one has editors', () => {
          let editors;
          beforeEach(() => {
            editors = {
              '5.6.1p4': { version: '5.6.1p4', location: ['my/path/somewhere'] },
              '2017.2.3b5': { version: '2017.2.3b5', location: ['my/path/somewhere/else'] }
            };
            customInstallLocationMock.setEditorsForPath(editors, installPath);
    
            return installedEditorList.setSecondaryInstallLocation(installPath);
          });
          
          itShouldRemoveEditorsFromCurrentLocation();
  
          it('should add editors to list', () => {
            Object.keys(editors).forEach((version) => {
              expect(installedEditorList.editors[version]).to.be.defined;
            });
          });
        });
        
        function itShouldRemoveEditorsFromCurrentLocation() {
          it('should remove editors from current location', () => {
            Object.keys(editorsInCurrentPath).forEach((version) => {
              expect(installedEditorList.editors[version]).not.to.be.defined;
            });
          });
        }
      });
    });
  
    describe('when base location has editors', () => {
      beforeEach(() => {
        installedEditorLocationMock.editors = {
          '2017.1.2a4': { version: '2017.1.2a4', location: ['my/base/path'] },
          '2018.1.0b6': { version: '2018.1.0b6', location: ['my/base/path'] }
        };
      });
      
      describe('and there is no current secondary path', () => {
        describe('and the new one matches the base install path', () => {
          beforeEach(() => {
            installPath = defaultInstallPath;
      
            return installedEditorList.setSecondaryInstallLocation(installPath)
          });
    
          it('should set secondary path to empty string', () => {
            expect(customInstallLocationMock.installPath).to.equal('');
          });
    
          it('should set secondary location editors to empty object', () => {
            expect(customInstallLocationMock.editors).to.eql({});
          });
  
          itShouldKeepBaseLocationEditors();
        });
        
        describe('and the new one is empty', () => {
          beforeEach(() => {
            return installedEditorList.setSecondaryInstallLocation(installPath);
          });
        
          it('should set secondary path for future use', () => {
            expect(installedEditorList.secondaryInstallPath).to.equal(installPath);
          });
          
          itShouldKeepBaseLocationEditors();
        });
      
        describe('and the new one has unique editors', () => {
          let editors;
          beforeEach(() => {
            editors = {
              '5.6.1p4': { version: '5.6.1p4', location: ['my/path/somewhere'] },
              '2017.2.3b5': { version: '2017.2.3b5', location: ['my/path/somewhere/else'] }
            };
            customInstallLocationMock.setEditorsForPath(editors, installPath);
          
            return installedEditorList.setSecondaryInstallLocation(installPath);
          });
        
          it('should add editors to list', () => {
            Object.keys(editors).forEach((version) => {
              expect(installedEditorList.editors[version]).to.be.defined;
            });
          });
          
          itShouldKeepBaseLocationEditors();
        });
  
        describe('and the new one has conflicting editors', () => {
          let editors;
          beforeEach(() => {
            editors = {
              '5.6.1p4': { version: '5.6.1p4', location: ['my/path/somewhere'] },
              '2017.2.3b5': { version: '2017.2.3b5', location: ['my/path/somewhere/else'] }
            };
            
            const conflictingVersion = Object.keys(installedEditorLocationMock.editors)[0];
            editors[conflictingVersion] = { version: conflictingVersion, location: ['conflict/path'] };
            
            customInstallLocationMock.setEditorsForPath(editors, installPath);
      
            return installedEditorList.setSecondaryInstallLocation(installPath);
          });
    
          it('should add unconflicting editors from new location to list', () => {
            Object.keys(editors).forEach((version) => {
              if (!installedEditorLocationMock.editors[version]) {
                expect(installedEditorList.editors[version]).to.eql(editors[version]);
              }
            });
          });
  
          it('should keep unconflicting editors from base location to list', () => {
            Object.keys(installedEditorLocationMock.editors).forEach((version) => {
              if (!editors[version]) {
                expect(installedEditorList.editors[version]).to.eql(installedEditorLocationMock.editors[version]);
              }
            });
          });
          
          it('should set conflicting editors with a merged location array', () => {
            Object.keys(installedEditorList.editors).forEach((version) => {
              const baseLocationEditor = installedEditorLocationMock.editors[version];
              if (baseLocationEditor && editors[version]) {
                expect(installedEditorList.editors[version].location).to.include(baseLocationEditor.location[0]);
                expect(installedEditorList.editors[version].location).to.include(editors[version].location[0]);
              }
            });
          });
        });
      });
    
      describe('and there is a current secondary path', () => {
        let currentPath,
          editorsInCurrentPath;
      
        beforeEach(() => {
          currentPath = 'my/current/path';
          editorsInCurrentPath = {
            '3.5.1b6': { version: '3.5.1b6' },
            '4.6.1a3': { version: '4.6.1a3' }
          };
        
          customInstallLocationMock.setEditorsForPath(editorsInCurrentPath, currentPath);
        });
        
        describe('and the new one matches the base install path', () => {
          beforeEach(() => {
            installPath = defaultInstallPath;
      
            return installedEditorList.setSecondaryInstallLocation(installPath)
          });
    
          it('should set secondary path to empty string', () => {
            expect(customInstallLocationMock.installPath).to.equal('');
          });
    
          it('should set secondary location editors to empty object', () => {
            expect(customInstallLocationMock.editors).to.eql({});
          });
  
          itShouldRemoveEditorsFromCurrentLocation();
  
          itShouldKeepBaseLocationEditors();
        });
      
        describe('and the new one is empty', () => {
          beforeEach(() => {
            return installedEditorList.setSecondaryInstallLocation(installPath);
          });
        
          it('should set secondary path for future use', () => {
            expect(installedEditorList.secondaryInstallPath).to.equal(installPath);
          });
        
          itShouldRemoveEditorsFromCurrentLocation();
          
          itShouldKeepBaseLocationEditors();
        });
      
        describe('and new one has editors', () => {
          let editors;
          beforeEach(() => {
            editors = {
              '5.6.1p4': { version: '5.6.1p4', location: ['my/path/somewhere'] },
              '2017.2.3b5': { version: '2017.2.3b5', location: ['my/path/somewhere/else'] }
            };
            customInstallLocationMock.setEditorsForPath(editors, installPath);
          
            return installedEditorList.setSecondaryInstallLocation(installPath);
          });
        
          itShouldRemoveEditorsFromCurrentLocation();
        
          it('should add editors to list', () => {
            Object.keys(editors).forEach((version) => {
              expect(installedEditorList.editors[version]).to.be.defined;
            });
          });
  
          itShouldKeepBaseLocationEditors();
        });
      
        function itShouldRemoveEditorsFromCurrentLocation() {
          it('should remove editors from current location', () => {
            Object.keys(editorsInCurrentPath).forEach((version) => {
              expect(installedEditorList[version]).not.to.be.defined;
            });
          });
        }
      });
  
      function itShouldKeepBaseLocationEditors() {
        it('should keep base location editors', () => {
          Object.keys(installedEditorLocationMock.editors).forEach((version) => {
            expect(installedEditorList.editors[version]).to.eql(installedEditorLocationMock.editors[version]);
          });
        });
      }
    });
  });
  
  function setSecondaryInstallPath() {
    const installPath = 'my/secondary/path';
    customInstallLocationMock.installPath = installPath;
    customInstallLocationMock.setEditorsForPath({}, installPath);
  }
});
