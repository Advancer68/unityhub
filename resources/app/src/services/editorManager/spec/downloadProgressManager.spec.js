const localStorage = require('electron-json-storage');

const chai = require('chai');
const expect = chai.expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const proxyquire = require('proxyquire');

chai.use(sinonChai);
chai.use(chaiAsPromised);

describe('downloadProgressManager', () => {
  let sandbox;
  let downloadProgressManager;
  let localStorageGetValue;
  let localStorageSetValue;
  let localStorageRemoveValue;

  let localStorageSetStub;
  let localStorageGetStub;
  let localStorageRemoveStub;

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    localStorageGetStub = sandbox.stub(localStorage, 'get').callsFake(() => Promise.resolve(localStorageGetValue));
    localStorageSetStub = sandbox.stub(localStorage, 'set').resolves();
    sandbox.stub(localStorage, 'remove').resolves();

    // define downloadprogress manager after stubs above because localStorage is defined as a constant of the module
    downloadProgressManager = proxyquire('../lib/downloadProgressManager', {
      'localStorage': {
        get: localStorageGetStub,
        set: localStorageSetStub,
        remove: localStorageRemoveStub
      },
    });
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('isEditorDownloadComplete', () => {
    describe('editor download last status was in progress', () => {
      it('should return false', async () => {
        localStorageGetValue = 'IN_PROGRESS';
        const isDownloadComplete = await downloadProgressManager.isEditorDownloadComplete('2019.1.1');
        expect(isDownloadComplete).to.equal(false);
      });
    });

    describe('editor download progress is not found', () => {
      it('should return true', async () => {
        localStorageGetValue = undefined;
        const isDownloadComplete = await downloadProgressManager.isEditorDownloadComplete('2019.1.1');
        expect(isDownloadComplete).to.equal(true);
      });
    });

    describe('editor download progress is an empty object', () => {
      it('should return true', async () => {
        localStorageGetValue = {};
        const isDownloadComplete = await downloadProgressManager.isEditorDownloadComplete('2019.1.1');
        expect(isDownloadComplete).to.equal(true);
      });
    });
  });

  describe('startTrackingEditorDownloadProgress', () => {
    it('should set in progress value to local storage', async () => {
      localStorageSetValue = {};
      await downloadProgressManager.startTrackingEditorDownloadProgress('2019.1.1');
      expect(localStorage.set).to.have.been.called;
    });
  });

  describe('setEditorDownloadProgressToComplete', () => {
    it('should remove download progress from local storage', async () => {
      localStorageRemoveValue = {};
      await downloadProgressManager.setEditorDownloadProgressToComplete('2019.1.1');
      expect(localStorage.remove).to.have.been.calledWith('downloadProgress:2019.1.1');
    });
  });
});
