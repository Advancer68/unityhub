const CustomInstallLocation = require('../lib/customInstallLocation');

// Private Symbols
const editors = Symbol();
const updateEditorVersion = Symbol();
const removeEditor = Symbol();
const setInstallLocation = Symbol();

class CustomInstallLocationMock {
  
  get editors() {
    if (!this[editors][this.installPath]) {
      return {};
    }
    
    return this[editors][this.installPath];
  }
  
  constructor() {
    this.installPath = '';
    this[editors] = {};
  }
  
  stubWithSandbox(sandbox) {
    sandbox.stub(CustomInstallLocation.prototype, 'installPath').get(() => this.installPath);
    sandbox.stub(CustomInstallLocation.prototype, 'editors').get(() => this.editors);
    
    sandbox.stub(CustomInstallLocation.prototype, 'initializeCustomLocation').resolves();
    sandbox.stub(CustomInstallLocation.prototype, 'scanForInstallsAndGenerateEditors').resolves();
    
    sandbox.stub(CustomInstallLocation.prototype, 'updateEditorVersion')
      .callsFake((version) => this[updateEditorVersion](version));
    
    sandbox.stub(CustomInstallLocation.prototype, 'removeEditor')
      .callsFake((editor) => this[removeEditor](editor));
  
    sandbox.stub(CustomInstallLocation.prototype, 'setInstallLocation')
      .callsFake((installPath) => this[setInstallLocation](installPath));
  }
  
  [updateEditorVersion](version) {
    let editor = this[editors][this.installPath][version];
    
    if (!editor) {
      editor = {
        version,
        location: ['bar'],
        update: 0
      };
    }
    
    editor.update++;
    
    this[editors][this.installPath][version] = editor;
    
    return Promise.resolve();
  }
  
  [removeEditor](editor) {
    delete this.editors[editor.version];
    return Promise.resolve();
  }
  
  [setInstallLocation](installPath) {
    this.installPath = installPath;
    return Promise.resolve();
  }
  
  setEditorsForPath(_editors, installPath) {
    this[editors][installPath] = _editors;
  }
}

module.exports = CustomInstallLocationMock;
