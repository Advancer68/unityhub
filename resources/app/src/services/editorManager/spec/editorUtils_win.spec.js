// internal
const editorUtils = require('../editorUtils.js');
const { fs } = require('../../../fileSystem');
const path = require('path');

// third parties
const chai = require('chai');
const expect = require('chai').expect;
const chaiAsPromised = require('chai-as-promised');
const sinon = require('sinon');
const sinonChai = require('sinon-chai');
const os = require('os');

// TODO: These tests should be fixed with HUB-2456, they only run if you are developing with linux (or in the CI pipeline)
if(os.platform() === 'win32') {//test only on Windows

  chai.use(sinonChai);
  chai.use(chaiAsPromised);

  // These are more e2e tests and cannot be executed on the build machine due to dependencies
  describe('EditorUtils', () => {

    let sandbox;

    beforeEach(() => {
      sandbox = sinon.sandbox.create();
    });

    afterEach(() => {
      sandbox.restore();
    });

    describe.skip('findUnityVersion', () => {
      it('should return editor version', (done) => {
        editorUtils.findUnityVersion('C:\\Program Files\\Unity\\Editor\\Unity.exe')
          .then(version => {
            expect(version.version).to.equal('5.3.0.9440');
          }).then(done, done);
      });
    });

    describe('validateEditorFile', () => {
      it.skip('should check the file', (done) => {
        sandbox.stub(fs, 'existsSync').returns(true);
        return editorUtils.validateEditorFile('C:\\Program Files\\Unity\\Editor\\Unity.exe')
          .then(isValid => {
            expect(isValid).to.equal(true);
          }).then(done, done);
      });
    });

    describe('findBuildPlatforms', () => {
      it('should find the build platform buildTarget and name of a given editor when it has only the default platform', () => {
        sandbox.stub(fs, 'readdir').returns(Promise.resolve(['windowsstandalonesupport']));
        return editorUtils.findBuildPlatforms('C:\\Program Files\\Unity\\Editor')
          .then(buildPlatforms => {
            expect(buildPlatforms).to.eql([
              {
                "buildTarget": "StandaloneWindows64",
                "dirName": "windowsstandalonesupport",
                "name": "Windows 64-bit",
              }
            ]);
          });
      });
      it('should find the build platform buildTarget, buildTargetGroup and name of a given editor when it has the facebook platform', () => {
        sandbox.stub(fs, 'readdir')
          .onFirstCall().returns(Promise.resolve(['Facebook']))
          .onSecondCall().returns(Promise.resolve([]));
        return editorUtils.findBuildPlatforms('/Applications/Unity/Hub/Editor/2018.1.0b5')
          .then(buildPlatforms => {
            expect(buildPlatforms).to.eql([
              {
                "buildTarget": "Win",
                "buildTargetGroup": "facebook",
                "dirName": "Facebook",
                "name": "Facebook",
              }
            ]);
          });
      });
      it('should find the build platform buildTargets and names of a given editor when it has multiple platforms', () => {
        sandbox.stub(fs, 'readdir').returns(Promise.resolve(['AndroidPlayer', 'windowsstandalonesupport', 'PS4Player']));
        return editorUtils.findBuildPlatforms('C:\\Program Files\\somewhere\\2017.3.1f1\\Editor')
          .then(buildPlatforms => {
            expect(buildPlatforms).to.eql([
              {
                "buildTarget": "Android",
                "dirName": "AndroidPlayer",
                "name": "Android",
              },
              {
                "buildTarget": "StandaloneWindows64",
                "dirName": "windowsstandalonesupport",
                "name": "Windows 64-bit",
              },
              {
                "buildTarget": "PS4",
                "dirName": "PS4Player",
                "name": "PS4",
              }
            ]);
          });
      });
    });

    it('should reject file that does not exist', (done) => {
      sandbox.stub(fs, 'existsSync').returns(false);
      editorUtils.validateEditorFile('C:\\Unity.exe')
        .catch(error => {
          expect(error.errorCode).to.equal('ERROR.FILE_NOT_FOUND');
        }).then(done, done);
    });

    it('should reject file that is not an app', (done) => {
      sandbox.stub(fs, 'existsSync').returns(true);
      editorUtils.validateEditorFile('editorUtils_win.spec.js')
        .catch(error => {
          expect(error.errorCode).to.equal('ERROR.NOT_AN_APP');
        }).then(done, done);
    });

    it.skip('should reject file not signed by Unity', () => {
      sandbox.stub(fs, 'existsSync').returns(true);
      return editorUtils.validateEditorFile('C:\\Windows\\System32\\cmd.exe')
        .catch(error => {
          expect(error.errorCode).to.equal('ERROR.NOT_SIGNED');
        });
    });

    describe('getRegistryPackagesPath', () => {
      it('should get the correct path for the registry packages', () => {
        const editorPath = path.join('testing', 'path');

        const builtInPackagesPath = editorUtils.getRegistryPackagesPath(editorPath);

        expect(builtInPackagesPath).to.equal('testing\\Data\\Resources\\PackageManager\\Editor');
      });
    });

    describe('getBuiltInPackagesPath', () => {
      it('should get the correct path for the built in packages', () => {
        const editorPath = path.join('testing', 'path');

        const builtInPackagesPath = editorUtils.getBuiltInPackagesPath(editorPath);

        expect(builtInPackagesPath).to.equal('testing\\Data\\Resources\\PackageManager\\BuiltInPackages');
      });
    });
  });
}
