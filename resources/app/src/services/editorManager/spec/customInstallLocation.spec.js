// internal
const CustomInstallLocation = require('../lib/customInstallLocation');
const InstalledEditorLocation = require('../lib/installedEditorLocation');

// third parties
const localStorage = require('electron-json-storage');

// Mocks
const LocalStorageMock = require('./localStorageMock');

//tests
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
  .use(require('sinon-chai'))
  .use(require('chai-as-promised'));


describe('CustomInstallLocation', () => {
  let sandbox,
    localStorageMock,
    installPathKey;
  
  beforeEach(() => {
    sandbox = sinon.sandbox.create();
  
    localStorageMock = new LocalStorageMock(localStorage);
    localStorageMock.stubWithSandbox(sandbox);
    
    installPathKey = 'installPath';
  });
  
  afterEach(() => {
    sandbox.restore();
  });
  
  describe('new instance', () => {
    it('should initialize installPath to empty string', () => {
      const customInstallLocation = new CustomInstallLocation(installPathKey);
      expect(customInstallLocation.installPath).to.equal('');
    });
  });
  
  describe('initializeCustomLocation', () => {
    let customInstallLocation;
    
    beforeEach(() => {
      customInstallLocation = new CustomInstallLocation(installPathKey);
    });
    
    describe('when no installPath was stored', () => {
      beforeEach(() => {
        return customInstallLocation.initializeCustomLocation();
      });
      
      itShouldSetInstallPathToEmptyString();
      
      itShouldSetStoredPathToEmptyString();
    });
    
    describe('when installPath storage throws error', () => {
      beforeEach(() => {
        localStorageMock.setKeyThatThrowsError(installPathKey, new Error('blablabla'));
        return customInstallLocation.initializeCustomLocation();
      });
      
      itShouldSetInstallPathToEmptyString();
  
      itShouldSetStoredPathToEmptyString();
    });
    
    describe('when stored installPath is invalid', () => {
      beforeEach(() => {
        localStorageMock.data[installPathKey] = {};
        return customInstallLocation.initializeCustomLocation();
      });
      
      itShouldSetInstallPathToEmptyString();
  
      itShouldSetStoredPathToEmptyString();
    });
    
    describe('when stored installPath is valid', () => {
      let storedInstallPath;
      beforeEach(() => {
        storedInstallPath = 'my/custom/install/path';
        localStorageMock.data[installPathKey] = storedInstallPath;
        return customInstallLocation.initializeCustomLocation();
      });
      
      it('should set installPath to storedValue', () => {
        expect(customInstallLocation.installPath).to.equal(storedInstallPath);
      });
    });
    
    function itShouldSetInstallPathToEmptyString() {
      it('should set installPath to empty string', () => {
        expect(customInstallLocation.installPath).to.equal('');
      });
    }
    
    function itShouldSetStoredPathToEmptyString() {
      it('should set stored path to empty string', () => {
        expect(localStorageMock.data[installPathKey]).to.equal('');
      });
    }
  });
  
  describe('scanForInstallsAndGenerateEditors', () => {
    let customInstallLocation;
  
    beforeEach(() => {
      customInstallLocation = new CustomInstallLocation(installPathKey);
      sandbox.stub(InstalledEditorLocation.prototype, 'scanForInstallsAndGenerateEditors').resolves();
    });
    
    describe('when installPath is not set', () => {
      beforeEach(() => {
        return customInstallLocation.scanForInstallsAndGenerateEditors();
      });
      
      it('should not scan for installs and generate editors', () => {
        expect(InstalledEditorLocation.prototype.scanForInstallsAndGenerateEditors).not.to.have.been.called;
      });
    });
    
    describe('when installPath is set', () => {
      beforeEach(() => {
        localStorageMock.data[installPathKey] = 'my/path';
        
        return customInstallLocation.initializeCustomLocation()
          .then(() => customInstallLocation.scanForInstallsAndGenerateEditors());
      });
      
      it('should scan for installs and generate editors', () => {
        expect(InstalledEditorLocation.prototype.scanForInstallsAndGenerateEditors).to.have.been.called;
      });
    });
  });
  
  describe('updateEditorVersion', () => {
    let customInstallLocation, editorVersion;
  
    beforeEach(() => {
      editorVersion = '2017.2.1b2';
      customInstallLocation = new CustomInstallLocation(installPathKey);
      sandbox.stub(InstalledEditorLocation.prototype, 'updateEditorVersion').resolves();
    });
  
    describe('when installPath is not set', () => {
      beforeEach(() => {
        return customInstallLocation.updateEditorVersion(editorVersion);
      });
    
      it('should not update editor', () => {
        expect(InstalledEditorLocation.prototype.updateEditorVersion).not.to.have.been.called;
      });
    });
  
    describe('when installPath is set', () => {
      beforeEach(() => {
        localStorageMock.data[installPathKey] = 'my/path';
      
        return customInstallLocation.initializeCustomLocation()
          .then(() => customInstallLocation.updateEditorVersion(editorVersion));
      });
    
      it('should update editor', () => {
        expect(InstalledEditorLocation.prototype.updateEditorVersion).to.have.been.calledWith(editorVersion);
      });
    });
  });
  
  describe('removeEditor', () => {
    let customInstallLocation, editor;
  
    beforeEach(() => {
      editor = {foo: 'bar'};
      customInstallLocation = new CustomInstallLocation(installPathKey);
      sandbox.stub(InstalledEditorLocation.prototype, 'removeEditor').resolves();
    });
  
    describe('when installPath is not set', () => {
      beforeEach(() => {
        return customInstallLocation.removeEditor(editor);
      });
    
      it('should not remove editor', () => {
        expect(InstalledEditorLocation.prototype.removeEditor).not.to.have.been.called;
      });
    });
  
    describe('when installPath is set', () => {
      beforeEach(() => {
        localStorageMock.data[installPathKey] = 'my/path';
      
        return customInstallLocation.initializeCustomLocation()
          .then(() => customInstallLocation.removeEditor(editor));
      });
    
      it('should remove editor', () => {
        expect(InstalledEditorLocation.prototype.removeEditor).to.have.been.calledWith(editor);
      });
    });
  });
  
  describe('setInstallLocation', () => {
    let customInstallLocation, installPath;
  
    beforeEach(() => {
      installPath = 'my/custom/install/path';
      customInstallLocation = new CustomInstallLocation(installPathKey);
    });
    
    describe('in any case', () => {
      beforeEach(() => {
        return customInstallLocation.setInstallLocation(installPath);
      });
      
      itShouldSetInstallPath();
      
      itShouldSetStoredPath();
    });
    
    describe('when installPath is empty', () => {
      beforeEach(() => {
        installPath = '';
        sandbox.stub(InstalledEditorLocation.prototype, 'editors').get(() => ({foo: 'bar'}));
        return customInstallLocation.setInstallLocation('');
      });
      
      itShouldSetInstallPath();
      
      itShouldSetStoredPath();
      
      it('should reset editors to empty object', () => {
        expect(customInstallLocation.editors).to.eql({});
      });
    });
  
    function itShouldSetInstallPath() {
      it('should set install path', () => {
        expect(customInstallLocation.installPath).to.equal(installPath);
      });
    }
    
    function itShouldSetStoredPath() {
      it('should set stored path', () => {
        expect(localStorageMock.data[installPathKey]).to.equal(installPath);
      });
    }
  });
});
