// Internal
const fetchReleases = require('./../lib/fetchReleases');
const saihaiDataFormatter = require('./../lib/saihaiDataFormatter');
// Tests
const nock = require('nock');
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
  .use(require('sinon-chai'))
  .use(require('chai-as-promised'));

describe('FetchReleases', () => {

  let isOnline;
  let releasesCount = 0;
  let publicReleasesCount = 0;
  let sandbox; // = sinon.sandbox.create();

  // If Saihai is available
  describe('check if saihai api is live', () => {
    let apiNock;
    beforeEach(() => {
      apiNock = nock("http://saihai.hq.unity3d.com")
    });

    afterEach(() => {
      nock.cleanAll();
    });

    it('isSaihaiAvailable should return false', async () => {
      apiNock.get("/api/json/v2/?format=json")
        .replyWithError("error");
      let res = await fetchReleases.isSaihaiAvailable();
      expect(res).to.eql(false);
    });

    it('isSaihaiAvailable should return true', async () => {
      apiNock.get('/api/json/v2/?format=json')
        .reply(200);
      let res = await fetchReleases.isSaihaiAvailable();
      expect(res).to.eql(true);
    });
  });

  describe('get releases', () => {
    beforeEach(() => {

      let saihai = nock('http://saihai.hq.unity3d.com');
      // is Api Available
      saihai.get('/api/json/v2/?format=json').reply(200);
      // Get promoted build count

      saihai.get("/api/json/v2/builds/promoted/?format=json&limit=0&offset=0")
        .reply(200, {
          "count": 1,
        });
      // Get published build count
      saihai.get("/api/json/v2/builds/published/?format=json&limit=0&offset=0")
        .reply(200, {
          "count": 1,
        });
      // Get promoted build
      saihai.get("/api/json/v2/builds/promoted/?format=json&limit=1&offset=0")
        .reply(200, {
          "results": [
            {
              "promoted_on": "2018-05-18T08:56:20.202152Z",
              "published_on": null,
              "revision_short": "645c9050ba4d",
              "katana_url": "https://katana.bf.unity3d.com/projects/Unity/builders/proj0-zCompleteBuild/builds/4916?unity_branch=2017.4/release",
              "details_url": "http://saihai.hq.unity3d.com/build/734",
              "branch": "2017.4/release",
              "version": "2017.4.4f1",
              "installers": ["<truncated>"]
            }
          ]
        });
      // Get published build
      saihai.get("/api/json/v2/builds/published/?format=json&limit=1&offset=0")
        .reply(200, {
          "results": [
            {
              "release_type": "live",
              "promoted_on": "2018-05-17T09:50:23.367354Z",
              "published_on": "2018-05-18T13:26:34.763148Z",
              "revision_short": "372229934efd",
              "katana_url": "https://katana.bf.unity3d.com/projects/Unity/builders/proj0-zCompleteBuild/builds/4904?unity_branch=2017.2/release",
              "details_url": "http://saihai.hq.unity3d.com/build/733",
              "branch": "2017.2/release",
              "version": "2017.2.3f1",
            }
          ]
        });
      let beta = nock('http://beta.unity3d.com');
      
        beta.get("/download/645c9050ba4d/unity-2017.4.4f1-win.ini")
        .reply(200,
          `[Unity]
          title=Unity 2017.4.4f1
          description=Unity Editor including MonoDevelop for building your games
          url=Windows64EditorInstaller/UnitySetup64.exe
          install=true
          mandatory=false
          size=526918
          size_h=515M
          installedsize=1913925
          cmd="{FILENAME}" -UI=reduced /D={INSTDIR}
          md5=7e8dad5971da06805d2f0398bee19d74
          [MonoDevelop]
          title=MonoDevelop / Unity Debugger
          description=Unity Editor including MonoDevelop for building your games
          url=WindowsMonoDevelopInstaller/UnityMonoDevelopSetup.exe
          install=true
          mandatory=false
          size=42697
          size_h=42M
          installedsize=109529
          cmd="{FILENAME}" /S /D={INSTDIR}
          md5=a2cdd015bf1bf17bcdb572701796ac8f`);

      sandbox = sinon.sandbox.create();
      sandbox.stub(saihaiDataFormatter, 'format').callsFake((releases) => {
        return [{
          version_base: releases[0].version.split('.').slice(0, 2).join('.'),
          version: releases[0].version,
          iniUrl: "http://beta.unity3d.com/download/645c9050ba4d/unity-2017.4.4f1-win.ini",
          modules: {
            Editor: []
          }
        }];
      });
    });

    afterEach(() => {
      nock.cleanAll();
      sandbox.restore();
    });

    it('it should get releases', (done) => {
      fetchReleases.fetch().then((res) => {
        try {
          expect(res.length).to.eql(1);
          releasesCount = res.length;
          done();
        } catch (e) {
          done(e);
        }
      });
    });

    it('check if releases were cached', () => {
      expect(fetchReleases.get().length).to.eql(releasesCount);
    });

    it('extract only public releases', (done) => {

      let pr = fetchReleases.getPublicReleases();
      try {
        pr.map((ver) => {
          publicReleasesCount++;
          Object.keys(ver.modules).map((modName) => {
            if (ver.modules[modName].internal) {
              done("There was atleast one internal version in json");
            }
          });
        });
        done();
      } catch (e) {
        done(e);
      }
    });

    it('it should convert releases array to map by base version', () => {
      expect(fetchReleases.getReleasesMappedByBaseVersion(true).constructor).to.eql(Object);
    });

    it('it should have cached mapped version of releases', () => {
      expect(fetchReleases.get().constructor).to.eql(Object);
    });

    it('check if mapped release count is the same after mapping was done', () => {
      let count = 0;
      const releases = fetchReleases.get();
      Object.keys(releases).map((base) => {
        releases[base].map((ver) => {
          count++;
        })
      });

      expect(count).to.eql(releasesCount);
    });


    it('check if public releases mapped version doesn\'t have internal version/module', (done) => {
      const releases = fetchReleases.getPublicReleases();
      try {
        Object.keys(releases).map((base) => {
          releases[base].map((ver) => {
            Object.values(ver.modules).map((mod) => {
              if (mod.internal) {
                done("Mapped version has internal release when there shouldn't be one");
              }
            })
          });
        });
        done();
      } catch (e) {
        done(e);
      }

    });

    it('it should cache public releases (mapped version)', () => {
      fetchReleases.getPublicReleases(true);

      const releases = fetchReleases.get();
      let count = 0;

      Object.keys(releases).map((base) => {
        releases[base].map((ver) => {
          count++;
        });
      });
      expect(count).to.eql(publicReleasesCount);

    });
  });
});
