const expect = require('chai').expect;
const sinon = require('sinon');
const nock = require('nock');
const proxyquire = require('proxyquire');


require('chai').use(require('sinon-chai'));


const EulaFetcher = proxyquire('../eulaFetcher', {
  '../../logger': {
    warn(){},
    debug(){}
  }
});

describe('EulaFetcher', () => {
  let sandbox;

  beforeEach(() => {
    sandbox = sinon.sandbox.create();
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('fetch', () => {
    describe('when module argument is undefined', () => {
      it('should reject', async () => {
        try {
          await EulaFetcher.fetch();
        } catch (e){
          expect(e).to.be.an('error');
        }
      });
    });

    describe('when module argument has undefined id property', () => {
      it('should return an empty string', async () => {
        expect(await EulaFetcher.fetch({})).to.be.empty;
      });
    });

    describe('when module argument id has no matching method in the class', () => {
      it('should return an empty string', async () => {
        expect(await EulaFetcher.fetch({id: 'an id'})).to.be.empty;
      });
    });

    describe('when module argument id has a matching method in the class', () => {
      it('should return the value of that methods calls', async () => {
        EulaFetcher._fakeMethod = sandbox.stub().returns('donuts');
        expect(await EulaFetcher.fetch({id: 'fakeMethod'})).to.equal('donuts');
        delete EulaFetcher._fakeMethod;
      });
    });
  });

  describe('eula fetching methods', () => {

    describe('android-sdk-ndk-tools', () => {
      const eulaUrl = 'http://www.eula.com';
      let httpStub;

      beforeEach(() => {
        httpStub = nock(eulaUrl);
      });

      describe('when given url is not valid or reachable', () => {
        it('should resolve with an empty string', async () => {
          httpStub.get('*').reply(404);
          const result = await EulaFetcher['_android-sdk-ndk-tools'](eulaUrl);
          expect(result).to.be.empty;
        });
      });

      describe('when remote content is not valid xml', () => {
        it('should resolve with an empty string', async () => {
          httpStub.get('*').reply(200, 'not valid xml');
          const result = await EulaFetcher['_android-sdk-ndk-tools'](eulaUrl);
          expect(result).to.be.empty;
        });
      });

      describe('when remote content is valid xml', () => {
        describe('and it does not contain a "license" node', () => {
          it('should resolve with an empty string', async () => {
            httpStub.get('*').reply(200, '<root></root>');
            const result = await EulaFetcher['_android-sdk-ndk-tools'](eulaUrl);
            expect(result).to.be.empty;
          });
        });

        describe('and it contains a "license" node', () => {
          describe('and no children has an id equal to "android-sdk-license"', () => {
            it('should resolve with an empty string', async () => {
              httpStub.get('*').reply(200, '<root><license>some content</license></root>');
              const result = await EulaFetcher['_android-sdk-ndk-tools'](eulaUrl);
              expect(result).to.be.empty;
            });
          });

          describe.skip('and a child has an id equal to "android-sdk-license"', () => {
            it('should resolves with this child data content', async () => {
              const expected = "eula content";
              const xmlResponse = `<root><license>some content</license><license id="android-sdk-license">${expected}</license></root>`;
              httpStub.get('*').reply(200, xmlResponse);
              const result = await EulaFetcher['_android-sdk-ndk-tools'](eulaUrl);
              expect(result).to.equal(expected);
            });
          });
        });
      });
    });
  })
});
