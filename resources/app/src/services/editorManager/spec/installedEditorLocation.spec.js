// internal
const InstalledEditorLocation = require('../lib/installedEditorLocation');
const releaseFileIO = require('../../localInstaller/lib/releaseFileIO');
const editorUtils = require('../editorUtils');
const EditorFolderScanner = require('../lib/editorFolderScanner');

// third parties
const path = require('path');

//tests
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
  .use(require('sinon-chai'))
  .use(require('chai-as-promised'));


describe('InstalledEditorLocation', () => {
  let sandbox,
    installPath;
  
  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    
    installPath = 'my/install/path';
  });
  
  afterEach(() => {
    sandbox.restore();
  });
  
  describe('new instance', () => {
    it('should initialize editorFolder to empty object', () => {
      const installedEditorLocation = new InstalledEditorLocation(installPath);
      expect(installedEditorLocation.editors).to.eql({});
    });
  });
  
  describe('scanForInstallsAndGenerateEditors', () => {
    let installedEditorLocation,
        editorNames,
        modules,
        editorLocation;
    
    beforeEach(() => {
      installedEditorLocation = new InstalledEditorLocation(installPath);
      
      editorNames = [];
      modules = [];
      editorLocation = 'my/editor/location';
      
      sandbox.stub(EditorFolderScanner.prototype, 'fetchEditorFolders').resolves();
      sandbox.stub(EditorFolderScanner.prototype, 'editorFolders').get(() => editorNames);
      sandbox.stub(editorUtils, 'getEditorPathFromFolder').callsFake(() => editorLocation);
      sandbox.stub(releaseFileIO, 'read').callsFake(() => Promise.resolve(modules));
    });
    
    describe('when install location is empty', () => {
      beforeEach(() => {
        return installedEditorLocation.scanForInstallsAndGenerateEditors();
      });
      
      it('editor list should remain empty', () => {
        expect(installedEditorLocation.editors).to.be.empty;
      });
    });
    
    describe('when install location contains one folder', () => {
      let expectedEditor;
      beforeEach(() => {
        expectedEditor = {
          version: '2017.2.1b3',
          shortVersion: '2017.2.1',
          location: [editorLocation]
        };
        editorNames = [expectedEditor.version];
        return installedEditorLocation.scanForInstallsAndGenerateEditors();
      });
      
      it('should set correct editor in editor list', () => {
        const installedEditor = installedEditorLocation.editors[expectedEditor.version];
        expect(installedEditor.version).to.eql(expectedEditor.version);
        expect(installedEditor.location).to.eql(expectedEditor.location);
      });
      
    });
    
    describe('when fetching modules for an editor fails', () => {
      beforeEach(() => {
        editorNames = ['2017.1.1b1'];
        releaseFileIO.read.rejects();
        return installedEditorLocation.scanForInstallsAndGenerateEditors();
      });
      
      it('should set editor manual flag to true instead', () => {
        const installedEditor = installedEditorLocation.editors[editorNames[0]];
        expect(installedEditor.modules).not.to.be.defined;
        expect(installedEditor.manual).to.be.true;
      });
    });
    
    describe('when modules for an editor are empty', () => {
      beforeEach(() => {
        editorNames = ['2017.1.1b1'];
        modules = [];
        return installedEditorLocation.scanForInstallsAndGenerateEditors();
      });
      
      it('should set the manual flag to true', () => {
        const installedEditor = installedEditorLocation.editors[editorNames[0]];
        expect(installedEditor.manual).to.be.true;
      });
      
      it('should not set modules property for editor', () => {
        const installedEditor = installedEditorLocation.editors[editorNames[0]];
        expect(installedEditor.manual).to.be.true;
      });
    });
  
    describe('when modules for an editor are not empty', () => {
      beforeEach(() => {
        editorNames = ['2017.1.1b1'];
        modules = [{foo: 'bar'}];
        return installedEditorLocation.scanForInstallsAndGenerateEditors();
      });
    
      it('should not set manual flag', () => {
        const installedEditor = installedEditorLocation.editors[editorNames[0]];
        expect(installedEditor.manual).not.to.be.defined;
      });
      
      it('should set modules for editor', () => {
        const installedEditor = installedEditorLocation.editors[editorNames[0]];
        expect(installedEditor.modules).to.eql(modules);
      });
    });
    
  });
  
  describe('updateEditorVersion', () => {
    let installedEditorLocation,
      version,
      editor,
      editorLocation;
  
    beforeEach(() => {
      installedEditorLocation = new InstalledEditorLocation(installPath);
      version = '2017.1.1b1';
      editor = {
        version,
        modules: [{foo: 'bar'}]
      };
      
      editorLocation = 'my/editor/location';
    
      sandbox.stub(editorUtils, 'getEditorPathFromFolder').callsFake(() => editorLocation);
      sandbox.stub(releaseFileIO, 'read').callsFake(() => Promise.resolve(editor.modules));
    });
  
    describe('when editor was not previously in list', () => {
      beforeEach(() => {
        return installedEditorLocation.updateEditorVersion(version);
      });
      
      it('should set a new editor in the list', () => {
        const newEditor = installedEditorLocation.editors[version];
        expect(newEditor.version).to.eql(version);
        expect(newEditor.modules).to.eql(editor.modules);
      });
    });
  
    describe('when editor was previously in list', () => {
      let newModules;
      beforeEach(() => {
        newModules = [{foo: 'new'}, {foo: 'modules'}];
       
        sandbox.stub(EditorFolderScanner.prototype, 'fetchEditorFolders').resolves([editor.version]);
        sandbox.stub(EditorFolderScanner.prototype, 'editorFolders').get(() => [editor.version]);
        
        return installedEditorLocation.scanForInstallsAndGenerateEditors()
          .then(() => { editor.modules = newModules; })
          .then(() => installedEditorLocation.updateEditorVersion(version));
      });
    
      it('should update editor in the list', () => {
        const newEditor = installedEditorLocation.editors[version];
        expect(newEditor.version).to.eql(version);
        expect(newEditor.modules).to.eql(newModules);
      });
    });
  });
  
  describe('removeEditor', () => {
    it('should remove editor from list', () => {
      const installedEditorLocation = new InstalledEditorLocation(installPath);
      const version = '2018.1.1b1';
      
      sandbox.stub(editorUtils, 'getEditorPathFromFolder').callsFake(() => '');
      sandbox.stub(releaseFileIO, 'read').callsFake(() => Promise.resolve([]));
      
      return installedEditorLocation.updateEditorVersion(version)
        .then(() => {
          expect(installedEditorLocation.editors[version]).to.be.defined;
          installedEditorLocation.removeEditor(version);
          expect(installedEditorLocation.editors[version]).not.to.be.defined;
        });
    });
  });
});
