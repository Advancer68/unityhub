// Native
const platform = require('os').platform();
// Internal
const saihaiDataFormatter = require('./../lib/saihaiDataFormatter');
const editorUtils = require('./../editorUtils');
// Third libraries
const moment = require('moment');

const saihaiModules = editorUtils.getSaihaiModuleNames();
const saihaiSettings = editorUtils.getSaihaiSettings();

// Tests
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
  .use(require('sinon-chai'))
  .use(require('chai-as-promised'));

describe("saihaiDataFormatter tests", () => {
  // Saihai settings constants
  let sandbox;
  let saihaiResult = [
    {
      "promoted_on": "2018-05-18T08:56:20.202152Z",
      "published_on": "2018-05-23T15:20:18.873125Z",
      "revision_short": "645c9050ba4d",
      "katana_url": "https://katana.bf.unity3d.com/projects/Unity/builders/proj0-zCompleteBuild/builds/4916?unity_branch=2017.4/release",
      "details_url": "http://saihai.hq.unity3d.com/build/734",
      "branch": "2017.4/release",
      "version": "2017.4.4f1"
    }
  ];
  let expectedProperties = [
    'iniUrl', 'installed', 'modules', 'promoted_on', 'published', 'published_on', 'revision_medium',
    'revision_short', 'timestamp', 'timestamp_sort', 'version', 'version_base'
  ];
  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    sandbox.stub(editorUtils, 'getSaihaiModuleNames').callsFake(() => {
      if (platform === 'win32') {
        return [
          {
            name: 'Editor',
            nameFull: 'Unity Editor'
          },
          {
            name: 'Linux',
            nameFull: 'Linux Support'
          },
          {
            name: 'WebGL',
            nameFull: 'WebGL Support'
          },
          {
            name: 'Android',
            nameFull: 'Android Support'
          },
          {
            name: 'AppleTV',
            nameFull: 'AppleTV Support'
          },
          {
            name: 'iOS',
            nameFull: 'iOS Support'
          },
          {
            name: 'Mac',
            nameFull: 'Mac Support'
          },
          {
            name: 'Mac-Mono',
            nameFull: 'Mac-Mono Support'
          },
          {
            name: 'Metro',
            nameFull: 'Metro Support'
          },
          {
            name: 'Facebook-Games',
            nameFull: 'Facebook-Games Support'
          },
          {
            name: 'UWP-IL2CPP',
            nameFull: 'UWP-IL2CPP Support'
          },
          {
            name: 'Windows-IL2CPP',
            nameFull: 'Windows-IL2CPP Support'
          },
          {
            name: 'Samsung-TV',
            nameFull: 'Samsung-TV Support'
          },
          {
            name: 'Tizen',
            nameFull: 'Tizen Support'
          },
          {
            name: 'Vuforia-AR',
            nameFull: 'Vuforia-AR Support'
          },
          {
            name: 'Wii-U',
            nameFull: 'Wii-U Support'
          },
          {
            name: 'Playstation-3',
            nameFull: 'Playstation-3 Support'
          },
          {
            name: 'Playstation-4',
            nameFull: 'Playstation-4 Support'
          },
          {
            name: 'Playstation-Vita',
            nameFull: 'Playstation-Vita Support'
          },
          {
            name: 'Xbox-One',
            nameFull: 'Xbox-One Support'
          },
          {
            name: 'StandardAssets',
            nameFull: 'Standard Assets'
          },
          {
            name: 'Documentation',
            nameFull: 'Documentation'
          }
        ];
      } else {
        return [
          {
            name: 'Editor',
            nameFull: 'Unity Editor'
          },
          {
            name: 'Linux',
            nameFull: 'Linux Support'
          },
          {
            name: 'WebGL',
            nameFull: 'WebGL Support'
          },
          {
            name: 'Android',
            nameFull: 'Android Support'
          },
          {
            name: 'AppleTV',
            nameFull: 'AppleTV Support'
          },
          {
            name: 'iOS',
            nameFull: 'iOS Support'
          },
          {
            name: 'Windows',
            nameFull: 'Windows Support'
          },
          {
            name: 'Mac-IL2CPP',
            nameFull: 'Mac-IL2CPP Support'
          },
          {
            name: 'Facebook-Games',
            nameFull: 'Facebook-Games Support'
          },
          {
            name: 'Samsung-TV',
            nameFull: 'Samsung-TV Support'
          },
          {
            name: 'Tizen',
            nameFull: 'Tizen Support'
          },
          {
            name: 'Vuforia-AR',
            nameFull: 'Vuforia-AR Support'
          },
          {
            name: 'StandardAssets',
            nameFull: 'Standard Assets'
          },
          {
            name: 'Documentation',
            nameFull: 'Documentation'
          }
        ];
      }
    });

    sandbox.stub(editorUtils, 'getSaihaiSettings').callsFake(() => {
      if (platform === 'win32') {
        return {
          unityPrefix: 'Unity',
          setupPostfix: 'Setup',
          platformName: 'Windows',
          platformNamePostfix: '64',
          installerFormat: 'exe',
          targetSupportString: 'TargetSupportInstaller',
          configurationPlatformName: 'win'
        };
      } else {
        return {
          unityPrefix: '',
          setupPostfix: '',
          platformName: 'Mac',
          platformNamePostfix: '',
          installerFormat: 'pkg',
          targetSupportString: 'MacEditorTargetInstaller',
          configurationPlatformName: 'osx'
        };
      }
    });

  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('Try giving incorrect input', () => {

    it('should fail when nothing is passed', () => {
      expect(() => saihaiDataFormatter.format()).to.throw(Error);
    });

    it('should fail when input doesn\'t have needed properties', () => {
      expect(() => saihaiDataFormatter.format([{badProperty:'any'}])).to.throw(Error);
    });
  });

  describe('Try giving correct input', () => {

    it('should have all expected properties', () => {
      let res = saihaiDataFormatter.format(saihaiResult)[0];
      for(const prop of expectedProperties) {
        expect(res).to.have.property(prop);
      }
    });

    it('should not fail when input is correct but has redundant properties', () => {
      let data = saihaiResult;
      data.randomProperty = 'ignore me';
      expect(() => saihaiDataFormatter.format(data)).to.not.throw(Error);
    });

  });
});


