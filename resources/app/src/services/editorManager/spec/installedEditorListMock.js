const InstalledEditorList = require('../lib/installedEditorList');

// Private Symbols
const editorsInInstallPaths = Symbol();
const updateEditorVersion = Symbol();
const removeEditor = Symbol();
const setSecondaryInstallLocation = Symbol();

class InstalledEditorListMock {
  constructor() {
    this.editors = {};
    this.secondaryInstallPath = '';
    this[editorsInInstallPaths] = {};
  }
  
  stubWithSandbox(sandbox) {
    sandbox.stub(InstalledEditorList.prototype, 'editors').get(() => this.editors);
    sandbox.stub(InstalledEditorList.prototype, 'secondaryInstallPath').get(() => this.secondaryInstallPath);
    
    sandbox.stub(InstalledEditorList.prototype, 'findInstalledEditors').resolves();
    
    sandbox.stub(InstalledEditorList.prototype, 'addEditorVersion')
      .callsFake((version) => this[updateEditorVersion](version));
    
    sandbox.stub(InstalledEditorList.prototype, 'updateEditorVersion')
      .callsFake((version) => this[updateEditorVersion](version));
    
    sandbox.stub(InstalledEditorList.prototype, 'removeEditor')
      .callsFake((editor) => this[removeEditor](editor));
  
    sandbox.stub(InstalledEditorList.prototype, 'setSecondaryInstallLocation')
      .callsFake((installPath) => this[setSecondaryInstallLocation](installPath));
  }
  
  [updateEditorVersion](version) {
    let editor = this.editors[version];
    
    if (!editor) {
      editor = {
        version,
        location: ['foo'],
        update: 0
      };
    }
    
    editor.update++;
    
    this.editors[version] = editor;
    
    return Promise.resolve();
  }
  
  [removeEditor](editor) {
    delete this.editors[editor.version];
    return Promise.resolve();
  }
  
  [setSecondaryInstallLocation](installPath) {
    const editors = this[editorsInInstallPaths][installPath];
    
    if (editors) {
      Object.assign(this.editors, editors);
    }
    
    this.secondaryInstallPath = installPath;
    
    return Promise.resolve();
  }
  
  setEditorsForPath(editors, installPath) {
    this[editorsInInstallPaths][installPath] = editors;
  }
}

module.exports = InstalledEditorListMock;
