'use strict';
const tools = require('../editorApp/platformtools');
const _ = require('lodash');
const path = require('path');
const { fs } = require('../../fileSystem');
const logger = require('../../logger')('EditorManager::Utils');
const buildTargetData = require('./buildTarget.json');

const editorUtilsPlatforms = tools.require(`${__dirname}/editorUtils`);

/**
 * Reading from the ivy file like the editor does in BuildPlatform.cs
 * On mac, playback engines are in 2 places:
 * 2017.1.0f1/PlaybackEngines or
 * 2017.1.0f1/Unity.app/Contents/PlaybackEngines (contains the default mac standalone support platform)
 *
 * On windows, playback engines are only in 1 place:
 * 2017.1.0f1/Editor/Data/PlaybackEngines
 *
 * The editor finds the list of platforms by finding all the folders under Data/PlaybackEngines
 * and ready an ivy file under each folder.
 * The ivy file is available since Unity 4.5.0
 */
function findBuildPlatformInfo(editorPath) {
  const buildPlatforms = [];
  const platformsPath = editorUtilsPlatforms.getPlaybackEnginesPathsFromFolder(editorPath);

  const promiseList = [];
  for (let i = 0; i < platformsPath.length; i++) {
    promiseList.push(findBuildPlatformsPerPath(platformsPath[i], buildPlatforms));
  }

  return Promise.all(promiseList)
    .then(() => buildPlatforms)
    .catch((e) => {
      logger.warn(`An error occurred while trying to find the platforms of ${editorPath}: ${e}`);
      return buildPlatforms;
    });
}

function findBuildPlatformsPerPath(platformsPath, buildPlatforms) {
  return getBuildPlatformFromFolder(platformsPath)
    .then((buildPlatformList) => {
      if (buildPlatformList) {
        buildPlatformList.forEach((buildPlatform) => {
          if (typeof buildPlatform === 'object') {
            buildPlatforms.push(buildPlatform);
          }
        });
      }
    });
}

/**
 * @param platformsPath
 */
function getBuildPlatformFromFolder(platformsPath) {
  return fs.readdir(platformsPath)
    .then((folders) => {
      if (folders.length > 0) {
        const promiseList = folders.map(folder => {
          const buildPlatform = getBuildPlatform(folder);
          if (buildPlatform && typeof buildPlatform === 'object') {
            return buildPlatform;
          }
          return Promise.resolve();
        });

        return Promise.all(promiseList);
      }

      return Promise.resolve([]);
    })
    .catch((e) => {
      if (e.code === 'ENOENT') {
        logger.debug(`No platform found in ${platformsPath}`);
      } else {
        throw e;
      }
    });
}

/**
 * Platform avialable
 * @param platformsPath
 * @returns {*}
 */
function getBuildPlatform(platformsPath) {
  const platformFolder = path.basename(platformsPath);
  const result = _.find(buildTargetData, { dirName: platformFolder });
  if (!result) {
    return null; 
  }
  if (result.buildTargetGroup) {
    result.buildTarget = editorUtilsPlatforms.getOSBuildTarget();
  }
  return result;
}

class EditorUtils {
  constructor() {
    const editorUtils = tools.require(`${__dirname}/editorUtils`);
    _.merge(this, editorUtils);
  }

  /**
   *
   * @param editorPath path to an editor executable
   */
  findBuildPlatforms(editorPath) {
    return findBuildPlatformInfo(path.dirname(editorPath));
  }
}

module.exports = new EditorUtils();
