const DOMParser = require('xmldom').DOMParser;
const axios = require('axios');
const logger = require('../../logger');

/**
 * EulaFetcher is a utility class made of static function to fetch online hosted EULA (End User License Agreement) content
 * for Unity Editor modules requiring user consent for these agreements.
 *
 * The only public API of this class is the fetch method expecting an editor's module object as parameter.
 * The proper fetching method will be called, based on the module id, if available.
 *
 */
class EulaFetcher {

  static async fetch(module) {
    const methodName = `_${module.id}`;
    if (typeof EulaFetcher[methodName] === 'function') {
      return EulaFetcher[methodName](module.eulaUrl1);
    }
    return '';
  }

  static async ['_android-sdk-ndk-tools'](url) {
    return axios(url)
      .then((response) => {
        const doc = new DOMParser().parseFromString(response.data, 'text/xml');
        let text = '';
        const elements = doc.documentElement.getElementsByTagName('license');
        for (let i = 0; i < elements.length; i++) {
          if (elements[i].getAttribute('id') === 'android-sdk-license') {
            text = elements[i].firstChild.data;
            break;
          }
        }
        return text;
      })
      .catch((e) => {
        // todo: we should revise the expected design here and figure out what todo when remote eula is unreachable
        logger.warn(`Failed to fetch eula for android-sdk-ndk-tools, at ${url}`);
        logger.debug(e);
        return '';
      });
  }
}

module.exports = EulaFetcher;
