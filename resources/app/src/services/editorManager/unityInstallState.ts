import UnityInstallInfo from './UnityInstallInfo';

class UnityInstallState {
  private installs: UnityInstallInfo[] = [];

  addInstall(id: string): void {
    this.installs.push(new UnityInstallInfo(id));
  }

  removeInstall(id: string): void {
    const index = this.getInstallIndex(id);
    if (index === -1) { return; }
    this.installs.splice(index, 1);
  }

  installError(id: string): void {
    const index = this.getInstallIndex(id);
    if (index === -1) { return; }
    this.installs[index].error = true;
  }

  installModulesError(id: string): void {
    const index = this.getInstallIndex(id);
    if (index === -1) { return; }
    this.installs[index].error = true;
    this.installs[index].errorIsForModules = true;
  }

  getFailedInstalls(): UnityInstallInfo[] {
    return this.installs.filter((installInfo) => installInfo.error);
  }

  private getInstallIndex(id: string): number {
    return this.installs.findIndex((installInfo) => installInfo.version === id);
  }
}

export = new UnityInstallState();
