const promisify = require('es6-promisify');
const localStorage = require('electron-json-storage');
const logger = require('../../../logger')('ListStorage');

// Private symbols
const storageKey = Symbol();
const items = Symbol();
const updateItemStorage = Symbol();
const isLocationAnArray = Symbol();
const primaryKey = Symbol();

class ListStorage {

  get items() { return this[items]; }

  constructor(_storageKey, _primaryKey = 'id') {
    this[storageKey] = _storageKey;
    this[primaryKey] = _primaryKey;
    this[items] = {};
  }

  async init() {
    await this.fetchItems();
  }

  fetchItems() {
    return promisify(localStorage.get)(this[storageKey])
      .then((_items) => {
        logger.info(`${Object.keys(_items).length} located items found in storage`);
        this[items] = _items;
      })
      .catch((err) => {
        logger.debug(err);
        logger.warn('The list of manually located items is corrupted, flushing list...');
        this[items] = {};
        return this[updateItemStorage]();
      });
  }

  [updateItemStorage]() {
    return promisify(localStorage.set)(this[storageKey], this[items]);
  }

  [isLocationAnArray](location) {
    return location instanceof Array;
  }

  addItem(item) {
    this[items][item[this[primaryKey]]] = item;
    return this[updateItemStorage]();
  }

  removeItem(item) {
    delete this[items][item[this[primaryKey]]];
    return this[updateItemStorage]();
  }

  clear() {
    this[items] = {};
    return this[updateItemStorage]();
  }

  save() {
    return this[updateItemStorage]();
  }
}

module.exports = ListStorage;
