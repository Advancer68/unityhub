const path = require('path');

const releaseFileIO = require('../../localInstaller/lib/releaseFileIO');
const editorUtils = require('../editorUtils');
const EditorFolderScanner = require('./editorFolderScanner');
const logger = require('../../../logger')('InstalledEditorLocation');

// Private symbols
const editorFolderScanner = Symbol();
const installPath = Symbol();
const editors = Symbol();

const generateInstalledEditors = Symbol();
const generateEditorFromFolderName = Symbol();
const fetchModules = Symbol();

class InstalledEditorLocation {

  get editors() { return this[editors]; }

  constructor(_installPath) {
    this[editorFolderScanner] = new EditorFolderScanner(_installPath);
    this[installPath] = _installPath;
    this[editors] = {};
  }

  scanForInstallsAndGenerateEditors() {
    return this[editorFolderScanner].fetchEditorFolders()
      .then(() => this[generateInstalledEditors]());
  }

  [generateInstalledEditors]() {
    const generatedEditorsPromises = [];

    this[editorFolderScanner].editorFolders.forEach((folderName) => {
      generatedEditorsPromises.push(this[generateEditorFromFolderName](folderName));
    });

    return Promise.all(generatedEditorsPromises);
  }

  [generateEditorFromFolderName](folderName) {
    const basePath = path.join(this[installPath], folderName);
    const moduleInfoFilePath = path.join(basePath, releaseFileIO.MODULES_FNAME);

    return this[fetchModules](moduleInfoFilePath).then((modules) => {
      const newEditor = {
        version: folderName,
        location: [editorUtils.getEditorPathFromFolder(basePath)],
        folderPath: basePath
      };

      // Located editor if modules.json was not found.
      if (modules.length > 0) {
        newEditor.modules = modules;
      } else {
        newEditor.manual = true;
      }

      this[editors][folderName] = newEditor;
    });
  }

  [fetchModules](modulesFilePath) {
    return releaseFileIO.read(modulesFilePath).catch(() => {
      logger.warn(`Failed to read from ${modulesFilePath}, using fallback release info`);
      return [];
    });
  }

  updateEditorVersion(version) {
    return this[generateEditorFromFolderName](version);
  }

  removeEditor(editor) {
    delete this[editors][editor.version];
    return Promise.resolve();
  }
}

module.exports = InstalledEditorLocation;
