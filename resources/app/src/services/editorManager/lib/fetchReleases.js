const requestp = require('request-promise-native'); // todo: switch to axios requests when integrating module.
const logger = require('../../../logger')('FetchReleases');
const { fs } = require('../../../fileSystem');
const iniUtility = require('ini');
const os = require('os');
const saihaiDataFormatter = require('./saihaiDataFormatter');

const platform = os.platform();

const fetchAllVersionsFromSaihai = Symbol();
const getConfigurations = Symbol();
const handleConfigurationsData = Symbol();
// Where cached result will be stored
const saihaiData = Symbol();

class FetchReleases {

  /**
   * Before calling this method, make sure isSaihaiAvailable() returns true, otherwise it will throw error
   * @returns {Promise<any>} releases object
   */
  async fetch() {
    const isSaihaiAvailable = await this.isSaihaiAvailable();
    if (!isSaihaiAvailable) {
      logger.warn('FetchReleases.get() was called when http://saihai.hq.unity3d.com is not available');
      throw new Error('http://saihai.hq.unity3d.com is not available');
    }

    const saihaiFreshData = await this[fetchAllVersionsFromSaihai]();
    this[saihaiData] = saihaiDataFormatter.format(saihaiFreshData);
    this[saihaiData] = await this[getConfigurations](this[saihaiData]);
    return this[saihaiData];
  }
  /**
   * @returns {any} saihaiData
   * NOTE: saihaiData will be available after fetch() method is executed and isSaihaiAvailable() returns true.
   *
   * Otherwise false will be returned
   */
  get() {
    return this[saihaiData] ? this[saihaiData] : false;
  }
  /**
   * Deletes cache
   */
  deleteCache() {
    this[saihaiData] = undefined;
  }

  /**
   * @description Save cached saihaiData to file system
   * @param {string} dir e.g. D:\\saihaiData.json
   * @param {boolean} formatted Should json be formatted or not (spaces will be added)
   *
   * NOTE: formatted option will result in bigger json size.
   */
  async save(dir, formatted = false) {
    if (this[saihaiData]) {
      try {
        await fs.outputFile(dir, JSON.stringify(this[saihaiData], null, formatted ? 4 : 0));
        return true;
      } catch (e) {
        logger.warn(`Error when trying to save saihaiData to file\n${e.message} is saihaiData defined: ${!!this[saihaiData]}`);
        throw e;
      }
    }
    return false;
  }

  /**
   *
   * @param {string} state - Can be either 'promoted' or 'published'
   * @param {Array<any>} releases - All releases(without modules) (this is used for recursive call)
   * @returns {Promise<Array<any>>} - Returns all releases from saihai api
   */
  async [fetchAllVersionsFromSaihai](state = 'promoted', releases = []) {
    try {
      // Get releases count
      const { count } = await requestp({
        uri: `http://saihai.hq.unity3d.com/api/json/v2/builds/${state}/?format=json&limit=0&offset=0`,
        json: true
      });
      // Get releases
      const { results } = await requestp({
        uri: `http://saihai.hq.unity3d.com/api/json/v2/builds/${state}/?format=json&limit=${count}&offset=0`,
        json: true
      });

      releases = releases.concat(results);
      // Do the same for published builds and return data
      if (state === 'promoted') {
        releases = await this[fetchAllVersionsFromSaihai]('published', releases);
        return releases;
      }

      return releases;
    } catch (e) {
      logger.warn(`Trying to get ${state} releases\n${e.message}`);
      throw e;
    }
  }

  async [getConfigurations](releases) {

    var promises = [];
    for (let i = 0; i < releases.length; i++) {
      promises.push(this[handleConfigurationsData](releases[i], i, releases));
    }
    try {
      await Promise.all(promises);
    } catch (e) {
      logger.warn(`Error occured when trying to get configuration data\n${e}`);
    }

    return this[saihaiData];
  }

  async [handleConfigurationsData](release, index, releases) {
    let modulesData;
    try {
      modulesData = await requestp(release.iniUrl);
    } catch (e) {
      // The ini file for this version doesn't exist
      logger.warn(`Configuration file is not available for ${release.version} (${release.revision_medium}), url: ${release.iniUrl}`);
      return;
    }

    try {
      modulesData = iniUtility.parse(modulesData);
    } catch (e) {
      logger.warn(`Configuration file is not valid. Parsing failed. ${release.version} (${release.revision_medium}), url: ${release.iniUrl}`);
      return;
    }

    Object.keys(modulesData).forEach((key) => {
      let newKey = key;
      if (key === 'Unity') newKey = 'Editor';

      if (releases[index].modules[newKey]) {
        releases[index].modules[newKey].description = modulesData[key].description;
        releases[index].modules[newKey].installedSize = platform === 'win32' ? modulesData[key].installedsize * 1024 : modulesData[key].installedsize;
        releases[index].modules[newKey].downloadSize = platform === 'win32' ? modulesData[key].size * 1024 : modulesData[key].size;
        releases[index].modules[newKey].checksum = modulesData[key].md5;
        releases[index].modules[newKey].internal = false;
      }
    });

    this[saihaiData] = releases;
  }
  /**
   *
   * @returns {Object} e.g.
   * @example
   * {
   *  2018.1:[<...>]
   *  5.6:[<...>]
   * }
   */
  getReleasesMappedByBaseVersion(replace = false) {
    if (!this[saihaiData]) return false;
    if (this[saihaiData].constructor === Object) return this[saihaiData];
    // Map all version by their base version
    const res = this[saihaiData].reduce((obj, release) => {
      if (!obj[release.version_base]) obj[release.version_base] = [];
      obj[release.version_base].push(release);
      return obj;
    }, {});
    // Sort it
    Object.keys(res).map((baseVersion) => {
      res[baseVersion].sort((a, b) => b.timestamp_sort - a.timestamp_sort);
      return baseVersion;
    });

    if (replace) this[saihaiData] = res;
    return res;
  }
  /**
   *
   * @param {boolean} replace
   * should cache be replaced with public releases only (Not recommended)
   * @description Public release is release that has configuration file, same goes with modules
   */
  getPublicReleases(replace = false) {
    let publicReleases;
    if (this[saihaiData].constructor === Object) {
      publicReleases = {};
      // Iterate through base versions
      Object.keys(this[saihaiData]).map((key) => {
        if (!publicReleases[key]) publicReleases[key] = [];
        // Iterate through Unity releases
        this[saihaiData][key].map((version, index) => {
          // If editor is visible internaly only, just skip
          if (this[saihaiData][key][index].modules.Editor.internal) return version;
          const id = publicReleases[key].push(this[saihaiData][key][index]) - 1;
          // Iterate through Unity release modules
          Object.keys(this[saihaiData][key][index].modules).map((modName) => {
            // If version is internal remove this module from list
            if (this[saihaiData][key][index].modules[modName].internal) {
              delete publicReleases[key][id].modules[modName];
            }
            return modName;
          });
          return version;
        });
        return key;
      });
      // Important return
      if (replace) this[saihaiData] = publicReleases;
      return publicReleases;
    }

    publicReleases = [];

    this[saihaiData].map((version, index) => {
      if (!version.modules.Editor.internal) {
        const id = publicReleases.push(version) - 1;
        Object.keys(this[saihaiData][index].modules).map((modName) => {
          if (this[saihaiData][index].modules[modName].internal) {
            delete publicReleases[id].modules[modName];
          }
          return modName;
        });
      }
      return version;
    });
    if (replace) this[saihaiData] = publicReleases;
    return publicReleases;
  }
  /**
   * @returns {Promise<boolean>} If saihai api is available
   */
  async isSaihaiAvailable() {
    try {
      await requestp('http://saihai.hq.unity3d.com/api/json/v2/?format=json');
      return true;
    } catch (e) {
      return false;
    }
  }
}

module.exports = new FetchReleases();
