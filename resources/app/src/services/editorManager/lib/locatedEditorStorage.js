const promisify = require('es6-promisify');
const localStorage = require('electron-json-storage');
const logger = require('../../../logger')('LocatedEditorStorage');

// Private symbols
const storageKey = Symbol();
const editors = Symbol();
const updateEditorStorage = Symbol();
const validateEditorLocation = Symbol();
const isLocationAnArray = Symbol();

class LocatedEditorStorage {
  
  get editors() { return this[editors]; }
  
  constructor(_storageKey) {
    this[storageKey] = _storageKey;
    this[editors] = {};
  }
  
  fetchEditors() {
    return promisify(localStorage.get)(this[storageKey])
      .then((_editors) => {
        logger.info(`${Object.keys(_editors).length} located Editors found in storage`);
        this[editors] = _editors;
        this[validateEditorLocation]();
      })
      .catch((err) => {
        logger.debug(err);
        logger.warn('The list of manually located Editors is corrupted, flushing list...');
        this[editors] = {};
        return this[updateEditorStorage]();
      });
  }
  
  [updateEditorStorage]() {
    return promisify(localStorage.set)(this[storageKey], this[editors]);
  }
  
  /**
   * Makes sure location attribute of each editor is an array.
   * @migration
   */
  [validateEditorLocation]() {
    Object.keys(this[editors]).forEach((version) => {
      const editor = this[editors][version];
      
      if (!this[isLocationAnArray](editor.location)) {
        editor.location = [editor.location];
      }
    });
  }
  
  [isLocationAnArray](location) {
    return location instanceof Array;
  }
  
  addEditor(editor) {
    this[editors][editor.version] = editor;
    return this[updateEditorStorage]();
  }
  
  removeEditor(editor) {
    delete this[editors][editor.version];
    return this[updateEditorStorage]();
  }
}

module.exports = LocatedEditorStorage;
