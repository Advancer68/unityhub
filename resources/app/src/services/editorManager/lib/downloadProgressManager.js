const localStorageCallback = require('electron-json-storage');
const promisify = require('es6-promisify');

const logger = require('../../../logger')('EditorDownloadProgress');

const localStorage = {
  get: promisify(localStorageCallback.get),
  set: promisify(localStorageCallback.set),
  remove: promisify(localStorageCallback.remove)
};

const IN_PROGRESS = 'IN_PROGRESS';
const DOWNLOAD_PROGRESS = 'downloadProgress';

class DownloadProgressManager {
  async isEditorDownloadComplete(editorId) {
    const storageKey = this._getDownloadProgressKey(editorId);
    try {
      const downloadProgress = await localStorage.get(storageKey);
      return downloadProgress !== IN_PROGRESS;
    } catch (err) {
      logger.error(`Unable to check for the download progress of editor: ${err}`);
      return true;
    }
  }

  async startTrackingEditorDownloadProgress(editorId) {
    const storageKey = this._getDownloadProgressKey(editorId);
    try {
      await localStorage.set(storageKey, IN_PROGRESS);
    } catch (err) {
      logger.error(`Unable to store download progress of editor in storage: ${err}`);
    }
  }

  async setEditorDownloadProgressToComplete(editorId) {
    const storageKey = this._getDownloadProgressKey(editorId);
    try {
      await localStorage.remove(storageKey);
    } catch (err) {
      logger.error(`Unable to remove download progress of editor in storage: ${err}`);
    }
  }

  _getDownloadProgressKey(editorId) {
    return `${DOWNLOAD_PROGRESS}:${editorId}`;
  }
}

module.exports = new DownloadProgressManager();
