const pathutil = require('path');
const { fs, hubFS } = require('../../../fileSystem');
const logger = require('../../../logger')('EditorSearch');
const postal = require('postal');

const editorUtils = require('../editorUtils');

const PATHS = {
  EDITOR_OLD: editorUtils.getEditorLocationPatterns('editor_old'), // 4.7 and lower
  EDITOR: editorUtils.getEditorLocationPatterns('editor')
};

class EditorSearch {
  /**
   * @param {string | Array} startPath // Starting path e.g. "D:\\" or ["D:\\", "C:\\"]
   * @param {number} levels  // how deep search will go in to the directories, defaults to 3
   */
  constructor(startPath, levels = 3) {
    this.startPath = startPath || [];
    this.levels = levels;
    // for caching editor paths
    this.editorPaths = [];
  }

  /**
   * Async Unity Editor search.
   * If callback is passed as parameter, will return Array<string> in callback.
   */
  async search(callback = false) {
    let editorPaths = [];
    try {
      // Set start path as Array always
      this.startPath = typeof this.startPath === 'string' ? [this.startPath] : this.startPath;

      for (const path of this.startPath) {
        /* eslint-disable no-await-in-loop */
        editorPaths = editorPaths.concat(await recursiveEditorSearch(path, this.levels));
      }

      editorPaths = Array.from(new Set(editorPaths));
      // cache paths
      this.editorPaths = editorPaths;

      if (callback) callback(null, editorPaths);
      emit('done', editorPaths);

      return editorPaths;
    } catch (e) {
      if (callback) callback(e, []);
      emit('error', e);
      logger.warn(e);
      throw e;
    }
  }

  /**
   * @param {Array<string>} paths (OPTIONAL) Takes absolute Editor paths e.g. "C:\Editor\Unity.exe"
   * @param {Function} callback callback can be passed as first argument as well (instead of paths) e.g. getEditorsInstalledWithHub((res, error)=>res);
   * Returns Array<string> of absolute paths of Editors that were installed with Unity HUB through promise or callback.
   * Callback returns paths as first parameter and error as second;
   *
   * USE CASES:
   * // Passing paths and callback
   * getEditorsInstalledWithHub(["C:\Editor\Unity.exe"], (error, paths)=>{});
   * // Passing only callback
   * getEditorsInstalledWithHub((error, paths)=>{});
   * // Using promises
   * getEditorsInstalledWithHub().then((res)=>{}).catch((err)=>{});
   */
  async getEditorsInstalledWithHub(paths, callback) {
    // If first parameter comes as callback
    if (paths instanceof Function) {
      callback = paths;
      paths = this.editorPaths;
    } else {
      paths = paths || this.editorPaths;
    }

    const editorsInstalledWithHub = [];
    // const promises = [];
    for (const path of paths) {
      try {
        const exists = await editorUtils.isEditorInstalledWithHub(path);
        if (exists) {
          editorsInstalledWithHub.push(path);
        }
      } catch (e) {
        logger.verbose(`Reading ${path} failed. Continue to next path. \nError: ${e.message}`);
      }
    }

    if (callback) callback(null, editorsInstalledWithHub);
    emit('done', editorsInstalledWithHub);
    return editorsInstalledWithHub;

  }

  /**
   * Get Editor paths.
   * NOTE: search() needs to be run atleast once.
   */
  getEditorPaths() {
    return this.editorPaths;
  }
}

/**
 * private function
 * Recursively search through directories and find Unity Installers
 * @param {string} startPathe starting directory path e.g. D://UnityVersions
 * @param {number} level how deep through sub-directories it will search e.g. 4
 * @param {Array<string>} paths (Optional) every path that we find, we will store in this array and return it at the end
 * @param {boolean} done (Optional) if its the first call of this recursive function, so it should always start with true
 * @returns {Array<string>}
 */
async function recursiveEditorSearch(startPath, level, paths = [], done = true) {

  let currDir;
  if (--level <= 0) {
    return paths;
  }

  let fileNames;
  try {
    fileNames = await fs.readdir(startPath);
  } catch (e) {
    logger.verbose(`Can't read ${startPath}. Stopping function execution and returning paths, is it first call: ${done}.\nError:${e.message}`);
    return paths;
  }

  const asyncCount = fileNames.length;
  if (!asyncCount) return paths;

  for (const fileName of fileNames) {

    try {
      currDir = await fs.lstat(pathutil.join(startPath, fileName));
    } catch (e) {
      logger.verbose(`Reading ${pathutil.join(startPath, fileName)} failed. Continue to next path. \nError: ${e.message}`);
    }

    try {
      if (currDir && currDir.isDirectory()) {

        const pathNewVersion = pathutil.join(startPath, fileName, PATHS.EDITOR);
        // This will be defined only on win32 platform
        const pathOldVersion = PATHS.EDITOR_OLD ? pathutil.join(startPath, fileName, PATHS.EDITOR_OLD) : false;

        const newPathExists = await fs.pathExists(pathNewVersion);

        if (newPathExists) {
          paths.push(pathNewVersion);
        }

        if (pathOldVersion) {
          const oldPathExists = await fs.pathExists(pathOldVersion);
          if (oldPathExists) {
            paths.push(pathOldVersion);
          }
        }

        paths = await recursiveEditorSearch(pathutil.join(startPath, fileName), level, paths, false);
      }
    } catch (e) {
      logger.warn(e);
      throw e;
    }
  }

  // Finising up
  if (done) {

    // Paths shouldn't include Editors from recycle bin
    // Can't use Array.filter() with async :/
    const tmpPaths = [];
    for (const path of paths) {
      if (!await hubFS.isInRecycleBin(path)) tmpPaths.push(path);
    }

    paths = tmpPaths;

    // Also checking if current folder is not Editor as well
    for (const fileName of fileNames) {
      if (fileName === 'Unity.app') {
        const path = pathutil.join(startPath, PATHS.EDITOR);
        if (await fs.pathExists(path)) {
          paths.push(path);
        }
      } else if (fileName === 'Unity.exe') {
        paths.push(pathutil.join(startPath, fileName));
      }
    }
  }
  return paths;
}

/**
 *
 * @param {string} eventName e.g. 'done', 'error', later will add 'data' & 'data-all'
 * @param {any} eventData what data will be passed to channel.topic
 */
function emit(eventName, eventData) {
  postal.publish({
    channel: 'EditorSearch',
    topic: eventName,
    data: eventData
  });
}

module.exports = EditorSearch;
