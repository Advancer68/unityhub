const promisify = require('es6-promisify');
const localStorage = require('electron-json-storage');

const InstalledEditorLocation = require('./installedEditorLocation');
const logger = require('../../../logger')('customInstallLocation');

// Private symbols
const installedEditorLocation = Symbol();
const installPathKey = Symbol();
const installPath = Symbol();

const fetchInstallPath = Symbol();
const getInstallPath = Symbol();
const isInstallPathValid = Symbol();
const updateInstallLocation = Symbol();
const setInstallPath = Symbol();

class CustomInstallLocation {
  
  get editors() {
    if (!this[installedEditorLocation]) {
      return {};
    }
    
    return this[installedEditorLocation].editors;
  }
  
  get installPath() { return this[installPath]; }
  
  constructor(_installPathKey) {
    this[installPathKey] = _installPathKey;
    this[installPath] = '';
  }
  
  initializeCustomLocation() {
    return this[fetchInstallPath]()
      .then(() => this[updateInstallLocation]());
  }
  
  [fetchInstallPath]() {
    return this[getInstallPath]()
      .then((_installPath) => {
        this[installPath] = _installPath;
      })
      .catch((error) => {
        if (error) {
          logger.warn('Custom install path could not be retrieved: ', error);
        }
        
        logger.info('Custom install path not present.');
        this[setInstallPath]('');
      });
  }
  
  [getInstallPath]() {
    return promisify(localStorage.get)(this[installPathKey])
      .then((_installPath) => {
        
        if (this[isInstallPathValid](_installPath)) {
          return Promise.resolve(_installPath);
        }
        
        return Promise.reject();
      });
  }
  
  [isInstallPathValid](_installPath) {
    return _installPath !== undefined && typeof _installPath === 'string';
  }
  
  [setInstallPath](_installPath) {
    return promisify(localStorage.set)(this[installPathKey], _installPath)
      .then(() => {
        this[installPath] = _installPath;
      });
  }
  
  [updateInstallLocation]() {
    this[installedEditorLocation] = undefined;
    
    if (this[installPath]) {
      this[installedEditorLocation] = new InstalledEditorLocation(this[installPath]);
    }
  }
  
  scanForInstallsAndGenerateEditors() {
    if (!this[installedEditorLocation]) {
      logger.info(`Cannot scan custom location ${this[installPath] || '(no path set)'}.`);
      return Promise.resolve();
    }
    
    return this[installedEditorLocation].scanForInstallsAndGenerateEditors();
  }
  
  updateEditorVersion(version) {
    if (!this[installedEditorLocation]) {
      logger.info(`Secondary location not set. Cannot update the Editor ${version}.`);
      return Promise.resolve();
    }
    
    return this[installedEditorLocation].updateEditorVersion(version);
  }
  
  removeEditor(editor) {
    if (!this[installedEditorLocation]) {
      logger.info('Secondary location not set. Cannot remove the Editor ', editor);
      return Promise.resolve();
    }
  
    return this[installedEditorLocation].removeEditor(editor);
  }
  
  setInstallLocation(_installPath) {
    return this[setInstallPath](_installPath)
      .then(() => this[updateInstallLocation]());
  }
}

module.exports = CustomInstallLocation;
