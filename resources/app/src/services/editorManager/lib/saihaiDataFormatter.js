const editorUtils = require('../editorUtils');
const moment = require('moment');

const saihaiModules = editorUtils.getSaihaiModuleNames();
const saihaiSettings = editorUtils.getSaihaiSettings();

const getReleasesArray = Symbol();
const generateLinksForModules = Symbol();

const {
  unityPrefix,
  setupPostfix,
  platformName,
  platformNamePostfix,
  installerFormat,
  targetSupportString,
  configurationPlatformName
} = saihaiSettings;

class SaihaiDataFormatter {
  /**
   * Main method for getting releases in Array type with hard-coded URLs
   * @param {Array} freshSaihaiData json from http://saihai.hq.unity3d.com/api/json/v2/builds/*
   * @returns {Array} Unity releases
   */
  format(freshSaihaiData) {
    let result;
    // Generate Array of unique releases and remove undesirable properties
    result = this[getReleasesArray](freshSaihaiData);
    // Generate links for each module
    result = this[generateLinksForModules](result);
    // Return releases
    return result;
  }

  [getReleasesArray](freshSaihaiData) {
    return freshSaihaiData.reduce((releases, version) => {
      const release = {};
      // Check for duplicate releases
      const index = releases.find((val) => val.revision_medium === version.revision_short);
      if (!index) {
        // e.g. 2018.2
        release.version_base = version.version.split('.').slice(0, 2).join('.');
        // e.g. 2018.2.0b2
        release.version = version.version;
        // e.g. 0a6b93065060
        release.revision_medium = version.revision_short;
        // e.g. 0a6b93
        release.revision_short = version.revision_short.slice(0, 6);
        // e.g. "2018-05-02 02:51:25"
        release.promoted_on = version.promoted_on ? moment(version.promoted_on).format('YYYY-MM-DD HH:mm:ss') : null;
        release.published_on = version.published_on ? moment(version.published_on).format('YYYY-MM-DD HH:mm:ss') : null;
        // Is version published already? true/false
        release.published = !!version.published_on;

        release.timestamp = moment(release.promoted_on).unix();
        release.timestamp_sort = release.published ? moment(release.published_on).unix() : moment(release.promoted_on).unix();

        release.installed = false;
        // Modules will include 'Editor' too
        release.modules = {};
        // Add katana url property for later use
        release.katana_url = version.katana_url;
        // Configuration file for version (.ini) url, I am not adding saihai link since everytime saihai link works, beta link works too but not vice versa.
        release.iniUrl = `http://beta.unity3d.com/download/${release.revision_medium}/unity-${release.version}-${configurationPlatformName}.ini`;
        // Save everything as new element
        releases.push(release);
      }
      return releases;
    }, []);
  }

  [generateLinksForModules](releases) {
    return releases.map((release) => {

      const katanaUrlSplit = release.katana_url.split('/');
      const fallbackUrlKatanaPart = `${katanaUrlSplit[6]}-${katanaUrlSplit[8].split('?')[0]}`;
      delete release.katana_url;

      saihaiModules.forEach((mod) => {
        const urls = [];
        let displayName = `${mod.name} Support`;
        let platformOther;
        if (mod.name === 'Editor') {
          displayName = `Unity ${mod.name}`;
          urls.push(`http://saihai.hq.unity3d.com/static/artifacts/${fallbackUrlKatanaPart}/${platformName}EditorInstallerSet/${platformName}${platformNamePostfix}EditorInstaller/Unity${setupPostfix}-${platformNamePostfix}.${installerFormat}`);
          urls.push(`http://beta.unity3d.com/download/${release.revision_medium}/${platformName}${platformNamePostfix}EditorInstaller/Unity${setupPostfix}${platformNamePostfix}-${release.version}.${installerFormat}`);
        } else if (mod.name === 'StandardAssets' || mod.name === 'Documentation') {
          displayName = mod.name.split(/(?=[A-Z])/).join(' ');
          urls.push(`http://saihai.hq.unity3d.com/static/artifacts/${fallbackUrlKatanaPart}/${platformName}EditorInstallerSet/${platformName}${mod.name}Installer/${unityPrefix}${mod.name}${setupPostfix}.${installerFormat}`);
          urls.push(`http://beta.unity3d.com/download/${release.revision_medium}/${platformName}${mod.name}Installer/${unityPrefix}${mod.name}${setupPostfix}-${release.version}.${installerFormat}`);
        } else if (mod.name === 'Wii-U' || mod.name === 'Playstation-3' || mod.name === 'Playstation-4' || mod.name === 'Playstation-Vita' || mod.name === 'Xbox-One') {
          if (mod.name === 'Wii-U') platformOther = 'wiiu';
          else if (mod.name === 'Playstation-3') platformOther = 'ps3';
          else if (mod.name === 'Playstation-4') platformOther = 'ps4';
          else if (mod.name === 'Playstation-Vita') platformOther = 'psp2';
          else if (mod.name === 'Xbox-One') platformOther = 'xboxOne';
          urls.push(`http://saihai.hq.unity3d.com/static/artifacts/${fallbackUrlKatanaPart}/${platformOther}/UnitySetup-${mod.name}-Support-for-Editor-${release.version}.${installerFormat}`);
        } else if (mod.name === 'Metro') {
          // 2018.2+ changed from metro-support to UWP-.NET-support
          urls.push(`http://saihai.hq.unity3d.com/static/artifacts/${fallbackUrlKatanaPart}/${platformName}EditorInstallerSet/${targetSupportString}/UnitySetup-UWP-.NET-Support-for-Editor-${release.version}.${installerFormat}`);
          urls.push(`http://beta.unity3d.com/download/${release.revision_medium}/${targetSupportString}/UnitySetup-UWP-.NET-Support-for-Editor-${release.version}.${installerFormat}`);
          // 2018.1 and lower
          urls.push(`http://saihai.hq.unity3d.com/static/artifacts/${fallbackUrlKatanaPart}/${platformName}EditorInstallerSet/${targetSupportString}/UnitySetup-${mod.name}-Support-for-Editor-${release.version}.${installerFormat}`);
          urls.push(`http://beta.unity3d.com/download/${release.revision_medium}/${targetSupportString}/UnitySetup-${mod.name}-Support-for-Editor-${release.version}.${installerFormat}`);
        } else if (mod.name === 'Windows') {
          // 2018.1+ changed from Windows-Support to Windows-Mono-Support
          urls.push(`http://saihai.hq.unity3d.com/static/artifacts/${fallbackUrlKatanaPart}/${platformName}EditorInstallerSet/${targetSupportString}/UnitySetup-${mod.name}-Mono-Support-for-Editor-${release.version}.${installerFormat}`);
          urls.push(`http://beta.unity3d.com/download/${release.revision_medium}/${targetSupportString}/UnitySetup-${mod.name}-Mono-Support-for-Editor-${release.version}.${installerFormat}`);
          // 2017.4 and lower
          urls.push(`http://saihai.hq.unity3d.com/static/artifacts/${fallbackUrlKatanaPart}/${platformName}EditorInstallerSet/${targetSupportString}/UnitySetup-${mod.name}-Support-for-Editor-${release.version}.${installerFormat}`);
          urls.push(`http://beta.unity3d.com/download/${release.revision_medium}/${targetSupportString}/UnitySetup-${mod.name}-Support-for-Editor-${release.version}.${installerFormat}`);
        } else {
          // Default links
          urls.push(`http://saihai.hq.unity3d.com/static/artifacts/${fallbackUrlKatanaPart}/${platformName}EditorInstallerSet/${targetSupportString}/UnitySetup-${mod.name}-Support-for-Editor-${release.version}.${installerFormat}`);
          urls.push(`http://beta.unity3d.com/download/${release.revision_medium}/${targetSupportString}/UnitySetup-${mod.name}-Support-for-Editor-${release.version}.${installerFormat}`);
        }

        release.modules[mod.name] = {
          nameFull: displayName,
          name: mod.name,
          url: urls,
          timestamp: release.timestamp,
          timestamp_sort: release.timestamp_sort,
          downloaded: false,
          downloadedTo: null,
          installed: false,
          installedTo: null,
          description: null,
          installedSize: null,
          downloadSize: null,
          checksum: null,
          internal: true
        };
      }); // End of module forEach
      return release;
    });// End of release map
  }
}

module.exports = new SaihaiDataFormatter();
