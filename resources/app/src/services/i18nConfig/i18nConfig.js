const { app } = require('electron');
const i18next = require('i18next');
const promisify = require('es6-promisify');
const localStorageAsync = require('electron-json-storage');

const localStorage = {
  get: promisify(localStorageAsync.get, localStorageAsync),
  set: promisify(localStorageAsync.set, localStorageAsync),
};

const LANGUAGE_STORAGE_KEY = 'languageConfig';

const languages = [
  { name: 'English', code: 'en', locale: 'en_US' },
  { name: '日本語', code: 'ja', locale: 'ja_JP' },
  { name: '한국어', code: 'ko', locale: 'ko_FR' },
  { name: '简体中文', code: 'zh_CN', locale: 'zh_CN' },
  { name: '繁體中文', code: 'zh_TW', locale: 'zh_TW' }
];

class i18nConfig {

  getAvailableLanguages() {
    // Add pseudo-localization if running in debug mode
    if (app.argv.debug) {
      return languages.concat({ name: 'Pseudo-localization', code: 'ploc' });
    }

    return languages;
  }

  getLocale() {
    const currentLanguage = i18next.language;
    const data = languages.find(x => x.code === currentLanguage);
    return data ? data.locale : 'en_US';
  }

  async getLanguage() {
    const data = await localStorage.get(LANGUAGE_STORAGE_KEY);
    return data ? data.language : undefined;
  }

  async setLanguage(code) {
    await localStorage.set(LANGUAGE_STORAGE_KEY, { language: code });
    return await i18next.changeLanguage(code);
  }
}

module.exports = new i18nConfig();
