// Disabling this rule to deal with proxyquire and typescipt import problem
/* eslint-disable new-cap */

import sinon from 'sinon';
import chai from 'chai';
import sinonChai from 'sinon-chai';
import proxyquire from 'proxyquire';
import windowManager from './windowManager/windowManager';
import unityHubProtocolHandler from './unityhubProtocolHandler';
import cloudAnalytics from './services/cloudAnalytics/cloudAnalytics';
import BaseBootstrap from './bootstrapSequences/baseBootstrap';
import FileAssociationBootstrap from './bootstrapSequences/fileAssociationBootstrap';
import InstallServiceBootstrap from './bootstrapSequences/installServiceBootstrap';
import SecondAppInstanceBootstrap from './bootstrapSequences/secondAppInstanceBootstrap';
import Bootstrap from './bootstrap';
import BugReporterBootStrap from './bootstrapSequences/bugReporterBootstrap';
import CliBootStrap from './bootstrapSequences/cliBootstrap';

chai.use(sinonChai);

const { expect } = chai;

describe('bootstrap.js', () => {
  let sandbox;
  let bootstrap;
  let yargs;
  let os;
  let electronApp;
  let isSecondAppInstance;
  let firstAppInstanceCallback;

  beforeEach(() => {
    sandbox = sinon.sandbox.create();

    isSecondAppInstance = false;
    electronApp = {
      requestSingleInstanceLock: sandbox.stub().callsFake(() => !isSecondAppInstance),
      on: sandbox.stub().callsFake((eventName, callback) => {
        if (eventName === 'second-instance') {
          firstAppInstanceCallback = callback;
        }
      }),
    };
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('by default', () => {
    it('should set isSecondAppInstance properly', () => {
      bootstrap = new Bootstrap(electronApp);
      expect(bootstrap.isSecondAppInstance).to.equal(isSecondAppInstance);
    });
  });

  describe('when second app instance starts', () => {
    beforeEach(() => {
      bootstrap = new Bootstrap(electronApp);
      sandbox.stub(windowManager.mainWindow, 'show');
      firstAppInstanceCallback();
    });

    it('should display the onboardingWindow', () => {
      expect(windowManager.mainWindow.show).to.have.been.called;
    });
  });

  describe('start', () => {
    beforeEach(() => {
      sandbox.stub(BaseBootstrap, 'start');
      sandbox.stub(InstallServiceBootstrap, 'start');
      sandbox.stub(FileAssociationBootstrap, 'start');
      sandbox.stub(BugReporterBootStrap, 'start');
      sandbox.stub(CliBootStrap, 'start');
      sandbox.stub(SecondAppInstanceBootstrap, 'start');
    });

    describe('when argument --winInstaller is passed', () => {
      beforeEach(() => {
        yargs = { argv: { winInstaller: true } };
      });

      describe('on windows', () => {
        beforeEach(() => {
          os = {
            platform: (): string => 'win32',
          };
        });

        it('should call InstallServiceBootstrap sequence', () => {
          bootstrap = new (proxyquire('./bootstrap', {
            yargs,
            os,
          }).default)(electronApp);

          bootstrap.start();

          expect(InstallServiceBootstrap.start).to.have.been.called;
        });
      });

      describe('on mac', () => {
        beforeEach(() => {
          os = {
            platform: (): string => 'darwin',
          };
        });

        it('should call BaseBootstrap sequence', () => {
          bootstrap = new (proxyquire('./bootstrap', {
            yargs,
            os,
          }).default)(electronApp);

          bootstrap.start();
          expect(BaseBootstrap.start).to.have.been.called;
        });
      });
    });

    describe('when application is a second instance', () => {
      beforeEach(() => {
        bootstrap = new Bootstrap(electronApp);
        bootstrap.isSecondAppInstance = true;

        sandbox.stub(cloudAnalytics, 'quitEvent').resolves();
      });

      describe('by default', () => {
        beforeEach(() => bootstrap.start());

        it('should send an analytics event', () => {
          expect(cloudAnalytics.quitEvent).to.have.been.called;
        });

        it('should call SecondAppInstanceBootstrap sequence', () => {
          expect(SecondAppInstanceBootstrap.start).to.have.been.called;
        });
      });

      describe('and was launched with a custom uri scheme passed as an argument', () => {
        let customUriArgument;
        beforeEach(() => {
          customUriArgument = `${unityHubProtocolHandler.fullProtocolName}foo/bar`;
          process.argv = [customUriArgument];
          return bootstrap.start();
        });

        it('should call FileAssociationBootstrap sequence', () => {
          expect(FileAssociationBootstrap.start).to.have.been.called;
        });
      });
    });

    describe('bootstrapping with --bugReporter', () => {
      beforeEach(() => {
        bootstrap = new (proxyquire('./bootstrap', {
          yargs: { argv: { bugReporter: true } },
        }).default)(electronApp);

        bootstrap.start();
      });

      it('should call BugReporterBootStrap sequence', () => {
        expect(BugReporterBootStrap.start).to.have.been.called;
      });
    });

    describe('bootstrapping with --headless', () => {
      beforeEach(() => {
        bootstrap = new (proxyquire('./bootstrap', {
          yargs: { argv: { headless: true } },
        }).default)(electronApp);

        bootstrap.start();
      });

      it('should call cliBootStrap sequence', () => {
        expect(CliBootStrap.start).to.have.been.called;
      });
    });

    describe('bootstrapping with nothing', () => {
      beforeEach(() => {
        bootstrap = new (proxyquire('./bootstrap', {
          yargs: { argv: {} },
        }).default)(electronApp);
        process.argv = [];

        bootstrap.start();
      });

      it('should call BaseBootstrap sequence', () => {
        expect(BaseBootstrap.start).to.have.been.called;
      });
    });
  });
});
