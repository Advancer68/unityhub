const { app, dialog } = require('electron');
const logger = require('../logger')('ErrorBox');
const cloudAnalytics = require('../services/cloudAnalytics/cloudAnalytics');

class ErrorBox {
  
  /**
   * Handle critical errors ourselves instead of letting electron do it.
   */
  replaceElectronAPI() {
    dialog.showErrorBox = this.showErrorBox.bind(this);
  }
  
  showErrorBox(title, content) {
    logger.error(`${title}\n${content}`);
    // these network related errors are excluded since auto-update send them and
    // the application should not stop working due to network error.
    if (content.includes('ERR_INTERNET_DISCONNECTED')
      || content.includes('ERR_CONNECTION_CLOSED')
      || content.includes('ERR_TIMED_OUT')
      || content.includes('ERR_ABORTED')
      || content.includes('ERR_CONNECTION_RESET')
      || content.includes('ERR_CONTENT_LENGTH_MISMATCH')) {
      return;
    }
  
    dialog.showMessageBox({
      type: 'error',
      title: 'Critical error',
      message: 'The Hub as encountered a critical error and must close. \nYou can report a bug using our bug reporter tool',
      buttons: ['Report a bug', 'Restart the Hub', 'Quit the Hub']
    }, (buttonIndex) => {
      const showBugReporter = buttonIndex === 0;
      const shouldRelaunch = buttonIndex === 1;
      
      if (showBugReporter) {
        app.showBugReporter();
      }
      
      cloudAnalytics.quitEvent('Error')
        .then(() => this.quitOrRelaunch(shouldRelaunch))
        .catch(() => this.quitOrRelaunch(shouldRelaunch));
    });
  }
  
  quitOrRelaunch(shouldRelaunch) {
    if (shouldRelaunch) {
      app.relaunch();
    } else {
      app.exit();
    }
  }
}

module.exports = new ErrorBox();
