/* eslint-disable no-underscore-dangle */
const path = require('path');
const os = require('os');
const { app, Tray, Menu } = require('electron');
const localProject = require('../../services/localProject/localProject');
const onboardingService = require('../../services/onboarding/onboardingService');
const windowManager = require('../../windowManager/windowManager');
const TrayMenuBuilder = require('./trayMenuBuilder');
const trayMenuData = require('./trayMenuData');
const logger = require('../../logger')('TrayMenu');
const postal = require('postal');
const auth = require('../../services/localAuth/auth');

const RECENT_PROJECTS_LOOKUP_INTERVAL = 60000;

class TrayMenu {

  init() {
    logger.info('Init');
    this.createTray();
    
    const debugMode = app.argv.debug === true;
    this.trayMenuBuilder = new TrayMenuBuilder(debugMode, trayMenuData);

    if (os.platform() === 'linux') {
      // This needs to happen here on linux for the tray menu to be populated initially
      this.buildTray();

      // We need to proactively update the context menu every minute (sloppy but will do for now)
      // because we can't rely on getting the click event
      // (the context menu being shown may not produce any event at all)
      setTimeout(() => this.buildTray(), RECENT_PROJECTS_LOOKUP_INTERVAL);
      postal.subscribe({
        channel: 'app',
        topic: 'connectInfo.changed',
        callback: () => {
          this.buildTray();
        }
      });
      postal.subscribe({
        channel: 'app',
        topic: 'userInfo.changed',
        callback: () => {
          this.buildTray();
        }
      });
      postal.subscribe({
        channel: 'app',
        topic: 'license.change',
        callback: () => {
          this.buildTray();
        }
      });
    }
  }

  createTray() {
    if (app.tray) {
      return;
    }

    const trayIconImage = (os.platform() !== 'darwin') ? 'images/tray/trayIconWindows.png' : 'images/tray/trayIconTemplate.png';
    const iconPath = process.defaultApp ? path.resolve(trayIconImage) : path.join(app.getAppPath(), trayIconImage);

    app.tray = new Tray(iconPath);
    app.tray.on('click', () => {
      if (os.platform() === 'win32') {
        this.onWindowsTrayClicked();
      } else {
        this.openTray();
      }
    });
    app.tray.on('right-click', () => this.openTray());
  }

  async onWindowsTrayClicked() {
    if (windowManager.mainWindow.isVisible()) {
      windowManager.mainWindow.show();
      return;
    }

    if (windowManager.splashWindow.isVisible()) {
      windowManager.splashWindow.show();
      return;
    }

    const user = await auth.getUserInfo();
    windowManager.showProperWindow(JSON.parse(user));
  }

  async buildTray(popUpContext = false) {
    const [recentProjects, onboardingState] = await Promise.all([this._getRecentProjects(), this._getOnboardingState()]);
    
    app.trayMenu = Menu.buildFromTemplate(this.trayMenuBuilder.buildTrayMenuTemplate(recentProjects, onboardingState));
    
    if (os.platform() === 'linux') {
      app.tray.setContextMenu(app.trayMenu);
    }
    
    if (popUpContext) {
      app.tray.popUpContextMenu(app.trayMenu);
    }
  }

  openTray() {
    return this.buildTray(true);
  }
  
  async _getRecentProjects() {
    const recentProjects = await localProject.getProjectsFromMemory();
    return recentProjects.sort(this._reverseSortProjectsByLastModifiedDate);
  }

  _reverseSortProjectsByLastModifiedDate(firstProject, secondProject) {
    return secondProject.lastModified - firstProject.lastModified;
  }
  
  async _getOnboardingState() {
    if (!onboardingService.hasOnboarding) return undefined;
    return await onboardingService.getOnboardingState();
  }
}

module.exports = new TrayMenu();
