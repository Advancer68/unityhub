'use strict';
const auth = require('../../services/localAuth/auth');
const licenseClient = require('../../services/licenseService/licenseClientProxy');
const windowManager = require('../../windowManager/windowManager');
const autoUpdater = require('../../services/hubAutoUpdate/auto-updater');
const localConfig = require('../../services/localConfig/localConfig');
const TrayMenuBuilder = require('./trayMenuBuilder');

const sinon = require('sinon');
const chai = require('chai');
const expect = chai.expect;
const sinonChai = require('sinon-chai');
chai.use(sinonChai);


describe('Tray Menu', function() {
  let sandbox, recentProjects, debugMode, trayMenuData, trayMenuBuilder, result;

  beforeEach(() => {
    recentProjects = [
      { title: 'project 1' },
      { title: 'project 2' },
      { title: 'project 3' },
      { title: 'project 4' },
      { title: 'project 5' },
      { title: 'project 6' },
      { title: 'project 7' }
    ];
    
    trayMenuData = {
      menuItems: {
        GO_TO_DASHBOARD: 'dashboard',
        SIGN_OUT: 'signOut',
        SIGN_IN: 'signIn',
        ACTIVATE_LICENSE: 'activateLicense',
        OPEN_HUB: 'openHub',
        RESTART_AND_UPDATE: 'restartAndUpdate',
        QUIT: 'quit',
        RELOAD: 'reload',
        DEVTOOLS: 'devtools',
        DEVTOOLS_SIGN_IN: 'devtoolsSignIn',
        RECENT_PROJECT: 'recentProject',
        BUG_REPORTER: 'bugReporter',
        OPEN_COMMUNITY: 'openCommunity',
        TROUBLESHOOTING: 'Troubleshooting',
      },
      menuData: {}
    };
    
    Object.values(trayMenuData.menuItems).forEach((key) => {
      if (key === trayMenuData.menuItems.SIGN_OUT || key === trayMenuData.menuItems.SIGN_IN) {
        trayMenuData.menuData[key] = () => ({ label: key });
        
      } else if (key === trayMenuData.menuItems.RECENT_PROJECT) {
        trayMenuData.menuData[key] = (project) => ({ label: project.title });
        
      } else {
        trayMenuData.menuData[key] = { label: key };
      }
    });
    
    autoUpdater.updateAvailable = false;
    
    sandbox = sinon.sandbox.create();
    
    sandbox.stub(licenseClient, 'isLicenseValid').returns(true);
    sandbox.stub(licenseClient, 'isLicenseEntitlementEnabled').returns(true);
    sandbox.stub(localConfig, 'isSignInDisabled').returns(false);
  });

  afterEach(() => {
    sandbox.restore();
  });

  describe('in regular case with projects', () => {
  
    beforeEach(() => {
      debugMode = false;
      auth.connectInfo.loggedIn = false;
      auth.userInfo.displayName = 'anonymous';
      autoUpdater.updateAvailable = false;
    
      trayMenuBuilder = new TrayMenuBuilder(debugMode, trayMenuData);
      result = trayMenuBuilder.buildTrayMenuTemplate(recentProjects);
    });
    
    it('index 0 to 4 are recent projects', () => {
      for (let i = 0; i < 5; i++) {
        expect(result[i].label).to.equal(`project ${i + 1}`);
      }
    });
    it('a separator is between recent projects and the rest', () => {
      expect(result[5].type).to.equal('separator');
    });
    it('items[6 is "Sign in"  if user is not signed in', () => {
      expect(result[6].label).to.equal(trayMenuData.menuItems.SIGN_IN);
    });
    it('a separator is after account section', () => {
      expect(result[7].type).to.equal('separator');
    });
    it('items[8] is "Troubleshooting"', () => {
      expect(result[8].label).to.equal(trayMenuData.menuItems.TROUBLESHOOTING);
    });
    it('items[9] is "Open Unity Hub"=', () => {
      expect(result[9].label).to.equal(trayMenuData.menuItems.OPEN_HUB);
    });
    it('items[10] is "Quit Unity Hub"=', () => {
      expect(result[11].label).to.equal(trayMenuData.menuItems.QUIT);
    });
  });

  describe('Menu Items - debug mode and no projects', () => {
    beforeEach(() => {
      windowManager.signInWindow = {
        isVisible: sandbox.stub()
      };
      debugMode = true;
      
      trayMenuBuilder = new TrayMenuBuilder(debugMode, trayMenuData);
      result = trayMenuBuilder.buildTrayMenuTemplate([]);
    });
    
    it('should display Sign in first', () => {
      expect(result[0].label).to.equal(trayMenuData.menuItems.SIGN_IN);
    });
  
    it('should display a separator', () => {
      expect(result[1].type).to.equal('separator');
    });
    
    it('should display app menu second', () => {
      expect(result[2].label).to.equal(trayMenuData.menuItems.TROUBLESHOOTING);
      expect(result[3].label).to.equal(trayMenuData.menuItems.OPEN_HUB);
      expect(result[5].label).to.equal(trayMenuData.menuItems.QUIT);
    });
  
    it('should display a separator', () => {
      expect(result[6].type).to.equal('separator');
    });
    
    it('should display dev menu third', () => {
      expect(result[7].label).to.equal(trayMenuData.menuItems.RELOAD);
      expect(result[8].label).to.equal(trayMenuData.menuItems.DEVTOOLS);
    });
  });

  describe('Menu Items - No license', () => {
    beforeEach(() => {
      licenseClient.isLicenseValid.returns(false);
      licenseClient.isLicenseEntitlementEnabled.returns(false);
      debugMode = false;
  
      trayMenuBuilder = new TrayMenuBuilder(debugMode, trayMenuData);
      result = trayMenuBuilder.buildTrayMenuTemplate(recentProjects);
    });
  
    it('should display Sign in first', () => {
      expect(result[0].label).to.equal(trayMenuData.menuItems.SIGN_IN);
    });
  
    it('should display a separator', () => {
      expect(result[1].type).to.equal('separator');
    });
  
    it('should display app menu with "Activate License" second', () => {
      expect(result[2].label).to.equal(trayMenuData.menuItems.TROUBLESHOOTING);
      expect(result[3].label).to.equal(trayMenuData.menuItems.ACTIVATE_LICENSE);
      expect(result[5].label).to.equal(trayMenuData.menuItems.QUIT);
    });
  });

  describe('Menu Items - user is logged in', () => {
    beforeEach(() => {
      debugMode = false;
      auth.connectInfo.loggedIn = true;
      auth.userInfo.displayName = 'foo bar';
      autoUpdater.updateAvailable = false;
  
      trayMenuBuilder = new TrayMenuBuilder(debugMode, trayMenuData);
      result = trayMenuBuilder.buildTrayMenuTemplate([]);
    });
    
    it('items[0] is "Go to Dashboard"', () => {
      expect(result[0].label).to.equal(trayMenuData.menuItems.GO_TO_DASHBOARD);
    });
    
    it('items[1] is "Sign out" ', () => {
      expect(result[1].label).to.equal(trayMenuData.menuItems.SIGN_OUT);
    });
  });
  
  describe('Menu Items - sign in is disabled', () => {
    beforeEach(() => {
      localConfig.isSignInDisabled.returns(true);
      
      trayMenuBuilder = new TrayMenuBuilder(debugMode, trayMenuData);
      result = trayMenuBuilder.buildTrayMenuTemplate([]);
    });
  
    it('should display app menu', () => {
      expect(result[0].label).to.equal(trayMenuData.menuItems.TROUBLESHOOTING);
      expect(result[1].label).to.equal(trayMenuData.menuItems.OPEN_HUB);
      expect(result[3].label).to.equal(trayMenuData.menuItems.QUIT);
    });
  });
  

});
