const { app, dialog, shell } = require('electron');
const postal = require('postal');
const cloudAnalytics = require('../../services/cloudAnalytics/cloudAnalytics');
const auth = require('../../services/localAuth/auth');
const editorManager = require('../../services/editorManager/editorManager');
const licenseClient = require('../../services/licenseService/licenseClientProxy');
const autoUpdater = require('../../services/hubAutoUpdate/auto-updater');
const windowManager = require('../../windowManager/windowManager');
const hubBugReporter = require('../../services/hubBugReporter/bug-reporter');
const logger = require('../../logger')('TrayMenuData');

const menuItems = {
  GO_TO_DASHBOARD: 'dashboard',
  SIGN_OUT: 'signOut',
  SIGN_IN: 'signIn',
  ACTIVATE_LICENSE: 'activateLicense',
  OPEN_HUB: 'openHub',
  OPEN_HUB_ONBOARDING: 'openHubOnboarding',
  OPEN_COMMUNITY: 'openCommunity',
  RESTART_AND_UPDATE: 'restartAndUpdate',
  QUIT_AND_UPDATE: 'quitAndUpdate',
  QUIT: 'quit',
  RELOAD: 'reload',
  DEVTOOLS: 'devtools',
  DEVTOOLS_SIGN_IN: 'devtoolsSignIn',
  DEVTOOLS_NEW_PROJECT: 'devtoolsNewProject',
  RECENT_PROJECT: 'recentProject',
  BUG_REPORTER: 'bugReporter',
  TROUBLESHOOTING: 'Troubleshooting',
};

const menuData = {
  [menuItems.GO_TO_DASHBOARD]: {
    label: { code: 'TRAY.GO_TO_DASHBOARD' },
    id: 'dashboard',
    click() {
      windowManager.openExternal('https://developer.cloud.unity3d.com', true);
    }
  },
  [menuItems.SIGN_OUT]: () => ({
    label: { code: 'TRAY.SIGN_OUT', params: { name: auth.userInfo.displayName } },
    id: 'signOut',
    enabled: auth.connectInfo.ready,
    click() {
      auth.logout();
    }
  }),
  [menuItems.SIGN_IN]: {
    label: { code: 'TRAY.SIGN_IN' },
    id: 'signIn',
    click() {
      if (licenseClient.isLicenseValid() === false) {
        windowManager.showSignInWindow(windowManager.signInWindow.pages.SIGN_IN_LICENSE);
      } else {
        windowManager.showSignInWindow();
      }
      postal.publish({
        channel: 'app',
        topic: 'main-window.change',
        data: {
          state: 'open',
          show: true,
          from: 'windowController:show',
          url: '#/login'
        }
      });
    },
  },
  [menuItems.ACTIVATE_LICENSE]: {
    label: { code: 'TRAY.ACTIVATE_LICENSE' },
    click() {
      windowManager.mainWindow.show(windowManager.mainWindow.pages.LICENSE_MANAGEMENT);
    },
  },
  [menuItems.OPEN_HUB]: {
    label: { code: 'TRAY.OPEN_HUB' },
    click() {
      windowManager.mainWindow.setTitle();
      windowManager.mainWindow.show();
    },
  },
  [menuItems.OPEN_HUB_ONBOARDING]: {
    label: { code: 'TRAY.OPEN_HUB' },
    click() {
      windowManager.onboardingWindow.show();
    },
  },
  [menuItems.OPEN_COMMUNITY]: {
    label: { code: 'TRAY.OPEN_COMMUNITY' },
    click() {
      windowManager.mainWindow.setTitle();
      windowManager.mainWindow.show('community');
    },
  },
  [menuItems.RESTART_AND_UPDATE]: {
    label: { code: 'TRAY.RESTART_AND_UPDATE' },
    click() {
      autoUpdater.restart();
    }
  },
  [menuItems.QUIT_AND_UPDATE]: {
    label: { code: 'TRAY.QUIT_AND_UPDATE' },
    click() {
      autoUpdater.quitWithUpdate();
    }
  },
  [menuItems.QUIT]: {
    label: { code: 'TRAY.QUIT_HUB' },
    click() {
      cloudAnalytics.quitEvent('TrayMenu').then(() => {
        app.quit();
      }).catch(() => {
        app.quit();
      });
    },
  },
  [menuItems.RELOAD]: {
    label: { code: 'TRAY.RELOAD' },
    click() {
      windowManager.mainWindow.reload();
    },
  },
  [menuItems.DEVTOOLS]: {
    label: { code: 'TRAY.OPEN_DEVTOOLS' },
    click() {
      windowManager.mainWindow.openDevTools();
    },
  },
  [menuItems.DEVTOOLS_SIGN_IN]: {
    label: { code: 'TRAY.OPEN_SIGNIN_DEVTOOLS' },
    click() {
      windowManager.signInWindow.openDevTools();
    }
  },
  [menuItems.DEVTOOLS_NEW_PROJECT]: {
    label: { code: 'TRAY.OPEN_NEW_PROJECT_DEVTOOLS' },
    click() {
      windowManager.newProjectWindow.openDevTools();
    }
  },
  [menuItems.RECENT_PROJECT]: (theProject) => {
    const click = (project) => {
      const isEditorAvailable = Object.keys(editorManager.availableEditors).length > 0;
      if (!isEditorAvailable) {
        handleNoEditor();
      } else if (!editorManager.availableEditors[project.version]) {
        handleMissingEditor(project);
      } else {
        openProject(project.path, project.version);
      }
    };
    
    return {
      label: theProject.title,
      click: click.bind(this, theProject)
    };
  },
  [menuItems.TROUBLESHOOTING]: {
    label: { code: 'TRAY.TROUBLESHOOTING' },
    submenu: [
      {
        label: { code: 'TRAY.RELEASE_NOTES' },
        click() {
          windowManager.openExternal('https://unity.cn/hub/whats-new');
        }
      },
      {
        label: { code: 'TRAY.HUB_DOCUMENTATION' },
        click() {
          windowManager.openExternal('https://docs.unity.cn/cn/2019.4/Manual/GettingStartedUnityHub.html');
        }
      },
      { label: { code: 'TRAY.OPEN_LOG_DIRECTORY' },
        click() {
          const logPath = logger.getLogDirectoryPath();
          shell.openItem(logPath);
        }
      },
      { label: { code: 'TRAY.REPORT_A_BUG' },
        click() {
          hubBugReporter.showBugReporter();
        }
      }
    ],
  },
};

function handleNoEditor() {
  dialog.showMessageBox({
    type: 'warning',
    title: 'Unity not found',
    message: 'The Hub could not locate any version of Unity on your machine. Please ' +
    'set a version of Unity in the installs tab',
    buttons: ['Go to Installs']
  }, (buttonIndex) => {
    if (buttonIndex === 0) {
      windowManager.mainWindow.show(windowManager.mainWindow.pages.INSTALLS);
    }
  });
}

function openProject(projectPath, editorVersion) {
  return postal.publish({
    channel: 'app',
    topic: 'project.open',
    data: {
      projectPath,
      editorVersion
    }
  });
}

function handleMissingEditor(project) {
  editorManager.getDefaultEditor().then((latest) => {
    dialog.showMessageBox({
      type: 'warning',
      title: 'Version not found',
      message: `This project has been previously saved with ${project.version}, this version of Unity is not installed within the Hub at the moment. ` +
      'You can open with the latest version on your machine.',
      buttons: [`Open with ${latest.version}`, 'Cancel']
    }, (buttonIndex) => {
      if (buttonIndex === 0) {
        openProject(project.path, latest.version);
      }
    });
  });
}

module.exports = {
  menuItems,
  menuData
};
