const EventEmitter = require('events');
const { MT_FILE_DL_STATUS } = require('../services/localDownload/lib/constants');

class Download extends EventEmitter {
  constructor(url, filePath, downloadManager) {
    super();
    this.url = url;
    this.filePath = filePath;
    this.status = MT_FILE_DL_STATUS.NOT_STARTED;
    this.error = '';
    this.stats = {
      total: { size: 0, downloaded: 0, completed: 0 }
    };
    this.request = null;
    this.downloadManager = downloadManager;
  }

  start() {
    this.downloadManager.startDownload(this);
    this.status = MT_FILE_DL_STATUS.PENDING;
    this.emit('start', this);
    return this;
  }

  stop() {
    this.downloadManager.stopDownload(this);
    this.status = MT_FILE_DL_STATUS.STOPPED;
    this.emit('stop', this);
  }

  destroy() {
    this.downloadManager.stopDownload(this);
    this.downloadManager.removeDownload(this);
    this.status = MT_FILE_DL_STATUS.DESTROYED;
    this.emit('destroy', this);
  }

  ended() {
    this.status = MT_FILE_DL_STATUS.FINISHED;
    this.emit('end', this);
  }

  isCancelled() {
    if (this.status === MT_FILE_DL_STATUS.STOPPED || this.status === MT_FILE_DL_STATUS.DESTROYED) {
      return true;
    }
    return false;
  }

  errored(e) {
    this.downloadManager.stopDownload(this);
    this.status = MT_FILE_DL_STATUS.ERROR;
    this.error = e;
    this.emit('error', this, e);
  }

  resume() {
    this.downloadManager.resumeDownload(this);
    this.status = MT_FILE_DL_STATUS.PENDING;
    this.emit('resume', this);
  }

  getStats() {
    return this.stats;
  }

  setStats(stats) {
    this.stats = stats;
  }
}

// private

module.exports = Download;
