// internal
const downloadManager = require('../downloadManager');
const Download = require('../download');
const DownloadRequest = require('../downloadRequest');
const { MT_FILE_DL_STATUS } = require('../../services/localDownload/lib/constants');
const { fs } = require('../../fileSystem');

// tests
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
  .use(require('sinon-chai'))
  .use(require('chai-as-promised'));


describe('Download Manager', () => {
  let sandbox;
  
  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    //clean up every time
    downloadManager.downloadsPending = [];
    downloadManager.downloadsProcessing = [];
  });
  
  afterEach(() => {
    sandbox.restore();
  });
  
  describe('download', () => {
    it('should return a download object', () => {
      const dl = downloadManager.download('url', 'filePath');
      expect(typeof dl).to.equal('object');
      expect(dl.url).to.equal('url');
      expect(dl.filePath).to.equal('filePath');
    });
  });

  describe('startDownload', () => {
    it('should add the download to the pending list and call processPendingDownloads', () => {
      let processPendingDownloads = sandbox.stub(downloadManager, 'processPendingDownloads');

      const dl = new Download('url', 'filePath', downloadManager);
      downloadManager.startDownload(dl);
      expect(downloadManager.downloadsPending).to.be.an('array').that.includes(dl);
      expect(processPendingDownloads).to.have.been.calledOnce;
    });
  });

  describe('stopDownload', () => {
    describe('should abort an ongoing download', () => {
      it('when download has a request', () => {
        const dl = new Download('url', 'filePath', downloadManager);
        dl.request = {abort: sandbox.stub()};
        sandbox.spy(dl.request.abort);
        downloadManager.stopDownload(dl);
        expect(dl.request.abort).to.have.been.calledOnce;
      });
    });
  });

  describe('removeDownload', () => {
    describe('should delete the file on disk, if any', () => {
      it('when file exists', () => {
        sandbox.stub(fs, 'existsSync').returns(true);
        let unlink = sandbox.stub(fs, 'unlink');

        const dl = new Download('url', 'filePath', downloadManager);
        downloadManager.removeDownload(dl);
        expect(unlink).to.have.been.calledOnce;
      });

      it('when file does not exist', () => {
        sandbox.stub(fs, 'existsSync').returns(false);
        let unlink = sandbox.stub(fs, 'unlink');
        const dl = new Download('url', 'filePath', downloadManager);
        downloadManager.removeDownload(dl);
        expect(unlink).to.not.have.been.called;
      });
    });
  });

  describe('processPendingDownloads', () => {
    describe('should start processing the pending downloads', () => {

      it('when the number asked is below the maxConcurrentDownload', () => {
        const downloadFile = sandbox.stub(DownloadRequest, 'downloadFile').resolves();

        const dl = new Download('url', 'filePath', downloadManager);
        downloadManager.downloadsPending.push(dl);
        downloadManager.processPendingDownloads();
        expect(downloadFile).to.have.been.calledOnce;
      });

      it('when the number asked is over the maxConcurrentDownload', (done) => {
        const downloads = [];
        let processedDownloadsCount = 0;
        //prepare
        const downloadCount = downloadManager.maxConcurrentDownloads + 2;
        for (let i = 0; i < downloadCount; i++) {
          const dl = sinon.createStubInstance(Download);
          downloads.push(dl);
          downloadManager.downloadsPending.push(dl);
        }

        sandbox.stub(DownloadRequest, 'downloadFile').resolves();

        for(let i = 0; i < downloads.length; i++) {
          downloads[i].ended.callsFake(() => {
            try{
              expect(DownloadRequest.downloadFile).to.have.been.calledWith(downloads[i]);
            } catch(e) {
              done(e);
            }

            processedDownloadsCount++;
            if (processedDownloadsCount >= downloadCount) {
              done();
            }
          });
        }

        downloadManager.processPendingDownloads();
      });

      it('when some downloads are cancelled, they should not be downloaded', () => {
        const downloadFile = sandbox.stub(DownloadRequest, 'downloadFile').resolves();

        for (let i = 0; i < 5; i++) {
          const dl = new Download(`url${i}`, `filePath${i}`, downloadManager);
          if (i%2 === 0) {
            dl.status = MT_FILE_DL_STATUS.STOPPED;
          }
          downloadManager.downloadsPending.push(dl);
        }
        expect(downloadManager.downloadsPending.length).to.equal(5);
        downloadManager.processPendingDownloads();
        expect(downloadFile).to.have.been.callCount(2);
      });

      it('when all the download fails (2)', (done) => {
        const downloads = [];
        let processedDownloadsCount = 0;
        //prepare
        const downloadCount = 2;
        for (let i = 0; i < downloadCount; i++) {
          const dl = sinon.createStubInstance(Download);
          downloads.push(dl);
          downloadManager.downloadsPending.push(dl);
        }

        sandbox.stub(DownloadRequest, 'downloadFile').rejects();

        for(let i = 0; i < downloads.length; i++) {
          downloads[i].errored.callsFake(() => {
            try {
              expect(DownloadRequest.downloadFile).to.have.been.calledWith(downloads[i]);
            } catch(e) {
              done(e);
            }

            processedDownloadsCount++;
            if (processedDownloadsCount >= downloadCount) {
              done();
            }
          });
        }

        downloadManager.processPendingDownloads();
      });

      it('when the number asked is over the maxConcurrentDownload and all download fails', (done) => {
        const downloads = [];
        let processedDownloadsCount = 0;
        //prepare
        const downloadCount = downloadManager.maxConcurrentDownloads + 2;
        for (let i = 0; i < downloadCount; i++) {
          const dl = sinon.createStubInstance(Download);
          downloads.push(dl);
          downloadManager.downloadsPending.push(dl);
        }

        sandbox.stub(DownloadRequest, 'downloadFile').rejects();

        for(let i = 0; i < downloads.length; i++) {
          downloads[i].errored.callsFake(() => {
            try {
              expect(DownloadRequest.downloadFile).to.have.been.calledWith(downloads[i]);
            } catch(e) {
              done(e);
            }

            processedDownloadsCount++;
            if (processedDownloadsCount >= downloadCount) {
              done();
            }
          });
        }

        downloadManager.processPendingDownloads();
      });
    });
  });
});
