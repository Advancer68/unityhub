// internal
const Download = require('../download');
let DownloadRequest = require('../downloadRequest');
const { MT_FILE_DL_STATUS } = require('../../services/localDownload/lib/constants');
const settings = require('../../services/localSettings/localSettings');
const { fs } = require('../../fileSystem');

//third parties
const proxyquire = require('proxyquire');
const request = require('request');

//tests
const expect = require('chai').expect;
const sinon = require('sinon');
require('chai')
  .use(require('sinon-chai'))
  .use(require('chai-as-promised'));


describe('DownloadRequest', () => {
  let sandbox;
  
  beforeEach(() => {
    sandbox = sinon.sandbox.create();
    sandbox.stub(settings, 'get').returns(15000);
  });
  
  afterEach(() => {
    sandbox.restore();
  });
  
  describe('downloadFile', () => {
    it('when the download is cancelled, it should resolve directly', () => {
      const dl = new Download('url', 'filePath');
      dl.status = MT_FILE_DL_STATUS.DESTROYED;
      return expect(DownloadRequest.downloadFile(dl)).to.eventually.be.resolved;
    });


    describe('downloadFile events', () => {
      let getCallbacks, progressCallbacks;
      let getStub, progressStub;

      beforeEach(() => {
        getCallbacks = {};
        progressCallbacks = {};
        getStub = {
          on: function (event, callback) {
            getCallbacks[event] = callback;
            return this;
          }
        };
        progressStub = {
          on: function (event, callback) {
            progressCallbacks[event] = callback;
            return this;
          }
        };
        DownloadRequest = proxyquire('../downloadRequest', {'request-progress': sandbox.stub().returns(progressStub)});
        sandbox.stub(request, 'get').returns(getStub);
      });

      describe('when request is successful (200)', () => {
        let createWriteStreamStub;
        let fsCallbacks;

        beforeEach(() => {
          fsCallbacks = {};
          createWriteStreamStub = {
            on: function (event, callback) {
              fsCallbacks[event] = callback;
              return this;
            }
          };
          sandbox.stub(fs, 'createWriteStream').returns(createWriteStreamStub);
        });

        it('when progress is reported and finished writing the file, should set the stats and resolve', () => {
          const dl = new Download('https://download.unity3d.com/download_unity/52d9cb89b362/Windows64EditorInstaller/UnitySetup64-2017.4.2f2.exe', 'filePath');
          const promise = DownloadRequest.downloadFile(dl)
            .then(() => {
              expect(dl.status).to.equal(MT_FILE_DL_STATUS.DOWNLOADING);
              expect(dl.request).to.equal(getStub);
              expect(dl.getStats()).to.eql({
                total: {
                  size: 10,
                  downloaded: 1,
                  completed: "10.00",
                  time: {
                    elapsed: 20,
                    remaining: 81
                  }
                }
              })
            });

          getCallbacks.response({
            statusCode: 200,
            pipe: sinon.spy(),
            on: function (event, callback) {
              progressCallbacks[event] = callback;
              return this;
            }
          });

          progressCallbacks.progress({
            size: {
              total: 10,
              transferred: 1
            },
            percent: 0.1,
            time: {
              elapsed: 20,
              remaining: 81
            }
          });

          fsCallbacks.close();

          return promise;
        });

        it('when error while writing the file, should reject', () => {
          const dl = new Download('https://download.unity3d.com/download_unity/52d9cb89b362/Windows64EditorInstaller/UnitySetup64-2017.4.2f2.exe', 'filePath');
          const promise = DownloadRequest.downloadFile(dl)
            .catch((e) => {
              expect(dl.status).to.equal(MT_FILE_DL_STATUS.DOWNLOADING);
              expect(dl.request).to.equal(getStub);
              expect(e.message).to.equal('write error');
            });

          getCallbacks.response({
            statusCode: 200,
            pipe: sinon.spy()
          });

          fsCallbacks.error(new TypeError('write error'));

          return promise;
        });
      });

      describe('when request is empty (204)', () => {
        it('should reject', () => {
          const dl = new Download('https://download.unity3d.com/download_unity/52d9cb89b362/Windows64EditorInstaller/UnitySetup64-2017.4.2f2.exe', 'filePath');
          const promise = DownloadRequest.downloadFile(dl)
            .catch((e) => {
              expect(dl.status).to.equal(MT_FILE_DL_STATUS.DOWNLOADING);
              expect(dl.request).to.equal(getStub);
              expect(e).to.include('Download file was empty');
            });

          getCallbacks.response({
            statusCode: 204,
            pipe: sinon.spy()
          });

          return promise;
        });
      });

      describe('when request is a server-side error (500)', () => {
        it('should reject', () => {
          const dl = new Download('https://download.unity3d.com/download_unity/52d9cb89b362/Windows64EditorInstaller/UnitySetup64-2017.4.2f2.exe', 'filePath');
          const promise = DownloadRequest.downloadFile(dl)
            .catch((e) => {
              expect(dl.status).to.equal(MT_FILE_DL_STATUS.DOWNLOADING);
              expect(dl.request).to.equal(getStub);
              expect(e).to.include('Download file could not be found and returned code');
            });

          getCallbacks.response({
            statusCode: 500,
            pipe: sinon.spy()
          });

          return promise;
        });
      });

      describe('when request is not found (404)', () => {
        it('should reject', () => {
          const dl = new Download('https://download.unity3d.com/download_unity/52d9cb89b362/Windows64EditorInstaller/UnitySetup64-2017.4.2f2.exe', 'filePath');
          const promise = DownloadRequest.downloadFile(dl)
            .catch((e) => {
              expect(dl.status).to.equal(MT_FILE_DL_STATUS.DOWNLOADING);
              expect(dl.request).to.equal(getStub);
              expect(e).to.include('Download file could not be found and returned code');
            });

          getCallbacks.response({
            statusCode: 404,
            pipe: sinon.spy()
          });

          return promise;
        });
      });

      it('when request is aborted, should resolve', () => {
        const dl = new Download('https://download.unity3d.com/download_unity/52d9cb89b362/Windows64EditorInstaller/UnitySetup64-2017.4.2f2.exe', 'filePath');
        const promise = DownloadRequest.downloadFile(dl)
          .then(() => {
            expect(dl.status).to.equal(MT_FILE_DL_STATUS.DOWNLOADING);
            expect(dl.request).to.equal(getStub);
          });

        getCallbacks.abort();

        return promise;
      });

      it('when request has error, should reject', () => {
        const dl = new Download('https://download.unity3d.com/download_unity/52d9cb89b362/Windows64EditorInstaller/UnitySetup64-2017.4.2f2.exe', 'filePath');
        const promise = DownloadRequest.downloadFile(dl)
          .catch(() => {
            expect(dl.status).to.equal(MT_FILE_DL_STATUS.DOWNLOADING);
            expect(dl.request).to.equal(getStub);
          });

        getCallbacks.error(new TypeError('download error'));

        return promise;
      });
    });
  });
});
