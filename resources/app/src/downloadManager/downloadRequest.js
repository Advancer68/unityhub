const request = require('request');
const progress = require('request-progress');
const { MT_FILE_DL_STATUS } = require('../services/localDownload/lib/constants');
const logger = require('../logger')('DownloadRequest');
const settings = require('../services/localSettings/localSettings');
const { fs } = require('../fileSystem');

const downloadRequest = {
  fakeUserAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36',
  /**
   * We fake a chrome user agent because some links require
   * a user agent of a valid browser set to actually work
   * (facebook SDK)
   *
   * @param download An object from the class Download.
   * @return {Promise}
   */
  downloadFile(download) {
    return new Promise((resolve, reject) => {
      if (!download || download.isCancelled()) {
        resolve();
        return;
      }

      logger.info(`Starting the download of ${download.url} to ${download.filePath}`);
      const req = request.get({
        url: download.url,
        timeout: settings.get(settings.keys.DOWNLOAD_REQUEST_TIMEOUT),
        headers: {
          'User-Agent': this.fakeUserAgent
        }
      })
        .on('response', (response) => onResponse(download, response, resolve, reject))
        .on('abort', () => {
          logger.debug('The download was cancelled');
          resolve();
        })
        .on('error', (e) => {
          logger.warn(`Something went wrong while downloading ${download.url} ${e} ${e.stack}`);
          reject(e);
        });
      progress(req, {})
        .on('progress', (state) => {
          download.setStats({
            total: {
              size: state.size.total,
              downloaded: state.size.transferred,
              completed: (state.percent * 100).toFixed(2),
              time: state.time
            }
          });
        })
        .on('error', (e) => {
          logger.warn(`Something went wrong while checking progress of ${download.url} ${e} ${e.stack}`);
        });
      download.request = req;
      download.status = MT_FILE_DL_STATUS.DOWNLOADING;
    });
  }
};

function onResponse(download, response, resolve, reject) {
  // TODO: What about code 301?
  if (response.statusCode === 200) {
    response.pipe(
      fs.createWriteStream(download.filePath)
        .on('close', () => {
          resolve();
        })
        .on('error', (e) => {
          logger.warn(`Something went wrong while writing to the file of ${download.url} ${e} ${e.stack}`);
          reject(e);
        })
    );
  } else if (response.statusCode === 204) {
    reject(`Download file was empty (${download.url})`);
  } else {
    reject(`Download file could not be found and returned code: ${response.statusCode} (${download.url})`);
  }
}

module.exports = downloadRequest;
