'use strict';

const path = require('path');
const os = require('os');
const EventEmitter = require('events');
const { BrowserWindow, dialog, shell } = require('electron');
const postal = require('postal');
const _ = require('lodash');
const windowStateKeeper = require('electron-window-state');
const proxyHelper = require('../proxyHelper');

const logger = require('../logger')('BaseWindow');
const cloudAnalytics = require('../services/cloudAnalytics/cloudAnalytics');
const networkInterceptors = require('../services/localAuth/networkInterceptors');

/**
 * Utility base class for instantiating and manipulating BrowserWindow objects.
 */
class BaseWindow extends EventEmitter {

  get name() { return 'base-window'; }

  get pages() { return {}; }

  constructor(rootPath, options, parentWindow) {
    super();

    this.browserWindow = null;
    this.initOptions = options;
    this.rootPath = rootPath;
    this.parentWindow = parentWindow;
    this.basePage = '';
    this.webContentsEventHandlers = {};

    this.registerWebContentsEventHandler('will-navigate', (event, url) => {
      // Prevent navigation to drag & dropped files.
      // see https://github.com/electron/electron/issues/908
      if (url.startsWith('file://')) {
        event.preventDefault();
      }
    });

    // Open external links in browser.
    this.registerWebContentsEventHandler('new-window', (event, url) => {
      event.preventDefault();

      shell.openExternal(url, (error) => {
        logger.warn(`Failed to open ${url} in default browser.`, error);

        // Falling back to new BrowserWindow.
        // https://github.com/electron/electron/blob/master/docs/api/web-contents.md#event-new-window
        const newWindow = new BrowserWindow({ width: 1028 });
        newWindow.loadURL(url);
        event.newGuest = newWindow;
      });
    });

    // Handle failed page loads
    this.registerWebContentsEventHandler('did-fail-load', (event, code, message, validatedUrl) => {
      logger.warn(`Failed to load page ${validatedUrl}. Error: ${code} - ${message}`);
      // see here for error codes: https://cs.chromium.org/chromium/src/net/base/net_error_list.h
      if (code === -106) {
        networkInterceptors.failedRequest();
      }
    });
  }

  /**
   * Opens the browserWindow along with it's devtools window.
   */
  openDevTools() {
    this.show();
    this.browserWindow.webContents.openDevTools({ detach: true });
  }

  /**
   * Reloads the current browserWindow and skips cache.
   */
  reload() {
    if (this.browserWindow !== null) {
      this.browserWindow.webContents.reloadIgnoringCache();
    }
  }

  /**
   * Displays the BrowserWindow and bind to event changes if it was not created before.
   *
   * @param {String} page The page to navigate to once the window is shown. Default is the basePage property.
   * @param {String} queryParameters parameters to pass along with the page.
   */
  async show(page, queryParameters = '') {
    // Display the browser window.
    if (this.browserWindow !== null) {
      this.browserWindow.show();
      this.browserWindow.focus();

      if (page) {
        this.loadPage(page, queryParameters);
      }

      return;
    }

    const options = Object.assign({}, this.initOptions);

    if (this.title !== undefined) {
      options.title = this.title;
    }

    if (this.parentWindow && this.parentWindow.browserWindow && this.parentWindow.browserWindow.isVisible()) {
      // Center with parent.
      const parentInfo = getParentSizeAndPosition(this.parentWindow.browserWindow);
      options.x = parentInfo.x + ((parentInfo.width - options.width) / 2);
      options.y = parentInfo.y + ((parentInfo.height - options.height) / 2);

      options.center = false;
    }

    const windowState = windowStateKeeper({
      defaultWidth: options.width,
      defaultHeight: options.height,
      file: `${this.name}.json`
    });
    options.minWidth = options.minWidth || 0;
    options.minHeight = options.minHeight || 0;
    options.x = windowState.x || options.x;
    options.y = windowState.y || options.y;
    options.width = windowState.width > options.minWidth ? windowState.width : options.minWidth;
    options.height = windowState.height > options.minHeight ? windowState.height : options.minHeight;

    // This is a hack to add the icon for linux, please read: https://github.com/electron-userland/electron-builder/issues/2269#issuecomment-342168989
    if (os.platform() === 'linux') {
      options.icon = path.join(__dirname, '../../images/linux-icon/icon.png');
    }

    // Create the browser window.
    cloudAnalytics.windowEvent('Open', this.name);
    this.browserWindow = new BrowserWindow(options);
    windowState.manage(this.browserWindow);

    await this.setupProxy();

    if (page) {
      this.loadPage(page, queryParameters);
    } else {
      // load the homepage
      this.loadPage(this.basePage, queryParameters);
    }

    // prevents the page window title set in the main process to be overridden when loading html page from the client
    this.browserWindow.on('page-title-updated', (e) => {
      e.preventDefault();
    });

    this.browserWindow.on('closed', () => {
      postal.publish({
        channel: 'app',
        topic: `${this.name}.change`,
        data: {
          state: 'all-closed',
          from: 'browserWindow:closed'
        }
      });

      this.browserWindow = null;
      cloudAnalytics.windowEvent('Close', this.name);
    });

    // Handle webContents events for BrowserWindow.
    _.each(this.webContentsEventHandlers, (handlers, eventName) => {
      this.browserWindow.webContents.on(eventName, (...args) => {
        handlers.forEach((handler) => {
          handler(...args);
        });
      });
    });
  }

  setupProxy() {
    return new Promise((resolve, reject) => {
      let proxyRules = '';

      if (proxyHelper.http) {
        proxyRules += `http=${proxyHelper.http.stringWithoutAuth};`;
      }

      if (proxyHelper.https) {
        proxyRules += `https=${proxyHelper.https.stringWithoutAuth}`;
      }

      if (proxyRules === '') {
        return resolve();
      }

      return this.browserWindow.webContents.session.setProxy({ proxyRules }, (error) => {
        if (error) {
          return reject(error);
        }

        return resolve();
      });
    });
  }

  /**
   * Shows the "Open File" dialog box as a child window of the BrowserWindow.
   * @param {Object} options The Electron.dialog options.
   * @returns {Promise} A promise that resolves when
   */
  showOpenFileDialog(options) {
    if (!this.browserWindow) {
      return Promise.reject();
    }

    // Throw if both fileType options are selected. This isn't supported on Windows and will produce unexpected results.
    // https://electronjs.org/docs/api/dialog#dialogshowopendialogbrowserwindow-options-callback
    if (options.properties.includes('openDirectory') && options.properties.includes('openFile')) {
      throw new Error('Cannot select both a file and a directory');
    }

    let fileType;
    if (options.properties.includes('openDirectory')) {
      fileType = 'directory';
    } else if (options.properties.includes('openFile')) {
      fileType = 'file';
    } else {
      throw new Error('No valid file type selection for dialog box');
    }

    return new Promise((resolve, reject) => {
      dialog.showOpenDialog(this.browserWindow, options, (filenames) => {
        if (filenames) {
          const result = options.properties.includes('multiSelections') ? filenames : filenames[0];
          resolve(result);
        } else {
          reject(`No ${fileType} selected`);
        }
      });
    });
  }

  /**
   * Hides BrowserWindow.
   */
  hide() {
    if (!this.browserWindow) {
      return;
    }
    this.browserWindow.hide();
    postal.publish({
      channel: 'app',
      topic: `${this.name}.change`,
      data: {
        state: 'all-closed',
        from: 'browserWindow:hide'
      }
    });
  }

  /**
   * Closes BrowserWindow.
   */
  close() {
    if (this.browserWindow !== null) {
      this.browserWindow.close();
    }
  }

  /**
   * Whether the current browserWindow is in focus.
   * @returns {boolean}
   */
  isFocused() {
    return this.browserWindow !== null && this.browserWindow.isFocused();
  }

  /**
   * Whether the current browserWindow is visible.
   * @returns {boolean}
   */
  isVisible() {
    return this.browserWindow !== null && this.browserWindow.isVisible();
  }

  /**
   * Loads a URL fragment if page is among page list.
   * @param {String} page A page from the window's list of pages.
   * @param queryParameters
   */
  loadPage(page, queryParameters = '') {
    if (!_.values(this.pages).includes(page)) {
      logger.warn(`Page ${page} is not among supported pages for window ${this.name}`);
    } else {
      this.loadFragmentURL(page, queryParameters);
    }
  }

  /**
   * Loads absolute URL to BrowserWindow with optional query parameters.
   * @param {String} url A valid url to load in the BrowserWindow.
   * @param {String} [queryParameters] The query parameters in string form without the leading question mark ('..index.html?a=1..').
   */
  loadURL(url, queryParameters = '', options) {
    this.browserWindow.loadURL(appendQueryParameters(url, queryParameters), options);
  }

  /**
   * Loads fragment with root URL to BrowserWindow with optional query parameters.
   * @param {String} fragment A URL fragment to append to the root URL with or without pound sign (#).
   * @param {String} [queryParameters] The query parameters in string form without the leading question mark ('..index.html?a=1..').
   */
  loadFragmentURL(fragment, queryParameters = '') {
    let newFragment = fragment;
    if (!newFragment.startsWith('#!')) {
      if (newFragment.startsWith('#/')) {
        newFragment = newFragment.substring(1);
      }
      newFragment = `#!${newFragment}`;
    }
    let newUrl = this.rootPath + newFragment;
    newUrl = appendQueryParameters(newUrl, queryParameters);

    if (this.browserWindow.webContents.getURL() !== newUrl) {
      this.browserWindow.loadURL(newUrl);
    }
  }

  /**
   * Sends IPC to BrowserWindow if it was created.
   * @param {String} eventName The event to which the IPC will be attached.
   * @param {*} data The data to pass along with the event.
   */
  sendContent(eventName, ...data) {
    if (this.browserWindow && this.browserWindow.webContents) {
      this.browserWindow.webContents.send(eventName, ...data);
    }
  }

  /**
   * Gets the native window handle linked to the BrowserWindow.
   * @returns {Buffer} The platform-specific handle of the window.
   */
  getNativeWindowHandle() {
    if (this.browserWindow === null) return null;

    return this.browserWindow.getNativeWindowHandle();
  }

  /**
   * Binds an event handler to a browser webContents event.
   * @param {String} eventName The name of the webContents event to bind to.
   * @param {Function} handler The handler to call upon event.
   */
  registerWebContentsEventHandler(eventName, handler) {
    if (!this.webContentsEventHandlers[eventName]) {
      this.webContentsEventHandlers[eventName] = [];
    }

    this.webContentsEventHandlers[eventName].push(handler);
  }

  setTitle(title) {
    this.title = title;
    if (this.browserWindow === null) return;
    this.browserWindow.setTitle(title);
  }
}

function getParentSizeAndPosition(parentBrowserWindow) {
  const [x, y] = parentBrowserWindow.getPosition();
  const [width, height] = parentBrowserWindow.getSize();
  return { x, y, width, height };
}

function appendQueryParameters(url, parameters) {
  let newUrl = url;

  if (parameters) {
    newUrl += `?${parameters}`;
  }

  return newUrl;
}

module.exports = BaseWindow;
