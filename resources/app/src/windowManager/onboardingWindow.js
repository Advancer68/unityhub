'use strict';

const BaseWindow = require('./baseWindow');
const path = require('path');

const rootPath = `file://${path.resolve(__dirname, '../../client-onboarding/public/index.html')}`;

class OnboardingWindow extends BaseWindow {

  get name() { return 'onboarding-window'; }

  get pages() {
    return {
      INSTALL: '/install',
      ACCOUNT: '/account',
      LEARN: '/learn',
      GAME_TEMPLATES: '/gameTemplates',
    };
  }

  constructor(optionsOverride, parentWindow) {
    const options = Object.assign({
      width: 1024,
      height: 672,
      minWidth: 1024,
      minHeight: 672,
      maxWidth: 1400,
      maxHeight: 800,
      center: true,
      resizable: true,
      minimizable: true,
      maximizable: false,
      fullscreen: false,
      fullscreenable: false,
      title: 'Unity Hub',
      webPreferences: {
        textAreasAreResizable: false,
        preload: path.join(__dirname, 'preloads', 'mainWindowPreload.js'),
        nodeIntegration: true,
        webviewTag: true
      },
    }, optionsOverride);
    super(rootPath, options, parentWindow);

    this.basePage = this.pages.INSTALL;
  }
}

module.exports = OnboardingWindow;
