'use strict';
const _ = require('lodash');
const sinon = require('sinon');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const sinonChai = require('sinon-chai');
const proxyquire =  require('proxyquire').noPreserveCache();
const cloudAnalytics = require('../services/cloudAnalytics/cloudAnalytics');
const networkInterceptors = require('../services/localAuth/networkInterceptors');
const proxyHelper = require('../proxyHelper');


const expect = chai.expect;
chai.use(chaiAsPromised);
chai.use(sinonChai);


const postal = require('postal');
const { BrowserWindow, dialog } = require('electron');
let BaseWindow;

describe('BaseWindow', function () {
  let sandbox;
  // Data
  let rootPath, options, parentWindow, baseWindow;
  // Mocks and Helpers
  let browserWindowInstance, webContentsEventHandlers;
  // Stubs
  let windowStateStub;

  beforeEach(() => {
    // Stubbing BrowserWindow require
    browserWindowInstance = sinon.createStubInstance(BrowserWindow);

    BaseWindow = proxyquire('./baseWindow', {
      'electron': {
        BrowserWindow: function(options) {
          browserWindowInstance.options = options;
          return browserWindowInstance;
        },

      },
      'electron-window-state': sinon.stub().callsFake(() => {
        return windowStateStub;
      })
    });

    
    // Stubbing parentWindow
    parentWindow = sinon.createStubInstance(BaseWindow);

    // Mock variables.
    rootPath = 'infamous://bob.the/great';
    options = { foo: 'bar' };
    webContentsEventHandlers = {};



    sandbox = sinon.sandbox.create();
    // Spies
    windowStateStub = {
      manage: sandbox.stub(),
    };

    sandbox.stub(BaseWindow.prototype, 'registerWebContentsEventHandler')
      .callsFake((eventName, callback) => webContentsEventHandlers[eventName] = callback);
    sandbox.stub(cloudAnalytics, 'addEvent').resolves();

    baseWindow = new BaseWindow(rootPath, options, parentWindow);
  });

  afterEach(() => {
    sandbox.restore();
  });
  
  describe('constructor', () => {
    it('should set browserWindow to null', () => {
      expect(baseWindow.browserWindow).to.be.null;
    });
  
    it('should set options to passed options', () => {
      expect(baseWindow.initOptions).to.eql(options);
    });
  
    it('should set rootPath to passed rootPath', () => {
      expect(baseWindow.rootPath).to.equal(rootPath);
    });
    
    it('should set parentWindow to passed parentWindow', () => {
      expect(baseWindow.parentWindow).to.equal(parentWindow);
    });
  
    it('should set basePage to empty string', () => {
      expect(baseWindow.basePage).to.equal('');
    });
  
    it('should set webContentsEventHandlers to empty object', () => {
      expect(baseWindow.webContentsEventHandlers).to.eql({});
    });
    
    it('should register a callback on the "will-navigate" event', () => {
      expect(baseWindow.registerWebContentsEventHandler).to.have.been.calledWith('will-navigate');
    });
    
    it('should prevent page to navigate to local files', () => {
      const event = { preventDefault: sandbox.stub() };
      let url = 'http://hohoho.net';
      webContentsEventHandlers['will-navigate'](event, url);
      expect(event.preventDefault).not.to.have.been.called;
      
      url = 'file://big/bad/path';
      webContentsEventHandlers['will-navigate'](event, url);
      expect(event.preventDefault).to.have.been.called;
    });
    
    it('should handle failed external page loads', () => {
      sandbox.stub(networkInterceptors, 'failedRequest');
      sandbox.stub(baseWindow, 'close');
      
      webContentsEventHandlers['did-fail-load'](null, -1);
      
      expect(networkInterceptors.failedRequest).not.to.have.been.called;
    });

    it('should ignore error code -3', () => {
      sandbox.stub(networkInterceptors, 'failedRequest');
      sandbox.stub(baseWindow, 'close');

      webContentsEventHandlers['did-fail-load'](null, -3);

      expect(networkInterceptors.failedRequest).not.to.have.been.called;
      expect(baseWindow.close).not.to.have.been.called;
    });
    it('should set the netowrk as offline only if the error code -106', () => {
      sandbox.stub(networkInterceptors, 'failedRequest');
      sandbox.stub(baseWindow, 'close');

      webContentsEventHandlers['did-fail-load'](null, -106);

      expect(networkInterceptors.failedRequest).to.have.been.called;
    });
  });
  
  describe('openDevTools', () => {
    beforeEach(() => {
      sandbox.stub(baseWindow, 'show');
      baseWindow.browserWindow = { webContents: { openDevTools: sandbox.stub() } };
      baseWindow.openDevTools();
    });
    
    it('should open the browser window', () => {
      expect(baseWindow.show).to.have.been.called;
    });
    it('should open the dev tools window', () => {
      expect(baseWindow.browserWindow.webContents.openDevTools).to.have.been.called;
    });
  });
  
  describe('reload', () => {
    let stub;
    beforeEach(() => {
      stub = sandbox.stub();
      baseWindow.browserWindow = { webContents: { reloadIgnoringCache: stub } };
    });
    
    it('should not reload if browserWindow is null', () => {
      baseWindow.browserWindow = null;
      baseWindow.reload();
      expect(stub).not.to.have.been.called;
    });
  
    it('should reload if browserWindow is not null', () => {
      baseWindow.reload();
      expect(stub).to.have.been.called;
    });
  });
  
  describe('show', () => {
    let page, queryParameters;
    describe('when browserWindow is not null', () => {
      beforeEach(() => {
        queryParameters = 'hoho=haha';
        baseWindow.browserWindow = browserWindowInstance;
        sandbox.stub(baseWindow, 'setupProxy').resolves();
      });
      
      describe('when no page is passed', () => {
        beforeEach(async () => {
          await baseWindow.show(page, queryParameters);
        });
  
        it('should show window', () => {
          expect(baseWindow.browserWindow.show).to.have.been.called;
        });
  
        it('should focus window', () => {
          expect(baseWindow.browserWindow.focus).to.have.been.called;
        });
  
        it('should not reload window', () => {
          expect(baseWindow.browserWindow.loadURL).not.to.have.been.called;
        });
  
        it('should not bind on change events again', () => {
          expect(baseWindow.browserWindow.on).not.to.have.been.called;
        });
      });
      
      describe('when page is passed', () => {
        beforeEach(async () => {
          page = 'hohoho';
          sandbox.stub(baseWindow, 'loadPage');
          await baseWindow.show(page, queryParameters);
        });
  
        it('should load Page if page is passed', () => {
          expect(baseWindow.loadPage).to.have.been.calledWith(page, queryParameters);
        });
      });
    });
    
    describe('when browserWindow is null', () => {
      beforeEach(() => {
        baseWindow.browserWindow = null;
        baseWindow.webContentsEventHandlers = {};
        sandbox.stub(baseWindow, 'setupProxy').resolves();
      });
      
      describe('when parentWindow and its browserWindow are defined and the window is visible', () => {
        let parentInfo;
        beforeEach(async () => {
          parentInfo = {
            x: 1,
            y: 2,
            width: 10,
            height: 10
          };
          baseWindow.initOptions.width = 8;
          baseWindow.initOptions.height = 8;
          baseWindow.parentWindow.browserWindow = sinon.createStubInstance(BrowserWindow);
          baseWindow.parentWindow.browserWindow.isVisible.returns(true);
          baseWindow.parentWindow.browserWindow.getPosition.returns([parentInfo.x, parentInfo.y]);
          baseWindow.parentWindow.browserWindow.getSize.returns([parentInfo.width, parentInfo.height]);
          await baseWindow.show();
        });
  
        it('should set center option to false', () => {
          expect(baseWindow.browserWindow.options.center).to.be.false;
        });
        it('should position window appropriately if the window state is not set', () => {
          expect(baseWindow.browserWindow.options.x).to.equal(2);
          expect(baseWindow.browserWindow.options.y).to.equal(3);
        });

      });
      describe('when the window state is set', () => {
        let parentInfo;
        beforeEach(async () => {
          parentInfo = {
            x: 1,
            y: 2,
            width: 10,
            height: 10
          };
          baseWindow.initOptions.width = 8;
          baseWindow.initOptions.height = 8;
          baseWindow.parentWindow.browserWindow = sinon.createStubInstance(BrowserWindow);
          baseWindow.parentWindow.browserWindow.isVisible.returns(true);
          baseWindow.parentWindow.browserWindow.getPosition.returns([parentInfo.x, parentInfo.y]);
          baseWindow.parentWindow.browserWindow.getSize.returns([parentInfo.width, parentInfo.height]);
          windowStateStub.width = 100;
          windowStateStub.height = 200;
          windowStateStub.x = 300;
          windowStateStub.y = 400;
          await baseWindow.show();
        });

        it('should position window appropriately if the window state is set', () => {
          expect(baseWindow.browserWindow.options.width).to.equal(100);
          expect(baseWindow.browserWindow.options.height).to.equal(200);
        });

        it('should set the window size appropriately if the window state is set', () => {

          expect(baseWindow.browserWindow.options.x).to.equal(300);
          expect(baseWindow.browserWindow.options.y).to.equal(400);
        });
      });
      
      it('should not set parent option if parentWindow is not visible', async () => {
        await baseWindow.show();
        baseWindow.parentWindow.browserWindow = sinon.createStubInstance(BrowserWindow);
        baseWindow.parentWindow.browserWindow.isVisible.returns(false);
        expect(baseWindow.browserWindow.options.parent).not.to.be.defined;
      });
      
      it('should not set parent option if parentWindow\'s browserWindow is undefined', async () => {
        await baseWindow.show();
        expect(baseWindow.browserWindow.options.parent).not.to.be.defined;
      });
  
      it('should not set parent option if parentWindow is undefined', async () => {
        baseWindow.parentWindow = undefined;
        await baseWindow.show();
        expect(baseWindow.browserWindow.options.parent).not.to.be.defined;
      });
  
      it('should create a new BrowserWindow', async () => {
        await baseWindow.show();
        expect(baseWindow.browserWindow).to.be.an.instanceof(BrowserWindow);
      });
      
      it('should load page if passed', async () => {
        page = 'hohoho';
        sandbox.stub(baseWindow, 'loadPage');
        await baseWindow.show(page, queryParameters);
        expect(baseWindow.loadPage).to.have.been.calledWith(page, queryParameters);
      });
      
      it('should load base page if no page was passed', async () => {
        baseWindow.basePage = 'hahaha';
        sandbox.stub(baseWindow, 'loadPage');
        await baseWindow.show();
        expect(baseWindow.loadPage).to.have.been.calledWith(baseWindow.basePage);
      });
      
      it('should bind to the closed event', async () => {
        await baseWindow.show();
        expect(baseWindow.browserWindow.on).to.have.been.calledWith('closed');
      });
  
      it('should bind its webContents events', async () => {
        const webContents = { on: sandbox.stub() };
        baseWindow.webContentsEventHandlers = {
          hohoho: [],
          hehehe: []
        };
        sandbox.stub(browserWindowInstance, 'webContents').get(() => webContents);
        await baseWindow.show();
        expect(baseWindow.browserWindow.webContents.on).to.have.been.calledWith('hohoho');
        expect(baseWindow.browserWindow.webContents.on).to.have.been.calledWith('hehehe');
      });
      
      describe('when window is closed', () => {
        beforeEach(async () => {
          // Store correct callback.
          let closedEventCallback;
          browserWindowInstance.on.callsFake((eventName, callback) => {
            if(eventName === 'closed') {
              closedEventCallback = callback;
            }
          });
          
          // Stubs
          sandbox.stub(postal, 'publish');
          
          await baseWindow.show();
          closedEventCallback();
        });
        
        it('should send correct event through postal', () => {
          expect(postal.publish).to.have.been.calledWith(sinon.match({
            channel: 'app',
            topic: 'base-window.change',
            data: {
              state: 'all-closed',
              from: 'browserWindow:closed'
            }
          }));
        });
        
        it('should set browser window to null', () => {
          expect(baseWindow.browserWindow).to.be.null;
        });
      });
      
      describe('when webContents event is triggered', () => {
        let eventCallbacks;
        beforeEach(async () => {
          // Setup webContents
          const webContents = { on: sandbox.stub() };
          sandbox.stub(browserWindowInstance, 'webContents').get(() => webContents);
          
          // Setup handlers
          baseWindow.webContentsEventHandlers = {
            'closed': [sandbox.stub()],
            'sunday': [sandbox.stub(), sandbox.stub()]
          };
          
          // Setup event callbacks
          eventCallbacks = {};
          webContents.on.callsFake((eventName, callback) => {
            eventCallbacks[eventName] = callback;
          });
          
          await baseWindow.show();
        });
        
        it('should call the specific callbacks with passed arguments', () => {
          _.each(baseWindow.webContentsEventHandlers, (stubs, eventName) => {
            // Data
            const args = _.sampleSize(['foo', {bar: 1}, 1, ['something']], 2);
            
            // Trigger webContents event
            eventCallbacks[eventName](...args);
            
            stubs.forEach((stub) => expect(stub).to.have.been.calledWith(...args));
          });
        });
      });
    });
  });
  
  describe('setupProxy', () => {
    let hasErrors;
    let error;
    beforeEach(() => {
      hasErrors = false;
      error = { message: 'foo' };
      baseWindow.browserWindow = browserWindowInstance;
      const setProxy = sandbox.stub().callsFake((rules, callback) => {
        if (hasErrors) {
          callback(error);
        } else {
          callback()
        }
      });
      sandbox.stub(baseWindow.browserWindow, 'webContents').get(() => ({ session: { setProxy } }));
    });
    
    describe('when http proxy is set', () => {
      it('should set browserWindow proxy properly', async () => {
        const proxy = 'http://test.com';
        const expectedResult = { proxyRules: `http=${proxy};` };
        sandbox.stub(proxyHelper, 'http').get(() => ({ stringWithoutAuth: proxy }));
        await baseWindow.setupProxy();
        expect(baseWindow.browserWindow.webContents.session.setProxy).to.have.been.calledWith(expectedResult)
      });
    });
    
    describe('when https proxy is set', async () => {
      it('should set browserWindow proxy properly', async () => {
        const proxy = 'https://test.com';
        const expectedResult = { proxyRules: `https=${proxy}` };
        sandbox.stub(proxyHelper, 'https').get(() => ({ stringWithoutAuth: proxy }));
        await baseWindow.setupProxy();
        expect(baseWindow.browserWindow.webContents.session.setProxy).to.have.been.calledWith(expectedResult)
      })
    });
  
    describe('when http and https proxy is set', () => {
      it('should set browserWindow proxy properly', async () => {
        const httpProxy = 'http://test.com';
        const httpsProxy = 'https://bar.com';
        const expectedResult = { proxyRules: `http=${httpProxy};https=${httpsProxy}` };
        sandbox.stub(proxyHelper, 'http').get(() => ({ stringWithoutAuth: httpProxy }));
        sandbox.stub(proxyHelper, 'https').get(() => ({ stringWithoutAuth: httpsProxy }));
        await baseWindow.setupProxy();
        expect(baseWindow.browserWindow.webContents.session.setProxy).to.have.been.calledWith(expectedResult)
      });
    });
    
    describe('when no proxy is set', () => {
      it('should not setup proxy', async () => {
        sandbox.stub(proxyHelper, 'http').get(() => undefined);
        sandbox.stub(proxyHelper, 'https').get(() => undefined);
        await baseWindow.setupProxy();
        expect(baseWindow.browserWindow.webContents.session.setProxy).not.to.have.been.called;
      });
    });
    
    describe('when browserWindow returns an error while setting the proxy', () => {
      it('should reject with the error', () => {
        const httpProxy = 'http://test.com';
        sandbox.stub(proxyHelper, 'http').get(() => ({ stringWithoutAuth: httpProxy }));
        hasErrors = true;
        expect(baseWindow.setupProxy()).to.eventually.be.rejectedWith(error);
      });
    })
  });
  
  describe('showOpenFileDialog', () => {
    let options;
    beforeEach(() => {
      options = {
        properties: ['openFile']
      };
      sandbox.stub(dialog, 'showOpenDialog');
    });
    
    it('should reject when browserWindow is not defined', () => {
      return expect(baseWindow.showOpenFileDialog(options)).to.be.rejected;
    });
  
    it('should throw if both file and directory are selected as file types', () => {
      options.properties.push('openDirectory');
      baseWindow.browserWindow = browserWindowInstance;
      expect(() => baseWindow.showOpenFileDialog(options)).to.throw(Error);
    });
  
    it('should throw if no file type has been selected', () => {
      options.properties = [];
      baseWindow.browserWindow = browserWindowInstance;
      expect(() => baseWindow.showOpenFileDialog(options)).to.throw(Error);
    });
  
    it('should not throw if file type is openDirectory', () => {
      options.properties = ['openDirectory'];
      baseWindow.browserWindow = browserWindowInstance;
      dialog.showOpenDialog.callsFake((parent, options, callback) => {
        callback(['hohoh']);
      });
      return expect(() => baseWindow.showOpenFileDialog(options)).not.to.throw(Error);
    });
  
  
    it('should open a dialog box to have the user select files or folders if browserWindow is defined', () => {
      dialog.showOpenDialog.callsFake((parent, options, callback) => {
        callback(['hohoh']);
      });
      baseWindow.browserWindow = browserWindowInstance;
      return baseWindow.showOpenFileDialog(options)
        .then(() => {
          expect(dialog.showOpenDialog).to.have.been.calledWith(baseWindow.browserWindow, options);
        });
    });
    
    it('should resolve with single file if user selected file', () => {
      const filenames = ['bob'];
      dialog.showOpenDialog.callsFake((parent, options, callback) => {
        callback(filenames);
      });
      baseWindow.browserWindow = browserWindowInstance;
      return baseWindow.showOpenFileDialog(options)
        .then((result) => {
          expect(result).to.eql(filenames[0]);
        });
    });
  
    it('should resolve with selected files if user selected files and multiSelection was enabled', () => {
      options.properties.push('multiSelections');
      const filenames = ['bob', 'bill', 'doe'];
      dialog.showOpenDialog.callsFake((parent, options, callback) => {
        callback(filenames);
      });
      baseWindow.browserWindow = browserWindowInstance;
      return baseWindow.showOpenFileDialog(options)
        .then((result) => {
          expect(result).to.eql(filenames);
        });
    });
  
    it('should reject with selected files if browserWindow is defined and user did not select files', () => {
      dialog.showOpenDialog.callsFake((parent, options, callback) => {
        callback();
      });
      baseWindow.browserWindow = browserWindowInstance;
      return expect(baseWindow.showOpenFileDialog(options)).to.be.rejected;
    });
  });

  describe('hide', () => {
    beforeEach(() => {
      sandbox.stub(postal, 'publish');
    });
    
    it('should do nothing if browserWindow is null', () => {
      baseWindow.hide();
      expect(browserWindowInstance.hide).not.to.have.been.called;
      expect(postal.publish).not.to.have.been.called;
    });
    
    describe('when browserWindow is not null', () => {
      beforeEach(() => {
        baseWindow.browserWindow = browserWindowInstance;
        baseWindow.hide();
      });
      
      it('should minimize window', () => {
        expect(baseWindow.browserWindow.hide).to.have.been.called;
      });
  
      it('should send correct event through postal', () => {
        expect(postal.publish).to.have.been.calledWith(sinon.match({
          channel: 'app',
          topic: 'base-window.change',
          data: {
            state: 'all-closed',
            from: 'browserWindow:hide'
          }
        }));
      });
    });
  });

  describe('close', () => {
    it('should do nothing if browserWindow is null', () => {
      baseWindow.close();
      expect(browserWindowInstance.close).not.to.have.been.called;
    });
    
    it('should close window if browserWindow is not null', () => {
      baseWindow.browserWindow = browserWindowInstance;
      baseWindow.close();
      expect(browserWindowInstance.close).to.have.been.called;
    });
  });
  
  describe('isFocused', () => {
    it('should return false when browserWindow is null', () => {
      expect(baseWindow.isFocused()).to.be.false;
    });
    
    it('should return false when browserWindow is not null but window is not focused', () => {
      browserWindowInstance.isFocused.returns(false);
      baseWindow.browserWindow = browserWindowInstance;
      expect(baseWindow.isFocused()).to.be.false;
    });
    
    it('should return true when browserWindow is not null and window is focused', () => {
      browserWindowInstance.isFocused.returns(true);
      baseWindow.browserWindow = browserWindowInstance;
      expect(baseWindow.isFocused()).to.be.true;
    });
  });
  
  describe('isVisible', () => {
    it('should return false when browserWindow is null', () => {
      expect(baseWindow.isVisible()).to.be.false;
    });
    
    it('should return false when browserWindow is not null but window is not visible', () => {
      browserWindowInstance.isVisible.returns(false);
      baseWindow.browserWindow = browserWindowInstance;
      expect(baseWindow.isVisible()).to.be.false;
    });
    
    it('should return true when browserWindow is not null and window is visible', () => {
      browserWindowInstance.isVisible.returns(true);
      baseWindow.browserWindow = browserWindowInstance;
      expect(baseWindow.isVisible()).to.be.true;
    });
  });
  
  describe('loadPage', () => {
    let page, queryParameters;
    beforeEach(() => {
      page = 'fun';
      queryParameters = 'foo=bar';
      sandbox.stub(baseWindow, 'loadFragmentURL');
    });
    
    it('should load the passed page if valid page is passed', () => {
      sandbox.stub(baseWindow, 'pages').get(() => ({FUN: 'fun'}));
      baseWindow.loadPage(page, queryParameters);
      expect(baseWindow.loadFragmentURL).to.have.been.calledWith(page, queryParameters);
    });
  
    it('should not load the passed page if invalid page is passed', () => {
      baseWindow.loadPage(page);
      expect(baseWindow.loadFragmentURL).not.to.have.been.called;
    });
  });

  describe('loadURL()', () => {
    it('should load passed URL', () => {
      const url = 'http://fun.fun.fun.org';
      baseWindow.browserWindow = browserWindowInstance;
      baseWindow.loadURL(url);
      expect(baseWindow.browserWindow.loadURL).to.have.been.calledWith(url);
    });
    
    it('should append query parameters if passed', () => {
      const url = 'http://fun.fun.fun.org';
      const params = 'a=1&b=2';
      baseWindow.browserWindow = browserWindowInstance;
      baseWindow.loadURL(url, params);
      expect(baseWindow.browserWindow.loadURL).to.have.been.calledWith(`${url}?${params}`);
    });
  });

  describe('loadFragmentURL()', () => {
    let fragment, webContents, currentURL;
    beforeEach(() => {
      // Data
      fragment = '#!login';
      currentURL = rootPath;
    
      // Setup
      webContents = { getURL: sandbox.stub().callsFake(() => currentURL) };
      sandbox.stub(browserWindowInstance, 'webContents').get(() => webContents);
      baseWindow.browserWindow = browserWindowInstance;
    });
    
    it('should load new URL with passed fragment', () => {
      baseWindow.loadFragmentURL(fragment);
      expect(baseWindow.browserWindow.loadURL).to.have.been.calledWith(`${rootPath}${fragment}`);
    });
  
    it('should prepend pound sign if not passed in fragment', () => {
      fragment = 'license';
      baseWindow.loadFragmentURL(fragment);
      expect(baseWindow.browserWindow.loadURL).to.have.been.calledWith(`${rootPath}#!${fragment}`);
    });

    it('should support the case where fragment starts with #/', () => {
      fragment = '#/manage';
      const realFragment = fragment.substring(1);
      baseWindow.loadFragmentURL(fragment);
      expect(baseWindow.browserWindow.loadURL).to.have.been.calledWith(`${rootPath}#!${realFragment}`);
    });

    it('should add query parameters if passed', () => {
      const params = 'query=null&null=query';
      baseWindow.loadFragmentURL(fragment, params);
      expect(baseWindow.browserWindow.loadURL).to.have.been.calledWith(`${rootPath}${fragment}?${params}`);
    });
  
    it('should prepend pound sign if not passed and add query parameters if passed', () => {
      fragment = 'license';
      const params = 'query=null&null=query';
      baseWindow.loadFragmentURL(fragment, params);
      expect(baseWindow.browserWindow.loadURL).to.have.been.calledWith(`${rootPath}#!${fragment}?${params}`);
    });
    
    it('should not redirect if current url is the same', () => {
      currentURL = `${rootPath}${fragment}`;
      baseWindow.loadFragmentURL(fragment);
      expect(baseWindow.browserWindow.loadURL).not.to.have.been.called;
    });
  });

  describe('sendContent', () => {
    let eventName, data, webContents;
    beforeEach(() => {
      // Data
      eventName = 'Oh noes!';
      data = [1, 2, 3];
  
      // Setup
      webContents = { send: sandbox.stub() };
      sandbox.stub(browserWindowInstance, 'webContents').get(() => webContents);
    });
    
    it('should do nothing if browserWindow is not defined', () => {
      baseWindow.sendContent();
      expect(browserWindowInstance.webContents.send).not.to.have.been.called;
    });
    
    it('should do nothing if browserWindow is defined but webContents is not', () => {
      baseWindow.browserWindow = browserWindowInstance;
      sandbox.stub(browserWindowInstance, 'webContents').get(() => undefined);
      baseWindow.sendContent();
      expect(webContents.send).not.to.have.been.called;
    });
  
    it('should send event with data if browserWindow and webContents is defined', () => {
      baseWindow.browserWindow = browserWindowInstance;
      baseWindow.sendContent(eventName, ...data);
      expect(webContents.send).to.have.been.calledWith(eventName, ...data);
    });
  });
  
  describe('getNativeWindowHandle', () => {
    it('should return null if browserWindow is null', () => {
      expect(baseWindow.getNativeWindowHandle()).to.be.null;
    });
    
    it('should return handle if browserWindow is not null', () => {
      const handle = {foo: 'bar'};
      browserWindowInstance.getNativeWindowHandle.returns(handle);
      baseWindow.browserWindow = browserWindowInstance;
      expect(baseWindow.getNativeWindowHandle()).to.eql(handle);
    });
  });
  
  describe('registerWebContentsEventHandler', () => {
    it('should add handler to handlers list for passed event', () => {
      baseWindow.registerWebContentsEventHandler.restore();
      const eventName = 'hoho';
      const handler = function() {};
      baseWindow.registerWebContentsEventHandler(eventName, handler);
      expect(baseWindow.webContentsEventHandlers[eventName][0]).to.equal(handler);
      const handler2 = function() {};
      baseWindow.registerWebContentsEventHandler(eventName, handler2);
      expect(baseWindow.webContentsEventHandlers[eventName][1]).to.equal(handler2);
    });
  });
});
