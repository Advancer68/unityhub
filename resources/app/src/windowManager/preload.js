if (window.location.href.startsWith('https://www.facebook.com/')) {
  window.require = null;
}

if (window.location.href.indexOf('.qq.com/') > 0) {
  // electron has some issue with jQuery, need to workaround
  // https://github.com/electron/electron/issues/254
  delete window.module;
}
