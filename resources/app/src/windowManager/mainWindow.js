'use strict';

const BaseWindow = require('./baseWindow');
const path = require('path');
const systemInfo = require('../services/localSystemInfo/systemInfo');
const { i18n } = require('../i18nHelper');

const rootPath = `file://${path.resolve(__dirname, '../../client/dist/index.html')}`;

class MainWindow extends BaseWindow {

  get name() { return 'main-window'; }

  get pages() {
    return {
      HOME: 'project/recent',
      RECENT_PROJECTS: 'project/recent',
      COMMUNITY: 'community',
      LEARN: 'learn',
      UPR: 'upr',
      INSTALLS: 'install',
      LICENSE_MANAGEMENT: '/licenseManagement',
      CLOSED_NET_ERROR: 'closedneterror'
    };
  }

  constructor(optionsOverride, parentWindow) {
    const options = Object.assign({
      width: 1280,
      height: 720,
      minWidth: 1280,
      minHeight: 720,
      center: true,
      resizable: true,
      fullscreen: false,
      fullscreenable: false,
      title: 'Unity Hub',
      webPreferences: {
        textAreasAreResizable: false,
        preload: path.join(__dirname, 'preloads', 'mainWindowPreload.js'),
        nodeIntegration: true,
      },
    }, optionsOverride);
    super(rootPath, options, parentWindow);

    this.basePage = this.pages.HOME;
  }

  setTitle(modalEditorVersion) {
    const version = systemInfo.hubVersion().replace('-', '');
    const title = i18n.translate('WINDOW.TITLE', { version });

    if (modalEditorVersion === undefined) {
      super.setTitle(title);
    } else {
      super.setTitle(`${title} ${i18n.translate('WINDOW.DEBUG_MODE', { version: modalEditorVersion })}`);
    }
  }
}

module.exports = MainWindow;
