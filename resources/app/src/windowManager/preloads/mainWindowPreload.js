const remote = require('electron').remote;
const path = require('path');

const logger = remote.require(path.join(__dirname, '../../logger'))('Front-End');

window.onerror = function (message, source, lineno, colno, error) {
  logger.error(error ? error.stack : message);
};
