!macro customInstall
  # Refresh desktop icons
  System::Call 'shell32::SHChangeNotify(i, i, i, i) v (0x08000000, 0, 0, 0)' #Refreshes the desktop

  # Register custom URI Scheme
  DetailPrint "Register unityhub URI Handler"
  DeleteRegKey HKCR "unityhub"
  WriteRegStr HKCR "unityhub" "" "URL:unityhub"
  WriteRegStr HKCR "unityhub" "URL Protocol" ""
  WriteRegStr HKCR "unityhub\DefaultIcon" "" "$INSTDIR\Unity Hub.exe"
  WriteRegStr HKCR "unityhub\shell" "" ""
  WriteRegStr HKCR "unityhub\shell\Open" "" ""
  WriteRegStr HKCR "unityhub\shell\Open\command" "" "$INSTDIR\Unity Hub.exe %1"

  # Remove desktop icon
  #WinShell::UninstShortcut "$desktopLink" #Removes pins using the shortcut
  #Delete "$desktopLink" #Deletes the shortcut
  #System::Call 'shell32::SHChangeNotify(i, i, i, i) v (0x08000000, 0, 0, 0)' #Refreshes the desktop

  # Add the firewall rules for the Hub
  ExecWait 'netsh advfirewall firewall delete rule name=all program="$INSTDIR\Unity Hub.exe"'
  ExecWait 'netsh advfirewall firewall add rule name="Unity Hub" dir=in action=allow program="$INSTDIR\Unity Hub.exe" profile=domain protocol=any'
!macroend
